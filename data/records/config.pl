#!/usr/bin/perl -w
# Perl config file with Record Breaker specific customisations, etc

our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

$config{'base_dir'} .= '/records'; # Append the game dir
$config{'setup_dir'} = abs_path(__DIR__ . '/../../setup/records');
$config{'config_dir'} = abs_path(__DIR__ . '/../../config');
$config{'config_env'} = "$config{'config_dir'}/_env.php"; # This file may not exist...

# We may be passed a bespoke log directory to use
$config{'log_dir'} = "$ENV{'DIR_LOG'}/"
  if defined($ENV{'DIR_LOG'});

# Return true to pacify the compiler
1;
