#!/usr/bin/perl -w
# Process Record Breaker games on a period boundary

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get base config
our %config;
foreach my $path ('config.pl', '../_global/update.pl') {
  my $config = abs_path(__DIR__ . "/$path"); require $config;
}

# Validate the arguments
#  - there needs to be at least one: a sport
#  - there could also be an additional argument though: date (absence implies "today")
if (!@ARGV) {
  print STDERR "Insufficient arguments to the processing wrapper script.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Declare variables
my ($sport, $date) = @ARGV;
$date = time2date(time()) if !defined($date) || $date !~ m/^20[1-9]\d-[01]\d-[0-3]\d$/;
$config{'process_only'} = grep(/^--process$/, @ARGV); # Run only the 'process' calculations (period we have just completed)
$config{'preview_only'} = grep(/^--preview$/, @ARGV); # Run only the 'preview' calculations (period we will be starting)

# Validate the arguments
if (! -e "$config{'setup_dir'}/$sport") {
  print STDERR "Unknown sport argument '$sport'.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Define log details
$config{'log_dir'} .= $sport;
my %logs = get_log_def();

# Prepare our MySQL command
my $cmd_mysql = "/usr/bin/mysql --defaults-extra-file=$config{'mysql_def_dir'}/$config{'db_user'}.conf --table $config{'db_name'}";

# Inform user
print "Processing $sport game(s) on $date\n";
print "- Processing-only steps requested\n" if $config{'process_only'};
print "- Preview-only steps requested\n" if $config{'preview_only'};
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# These group of scripts are core to the script
$config{'exit_status'} = 'core';

##
## Process previous round
##
if (!$config{'preview_only'}) {
  # Process the previous round
  print '=> Process Previous Period: ';
  command("echo \"CALL records_process('$sport', '$date');\" | $cmd_mysql",
          "$config{'log_dir'}/$logs{'prev_calc'}.log",
          "$config{'log_dir'}/$logs{'prev_calc'}.err");
  done(0);

  # Open Graph image generation for the previous round
  print '=> Process Open Graph: ';
  command("/usr/bin/php $config{'skel_dir'}/artisan debear:fantasy:records-og-create --sport='$sport' --date='$date'",
          "$config{'log_dir'}/$logs{'prev_og'}.log",
          "$config{'log_dir'}/$logs{'prev_og'}.err");
  done(5);
}

##
## Process any social media posts
##
print '=> Process Social Media: ';
command("echo \"CALL records_social_media('$sport', '$date');\" | $cmd_mysql",
        "$config{'log_dir'}/$logs{'social_media'}.log",
        "$config{'log_dir'}/$logs{'social_media'}.err");
done(3);

##
## Prepare for next round
##
if (!$config{'process_only'}) {
  # Ensure the period dates line up
  print '=> Validate Period Dates: ';
  command("echo \"CALL records_validate_dates('$sport', '$date');\" | $cmd_mysql",
          "$config{'log_dir'}/$logs{'next_dates'}.log",
          "$config{'log_dir'}/$logs{'next_dates'}.err");
  done(2);

  # Preview the subsequent round
  print '=> Preview Next Period: ';
  command("echo \"CALL records_preview('$sport', '$date');\" | $cmd_mysql",
          "$config{'log_dir'}/$logs{'next_calc'}.log",
          "$config{'log_dir'}/$logs{'next_calc'}.err");
  done(4);
}

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'prev_calc'    => '01_process',
    'prev_og'      => '02_opengraph',
    'social_media' => '03_social',
    'next_dates'   => '04_fixdates',
    'next_calc'    => '05_preview',
  );
}
