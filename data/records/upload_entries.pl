#!/usr/bin/perl -w
# Safely upload period entry scoring changes to live, without stamping over changes to future periods

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;

our %_networking;
require '/var/www/debear/lib/network.pl';

# Validate the arguments
#  - there needs to be at least three: a sport, a season and remote
#  - there could also be an additional argument though: date (absence implies "today")
if (@ARGV < 3) {
  print STDERR "Insufficient arguments to the processing upload script.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Declare variables
my ($sport, $season, $remote) = @ARGV;

# Validate the arguments
if (! -e "$config{'setup_dir'}/$sport") {
  print STDERR "Unknown sport argument '$sport'.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
} elsif (! -e "$config{'setup_dir'}/$sport/$season.sql") {
  print STDERR "Unknown season argument '$season'.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
} elsif ($remote !~ m/^(data|live)$/) {
  print STDERR "Unknown remote argument '$remote'.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}
$config{'debug'} = grep(/^--debug$/, @ARGV);
$config{'to_live'} = ($remote eq 'live');
$config{'full_sync'} = (!$config{'to_live'} || grep(/^--full$/, @ARGV));
$config{'date_from_env'} = 0; # Determine 'today' via the _env override
$config{'sync_file'} = sprintf('/tmp/sync-fantasy.records.%s.%03d.sql.gz', $$, rand(1000));
unlink($config{'sync_file'}) if -e $config{'sync_file'};

# Prepare our MySQL command
my $cmd_mysqldump = "/usr/bin/mysqldump --defaults-extra-file=$config{'mysql_def_dir'}/$config{'db_name'}.conf --no-tablespaces $config{'db_name'}";

# Inform user what we are about to do
print "## Generating " . ($config{'full_sync'} ? '(full) ' : '') . "period scoring SQL upload to $remote for $sport-$season game(s)\n";

# Determine the game(s) and period(s) we are covering
my $date = get_date();
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT GAME.season, GAME.game_id, GAME.name AS game_name, PERIOD.period_id,
  IFNULL(PERIOD.name, CONCAT(PERIOD.start_date, IF(PERIOD.start_date <> PERIOD.end_date, CONCAT(" to ", PERIOD.end_date), ""))) AS period_name
FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD
JOIN FANTASY_RECORDS_GAMES AS GAME
  ON (GAME.sport = PERIOD.sport
  AND GAME.season = PERIOD.season
  AND PERIOD.start_date BETWEEN DATE(GAME.date_start) AND DATE(GAME.date_end))
JOIN FANTASY_RECORDS_GAMES_PERIODS_SUMMARY AS SUMMARY
  ON (SUMMARY.sport = GAME.sport
  AND SUMMARY.season = GAME.season
  AND SUMMARY.game_id = GAME.game_id
  AND SUMMARY.period_id = PERIOD.period_id)
WHERE PERIOD.sport = ?
AND   PERIOD.season = ?
AND   PERIOD.start_date < ?
AND   (? = 1 OR SUMMARY.when_synced IS NULL)
ORDER BY PERIOD.period_order, GAME.game_id;';
my $game_list = $dbh->selectall_arrayref($sql, { Slice => {} }, $sport, $season, $date, $config{'full_sync'});
$dbh->disconnect if defined($dbh);

# Silently abort if no games found
exit $config{'exit_codes'}{'okay'}
  if !@$game_list;

# Loop through the games to generate the changes
foreach my $game (@$game_list) {
  print "\n##\n## Game $sport-$$game{'season'}-$$game{'game_id'} '$$game{'game_name'}', Period $$game{'period_id'} '$$game{'period_name'}'\n##\n";

  # Generate the SQL diff (in an order of least impact during the upload process)
  generate_diff('FANTASY_RECORDS_ENTRIES_BYPERIOD', "sport = '$sport' AND  season = '$$game{'season'}' AND  game_id = '$$game{'game_id'}' AND  period_id = '$$game{'period_id'}'", [
    'result', 'stress', 'opponents', 'achievements', 'overall_res', 'overall_pos', 'current_res', 'current_pos', 'hitrate_num', 'hitrate_pos',
  ]);
  generate_diff('FANTASY_RECORDS_ENTRIES_SCORES', "sport = '$sport' AND season = '$$game{'season'}' AND game_id = '$$game{'game_id'}'", [
    'overall_res', 'overall_pos', 'current_res', 'current_pos', 'stress', 'opponents', 'achievements', 'hitrate_num', 'hitrate_pos',
  ]);

  # State that we've synced (if uploading to live)
  if ($config{'to_live'}) {
    print "\n# Confirm that we've synced this game\n";
    system "echo \"UPDATE \\`FANTASY_RECORDS_GAMES_PERIODS_SUMMARY\\`
SET \\`when_synced\\` = NOW()
WHERE \\`sport\\` = '$sport'
AND   \\`season\\` = '$season'
AND   \\`game_id\\` = '$$game{'game_id'}'
AND   \\`period_id\\` = '$$game{'period_id'}';\" | gzip >>$config{'sync_file'}";
    exit_on_error($?, "An error occurred whilst appending the sync-complete step to the dump", 'core');
  }
}

# Finally, import the file to the appropriate destination
print "\n##\n## Uploading changes\n##\n\n# Sync file: $config{'sync_file'}\n";
my $cmd_sync = "zcat $config{'sync_file'} | mysql --defaults-extra-file=$config{'mysql_def_dir'}/env_$remote.conf --host=127.0.0.1 --port=" . $_networking{$remote}{'port'} . " $config{'db_name'}";
print "# Sync command: $cmd_sync\n";
if ($config{'debug'}) {
  # In debug mode, just output the command we would have run
  print "# Skipping in 'debug', mode.\n";
} else {
  print "# ";
  system "/usr/bin/ls -l $config{'sync_file'}";
  exit_on_error($?, "An error occurred whilst confirming the status of the sync file '$config{'sync_file'}'", 'core');
  # Perform the upload
  open_tunnel($remote);
  system "$cmd_sync";
  exit_on_error($?, 'An error occurred whilst importing the SQL', 'core');
  close_tunnel();
  # On success, remove the sync file
  unlink $config{'sync_file'};
}

#
# Wrapper for use in testing, to determine the appropriate sync date
#
sub get_date {
  # Use the current date in all instances to live, or we're not using / relying on the _env file
  return time2date(time())
    if $config{'to_live'} || !$config{'date_from_env'} || ! -e $config{'config_env'};

  my $date = `grep -Po "'override' => '[^']+'" $config{'config_env'} 2>&1 | awk -F"'" '{ print \$4 }' | awk -F' ' '{ print \$1 }'`;
  # Ensure this succeeded
  exit_on_error($?, "An error occurred whilst determining the date via $config{'config_env'}", 'pre-flight');
  # If what we get back doesn't look like a date (but the command didn't fail), fall back to today
  chomp($date);
  if ($date !~ m/^20\d{2}\-[01]\d\-[0-3]\d$/) {
    print "# Unable to determine date from $config{'config_env'} - falling back to 'today'.\n";
    $config{'date_from_env'} = 0;
    $date = get_date();
  }
  return $date;
}


#
# Define a diff with an ON DUPLICATE KEY UPDATE clause
#
sub generate_diff {
  my ($tbl, $where, $odku_cols) = @_;
  my $odku = join(', ', map {"\`$_\` = VALUES(\`$_\`)"} @$odku_cols);

  print "\n# $tbl\n# - WHERE: $where\n# - ODKU: " . join(', ', @$odku_cols) . "\n";
  if ($config{'debug'}) {
      print "# Skipping in 'debug' mode.\n";
      return;
  }
  system "$cmd_mysqldump --skip-lock-tables --no-create-info --complete-insert --no-set-names --hex-blob --order-by-primary $tbl --where=\"$where\" | grep -P '^INSERT INTO' | sed -e 's/) VALUES (/) VALUES\\n  (/' -e 's/),(/),\\n  (/g' -e 's/;\$/\\nON DUPLICATE KEY UPDATE $odku;/g' | gzip >>$config{'sync_file'}";
  exit_on_error($?, "An error occurred whilst generating the $tbl dump", 'core');
}

#
# Wrapper methods around the core tunnel open/close methods to include display logging
#
sub open_tunnel {
  my ($remote) = @_;
  print '# - Opening SSH Tunnel: ';
  if (ssh_tunnel_open($remote)) {
    print '[ Done ]' . "\n";
  } else {
    print '[ Skipped ]' . "\n";
  }
}

sub close_tunnel {
  if (ssh_tunnel_close()) {
    print '# - Closing SSH Tunnel: [ Done ]' . "\n";
  }
}

#
# Validate the return code of an external command, and abort if it return an error state
#
sub exit_on_error {
  my ($rc, $msg, $exit_code) = @_;
  $rc = ($rc >> 8);
  if ($rc) {
    print STDERR "$msg\n";
    close_tunnel(); # If we opened it, ensure we close the tunnel
    exit $config{'exit_codes'}{$exit_code};
  }
}
