#!/bin/bash
# Upload the latest Open Graph images, but applying a narrower retention policy on live

# Some config
dir_base=$(dirname $0)
dir_tmp="/tmp/debear/fantasy/records/og/$$"
dir_og_base=debear/sites/cdn/htdocs/fantasy/records/og
dir_og_local="/var/www/$dir_og_base"
dir_og_live="~/$dir_og_base"

live_retention=3 # Keep no more than this number of images (per-game) on live

# Get and validate our arguments
sport="$1"
season="$2"
remote="$3"
[[ "x$4" = 'x--debug' ]] && rsync_flags="-v -n" || rsync_flags=""

dir_setup=$(realpath $dir_base/../../setup/records)
if [ ! -e "$dir_setup/$sport" ]; then
  echo "Unknown sport argument '$sport'." >&2
  exit 98
elif [ ! -e "$dir_setup/$sport/$season.sql" ]; then
  echo "Unknown season argument '$season'." >&2
  exit 98
elif [ $(echo "$remote" | grep -Pc '^(data|live)$') -eq 0 ]; then
  echo "Unknown remote argument '$remote'." >&2
  exit 98
fi

# If we're syncing to data, things are a little easier as we do not apply retention
if [ "x$remote" = 'xdata' ]; then
  rsync -az $rsync_flags $dir_og_local/$sport-$season-* data:$dir_og_local/
  exit $?
fi

# If we're syncing to live, we need to sync per game and determine the recent images to be kept
mkdir -p $dir_tmp
cd $dir_og_local
if [ $(ls $sport-$season-*.png 2>/dev/null | wc -l) -gt 0 ]; then
  rsync -az --delete $sport-$season-*.png $dir_tmp/
fi
for game_ref in $(ls -d $sport-$season-?); do
  mkdir $dir_tmp/$game_ref
  # Determine the latest entries to be uploaded in our temporary location (explicitly ignore dry-run in any debug mode flags)
  rsync -az --delete $(echo $rsync_flags | sed 's/ -n//') $(ls -t $game_ref/* | head -n $live_retention) $dir_tmp/$game_ref/
  # Perform some error handling
  retcode=$?
  if [ $retcode -gt 0 ]; then
    exit $retcode
  fi
done
# And now upload these to remote, with appropriate flags to perform the retention
cd $dir_tmp
rsync -az $rsync_flags --delete $sport-$season-* debear-nt:$dir_og_live/
retcode=$? # Preserve for a moment, whilst we...
rm -rf $dir_tmp # ...prune our temporary dir now we've finished with it...
exit $retcode # ...and now return our rsync exit code
