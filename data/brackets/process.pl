#!/usr/bin/perl -w
# Process Bracket Challenge games on a period boundary

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get base config
our %config;
foreach my $path ('config.pl', '../_global/update.pl') {
  my $config = abs_path(__DIR__ . "/$path"); require $config;
}

# Validate the arguments
#  - there needs to be one: a sport
if (!@ARGV) {
  print STDERR "Insufficient arguments to the processing wrapper script.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Declare variables
my ($sport) = @ARGV;
my $date = time2date(time());

# Validate the arguments
if (! -e "$config{'setup_dir'}/$sport") {
  print STDERR "Unknown sport argument '$sport'.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Define log details
$config{'log_dir'} .= $sport;
my %logs = get_log_def();

# Prepare our MySQL command
my $cmd_mysql = "/usr/bin/mysql --defaults-extra-file=$config{'mysql_def_dir'}/$config{'db_user'}.conf --table $config{'db_name'}";
# Calculate the season being processed
my ($season, $curr_month) = ($date =~ m/^(20\d{2})-0?(\d{1,2})-/);
$season-- if ($sport eq 'nfl' && $curr_month <= 4) || ($sport =~ m/^[na]hl/ && $curr_month <= 8);

# Inform user
print "Processing $sport season $season on $date\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# These group of scripts are core to the script
$config{'exit_status'} = 'core';

##
## Game setup processing
##
# Process the previous round
print '=> Game Setup: ';
command("echo \"CALL brackets_setup('$sport', '$season');\" | $cmd_mysql",
        "$config{'log_dir'}/$logs{'setup'}.log",
        "$config{'log_dir'}/$logs{'setup'}.err");
done(0);

##
## Process previous round
##

##
## Process any social media posts
##

##
## Prepare for next round
##

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'setup' => '01_setup',
  );
}
