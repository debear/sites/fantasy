#!/usr/bin/perl -w
# Perl config file with Bracket Challenge specific customisations, etc

our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

$config{'base_dir'} .= '/brackets'; # Append the game dir
$config{'setup_dir'} = abs_path(__DIR__ . '/../../setup/brackets');

# Return true to pacify the compiler
1;
