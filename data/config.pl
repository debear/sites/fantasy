#!/usr/bin/perl -w
# Global perl config and methods for data scripts

use File::Path;
use MIME::Base64;

our %config;
$config{'base_dir'} = abs_path(__DIR__);
$config{'skel_dir'} = abs_path(__DIR__ . '/../../_debear');
$config{'mysql_def_dir'} = abs_path(__DIR__ . '/../../../etc/mysql');

# Database details
$config{'db_name'} = (defined($ENV{'DB_FANTASY_DATABASE'}) ? $ENV{'DB_FANTASY_DATABASE'} : 'debearco_fantasy');
$config{'db_user'} = (defined($ENV{'DB_FANTASY_DATABASE'}) ? 'debearco_sysadmin' : $config{'db_name'});
# Password
my $db_pass_file = $config{'base_dir'}; $db_pass_file =~ s@/sites/.+$@/etc/passwd/web/db@;
$config{'db_pass'} = do { local(@ARGV, $/) = $db_pass_file; <> }; chomp($config{'db_pass'});
$config{'db_pass'} = decode_base64($config{'db_pass'});

# Exit statuses
%{$config{'exit_codes'}} = (
  'okay' => 0,
  # Pre-requisite checks
  'failed-prereq' => 98,
  'failed-prereq-silent' => 97, # Same as above, but does not trigger a warning
  # Generic error
  'unknown' => 99,
  # Pre-processing
  'pre-flight' => 10,
  # Core part of script
  'core' => 20,
  # Arbitrarily grouped sections of post-processing
  'post-process-1' => 31,
  'post-process-2' => 32,
  'post-process-3' => 33,
  'post-process-4' => 34,
  'post-process-5' => 35,
);

# Load global methods
require $config{'base_dir'} . '/_global/benchmark.pl';
require $config{'base_dir'} . '/_global/file.pl';
require $config{'base_dir'} . '/_global/flow.pl';
require $config{'base_dir'} . '/_global/log.pl';
require $config{'base_dir'} . '/_global/strings.pl';

# Return true to pacify the compiler
1;
