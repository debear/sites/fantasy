// Generic page screenshotting process
const puppeteer = require('puppeteer-core');
const fs = require('fs');
const requestTimeout = 5000; // Milliseconds

// Validate the arguments we have been supplied
//  - There needs to be at least one JSON object
let jobsArg = process.argv.slice(2);
if (!jobsArg.length) {
    console.error('Insufficient arguments to the page screenshot puppeteer script');
    process.exit(1);
}
// Parse to ensure validity of the JSON objects
let jobs = [];
jobsArg.forEach((str, i) => {
    let json;
    try {
        // This appears to be a SemGrep false positive, as we are not parsing XML input but JSON
        // nosemgrep javascript-xml-rule-node_xpath_injection
        json = JSON.parse(str);
    } catch (err) {
        console.error('Argument %d does not appear to be valid JSON: %s', i, err.message);
        process.exit(1);
    }
    // Must include a 'src' and 'dst' option
    if (json.src === undefined) {
        console.error('Argument %d does not include a required "src" option', i);
        process.exit(1);
    } else if (json.dst === undefined) {
        console.error('Argument %d does not include a required "dst" option', i);
        process.exit(1);
    }
    // Other arguments are optional, so use
    jobs.push(json);
});

// Ensure we use a common browser directory across all requests
const baseDir = (process.env.DIR_TMP ?? '/tmp/debear/puppeteer');
const instanceRef = (process.env.INSTANCE_REF ?? process.pid);
const browserDataDir = `${baseDir}/screenshot-${instanceRef}`;
// This next line is semgrep noise - no amount of sanitising the input will satisfy it
if (!fs.existsSync(browserDataDir)) { // nosemgrep detect-non-literal-fs-filename
    try {
        // This next line is semgrep noise - no amount of sanitising the input will satisfy it
        fs.mkdirSync(browserDataDir, { recursive: true }); // nosemgrep detect-non-literal-fs-filename
    } catch (err) {
        console.error('Unable to create browser data dir: %s', err.message);
        process.exit(1);
    }
}

// Our main processing code
(async () => {
    console.log(`# Puppeteer starting: ${new Date()}`);

    // Set up our browser objects
    const browser = await puppeteer.launch({
        headless: true,
        executablePath: '/opt/google/chrome/chrome',
        userDataDir: browserDataDir
    });
    const page = await browser.newPage();

    // Loop through our jobs
    for (let i = 0; i < jobs.length; i++) {
        console.log(`Job ${i}: Saving '${jobs[i].src}' to '${jobs[i].dst}'` + (jobs[i].descrip !== undefined ? ` ("${jobs[i].descrip}")` : ''));

        // Determine appropriate viewport arguments
        let fullPage = ((jobs[i].width === undefined) || (jobs[i].height === undefined));
        let imgWidth = (jobs[i].width !== undefined ? parseInt(jobs[i].width, 10) : 1024);
        let imgHeight = (jobs[i].height !== undefined ? parseInt(jobs[i].height, 10) : 768);
        console.log(`- Setting viewport to ${imgWidth}px x ${imgHeight}px (with fullPage == ${fullPage})`);
        await page.setViewport({ width: imgWidth, height: imgHeight });

        // Load the page
        await page.goto(jobs[i].src, { waitUntil: 'networkidle0', timeout: requestTimeout });

        // Logging in?
        if (jobs[i].login !== undefined) {
            let creds = jobs[i].login.split(':');
            console.log(`- Logging in as user '${creds[0]}'`);
            // Now interact with the page to perform the action
            try {
                await page.click('#header_login_link');
                await page.type('header input[name="username"]', creds[0]);
                await page.type('header input[name="password"]', creds[1]);
                await page.click('header button[type="submit"]');

                await page.waitForResponse(response => response.status() === 200);
                await page.reload();
            } catch(err) {
                console.error('  - Unable to complete job %d login step: no 200 response received within %dms', i, requestTimeout);
                console.log(`  - Error logging in: ${err}`);
                process.exit(1);
            }
        }

        // Proceed to rendering the screenshot
        try {
            console.log('- Page loaded, taking screenshot');
            await page.screenshot({
                path: jobs[i].dst,
                fullPage: fullPage
            });
            // Log the image info (the next line is semgrep noise - no amount of sanitising the input will satisfy it)
            let stat = fs.statSync(jobs[i].dst); // nosemgrep detect-non-literal-fs-filename
            console.log(`- Resulting ${jobs[i].dst}:`);
            console.log(`  - Created:  ${stat.ctime}`);
            console.log(`  - Modified: ${stat.mtime}`);
            console.log(`  - Size:     ${stat.size} byte(s)`);

        } catch (err) {
            // A generic error occurred
            console.error('- An error occurred processing job %i: %s', i, err.message);
            process.exit(1);
        }

        // Should we now log out?
        if ((jobs[i].logout !== undefined) && jobs[i].logout) {
            console.log(`- Logging user out`);
            // Now interact with the page to perform the action
            try {
                await page.click('#header_logout_link');

                await page.waitForResponse(response => response.status() === 200);
                await page.reload();
            } catch (err) {
                console.error('  - Unable to complete job %d logout step: no 200 response received within %dms', i, requestTimeout);
                console.log(`  - Error logging out: ${err}`);
                process.exit(1);
            }
        }
    };

    // Tidy up upon completion
    console.log(`${jobs.length} image%s generated successfully`, jobs.length == 1 ? '' : 's');
    await page.close();
    await browser.close();

    console.log(`# Puppeteer ended: ${new Date()}`);
})();
