SET @sport := 'f1';
SET @season := 2015;

#
# Remove any previous setup
#
DELETE FROM `FANTASY_COMMON_ANNOUNCEMENTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_ARTICLES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_SECTIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_SETUP_TWITTER` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES_RANK` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES_USERS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_ENTRIES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_INVITES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_MESSAGES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_STANDINGS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_CATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_COSTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_GROUPS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_GROUPS_COSTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_GROUPS_SEL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_STATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_STATUSES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_SUMMARY` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SETUP_STATUS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SETUP_VALUATIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_STATS` WHERE `sport` = @sport;
DELETE FROM `FANTASY_MOTORS_STATS_SEASON` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_STATS_SECTIONS` WHERE `sport` = @sport;
DELETE FROM `FANTASY_MOTORS_TEAMS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_TEAMS_INVALID` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_TEAMS_SEL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_TEAMS_STANDINGS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_USER_PREFS` WHERE `sport` = @sport AND `season` = @season;

#
# Selections
#

# A Driver
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 1,  2, 'regular', 1); # HAM
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 3,  4, 'regular', 1); # MAS
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 4,  5, 'regular', 1); # RAI
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 22, 1, 'regular', 1); # BUT
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 31, 3, 'regular', 1); # HUL
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 59, 7, 'regular', 1); # STE
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 62, 6, 'regular', 1); # SAI

# B Driver
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 16, 6, 'regular', 1); # ROS
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 46, 5, 'regular', 1); # RIC
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 55, 2, 'regular', 1); # KVY
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 26, 1, 'regular', 1); # GRO
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 43, 4, 'regular', 1); # PER
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 60, 3, 'regular', 1); # NAS
-- INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, , , 'temporary', 1); # MER

# C Driver
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 50, 2, 'regular', 1); # BOT
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 15, 6, 'regular', 1); # VET
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 7,  1, 'regular', 1); # ALO
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 41, 4, 'regular', 1); # MAL
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 61, 5, 'regular', 1); # VER
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 56, 3, 'regular', 1); # ERI

# Teams
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 1,  4,  'regular', 1); # McLaren
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 2,  1,  'regular', 1); # Ferrari
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 3,  8,  'regular', 1); # Sauber
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 4,  3,  'regular', 1); # Lotus
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 6,  9,  'regular', 1); # Toro Rosso
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 7,  7,  'regular', 1); # Red Bull
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 8,  10, 'regular', 1); # Williams
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 9,  2,  'regular', 1); # Force India
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 10, 6,  'regular', 1); # Mercedes
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 4, 17, 5,  'regular', 1); # Manor

#
# Valuations
#
DROP TEMPORARY TABLE IF EXISTS `tmp_ROUNDS`;
CREATE TEMPORARY TABLE `tmp_ROUNDS` (`round` INT(11) UNSIGNED NOT NULL, PRIMARY KEY (`round`));
INSERT INTO `tmp_ROUNDS` (`round`) VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12), (13), (14), (15), (16), (17), (18), (19), (20);

DROP TEMPORARY TABLE IF EXISTS `tmp_VALUATIONS`;
CREATE TEMPORARY TABLE `tmp_VALUATIONS` (`cat_id` TINYINT(3) UNSIGNED NOT NULL, `link_id` INT(11) UNSIGNED NOT NULL, `cost` DOUBLE NOT NULL, PRIMARY KEY (`cat_id`, `link_id`));

# Front Runner: 2.5 - 3.2
# Mid Field:    1.9 - 2.4
# Back Marker:  0.8 - 1.5

# A Driver
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 1,  3.2); # HAM
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 3,  2.7); # MAS
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 4,  2.3); # RAI
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 22, 1.8); # BUT
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 31, 1.2); # HUL
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 62, 0.9); # SAI
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 59, 0.5); # STE

# B Driver
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 16, 3.0); # ROS
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 46, 2.6); # RIC
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 55, 2.2); # KVY
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 26, 1.6); # GRO
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 43, 1.1); # PER
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 60, 0.7); # NAS
-- INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, , 0.5); # MER

# C Driver
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 50, 2.8); # BOT
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 15, 2.5); # VET
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 7,  1.9); # ALO
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 41, 1.4); # MAL
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 61, 0.9); # VER
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 56, 0.7); # ERI

# Teams: 4.1 - 5.5
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 10, 5.5); # Mercedes
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 8,  5.3); # Williams
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 2,  5.1); # Ferrari
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 7,  5.0); # Red Bull
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 1,  4.8); # McLaren
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 4,  4.7); # Lotus
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 6,  4.5); # Toro Rosso
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 9,  4.4); # Force India
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 3,  4.3); # Sauber
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (4, 17, 4.1); # Manor

INSERT INTO `FANTASY_MOTORS_SELECTIONS_COSTS` (`sport`, `season`, `cat_id`, `link_id`, `round`, `cost`, `totr`)
  SELECT @sport, @season, `tmp_VALUATIONS`.`cat_id`, `tmp_VALUATIONS`.`link_id`, `tmp_ROUNDS`.`round`, `tmp_VALUATIONS`.`cost`, 0
  FROM `tmp_VALUATIONS`
  JOIN `tmp_ROUNDS` ON (1 = 1);

#
# Selection categories
#
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 1, 'A Driver', 'driver', 1, 1);
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 2, 'B Driver', 'driver', 2, 1);
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 3, 'C Driver', 'driver', 3, 1);
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 4, 'Team', 'team', 4, 1);

#
# Cost rounds
#
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 1, 1, 4);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 2, 5, 8);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 3, 9, 12);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 4, 13, 16);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 5, 17, 20);

#
# Fan Leagues
#
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 1, 'country', 'Andorra', 'ad', 0, 50);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 2, 'country', 'United Arab Emirates', 'ae', 0, 41);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 3, 'country', 'Afghanistan', 'af', 0, 46);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 4, 'country', 'Antigua and Barbuda', 'ag', 0, 53);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 5, 'country', 'Anguilla', 'ai', 0, 52);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 6, 'country', 'Albania', 'al', 0, 47);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 7, 'country', 'Armenia', 'am', 0, 54);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 8, 'country', 'Netherlands Antilles', 'an', 0, 171);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 9, 'country', 'Angola', 'ao', 0, 51);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 10, 'country', 'Argentina', 'ar', 0, 1);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 11, 'country', 'American Samoa', 'as', 0, 49);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 12, 'country', 'Austria', 'at', 0, 3);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 13, 'country', 'Australia', 'au', 0, 2);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 14, 'country', 'Aruba', 'aw', 0, 55);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 15, 'country', '&Aring;land Islands', 'ax', 0, 45);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 16, 'country', 'Azerbaijan', 'az', 0, 56);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 17, 'country', 'Bosnia and Herzegovina', 'ba', 0, 66);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 18, 'country', 'Barbados', 'bb', 0, 59);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 19, 'country', 'Bangladesh', 'bd', 0, 58);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 20, 'country', 'Belgium', 'be', 0, 5);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 21, 'country', 'Burkina Faso', 'bf', 0, 73);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 22, 'country', 'Bulgaria', 'bg', 0, 72);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 23, 'country', 'Bahrain', 'bh', 0, 4);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 24, 'country', 'Burundi', 'bi', 0, 74);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 25, 'country', 'Benin', 'bj', 0, 62);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 26, 'country', 'Bermuda', 'bm', 0, 63);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 27, 'country', 'Brunei Darussalam', 'bn', 0, 71);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 28, 'country', 'Bolivia', 'bo', 0, 65);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 29, 'country', 'Brazil', 'br', 0, 6);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 30, 'country', 'Bahamas', 'bs', 0, 57);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 31, 'country', 'Bhutan', 'bt', 0, 64);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 32, 'country', 'Bouvet Island', 'bv', 0, 68);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 33, 'country', 'Botswana', 'bw', 0, 67);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 34, 'country', 'Belarus', 'by', 0, 60);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 35, 'country', 'Belize', 'bz', 0, 61);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 36, 'country', 'Canada', 'ca', 0, 7);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 37, 'country', 'Cocos Islands', 'cc', 0, 84);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 38, 'country', 'Democratic Republic of Congo', 'cd', 0, 94);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 39, 'country', 'Central African Republic', 'cf', 0, 80);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 40, 'country', 'Congo', 'cg', 0, 87);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 41, 'country', 'Switzerland', 'ch', 0, 39);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 42, 'country', 'C&ocirc;te d&#39;Ivoire', 'ci', 0, 75);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 43, 'country', 'Cook Islands', 'ck', 0, 88);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 44, 'country', 'Chile', 'cl', 0, 82);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 45, 'country', 'Cameroon', 'cm', 0, 77);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 46, 'country', 'China', 'cn', 0, 8);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 47, 'country', 'Colombia', 'co', 0, 85);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 48, 'country', 'Costa Rica', 'cr', 0, 89);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 49, 'country', 'Cuba', 'cu', 0, 91);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 50, 'country', 'Cape Verde', 'cv', 0, 78);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 51, 'country', 'Christmas Island', 'cx', 0, 83);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 52, 'country', 'Wales', 'cym', 0, 44);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 53, 'country', 'Cyprus', 'cy', 0, 92);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 54, 'country', 'Czech Republic', 'cz', 0, 9);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 55, 'country', 'Germany', 'de', 0, 14);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 56, 'country', 'Djibouti', 'dj', 0, 95);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 57, 'country', 'Denmark', 'dk', 0, 10);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 58, 'country', 'Dominica', 'dm', 0, 96);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 59, 'country', 'Dominican Republic', 'do', 0, 97);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 60, 'country', 'Algeria', 'dz', 0, 48);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 61, 'country', 'Ecuador', 'ec', 0, 98);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 62, 'country', 'Estonia', 'ee', 0, 103);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 63, 'country', 'Egypt', 'eg', 0, 99);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 64, 'country', 'Western Sahara', 'eh', 0, 241);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 65, 'country', 'England', 'eng', 0, 11);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 66, 'country', 'Eritrea', 'er', 0, 102);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 67, 'country', 'Spain', 'es', 0, 37);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 68, 'country', 'Ethiopia', 'et', 0, 104);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 69, 'country', 'Finland', 'fi', 0, 12);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 70, 'country', 'Fiji', 'fj', 0, 108);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 71, 'country', 'Falkland Islands', 'fk', 0, 105);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 72, 'country', 'Federated States of Micronesia', 'fm', 0, 107);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 73, 'country', 'Faroe Islands', 'fo', 0, 106);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 74, 'country', 'France', 'fr', 0, 13);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 75, 'country', 'Gabon', 'ga', 0, 113);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 76, 'country', 'United Kingdom', 'gb', 1, 234);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 77, 'country', 'Grenada', 'gd', 0, 120);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 78, 'country', 'Georgia', 'ge', 0, 115);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 79, 'country', 'French Guiana', 'gf', 0, 110);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 80, 'country', 'Ghana', 'gh', 0, 116);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 81, 'country', 'Gibraltar', 'gi', 0, 117);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 82, 'country', 'Greenland', 'gl', 0, 119);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 83, 'country', 'Gambia', 'gm', 0, 114);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 84, 'country', 'Guinea', 'gn', 0, 124);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 85, 'country', 'Guadeloupe', 'gp', 0, 121);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 86, 'country', 'Equatorial Guinea', 'gq', 0, 101);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 87, 'country', 'Greece', 'gr', 0, 118);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 88, 'country', 'South Georgia and the South Sandwich Islands', 'gs', 0, 211);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 89, 'country', 'Guatemala', 'gt', 0, 123);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 90, 'country', 'Guam', 'gu', 0, 122);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 91, 'country', 'Guinea-Bissau', 'gw', 0, 125);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 92, 'country', 'Guyana', 'gy', 0, 126);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 93, 'country', 'Hong Kong', 'hk', 0, 131);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 94, 'country', 'Heard Island and McDonald Islands', 'hm', 0, 128);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 95, 'country', 'Honduras', 'hn', 0, 130);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 96, 'country', 'Croatia', 'hr', 0, 90);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 97, 'country', 'Haiti', 'ht', 0, 127);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 98, 'country', 'Hungary', 'hu', 0, 15);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 99, 'country', 'Indonesia', 'id', 0, 132);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 100, 'country', 'Ireland', 'ie', 0, 18);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 101, 'country', 'Northern Ireland', 'nir', 0, 27);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 102, 'country', 'Israel', 'il', 0, 135);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 103, 'country', 'India', 'in', 0, 17);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 104, 'country', 'British Indian Ocean Territory', 'io', 0, 69);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 105, 'country', 'Iraq', 'iq', 0, 133);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 106, 'country', 'Islamic Republic of Iran', 'ir', 0, 134);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 107, 'country', 'Iceland', 'is', 0, 16);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 108, 'country', 'Italy', 'it', 0, 19);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 109, 'country', 'Jamaica', 'jm', 0, 136);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 110, 'country', 'Jordan', 'jo', 0, 137);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 111, 'country', 'Japan', 'jp', 0, 20);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 112, 'country', 'Kenya', 'ke', 0, 139);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 113, 'country', 'Kyrgyzstan', 'kg', 0, 142);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 114, 'country', 'Cambodia', 'kh', 0, 76);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 115, 'country', 'Kiribati', 'ki', 0, 140);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 116, 'country', 'Comoros', 'km', 0, 86);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 117, 'country', 'Saint Kitts and Nevis', 'kn', 0, 196);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 118, 'country', 'Democratic People&#39;s Republic of Korea', 'kp', 0, 93);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 119, 'country', 'Republic of Korea', 'kr', 0, 31);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 120, 'country', 'Kuwait', 'kw', 0, 141);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 121, 'country', 'Cayman Islands', 'ky', 0, 79);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 122, 'country', 'Kazakhstan', 'kz', 0, 138);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 123, 'country', 'Lao People&#39;s Democratic Republic', 'la', 0, 143);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 124, 'country', 'Lebanon', 'lb', 0, 145);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 125, 'country', 'Saint Lucia', 'lc', 0, 197);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 126, 'country', 'Liechtenstein', 'li', 0, 149);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 127, 'country', 'Sri Lanka', 'lk', 0, 212);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 128, 'country', 'Liberia', 'lr', 0, 147);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 129, 'country', 'Lesotho', 'ls', 0, 146);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 130, 'country', 'Lithuania', 'lt', 0, 150);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 131, 'country', 'Luxembourg', 'lu', 0, 21);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 132, 'country', 'Latvia', 'lv', 0, 144);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 133, 'country', 'Libyan Arab Jamahiriya', 'ly', 0, 148);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 134, 'country', 'Morocco', 'ma', 0, 165);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 135, 'country', 'Monaco', 'mc', 0, 24);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 136, 'country', 'Republic of Moldova', 'md', 0, 192);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 137, 'country', 'Montenegro', 'me', 0, 163);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 138, 'country', 'Madagascar', 'mg', 0, 152);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 139, 'country', 'Marshall Islands', 'mh', 0, 157);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 140, 'country', 'Former Yugoslav Republic of Macedonia', 'mk', 0, 109);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 141, 'country', 'Mali', 'ml', 0, 155);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 142, 'country', 'Myanmar', 'mm', 0, 167);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 143, 'country', 'Mongolia', 'mn', 0, 162);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 144, 'country', 'Macao', 'mo', 0, 151);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 145, 'country', 'Northern Mariana Islands', 'mp', 0, 178);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 146, 'country', 'Martinique', 'mq', 0, 158);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 147, 'country', 'Mauritania', 'mr', 0, 159);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 148, 'country', 'Montserrat', 'ms', 0, 164);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 149, 'country', 'Malta', 'mt', 0, 156);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 150, 'country', 'Mauritius', 'mu', 0, 160);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 151, 'country', 'Maldives', 'mv', 0, 154);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 152, 'country', 'Malawi', 'mw', 0, 153);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 153, 'country', 'Mexico', 'mx', 0, 23);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 154, 'country', 'Malaysia', 'my', 0, 22);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 155, 'country', 'Mozambique', 'mz', 0, 166);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 156, 'country', 'Namibia', 'na', 0, 168);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 157, 'country', 'New Caledonia', 'nc', 0, 172);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 158, 'country', 'Niger', 'ne', 0, 174);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 159, 'country', 'Norfolk Island', 'nf', 0, 177);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 160, 'country', 'Nigeria', 'ng', 0, 175);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 161, 'country', 'Nicaragua', 'ni', 0, 173);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 162, 'country', 'Netherlands', 'nl', 0, 25);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 163, 'country', 'Norway', 'no', 0, 28);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 164, 'country', 'Nepal', 'np', 0, 170);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 165, 'country', 'Nauru', 'nr', 0, 169);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 166, 'country', 'Niue', 'nu', 0, 176);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 167, 'country', 'New Zealand', 'nz', 0, 26);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 168, 'country', 'Oman', 'om', 0, 179);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 169, 'country', 'Panama', 'pa', 0, 183);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 170, 'country', 'Peru', 'pe', 0, 186);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 171, 'country', 'French Polynesia', 'pf', 0, 111);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 172, 'country', 'Papua New Guinea', 'pg', 0, 184);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 173, 'country', 'Philippines', 'ph', 0, 187);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 174, 'country', 'Pakistan', 'pk', 0, 180);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 175, 'country', 'Poland', 'pl', 0, 29);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 176, 'country', 'Saint Pierre and Miquelon', 'pm', 0, 198);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 177, 'country', 'Pitcairn', 'pn', 0, 188);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 178, 'country', 'Puerto Rico', 'pr', 0, 189);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 179, 'country', 'Palestine', 'ps', 0, 182);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 180, 'country', 'Portugal', 'pt', 0, 30);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 181, 'country', 'Palau', 'pw', 0, 181);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 182, 'country', 'Paraguay', 'py', 0, 185);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 183, 'country', 'Qatar', 'qa', 0, 190);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 184, 'country', 'R&eacute;union', 're', 0, 191);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 185, 'country', 'Romania', 'ro', 0, 193);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 186, 'country', 'Serbia', 'rs', 0, 204);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 187, 'country', 'Russian Federation', 'ru', 0, 32);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 188, 'country', 'Rwanda', 'rw', 0, 194);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 189, 'country', 'Saudi Arabia', 'sa', 0, 202);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 190, 'country', 'Solomon Islands', 'sb', 0, 209);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 191, 'country', 'Scotland', 'sco', 0, 34);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 192, 'country', 'Seychelles', 'sc', 0, 205);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 193, 'country', 'Sudan', 'sd', 0, 213);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 194, 'country', 'Sweden', 'se', 0, 38);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 195, 'country', 'Singapore', 'sg', 0, 35);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 196, 'country', 'Saint Helena', 'sh', 0, 195);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 197, 'country', 'Slovenia', 'si', 0, 208);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 198, 'country', 'Svalbard and Jan Mayen', 'sj', 0, 215);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 199, 'country', 'Slovakia', 'sk', 0, 207);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 200, 'country', 'Sierra Leone', 'sl', 0, 206);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 201, 'country', 'San Marino', 'sm', 0, 33);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 202, 'country', 'Senegal', 'sn', 0, 203);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 203, 'country', 'Somalia', 'so', 0, 210);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 204, 'country', 'Suriname', 'sr', 0, 214);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 205, 'country', 'Sao Tome and Principe', 'st', 0, 201);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 206, 'country', 'El Salvador', 'sv', 0, 100);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 207, 'country', 'Syrian Arab Republic', 'sy', 0, 217);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 208, 'country', 'Swaziland', 'sz', 0, 216);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 209, 'country', 'Turks and Caicos Islands', 'tc', 0, 229);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 210, 'country', 'Chad', 'td', 0, 81);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 211, 'country', 'French Southern Territories', 'tf', 0, 112);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 212, 'country', 'Togo', 'tg', 0, 223);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 213, 'country', 'Thailand', 'th', 0, 221);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 214, 'country', 'Tajikistan', 'tj', 0, 219);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 215, 'country', 'Tokelau', 'tk', 0, 224);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 216, 'country', 'Timor-Leste', 'tl', 0, 222);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 217, 'country', 'Turkmenistan', 'tm', 0, 228);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 218, 'country', 'Tunisia', 'tn', 0, 227);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 219, 'country', 'Tonga', 'to', 0, 225);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 220, 'country', 'Turkey', 'tr', 0, 40);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 221, 'country', 'Trinidad and Tobago', 'tt', 0, 226);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 222, 'country', 'Tuvalu', 'tv', 0, 230);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 223, 'country', 'Taiwan', 'tw', 0, 218);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 224, 'country', 'Tanzania', 'tz', 0, 220);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 225, 'country', 'Ukraine', 'ua', 0, 233);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 226, 'country', 'Uganda', 'ug', 0, 232);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 227, 'country', 'United States Minor Outlying Islands', 'um', 0, 235);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 228, 'country', 'United States', 'us', 0, 42);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 229, 'country', 'Uruguay', 'uy', 0, 236);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 230, 'country', 'Uzbekistan', 'uz', 0, 237);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 231, 'country', 'Holy See', 'va', 0, 129);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 232, 'country', 'Saint Vincent and the Grenadines', 'vc', 0, 199);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 233, 'country', 'Venezuela', 've', 0, 43);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 234, 'country', 'British Virgin Islands', 'vg', 0, 70);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 235, 'country', 'U.S. Virgin Islands', 'vi', 0, 231);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 236, 'country', 'Viet Nam', 'vn', 0, 239);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 237, 'country', 'Vanuatu', 'vu', 0, 238);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 238, 'country', 'Wallis and Futuna', 'wf', 0, 240);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 239, 'country', 'Samoa', 'ws', 0, 200);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 240, 'country', 'Yemen', 'ye', 0, 242);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 241, 'country', 'Mayotte', 'yt', 0, 161);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 242, 'country', 'South Africa', 'za', 0, 36);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 243, 'country', 'Zambia', 'zm', 0, 243);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 244, 'country', 'Zimbabwe', 'zw', 0, 244);

INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 52);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 65);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 101);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 191);

# Now the teams and drivers
ALTER TABLE `FANTASY_MOTORS_FANLEAGUES` CHANGE `league_id` `league_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;

INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`)
  SELECT @sport, `SPORTS_F1_RACE_DRIVERS`.`season`, NULL, 'driver', CONCAT(`SPORTS_F1_DRIVERS`.`first_name`, ' ', `SPORTS_F1_DRIVERS`.`surname`), `SPORTS_F1_DRIVERS`.`driver_id`, 0, COUNT(DISTINCT `OTHERS`.`driver_id`) + 1
  FROM `debearco_sports`.`SPORTS_F1_RACE_DRIVERS`
  JOIN `debearco_sports`.`SPORTS_F1_DRIVERS`
    ON (`SPORTS_F1_DRIVERS`.`driver_id` = `SPORTS_F1_RACE_DRIVERS`.`driver_id`)
  LEFT JOIN `debearco_sports`.`SPORTS_F1_RACE_DRIVERS` AS `OTHER_DRIVERS`
    ON (`OTHER_DRIVERS`.`season` = `SPORTS_F1_RACE_DRIVERS`.`season`
    AND `OTHER_DRIVERS`.`driver_id` <> `SPORTS_F1_RACE_DRIVERS`.`driver_id`)
  LEFT JOIN `debearco_sports`.`SPORTS_F1_DRIVERS` AS `OTHERS`
    ON (`OTHERS`.`driver_id` = `OTHER_DRIVERS`.`driver_id`
    AND (`OTHERS`.`surname` < `SPORTS_F1_DRIVERS`.`surname`
      OR (`OTHERS`.`surname` = `SPORTS_F1_DRIVERS`.`surname` AND `OTHERS`.`first_name` < `SPORTS_F1_DRIVERS`.`first_name`)))
  WHERE `SPORTS_F1_RACE_DRIVERS`.`season` = @season
  GROUP BY `SPORTS_F1_DRIVERS`.`driver_id`;

INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`)
  SELECT @sport, `SPORTS_F1_TEAMS`.`season`, NULL, 'team', `SPORTS_F1_TEAMS`.`team_name`, `SPORTS_F1_TEAMS`.`team_id`, 0, COUNT(DISTINCT `OTHERS`.`team_id`) + 1
  FROM `debearco_sports`.`SPORTS_F1_TEAMS`
  LEFT JOIN `debearco_sports`.`SPORTS_F1_TEAMS` AS `OTHERS`
    ON (`OTHERS`.`season` = `SPORTS_F1_TEAMS`.`season`
    AND `OTHERS`.`team_id` <> `SPORTS_F1_TEAMS`.`team_id`
    AND `OTHERS`.`team_name` < `SPORTS_F1_TEAMS`.`team_name`)
  WHERE `SPORTS_F1_TEAMS`.`season` = @season
  GROUP BY `SPORTS_F1_TEAMS`.`team_id`;

ALTER TABLE `FANTASY_MOTORS_FANLEAGUES` CHANGE `league_id` `league_id` INT(11) UNSIGNED NOT NULL;

#
# Help articles
#
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 1, 'Rules', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 2, 'Registration', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 3, 'Team Management', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 4, 'Groups', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 5, 'What&#39;s Changed in 2015?', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 6, 'Contact Us', 6, 1);

INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 1, 'What is the aim of DeBear Fantasy Formula One?', '<P>DeBear Fantasy Formula One pits your knowledge of Formula One against other racing fans. Every Grand Prix, select three drivers and a team from a pre-defined list within an overall budget and score points based on their performance in real life races. The better your predictions, the higher you will be ranked on the overall leaderboard. Can you attain the Number One slot?!</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 2, 'Why do I have a team budget?', '<P>A team budget has been put in place to prevent you from selecting the best teams and drivers in every race. Rather than limit the number of times a particular team or driver can be selected, we decided to add another element of strategy to the game: managing a budget. Each selection has an associated cost &ndash; measured in our fictional units, &sect; &ndash; according to their expected level of performance. As long as the total cost of the four selections does not exceed the maximum budget in a round &ndash; 10.0 &sect; &ndash; the team will score you points.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 3, 'How do my selections score points for my team?', '<P>Points are awarded based on performance in both Qualifying and during the Race itself. Scores for teams are the combination of both their drivers, with the exception of the &quot;Top Driver&quot; points for qualifying and the race, which apply only to the drivers.</P>\r\n<H5>Qualifying</H5>\r\n<DL>\r\n  <DT>Qualifying</DT>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are awarded to any driver who qualifies for the race &ndash; a driver fails to qualify if their best time in Q1 is not within 107% of the fastest time set in Q1.</DD>\r\n    <DD CLASS="info">If a driver fails to set a time within the &quot;107% Rule&quot; (which includies not posting a Q1 time at all) but is subsequently allowed to race by the race stewards, <SPAN CLASS="field">ZERO</SPAN> points will be awarded for EVERY qualification category. However, with the exception of &quot;Place Gains&quot;, which relies on a qualifying position, any fantasy points scored by the driver in the race itself WILL still be awarded.</DD>\r\n  <DT>Outqualifying Teammate</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> are awarded to the driver in each team who qualifies highest for the race <SPAN CLASS="info field">before</SPAN> any grid penalties are applied.</DD>\r\n  <DT>Topping Q1 or Q2</DT>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> is awarded to the driver who tops Q1 and another <SPAN CLASS="field">1 point</SPAN> is awarded to the driver who tops Q2.</DD>\r\n  <DT>Pole Bonus</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> will be awarded to the driver who qualified in Pole Position.</DD>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> will be awarded to the driver who qualified second.</DD>\r\n  <DD CLASS="info">All points are awarded <SPAN CLASS="field">before</SPAN> any grid penalties are applied.</DD>\r\n</DL>\r\n<H5>Race Day</H5>\r\n<DL>\r\n  <DT>Finish Bonus</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> will be awarded to any driver who is deemed a <SPAN CLASS="info">classified finisher</SPAN>, reaching 90% of the completed race distance.</DD>\r\n  <DT>Outperform Teammate</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> will be awarded to the driver in each team who finishes highest in the race.</DD>\r\n    <DD CLASS="info">A driver MUST be classified for these points to be awarded.</DD>\r\n  <DT>Place Gain</DT>\r\n    <DD><SPAN CLASS="field">1 point per position gained</SPAN> will be awarded to any driver who qualifies outside the Top 10 (before grid penalties) and finishes in a higher position than they qualified.</DD>\r\n    <DD CLASS="info">No points will be deducted if a driver finishes lower than his qualifying position.</DD>\r\n    <DD CLASS="info">A driver MUST be classified for these points to be awarded.</DD>\r\n  <DT>Laps Led</DT>\r\n    <DD>Points are tiered and will be awarded based on the total number of laps led during the race:</DD>\r\n    <DD CLASS="bullet"><SPAN CLASS="field">2 points</SPAN> will be awarded to every driver leading between one (1) and three (3) laps;</SPAN></DD>\r\n    <DD CLASS="bullet"><SPAN CLASS="field">3 points</SPAN> will be awarded to every driver leading between four (4) and ten (10) laps;</SPAN></DD>\r\n    <DD CLASS="bullet"><SPAN CLASS="field">4 points</SPAN> will be awarded to every driver leading more than ten (10) laps but less than half the total number of laps;</SPAN></DD>\r\n    <DD CLASS="bullet"><SPAN CLASS="field">6 points</SPAN> will be awarded to a driver leading at least half the total number of laps but not every lap;</SPAN></DD>\r\n    <DD CLASS="bullet"><SPAN CLASS="field">10 points</SPAN> will be awarded to a driver leading every lap in the race.</SPAN></DD>\r\n    <DD><SPAN CLASS="field info">Please Note:</SPAN> The definition of a &quot;lap led&quot; is the first car to cross the start/finish line each lap, <SPAN CLASS="info">excluding</SPAN> the start of the very first lap, which is simply the grid order.</DD>\r\n  <DT>Fastest Lap</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> will be awarded to the driver who completes the fastest lap during the race.</DD>\r\n  <DT>Championship Points</DT>\r\n    <DD>All World Championship points scored by a driver will be awarded to your team.</DD>\r\n  <DD CLASS="info">The one exception to this will be the final race of the season, the Abu Dhabi Grand Prix, where &quot;double points&quot; will be awarded to real-life teams and drivers (50pts for the Winner, 36pts for second, 30pts for third, down to 2pts for tenth). As we deem this a silly rule that over-values a single arbitrary race, we will only award the equivalent &quot;single points&quot; (25pts for the Winner, etc).</DD>\r\n</DL>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 4, 'How are scoring ties broken?', '<P>When two or more Fantasy Formula One teams have scored the same number of points, ties in the standings are broken based on total budget spent over the course of the season &ndash; the teams that have cost the least to build are given the higher rank in the standings.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 5, 'What happens if I do not fully use my allocated budget?', '<P>Any unallocated budget at the end of each round is <SPAN CLASS="field">NOT</SPAN> carried over to future rounds &ndash; you will not be allowed to use it in future races, nor receive any bonus points towards your team score, so the only potential (and risky) benefit from not using your full budget is in tie-breakers.</P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 6, 'What do I do I think you''ve made a scoring error?', '<P>Scoring disputes should be made by email to <A HREF="mailto:support@debear.uk">support@debear.uk</A>. Please include as much information as possible, in particular the stat(s) you believe to be incorrect and your source to indicate the corrected value! We will review the information you provide and update the scoring accordingly.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 1, 'How do I create a team?', '<P>To create a team click the &quot;Create New Team&quot; links available either on the DeBear Fantasy Formula One homepage or the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page and follow the instructions.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 2, 'How many teams can I create?', '<P>You can create a maximum of three (3) Fantasy Formula One teams with the same DeBear.uk user account.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 3, 'When can I create a team?', '<P>Teams can be created from Saturday 1st March until Saturday 24th May, in advance of qualifying for the Monaco Grand Prix. After this date no new teams can be created, even if you have not reached your team limit.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 4, 'What information do you need?', '<P>All we need from you when you create your Fantasy Formula One team is a team name, and the country you wish to represent &ndash; each team will be displayed throughout the game with an associated flag, just like teams and drivers have their nationality displayed, as well as being automatically entered into a &quot;Fan League&quot; so you can compare yourself with fans from your country. Other information, such as your favourite team and driver is purely optional so you can rate yourself against other fans.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 1, 'What does the &quot;INJ&quot;, &quot;NE&quot;, &quot;OUT&quot;, &quot;SUB&quot; or &quot;SUSP&quot; next to a driver mean?', '<P>If it is expected &ndash; or known &ndash; that a driver will not compete in a given Grand Prix, it will be indicated by an appropriate status update next to the driver&#39;s name on the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page.</P>\r\n<UL>\r\n  <LI><SPAN CLASS="field">NE</SPAN> indicates a driver who has not been entered by a team and so will not participate in the race.</LI>\r\n  <LI><SPAN CLASS="field">INJ</SPAN> indicates a driver who is injured and there is uncertainty whether they will participate in the race.</LI>\r\n  <LI><SPAN CLASS="field">OUT</SPAN> indicates a driver who is <SPAN CLASS="info">confirmed</SPAN> as not participating in the race.</LI>\r\n  <LI><SPAN CLASS="field">SUB</SPAN> indicates a driver who is only entered in the race as a temporary replacement for a regular driver.</LI>\r\n  <LI><SPAN CLASS="field">SUSP</SPAN> indicates a driver who has been suspended from the race by the FIA.</LI>\r\n</UL>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 2, 'When can I start editing my team?', '<P>Teams can be edited soon as they are created and there is no restriction on when you can start making selections, so if you wanted you could make your selections for the final round before the season has even started!</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 3, 'Why am I unable to edit my team?', '<P>Selections for a Grand Prix lock five (5) minutes prior to the start of the first qualifying session on the Saturday of a race weekend. Your selections at this point will be entered as your team for that round. The time this will take place is listed on the applicable pages, such as the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page when making your selections.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 4, 'What happens if I do not make any selections for a race?', '<P>Nothing! You will also not score any fantasy points for that round, but the budget for this round will <SPAN CLASS="field">NOT</SPAN> be added to your team for future rounds.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 5, 'Why has the cost of a team / driver changed since the last Grand Prix?', '<P>To ensure the cost of each team and driver accurately reflects their value in Fantasy Formula One, throughout the season we will monitor and update the valuations of each team and driver at four (4) designated points:\r\n<OL>\r\n  <LI>Between the Chinese and Spanish Grand Prix <SPAN CLASS="info">(Rounds 4 and 5)</SPAN>;</LI>\r\n  <LI>Between the Austrian and British Grand Prix <SPAN CLASS="info">(Rounds 8 and 9)</SPAN>;</LI>\r\n  <LI>Between the Belgian and Italian Grand Prix <SPAN CLASS="info">(Rounds 12 and 13)</SPAN>;</LI>\r\n  <LI>Between the Russian and United States Grand Prix <SPAN CLASS="info">(Rounds 16 and 17)</SPAN>.</LI>\r\n</OL></P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 7, 'When will these valuation changes actually be made?', '<P>All changes will be made and finalised no later than the Wednesday before the next race. Please make sure you monitor the <A HREF="valuations">Valuations</A> page to see when the cost of each team and driver will be reviewed and updated, and what the new costs will be.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 1, 'What are &quot;Groups&quot; and &quot;Fan Leagues&quot;?', '<P>Groups and Fan Leagues are a great way of comparing the progress of your team(s) against that of your friends and other like-minded Formula One fans. Instead of being ranked against every other player in the World, your team will be listed &ndash; and more importantly ranked! &ndash; against the other teams who have joined your Group or selected the same Fan League.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 2, 'How do I create a Group?', '<P>Groups are created from the &quot;Groups&quot; link at the top of each page. Select the &quot;Create New Group&quot; option, give your Group a name, select which of your existing teams you would like to join the group and click &quot;Create&quot;. That&#39;s it!</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 3, 'How do I invite my friends to a Group I have just created?', '<P>Friends can be invited to your group by clicking the &quot;Invite Friends&quot; link on the <SPAN CLASS="info">Group</SPAN> page and entering the email address of the friends you would like to join. They will receive an email inviting them to join your group, and if applicable, create a team first.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 4, 'How do I add my team to an existing Group?', '<P>There are two ways to join a group a friend has created:</P>\r\n<H5>1. Using the e-mail you were sent</H5>\r\n<P>Simply click the link in the e-mail, select the team(s) you would like entered and click &quot;Add Teams&quot;.</P>\r\n\r\n<H5>2. Using the Group PIN</H5>\r\n<P>Each Group has a unique PIN &ndash; having been provided this PIN by the group&#39;s creator, go to the <SPAN CLASS="info">Groups</SPAN> page and enter the PIN and the Group&#39;s password in the &quot;Join Group via PIN&quot; section.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 5, 'How do I join a Fan League?', '<P>You can select the Fan League(s) you wish to join when you first create a team. Once you have created your first team, you can change the Fan League(s) your team(s) belong to when either creating an additional team, or via the &quot;<SPAN CLASS="info">Update Preferences</SPAN>&quot; page.</P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 6, 'Why can I no longer change which Fan League(s) I belong to?', '<P>You can choose which Fan League(s) your team(s) belong to up until selections lock for the first race your team(s) compete in &ndash; after this point, you cannot change which Fan League(s) your team(s) belong to.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 7, 'Can I join multiple Fan Leagues?', '<P>No &ndash; the Fan League(s) you join apply to <SPAN CLASS="info">all</SPAN> your teams, it is not possible to enter one team to a Fan League and a second team to a different Fan League.</P>', 7, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 8, 'How do I create a new Fan League?', '<P>It is not possible to create a custom Fan League. The list of Fan Leagues for Countries, Drivers and Teams is pre-determined before the start of the season and new leagues will not be added throughout the course of the season.</P>', 8, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 5, 1, 'Why the changes?', '<P>After receiving all the feedback from last season''s Fantasy Formula One, we feel changes needed to be made to improve the quality of game play, add to the challenge over the course of the season and to close any loopholes that had been identified.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 5, 2, 'What do I need to know then?', '<P>The one change is in the driver groupings. Rather than being grouped by standard &ndash; All Front Runners together, all Mid Fielders together, etc &ndash; we have decided to mix the driver standards to allow for greater player choice. The drivers are now in arbitrary groups that we''ve called ''A'', ''B'' and ''C'' and each group contains a mix of drivers from the old-style groups: some Front Runners, some Mid Fielders and some Back Markers. The same restriction applies that a driver must be selected from each group, and once allocated a group a driver will never move between groups. This change does not affect the team selection as the teams will continue to be grouped in a single category.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 6, 1, 'I still have a problem or unanswered question, what do I do?', '<P>The best thing to do in this case is to email us on <A HREF="mailto:support@debear.uk">support@debear.uk</A>. We will get back to you as soon as we can, however if you are having problems it would really help us if you could provide as much information as possible &ndash; things like what you were trying to do and any error messages that appeared will help us better understand your problem and how we can resolve it. Please don&#39;t worry if you feel unable to provide such information though, your problem will not go unresolved if you do not provide it!</P>', 1, 1);

#
# Stats
#
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 1, 1, 'qualify', 'Qualify', 1, 0, 1);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 2, 1, 'top_q', 'Top Q1 / Q2', 2, 0, 3);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 3, 1, 'pole', 'Pole Bonus', 2, 0, 4);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 4, 2, 'finish', 'Finish Bonus', 2, 0, 1);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 5, 2, 'fastest_lap', 'Fastest Lap', 2, 0, 4);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 6, 2, 'place_gains', 'Place Gains', 2, 0, 5);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 7, 2, 'champ_pts', 'Champ Points', 2, 0, 6);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 8, 1, 'beat_teammate', 'Beat Teammate', 2, 1, 2);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 9, 2, 'beat_teammate', 'Beat Teammate', 2, 1, 2);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES (@sport, 10, 2, 'laps_led', 'Laps Led', 1, 0, 3);

INSERT INTO `FANTASY_MOTORS_STATS_SECTIONS` (`sport`, `sect_id`, `name`, `disp_order`) VALUES (@sport, 1, 'Qualifying', 1);
INSERT INTO `FANTASY_MOTORS_STATS_SECTIONS` (`sport`, `sect_id`, `name`, `disp_order`) VALUES (@sport, 2, 'Race Day', 2);

INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 1);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 2);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 3);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 4);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 5);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 6);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 7);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 8);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 9);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 10);

#
# Twitter stuff
#
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 1, 'Just {days} day{days_s} until the deadline for the {race} - is your team ready?! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 2, 'Just {days} day{days_s} until the deadline for the {race} - have you made your selections? #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 3, 'The {race} locks {days_rel} - are you ready!? #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 4, 'The {race} locks {days_rel} - have you made your selections? #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 5, 'Have you made your selections for the {race}? Selections lock {days_rel}! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 6, 'Is your team ready? There''s only {days} day{days_s} until the {race} gets underway! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 7, 'The {race} is this weekend - time to get your selections in! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 8, 'Selections due {days_rel} for the {race} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 9, 'Selections for the {race} due {days_rel} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 10, 'Don''t forget to make your selections for the {race} - the deadline is {days_rel}! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 1, 'The {race} results are in! How did your team fare? {standings_url} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 2, 'Race results have been uploaded after the {race}. {standings_url} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 3, 'Time to check your team(s) - the {race} results are in! {standings_url} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 4, 'We''ve updated the site after the {race} - who is leading the way now? {standings_url} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 5, 'Find out if your team leads the way in the {race} - The race results have been uploaded! {standings_url} #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 6, 'Time to check the results - the scores from the {race} are in! {standings_url} #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_3rd', 1, 'In 3rd place, and bottom step of the rostrum, scoring {team_score} is {team_name} ({team_owner}). #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_3rd', 2, 'Scoring {team_score} to finish in 3rd place overall, {team_name} ({team_owner})! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_3rd', 3, 'With a score of {team_score}, our 3rd place finisher {team_name} ({team_owner})! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_T3rd', 1, 'Who\'d have thought that after all those races we have {num_teams} teams tied for 3rd with a score of {team_score}?! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_2nd', 1, 'In 2nd place, and second step of the rostrum, scoring {team_score} is {team_name} ({team_owner}). #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_2nd', 2, 'Scoring {team_score} to finish in 2nd place overall, {team_name} ({team_owner})! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_2nd', 3, 'With a score of {team_score}, our 2nd place finisher {team_name} ({team_owner})! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_T2nd', 1, 'Who\'d have thought that after all those races we have {num_teams} teams tied for 2nd with a score of {team_score}?! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_1st', 1, 'In 1st place, and top step of the rostrum, scoring {team_score} is {team_name} ({team_owner})! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_1st', 2, 'Our champion for {_SEASON}, with a score of {team_score} is {team_name} ({team_owner})! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_1st', 3, 'With a score of {team_score}, our leading team at the end of {_SEASON}, {team_name} ({team_owner})! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_T1st', 1, 'Who\'d have thought that after all those races we have {num_teams} teams tied for the win with a score of {team_score}?! #DeBearF1');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_complete', 1, 'That\'s it for {_SEASON}! Thank you for playing and we look forward to seeing you in {next_season}! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_complete', 2, 'Another season is now in the books! Thank you to all our players and we\'ll be back on the grid in {next_season}! #DeBearF1');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_complete', 3, 'Congratulations to all our winners, thank you all for taking part and we\'ll see you in the paddock in {next_season}! #DeBearF1');

#
# Default announcements
#
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@sport, @season, 1, 'success', 'Welcome!', '<P>Hello and welcome to DeBear Fantasy Sports Formula One!</P>\r\n\r\n<P>To those players joining us for the first time in 2015, thank you for joining and we hope you have fun!</P>\r\n\r\n<P>To those players returning from 2014, welcome back &ndash; thank you for feedback following last season&#39;s game as well as your decision to return this year! Please take the time to check out the <A HREF="help">Help and Rules</A> section to familiarise yourselves with the rules and scoring and get ready for an action packed season!</P>', '2015-01-01 00:00:00', '2015-03-14 05:54:59', 1, 1);
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@sport, @season, 2, 'status', 'Try our mobile site!', '<P>Access Fantasy Formula One on the go by bookmarking <SPAN CLASS="field">fantasy.debear.uk</SPAN> on your mobile phone!</P>\r\n\r\n<P>Using our smartphone site you will be able to view and modify your team(s), check overall standings, as well how your team is faring in its Groups and Fan Leagues plus the season&#39;s race schedule and help / rules on Fantasy Formula One!</P>', '2015-01-01 00:00:00', '2015-03-31 23:59:59', 1, 0);
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@sport, @season, 3, 'status', 'Status of Manor Marussia', '<P>Whilst Manor Marussia have announced their intention to race at the opening Australian Grand Prix, due to the uncertainty of their participation we have decided not to add the team and their drivers until official confirmation nearer the race. As soon as that confirmation is announced we will add them to the game as quickly as we can.</P>', '2015-01-01 00:00:00', '2015-03-05 05:54:59', 1, 1);

