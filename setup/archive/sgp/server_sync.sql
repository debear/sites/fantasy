SET @sport := 'sgp';
SET @season := 2016;

#
# Fantasy SGP Stats
#
SET @sync_app := 'fantasy_sgp';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy SGP (Down)', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ Fantasy Teams', 1, 'script'),
    (@sync_app, 2, '__DIR__ FANTASY_MOTORS_FANLEAGUES_RANK', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_MOTORS_GROUPS_STANDINGS', 3, 'database'),
    (@sync_app, 4, '__DIR__ FANTASY_MOTORS_SELECTIONS', 4, 'database'),
    (@sync_app, 5, '__DIR__ FANTASY_MOTORS_SELECTIONS_COSTS', 5, 'database'),
    (@sync_app, 6, '__DIR__ FANTASY_MOTORS_SELECTIONS_GROUPS_SEL', 6, 'database'),
    (@sync_app, 7, '__DIR__ FANTASY_MOTORS_SELECTIONS_STATS', 7, 'database'),
    (@sync_app, 8, '__DIR__ FANTASY_MOTORS_SELECTIONS_STATUSES', 8, 'database'),
    (@sync_app, 9, '__DIR__ FANTASY_MOTORS_SELECTIONS_SUMMARY', 9, 'database'),
    (@sync_app, 10, '__DIR__ FANTASY_MOTORS_SETUP_STATUS', 10, 'database'),
    (@sync_app, 11, '__DIR__ FANTASY_MOTORS_TEAMS_STANDINGS', 11, 'database'),
    (@sync_app, 12, '__DIR__ COMMS_EMAIL', 12, 'database'),
    (@sync_app, 13, '__DIR__ COMMS_EMAIL_LINK', 13, 'database'),
    (@sync_app, 14, '__DIR__ COMMS_TWITTER', 14, 'database'),
    (@sync_app, 15, '__DIR__ WEATHER_FORECAST', 15, 'database'),
    (@sync_app, 16, '__DIR__ FANTASY_PROFILE_BUDGET_GROUPS', 16, 'database'),
    (@sync_app, 17, '__DIR__ FANTASY_PROFILE_BUDGET_GROUPS_TEAMS', 17, 'database'),
    (@sync_app, 18, '__DIR__ FANTASY_PROFILE_BUDGET_TEAMS', 18, 'database'),
    (@sync_app, 19, '__DIR__ FANTASY_PROFILE_GAMES', 19, 'database'),
    (@sync_app, 20, '__DIR__ FANTASY_PROFILE_GAMES_SCORING', 20, 'database'),
    (@sync_app, 21, '__DIR__ Static Images', 20, 'file');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args) VALUES
    (@sync_app, 1, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ fantasy_sgp_teams __REMOTE__');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_MOTORS_FANLEAGUES_RANK', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_MOTORS_GROUPS_STANDINGS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 4, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 5, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_COSTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 6, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_GROUPS_SEL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 7, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_STATS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 8, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_STATUSES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 9, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_SUMMARY', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 10, 'debearco_fantasy', 'FANTASY_MOTORS_SETUP_STATUS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_MOTORS_TEAMS_STANDINGS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 12, 'debearco_common', 'COMMS_EMAIL', CONCAT('app = ''fantasy_', @sport, '''')),
    (@sync_app, 13, 'debearco_common', 'COMMS_EMAIL_LINK', CONCAT('app = ''fantasy_', @sport, '''')),
    (@sync_app, 14, 'debearco_common', 'COMMS_TWITTER', CONCAT('app = ''fantasy_', @sport, '''')),
    (@sync_app, 15, 'debearco_common', 'WEATHER_FORECAST', CONCAT('app = ''fantasy_', @sport, ''' AND db_id LIKE ''', @season, ':%''')),
    (@sync_app, 16, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_GROUPS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 17, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_GROUPS_TEAMS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 18, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_TEAMS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 19, 'debearco_fantasy', 'FANTASY_PROFILE_GAMES', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 20, 'debearco_fantasy', 'FANTASY_PROFILE_GAMES_SCORING', CONCAT('game = ''', @sport, ''' AND season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt) VALUES
    (@sync_app, 21, CONCAT('/var/www/debear/sites/cdn/htdocs/fantasy/', @sport, '/', @season, '/static'), CONCAT('debear/sites/cdn/htdocs/fantasy/', @sport, '/', @season, '/static'), NULL);

#
# Fantasy SGP Entries
#
SET @sync_app := 'fantasy_sgp_entries';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy SGP Entries (Up)', 'dev,data,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ FANTASY_MOTORS_SELECTIONS', 1, 'database'),
    (@sync_app, 2, '__DIR__ FANTASY_MOTORS_SELECTIONS_COSTS', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_MOTORS_SELECTIONS_GROUPS', 3, 'database'),
    (@sync_app, 4, '__DIR__ FANTASY_MOTORS_SELECTIONS_GROUPS_COST', 4, 'database'),
    (@sync_app, 5, '__DIR__ FANTASY_MOTORS_SELECTIONS_GROUPS_SEL', 5, 'database'),
    (@sync_app, 6, '__DIR__ FANTASY_MOTORS_SELECTIONS_STATUSES', 6, 'database'),
    (@sync_app, 7, '__DIR__ Rider Mugshots', 7, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_COSTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_GROUPS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 4, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_GROUPS_COSTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 5, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_GROUPS_SEL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 6, 'debearco_fantasy', 'FANTASY_MOTORS_SELECTIONS_STATUSES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt) VALUES
    (@sync_app, 7, CONCAT('/var/www/debear/sites/cdn/htdocs/fantasy/', @sport, '/', @season, '/drivers'), CONCAT('debear/sites/cdn/htdocs/fantasy/', @sport, '/', @season, '/drivers'), NULL);

#
# Fantasy SGP Teams
#
SET @sync_app := 'fantasy_sgp_teams';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy SGP Teams', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ USERS', 1, 'database'),
    (@sync_app, 2, '__DIR__ FANTASY_MOTORS_TEAMS', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_MOTORS_TEAMS_INVALID', 3, 'database'),
    (@sync_app, 4, '__DIR__ FANTASY_MOTORS_TEAMS_SEL', 4, 'database'),
    (@sync_app, 5, '__DIR__ FANTASY_MOTORS_GROUPS', 5, 'database'),
    (@sync_app, 6, '__DIR__ FANTASY_MOTORS_GROUPS_ENTRIES', 6, 'database'),
    (@sync_app, 7, '__DIR__ FANTASY_MOTORS_GROUPS_INVITES', 7, 'database'),
    (@sync_app, 8, '__DIR__ FANTASY_MOTORS_GROUPS_MESSAGES', 8, 'database'),
    (@sync_app, 9, '__DIR__ FANTASY_MOTORS_FANLEAGUES_USERS', 9, 'database'),
    (@sync_app, 10, '__DIR__ FANTASY_MOTORS_USER_PREFS', 10, 'database'),
    (@sync_app, 11, '__DIR__ FANTASY_MOTORS_SETUP_STATUS', 11, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_admin', 'USERS', ''),
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_MOTORS_TEAMS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_MOTORS_TEAMS_INVALID', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 4, 'debearco_fantasy', 'FANTASY_MOTORS_TEAMS_SEL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 5, 'debearco_fantasy', 'FANTASY_MOTORS_GROUPS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 6, 'debearco_fantasy', 'FANTASY_MOTORS_GROUPS_ENTRIES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 7, 'debearco_fantasy', 'FANTASY_MOTORS_GROUPS_INVITES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 8, 'debearco_fantasy', 'FANTASY_MOTORS_GROUPS_MESSAGES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 9, 'debearco_fantasy', 'FANTASY_MOTORS_FANLEAGUES_USERS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 10, 'debearco_fantasy', 'FANTASY_MOTORS_USER_PREFS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_MOTORS_SETUP_STATUS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, ''''));

#
# SGP News & Fantasy Announcements
#
SET @sync_app := 'fantasy_sgp_news';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy SGP News', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ FANTASY_COMMON_ANNOUNCEMENTS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_COMMON_ANNOUNCEMENTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, ''''));
