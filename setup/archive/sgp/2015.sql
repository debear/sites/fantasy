SET @sport := 'sgp';
SET @season := 2015;

#
# Remove any previous setup
#
DELETE FROM `FANTASY_COMMON_ANNOUNCEMENTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_ARTICLES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_SECTIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_SETUP_TWITTER` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES_RANK` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_FANLEAGUES_USERS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_ENTRIES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_INVITES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_MESSAGES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_GROUPS_STANDINGS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_CATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_COSTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_GROUPS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_GROUPS_COSTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_GROUPS_SEL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_STATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_STATUSES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SELECTIONS_SUMMARY` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SETUP_STATUS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_SETUP_VALUATIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_STATS` WHERE `sport` = @sport;
DELETE FROM `FANTASY_MOTORS_STATS_SEASON` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_STATS_SECTIONS` WHERE `sport` = @sport;
DELETE FROM `FANTASY_MOTORS_TEAMS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_TEAMS_INVALID` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_TEAMS_SEL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_TEAMS_STANDINGS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_MOTORS_USER_PREFS` WHERE `sport` = @sport AND `season` = @season;

#
# Selections
#
# Group A
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 4,  1, 'regular', 1); # Hancock
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 12, 2, 'regular', 1); # Holder
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 43, 3, 'regular', 1); # KK
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 6,  4, 'regular', 1); # Ped
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 1, 15, 5, 'regular', 1); # Woff

# Group B
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 13, 1, 'regular', 1); # Hampel
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 24, 2, 'regular', 1); # NKI
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 38, 3, 'regular', 1); # MJJ
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 5,  4, 'regular', 1); # AJ
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 2, 31, 5, 'regular', 1); # Zagar

# Group C
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 99,  1, 'regular', 1); # Batch
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 115, 2, 'regular', 1); # Doyle
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 14,  3, 'regular', 1); # Harris
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 41,  4, 'regular', 1); # Janowski
INSERT INTO `FANTASY_MOTORS_SELECTIONS` (`sport`, `season`, `cat_id`, `link_id`, `link_order`, `sel_type`, `sel_type_set`) VALUES (@sport, @season, 3, 22,  5, 'regular', 1); # Jonasson

# Wildcards
INSERT INTO `FANTASY_MOTORS_SELECTIONS_GROUPS` (`sport`, `season`, `cat_id`, `group_id`, `group_name`, `group_short`, `link_order`) VALUES (@sport, @season, 3, 1, 'Wildcards', 'WC', 6);

#
# Valuations
#
DROP TEMPORARY TABLE IF EXISTS `tmp_ROUNDS`;
CREATE TEMPORARY TABLE `tmp_ROUNDS` (`round` INT(11) UNSIGNED NOT NULL, PRIMARY KEY (`round`));
INSERT INTO `tmp_ROUNDS` (`round`) VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12);

DROP TEMPORARY TABLE IF EXISTS `tmp_VALUATIONS`;
CREATE TEMPORARY TABLE `tmp_VALUATIONS` (`cat_id` TINYINT(3) UNSIGNED NOT NULL, `link_id` INT(11) UNSIGNED NOT NULL, `cost` DOUBLE NOT NULL, PRIMARY KEY (`cat_id`, `link_id`));

# Group A: 3.7 - 4.1
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 4,  4.1); # Hancock 
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 43, 4.0); # Kasprzak
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 6,  3.9); # Pedersen
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 15, 3.8); # Woffinden
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (1, 12, 3.7); # Holder

# Group B: 3.3 - 3.7
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 31, 3.7); # Zagar
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 24, 3.6); # Iversen
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 13, 3.5); # Hampel
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 5,  3.4); # Jonsson
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (2, 38, 3.3); # Jepsen Jensen

# Group C: 2.9 - 3.3
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 99,  3.3); # Batchelor
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 41,  3.2); # Janowski
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 14,  3.1); # Harris
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 22,  3.0); # Jonasson
INSERT INTO `tmp_VALUATIONS` (`cat_id`, `link_id`, `cost`) VALUES (3, 115, 2.9); # Doyle

INSERT INTO `FANTASY_MOTORS_SELECTIONS_COSTS` (`sport`, `season`, `cat_id`, `link_id`, `round`, `cost`, `totr`)
  SELECT @sport, @season, `tmp_VALUATIONS`.`cat_id`, `tmp_VALUATIONS`.`link_id`, `tmp_ROUNDS`.`round`, `tmp_VALUATIONS`.`cost`, 0
  FROM `tmp_VALUATIONS`
  JOIN `tmp_ROUNDS` ON (1 = 1);

#
# Selection categories
#
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 1, 'Group A', 'driver', 1, 1);
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 2, 'Group B', 'driver', 2, 1);
INSERT INTO `FANTASY_MOTORS_SELECTIONS_CATS` (`sport`, `season`, `cat_id`, `cat_name`, `link_type`, `cat_order`, `num_sel`) VALUES (@sport, @season, 3, 'Group C', 'driver', 3, 1);

#
# Cost rounds
#
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 1, 1, 3);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 2, 4, 6);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 3, 7, 9);
INSERT INTO `FANTASY_MOTORS_SETUP_VALUATIONS` (`sport`, `season`, `valuation_group`, `round_from`, `round_to`) VALUES (@sport, @season, 4, 10, 12);

# Wildcard costs (rely on valuation groups)
INSERT INTO `FANTASY_MOTORS_SELECTIONS_GROUPS_COSTS` (`sport`, `season`, `cat_id`, `group_id`, `round_from`, `cost`)
  SELECT `sport`, `season`, 3, 1, `round_from`, 2.8
  FROM `FANTASY_MOTORS_SETUP_VALUATIONS`
  WHERE `sport` = @sport
  AND   `season` = @season
  ORDER BY `round_from`;

#
# Fan Leagues
#
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 1, 'country', 'Andorra', 'ad', 0, 50);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 2, 'country', 'United Arab Emirates', 'ae', 0, 41);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 3, 'country', 'Afghanistan', 'af', 0, 46);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 4, 'country', 'Antigua and Barbuda', 'ag', 0, 53);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 5, 'country', 'Anguilla', 'ai', 0, 52);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 6, 'country', 'Albania', 'al', 0, 47);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 7, 'country', 'Armenia', 'am', 0, 54);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 8, 'country', 'Netherlands Antilles', 'an', 0, 171);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 9, 'country', 'Angola', 'ao', 0, 51);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 10, 'country', 'Argentina', 'ar', 0, 1);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 11, 'country', 'American Samoa', 'as', 0, 49);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 12, 'country', 'Austria', 'at', 0, 3);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 13, 'country', 'Australia', 'au', 0, 2);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 14, 'country', 'Aruba', 'aw', 0, 55);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 15, 'country', '&Aring;land Islands', 'ax', 0, 45);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 16, 'country', 'Azerbaijan', 'az', 0, 56);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 17, 'country', 'Bosnia and Herzegovina', 'ba', 0, 66);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 18, 'country', 'Barbados', 'bb', 0, 59);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 19, 'country', 'Bangladesh', 'bd', 0, 58);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 20, 'country', 'Belgium', 'be', 0, 5);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 21, 'country', 'Burkina Faso', 'bf', 0, 73);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 22, 'country', 'Bulgaria', 'bg', 0, 72);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 23, 'country', 'Bahrain', 'bh', 0, 4);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 24, 'country', 'Burundi', 'bi', 0, 74);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 25, 'country', 'Benin', 'bj', 0, 62);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 26, 'country', 'Bermuda', 'bm', 0, 63);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 27, 'country', 'Brunei Darussalam', 'bn', 0, 71);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 28, 'country', 'Bolivia', 'bo', 0, 65);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 29, 'country', 'Brazil', 'br', 0, 6);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 30, 'country', 'Bahamas', 'bs', 0, 57);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 31, 'country', 'Bhutan', 'bt', 0, 64);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 32, 'country', 'Bouvet Island', 'bv', 0, 68);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 33, 'country', 'Botswana', 'bw', 0, 67);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 34, 'country', 'Belarus', 'by', 0, 60);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 35, 'country', 'Belize', 'bz', 0, 61);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 36, 'country', 'Canada', 'ca', 0, 7);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 37, 'country', 'Cocos Islands', 'cc', 0, 84);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 38, 'country', 'Democratic Republic of Congo', 'cd', 0, 94);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 39, 'country', 'Central African Republic', 'cf', 0, 80);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 40, 'country', 'Congo', 'cg', 0, 87);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 41, 'country', 'Switzerland', 'ch', 0, 39);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 42, 'country', 'C&ocirc;te d&#39;Ivoire', 'ci', 0, 75);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 43, 'country', 'Cook Islands', 'ck', 0, 88);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 44, 'country', 'Chile', 'cl', 0, 82);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 45, 'country', 'Cameroon', 'cm', 0, 77);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 46, 'country', 'China', 'cn', 0, 8);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 47, 'country', 'Colombia', 'co', 0, 85);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 48, 'country', 'Costa Rica', 'cr', 0, 89);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 49, 'country', 'Cuba', 'cu', 0, 91);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 50, 'country', 'Cape Verde', 'cv', 0, 78);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 51, 'country', 'Christmas Island', 'cx', 0, 83);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 52, 'country', 'Wales', 'cym', 0, 44);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 53, 'country', 'Cyprus', 'cy', 0, 92);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 54, 'country', 'Czech Republic', 'cz', 0, 9);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 55, 'country', 'Germany', 'de', 0, 14);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 56, 'country', 'Djibouti', 'dj', 0, 95);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 57, 'country', 'Denmark', 'dk', 0, 10);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 58, 'country', 'Dominica', 'dm', 0, 96);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 59, 'country', 'Dominican Republic', 'do', 0, 97);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 60, 'country', 'Algeria', 'dz', 0, 48);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 61, 'country', 'Ecuador', 'ec', 0, 98);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 62, 'country', 'Estonia', 'ee', 0, 103);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 63, 'country', 'Egypt', 'eg', 0, 99);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 64, 'country', 'Western Sahara', 'eh', 0, 241);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 65, 'country', 'England', 'eng', 0, 11);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 66, 'country', 'Eritrea', 'er', 0, 102);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 67, 'country', 'Spain', 'es', 0, 37);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 68, 'country', 'Ethiopia', 'et', 0, 104);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 69, 'country', 'Finland', 'fi', 0, 12);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 70, 'country', 'Fiji', 'fj', 0, 108);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 71, 'country', 'Falkland Islands', 'fk', 0, 105);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 72, 'country', 'Federated States of Micronesia', 'fm', 0, 107);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 73, 'country', 'Faroe Islands', 'fo', 0, 106);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 74, 'country', 'France', 'fr', 0, 13);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 75, 'country', 'Gabon', 'ga', 0, 113);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 76, 'country', 'United Kingdom', 'gb', 1, 234);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 77, 'country', 'Grenada', 'gd', 0, 120);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 78, 'country', 'Georgia', 'ge', 0, 115);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 79, 'country', 'French Guiana', 'gf', 0, 110);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 80, 'country', 'Ghana', 'gh', 0, 116);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 81, 'country', 'Gibraltar', 'gi', 0, 117);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 82, 'country', 'Greenland', 'gl', 0, 119);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 83, 'country', 'Gambia', 'gm', 0, 114);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 84, 'country', 'Guinea', 'gn', 0, 124);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 85, 'country', 'Guadeloupe', 'gp', 0, 121);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 86, 'country', 'Equatorial Guinea', 'gq', 0, 101);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 87, 'country', 'Greece', 'gr', 0, 118);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 88, 'country', 'South Georgia and the South Sandwich Islands', 'gs', 0, 211);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 89, 'country', 'Guatemala', 'gt', 0, 123);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 90, 'country', 'Guam', 'gu', 0, 122);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 91, 'country', 'Guinea-Bissau', 'gw', 0, 125);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 92, 'country', 'Guyana', 'gy', 0, 126);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 93, 'country', 'Hong Kong', 'hk', 0, 131);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 94, 'country', 'Heard Island and McDonald Islands', 'hm', 0, 128);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 95, 'country', 'Honduras', 'hn', 0, 130);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 96, 'country', 'Croatia', 'hr', 0, 90);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 97, 'country', 'Haiti', 'ht', 0, 127);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 98, 'country', 'Hungary', 'hu', 0, 15);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 99, 'country', 'Indonesia', 'id', 0, 132);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 100, 'country', 'Ireland', 'ie', 0, 18);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 101, 'country', 'Northern Ireland', 'nir', 0, 27);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 102, 'country', 'Israel', 'il', 0, 135);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 103, 'country', 'India', 'in', 0, 17);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 104, 'country', 'British Indian Ocean Territory', 'io', 0, 69);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 105, 'country', 'Iraq', 'iq', 0, 133);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 106, 'country', 'Islamic Republic of Iran', 'ir', 0, 134);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 107, 'country', 'Iceland', 'is', 0, 16);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 108, 'country', 'Italy', 'it', 0, 19);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 109, 'country', 'Jamaica', 'jm', 0, 136);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 110, 'country', 'Jordan', 'jo', 0, 137);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 111, 'country', 'Japan', 'jp', 0, 20);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 112, 'country', 'Kenya', 'ke', 0, 139);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 113, 'country', 'Kyrgyzstan', 'kg', 0, 142);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 114, 'country', 'Cambodia', 'kh', 0, 76);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 115, 'country', 'Kiribati', 'ki', 0, 140);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 116, 'country', 'Comoros', 'km', 0, 86);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 117, 'country', 'Saint Kitts and Nevis', 'kn', 0, 196);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 118, 'country', 'Democratic People&#39;s Republic of Korea', 'kp', 0, 93);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 119, 'country', 'Republic of Korea', 'kr', 0, 31);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 120, 'country', 'Kuwait', 'kw', 0, 141);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 121, 'country', 'Cayman Islands', 'ky', 0, 79);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 122, 'country', 'Kazakhstan', 'kz', 0, 138);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 123, 'country', 'Lao People&#39;s Democratic Republic', 'la', 0, 143);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 124, 'country', 'Lebanon', 'lb', 0, 145);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 125, 'country', 'Saint Lucia', 'lc', 0, 197);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 126, 'country', 'Liechtenstein', 'li', 0, 149);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 127, 'country', 'Sri Lanka', 'lk', 0, 212);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 128, 'country', 'Liberia', 'lr', 0, 147);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 129, 'country', 'Lesotho', 'ls', 0, 146);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 130, 'country', 'Lithuania', 'lt', 0, 150);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 131, 'country', 'Luxembourg', 'lu', 0, 21);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 132, 'country', 'Latvia', 'lv', 0, 144);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 133, 'country', 'Libyan Arab Jamahiriya', 'ly', 0, 148);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 134, 'country', 'Morocco', 'ma', 0, 165);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 135, 'country', 'Monaco', 'mc', 0, 24);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 136, 'country', 'Republic of Moldova', 'md', 0, 192);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 137, 'country', 'Montenegro', 'me', 0, 163);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 138, 'country', 'Madagascar', 'mg', 0, 152);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 139, 'country', 'Marshall Islands', 'mh', 0, 157);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 140, 'country', 'Former Yugoslav Republic of Macedonia', 'mk', 0, 109);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 141, 'country', 'Mali', 'ml', 0, 155);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 142, 'country', 'Myanmar', 'mm', 0, 167);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 143, 'country', 'Mongolia', 'mn', 0, 162);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 144, 'country', 'Macao', 'mo', 0, 151);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 145, 'country', 'Northern Mariana Islands', 'mp', 0, 178);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 146, 'country', 'Martinique', 'mq', 0, 158);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 147, 'country', 'Mauritania', 'mr', 0, 159);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 148, 'country', 'Montserrat', 'ms', 0, 164);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 149, 'country', 'Malta', 'mt', 0, 156);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 150, 'country', 'Mauritius', 'mu', 0, 160);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 151, 'country', 'Maldives', 'mv', 0, 154);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 152, 'country', 'Malawi', 'mw', 0, 153);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 153, 'country', 'Mexico', 'mx', 0, 23);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 154, 'country', 'Malaysia', 'my', 0, 22);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 155, 'country', 'Mozambique', 'mz', 0, 166);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 156, 'country', 'Namibia', 'na', 0, 168);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 157, 'country', 'New Caledonia', 'nc', 0, 172);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 158, 'country', 'Niger', 'ne', 0, 174);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 159, 'country', 'Norfolk Island', 'nf', 0, 177);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 160, 'country', 'Nigeria', 'ng', 0, 175);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 161, 'country', 'Nicaragua', 'ni', 0, 173);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 162, 'country', 'Netherlands', 'nl', 0, 25);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 163, 'country', 'Norway', 'no', 0, 28);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 164, 'country', 'Nepal', 'np', 0, 170);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 165, 'country', 'Nauru', 'nr', 0, 169);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 166, 'country', 'Niue', 'nu', 0, 176);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 167, 'country', 'New Zealand', 'nz', 0, 26);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 168, 'country', 'Oman', 'om', 0, 179);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 169, 'country', 'Panama', 'pa', 0, 183);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 170, 'country', 'Peru', 'pe', 0, 186);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 171, 'country', 'French Polynesia', 'pf', 0, 111);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 172, 'country', 'Papua New Guinea', 'pg', 0, 184);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 173, 'country', 'Philippines', 'ph', 0, 187);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 174, 'country', 'Pakistan', 'pk', 0, 180);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 175, 'country', 'Poland', 'pl', 0, 29);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 176, 'country', 'Saint Pierre and Miquelon', 'pm', 0, 198);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 177, 'country', 'Pitcairn', 'pn', 0, 188);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 178, 'country', 'Puerto Rico', 'pr', 0, 189);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 179, 'country', 'Palestine', 'ps', 0, 182);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 180, 'country', 'Portugal', 'pt', 0, 30);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 181, 'country', 'Palau', 'pw', 0, 181);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 182, 'country', 'Paraguay', 'py', 0, 185);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 183, 'country', 'Qatar', 'qa', 0, 190);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 184, 'country', 'R&eacute;union', 're', 0, 191);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 185, 'country', 'Romania', 'ro', 0, 193);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 186, 'country', 'Serbia', 'rs', 0, 204);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 187, 'country', 'Russian Federation', 'ru', 0, 32);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 188, 'country', 'Rwanda', 'rw', 0, 194);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 189, 'country', 'Saudi Arabia', 'sa', 0, 202);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 190, 'country', 'Solomon Islands', 'sb', 0, 209);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 191, 'country', 'Scotland', 'sco', 0, 34);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 192, 'country', 'Seychelles', 'sc', 0, 205);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 193, 'country', 'Sudan', 'sd', 0, 213);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 194, 'country', 'Sweden', 'se', 0, 38);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 195, 'country', 'Singapore', 'sg', 0, 35);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 196, 'country', 'Saint Helena', 'sh', 0, 195);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 197, 'country', 'Slovenia', 'si', 0, 208);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 198, 'country', 'Svalbard and Jan Mayen', 'sj', 0, 215);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 199, 'country', 'Slovakia', 'sk', 0, 207);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 200, 'country', 'Sierra Leone', 'sl', 0, 206);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 201, 'country', 'San Marino', 'sm', 0, 33);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 202, 'country', 'Senegal', 'sn', 0, 203);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 203, 'country', 'Somalia', 'so', 0, 210);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 204, 'country', 'Suriname', 'sr', 0, 214);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 205, 'country', 'Sao Tome and Principe', 'st', 0, 201);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 206, 'country', 'El Salvador', 'sv', 0, 100);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 207, 'country', 'Syrian Arab Republic', 'sy', 0, 217);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 208, 'country', 'Swaziland', 'sz', 0, 216);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 209, 'country', 'Turks and Caicos Islands', 'tc', 0, 229);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 210, 'country', 'Chad', 'td', 0, 81);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 211, 'country', 'French Southern Territories', 'tf', 0, 112);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 212, 'country', 'Togo', 'tg', 0, 223);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 213, 'country', 'Thailand', 'th', 0, 221);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 214, 'country', 'Tajikistan', 'tj', 0, 219);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 215, 'country', 'Tokelau', 'tk', 0, 224);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 216, 'country', 'Timor-Leste', 'tl', 0, 222);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 217, 'country', 'Turkmenistan', 'tm', 0, 228);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 218, 'country', 'Tunisia', 'tn', 0, 227);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 219, 'country', 'Tonga', 'to', 0, 225);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 220, 'country', 'Turkey', 'tr', 0, 40);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 221, 'country', 'Trinidad and Tobago', 'tt', 0, 226);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 222, 'country', 'Tuvalu', 'tv', 0, 230);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 223, 'country', 'Taiwan', 'tw', 0, 218);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 224, 'country', 'Tanzania', 'tz', 0, 220);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 225, 'country', 'Ukraine', 'ua', 0, 233);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 226, 'country', 'Uganda', 'ug', 0, 232);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 227, 'country', 'United States Minor Outlying Islands', 'um', 0, 235);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 228, 'country', 'United States', 'us', 0, 42);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 229, 'country', 'Uruguay', 'uy', 0, 236);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 230, 'country', 'Uzbekistan', 'uz', 0, 237);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 231, 'country', 'Holy See', 'va', 0, 129);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 232, 'country', 'Saint Vincent and the Grenadines', 'vc', 0, 199);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 233, 'country', 'Venezuela', 've', 0, 43);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 234, 'country', 'British Virgin Islands', 'vg', 0, 70);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 235, 'country', 'U.S. Virgin Islands', 'vi', 0, 231);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 236, 'country', 'Viet Nam', 'vn', 0, 239);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 237, 'country', 'Vanuatu', 'vu', 0, 238);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 238, 'country', 'Wallis and Futuna', 'wf', 0, 240);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 239, 'country', 'Samoa', 'ws', 0, 200);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 240, 'country', 'Yemen', 'ye', 0, 242);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 241, 'country', 'Mayotte', 'yt', 0, 161);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 242, 'country', 'South Africa', 'za', 0, 36);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 243, 'country', 'Zambia', 'zm', 0, 243);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 244, 'country', 'Zimbabwe', 'zw', 0, 244);

INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 52);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 65);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 101);
INSERT INTO `FANTASY_MOTORS_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 76, 191);

# Now the teams and drivers
ALTER TABLE `FANTASY_MOTORS_FANLEAGUES` CHANGE `league_id` `league_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;

INSERT INTO `FANTASY_MOTORS_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`)
  SELECT @sport, `SPORTS_SGP_RACE_RIDERS`.`season`, NULL, 'driver', CONCAT(`SPORTS_SPEEDWAY_RIDERS`.`first_name`, ' ', `SPORTS_SPEEDWAY_RIDERS`.`surname`), `SPORTS_SPEEDWAY_RIDERS`.`rider_id`, 0, COUNT(DISTINCT `OTHERS`.`rider_id`) + 1
  FROM `debearco_sports`.`SPORTS_SGP_RACE_RIDERS`
  JOIN `debearco_sports`.`SPORTS_SPEEDWAY_RIDERS`
    ON (`SPORTS_SPEEDWAY_RIDERS`.`rider_id` = `SPORTS_SGP_RACE_RIDERS`.`rider_id`)
  LEFT JOIN `debearco_sports`.`SPORTS_SGP_RACE_RIDERS` AS `OTHER_RIDERS`
    ON (`OTHER_RIDERS`.`season` = `SPORTS_SGP_RACE_RIDERS`.`season`
    AND `OTHER_RIDERS`.`rider_id` <> `SPORTS_SGP_RACE_RIDERS`.`rider_id`)
  LEFT JOIN `debearco_sports`.`SPORTS_SPEEDWAY_RIDERS` AS `OTHERS`
    ON (`OTHERS`.`rider_id` = `OTHER_RIDERS`.`rider_id`
    AND (REPLACE(`OTHERS`.`surname`, '&#381;', 'Z') < REPLACE(`SPORTS_SPEEDWAY_RIDERS`.`surname`, '&#381;', 'Z')
      OR (REPLACE(`OTHERS`.`surname`, '&#381;', 'Z') = REPLACE(`SPORTS_SPEEDWAY_RIDERS`.`surname`, '&#381;', 'Z') AND `OTHERS`.`first_name` < `SPORTS_SPEEDWAY_RIDERS`.`first_name`)))
  WHERE `SPORTS_SGP_RACE_RIDERS`.`season` = @season
  AND   `SPORTS_SGP_RACE_RIDERS`.`bib_no` NOT BETWEEN 16 AND 22
  GROUP BY `SPORTS_SPEEDWAY_RIDERS`.`rider_id`;

ALTER TABLE `FANTASY_MOTORS_FANLEAGUES` CHANGE `league_id` `league_id` INT(11) UNSIGNED NOT NULL;

#
# Help articles
#
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 1, 'Rules', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 2, 'Registration', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 3, 'Team Management', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 4, 'Groups', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@sport, @season, 5, 'Contact Us', 5, 1);

INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 1, 'What is the aim of DeBear Fantasy SGP?', '<P>DeBear Fantasy SGP pits your knowledge of Grand Prix Speedway against other racing fans. Every Grand Prix, select three riders from a pre-defined list within an overall budget and score points based on their performance in real life meetings. The better your predictions, the higher you will be ranked on the overall leaderboard. Can you attain the Number One slot?!</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 2, 'Why do I have a team budget?', '<P>A team budget has been put in place to prevent you from selecting the best riders in every meeting. Rather than limit the number of times a particular rider can be selected, we decided to add another element of strategy to the game: managing a budget. Each selection has an associated cost &ndash; measured in our fictional units, &sect; &ndash; according to their expected level of performance. As long as the total cost of the three selections does not exceed the maximum budget in a round &ndash; 10.0 &sect; &ndash; the team will score you points.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 3, 'How do my selections score points for my team?', '<P>Points are awarded based on performance in both the Main Card and Knockout Stages of a meeting.</P>\r\n<H5>Main Card</H5>\r\n<DL>\r\n  <DT>Intermediate Points</DT>\r\n    <DD><SPAN CLASS="field">All points scored</SPAN> by a rider in the first 20 heats of the meeting will be awarded to your team.</DD>\r\n  <DT>Heats Completed</DT>\r\n    <DD><SPAN CLASS="field">1 point per heat</SPAN> will be awarded to each rider who completes a heat (i.e., is not excluded from the official result, does not retire, etc).</DD>\r\n  <DT>Heat Wins</DT>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> will be awarded to the winner of each heat.</DD>\r\n</DL>\r\n<H5>Knockout Stages</H5>\r\n<DL>\r\n  <DT>Semi-Final Bonus</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> will be awarded to the riders who qualify for the Grand Final.</DD>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> will be awarded to the riders who qualify for the Semi Final but do not advance to the Grand Final.</DD>\r\n  <DT>Grand Final Bonus</DT>\r\n    <DD><SPAN CLASS="field">6 points</SPAN> will be awarded to the rider who wins the Grand Final.</DD>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> will be awarded to the rider who finishes second in the Grand Final.</DD>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> will be awarded to the rider who finishes third in the Grand Final.</DD>\r\n</DL>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 4, 'How are scoring ties broken?', '<P>When Fantasy SGP teams have scored the same number of points, ties in the standings are broken based on total budget spent over the course of the season &ndash; the teams that have cost the least to build are given the higher rank in the standings.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 5, 'What happens if I do not fully use my allocated budget?', '<P>Any unallocated budget at the end of each round is <SPAN CLASS="field">NOT</SPAN> carried over to future rounds &ndash; you will not be allowed to use it in future meetings, nor receive any bonus points towards your team score, so the only potential (and risky) benefit from not using your full budget is in tie-breakers.</P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 1, 6, 'What do I do I think you''ve made a scoring error?', '<P>Scoring disputes should be made by email to <A HREF="mailto:support@debear.uk">support@debear.uk</A>. Please include as much information as possible, in particular the stat(s) you believe to be incorrect and your source to indicate the corrected value!</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 1, 'How do I create a team?', '<P>To create a team click the &quot;Create New Team&quot; links available either on the DeBear Fantasy SGP homepage or the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page and follow the instructions.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 2, 'How many teams can I create?', '<P>You can create up to three (3) Fantasy SGP teams with the same DeBear.uk user account.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 3, 'When can I create a team?', '<P>Teams can be created from Saturday 15th March until Saturday 31st May, in advance of the Czech Grand Prix. After this date no new teams can be created, even if you have not reached your team limit.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 2, 4, 'What information do you need?', '<P>All we need from you when you create your Fantasy SGP team is a team name, and the country you wish to represent &ndash; each team will be displayed throughout the game with an associated flag, just like riders have their nationality displayed, as well as being automatically entered into a &quot;Fan League&quot; so you can compare yourself with fans from your country. Other information, such as your favourite rider is purely optional so you can rate yourself against other fans.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 1, 'What does the &quot;INJ&quot;, &quot;NE&quot;, &quot;OUT&quot;, &quot;SUB&quot; or &quot;SUSP&quot; next to a rider mean?', '<P>If it is expected &ndash; or known &ndash; that a rider will not compete in a given Grand Prix, it will be indicated by an appropriate status update next to the rider&#39;s name on the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page.</P>\r\n<UL>\r\n  <LI><SPAN CLASS="field">NE</SPAN> indicates a rider who has not been entered and so will not participate in the meeting.</LI>\r\n  <LI><SPAN CLASS="field">INJ</SPAN> indicates a rider who is injured and there is uncertainty whether they will participate in the meeting.</LI>\r\n  <LI><SPAN CLASS="field">OUT</SPAN> indicates a rider who is <SPAN CLASS="info">confirmed</SPAN> as not participating in the meeting.</LI>\r\n  <LI><SPAN CLASS="field">SUB</SPAN> indicates a rider who is only entered in the race as a temporary replacement for a regular rider.</LI>\r\n  <LI><SPAN CLASS="field">SUSP</SPAN> indicates a rider who has been suspended from the meeting by the FIM.</LI>\r\n</UL>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 2, 'When can I start editing my team?', '<P>Teams can be edited soon as they are created and there is no restriction on when you can start making selections, so if you wanted you could make your selections for the final round before the season has even started!</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 3, 'Why am I unable to edit my team?', '<P>Selections for a round lock five (5) minutes prior to the advertised start of each Grand Prix. Your selections at this point will be entered as your team for that round. The time this will take place is listed on the applicable pages, such as the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page when making your selections.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 4, 'What happens if I do not make any selections for a meeting?', '<P>Nothing! You will also not score any fantasy points for that round, but the budget for this round will <SPAN CLASS="field">NOT</SPAN> be added to your team for future rounds.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 5, 'Why has the cost of a rider changed since the last Grand Prix?', '<P>To ensure the cost of each rider accurately reflects their value in Fantasy SGP, throughout the season we will monitor and update the valuations of each rider at three (3) designated points:\r\n<OL>\r\n  <LI>Between the Finnish and Czech Grand Prix <SPAN CLASS="info">(Rounds 3 and 4)</SPAN>;</LI>\r\n  <LI>Between the Danish and British Grand Prix <SPAN CLASS="info">(Rounds 6 and 7)</SPAN>;</LI>\r\n  <LI>Between the Gorz&oacute;w and Nordic Grand Prix <SPAN CLASS="info">(Rounds 9 and 10)</SPAN>;</LI>\r\n</OL></P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 3, 7, 'When will these valuation changes actually be made?', '<P>All changes will be made and finalised no later than the Wednesday before the first meeting following this review period. Please make sure you monitor the <A HREF="valuations">Valuations</A> page to see when the cost of each team and driver will be reviewed and updated, and what the new costs will be.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 1, 'What are &quot;Groups&quot; and &quot;Fan Leagues&quot;?', '<P>Groups and Fan Leagues are a great way of comparing the progress of your team(s) against that of your friends and other like-minded SGP fans. Instead of being ranked against every other player in the World, your team will be listed &ndash; and more importantly ranked! &ndash; against the other teams who have joined your Group or selected the same Fan League.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 2, 'How do I create a Group?', '<P>Groups are created from the &quot;Groups&quot; link at the top of each page. Select the &quot;Create New Group&quot; option, give your Group a name, select which of your existing teams you would like to join the group and click &quot;Create&quot;. That&#39;s it!</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 3, 'How do I invite my friends to a Group I have just created?', '<P>Friends can be invited to your group by clicking the &quot;Invite Friends&quot; link on the <SPAN CLASS="info">Group</SPAN> page and entering the email address of the friends you would like to join. They will receive an email inviting them to join your group, and if applicable, create a team first.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 4, 'How do I add my team to an existing Group?', '<P>There are two ways to join a group a friend has created:</P>\r\n<H5>1. Using the e-mail you were sent</H5>\r\n<P>Simply click the link in the e-mail, select the team(s) you would like entered and click &quot;Add Teams&quot;.</P>\r\n\r\n<H5>2. Using the Group PIN</H5>\r\n<P>Each Group has a unique PIN &ndash; having been provided this PIN by the group&#39;s creator, go to the <SPAN CLASS="info">Groups</SPAN> page and enter the PIN and the Group&#39;s password in the &quot;Join Group via PIN&quot; section.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 5, 'How do I join a Fan League?', '<P>You can select the Fan League(s) you wish to join when you first create a team. Once you have created your first team, you can change the Fan League(s) your team(s) belong to when either creating an additional team, or via the &quot;<SPAN CLASS="info">Update Preferences</SPAN>&quot; page.</P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 6, 'Why can I no longer change which Fan League(s) I belong to?', '<P>You can choose which Fan League(s) your team(s) belong to up until selections lock for the first meeting your team(s) compete in &ndash; after this point, you cannot change which Fan League(s) your team(s) belong to.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 7, 'Can I join multiple Fan Leagues?', '<P>No &ndash; the Fan League(s) you join apply to <SPAN CLASS="info">all</SPAN> your teams, it is not possible to enter one team to a Fan League and a second team to a different Fan League.</P>', 7, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 4, 8, 'How do I create a new Fan League?', '<P>It is not possible to create a custom Fan League. The list of Fan Leagues for Countries and Riders is pre-determined before the start of the season and new leagues will not be added throughout the course of the season.</P>', 8, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@sport, @season, 5, 1, 'I still have a problem or unanswered question, what do I do?', '<P>The best thing to do in this case is to email us on <A HREF="mailto:support@debear.uk">support@debear.uk</A>. We will get back to you as soon as we can, however if you are having problems it would really help us if you could provide as much information as possible &ndash; things like what you were trying to do and any error messages that appeared will help us better understand your problem and how we can resolve it. Please don&#39;t worry if you feel unable to provide such information though, your problem will not go unresolved if you do not provide it!</P>', 1, 1);

#
# Stats
#
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES(@sport, 1, 1, 'heat_pts', 'Int. Points', 1, 0, 1);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES(@sport, 2, 1, 'heat_cmpl', 'Heats Completed', 2, 0, 2);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES(@sport, 3, 1, 'heat_wins', 'Heat Wins', 1, 0, 3);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES(@sport, 4, 2, 'semi_final', 'Semi-Final', 1, 0, 1);
INSERT INTO `FANTASY_MOTORS_STATS` (`sport`, `stat_id`, `sect_id`, `stat_short`, `name`, `name_lines`, `driver_only`, `disp_order`) VALUES(@sport, 5, 2, 'grand_final', 'Grand Final', 2, 0, 2);

INSERT INTO `FANTASY_MOTORS_STATS_SECTIONS` (`sport`, `sect_id`, `name`, `disp_order`) VALUES (@sport, 1, 'Main Card', 1);
INSERT INTO `FANTASY_MOTORS_STATS_SECTIONS` (`sport`, `sect_id`, `name`, `disp_order`) VALUES (@sport, 2, 'Knockout Stages', 2);

INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 1);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 2);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 3);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 4);
INSERT INTO `FANTASY_MOTORS_STATS_SEASON` (`sport`, `season`, `stat_id`) VALUES (@sport, @season, 5);

#
# Twitter stuff
#
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 1, 'Just {days} day{days_s} until the deadline for the {race} - is your team ready?! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 2, 'Just {days} day{days_s} until the deadline for the {race} - have you made your selections? #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 3, 'The {race} locks {days_rel} - are you ready!? #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 4, 'The {race} locks {days_rel} - have you made your selections? #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 5, 'Have you made your selections for the {race}? Selections lock {days_rel}! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 6, 'Is your team ready? There''s only {days} day{days_s} until the {race} gets underway! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 7, 'The {race} is this weekend - time to get your selections in! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 8, 'Selections due {days_rel} for the {race} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 9, 'Selections for the {race} due {days_rel} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_deadline', 10, 'Don''t forget to make your selections for the {race} - the deadline is {days_rel}! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 1, 'The {race} results are in! How did your team fare? {standings_url} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 2, 'Race results have been uploaded after the {race}. {standings_url} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 3, 'Time to check your team(s) - the {race} results are in! {standings_url} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 4, 'We''ve updated the site after the {race} - who is leading the way now? {standings_url} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 5, 'Find out if your team leads the way in the {race} - The race results have been uploaded! {standings_url} #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'round_results', 6, 'Time to check the results - the scores from the {race} are in! {standings_url} #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_3rd', 1, 'In 3rd place, and bottom step of the rostrum, scoring {team_score} is {team_name} ({team_owner}). #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_3rd', 2, 'Scoring {team_score} to finish in 3rd place overall, {team_name} ({team_owner})! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_3rd', 3, 'With a score of {team_score}, our 3rd place finisher {team_name} ({team_owner})! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_T3rd', 1, 'Who\'d have thought that after all those meetings we have {num_teams} teams tied for 3rd with a score of {team_score}?! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_2nd', 1, 'In 2nd place, and second step of the rostrum, scoring {team_score} is {team_name} ({team_owner}). #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_2nd', 2, 'Scoring {team_score} to finish in 2nd place overall, {team_name} ({team_owner})! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_2nd', 3, 'With a score of {team_score}, our 2nd place finisher {team_name} ({team_owner})! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_T2nd', 1, 'Who\'d have thought that after all those meetings we have {num_teams} teams tied for 2nd with a score of {team_score}?! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_1st', 1, 'In 1st place, and top step of the rostrum, scoring {team_score} is {team_name} ({team_owner})! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_1st', 2, 'Our champion for {_SEASON}, with a score of {team_score} is {team_name} ({team_owner})! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_1st', 3, 'With a score of {team_score}, our leading team at the end of {_SEASON}, {team_name} ({team_owner})! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_results_T1st', 1, 'Who\'d have thought that after all those meetings we have {num_teams} teams tied for the win with a score of {team_score}?! #DeBearSGP');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_complete', 1, 'That\'s it for {_SEASON}! Thank you for playing and we look forward to seeing you in {next_season}! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_complete', 2, 'Another season is now in the books! Thank you to all our players and we\'ll be back at the tracks in {next_season}! #DeBearSGP');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@sport, @season, 'season_complete', 3, 'Congratulations to all our winners, thank you all for taking part and we\'ll see you in the paddock in {next_season}! #DeBearSGP');

#
# Default announcements
#
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@sport, @season, 1, 'success', 'Welcome!', '<P>Hello and welcome to DeBear Fantasy Sports SGP!</P>\r\n\r\n<P>To those players joining us for the first time in 2015, thank you for joining and we hope you have fun!</P>\r\n\r\n<P>To those players returning from 2014, welcome back &ndash; thank you for feedback following last season&#39;s game as well as your decision to return this year! Please take the time to check out the <A HREF="help">Help and Rules</A> section to familiarise yourselves with the rules and scoring and get ready for an action packed season!</P>', '2015-01-01 00:00:00', '2015-04-18 16:54:59', 1, 1);
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@sport, @season, 2, 'status', 'Try our mobile site!', '<P>Access Fantasy SGP on the go by bookmarking <SPAN CLASS="field">fantasy.debear.uk</SPAN> on your mobile phone!</P>\r\n\r\n<P>Using our smartphone site you will be able to view and modify your team(s), check overall standings, as well how your team is faring in its Groups and Fan Leagues plus the season&#39;s schedule of meetings and help / rules on Fantasy SGP!</P>', '2015-01-01 00:00:00', '2015-05-23 23:59:59', 1, 0);

