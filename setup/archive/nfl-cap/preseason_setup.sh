# This is intended as a scratchpad, rather than an executable...
echo "** This script is not intended to be run..." >&2
exit 1;

season=2017

##
## Intial Load
##
mysql debearco_sports <$season.sql

##
## Selection updates
##
# Eligibility / Build player list...
echo "CALL salcap_position_eligibility('nfl', $season, 1);" | mysql debearco_fantasy

# Download/Process Projections

# Calc initial Valuations based on these projections
echo "CALL salcap_valuations('nfl', $season, 1, 0);" | mysql debearco_fantasy

# Sync
server-sync fantasy_nfl-cap_costs live
server-sync fantasy_nfl-cap_proj live

