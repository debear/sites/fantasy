SET @sport := 'nfl';
SET @section := CONCAT(@sport, '-cap');
SET @season := 2017;

#
# Fantasy NFL SalCap Stats
#
SET @sync_app := 'fantasy_nfl-cap';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL SalCap (Down)', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ Fantasy Teams', 1, 'script'),
    (@sync_app, 2, '__DIR__ FANTASY_SALCAP_FANLEAGUES_RANK', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_SALCAP_GROUPS_STANDINGS', 3, 'database'),
    (@sync_app, 4, '__DIR__ FANTASY_SALCAP_SELECTIONS_COSTS', 4, 'database'),
    (@sync_app, 5, '__DIR__ FANTASY_SALCAP_SELECTIONS_ELIGIBILITY', 5, 'database'),
    (@sync_app, 6, '__DIR__ FANTASY_SALCAP_SELECTIONS_MAPPED', 6, 'database'),
    (@sync_app, 7, '__DIR__ FANTASY_SALCAP_SELECTIONS_SCORING', 7, 'database'),
    (@sync_app, 8, '__DIR__ FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL', 8, 'database'),
    (@sync_app, 9, '__DIR__ FANTASY_SALCAP_SELECTIONS_STATS_PROJECTIONS', 9, 'database'),
    (@sync_app, 10, '__DIR__ FANTASY_SALCAP_SELECTIONS_STATS_TOTALS', 10, 'database'),
    (@sync_app, 11, '__DIR__ FANTASY_SALCAP_SELECTIONS_TOTR', 11, 'database'),
    (@sync_app, 12, '__DIR__ FANTASY_SALCAP_SELECTIONS_USAGE', 12, 'database'),
    (@sync_app, 13, '__DIR__ FANTASY_SALCAP_OPPONENT_PTSAGST', 13, 'database'),
    (@sync_app, 14, '__DIR__ FANTASY_SALCAP_OPPONENT_PTSAGST_DETAIL', 14, 'database'),
    (@sync_app, 15, '__DIR__ FANTASY_SALCAP_SETUP_ROUNDS', 15, 'database'),
    (@sync_app, 16, '__DIR__ FANTASY_SALCAP_SETUP_STATUS', 16, 'database'),
    (@sync_app, 17, '__DIR__ FANTASY_SALCAP_TEAMS_STANDINGS', 17, 'database'),
    (@sync_app, 18, '__DIR__ COMMS_EMAIL', 18, 'database'),
    (@sync_app, 19, '__DIR__ COMMS_EMAIL_LINK', 19, 'database'),
    (@sync_app, 20, '__DIR__ COMMS_TWITTER', 20, 'database'),
    (@sync_app, 21, '__DIR__ FANTASY_PROFILE_BUDGET_GROUPS', 21, 'database'),
    (@sync_app, 22, '__DIR__ FANTASY_PROFILE_BUDGET_GROUPS_TEAMS', 22, 'database'),
    (@sync_app, 23, '__DIR__ FANTASY_PROFILE_BUDGET_TEAMS', 23, 'database'),
    (@sync_app, 24, '__DIR__ FANTASY_PROFILE_GAMES', 24, 'database'),
    (@sync_app, 25, '__DIR__ FANTASY_PROFILE_GAMES_SCORING', 25, 'database');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args) VALUES
    (@sync_app, 1, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ fantasy_nfl-cap_teams __REMOTE__');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_SALCAP_FANLEAGUES_RANK', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_SALCAP_GROUPS_STANDINGS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 4, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_COSTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 5, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_ELIGIBILITY', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 6, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_MAPPED', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 7, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_SCORING', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 8, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 9, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_STATS_PROJECTIONS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 10, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_STATS_TOTALS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_TOTR', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 12, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_USAGE', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 13, 'debearco_fantasy', 'FANTASY_SALCAP_OPPONENT_PTSAGST', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 14, 'debearco_fantasy', 'FANTASY_SALCAP_OPPONENT_PTSAGST_DETAIL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 15, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_ROUNDS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 16, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_STATUS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 17, 'debearco_fantasy', 'FANTASY_SALCAP_TEAMS_STANDINGS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 18, 'debearco_common', 'COMMS_EMAIL', CONCAT('app = ''fantasy_', @section, '''')),
    (@sync_app, 19, 'debearco_common', 'COMMS_EMAIL_LINK', CONCAT('app = ''fantasy_', @section, '''')),
    (@sync_app, 20, 'debearco_common', 'COMMS_TWITTER', CONCAT('app = ''fantasy_', @section, '''')),
    (@sync_app, 21, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_GROUPS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 22, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_GROUPS_TEAMS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 23, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_TEAMS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 24, 'debearco_fantasy', 'FANTASY_PROFILE_GAMES', CONCAT('game = ''', @sport, ''' AND game_type = ''budget'' AND season = ''', @season, '''')),
    (@sync_app, 25, 'debearco_fantasy', 'FANTASY_PROFILE_GAMES_SCORING', CONCAT('game = ''', @sport, ''' AND game_type = ''budget'' AND season = ''', @season, ''''));

#
# Fantasy NFL SalCap Teams
#
SET @sync_app := 'fantasy_nfl-cap_teams';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL SalCap Teams', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ USERS', 1, 'database'),
    (@sync_app, 2, '__DIR__ FANTASY_SALCAP_TEAMS', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_SALCAP_TEAMS_INVALID', 3, 'database'),
    (@sync_app, 4, '__DIR__ FANTASY_SALCAP_TEAMS_SEL', 4, 'database'),
    (@sync_app, 5, '__DIR__ FANTASY_SALCAP_GROUPS', 5, 'database'),
    (@sync_app, 6, '__DIR__ FANTASY_SALCAP_GROUPS_ENTRIES', 6, 'database'),
    (@sync_app, 7, '__DIR__ FANTASY_SALCAP_GROUPS_INVITES', 7, 'database'),
    (@sync_app, 8, '__DIR__ FANTASY_SALCAP_GROUPS_MESSAGES', 8, 'database'),
    (@sync_app, 9, '__DIR__ FANTASY_SALCAP_FANLEAGUES_USERS', 9, 'database'),
    (@sync_app, 10, '__DIR__ FANTASY_SALCAP_USER_PREFS', 10, 'database'),
    (@sync_app, 11, '__DIR__ FANTASY_SALCAP_SETUP_POSITIONS', 11, 'database'),
    (@sync_app, 12, '__DIR__ FANTASY_SALCAP_SETUP_POSITIONS_BREAKDOWN', 12, 'database'),
    (@sync_app, 13, '__DIR__ FANTASY_SALCAP_SETUP_POSITIONS_MAPPING', 13, 'database'),
    (@sync_app, 14, '__DIR__ FANTASY_SALCAP_SETUP_SCORING', 14, 'database'),
    (@sync_app, 15, '__DIR__ FANTASY_SALCAP_SETUP_SCORING_DETAILS', 15, 'database'),
    (@sync_app, 16, '__DIR__ FANTASY_SALCAP_SETUP_STATUS', 16, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_admin', 'USERS', ''),
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_SALCAP_TEAMS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_SALCAP_TEAMS_INVALID', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 4, 'debearco_fantasy', 'FANTASY_SALCAP_TEAMS_SEL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 5, 'debearco_fantasy', 'FANTASY_SALCAP_GROUPS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 6, 'debearco_fantasy', 'FANTASY_SALCAP_GROUPS_ENTRIES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 7, 'debearco_fantasy', 'FANTASY_SALCAP_GROUPS_INVITES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 8, 'debearco_fantasy', 'FANTASY_SALCAP_GROUPS_MESSAGES', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 9, 'debearco_fantasy', 'FANTASY_SALCAP_FANLEAGUES_USERS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 10, 'debearco_fantasy', 'FANTASY_SALCAP_USER_PREFS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_POSITIONS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 12, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_POSITIONS_BREAKDOWN', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 13, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_POSITIONS_MAPPING', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 14, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_SCORING', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 15, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_SCORING_DETAILS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 16, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_STATUS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, ''''));

#
# NFL SalCap News & Fantasy Announcements
#
SET @sync_app := 'fantasy_nfl-cap_news';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL SalCap News', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ FANTASY_COMMON_ANNOUNCEMENTS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_COMMON_ANNOUNCEMENTS', CONCAT('sport = ''', @section, ''' AND season = ''', @season, ''''));

#
# NFL SalCap valuation upload
#
SET @sync_app := 'fantasy_nfl-cap_costs';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL SalCap Costs', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ FANTASY_SALCAP_SELECTIONS_ELIGIBILITY', 1, 'database'),
    (@sync_app, 2, '__DIR__ FANTASY_SALCAP_SELECTIONS_COSTS', 3, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_SALCAP_SETUP_ROUNDS', 3, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_ELIGIBILITY', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_COSTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_ROUNDS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, ''''));

#
# NFL SalCap projection upload
#
SET @sync_app := 'fantasy_nfl-cap_proj';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL SalCap Proj', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    (@sync_app, 1, '__DIR__ FANTASY_SALCAP_SELECTIONS_SCORING', 1, 'database'),
    (@sync_app, 2, '__DIR__ FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_SALCAP_SELECTIONS_STATS_TOTALS', 3, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_SCORING', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_STATS_TOTALS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, ''''));

#
# NFL SalCap scoring upload
#
SET @sync_app := 'fantasy_nfl-cap_up';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL SalCap (Up)', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Selections
    (@sync_app, 1, '__DIR__ FANTASY_SALCAP_SELECTIONS_COSTS', 1, 'database'),
    (@sync_app, 2, '__DIR__ FANTASY_SALCAP_SELECTIONS_ELIGIBILITY', 2, 'database'),
    (@sync_app, 3, '__DIR__ FANTASY_SALCAP_SELECTIONS_SCORING', 3, 'database'),
    (@sync_app, 4, '__DIR__ FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL', 4, 'database'),
    (@sync_app, 5, '__DIR__ FANTASY_SALCAP_SELECTIONS_STATS_PROJECTIONS', 5, 'database'),
    (@sync_app, 6, '__DIR__ FANTASY_SALCAP_SELECTIONS_STATS_TOTALS', 6, 'database'),
    (@sync_app, 7, '__DIR__ FANTASY_SALCAP_SELECTIONS_TOTR', 7, 'database'),
    (@sync_app, 8, '__DIR__ FANTASY_SALCAP_SELECTIONS_USAGE', 8, 'database'),
    (@sync_app, 9, '__DIR__ FANTASY_SALCAP_OPPONENT_PTSAGST', 9, 'database'),
    (@sync_app, 10, '__DIR__ FANTASY_SALCAP_OPPONENT_PTSAGST_DETAIL', 10, 'database'),
    (@sync_app, 11, '__DIR__ FANTASY_SALCAP_SETUP_ROUNDS', 11, 'database'),
    (@sync_app, 12, '__DIR__ FANTASY_SALCAP_SETUP_STATUS', 12, 'database'),
    -- Fantasy Teams
    (@sync_app, 13, '__DIR__ FANTASY_SALCAP_TEAMS_STANDINGS', 13, 'database'),
    (@sync_app, 14, '__DIR__ FANTASY_SALCAP_FANLEAGUES_RANK', 14, 'database'),
    (@sync_app, 15, '__DIR__ FANTASY_SALCAP_GROUPS_STANDINGS', 15, 'database'),
    (@sync_app, 16, '__DIR__ FANTASY_SALCAP_TEAMS_INVALID', 16, 'database'),
    -- Overall fantasy
    (@sync_app, 17, '__DIR__ FANTASY_PROFILE_GAMES', 17, 'database'),
    (@sync_app, 18, '__DIR__ FANTASY_PROFILE_GAMES_SCORING', 18, 'database'),
    (@sync_app, 19, '__DIR__ FANTASY_PROFILE_BUDGET_TEAMS', 19, 'database'),
    (@sync_app, 20, '__DIR__ FANTASY_PROFILE_BUDGET_GROUPS', 20, 'database'),
    (@sync_app, 21, '__DIR__ FANTASY_PROFILE_BUDGET_GROUPS_TEAMS', 21, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Selections
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_COSTS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 2, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_ELIGIBILITY', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 3, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_SCORING', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 4, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 5, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_STATS_PROJECTIONS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 6, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_STATS_TOTALS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 7, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_TOTR', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 8, 'debearco_fantasy', 'FANTASY_SALCAP_SELECTIONS_USAGE', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 9, 'debearco_fantasy', 'FANTASY_SALCAP_OPPONENT_PTSAGST', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 10, 'debearco_fantasy', 'FANTASY_SALCAP_OPPONENT_PTSAGST_DETAIL', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_ROUNDS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 12, 'debearco_fantasy', 'FANTASY_SALCAP_SETUP_STATUS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    -- Fantasy teams
    (@sync_app, 13, 'debearco_fantasy', 'FANTASY_SALCAP_TEAMS_STANDINGS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 14, 'debearco_fantasy', 'FANTASY_SALCAP_FANLEAGUES_RANK', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 15, 'debearco_fantasy', 'FANTASY_SALCAP_GROUPS_STANDINGS', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 16, 'debearco_fantasy', 'FANTASY_SALCAP_TEAMS_INVALID', CONCAT('sport = ''', @sport, ''' AND season = ''', @season, '''')),
    -- Overall fantasy
    (@sync_app, 17, 'debearco_fantasy', 'FANTASY_PROFILE_GAMES', CONCAT('game = ''', @sport, ''' AND game_type = ''budget'' AND season = ''', @season, '''')),
    (@sync_app, 18, 'debearco_fantasy', 'FANTASY_PROFILE_GAMES_SCORING', CONCAT('game = ''', @sport, ''' AND game_type = ''budget'' AND season = ''', @season, '''')),
    (@sync_app, 19, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_TEAMS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 20, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_GROUPS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, '''')),
    (@sync_app, 21, 'debearco_fantasy', 'FANTASY_PROFILE_BUDGET_GROUPS_TEAMS', CONCAT('game = ''', @sport, ''' AND season = ''', @season, ''''));
