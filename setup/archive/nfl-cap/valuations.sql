##
## SQL used to populate the 'Links' tab in the spreadsheet
##
SET @season := 2016;
SELECT ELIG.link_id, ELIG.position,
       IF(SEL_MAP.link_id IS NOT NULL,
          CONCAT(TEAM.city, ' ', TEAM.franchise),
          CONCAT(PLAYER.first_name, ' ', PLAYER.surname)) AS link_name,
       COST.scoring_id,
       SUM(SCORING.score IS NULL) AS proj_gp,
       SUM(SCORING.score IS NOT NULL) AS actual_gp,
       REPLACE(SUM(IF(SCORING.score IS NULL, SCORING.proj, 0)), '.00', '') AS proj_tot,
       REPLACE(SUM(IFNULL(SCORING.score, 0)), '.00', '') AS score_tot,
       IF(SUM(SCORING.score IS NULL) > 0, SUM(IF(SCORING.score IS NULL, SCORING.proj, 0)) / SUM(SCORING.score IS NULL), '') AS proj_per_gp,
       IF(SUM(SCORING.score IS NOT NULL) > 0, SUM(IFNULL(SCORING.score, 0)) / SUM(SCORING.score IS NOT NULL), '') AS score_per_gp,
       (IF(SUM(SCORING.score IS NULL) > 0, SUM(IF(SCORING.score IS NULL, SCORING.proj, 0)) / SUM(SCORING.score IS NULL), 0) * (17 / 17))
         + (IF(SUM(SCORING.score IS NOT NULL) > 0, SUM(IFNULL(SCORING.score, 0)) / SUM(SCORING.score IS NOT NULL), 0) * (0 / 17)) AS combined,
       COST.cost
FROM FANTASY_SALCAP_SELECTIONS_ELIGIBILITY AS ELIG
LEFT JOIN FANTASY_SALCAP_SELECTIONS_MAPPED AS SEL_MAP
  ON (SEL_MAP.sport = ELIG.sport
  AND SEL_MAP.season = ELIG.season
  AND SEL_MAP.link_id = ELIG.link_id)
LEFT JOIN debearco_sports.SPORTS_NFL_TEAMS AS TEAM
  ON (TEAM.team_id = SEL_MAP.raw_id)
LEFT JOIN debearco_sports.SPORTS_NFL_PLAYERS AS PLAYER
  ON (PLAYER.player_id = ELIG.link_id)
JOIN FANTASY_SALCAP_SELECTIONS_COSTS AS COST
  ON (COST.sport = ELIG.sport
  AND COST.season = ELIG.season
  AND COST.link_id = ELIG.link_id
  AND COST.round = 1)
JOIN FANTASY_SALCAP_SELECTIONS_SCORING AS SCORING
  ON (SCORING.sport = COST.sport
  AND SCORING.season = COST.season
  AND SCORING.link_id = COST.link_id
  AND SCORING.scoring_id = COST.scoring_id
  AND SCORING.round > 0)
WHERE ELIG.sport = 'nfl'
AND   ELIG.season = @season
AND   ELIG.type = 'primary'
GROUP BY COST.sport, COST.season, COST.scoring_id, ELIG.link_id
ORDER BY COST.scoring_id, COST.cost DESC, combined DESC, ELIG.link_id;
