SET @sport := 'nfl';
SET @section := CONCAT(@sport, '-cap');
SET @season := 2017;

#
# Remove any previous setup
#
DELETE FROM `FANTASY_COMMON_ANNOUNCEMENTS` WHERE `sport` = @section AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_ARTICLES` WHERE `sport` = @section AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_SECTIONS` WHERE `sport` = @section AND `season` = @season;
DELETE FROM `FANTASY_COMMON_SETUP_TWITTER` WHERE `sport` = @section AND `season` = @season;

DELETE FROM `FANTASY_SALCAP_FANLEAGUES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_FANLEAGUES_COMPOSITE` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_FANLEAGUES_RANK` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_FANLEAGUES_USERS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_GROUPS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_GROUPS_ENTRIES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_GROUPS_INVITES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_GROUPS_MESSAGES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_GROUPS_STANDINGS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_OPPONENT_PTSAGST` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_OPPONENT_PTSAGST_DETAIL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_COSTS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_ELIGIBILITY` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_MAPPED` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_SCORING` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_SCORING_DETAIL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_STATS_PROJECTIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_STATS_TOTALS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_TOTR` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SELECTIONS_USAGE` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_POSITIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_POSITIONS_BREAKDOWN` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_POSITIONS_MAPPING` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_ROUNDS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_SCORING` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_SCORING_DETAILS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_STATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_STATS_COLUMNS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_STATS_POSITIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_SETUP_STATUS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_TEAMS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_TEAMS_INVALID` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_TEAMS_SEL` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_TEAMS_STANDINGS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_SALCAP_USER_PREFS` WHERE `sport` = @sport AND `season` = @season;

#
# Setup
#
INSERT INTO `FANTASY_SALCAP_SETUP_ROUNDS` (`sport`, `season`, `round`, `name`, `start_date`, `end_date`, `valuation_group`)
  SELECT @sport, `season`, `week` AS `round`, CONCAT('Week ', `week`) AS `name`, `start_date`, `end_date`,
         CASE
           WHEN `week` <  4 THEN 1
           WHEN `week` <  7 THEN 2
           WHEN `week` < 10 THEN 3
           WHEN `week` < 13 THEN 4
           WHEN `week` < 16 THEN 5
           ELSE                  6
         END AS `valuation_group`
  FROM `debearco_sports`.`SPORTS_NFL_SCHEDULE_WEEKS`
  WHERE `season` = @season
  AND   `game_type` = 'regular';

# Positions
INSERT INTO `FANTASY_SALCAP_SETUP_POSITIONS` (`sport`, `season`, `posconf_id`, `name`, `display_order`, `public`)
  VALUES (@sport, @season, 1, 'Standard', 10, 1);
INSERT INTO `FANTASY_SALCAP_SETUP_POSITIONS_BREAKDOWN` (`sport`, `season`, `posconf_id`, `position`, `number`, `is_mapped`, `display_order`)
  VALUES (@sport, @season, 1, 'QB', 1, 0, 10),
         (@sport, @season, 1, 'WR', 2, 0, 20),
         (@sport, @season, 1, 'RB', 2, 0, 30),
         (@sport, @season, 1, 'TE', 1, 0, 40),
         (@sport, @season, 1, 'D/ST', 1, 1, 50),
         (@sport, @season, 1, 'K', 1, 0, 60);
INSERT INTO `FANTASY_SALCAP_SETUP_POSITIONS_MAPPING` (`sport`, `season`, `position`, `pos_code`)
  VALUES (@sport, @season, 'QB', 'QB'),
         (@sport, @season, 'WR', 'WR'),
         (@sport, @season, 'RB', 'RB'),
         (@sport, @season, 'RB', 'FB'),
         (@sport, @season, 'TE', 'TE'),
         (@sport, @season, 'D/ST', 'D/ST'),
         (@sport, @season, 'K', 'K');

# Scoring / Stats
INSERT INTO `FANTASY_SALCAP_SETUP_STATS` (`sport`, `season`, `stat_id`, `name_long`, `name_short`, `code`, `display_order`, `totals_avg`)
  VALUES (@sport, @season,  0, 'Games Played', 'GP', 'gp', 0, 0),
         (@sport, @season,  1, 'Passing Yards', 'Pass Yds', 'pass-yds', 10, 0),
         (@sport, @season,  2, 'Passing TDs', 'Pass TDs', 'pass-tds', 20, 0),
         (@sport, @season,  3, 'Interceptions', 'INTs', 'pass-int', 30, 0),
         (@sport, @season,  4, 'Sacked', 'Sack', 'sacked', 40, 0),
         (@sport, @season,  5, 'Rushing Yards', 'Rush Yds', 'rush-yds', 50, 0),
         (@sport, @season,  6, 'Rushing TDs', 'Rush TDs', 'rush-tds', 60, 0),
         (@sport, @season,  7, 'Receptions', 'Recept', 'recept', 70, 0),
         (@sport, @season,  8, 'Receiving Yards', 'Recept Yds', 'recept-yds', 80, 0),
         (@sport, @season,  9, 'Receiving TDs', 'Recept TDs', 'recept-tds', 90, 0),
         (@sport, @season, 10, 'Fumbles Lost', 'Fumb Lost', 'fumb-lost', 100, 0),
         (@sport, @season, 11, '2pt Conversion', '2pt', '2pt-cnv', 110, 0),
         (@sport, @season, 12, 'FGs Under 50yds', 'FGs U50', 'fg-u50', 120, 0),
         (@sport, @season, 13, 'FGs 50yds+', 'FDs 50+', 'fg-o50', 130, 0),
         (@sport, @season, 14, 'XPs', 'XP', 'xp', 140, 0),
         (@sport, @season, 15, 'XPs Missed', 'XPM', 'xp-missed', 150, 0),
         (@sport, @season, 16, 'Points Allowed', 'Pts Agst', 'pts-agst', 160, 1),
         (@sport, @season, 17, 'Sacks', 'Sack', 'sacks', 170, 0),
         (@sport, @season, 18, 'Forced Fumbles', 'Fumb', 'force-fumb', 180, 0),
         (@sport, @season, 19, 'Interceptions', 'INTs', 'def-int', 190, 0),
         (@sport, @season, 20, 'Return TDs', 'Ret TDs', 'ret-td', 200, 0),
         (@sport, @season, 21, 'Safety', 'Sfty', 'safety', 210, 0),
         (@sport, @season, 22, '2pt Returns', '2pt Ret', '2pt-ret', 220, 0);

INSERT INTO `FANTASY_SALCAP_SETUP_STATS_COLUMNS` (`sport`, `season`, `stat_id`, `col_id`, `stat_table`, `stat_column`, `stat_link_column`, `stat_method`)
  VALUES (@sport, @season,  0, 1, 'SPORTS_NFL_GAME_LINEUP', 'status IN ("starter","substitute")', 'player_id', NULL),
         (@sport, @season,  0, 2, 'SPORTS_NFL_SCHEDULE', 'status IS NOT NULL', 'home', NULL),
         (@sport, @season,  0, 3, 'SPORTS_NFL_SCHEDULE', 'status IS NOT NULL', 'visitor', NULL),
         (@sport, @season,  1, 1, 'SPORTS_NFL_PLAYERS_GAME_PASSING', 'yards', 'player_id', NULL),
         (@sport, @season,  2, 1, 'SPORTS_NFL_PLAYERS_GAME_PASSING', 'td', 'player_id', NULL),
         (@sport, @season,  3, 1, 'SPORTS_NFL_PLAYERS_GAME_PASSING', '`int`', 'player_id', NULL),
         (@sport, @season,  4, 1, 'SPORTS_NFL_PLAYERS_GAME_PASSING', 'sacked', 'player_id', NULL),
         (@sport, @season,  5, 1, 'SPORTS_NFL_PLAYERS_GAME_RUSHING', 'yards', 'player_id', NULL),
         (@sport, @season,  6, 1, 'SPORTS_NFL_PLAYERS_GAME_RUSHING', 'td', 'player_id', NULL),
         (@sport, @season,  7, 1, 'SPORTS_NFL_PLAYERS_GAME_RECEIVING', 'recept', 'player_id', NULL),
         (@sport, @season,  8, 1, 'SPORTS_NFL_PLAYERS_GAME_RECEIVING', 'yards', 'player_id', NULL),
         (@sport, @season,  9, 1, 'SPORTS_NFL_PLAYERS_GAME_RECEIVING', 'td', 'player_id', NULL),
         (@sport, @season, 10, 1, 'SPORTS_NFL_PLAYERS_GAME_FUMBLES', 'num_lost', 'player_id', NULL),
         (@sport, @season, 11, 1, 'SPORTS_NFL_GAME_SCORING_2PT', 'made', 'qb', NULL),
         (@sport, @season, 11, 2, 'SPORTS_NFL_GAME_SCORING_2PT', 'made', 'player', NULL),
         (@sport, @season, 12, 1, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'fg_u20', 'player_id', NULL),
         (@sport, @season, 12, 2, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'fg_u30', 'player_id', NULL),
         (@sport, @season, 12, 3, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'fg_u40', 'player_id', NULL),
         (@sport, @season, 12, 4, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'fg_u50', 'player_id', NULL),
         (@sport, @season, 13, 1, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'fg_o50', 'player_id', NULL),
         (@sport, @season, 14, 1, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'xp_made', 'player_id', NULL),
         (@sport, @season, 15, 1, 'SPORTS_NFL_PLAYERS_GAME_KICKING', 'xp_att - xp_made', 'player_id', NULL),
         (@sport, @season, 16, 1, 'SPORTS_NFL_SCHEDULE', NULL, 'home', 'nfl_scoring__pts_agst'),
         (@sport, @season, 16, 2, 'SPORTS_NFL_SCHEDULE', NULL, 'visitor', 'nfl_scoring__pts_agst'),
         (@sport, @season, 17, 1, 'SPORTS_NFL_PLAYERS_GAME_TACKLES', 'sacks', 'team_id', NULL),
         (@sport, @season, 18, 1, 'SPORTS_NFL_PLAYERS_GAME_FUMBLES', 'num_forced', 'team_id', NULL),
         (@sport, @season, 19, 1, 'SPORTS_NFL_PLAYERS_GAME_PASSDEF', '`int`', 'team_id', NULL),
         (@sport, @season, 20, 1, 'SPORTS_NFL_GAME_STATS_TDS', 'num_fumbles + num_ko + num_punt + num_other', 'team_id', NULL),
         (@sport, @season, 21, 1, 'SPORTS_NFL_GAME_STATS_TDS', 'safties', 'team_id', NULL),
         (@sport, @season, 22, 1, 'SPORTS_NFL_GAME_SCORING', 'STAT.type = \'def+2pt\'', 'team_id', NULL);

INSERT INTO `FANTASY_SALCAP_SETUP_STATS_POSITIONS` (`sport`, `season`, `stat_id`, `position`, `summary`)
  VALUES (@sport, @season,  1, 'QB', 1), (@sport, @season,  1, 'RB', 0), (@sport, @season,  1, 'WR', 0), (@sport, @season,  1, 'TE', 0),
         (@sport, @season,  2, 'QB', 1), (@sport, @season,  2, 'RB', 0), (@sport, @season,  2, 'WR', 0), (@sport, @season,  2, 'TE', 0),
         (@sport, @season,  3, 'QB', 1), (@sport, @season,  3, 'RB', 0), (@sport, @season,  3, 'WR', 0), (@sport, @season,  3, 'TE', 0),
         (@sport, @season,  4, 'QB', 1), (@sport, @season,  4, 'RB', 0), (@sport, @season,  4, 'WR', 0), (@sport, @season,  4, 'TE', 0),
         (@sport, @season,  5, 'QB', 0), (@sport, @season,  5, 'RB', 1), (@sport, @season,  5, 'WR', 0), (@sport, @season,  5, 'TE', 0),
         (@sport, @season,  6, 'QB', 0), (@sport, @season,  6, 'RB', 1), (@sport, @season,  6, 'WR', 0), (@sport, @season,  6, 'TE', 0),
         (@sport, @season,  7, 'QB', 0), (@sport, @season,  7, 'RB', 0), (@sport, @season,  7, 'WR', 1), (@sport, @season,  7, 'TE', 1),
         (@sport, @season,  8, 'QB', 0), (@sport, @season,  8, 'RB', 0), (@sport, @season,  8, 'WR', 1), (@sport, @season,  8, 'TE', 1),
         (@sport, @season,  9, 'QB', 0), (@sport, @season,  9, 'RB', 0), (@sport, @season,  9, 'WR', 1), (@sport, @season,  9, 'TE', 1),
         (@sport, @season, 10, 'QB', 1), (@sport, @season, 10, 'RB', 1), (@sport, @season, 10, 'WR', 1), (@sport, @season, 10, 'TE', 1),
         (@sport, @season, 11, 'QB', 1), (@sport, @season, 11, 'RB', 1), (@sport, @season, 11, 'WR', 1), (@sport, @season, 11, 'TE', 1),
         (@sport, @season, 12, 'K', 1),
         (@sport, @season, 13, 'K', 1),
         (@sport, @season, 14, 'K', 1),
         (@sport, @season, 15, 'K', 1),
         (@sport, @season, 16, 'D/ST', 1),
         (@sport, @season, 17, 'D/ST', 1),
         (@sport, @season, 18, 'D/ST', 1),
         (@sport, @season, 19, 'D/ST', 1),
         (@sport, @season, 20, 'D/ST', 1),
         (@sport, @season, 21, 'D/ST', 1),
         (@sport, @season, 22, 'D/ST', 1);

# Note: Including PPR as option, but not actually launching with it
INSERT INTO `FANTASY_SALCAP_SETUP_SCORING` (`sport`, `season`, `scoring_id`, `name`, `display_order`, `public`, `fractional`)
  VALUES (@sport, @season, 1, 'Standard', 10, 1, 0); #,
#         (@sport, @season, 2, 'PPR', 20, 1, 0);
INSERT INTO `FANTASY_SALCAP_SETUP_SCORING_DETAILS` (`sport`, `season`, `scoring_id`, `stat_id`, `pts_base`, `pts_per`, `denomination`)
  VALUES (@sport, @season, 1,  0,  0,  0,  0), #  n/a
         (@sport, @season, 1,  1,  0,  1, 30), #  1pt per 30yds
         (@sport, @season, 1,  2,  0,  6,  1), #  6pts per TD
         (@sport, @season, 1,  3,  0, -3,  1), # -3pts per INT
         (@sport, @season, 1,  4,  0, -2,  1), # -2pts per Sack
         (@sport, @season, 1,  5,  0,  1, 10), #  1pt per 10yds
         (@sport, @season, 1,  6,  0,  6,  1), #  6pts per TD
         ## Intentionally skipping Receptions (Intended PPR option...)
         (@sport, @season, 1,  8,  0,  1, 10), #  1pt per 10yds
         (@sport, @season, 1,  9,  0,  6,  1), #  6pts per TD
         (@sport, @season, 1, 10,  0, -2,  1), # -2pts per Fumb Lost
         (@sport, @season, 1, 11,  0,  2,  1), #  2pts per 2pt Conversion
         (@sport, @season, 1, 12,  0,  3,  1), #  3pts per FG
         (@sport, @season, 1, 13,  0,  5,  1), #  5pts per FG 50+
         (@sport, @season, 1, 14,  0,  1,  1), #  1pt per XP
         (@sport, @season, 1, 15,  0, -1,  1), # -1pt per XP Missed
         (@sport, @season, 1, 16, 10, -1,  3), # Start with 10pts then -1pt per 3pts allowed
         (@sport, @season, 1, 17,  0,  3,  1), #  3pts per Sack
         (@sport, @season, 1, 18,  0,  3,  1), #  3pts per Forced Fumble
         (@sport, @season, 1, 19,  0,  3,  1), #  3pts per INT
         (@sport, @season, 1, 20,  0,  6,  1), #  6pts per TD
         (@sport, @season, 1, 21,  0,  2,  1), #  2pts per Safety
         (@sport, @season, 1, 22,  0,  2,  1); #  2pts per 2pt Return
## Now add the PPR version: same as above, but with the catch option...
#INSERT INTO `FANTASY_SALCAP_SETUP_SCORING_DETAILS` (`sport`, `season`, `scoring_id`, `stat_id`, `pts_base`, `pts_per`, `denomination`)
#  VALUES (@sport, @season, 2,  7,  0,  1,  1); #  1pt per Catch
#INSERT INTO `FANTASY_SALCAP_SETUP_SCORING_DETAILS` (`sport`, `season`, `scoring_id`, `stat_id`, `pts_base`, `pts_per`, `denomination`)
#  SELECT `sport`, `season`, 2 AS `scoring_id`, `stat_id`, `pts_base`, `pts_per`, `denomination`
#  FROM `FANTASY_SALCAP_SETUP_SCORING_DETAILS`
#  WHERE `sport` = @sport
#  AND   `season` = @season
#  AND   `scoring_id` = 1;
ALTER TABLE `FANTASY_SALCAP_SETUP_SCORING_DETAILS` ORDER BY `sport`, `season`, `scoring_id`, `stat_id`;

# Teams as selections
CREATE TEMPORARY TABLE `tmp_team_list` (
  `team_id` VARCHAR(3),
  PRIMARY KEY (`team_id`)
) ENGINE = MEMORY
  SELECT `team_id`
  FROM `debearco_sports`.`SPORTS_NFL_TEAMS_GROUPINGS`
  WHERE @season BETWEEN `season_from` AND IFNULL(`season_to`, 2099);
CREATE TEMPORARY TABLE `tmp_team_list_cp` LIKE `tmp_team_list`;
INSERT INTO `tmp_team_list_cp` SELECT * FROM `tmp_team_list`;

INSERT INTO `FANTASY_SALCAP_SELECTIONS_MAPPED` (`sport`, `season`, `raw_id`, `link_id`)
  SELECT @sport, @season, `tmp_team_list`.`team_id`, 65535 - COUNT(`tmp_team_list_cp`.`team_id`)
  FROM `tmp_team_list`
  LEFT JOIN `tmp_team_list_cp`
    ON (`tmp_team_list_cp`.`team_id` > `tmp_team_list`.`team_id`)
  GROUP BY `tmp_team_list`.`team_id`;

INSERT INTO `FANTASY_SALCAP_SELECTIONS_ELIGIBILITY` (`sport`, `season`, `link_id`, `position`, `round_gained`, `type`)
  SELECT `sport`, `season`, `link_id`, 'D/ST' AS `position`, 1 AS `round_gained`, 'primary' AS `type`
  FROM `FANTASY_SALCAP_SELECTIONS_MAPPED`
  WHERE `sport` = @sport
  AND   `season` = @season;

#
# Announcements
#
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@section, @season, 1, 'success', 'Welcome!', '<P>Hello and welcome to DeBear Fantasy Sports NFL Salary Cap!</P>\r\n\r\n<P>Welcome to the latest addition to the DeBear Fantasy Sports portfolio! Please take the time to check out the <A HREF="help">Help and Rules</A> section to familiarise yourselves with the rules and scoring and get ready for an action packed season!</P>', '2017-08-01 00:00:00', '2017-09-18 23:59:59', 1, 1);
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`sport`, `season`, `announce_id`, `type`, `subject`, `body`, `disp_start`, `disp_end`, `disp_web`, `disp_mobile`) VALUES (@section, @season, 2, 'success', 'When will the selections be loaded?', '<P>We&#39;re waiting for the conclusion of training camps and the final roster cuts, on 2nd September at 4pm ET. Once the 53 man rosters are announced, we will upload the selections as soon as possible ready for the Thursday night kick-off between New England and Kansas City.</P>', '2017-08-01 00:00:00', '2017-09-02 23:59:59', 1, 1);

#
# Help articles
#
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@section, @season, 1, 'Rules', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@section, @season, 2, 'Registration', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@section, @season, 3, 'Team Management', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@section, @season, 4, 'Groups', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`sport`, `season`, `section_id`, `section_name`, `section_order`, `active`) VALUES (@section, @season, 5, 'Contact Us', 5, 1);

INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 1, 'What is the aim of DeBear Fantasy NFL Salary Cap?', '<P>DeBear Fantasy NFL Salary Cap pits your ability to pick a competitive fictional team of NFL players. Every week, select a team from a pre-defined list within an overall budget and score points based on their performance in real life games. The better your predictions, the higher you will be ranked on the overall leaderboard. Can you attain the Number One slot?!</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 2, 'Why do I have a team budget?', '<P>A team budget has been put in place to prevent you from selecting the best options every week. Rather than limit the number of times a particular option can be selected, we decided to add another element of strategy to the game: managing a budget. Each selection has an associated cost &ndash; measured in $ &ndash; according to their expected level of performance. As long as the total cost of all your selections does not exceed the maximum budget in a round &ndash; $100 &ndash; the team will score you points.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 3, 'How do my selections score points for my team?', '<P>Points are awarded based on the stats your selections accrue over the course of each real-life NFL game. Scoring for individuals is based on all their accrued statistics, not just those expected for their position &ndash; for example, if, on a trick play, a Running Back throws a touchdown pass to the Quarterback, the RB will still get the points for a Passing TD and the QB the points for a Receiving TD, even though they are not traditional scoring categories for their position.</P>\r\n<DL>\r\n  <DT>Passing</DT>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> is awarded for every 30 passing yards. No rounding is factored in, so a player who passes for 265 yards will be awarded 8 points.</DD>\r\n    <DD><SPAN CLASS="field">6 points</SPAN> are awarded for every Touchdown pass thrown.</DD>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> are <SPAN CLASS="field">deducted</SPAN> for every Interception thrown.</DD>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are <SPAN CLASS="field">deducted</SPAN> for every time a player is Sacked.</DD>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are awarded for every successfully thrown a 2 Point Conversion.</DD>\r\n  <DT>Rushing</DT>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> is awarded for every 10 rushing yards. No rounding is factored in, so a player who rushes for 85 yards will be awarded 8 points.</DD>\r\n    <DD><SPAN CLASS="field">6 points</SPAN> are awarded for every rushing Touchdown.</DD>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are awarded for every successfully rushed 2 Point Conversion.</DD>\r\n  <DT>Receiving</DT>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> is awarded for every 10 receiving yards. No rounding is factored in, so a player who has 85 receiving yards will be awarded 8 points.</DD>\r\n    <DD><SPAN CLASS="field">6 points</SPAN> are awarded for every Touchdown reception.</DD>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are awarded for every successfully caught 2 Point Conversion.</DD>\r\n  <DT>General Offense</DT>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are <SPAN CLASS="field">deducted</SPAN> for every Fumble Lost.</DD>\r\n  <DT>Kicking</DT>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> are awarded for every Field Goal made that is under 50 yards in length.</DD>\r\n    <DD><SPAN CLASS="field">5 points</SPAN> are awarded for every Field Goal made that is 50 yards or more in length.</DD>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> is awarded for every Extra Point made.</DD>\r\n    <DD><SPAN CLASS="field">1 point</SPAN> is <SPAN CLASS="field">deducted</SPAN> for every Extra Point missed.</DD>\r\n  <DT>Defense / Special Teams</DT>\r\n    <DD><SPAN CLASS="field">10 points</SPAN> are awarded to every Defense / Special Teams that do not concede any points, with <SPAN CLASS="field">1 point deducted</SPAN> from this for every 3 points conceded <SPAN CLASS="info">by the Defense / Special Teams unit</SPAN> &ndash; points conceded by the Offense (such as from Interception Returns) are <SPAN CLASS="info">NOT</SPAN> counted in the Defense / Special Teams unit&#39;s Points Allowed. No rounding is factored in, so a Defense / Special Teams that concedes 6 points will be awarded 8 points.</DD>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> are awarded for every Sack by the defending team.</DD>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> are awarded for every Forced Fumble.</DD>\r\n    <DD><SPAN CLASS="field">3 points</SPAN> are awarded for every Interception caught.</DD>\r\n    <DD><SPAN CLASS="field">6 points</SPAN> are awarded for every Touchdown returned by the defending team (such Interception Returns, Fumble Returns, Punt Returns and Kick-off Returns).</DD>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are awarded for every Safety scored by the defending team.</DD>\r\n    <DD><SPAN CLASS="field">2 points</SPAN> are awarded for every 2 Point Convervsion <SPAN CLASS="info">returned</SPAN> by the defending team.</DD>\r\n</DL>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 4, 'How is the scoring percentage calculated?', '<P>The percentage is based on the the team&#39;s score compared to the highest possible score &ndash; if a team scores 100pts and the highest scoring combination would score 150pts, then the team&#39;s percentage score would be 66.67%: (100 / 150) * 100</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 5, 'How are scoring ties broken?', '<P>When two or more Fantasy NFL Salary Cap teams have scored the same number of points, ties in the standings are broken based on total budget spent over the course of the season &ndash; the teams that have cost the least to build are given the higher rank in the standings.</P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 6, 'What happens if I do not fully use my allocated budget?', '<P>Any unallocated budget at the end of each round is <SPAN CLASS="field">NOT</SPAN> carried over to future rounds &ndash; you will not be allowed to use it in future weeks, nor receive any bonus points towards your team score, so the only potential (and risky) benefit from not using your full budget is in tie-breakers.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 1, 7, 'What do I do I think you''ve made a scoring error?', '<P>Scoring disputes should be made by email to <A HREF="mailto:support@debear.uk">support@debear.uk</A>. Please include as much information as possible, in particular the stat(s) you believe to be incorrect and your source to indicate the corrected value! We will review the information you provide and update the scoring accordingly.</P>', 7, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 2, 1, 'How do I create a team?', '<P>To create a team click the &quot;Create New Team&quot; links available either on the DeBear Fantasy NFL Salary Cap homepage or the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page and follow the instructions.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 2, 2, 'How many teams can I create?', '<P>You can create a maximum of three (3) Fantasy NFL Salary Cap teams with the same DeBear.uk user account.</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 2, 3, 'When can I create a team?', '<P>Teams can be created from Friday 25th August until Saturday 28th October, in advance of the bulk of Week 8''s games. After this date no new teams can be created, even if you have not reached your team limit.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 2, 4, 'What information do you need?', '<P>All we need from you when you create your Fantasy NFL Salary Cap team is a team name, optionally your location and favourite team, so you can compare your progress against fellow fans from where you are and the team you follow!</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 3, 1, 'What do the labels next to a selection mean?', '<P>If it is possible &ndash; or known &ndash; that a player will not take part in his next game, it will be indicated by an appropriate status update next to the player&#39;s name on the &quot;<SPAN CLASS="info">My Team</SPAN>&quot; page.</P>\r\n<UL>\r\n  <LI><SPAN CLASS="field">Q</SPAN> indicates a player who is marked as <SPAN CLASS="info">Questionable</SPAN> on the latest injury report, which means it is uncertain as to whether the player will play in the next game.</LI>\r\n  <LI><SPAN CLASS="field">D</SPAN> indicates a player who is marked as <SPAN CLASS="info">Doubtful</SPAN> on the latest injury report, which means it is unlikely the player will participate in the next game.</LI>\r\n  <LI><SPAN CLASS="field">O</SPAN> indicates a player who is marked as <SPAN CLASS="info">Out</SPAN> on the latest injury report, so will definitely not play in the next game.</LI>\r\n  <LI><SPAN CLASS="field">IR-R</SPAN> indicates a player who is on the team&#39;s <SPAN CLASS="info">Designated for Return</SPAN> list, and will not play this week but may return at some point later in the season.</LI>\r\n  <LI><SPAN CLASS="field">IR</SPAN> indicates a player who is on the team&#39;s <SPAN CLASS="info">Injured Reserve</SPAN> list and will not play again this season.</LI>\r\n  <LI><SPAN CLASS="field">BYE</SPAN> indicates a player whose team is on a Bye Week, and so will not play this week.</LI>\r\n  <LI><SPAN CLASS="field">SUSP</SPAN> indicates a player who has been suspended for the next round by either his team of the league.</LI>\r\n  <LI><SPAN CLASS="field">PUP</SPAN> indicates a player who is on the team&#39;s <SPAN CLASS="info">Physically Unable to Perform</SPAN> list, so is currently inelgible to play in games but may return later in the season.</LI>\r\n  <LI><SPAN CLASS="field">NFI</SPAN> indicates a player who is on the team&#39;s <SPAN CLASS="info">Non-Football Injury</SPAN> list, so is currently inelgible to play in games but may return later in the season.</LI>\r\n  <LI><SPAN CLASS="field">NA</SPAN> indicates a player who is <SPAN CLASS="info">Not Active</SPAN>, so no longer on a team&#39;s active roster and will not appear in a game this week.</LI>\r\n  <LI><SPAN CLASS="field">FA</SPAN> indicates a player who is a <SPAN CLASS="info">Free Agent</SPAN> and has yet to sign with a team and will not appear in a game this week.</LI>\r\n</UL>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 3, 2, 'When can I start editing my team?', '<P>Teams can be edited soon as they are created and there is no restriction on when you can start making selections, so if you wanted you could make your selections for the final round before the season has even started!</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 3, 3, 'Why am I unable to edit my team?', '<P>Each selection locks five (5) minutes prior to the start of their game that week.  If a selection has been included in your team at this time, it will be fixed in to your team that week, however if you have not included it then it will no longer be available to you.  Bye week selections will not lock, but will not score you points either!</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 3, 4, 'What happens if I do not make any selections for a round?', '<P>Nothing! You will also not score any fantasy points for that round, but the budget for this round will <SPAN CLASS="field">NOT</SPAN> be added to your team for future rounds.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 3, 5, 'Why has the cost of a selection changed since last week?', '<P>To ensure the cost of each selection accurately reflects their value in Fantasy NFL Salary Cap, throughout the season we will monitor and update the valuations of each selection at five (5) designated points:\r\n<OL>\r\n  <LI>Between Weeks 3 and 4;</LI>\r\n  <LI>Between Weeks 6 and 7;</LI>\r\n  <LI>Between Weeks 9 and 10;</LI>\r\n  <LI>Between Weeks 12 and 13.</LI>\r\n  <LI>Between Weeks 15 and 16.</LI>\r\n</OL></P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 3, 7, 'When will these valuation changes actually be made?', '<P>All changes will be made and finalised no later than the Wednesday before next round starts.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 1, 'What are &quot;Groups&quot; and &quot;Fan Leagues&quot;?', '<P>Groups and Fan Leagues are a great way of comparing the progress of your team(s) against that of your friends and other like-minded football fans. Instead of being ranked against every other player in the World, your team will be listed &ndash; and more importantly ranked! &ndash; against the other teams who have joined your Group or selected the same Fan League.</P>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 2, 'How do I create a Group?', '<P>Groups are created from the &quot;Groups&quot; link at the top of each page. Select the &quot;Create New Group&quot; option, give your Group a name, select which of your existing teams you would like to join the group and click &quot;Create&quot;. That&#39;s it!</P>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 3, 'How do I invite my friends to a Group I have just created?', '<P>Friends can be invited to your group by clicking the &quot;Invite Friends&quot; link on the <SPAN CLASS="info">Group</SPAN> page and entering the email address of the friends you would like to join. They will receive an email inviting them to join your group, and if applicable, create a team first.</P>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 4, 'How do I add my team to an existing Group?', '<P>There are two ways to join a group a friend has created:</P>\r\n<H5>1. Using the e-mail you were sent</H5>\r\n<P>Simply click the link in the e-mail, select the team(s) you would like entered and click &quot;Add Teams&quot;.</P>\r\n\r\n<H5>2. Using the Group PIN</H5>\r\n<P>Each Group has a unique PIN &ndash; having been provided this PIN by the group&#39;s creator, go to the <SPAN CLASS="info">Groups</SPAN> page and enter the PIN and the Group&#39;s password in the &quot;Join Group via PIN&quot; section.</P>', 4, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 5, 'How do I join a Fan League?', '<P>You can select the Fan League(s) you wish to join when you first create a team. Once you have created your first team, you can change the Fan League(s) your team(s) belong to when either creating an additional team, or via the &quot;<SPAN CLASS="info">Update Preferences</SPAN>&quot; page.</P>', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 6, 'Why can I no longer change which Fan League(s) I belong to?', '<P>You can choose which Fan League(s) your team(s) belong to up until the conclusion of your first week &ndash; after this point, you cannot change which Fan League(s) your team(s) belong to.</P>', 6, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 7, 'Can I join multiple Fan Leagues?', '<P>No &ndash; the Fan League(s) you join apply to <SPAN CLASS="info">all</SPAN> your teams, it is not possible to enter one team to a Fan League and a second team to a different Fan League.</P>', 7, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 4, 8, 'How do I create a new Fan League?', '<P>It is not possible to create a custom Fan League. The list of Fan Leagues for Location and Teams is pre-determined before the start of the season and new leagues will not be added throughout the course of the season.</P>', 8, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`sport`, `season`, `section_id`, `article_id`, `article_title`, `article`, `article_order`, `active`) VALUES (@section, @season, 5, 1, 'I still have a problem or unanswered question, what do I do?', '<P>The best thing to do in this case is to email us on <A HREF="mailto:support@debear.uk">support@debear.uk</A>. We will get back to you as soon as we can, however if you are having problems it would really help us if you could provide as much information as possible &ndash; things like what you were trying to do and any error messages that appeared will help us better understand your problem and how we can resolve it. Please don&#39;t worry if you feel unable to provide such information though, your problem will not go unresolved if you do not provide it!</P>', 1, 1);

#
# Twitter
#
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Wed', 1, 'Another week of #DeBearNFL action gets underway {next_deadline_day}. Is your team ready?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Wed', 2, '{round_name} kicks off {next_deadline}, so have you checked your #DeBearNFL is ready?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Wed', 3, '{last_round_name} is in the books, so it''s time to prepare your #DeBearNFL team for {round_name} which kicks off {next_deadline}.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Wed', 4, 'With the {round_name} action kicking off {next_deadline}, make sure you update your #DeBearNFL team in time!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Wed', 5, 'Time to start focusing on {round_name} - the first deadline is {next_deadline}, so get your #DeBearNFL changes made in time.');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu',  6, 'The {round_name} actions starts with tonight''s game at {next_deadline_time}. Make sure you''re ready! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu',  7, 'Don''t get caught out by tonight''s game, check your #DeBearNFL team by the {next_deadline_time} kick off!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu',  8, 'Is your #DeBearNFL team ready for tonight''s {round_name} opening clash?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu',  9, 'Get ready for {round_name}, the #DeBearNFL action gets underway tonight!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu', 10, 'It''s time to start checking your #DeBearNFL team, as the {round_name} action starts tonight!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu_NoTNF', 11, 'It''s a rare Thursday night without football tonight, but there''s still time to get your #DeBearNFL team ready for the weekend''s action!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Thu_Thksgvng', 12, 'Happy Thanksgiving! It means an early start for your #DeBearNFL team, so make sure you get your changes in soon.');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Fri', 13, 'The #DeBearNFL action resumes {next_deadline_day}, time to start thinking through those selections!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Fri', 14, 'Have you checked the {round_name} schedule to build your best #DeBearNFL team?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Fri', 15, 'With the next #DeBearNFL action {next_deadline_day}, is your team ready?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Fri', 16, 'Are you happy with your #DeBearNFL selections for {round_name}? The bulk of the action gets underway {next_deadline_day}.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Fri', 17, '{round_name} continues {next_deadline_day}, how ready is your #DeBearNFL team?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Fri_Game', 18, 'Enjoy the bonus football today! Don''t forget to check your #DeBearNFL team to ensure you make all changes you need to.');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sat', 19, 'Another action packed Sunday of football kicks off at {next_deadline_time} tomorrow, so get your #DeBearNFL teams ready!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sat', 20, 'The action starts {next_deadline}, so don''t forget to check your #DeBearNFL team beforehand.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sat', 21, 'Is your #DeBearNFL team ready for tomorrow''s slate of games? First kick off is at {next_deadline_time}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sat', 22, 'Time to check your #DeBearNFL team is ready for tomorrow''s games, the action gets underway at {next_deadline_time}.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sat', 23, 'Have you checked your #DeBearNFL team is ready for tomorrow''s action, with the first kick off at {next_deadline_time}?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sat_Game', 24, 'Enjoy the bonus football today! Don''t forget to check your #DeBearNFL team to ensure you make all changes you need to.');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sun', 25, 'It''s gameday in #DeBearNFL! The action starts at {next_deadline_time}, so get your team ready for kick off.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sun', 26, 'Are you ready for some football?! #DeBearNFL kicks off at {next_deadline_time}, so get your team ready too!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sun', 27, 'An action-packed slate of games today in #DeBearNFL. Is your team ready for the {next_deadline_time} kick off?');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sun', 28, 'Today''s #DeBearNFL action starts at {next_deadline_time}, so get your team ready for kick off!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Sun', 29, 'Don''t get caught out by last minute inactives or injuries, check your #DeBearNFL team just before the {next_deadline_time} kick off!');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Mon', 30, 'The final {round_name} game{games_plural} kick{games_plural_inv} off tonight at {next_deadline_time}, so get your #DeBearNFL team ready!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Mon', 31, '{round_name} ends tonight, so get any final #DeBearNFL changes in before the {next_deadline_time} kick off.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Mon', 32, 'If your #DeBearNFL team planned to use players in tonights matchup{games_plural}, make your final checks before the {next_deadline_time} kick off!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Mon', 33, 'Last chance to make a jump in #DeBearNFL - final changes due by {next_deadline_time}!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_deadline_Mon', 34, 'Could your #DeBearNFL team get a final boost in tonight''s {round_name} game{game_plural}?');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'valuations', 1, '{round_name} marks the latest check of #DeBearNFL selection''s cost, so check your team is still in budget!');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'valuations', 2, 'We''ve updated the cost of each #DeBearNFL selection, so best check the effect of this update on your team.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'valuations', 3, 'Beware your #DeBearNFL selections for {round_name} will have new costs due to our latest pricing update.');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'valuations', 4, 'Before {round_name} we''ve updated the prices of all #DeBearNFL selections, so please check how this could affect your team!');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  1, '{round_name} is in the books and the #DeBearNFL scores have been calculated. Was it a good week for you? {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  2, 'We''ve updated #DeBearNFL with the scoring from {round_name}''s games. We hope you did well! {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  3, 'We''ve updated #DeBearNFL following the conclusion of {round_name}. Who leads the way now? {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  4, 'Time to check how your team did in #DeBearNFL this week - the results have been calculated and leaders determined! {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  5, 'The #DeBearNFL scores from {round_name} are in! How did your team fare? {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  6, 'Did you top score in #DeBearNFL this week? Best check, the {round_name} results are in! {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  7, 'Did your #DeBearNFL team perform to expectation or did it fall flat? The results are in! {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  8, 'With {round_name} now complete, the #DeBearNFL scores have been calculated. How did you do? {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results',  9, 'Were you a {round_name} winner in #DeBearNFL? We''ve calculated this week''s results! {standings_url}');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'round_results', 10, 'The #DeBearNFL results are in after another exciting week of action. Did your team perform well? {standings_url}');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_3rd', 1, 'In 3rd place, and bottom step of the rostrum, scoring {team_score} is {team_name} ({team_owner}). #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_3rd', 2, 'Scoring {team_score} to finish in 3rd place overall, {team_name} ({team_owner})! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_3rd', 3, 'With a score of {team_score}, our 3rd place finisher {team_name} ({team_owner})! #DeBearNFL');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_T3rd', 1, 'Who\'d have thought that after all those games we have {num_teams} teams tied for 3rd with a score of {team_score}?! #DeBearNFL');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_2nd', 1, 'In 2nd place, and second step of the rostrum, scoring {team_score} is {team_name} ({team_owner}). #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_2nd', 2, 'Scoring {team_score} to finish in 2nd place overall, {team_name} ({team_owner})! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_2nd', 3, 'With a score of {team_score}, our 2nd place finisher {team_name} ({team_owner})! #DeBearNFL');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_T2nd', 1, 'Who\'d have thought that after all those games we have {num_teams} teams tied for 2nd with a score of {team_score}?! #DeBearNFL');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_1st', 1, 'In 1st place, and top step of the rostrum, scoring {team_score} is {team_name} ({team_owner})! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_1st', 2, 'Our champion for {_SEASON}, with a score of {team_score} is {team_name} ({team_owner})! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_1st', 3, 'With a score of {team_score}, our leading team at the end of {_SEASON}, {team_name} ({team_owner})! #DeBearNFL');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_results_T1st', 1, 'Who\'d have thought that after all those games we have {num_teams} teams tied for the win with a score of {team_score}?! #DeBearNFL');

INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_complete', 1, 'That\'s it for {_SEASON}! Thank you for playing and we look forward to seeing you in {next_season}! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_complete', 2, 'Another season is now in the books! Thank you to all our players and we\'ll be back on the field in {next_season}! #DeBearNFL');
INSERT INTO `FANTASY_COMMON_SETUP_TWITTER` (`sport`, `season`, `section`, `tweet_id`, `template`) VALUES (@section, @season, 'season_complete', 3, 'Congratulations to all our winners, thank you all for taking part and we\'ll see you ready for training camp in {next_season}! #DeBearNFL');

#
# Fan Leagues
#
# State
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 1, 'location', 'Alaska', 'us-ak', 0, 2);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 2, 'location', 'Alabama', 'us-al', 0, 1);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 3, 'location', 'Arkansas', 'us-ar', 0, 4);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 4, 'location', 'Arizona', 'us-az', 0, 3);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 5, 'location', 'California', 'us-ca', 0, 5);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 6, 'location', 'Colorado', 'us-co', 0, 6);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 7, 'location', 'Connecticut', 'us-ct', 0, 7);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 8, 'location', 'District of Columbia', 'us-dc', 0, 9);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 9, 'location', 'Delaware', 'us-de', 0, 8);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 10, 'location', 'Florida', 'us-fl', 0, 10);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 11, 'location', 'Georgia', 'us-ga', 0, 11);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 12, 'location', 'Hawaii', 'us-hi', 0, 12);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 13, 'location', 'Iowa', 'us-ia', 0, 16);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 14, 'location', 'Idaho', 'us-id', 0, 13);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 15, 'location', 'Illinois', 'us-il', 0, 14);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 16, 'location', 'Indiana', 'us-in', 0, 15);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 17, 'location', 'Kansas', 'us-ks', 0, 17);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 18, 'location', 'Kentucky', 'us-ky', 0, 18);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 19, 'location', 'Louisiana', 'us-la', 0, 19);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 20, 'location', 'Massachusetts', 'us-ma', 0, 22);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 21, 'location', 'Maryland', 'us-md', 0, 21);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 22, 'location', 'Maine', 'us-me', 0, 20);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 23, 'location', 'Michigan', 'us-mi', 0, 23);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 24, 'location', 'Minnesota', 'us-mn', 0, 24);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 25, 'location', 'Missouri', 'us-mo', 0, 26);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 26, 'location', 'Mississippi', 'us-ms', 0, 25);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 27, 'location', 'Montana', 'us-mt', 0, 27);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 28, 'location', 'North Carolina', 'us-nc', 0, 34);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 29, 'location', 'North Dakota', 'us-nd', 0, 35);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 30, 'location', 'Nebraska', 'us-ne', 0, 28);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 31, 'location', 'New Hampshire', 'us-nh', 0, 30);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 32, 'location', 'New Jersey', 'us-nj', 0, 31);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 33, 'location', 'New Mexico', 'us-nm', 0, 32);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 34, 'location', 'Nevada', 'us-nv', 0, 29);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 35, 'location', 'New York', 'us-ny', 0, 33);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 36, 'location', 'Ohio', 'us-oh', 0, 36);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 37, 'location', 'Oklahoma', 'us-ok', 0, 37);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 38, 'location', 'Oregon', 'us-or', 0, 38);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 39, 'location', 'Pennsylvania', 'us-pa', 0, 39);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 40, 'location', 'Rhode Island', 'us-ri', 0, 40);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 41, 'location', 'South Carolina', 'us-sc', 0, 41);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 42, 'location', 'South Dakota', 'us-sd', 0, 42);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 43, 'location', 'Tennessee', 'us-tn', 0, 43);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 44, 'location', 'Texas', 'us-tx', 0, 44);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 45, 'location', 'Utah', 'us-ut', 0, 45);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 46, 'location', 'Virginia', 'us-va', 0, 47);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 47, 'location', 'Vermont', 'us-vt', 0, 46);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 48, 'location', 'Washington', 'us-wa', 0, 48);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 49, 'location', 'Wisconsin', 'us-wi', 0, 50);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 50, 'location', 'West Virginia', 'us-wv', 0, 49);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 51, 'location', 'Wyoming', 'us-wy', 0, 51);

# Countries - Raw
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 52, 'location', 'Andorra', 'ad', 0, 93);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 53, 'location', 'United Arab Emirates', 'ae', 0, 283);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 54, 'location', 'Afghanistan', 'af', 0, 90);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 55, 'location', 'Antigua and Barbuda', 'ag', 0, 96);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 56, 'location', 'Anguilla', 'ai', 0, 95);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 57, 'location', 'Albania', 'al', 0, 91);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 58, 'location', 'Armenia', 'am', 0, 98);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 59, 'location', 'Netherlands Antilles', 'an', 0, 217);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 60, 'location', 'Angola', 'ao', 0, 94);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 61, 'location', 'Argentina', 'ar', 0, 97);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 62, 'location', 'American Samoa', 'as', 0, 52);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 63, 'location', 'Austria', 'at', 0, 100);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 64, 'location', 'Australia', 'au', 0, 53);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 65, 'location', 'Aruba', 'aw', 0, 99);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 66, 'location', '&Aring;land Islands', 'ax', 0, 89);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 67, 'location', 'Azerbaijan', 'az', 0, 101);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 68, 'location', 'Bosnia and Herzegovina', 'ba', 0, 112);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 69, 'location', 'Barbados', 'bb', 0, 105);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 70, 'location', 'Bangladesh', 'bd', 0, 104);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 71, 'location', 'Belgium', 'be', 0, 54);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 72, 'location', 'Burkina Faso', 'bf', 0, 119);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 73, 'location', 'Bulgaria', 'bg', 0, 118);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 74, 'location', 'Bahrain', 'bh', 0, 103);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 75, 'location', 'Burundi', 'bi', 0, 120);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 76, 'location', 'Benin', 'bj', 0, 108);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 77, 'location', 'Bermuda', 'bm', 0, 109);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 78, 'location', 'Brunei Darussalam', 'bn', 0, 117);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 79, 'location', 'Bolivia', 'bo', 0, 111);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 80, 'location', 'Brazil', 'br', 0, 55);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 81, 'location', 'Bahamas', 'bs', 0, 102);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 82, 'location', 'Bhutan', 'bt', 0, 110);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 83, 'location', 'Bouvet Island', 'bv', 0, 114);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 84, 'location', 'Botswana', 'bw', 0, 113);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 85, 'location', 'Belarus', 'by', 0, 106);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 86, 'location', 'Belize', 'bz', 0, 107);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 87, 'location', 'Canada', 'ca', 0, 56);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 88, 'location', 'Cocos Islands', 'cc', 0, 130);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 89, 'location', 'Democratic Republic of Congo', 'cd', 0, 139);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 90, 'location', 'Central African Republic', 'cf', 0, 126);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 91, 'location', 'Congo', 'cg', 0, 132);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 92, 'location', 'Switzerland', 'ch', 0, 265);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 93, 'location', 'C&ocirc;te d&#39;Ivoire', 'ci', 0, 121);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 94, 'location', 'Cook Islands', 'ck', 0, 133);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 95, 'location', 'Chile', 'cl', 0, 128);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 96, 'location', 'Cameroon', 'cm', 0, 123);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 97, 'location', 'China', 'cn', 0, 57);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 98, 'location', 'Colombia', 'co', 0, 58);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 99, 'location', 'Costa Rica', 'cr', 0, 134);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 100, 'location', 'Cuba', 'cu', 0, 136);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 101, 'location', 'Cape Verde', 'cv', 0, 124);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 102, 'location', 'Christmas Island', 'cx', 0, 129);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 103, 'location', 'Cyprus', 'cy', 0, 137);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 104, 'location', 'Wales', 'cym', 0, 88);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 105, 'location', 'Czech Republic', 'cz', 0, 59);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 106, 'location', 'Germany', 'de', 0, 64);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 107, 'location', 'Djibouti', 'dj', 0, 140);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 108, 'location', 'Denmark', 'dk', 0, 60);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 109, 'location', 'Dominica', 'dm', 0, 141);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 110, 'location', 'Dominican Republic', 'do', 0, 142);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 111, 'location', 'Algeria', 'dz', 0, 92);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 112, 'location', 'Ecuador', 'ec', 0, 143);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 113, 'location', 'Estonia', 'ee', 0, 148);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 114, 'location', 'Egypt', 'eg', 0, 144);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 115, 'location', 'Western Sahara', 'eh', 0, 290);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 116, 'location', 'England', 'eng', 0, 61);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 117, 'location', 'Eritrea', 'er', 0, 147);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 118, 'location', 'Spain', 'es', 0, 84);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 119, 'location', 'Ethiopia', 'et', 0, 149);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 120, 'location', 'Finland', 'fi', 0, 62);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 121, 'location', 'Fiji', 'fj', 0, 153);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 122, 'location', 'Falkland Islands', 'fk', 0, 150);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 123, 'location', 'Federated States of Micronesia', 'fm', 0, 152);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 124, 'location', 'Faroe Islands', 'fo', 0, 151);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 125, 'location', 'France', 'fr', 0, 63);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 126, 'location', 'Gabon', 'ga', 0, 158);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 128, 'location', 'Grenada', 'gd', 0, 164);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 129, 'location', 'Georgia', 'ge', 0, 160);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 130, 'location', 'French Guiana', 'gf', 0, 155);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 131, 'location', 'Ghana', 'gh', 0, 161);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 132, 'location', 'Gibraltar', 'gi', 0, 162);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 133, 'location', 'Greenland', 'gl', 0, 163);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 134, 'location', 'Gambia', 'gm', 0, 159);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 135, 'location', 'Guinea', 'gn', 0, 168);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 136, 'location', 'Guadeloupe', 'gp', 0, 165);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 137, 'location', 'Equatorial Guinea', 'gq', 0, 146);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 138, 'location', 'Greece', 'gr', 0, 65);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 139, 'location', 'South Georgia and the South Sandwich Islands', 'gs', 0, 259);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 140, 'location', 'Guatemala', 'gt', 0, 166);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 141, 'location', 'Guam', 'gu', 0, 66);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 142, 'location', 'Guinea-Bissau', 'gw', 0, 167);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 143, 'location', 'Guyana', 'gy', 0, 169);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 144, 'location', 'Hong Kong', 'hk', 0, 174);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 145, 'location', 'Heard Island and McDonald Islands', 'hm', 0, 171);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 146, 'location', 'Honduras', 'hn', 0, 173);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 147, 'location', 'Croatia', 'hr', 0, 135);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 148, 'location', 'Haiti', 'ht', 0, 170);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 149, 'location', 'Hungary', 'hu', 0, 67);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 150, 'location', 'Indonesia', 'id', 0, 176);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 151, 'location', 'Ireland', 'ie', 0, 69);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 152, 'location', 'Israel', 'il', 0, 179);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 153, 'location', 'India', 'in', 0, 175);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 154, 'location', 'British Indian Ocean Territory', 'io', 0, 115);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 155, 'location', 'Iraq', 'iq', 0, 177);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 156, 'location', 'Islamic Republic of Iran', 'ir', 0, 178);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 157, 'location', 'Iceland', 'is', 0, 68);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 158, 'location', 'Italy', 'it', 0, 70);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 159, 'location', 'Jamaica', 'jm', 0, 180);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 160, 'location', 'Jordan', 'jo', 0, 181);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 161, 'location', 'Japan', 'jp', 0, 71);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 162, 'location', 'Kenya', 'ke', 0, 183);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 163, 'location', 'Kyrgyzstan', 'kg', 0, 186);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 164, 'location', 'Cambodia', 'kh', 0, 122);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 165, 'location', 'Kiribati', 'ki', 0, 184);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 166, 'location', 'Comoros', 'km', 0, 131);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 167, 'location', 'Saint Kitts and Nevis', 'kn', 0, 242);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 168, 'location', 'Democratic People&#39;s Republic of Korea', 'kp', 0, 138);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 169, 'location', 'Republic of Korea', 'kr', 0, 237);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 170, 'location', 'Kuwait', 'kw', 0, 185);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 171, 'location', 'Cayman Islands', 'ky', 0, 125);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 172, 'location', 'Kazakhstan', 'kz', 0, 182);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 173, 'location', 'Lao People&#39;s Democratic Republic', 'la', 0, 187);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 174, 'location', 'Lebanon', 'lb', 0, 189);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 175, 'location', 'Saint Lucia', 'lc', 0, 243);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 176, 'location', 'Liechtenstein', 'li', 0, 193);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 177, 'location', 'Sri Lanka', 'lk', 0, 260);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 178, 'location', 'Liberia', 'lr', 0, 191);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 179, 'location', 'Lesotho', 'ls', 0, 190);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 180, 'location', 'Lithuania', 'lt', 0, 72);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 181, 'location', 'Luxembourg', 'lu', 0, 194);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 182, 'location', 'Latvia', 'lv', 0, 188);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 183, 'location', 'Libyan Arab Jamahiriya', 'ly', 0, 192);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 184, 'location', 'Morocco', 'ma', 0, 211);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 185, 'location', 'Monaco', 'mc', 0, 207);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 186, 'location', 'Republic of Moldova', 'md', 0, 238);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 187, 'location', 'Montenegro', 'me', 0, 209);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 188, 'location', 'Madagascar', 'mg', 0, 196);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 189, 'location', 'Marshall Islands', 'mh', 0, 202);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 190, 'location', 'Former Yugoslav Republic of Macedonia', 'mk', 0, 154);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 191, 'location', 'Mali', 'ml', 0, 200);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 192, 'location', 'Myanmar', 'mm', 0, 213);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 193, 'location', 'Mongolia', 'mn', 0, 208);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 194, 'location', 'Macao', 'mo', 0, 195);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 195, 'location', 'Northern Mariana Islands', 'mp', 0, 77);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 196, 'location', 'Martinique', 'mq', 0, 203);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 197, 'location', 'Mauritania', 'mr', 0, 204);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 198, 'location', 'Montserrat', 'ms', 0, 210);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 199, 'location', 'Malta', 'mt', 0, 201);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 200, 'location', 'Mauritius', 'mu', 0, 205);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 201, 'location', 'Maldives', 'mv', 0, 199);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 202, 'location', 'Malawi', 'mw', 0, 197);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 203, 'location', 'Mexico', 'mx', 0, 73);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 204, 'location', 'Malaysia', 'my', 0, 198);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 205, 'location', 'Mozambique', 'mz', 0, 212);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 206, 'location', 'Namibia', 'na', 0, 214);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 207, 'location', 'New Caledonia', 'nc', 0, 218);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 208, 'location', 'Niger', 'ne', 0, 220);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 209, 'location', 'Norfolk Island', 'nf', 0, 223);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 210, 'location', 'Nigeria', 'ng', 0, 221);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 211, 'location', 'Nicaragua', 'ni', 0, 219);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 212, 'location', 'Northern Ireland', 'nir', 0, 76);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 213, 'location', 'Netherlands', 'nl', 0, 74);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 214, 'location', 'Norway', 'no', 0, 78);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 215, 'location', 'Nepal', 'np', 0, 216);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 216, 'location', 'Nauru', 'nr', 0, 215);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 217, 'location', 'Niue', 'nu', 0, 222);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 218, 'location', 'New Zealand', 'nz', 0, 75);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 219, 'location', 'Oman', 'om', 0, 224);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 220, 'location', 'Panama', 'pa', 0, 228);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 221, 'location', 'Peru', 'pe', 0, 231);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 222, 'location', 'French Polynesia', 'pf', 0, 156);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 223, 'location', 'Papua New Guinea', 'pg', 0, 229);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 224, 'location', 'Philippines', 'ph', 0, 232);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 225, 'location', 'Pakistan', 'pk', 0, 225);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 226, 'location', 'Poland', 'pl', 0, 79);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 227, 'location', 'Saint Pierre and Miquelon', 'pm', 0, 244);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 228, 'location', 'Pitcairn', 'pn', 0, 233);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 229, 'location', 'Puerto Rico', 'pr', 0, 80);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 230, 'location', 'Palestine', 'ps', 0, 227);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 231, 'location', 'Portugal', 'pt', 0, 234);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 232, 'location', 'Palau', 'pw', 0, 226);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 233, 'location', 'Paraguay', 'py', 0, 230);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 234, 'location', 'Qatar', 'qa', 0, 235);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 235, 'location', 'R&eacute;union', 're', 0, 236);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 236, 'location', 'Romania', 'ro', 0, 239);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 237, 'location', 'Serbia', 'rs', 0, 251);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 238, 'location', 'Russian Federation', 'ru', 0, 81);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 239, 'location', 'Rwanda', 'rw', 0, 240);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 240, 'location', 'Saudi Arabia', 'sa', 0, 249);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 241, 'location', 'Solomon Islands', 'sb', 0, 256);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 242, 'location', 'Seychelles', 'sc', 0, 252);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 243, 'location', 'Scotland', 'sco', 0, 82);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 244, 'location', 'Sudan', 'sd', 0, 261);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 245, 'location', 'Sweden', 'se', 0, 85);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 246, 'location', 'Singapore', 'sg', 0, 254);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 247, 'location', 'Saint Helena', 'sh', 0, 241);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 248, 'location', 'Slovenia', 'si', 0, 255);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 249, 'location', 'Svalbard and Jan Mayen', 'sj', 0, 263);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 250, 'location', 'Slovakia', 'sk', 0, 83);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 251, 'location', 'Sierra Leone', 'sl', 0, 253);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 252, 'location', 'San Marino', 'sm', 0, 247);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 253, 'location', 'Senegal', 'sn', 0, 250);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 254, 'location', 'Somalia', 'so', 0, 257);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 255, 'location', 'Suriname', 'sr', 0, 262);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 256, 'location', 'Sao Tome and Principe', 'st', 0, 248);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 257, 'location', 'El Salvador', 'sv', 0, 145);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 258, 'location', 'Syrian Arab Republic', 'sy', 0, 266);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 259, 'location', 'Swaziland', 'sz', 0, 264);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 260, 'location', 'Turks and Caicos Islands', 'tc', 0, 279);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 261, 'location', 'Chad', 'td', 0, 127);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 262, 'location', 'French Southern Territories', 'tf', 0, 157);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 263, 'location', 'Togo', 'tg', 0, 272);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 264, 'location', 'Thailand', 'th', 0, 270);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 265, 'location', 'Tajikistan', 'tj', 0, 268);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 266, 'location', 'Tokelau', 'tk', 0, 273);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 267, 'location', 'Timor-Leste', 'tl', 0, 271);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 268, 'location', 'Turkmenistan', 'tm', 0, 278);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 269, 'location', 'Tunisia', 'tn', 0, 276);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 270, 'location', 'Tonga', 'to', 0, 274);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 271, 'location', 'Turkey', 'tr', 0, 277);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 272, 'location', 'Trinidad and Tobago', 'tt', 0, 275);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 273, 'location', 'Tuvalu', 'tv', 0, 280);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 274, 'location', 'Taiwan', 'tw', 0, 267);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 275, 'location', 'Tanzania', 'tz', 0, 269);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 276, 'location', 'Ukraine', 'ua', 0, 282);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 277, 'location', 'Uganda', 'ug', 0, 281);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 278, 'location', 'United States Minor Outlying Islands', 'um', 0, 87);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 280, 'location', 'Uruguay', 'uy', 0, 284);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 281, 'location', 'Uzbekistan', 'uz', 0, 285);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 282, 'location', 'Holy See', 'va', 0, 172);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 283, 'location', 'Saint Vincent and the Grenadines', 'vc', 0, 245);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 284, 'location', 'Venezuela', 've', 0, 287);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 285, 'location', 'British Virgin Islands', 'vg', 0, 116);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 286, 'location', 'U.S. Virgin Islands', 'vi', 0, 86);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 287, 'location', 'Viet Nam', 'vn', 0, 288);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 288, 'location', 'Vanuatu', 'vu', 0, 286);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 289, 'location', 'Wallis and Futuna', 'wf', 0, 289);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 290, 'location', 'Samoa', 'ws', 0, 246);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 291, 'location', 'Yemen', 'ye', 0, 291);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 292, 'location', 'Mayotte', 'yt', 0, 206);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 293, 'location', 'South Africa', 'za', 0, 258);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 294, 'location', 'Zambia', 'zm', 0, 292);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 295, 'location', 'Zimbabwe', 'zw', 0, 293);

# Countries - Composite
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 127, 'location', 'United Kingdom', 'gb', 1, 294);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`) VALUES (@sport, @season, 279, 'location', 'United States', 'us', 1, 295);
# - Makeup
#   - UK
INSERT INTO `FANTASY_SALCAP_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 127, 104);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 127, 116);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 127, 212);
INSERT INTO `FANTASY_SALCAP_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`) VALUES (@sport, @season, 127, 243);
#   - US
INSERT INTO `FANTASY_SALCAP_FANLEAGUES_COMPOSITE` (`sport`, `season`, `league_id`, `composite_id`)
  SELECT `sport`, `season`, 279, `league_id`
  FROM `FANTASY_SALCAP_FANLEAGUES`
  WHERE `sport` = @sport
  AND   `season` = @season
  AND   `extra_info` LIKE 'us-%'
  ORDER BY `league_id`;

# Teams (uses temporary table from above)
INSERT INTO `FANTASY_SALCAP_FANLEAGUES` (`sport`, `season`, `league_id`, `league_type`, `name`, `extra_info`, `composite`, `league_order`)
  SELECT @sport, @season, 297 + COUNT(`tmp_team_list_cp`.`team_id`), 'team', CONCAT(`TEAM`.`city`, ' ', `TEAM`.`franchise`), `TEAM`.`team_id`, 0, COUNT(`tmp_team_list_cp`.`team_id`) + 1
  FROM `tmp_team_list`
  JOIN `debearco_sports`.`SPORTS_NFL_TEAMS` AS `TEAM`
    ON (`TEAM`.`team_id` = `tmp_team_list`.`team_id`)
  LEFT JOIN `tmp_team_list_cp`
    ON (`tmp_team_list_cp`.`team_id` < `tmp_team_list`.`team_id`)
  GROUP BY `tmp_team_list`.`team_id`;

