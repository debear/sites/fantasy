##
## SGP
##
SET @sport := 'sgp';
SET @season := 2025;
SET @twitter_app := 'fantasy_records';

CALL records_setup_reset(@sport, @season, @twitter_app);

# 192 Points in a Season (Artem Laguta, 2021) // ind, total
# - (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 1, '192 Point Season', 'Select a rider each meeting and collect as most World Championship points as possible over the course of the season!', 'pts', 'ind', 'total', 'HITRATE_LEADERBOARD', NULL, 193, 192, 'Artem Laguta, 2021', '2025-04-23 12:00:00', '2025-05-03 17:00:00', '2025-05-31 16:55:00', '2025-09-17 00:00:00', '2025-10-15 00:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 1, 'One (1) selection can be made every Grand Prix. All World Championship points scored by that selection will be added to your season total.
If no selection is made for a Grand Prix, your team&#39;s total will neither increase nor reset to zero (0).
Selections will lock five (5) minutes prior to the advertised start time of the meeting.
An individual rider can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 1, 1, 'Pts/Heat', 'points-per-heat', 'Points scored per heat', 1, NULL, NULL),
  (@sport, @season, 1, 2, 'SF App', 'sf-app', 'Appearances in the Semi-Finals', 2, NULL, NULL),
  (@sport, @season, 1, 3, 'GF App', 'gf-app', 'Appearances in the Grand Final', 3, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 'Championship Totals', 'Some milestones to hit along the journey to the record.', 'score', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 1, 30, 1, 10),
  (@sport, @season, 1, 1, 2, 60, 2, 20),
  (@sport, @season, 1, 1, 3, 100, 3, 30),
  (@sport, @season, 1, 1, 4, 140, 4, 40),
  (@sport, @season, 1, 1, 5, 180, 5, 50);

##
## Game dates (storing as race day)
##
SELECT `round` INTO @v_final_meeting
FROM `debearco_sports`.`SPORTS_SGP_RACES`
WHERE `season` = @season
ORDER BY `race_time` DESC
LIMIT 1;

INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, `name`, `name_short`, `icon`, `icon_type`, `start_date`, `end_date`, `period_order`, `summarise`)
  SELECT `GAME`.`sport`,
         `BASE`.`season`,
         `BASE`.`round` AS `period_id`,
         `BASE`.`name_full` AS `name`,
         `BASE`.`name_short`,
         IF(`BASE`.`flag` LIKE '%-%', SUBSTRING(`BASE`.`flag`, 1, LOCATE('-', `BASE`.`flag`) - 1), `BASE`.`flag`) AS `icon`,
         'flag' AS `icon_type`,
         IF(`PREV`.`race_time` IS NULL OR DATE(`BASE`.`race_time`) > DATE(IFNULL(DATE_ADD(`PREV`.`race_time`, INTERVAL 3 DAY), `GAME`.`date_start`)),
           DATE(IFNULL(DATE_ADD(`PREV`.`race_time`, INTERVAL 3 DAY), `GAME`.`date_start`)),
           DATE(`BASE`.`race_time`)
         ) AS `start_date`,
         IF(DATE(`NEXT`.`race_time`) < DATE(IF(`NEXT`.`round_order` IS NOT NULL, DATE_ADD(`BASE`.`race_time`, INTERVAL 2 DAY), DATE_SUB(`GAME`.`date_end`, INTERVAL 1 SECOND))),
           DATE(`BASE`.`race_time`),
           DATE(IF(`NEXT`.`round_order` IS NOT NULL, DATE_ADD(`BASE`.`race_time`, INTERVAL 2 DAY), DATE_SUB(`GAME`.`date_end`, INTERVAL 1 SECOND)))
         ) AS `end_date`,
         `BASE`.`round_order` AS `period_order`,
         `BASE`.`round` <> @v_final_meeting AS `summarise` -- Not the final meeting
  FROM `debearco_sports`.`SPORTS_SGP_RACES` AS `BASE`
  LEFT JOIN `debearco_sports`.`SPORTS_SGP_RACES` AS `PREV`
    ON (`PREV`.`season` = `BASE`.`season`
    AND `PREV`.`round_order` = `BASE`.`round_order` - 1)
  LEFT JOIN `debearco_sports`.`SPORTS_SGP_RACES` AS `NEXT`
    ON (`NEXT`.`season` = `BASE`.`season`
    AND `NEXT`.`round_order` = `BASE`.`round_order` + 1)
  LEFT JOIN `FANTASY_RECORDS_GAMES` AS `GAME`
    ON (`GAME`.`sport` = @sport
    AND `GAME`.`season` = `BASE`.`season`
    AND `GAME`.`game_id` = 1)
  WHERE `BASE`.`season` = @season
  ORDER BY `BASE`.`round_order`;

##
## Twitter
##

# Upcoming games
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_upcoming'), 1, 'As riders start making their final preperations for the #SpeedwayGP season, #DeBearRecords is getting ready for a new SGP challenge.');

# Registration open
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 1, 'It&#39;s nearly time for tapes-up, so get your #DeBearRecords entry in and take on our #SpeedwayGP challenge. {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 2, 'Can you beat Artem Laguta&#39;s 192 #SpeedwayGP Points in a season? Join #DeBearRecords to find out. {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 3, 'Take on the #DeBearRecords #SpeedwayGP challenge and see if you can top 192 championship points in a season! {domain}/records');

# Game starting
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_starting'), 1, 'The new season starts tomorrow night. Is your #DeBearRecords #SpeedwayGP entry ready? {domain}/records');

# Registration closing
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing'), 1, 'Tomorrow is the last day to join the #DeBearRecords #SpeedwayGP challenge. Is your entry in? {domain}/records');

# Weekly leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 1, 'After the {period_name}, our #DeBearRecords SGP Points leader has {score}pt{score_s}. See the leaderboard {domain}/records/{slug} and check out how you&#39;re getting on.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 2, 'The #DeBearRecords SGP Points leader is on {score}pt{score_s} after the {period_name}. Check out {domain}/records/{slug} to see how your entry compares.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 3, 'After the {period_num_ord} meeting of the season, the #DeBearRecords SGP Points leader is on {score}pt{score_s}. Check out {domain}/records/{slug} for the leaderboard.');

# Record beaten (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_1'), 1, 'Congratulations to {owner} for scoring {score}pts and beating the #SpeedwayGP record of 192pts in a season! #DeBearRecords {domain}/records/{slug}');

# End of season leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_1'), 1, 'Congratulations to {owner} for topping this year&#39;s #DeBearRecords SGP Points total with {score}pt{score_s}. Head to {domain}/records/{slug} for the final leaderboard.');

## Schedule
SET @date_start := '2025-05-03';
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_open`, INTERVAL 1 DAY)) AS `date`, 'season_soon' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 7 DAY) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 5 DAY) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 3 DAY) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 1 DAY) AS `date`, 'season_tomorrow' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_close`, INTERVAL 1 DAY)) AS `date`, 'reg_closing' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_end`, INTERVAL 1 DAY)) AS `date`, 'season_ended' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;

##
## Seed with automated players
##
CALL records_autouser_join(@sport, @season, 1, 8, 15); # Between 8% and 15% of automated users

##
## Admin
##
CALL records_setup_profiles(@sport, @season);
CALL records_setup_order();
