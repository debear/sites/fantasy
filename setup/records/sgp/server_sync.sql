#
# Record Breakers scoring prep
#
SET @sync_app := 'fantasy_records_sgp_selections';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy Record Breakers sgp-2025 (Down)', 'live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Games (ID 3 archived)
    (@sync_app,  1, '__DIR__ FANTASY_RECORDS_GAMES',                    10, 'database'),
    (@sync_app,  2, '__DIR__ FANTASY_RECORDS_GAMES_POSITIONS',          20, 'database'),
    (@sync_app,  4, '__DIR__ FANTASY_COMMON_PERIODS_DATES',             30, 'database'),
    (@sync_app,  5, '__DIR__ FANTASY_COMMON_SELECTIONS_MAPPED',         40, 'database'),
    (@sync_app,  6, '__DIR__ FANTASY_RECORDS_GAMES_PERIODS_SUMMARY',    50, 'database'),
    -- Entries
    (@sync_app,  7, '__DIR__ FANTASY_AUTOMATED_USER',                   60, 'database'),
    (@sync_app,  8, '__DIR__ FANTASY_RECORDS_ENTRIES',                  70, 'database'),
    (@sync_app,  9, '__DIR__ FANTASY_RECORDS_ENTRIES_SCORES',           80, 'database'),
    -- Selections
    (@sync_app, 18, '__DIR__ FANTASY_RECORDS_ENTRIES_BYPERIOD',         90, 'database'),
    -- Profiles
    (@sync_app, 12, '__DIR__ FANTASY_PROFILE_RECORD_TEAMS',            100, 'database'),
    -- Social Media / Comms
    (@sync_app, 19, '__DIR__ FANTASY_RECORDS_SETUP_EMAIL',             110, 'database'),
    (@sync_app, 13, '__DIR__ COMMS_TWITTER_TEMPLATE',                  120, 'database'),
    (@sync_app, 14, '__DIR__ COMMS_TWITTER',                           130, 'database'),
    (@sync_app, 15, '__DIR__ COMMS_TWITTER_SYNC',                      140, 'database'),
    (@sync_app, 16, '__DIR__ COMMS_TWITTER_MEDIA',                     150, 'database'),
    (@sync_app, 17, '__DIR__ COMMS_TWITTER_SYNC_MEDIA',                160, 'database');

INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Games (ID 3 archived)
    (@sync_app,  1, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES',                   'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  2, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_POSITIONS',         'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  4, 'debearco_fantasy', 'FANTASY_COMMON_PERIODS_DATES',            'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  5, 'debearco_fantasy', 'FANTASY_COMMON_SELECTIONS_MAPPED',        'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  6, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_PERIODS_SUMMARY',   'sport = ''sgp'' AND season = ''2025'''),
    -- Entries
    (@sync_app,  7, 'debearco_fantasy', 'FANTASY_AUTOMATED_USER',                  ''),
    (@sync_app,  8, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES',                 ''),
    (@sync_app,  9, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_SCORES',          'sport = ''sgp'' AND season = ''2025'''),
    -- Selections (ID 10 & 11 archived)
    (@sync_app, 18, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_BYPERIOD',        'sport = ''sgp'' AND season = ''2025'''),
    -- Profiles
    (@sync_app, 12, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_TEAMS',            'sport = ''sgp'' AND season = ''2025'''),
    -- Social Media / Comms
    (@sync_app, 19, 'debearco_fantasy', 'FANTASY_RECORDS_SETUP_EMAIL',             'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 13, 'debearco_common',  'COMMS_TWITTER_TEMPLATE',                  'app = ''fantasy_records'' AND tweet_type LIKE CONCAT(''sgp\_2025%'')'),
    (@sync_app, 14, 'debearco_common',  'COMMS_TWITTER',                           'app = ''fantasy_records'' AND tweet_type LIKE CONCAT(''sgp\_2025%'')'),
    (@sync_app, 15, 'debearco_common',  'COMMS_TWITTER_SYNC',                      'app = ''fantasy_records'' AND tweet_type LIKE CONCAT(''sgp\_2025%'')'),
    (@sync_app, 16, 'debearco_common',  'COMMS_TWITTER_MEDIA',                     'app = ''fantasy_records'' AND media_unique_ref LIKE CONCAT(''sgp-2025%'')'),
    (@sync_app, 17, 'debearco_common',  'COMMS_TWITTER_SYNC_MEDIA',                'app = ''fantasy_records'' AND media_unique_ref LIKE CONCAT(''sgp-2025%'')');

#
# Record Breakers scoring upload
#
SET @sync_app := 'fantasy_records_sgp_scoring';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy Record Breakers sgp-2025 (Up)', 'dev,data', 'data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Games
    (@sync_app,  1, '__DIR__ FANTASY_RECORDS_GAMES',                    10, 'database'),
    (@sync_app,  2, '__DIR__ FANTASY_RECORDS_GAMES_PERIODS_SUMMARY',    20, 'database'),
    (@sync_app, 17, '__DIR__ FANTASY_COMMON_PERIODS_DATES',             30, 'database'),
    -- Selections
    (@sync_app,  3, '__DIR__ FANTASY_RECORDS_SELECTIONS',               40, 'database'),
    (@sync_app,  4, '__DIR__ FANTASY_RECORDS_SELECTIONS_SCORING',       50, 'database'),
    (@sync_app,  5, '__DIR__ FANTASY_RECORDS_SELECTIONS_STATS',         60, 'database'),
    -- Entry Selections and Entries (without over-writing changes to future periods)
    (@sync_app,  6, '__DIR__ FANTASY_AUTOMATED_USER',                   70, 'database'),
    (@sync_app,  7, '__DIR__ FANTASY_RECORDS_ENTRIES',                  80, 'database'),
    (@sync_app,  8, '__DIR__ Entries and Entry Selections',             90, 'script'),
    (@sync_app, 18, '__DIR__ FANTASY_RECORDS_ENTRIES_BUSTERS',         100, 'database'),
    (@sync_app, 15, '__DIR__ FANTASY_RECORDS_ENTRIES_DROPOFF',         110, 'database'),
    -- Profiles
    (@sync_app,  9, '__DIR__ FANTASY_PROFILE_RECORD_GAMES',            120, 'database'),
    (@sync_app, 10, '__DIR__ FANTASY_PROFILE_RECORD_TEAMS',            130, 'database'),
    -- Admin (Skipping until required)
    -- (@sync_app, 11, '__DIR__ USER_ACTIVITY_LOG',                    140, 'database'),
    -- Social Media
    (@sync_app, 12, '__DIR__ COMMS_TWITTER_SYNC',                      150, 'database'),
    (@sync_app, 16, '__DIR__ COMMS_TWITTER_SYNC_MEDIA',                160, 'database'),
    (@sync_app, 13, '__DIR__ Open Graph Images',                       170, 'script'),
    (@sync_app, 14, '__DIR__ FANTASY_RECORDS_GAMES_META',              180, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Games
    (@sync_app,  1, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES',                 'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  2, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_PERIODS_SUMMARY', 'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 17, 'debearco_fantasy', 'FANTASY_COMMON_PERIODS_DATES',          'sport = ''sgp'' AND season = ''2025'''),
    -- Selections
    (@sync_app,  3, 'debearco_fantasy', 'FANTASY_RECORDS_SELECTIONS',            'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  4, 'debearco_fantasy', 'FANTASY_RECORDS_SELECTIONS_SCORING',    'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  5, 'debearco_fantasy', 'FANTASY_RECORDS_SELECTIONS_STATS',      'sport = ''sgp'' AND season = ''2025'''),
    -- Entry Selections and Entries
    (@sync_app,  6, 'debearco_fantasy', 'FANTASY_AUTOMATED_USER',                ''),
    (@sync_app,  7, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES',               ''),
    (@sync_app, 18, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_BUSTERS',       'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 15, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_DROPOFF',       'sport = ''sgp'' AND season = ''2025'''),
    -- Profiles
    (@sync_app,  9, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_GAMES',          'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 10, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_TEAMS',          'sport = ''sgp'' AND season = ''2025'''),
    -- Admin (Skipping until required)
    -- (@sync_app, 11, 'debearco_admin',   'USER_ACTIVITY_LOG',                     'type BETWEEN 40 AND 49'),
    -- Social Media
    (@sync_app, 12, 'debearco_common',  'COMMS_TWITTER_SYNC',                    'app = ''fantasy_records'' AND tweet_type LIKE CONCAT(''sgp\_2025%'')'),
    (@sync_app, 16, 'debearco_common',  'COMMS_TWITTER_SYNC_MEDIA',              'app = ''fantasy_records'' AND media_unique_ref LIKE CONCAT(''sgp-2025%'')'),
    (@sync_app, 14, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_META',            'sport = ''sgp'' AND season = ''2025''');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args) VALUES
    (@sync_app,  8, '/var/www/debear/sites/fantasy/data/records/upload_entries.pl', '"sgp" "2025" __REMOTE__'),
    (@sync_app, 13, '/var/www/debear/sites/fantasy/data/records/upload_og_img.sh', '"sgp" "2025" __REMOTE__');

#
# Record Breakers dev sync
#
SET @sync_app := 'fantasy_records_sgp_dev';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy Record Breakers sgp-2025 (Dev)', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Games
    (@sync_app,  1, '__DIR__ FANTASY_RECORDS_GAMES',                    10, 'database'),
    (@sync_app,  2, '__DIR__ FANTASY_RECORDS_GAMES_PERIODS_SUMMARY',    20, 'database'),
    (@sync_app, 19, '__DIR__ FANTASY_COMMON_PERIODS_DATES',             30, 'database'),
    -- Selections
    (@sync_app,  3, '__DIR__ FANTASY_RECORDS_SELECTIONS',               40, 'database'),
    (@sync_app,  4, '__DIR__ FANTASY_RECORDS_SELECTIONS_SCORING',       50, 'database'),
    (@sync_app,  5, '__DIR__ FANTASY_RECORDS_SELECTIONS_STATS',         60, 'database'),
    -- Entry Selections and Entries (without over-writing changes to future periods)
    (@sync_app,  6, '__DIR__ FANTASY_AUTOMATED_USER',                   70, 'database'),
    (@sync_app,  7, '__DIR__ FANTASY_RECORDS_ENTRIES',                  80, 'database'),
    (@sync_app, 18, '__DIR__ FANTASY_RECORDS_ENTRIES_BYPERIOD',         90, 'database'),
    (@sync_app,  9, '__DIR__ FANTASY_RECORDS_ENTRIES_SCORES',          100, 'database'),
    (@sync_app, 21, '__DIR__ FANTASY_RECORDS_ENTRIES_BUSTERS',         110, 'database'),
    (@sync_app, 16, '__DIR__ FANTASY_RECORDS_ENTRIES_DROPOFF',         120, 'database'),
    -- Profiles
    (@sync_app, 11, '__DIR__ FANTASY_PROFILE_RECORD_GAMES',            130, 'database'),
    (@sync_app, 12, '__DIR__ FANTASY_PROFILE_RECORD_TEAMS',            140, 'database'),
    -- Admin (Skipping until required)
    -- (@sync_app, 13, '__DIR__ USER_ACTIVITY_LOG',                    150, 'database'),
    -- Social Media / Comms
    (@sync_app, 20, '__DIR__ FANTASY_RECORDS_SETUP_EMAIL',             160, 'database'),
    (@sync_app, 14, '__DIR__ COMMS_TWITTER_SYNC',                      170, 'database'),
    (@sync_app, 17, '__DIR__ COMMS_TWITTER_SYNC_MEDIA',                180, 'database'),
    (@sync_app, 15, '__DIR__ FANTASY_RECORDS_GAMES_META',              190, 'database'),
    (@sync_app, 22, '__DIR__ Open Graph Images',                       200, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Games
    (@sync_app,  1, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES',                   'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  2, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_PERIODS_SUMMARY',   'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 19, 'debearco_fantasy', 'FANTASY_COMMON_PERIODS_DATES',            'sport = ''sgp'' AND season = ''2025'''),
    -- Selections
    (@sync_app,  3, 'debearco_fantasy', 'FANTASY_RECORDS_SELECTIONS',              'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  4, 'debearco_fantasy', 'FANTASY_RECORDS_SELECTIONS_SCORING',      'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  5, 'debearco_fantasy', 'FANTASY_RECORDS_SELECTIONS_STATS',        'sport = ''sgp'' AND season = ''2025'''),
    -- Entry Selections and Entries (IDs 8 & 10 archived)
    (@sync_app,  6, 'debearco_fantasy', 'FANTASY_AUTOMATED_USER',                  ''),
    (@sync_app,  7, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES',                 ''),
    (@sync_app, 18, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_BYPERIOD',        'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  9, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_SCORES',          'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 21, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_BUSTERS',         'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 16, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_DROPOFF',         'sport = ''sgp'' AND season = ''2025'''),
    -- Profiles
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_GAMES',            'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 12, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_TEAMS',            'sport = ''sgp'' AND season = ''2025'''),
    -- Admin (Skipping until required)
    -- (@sync_app, 13, 'debearco_admin',   'USER_ACTIVITY_LOG',                       'type BETWEEN 40 AND 49'),
    -- Social Media / Comms
    (@sync_app, 20, 'debearco_fantasy', 'FANTASY_RECORDS_SETUP_EMAIL',             'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 14, 'debearco_common',  'COMMS_TWITTER_SYNC',                      'app = ''fantasy_records'' AND tweet_type LIKE CONCAT(''sgp\_2025%'')'),
    (@sync_app, 17, 'debearco_common',  'COMMS_TWITTER_SYNC_MEDIA',                'app = ''fantasy_records'' AND media_unique_ref LIKE CONCAT(''sgp-2025%'')'),
    (@sync_app, 15, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_META',              'sport = ''sgp'' AND season = ''2025''');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt) VALUES
    (@sync_app, 22, '/var/www/debear/sites/cdn/htdocs/fantasy/records/og/sgp-2025-*', 'debear/sites/cdn/htdocs/fantasy/records/og/', NULL);

#
# Record Breakers game setup
#
SET @sync_app := 'fantasy_records_sgp_setup';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy Record Breakers sgp-2025 (Setup)', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Games (ID 5 archived)
    (@sync_app,  1, '__DIR__ FANTASY_RECORDS_GAMES',                             10, 'database'),
    (@sync_app,  2, '__DIR__ FANTASY_RECORDS_GAMES_RULES',                       20, 'database'),
    (@sync_app,  3, '__DIR__ FANTASY_RECORDS_GAMES_POSITIONS',                   30, 'database'),
    (@sync_app,  4, '__DIR__ FANTASY_RECORDS_GAMES_STATS',                       40, 'database'),
    (@sync_app, 15, '__DIR__ FANTASY_RECORDS_GAMES_ACHIEVEMENTS',                50, 'database'),
    (@sync_app, 16, '__DIR__ FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS',         60, 'database'),
    (@sync_app,  6, '__DIR__ FANTASY_COMMON_PERIODS_DATES',                      70, 'database'),
    (@sync_app,  7, '__DIR__ FANTASY_COMMON_SELECTIONS_MAPPED',                  80, 'database'),
    -- Social Media
    (@sync_app,  8, '__DIR__ FANTASY_RECORDS_GAMES_META',                        90, 'database'),
    (@sync_app,  9, '__DIR__ Open Graph Images',                                100, 'file'),
    (@sync_app, 10, '__DIR__ COMMS_TWITTER_TEMPLATE',                           110, 'database'),
    (@sync_app, 14, '__DIR__ FANTASY_RECORDS_SETUP_TWITTER',                    120, 'database'),
    -- Automated Users and Entries
    (@sync_app, 11, '__DIR__ FANTASY_AUTOMATED_USER',                           130, 'database'),
    (@sync_app, 12, '__DIR__ FANTASY_RECORDS_ENTRIES',                          140, 'database'),
    (@sync_app, 13, '__DIR__ FANTASY_RECORDS_ENTRIES_SCORES',                   150, 'database'),
    -- Profiles
    (@sync_app, 17, '__DIR__ FANTASY_PROFILE_RECORD_GAMES',                     160, 'database'),
    (@sync_app, 18, '__DIR__ FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS',        170, 'database'),
    (@sync_app, 19, '__DIR__ FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS_LEVELS', 180, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Games (ID 5 archived)
    (@sync_app,  1, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES',                            'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  2, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_RULES',                      'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  3, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_POSITIONS',                  'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  4, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_STATS',                      'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 15, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_ACHIEVEMENTS',               'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 16, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS',        'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  6, 'debearco_fantasy', 'FANTASY_COMMON_PERIODS_DATES',                     'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app,  7, 'debearco_fantasy', 'FANTASY_COMMON_SELECTIONS_MAPPED',                 'sport = ''sgp'' AND season = ''2025'''),
    -- Social Media
    (@sync_app,  8, 'debearco_fantasy', 'FANTASY_RECORDS_GAMES_META',                       'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 10, 'debearco_common',  'COMMS_TWITTER_TEMPLATE',                           'app = ''fantasy_records'' AND tweet_type LIKE CONCAT(''sgp\_2025%'')'),
    (@sync_app, 14, 'debearco_fantasy', 'FANTASY_RECORDS_SETUP_TWITTER',                    'sport = ''sgp'' AND season = ''2025'''),
    -- Automated Users and Entries
    (@sync_app, 11, 'debearco_fantasy', 'FANTASY_AUTOMATED_USER',                           ''),
    (@sync_app, 12, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES',                          'user_id > 60000'),
    (@sync_app, 13, 'debearco_fantasy', 'FANTASY_RECORDS_ENTRIES_SCORES',                   'sport = ''sgp'' AND season = ''2025'' AND user_id > 60000'),
    -- Profiles
    (@sync_app, 17, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_GAMES',                     'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 18, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS',        'sport = ''sgp'' AND season = ''2025'''),
    (@sync_app, 19, 'debearco_fantasy', 'FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS_LEVELS', 'sport = ''sgp'' AND season = ''2025''');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt) VALUES
    (@sync_app, 9, '/var/www/debear/sites/cdn/htdocs/fantasy/records/og/sgp-2025-*', 'debear/sites/cdn/htdocs/fantasy/records/og/', NULL);
