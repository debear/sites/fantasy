#!/bin/bash
# Import a game and perform the relevant post-processing

dir_base=$(realpath $(dirname $0))
dir_skel=$(realpath $dir_base/../../../_debear)
dir_helpers=$(realpath $dir_base/../../tests/_scripts)
source $dir_helpers/helpers.sh

# Load and validate the game arguments
sport="$1"
season="$2"
if [ -z "$sport" ] || [ -z "$season" ] || [ ! -e "$dir_base/$sport/$season.sql" ]; then
    echo -e "Invalid arguments.\n$0 sport season" >&2
    exit 1
fi
display_title "Importing $sport/$season.sql"; echo

# Additional config
dir_mysql_conf=$(realpath $dir_base/../../../../etc/mysql)
cmd_mysql="/usr/bin/mysql --defaults-extra-file=$dir_mysql_conf/debearco_sysadmin.conf --table"
db_fantasy=debearco_fantasy

# Inform the user if an error occurred, and reflect the exit code we encountered
function handle_error() {
  retcode="$1"
  if [ $retcode -gt 0 ]; then
    display_error "An error occurred performing this step - aborting the rest of the import."
    exit $retcode
  fi
}

# Step 1: Import
display_section "Importing:"
$cmd_mysql $db_fantasy <$dir_base/$sport/$season.sql
handle_error $?
display_info "Date verification:"
# Date verification
echo "SELECT CONCAT(GAME.sport, '-', GAME.season, '-', GAME.game_id) AS game_ref,
       DATEDIFF(MIN(DATES.start_date), DATE(GAME.date_open)) AS days_open,
       IF(DATE(GAME.date_start) = MIN(DATES.start_date), '[X]', '[ ]') AS check_start,
       DATEDIFF(DATE(GAME.date_close), MIN(DATES.start_date)) AS days_close_in,
       DATEDIFF(MAX(DATES.end_date), DATE(GAME.date_close)) AS days_close_togo,
       IF(DATE(DATE_SUB(GAME.date_end, INTERVAL 1 DAY)) = MAX(DATES.end_date), '[X]', '[ ]') AS check_end,
       DATEDIFF(DATE(GAME.date_complete), MAX(DATES.end_date)) AS days_complete
FROM FANTASY_RECORDS_GAMES AS GAME
LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
  ON (DATES.sport = GAME.sport
  AND DATES.season = GAME.season)
WHERE GAME.sport = '$sport'
AND   GAME.season = '$season'
GROUP BY GAME.sport, GAME.season, GAME.game_id
ORDER BY GAME.sport, GAME.season, GAME.game_id;" | $cmd_mysql $db_fantasy
# Social Media dates
display_info "Social Media dates:"
echo "SELECT CONCAT(GAME.sport, '-', GAME.season, '-', GAME.game_id) AS game_ref,
       GROUP_CONCAT(IF(TWITTER_SCHED.tweet_type = 'season_soon', TWITTER_SCHED.date, NULL) ORDER BY TWITTER_SCHED.date SEPARATOR ', ') AS season_soon,
       DATE(GAME.date_open) AS date_open,
       GROUP_CONCAT(IF(TWITTER_SCHED.tweet_type = 'reg_opening', TWITTER_SCHED.date, NULL) ORDER BY TWITTER_SCHED.date SEPARATOR ', ') AS reg_opening,
       GROUP_CONCAT(IF(TWITTER_SCHED.tweet_type = 'season_tomorrow', TWITTER_SCHED.date, NULL) ORDER BY TWITTER_SCHED.date SEPARATOR ', ') AS season_tomorrow,
       DATE(GAME.date_start) AS date_start,
       GROUP_CONCAT(IF(TWITTER_SCHED.tweet_type = 'reg_closing', TWITTER_SCHED.date, NULL) ORDER BY TWITTER_SCHED.date SEPARATOR ', ') AS reg_closing,
       DATE(date_close) AS date_close,
       DATE(GAME.date_end) AS date_end,
       GROUP_CONCAT(IF(TWITTER_SCHED.tweet_type = 'season_ended', TWITTER_SCHED.date, NULL) ORDER BY TWITTER_SCHED.date SEPARATOR ', ') AS season_ended
FROM FANTASY_RECORDS_GAMES AS GAME
LEFT JOIN FANTASY_RECORDS_SETUP_TWITTER AS TWITTER_SCHED
  ON (TWITTER_SCHED.sport = GAME.sport
  AND TWITTER_SCHED.season = GAME.season
  AND TWITTER_SCHED.game_id = GAME.game_id)
WHERE GAME.sport = '$sport'
AND   GAME.season = '$season'
GROUP BY GAME.sport, GAME.season, GAME.game_id
ORDER BY GAME.sport, GAME.season, GAME.game_id;" | $cmd_mysql $db_fantasy
echo

# Confirm user is happy to proceed
display_info -n "Are you happy to proceed with the remaining steps in importing the game? [yN]: "
response='-'
while [ $(echo "$response" | grep -Pci '^[yn]?$') -eq 0 ]; do
  read response
  if [ $(echo "$response" | grep -Pci '^[yn]?$') -eq 0 ]; then
    # Invalid response, ask user to try again
    display_red -n "'$response' in not a valid response, please try again."
    display_info -n " Proceed? [yN]: "
  elif [ $(echo "$response" | grep -Pci '^n?$') -eq 1 ]; then
    # User has selected not to continue
    display_yellow 'Exiting, no more steps will be performed - beware the imported data has not been removed from the database.'
    exit
  fi
done
echo

# Step 2: Open Graph image
display_section "Open Graph image(s):"
cd $dir_skel
php artisan debear:fantasy:records-og-preview --sport="$sport" --season="$season"
handle_error $?
cd $dir_base
echo

# Step 3: Update the server_sync SQL
display_section "Server Sync definition:"
$dir_base/server_sync.sh $sport $season
handle_error $?
echo

# Step 4: Upload (manual)
display_section "Upload:"
echo "Once happy, run the following commands to upload the new game to live:"
display_yellow -n '$ '; display_info "server-sync --resync-defs"
display_yellow -n '$ '; display_info "server-sync fantasy_records_${sport}_setup live --up"
