##
## NHL
##
SET @sport := 'nhl';
SET @season := 2024;
SET @twitter_app := 'fantasy_records';

CALL records_setup_reset(@sport, @season, @twitter_app);

# 50 in 50 // ind, total, limited period
# - Richard: https://s-media-cache-ak0.pinimg.com/originals/12/99/60/12996079970f47152329036e48dc96d6.jpg,
# - Bossy: http://images.huffingtonpost.com/2012-08-27-Bossy_51427991.jpg
# - Gretzky: http://www.hockeydb.com/ihdb/photos/wayne-gretzky-1996-40.jpg
# - Lemieux: http://www.hockeydb.com/ihdb/photos/mario-lemieux-1996-50.jpg
# - Hull: http://www.hockeydb.com/ihdb/photos/brett-hull-1998-36.jpg
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 1, '50 in 50', 'The gold-standard in NHL goal scoring is the 50 goals in 50 games. First achieved in 1944-45 it saw a boon during the high scoring 1980s, but no individual player has come close since the mid 1990s!', '50in50', 'ind', 'total', 0, 1, 1, 50, 50, NULL, 'Maurice Richard, 1944-45; Mike Bossy, 1980-81; Wayne Gretzky, 1981-82, 1983-84, 1984-85; Mario Lemieux, 1988-89; Brett Hull, 1990-91, 1991-92', '2024-09-24 17:00:00', '2024-10-04 17:00:00', '2024-12-31 17:00:00', '2025-04-18 17:00:00', '2025-04-30 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 1, 'One (1) selection can be made every day. All Goals scored by that player will be added to your season total.
A team&#39;s &quot;season total&quot; is deemed to be the number of goals scored by the most recent fifty (50) selections who were credited with a Game Played.
If no selection is made for that day, your team&#39;s total count will not change and will not contribute towards the &quot;most recent fifty (50) selections&quot;.
Unlike the real-life version of this record, the attempt can commence at any point in the season &ndash; it does not have to begin at the start of the season.
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual player can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 1, 'C', 10),
  (@sport, @season, 1, 'LW', 20),
  (@sport, @season, 1, 'RW', 30),
  (@sport, @season, 1, 'D', 40);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 1, 1, 'Goals', 'goals', 'Total Goals scored this season', 1, NULL, NULL),
  (@sport, @season, 1, 2, 'Goals/Gm', 'goals-per-game', 'Average Goals scored per game played', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 'Goal Totals', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 1, 2, 'Multi-Goal Performances', 'Getting one goal is great for the total. Imagine how good a multi-goal game would be??', 'period', 20),
  (@sport, @season, 1, 3, 'Opponents', 'Spread the misery, and target a variety of opponents.', 'opponent', 30),
  (@sport, @season, 1, 4, 'Selection Usage', 'Whilst still choosing a player who pots a goal, make the not-so-obvious selection.', 'sel_usage', 40);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 1, 5, 1, 10),
  (@sport, @season, 1, 1, 2, 10, 2, 20),
  (@sport, @season, 1, 1, 3, 20, 3, 30),
  (@sport, @season, 1, 1, 4, 30, 4, 40),
  (@sport, @season, 1, 1, 5, 40, 5, 50),
  (@sport, @season, 1, 2, 2, 2, 6, 10),
  (@sport, @season, 1, 2, 3, 3, 7, 20),
  (@sport, @season, 1, 2, 5, 4, 8, 30),
  (@sport, @season, 1, 3, 1, 3, 9, 10),
  (@sport, @season, 1, 3, 2, 5, 10, 20),
  (@sport, @season, 1, 3, 3, 10, 11, 30),
  (@sport, @season, 1, 3, 4, 20, 12, 40),
  (@sport, @season, 1, 3, 5, 32, 13, 50),
  (@sport, @season, 1, 4, 2, 3, 14, 10),
  (@sport, @season, 1, 4, 3, 2, 15, 20),
  (@sport, @season, 1, 4, 5, 1, 16, 30);

# 51 Game Point Streak (Wayne Gretzky, 1983-84) // ind, streak
# - http://www.hockeydb.com/ihdb/photos/wayne-gretzky-1996-40.jpg
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 2, '51 Game Point Streak', 'One of the many Gretzky records where he appears multiple times on the leaderboard, continue picking players who score a point and repeat with the next game. Simple!', 'pts', 'ind', 'streak', 0, 1, 1, NULL, 52, 51, 'Wayne Gretzky, 1983-84', '2024-09-24 17:00:00', '2024-10-04 17:00:00', '2024-12-31 17:00:00', '2025-04-18 17:00:00', '2025-04-30 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 2, 'One (1) selection can be made every day. If that player scores at least one point (goal or assist), your streak will increase by one (1). If he does not, it will reset to zero (0).
If the selected player is not credited with a Game Played, your team&#39;s streak will stay as-is and neither be extended nor reset to zero (0).
If no player is selected for that day, your team&#39;s streak will stay as-is and neither be extended nor reset to zero (0).
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual player can be selected multiple times during a streak.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 2, 'C', 10),
  (@sport, @season, 2, 'LW', 20),
  (@sport, @season, 2, 'RW', 30),
  (@sport, @season, 2, 'D', 40);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 2, 1, 'Points', 'points', 'Total points (goals and assists) scored this season', 1, NULL, NULL),
  (@sport, @season, 2, 2, 'Points/Gm', 'points-per-game', 'Average points (goals and assists) per game played', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 'Points Streak', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 2, 2, 'Opponents', 'Spread the misery, and target a variety of opponents.', 'opponent', 20),
  (@sport, @season, 2, 3, 'Selection Usage', 'Whilst still choosing a player who nets a point, make the not-so-obvious selection.', 'sel_usage', 30);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 1, 3, 1, 10),
  (@sport, @season, 2, 1, 2, 5, 2, 20),
  (@sport, @season, 2, 1, 3, 10, 3, 30),
  (@sport, @season, 2, 1, 4, 20, 4, 40),
  (@sport, @season, 2, 1, 5, 40, 5, 50),
  (@sport, @season, 2, 2, 1, 3, 6, 10),
  (@sport, @season, 2, 2, 2, 5, 7, 20),
  (@sport, @season, 2, 2, 3, 10, 8, 30),
  (@sport, @season, 2, 2, 4, 20, 9, 40),
  (@sport, @season, 2, 2, 5, 32, 10, 50),
  (@sport, @season, 2, 3, 2, 3, 11, 10),
  (@sport, @season, 2, 3, 3, 2, 12, 20),
  (@sport, @season, 2, 3, 5, 1, 13, 30);

# 17 Game Winning Streak (Pittsburgh Penguins, 1992-93) // team, streak
# - https://content.sportslogos.net/logos/1/24/full/pittsburgh_penguins_logo_primary_19935752.png
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 3, '17 Game Winning Streak', 'A team with Lemieux, Jagr and Barrasso just kept winning. Channel your inner-mullet to keep picking winners.', 'win', 'team', 'streak', 0, 1, 1, NULL, 18, 17, 'Pittsburgh Penguins, 1992-93', '2024-09-24 17:00:00', '2024-10-04 17:00:00', '2024-12-31 17:00:00', '2025-04-18 17:00:00', '2025-04-30 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 3, 'One (1) selection can be made every day. If that team wins your streak increases by one (1) otherwise it will reset to zero (0).
A win can come in any scenario &ndash; regulation, overtime and shootout are all considered equal.
If no selection is made for that day, your streak will stay as-is and neither be extended nor reset to zero (0).
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual team can be selected multiple times during a streak.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 3, 1, 'Record', 'record', 'Overall Win/Loss record', 1, NULL, NULL),
  (@sport, @season, 3, 2, 'Last 10', 'recent', 'Win/Loss record over the Last 10 games', 2, NULL, NULL),
  (@sport, @season, 3, 3, 'Home/Road', 'home-road', 'Win/Loss record at Home/on the Road', 3, NULL, NULL),
  (@sport, @season, 3, 4, 'Odds', 'odds', 'Bookmaker odds to win the game', 4, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 'Win Streak', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 3, 2, 'Opponents', 'Spread the misery, and target a variety of opponents.', 'opponent', 20),
  (@sport, @season, 3, 3, 'Selection Usage', 'Whilst still choosing a winning team, make the not-so-obvious selection.', 'sel_usage', 30);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 1, 3, 1, 10),
  (@sport, @season, 3, 1, 2, 5, 2, 20),
  (@sport, @season, 3, 1, 3, 7, 3, 30),
  (@sport, @season, 3, 1, 4, 10, 4, 40),
  (@sport, @season, 3, 1, 5, 15, 5, 50),
  (@sport, @season, 3, 2, 1, 3, 6, 10),
  (@sport, @season, 3, 2, 2, 5, 7, 20),
  (@sport, @season, 3, 2, 3, 10, 8, 30),
  (@sport, @season, 3, 2, 4, 20, 9, 40),
  (@sport, @season, 3, 2, 5, 32, 10, 50),
  (@sport, @season, 3, 3, 2, 3, 11, 10),
  (@sport, @season, 3, 3, 3, 2, 12, 20),
  (@sport, @season, 3, 3, 5, 1, 13, 30);

##
## Game dates
##
# Get the game dates in an order
DROP TEMPORARY TABLE IF EXISTS `tmp_sched_dates`;
CREATE TEMPORARY TABLE `tmp_sched_dates` (
  `period_id` TINYINT UNSIGNED,
  `game_date` DATE,
  PRIMARY KEY (`game_date`)
) ENGINE = MEMORY
  SELECT DISTINCT NULL AS `period_id`, `game_date`
  FROM `debearco_sports`.`SPORTS_NHL_SCHEDULE`
  WHERE `season` = @season
  AND   `game_type` = 'regular'
  ORDER BY `game_date`;

CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpB');
INSERT INTO `tmp_sched_dates` (`game_date`, `period_id`)
  SELECT `A`.`game_date`, COUNT(DISTINCT `B`.`game_date`) + 1 AS `period_id`
  FROM `tmp_sched_dates_cpA` AS `A`
  LEFT JOIN `tmp_sched_dates_cpB` AS `B`
    ON (`B`.`game_date` < `A`.`game_date`)
  GROUP BY `A`.`game_date`
ON DUPLICATE KEY UPDATE `period_id` = VALUES(`period_id`);

CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, `name`, `name_short`, `icon`, `icon_type`, `start_date`, `end_date`, `period_order`, `summarise`)
  SELECT @sport AS `sport`, @season AS `season`,
         `GAME_DATE`.`period_id`,
         NULL AS `name`, NULL AS `name_short`, NULL as `icon`, NULL AS `icon_type`,
         IFNULL(DATE_ADD(`PREV_DATE`.`game_date`, INTERVAL 1 DAY), `GAME_DATE`.`game_date`) AS `start_date`,
         `GAME_DATE`.`game_date` AS `end_date`,
         `GAME_DATE`.`period_id` AS `period_order`,
         0 AS `summarise` # This will be calculated separately
  FROM `tmp_sched_dates` AS `GAME_DATE`
  LEFT JOIN `tmp_sched_dates_cpA` AS `PREV_DATE`
    ON (`PREV_DATE`.`period_id` = `GAME_DATE`.`period_id` - 1)
  ORDER BY `GAME_DATE`.`period_id`;

# Process on a Wednesday, skipping the very early part of the season and the final week
SELECT `end_date` INTO @v_final_week_cutoff
FROM `FANTASY_COMMON_PERIODS_DATES`
WHERE `sport` = @sport
AND   `season` = @season
ORDER BY `end_date` DESC
LIMIT 3, 1;

UPDATE `FANTASY_COMMON_PERIODS_DATES`
SET `summarise` = (
      `period_order` >= 7
  AND `end_date` <= @v_final_week_cutoff
  AND IF(DAYOFWEEK(`start_date`) <= DAYOFWEEK(`end_date`),
        3 BETWEEN DAYOFWEEK(`start_date`) AND DAYOFWEEK(`end_date`),
            3 BETWEEN DAYOFWEEK(`start_date`) AND (DAYOFWEEK(`end_date`) + 7)
        OR 10 BETWEEN DAYOFWEEK(`start_date`) AND (DAYOFWEEK(`end_date`) + 7)
      ))
WHERE `sport` = @sport
AND   `season` = @season;

##
## Team information
##
DROP TEMPORARY TABLE IF EXISTS `tmp_team_list`;
CREATE TEMPORARY TABLE `tmp_team_list` (
  `team_id` CHAR(3),
  PRIMARY KEY (`team_id`)
) ENGINE = MEMORY
  SELECT `team_id`
  FROM `debearco_sports`.`SPORTS_NHL_TEAMS_GROUPINGS`
  WHERE @season BETWEEN `season_from` AND IFNULL(`season_to`, 2099);
CALL _duplicate_tmp_table('tmp_team_list', 'tmp_team_list_cp');

SELECT COUNT(*) INTO @v_num_teams FROM tmp_team_list;

INSERT INTO `FANTASY_COMMON_SELECTIONS_MAPPED` (`sport`, `season`, `raw_id`, `bit_flag`, `link_id`)
  SELECT @sport, @season, `tmp_team_list`.`team_id`, @v_num_teams - COUNT(`tmp_team_list_cp`.`team_id`), 65535 - COUNT(`tmp_team_list_cp`.`team_id`)
  FROM `tmp_team_list`
  LEFT JOIN `tmp_team_list_cp`
    ON (`tmp_team_list_cp`.`team_id` > `tmp_team_list`.`team_id`)
  GROUP BY `tmp_team_list`.`team_id`;

##
## Twitter
##

# Upcoming games
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_upcoming'), 1, 'The nights are drawing in. The leaves are turning. #DeBearRecords is getting ready for another exciting #NHL season.');

# Registration open
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 1, '#DeBearRecords is open for #NHL registration. Sign up now at {domain}/records and try to beat some of Gretzky&#39;s records.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 2, 'The ice is cut. The rink is ready. Sign up to #DeBearRecords and take on some legendary #NHL records. {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 3, 'It&#39;s time to lace them up. Join #DeBearRecords and try to beat some legendary #NHL records. {domain}/records');

# Game starting
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_starting'), 1, 'The excitement of another #NHL season starts tomorrow so make sure you&#39;ve joined #DeBearRecords. {domain}/records');

# Registration closing
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing'), 1, 'We&#39;re closing the registration of new #NHL entries to #DeBearRecords tomorrow night so don&#39;t miss out. {domain}/records');

# Weekly leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 1, 'At the conclusion of the NHL&#39;s {period_num_ord} week&#39;s games, the #DeBearRecords 50 in 50 leader has {score} goal{score_s}. Head to {domain}/records/{slug} to see the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 2, 'After the latest NHL games, the 50 in 50 #DeBearRecords leader has {score} goal{score_s}. Check out {domain}/records/{slug} to see how your entry is getting on.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 3, '{score} goal{score_s} ha{score_s-ve} been scored by the #DeBearRecords 50 in 50 NHL leader with {periods_remaining_num} week{periods_remaining_num_s} to go. Check out {domain}/records/{slug} for the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 1, 'With a point in {score} straight game{score_s}, the #DeBearRecords NHL points streak leader has {periods_remaining_num} week{periods_remaining_num_s} to improve. Head to {domain}/records/{slug} to see the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 2, 'The #DeBearRecords NHL points streak leader has a point in {score} straight game{score_s} with {periods_remaining_num} week{periods_remaining_num_s} to go. Check out {domain}/records/{slug} to see how your entry is getting on.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 3, 'After the {period_num_ord} week of NHL action, the #DeBearRecords points streak leader has a point in {score} consecutive game{score_s}. Check out {domain}/records/{slug} for the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 1, 'The #DeBearRecords NHL win leader has {score} win{score_s} after {period_num} week{period_num_s}. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 2, 'There&#39;s {periods_remaining_num} week{periods_remaining_num_s} left to improve on the current #DeBearRecords NHL leader&#39;s {score} consecutive win{score_s}. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 3, '{score} consecutive win{score_s} have been registered by our #DeBearRecords NHL leader. With {periods_remaining_num} week{periods_remaining_num_s} left of the season, check out {domain}/records/{slug} for the leaderboard');

# Record beaten (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_1'), 1, 'Welcome to the sixth member of the NHL&#39;s 50 in 50 club, {owner} &ndash; {score} goals in 50 #DeBearRecords games. {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_2'), 1, 'Well done to {owner} for breaking the NHL record by scoring a point in their {score_ord} consecutive #DeBearRecords game. {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_3'), 1, 'The 1992-93 Penguins have been toppled, there&#39;s a new record holder now &ndash; {owner} just won their 29th straight game. {domain}/records/{slug} #DeBearRecords');

# End of season leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_1'), 1, 'Congratulations to {owner} for scoring the most goals in a 50 game window, with {score} goal{score_s} {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_2'), 1, 'Well done to {owner} for producing this season&#39;s longest NHL point streak in #DeBearRecords at {score} game{score_s}. Check out {domain}/records/{slug} to see your final position!'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_3'), 1, 'Having won {score} connsecutive game{score_s}, {owner} built the longest #DeBearRecords NHL team winning streak. Congratulations! Check out {domain}/records/{slug} to see where you finished!');

## Schedule
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_open`, INTERVAL 1 DAY)) AS `date`, 'season_soon' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 7 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 5 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 3 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 1 DAY)) AS `date`, 'season_tomorrow' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_close`, INTERVAL 1 DAY)) AS `date`, 'reg_closing' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_end`, INTERVAL -1 DAY)) AS `date`, 'season_ended' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;

##
## Seed with automated players
##
CALL records_autouser_join(@sport, @season, 1, 52, 66); # Between 52% and 66% of automated users
CALL records_autouser_join(@sport, @season, 2, 35, 53); # Between 35% and 53% of automated users
CALL records_autouser_join(@sport, @season, 3, 27, 44); # Between 27% and 44% of automated users

##
## Admin
##
CALL records_setup_profiles(@sport, @season);
CALL records_setup_order();
