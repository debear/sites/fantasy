#!/bin/bash
# Process for storing an archive of a Record Breakers sport at the end of a season

set -o pipefail # Propagate exit codes through pipes

dir_base=$(dirname $0)

# Get and validate the sport
sport="$1"
if [ -z "$sport" ]; then
    echo "* Missing argument 'sport'" >&2
    exit 99
elif [ ! -e "$dir_base/$sport" ]; then
    echo "* Unknown sport argument '$sport'" >&2
    exit 99
fi

# Other config
dir_archive="$dir_base/$sport/archive"
dir_data_logs=$(realpath $dir_base/../../data/records/_logs)
dir_og=$(realpath $dir_base/../../resources/images/records/og)
dir_holders=$(realpath $dir_base/../../resources/images/records/holders)

dir_mysql_conf=$(realpath $dir_base/../../../../etc/mysql)
cmd_mysqldump="/usr/bin/mysqldump --defaults-extra-file=$dir_mysql_conf/debearco_sysadmin.conf --no-create-info --order-by-primary --skip-extended-insert --complete-insert"
cmd_mysql="/usr/bin/mysql --defaults-extra-file=$dir_mysql_conf/debearco_sysadmin.conf --silent"

db_fantasy=debearco_fantasy
db_common=debearco_common

# Season being archived for this sport
season=$(echo "SELECT DISTINCT season FROM FANTASY_RECORDS_GAMES WHERE sport = '$sport';" | $cmd_mysql $db_fantasy)
game_ids=$(echo "SELECT game_id FROM FANTASY_RECORDS_GAMES WHERE sport = '$sport' AND season = '$season' ORDER BY game_id;" | $cmd_mysql $db_fantasy)

# Tables to be archived, by group
tbl_agnostic_keep=( # These tables we backup but do not prune
    'FANTASY_COMMON_SELECTIONS_MAPPED'
)
tbl_agnostic_prune=( # These tables we backup _and_ prune
    'FANTASY_COMMON_PERIODS_DATES'
)
tbl_by_game_keep=( # These tables we backup but do not prune
    'FANTASY_RECORDS_GAMES'
    'FANTASY_RECORDS_GAMES_RULES'
    'FANTASY_RECORDS_GAMES_ACHIEVEMENTS'
    'FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS'
    'FANTASY_RECORDS_ENTRIES_SCORES'
)
tbl_by_game_prune=( # These tables we backup _and_ prune
    'FANTASY_RECORDS_GAMES_META'
    'FANTASY_RECORDS_GAMES_PERIODS_SUMMARY'
    'FANTASY_RECORDS_GAMES_POSITIONS'
    'FANTASY_RECORDS_GAMES_STATS'
    'FANTASY_RECORDS_ENTRIES_BUSTERS'
    'FANTASY_RECORDS_ENTRIES_BYPERIOD'
    'FANTASY_RECORDS_ENTRIES_DROPOFF'
    'FANTASY_RECORDS_SELECTIONS'
    'FANTASY_RECORDS_SELECTIONS_SCORING'
    'FANTASY_RECORDS_SELECTIONS_STATS'
    'FANTASY_RECORDS_SETUP_EMAIL'
    'FANTASY_RECORDS_SETUP_TWITTER'
)
tbl_comms=(
    'COMMS_TWITTER_TEMPLATE'
    'COMMS_TWITTER_SYNC'
    'COMMS_TWITTER'
)

# Helper methods
function log() {
    echo "[$(date +'%F %T')] [$1] $2"
}

function log_error() {
    retcode="$1"
    if [ $retcode -ne 0 ]; then
        log ERROR '* An error occcurred - aborting.'
        exit $retcode
    fi
}

##
## Backups
##

# Ensure the backup location exists (as it shoud be)
if [ ! -e "$dir_archive" ] || [ ! -L "$dir_archive" ]; then
    log ERROR "Archive directory must exist as a symlink outside the code base."
    exit 1
fi

# Prune a previous backup attempt, or create if not
if [ -e "$dir_archive/$season" ] || [ -e "$dir_archive/$season.tar" ]; then
    log INFO "Previous archive exists - to proceed, it needs to be pruned. Is that okay? (yes/no)"
    # Ask the user how to proceed
    proceed=0
    while [ $proceed -eq 0 ]; do
        read response
        if [ $(echo "$response" | grep -Pc '^(yes|no)$' ) -eq 0 ]; then
            log ERROR "'$response' is not an expected response. Please try again."
        elif [ "x$response" = 'xyes' ]; then
            rm -rf $dir_archive/$season $dir_archive/$season.tar
            log INFO "- Previous archive pruned"
            proceed=1
        elif [ "x$response" = 'xno' ]; then
            log WARN "- Okay, will not proceed"
            exit 1
        fi
    done
fi
mkdir -p $dir_archive/$season

# Database: Game Agnostic
log INFO '- Backing Up Database:'
log INFO "  - Game Agnostic"
$cmd_mysqldump $db_fantasy ${tbl_agnostic_keep[@]} ${tbl_agnostic_prune[@]} --where="sport = '$sport' AND season = '$season'" | grep -P '^INSERT INTO ' | gzip >$dir_archive/$season/agnostic.sql.gz
log_error $?

# Database: By Game
log INFO "  - By Game"
for game_id in $game_ids; do
    log INFO "    - $sport-$season-$game_id"
    $cmd_mysqldump $db_fantasy ${tbl_by_game_keep[@]} ${tbl_by_game_prune[@]} --where="sport = '$sport' AND season = '$season' AND game_id = '$game_id'" | grep -P '^INSERT INTO ' | gzip >$dir_archive/$season/$sport-$season-$game_id.sql.gz
    log_error $?
done

# Database: Comms
log INFO "  - Comms"
$cmd_mysqldump $db_common ${tbl_comms[@]} --where="app = 'fantasy_records' AND tweet_type LIKE '$sport\_$season\_%'" | grep -P '^INSERT INTO ' | gzip >$dir_archive/$season/comms.sql.gz
log_error $?

# Sync Logs
mkdir $dir_archive/$season/logs
log INFO '- Backing Up Process logs:'
cp -rp $dir_data_logs/$sport/*.tar.gz $dir_archive/$season/logs
log_error $?

# Open Graph images
mkdir $dir_archive/$season/og
log INFO '- Backing Up Open Graph images:'
for game_id in $game_ids; do
    log INFO "  - $sport-$season-$game_id"
    cp -rp $dir_og/$sport-$season-$game_id* $dir_archive/$season/og/
    log_error $?
done

# Holders
mkdir $dir_archive/$season/holders
log INFO '- Backing Up Holder images:'
for game_id in $game_ids; do
    log INFO "  - $sport-$season-$game_id"
    cp -rp $dir_holders/$sport-$season-$game_id* $dir_archive/$season/holders/
    log_error $?
done

log INFO '* Backup Complete'

##
## Confirm backup status before moving on to the deletion phase
##

log INFO '- Backup Status:'
log INFO '  - Database:'
ls -lh $dir_archive/$season/*.sql.gz
log INFO '  - Process logs:'
echo -n "File count: "
ls $dir_archive/$season/logs/*.tar.gz | wc -l
log INFO '  - Open Graph images:'
ls -ld $dir_archive/$season/og/*
log INFO '  - Holder images:'
ls -ld $dir_archive/$season/holders/*

log INFO 'If this looks good, please type the name of the sport and enter, or Ctrl+C to abort:'
proceed=0
while [ $proceed -eq 0 ]; do
    read response
    if [ "x$response" != "x$sport" ]; then
        log ERROR "'$response' is not correct. Please try again."
    else
        proceed=1
    fi
done

log INFO 'Tarring archive'
tar -cf $dir_archive/$season.tar -C $dir_archive $season
log_error $?
rm -rf $dir_archive/$season
log_error $?

log INFO 'Proceeding...'

##
## Deletion
##

echo "DANGER: PROCEED WITH CAUTION..." | gzip >$dir_archive/$season-prune.sql.gz

# Database: Fantasy (Merge of 'Game Agnostic' and 'By Game')
log INFO '- Pruning Database:'
for tbl in ${tbl_agnostic_prune[@]} ${tbl_by_game_prune[@]}; do
    log INFO "  - $tbl:"

    log INFO "    - DELETE"
    echo "DELETE FROM \`$tbl\` WHERE sport = '$sport' AND season = '$season';" | $cmd_mysql $db_fantasy
    log_error $?
    echo "DELETE FROM \`$db_fantasy\`.\`$tbl\` WHERE sport = '$sport' AND season = '$season';" | gzip >>$dir_archive/$season-prune.sql.gz

    log INFO "    - OPTIMIZE"
    echo "OPTIMIZE TABLE \`$tbl\`;" | $cmd_mysql --table $db_fantasy
    log_error $?
    echo "OPTIMIZE TABLE \`$db_fantasy\`.\`$tbl\`;" | gzip >>$dir_archive/$season-prune.sql.gz
done

# Database: Comms
for tbl in ${tbl_comms[@]}; do
    log INFO "  - $tbl:"

    log INFO "    - DELETE"
    echo "DELETE FROM \`$tbl\` WHERE app = 'fantasy_records' AND tweet_type LIKE '$sport\_$season\_%';" | $cmd_mysql $db_common
    log_error $?
    echo "DELETE FROM \`$db_common\`.\`$tbl\` WHERE app = 'fantasy_records' AND tweet_type LIKE '$sport\_$season\_%';" | gzip >>$dir_archive/$season-prune.sql.gz

    log INFO "    - OPTIMIZE"
    echo "OPTIMIZE TABLE \`$tbl\`;" | $cmd_mysql --table $db_common
    log_error $?
    echo "OPTIMIZE TABLE \`$db_common\`.\`$tbl\`;" | gzip >>$dir_archive/$season-prune.sql.gz
done

# Filesystem: Setup
echo "DROP TEMPORARY TABLE IF EXISTS tmp_next_steps;
CREATE TEMPORARY TABLE tmp_next_steps (
  next_step VARCHAR(500)
) ENGINE = MEMORY;" | gzip >>$dir_archive/$season-prune.sql.gz

# Filesystem: Sync Logs
log INFO '- Pruning Process logs'
rm -rf $dir_data_logs/$sport/*.tar.gz
echo "INSERT INTO tmp_next_steps VALUES ('rm -rf $dir_data_logs/$sport/*.tar.gz');" | gzip >>$dir_archive/$season-prune.sql.gz
log_error $?

# Filesystem: Open Graph images
log INFO '- Pruning Open Graph images:'
for game_id in $game_ids; do
    log INFO "  - $sport-$season-$game_id"
    rm -rf $dir_og/$sport-$season-$game_id
    echo "INSERT INTO tmp_next_steps VALUES ('rm -rf $dir_og/$sport-$season-$game_id');" | gzip >>$dir_archive/$season-prune.sql.gz
    log_error $?
done

# Filesystem: Holder images
log INFO '- Pruning Holder images:'
for game_id in $game_ids; do
    log INFO "  - $sport-$season-$game_id"
    rm -rf $dir_holders/$sport-$season-$game_id*
    echo "INSERT INTO tmp_next_steps VALUES ('rm -rf $dir_holders/$sport-$season-$game_id*');" | gzip >>$dir_archive/$season-prune.sql.gz
    log_error $?
done

echo "SELECT * FROM tmp_next_steps;" | gzip >>$dir_archive/$season-prune.sql.gz
log INFO '* Pruning Complete'
