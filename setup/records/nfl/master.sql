##
## NFL
##
SET @sport := 'nfl';
SET @season := 2021;
SET @twitter_app := 'fantasy_records';

CALL records_setup_reset(@sport, @season, @twitter_app);

# 2,105 rushing yards (Eric Dickerson, 1984) // ind, total
# - http://www.profootballhof.com/assets/1/19/Dickerson-Feature.jpg?28880
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 1, '2,105 Yard Rushing Season', 'Keep pounding the football, all the way past this record that&#39;s stood for nearly 40 years. Find the plus matchup, but remember&hellip; receiving yards don&#39;t count!', 'rushyds', 'ind', 'total', 'NO_PERIOD_SUMMARY', NULL, 2106, 2105, 'Eric Dickerson, 1984', '2021-08-29 17:00:00', '2021-09-08 17:00:00', '2021-10-31 17:00:00', '2022-01-12 17:00:00', '2022-01-25 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 1, 'One (1) selection can be made every NFL game week. All Rushing Yards gained (or lost!) by that player will be added to your season total.
If no selection is made for that week, your team&#39;s total count will not change.
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual player can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 1, 'RB', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 1, 1, 'Rush Yds', 'rush-yds', 'Total rushing yards this season', 1, NULL, NULL),
  (@sport, @season, 1, 2, 'Rush Yds/Gm', 'rush-yds-per-game', 'Average rushing yards per game played', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 'Rushing Totals', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 1, 2, 'In-Game Performances', 'The record needs some big games. Check off some smaller performances along the way.', 'period', 20);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 1, 250, 1, 10),
  (@sport, @season, 1, 1, 2, 500, 2, 20),
  (@sport, @season, 1, 1, 3, 1000, 3, 30),
  (@sport, @season, 1, 1, 4, 1500, 4, 40),
  (@sport, @season, 1, 1, 5, 2000, 5, 50),
  (@sport, @season, 1, 2, 2, 70, 6, 10),
  (@sport, @season, 1, 2, 3, 100, 7, 20),
  (@sport, @season, 1, 2, 4, 120, 8, 30),
  (@sport, @season, 1, 2, 5, 150, 9, 40);

# 55 passing TDs (Peyton Manning, 2013) // ind, total
# - (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 2, '55 Passing TD Season', 'Despite this golden age of offense and QBs passing more efficiently than ever, find the matchups to keep up the high rate of Passing TDs that Peyton managed during the 2013 season!', 'passtd', 'ind', 'total', 'NO_PERIOD_SUMMARY', NULL, 56, 55, 'Peyton Manning, 2013', '2021-08-29 17:00:00', '2021-09-08 17:00:00', '2021-10-31 17:00:00', '2022-01-12 17:00:00', '2022-01-25 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 2, 'One (1) selection can be made every NFL game week. All Passing TDs thrown by that player will be added to your season total.
If no selection is made for that week, your team&#39;s total count will not change.
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual player can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 2, 'QB', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 2, 1, 'TDs', 'touchdowns', 'Total passing touchdowns this season', 1, NULL, NULL),
  (@sport, @season, 2, 2, 'TDs/Gm', 'touchdowns-per-game', 'Average passing touchdowns per game played', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 'Touchdown Totals', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 2, 2, 'Multi-Touchdown Performances', 'Getting one touchdown is great for the total. Imagine how good a multi-touchdown game would be??', 'period', 20),
  (@sport, @season, 2, 3, 'Selection Usage', 'Whilst still choosing a player passing for a touchdown, make the not-so-obvious selection.', 'sel_usage', 30);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 1, 5, 1, 10),
  (@sport, @season, 2, 1, 2, 10, 2, 20),
  (@sport, @season, 2, 1, 3, 15, 3, 30),
  (@sport, @season, 2, 1, 4, 25, 4, 40),
  (@sport, @season, 2, 1, 5, 40, 5, 50),
  (@sport, @season, 2, 2, 2, 2, 6, 10),
  (@sport, @season, 2, 2, 3, 3, 7, 20),
  (@sport, @season, 2, 2, 5, 4, 8, 30),
  (@sport, @season, 2, 3, 1, 5, 9, 10),
  (@sport, @season, 2, 3, 2, 3, 10, 20),
  (@sport, @season, 2, 3, 4, 1, 11, 30);

# Perfect Season (New England, 2007) // team, streak
# - (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 3, 'The Perfect Season', 'Ultimately a somewhat infamous record &ndash; don&#39;t mention David Tyree to a Pats fan! &ndash; pick a winner each week&hellip; but beware not even a tie will keep your streak in tact!', 'win', 'team', 'streak', 'SEL_REQUIRED,SEL_SINGLE_USE,GAME_ENDS_ON_NEGATIVE', NULL, 18, 16, 'New England Patriots, 2007', '2021-08-29 17:00:00', '2021-09-08 17:00:00', '2021-09-12 17:00:00', '2022-01-12 17:00:00', '2022-01-25 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 3, 'One (1) selection can be made every NFL game week. If that team wins, your streak increases by one (1). If they do not &ndash; and this includes ties &ndash; you will be eliminated and your streak will end.
If no selection is made for that week, you will be eliminated and your streak will end.
When you are eliminated from the contest you will not be able to re-join and start a new streak.
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual team can only be selected one (1) time during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 3, 1, 'Record', 'record', 'Overall Win/Loss record', 1, NULL, NULL),
  (@sport, @season, 3, 2, 'Home/Road', 'home-road', 'Win/Loss record at Home/on the Road', 2, NULL, NULL),
  (@sport, @season, 3, 3, 'Spread', 'spread', 'Bookmaker handicap for the team', 3, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 'Win Streak', 'Some milestones to hit along the journey to the record.', 'score', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 1, 1, 1, 10),
  (@sport, @season, 3, 1, 2, 3, 2, 20),
  (@sport, @season, 3, 1, 3, 5, 3, 30),
  (@sport, @season, 3, 1, 4, 10, 4, 40),
  (@sport, @season, 3, 1, 5, 15, 5, 50);

##
## Game dates (storing as Sun)
##
SELECT `week` INTO @v_final_week
FROM `debearco_sports`.`SPORTS_NFL_SCHEDULE_WEEKS`
WHERE `season` = @season
AND   `game_type` = 'regular'
ORDER BY `end_date` DESC
LIMIT 1;

INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, `name`, `name_short`, `icon`, `icon_type`, `start_date`, `end_date`, `period_order`, `summarise`)
  SELECT @sport AS `sport`,
         `season`,
         `week` AS `period_id`,
         CONCAT('Week ', `week`) AS `name`,
         CONCAT('Week ', `week`) AS `name_short`,
         NULL AS `icon`,
         NULL AS `icon_type`,
         `start_date`,
         `end_date`,
         `week` AS `period_order`,
         `week` <> @v_final_week AS `summarise` -- Not the final week
  FROM `debearco_sports`.`SPORTS_NFL_SCHEDULE_WEEKS`
  WHERE `season` = @season
  AND   `game_type` = 'regular'
  ORDER BY `end_date`;

##
## Team information
##
DROP TEMPORARY TABLE IF EXISTS `tmp_team_list`;
CREATE TEMPORARY TABLE `tmp_team_list` (
  `team_id` CHAR(3),
  PRIMARY KEY (`team_id`)
) ENGINE = MEMORY
  SELECT `team_id`
  FROM `debearco_sports`.`SPORTS_NFL_TEAMS_GROUPINGS`
  WHERE @season BETWEEN `season_from` AND IFNULL(`season_to`, 2099);
CALL _duplicate_tmp_table('tmp_team_list', 'tmp_team_list_cp');

SELECT COUNT(*) INTO @v_num_teams FROM tmp_team_list;

INSERT INTO `FANTASY_COMMON_SELECTIONS_MAPPED` (`sport`, `season`, `raw_id`, `bit_flag`, `link_id`)
  SELECT @sport, @season, `tmp_team_list`.`team_id`, @v_num_teams - COUNT(`tmp_team_list_cp`.`team_id`), 65535 - COUNT(`tmp_team_list_cp`.`team_id`)
  FROM `tmp_team_list`
  LEFT JOIN `tmp_team_list_cp`
    ON (`tmp_team_list_cp`.`team_id` > `tmp_team_list`.`team_id`)
  GROUP BY `tmp_team_list`.`team_id`;

##
## Twitter
##

# Upcoming games
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_upcoming'), 1, 'Training Camps are well underway, and #DeBearRecords is getting ready for some record-breaking NFL adventures.');

# Registration open
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 1, 'Registration is open for the #DeBearRecords #NFL games. Will you topple the toughest records in football? {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 2, 'Join #DeBearRecords as the #NFL season gets underway. How will you fare against some its toughest records? {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 3, 'With the #NFL back, #DeBearRecords is giving you the chance to take on its toughest records. Sign up now at {domain}/records!');

# Game starting
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_starting'), 1, 'The #NFL is back! Have you made your #DeBearRecords selections for the opening weekend at {domain}/records?');

# Registration closing
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing'), 1, 'Last chance to get involved in the #DeBearRecords #NFL action - registration closes tomorrow. Join now at {domain}/records!'),
  # Customisation: The Perfect Season (Game 3: earlier deadline)
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing_3'), 1, 'The main NFL action starts tomorrow, so it&#39;s the last chance to start building your #DeBearRecords Perfect Season. Sign up now at {domain}/records/{slug}!');

# Weekly leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 1, 'At the conclusion of {period_name}, the #DeBearRecords rushing leader is on pace for {projected_score} yard{projected_score_s}. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 2, 'With {period_name} in the books, the #DeBearRecords rushing leader has amassed {score} rushing yard{score_s}. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 3, 'With {periods_remaining_num} week{periods_remaining_num_s} to go, the #DeBearRecords rushing leader has a total of {score} rushing yard{score_s}. Check out {domain}/records/{slug} for the leaderboard'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 1, 'At the conclusion of {period_name}, the #DeBearRecords Passing TDs leader has {score} TD{score_s}. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 2, 'With {period_name} in the books, the #DeBearRecords Passing TDs leader is on pace for {projected_score} TD{projected_score_s}. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 3, 'With {periods_remaining_num} week{periods_remaining_num_s} to go, the #DeBearRecords Passing TDs leader is on pace for {projected_score} TD{projected_score_s}. Check out {domain}/records/{slug} for the leaderboard'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 1, 'After {period_name}, the #DeBearRecords &quot;Perfect Season&quot; leader has a win in {score} straight week{score_s}. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 2, 'With a win in {score} straight week{score_s}, the #DeBearRecords &quot;Perfect Season&quot; leader has {periods_remaining_num} week{periods_remaining_num_s} left to improve their streak. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 3, 'The #DeBearRecords &quot;Perfect Season&quot; leader has {periods_remaining_num} week{periods_remaining_num_s} left to improve their streak of {score} win{score_s}. Check out {domain}/records/{slug} for the leaderboard');

# Record beaten (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_1'), 1, 'Dickerson&#39;s 30 year record has finally toppled! Congratulations to {owner} for a #DeBearRecords {score} rushing yard season. {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_2'), 1, 'We have a new #DeBearRecords gunslinger in town! {owner} has beaten Peyton&#39;s record with a {score_ord} passing touchdown. {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_3'), 1, 'The Patriots are no longer alone! {owner} just had their 18th straight win and an undefeated #DeBearRecords season. {domain}/records/{slug}');

# End of season leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_1'), 1, 'This year&#39;s #DeBearRecords NFL rushing champion is {owner} with {score} yard{score_s}. Congratulations! Head on over to {domain}/records/{slug} to see the final leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_2'), 1, 'Congratulations to {owner} for a #DeBearRecords leading season of {score} passing touchdowns. Check out {domain}/records/{slug} to see your final position!'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_3'), 1, 'The longest #DeBearRecords NFL win streak this season was {score} win{score_s} by {owner}, congratulations! Visit {domain}/records/{slug} for the final leaderboard.');

## Schedule
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_open`, INTERVAL 1 DAY)) AS `date`, 'season_soon' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, '2021-09-02' AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, '2021-09-04' AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, '2021-09-06' AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, '2021-09-08' AS `date`, 'season_tomorrow' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_close`, INTERVAL 1 DAY)) AS `date`, 'reg_closing' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_end`, INTERVAL -1 DAY)) AS `date`, 'season_ended' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;

##
## Seed with automated players
##
CALL records_autouser_join(@sport, @season, 1, 47, 59); # Between 47% and 59% of automated users
CALL records_autouser_join(@sport, @season, 2, 51, 63); # Between 51% and 63% of automated users
CALL records_autouser_join(@sport, @season, 3, 76, 86); # Between 76% and 86% of automated users

##
## Admin
##
CALL records_setup_profiles(@sport, @season);
CALL records_setup_order();
