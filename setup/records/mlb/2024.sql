##
## MLB
##
SET @sport := 'mlb';
SET @season := 2024;
SET @twitter_app := 'fantasy_records';

DELETE FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_GAMES_META` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_GAMES_PERIODS_SUMMARY` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_GAMES_RULES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_GAMES_POSITIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_GAMES_STATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_ENTRIES_SCORES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_ENTRIES_BYPERIOD` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_ENTRIES_DROPOFF` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_SELECTIONS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_SELECTIONS_SCORING` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_SELECTIONS_STATS` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_SETUP_SCORING` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_RECORDS_SETUP_TWITTER` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_PERIODS_DATES` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `FANTASY_COMMON_SELECTIONS_MAPPED` WHERE `sport` = @sport AND `season` = @season;
DELETE FROM `debearco_common`.`COMMS_TWITTER_TEMPLATE` WHERE `app` = @twitter_app AND `tweet_type` LIKE CONCAT(@sport, '\_', @season, '%');

# 56 Game Hitting Streak (Joe DiMaggio, 1941) // ind, streak
# - https://nbchardballtalk.files.wordpress.com/2011/08/joe-dimaggio.jpg
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 1, '56 Game Hitting Streak', 'The streak of all streaks &ndash; Joe DiMaggio&#39;s 56 game hitting streak. Many have tried. Some have gotten close(ish). Isn&#39;t it time someone set a new benchmark?', 'ind', 'streak', 0, 1, 1, NULL, 57, 56, 'Joe DiMaggio, 1941', '2024-03-10 17:00:00', '2024-03-20 17:00:00', '2024-07-31 17:00:00', '2024-09-29 17:00:00', '2024-10-13 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 1, 'One (1) selection can be made every day. If that player registers a Hit, your streak increases by one (1). If he doesn&#39;t it will reset to zero (0).
A selection must complete at least one (1) At Bat in the game. If a selected player has no Plate Appearances that result in at least one (1) At Bat, your team&#39;s streak will stay as-is and neither be extended nor reset to zero (0).
If no player is selected for that day, your team&#39;s streak will stay as-is and neither be extended nor reset to zero (0).
If a selected player appears in both games of a Doubleheader, he must either register a Hit or have zero (0) At Bats in both games or your streak will be reset to zero (0).
If a selected player appears in both games of a Doubleheader and registers a Hit in both games, your streak will increase by two (2).
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time. In the event of Doubleheaders, the selection locks before the player&#39;s first game.
A game must be deemed Final (not Suspended) to be counted. Any selection participating in a Suspended game will be treated as as if no selection was made.
An individual player can be selected multiple times during a streak.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 1, 'C', 10),
  (@sport, @season, 1, '1B', 20),
  (@sport, @season, 1, '2B', 30),
  (@sport, @season, 1, 'SS', 40),
  (@sport, @season, 1, '3B', 50),
  (@sport, @season, 1, 'LF', 60),
  (@sport, @season, 1, 'CF', 70),
  (@sport, @season, 1, 'RF', 80),
  (@sport, @season, 1, 'DH', 90),
  (@sport, @season, 1, 'TW', 100);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 1, 1, 'Avg', 'Batting Average', 1, NULL, NULL),
  (@sport, @season, 1, 2, 'vs L/RHP', 'Batting Average against Left/Right Handed Pitching', 2, NULL, NULL),
  (@sport, @season, 1, 3, 'vs SP', 'Career Batting Average against the Starting Pitcher', 3, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_SETUP_SCORING` (`sport`, `season`, `game_id`, `calc_method`) VALUES
  (@sport, @season, 1, 'mlb_ind_hit_streak');

# 73 Home Runs (Barry Bonds, 2001) // ind, total
# - http://images.performgroup.com/di/library/sporting_news/a8/76/barry-bonds_z1408lythf3b13odx7tf4g2ic.jpg?t=-316146333&h=600
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 2, '73 Home Run Season', 'The Home Run is now the epitomy of modern day baseball, one of the infamous &quot;Three True Outcomes&quot;. What would it take to &quot;clean up&quot; a record viewed by many as tainted?', 'ind', 'total', 0, 1, 1, NULL, 74, 73, 'Barry Bonds, 2001', '2024-03-10 17:00:00', '2024-03-20 17:00:00', '2024-05-31 17:00:00', '2024-09-29 17:00:00', '2024-10-13 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 2, 'One (1) selection can be made every day. Any Home Runs hit by that player will be added to your season total.
If no selection is made for that day, your team&#39;s total count will not change.
If a selected player appears in both games of a Doubleheader, Home Runs hit in both games will be included.
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time. In the event of Doubleheaders, the selection locks before the player&#39;s first game.
A game must be deemed Final (not Suspended) to be counted. Any selection participating in a Suspended game will be treated as as if no selection was made.
An individual player can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 2, 'C', 10),
  (@sport, @season, 2, '1B', 20),
  (@sport, @season, 2, '2B', 30),
  (@sport, @season, 2, 'SS', 40),
  (@sport, @season, 2, '3B', 50),
  (@sport, @season, 2, 'LF', 60),
  (@sport, @season, 2, 'CF', 70),
  (@sport, @season, 2, 'RF', 80),
  (@sport, @season, 2, 'DH', 90),
  (@sport, @season, 2, 'TW', 100);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 2, 1, 'HR', 'Total Home Runs this season', 1, NULL, NULL),
  (@sport, @season, 2, 2, 'vs L/RHP', 'Home Runs against Left/Right Handed Pitching', 2, NULL, NULL),
  (@sport, @season, 2, 3, 'vs SP', 'Career Home Runs against the Starting Pitcher', 3, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_SETUP_SCORING` (`sport`, `season`, `game_id`, `calc_method`) VALUES
  (@sport, @season, 2, 'mlb_ind_hr_total');

# 26 Game Winning Streak (New York Giants, 1916) // team, streak
# - //content.sportslogos.net/logos/54/86/full/6677_new_york_giants-primary-1916.png
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 3, '26 Game Winning Streak', 'An old-school MLB record, with the leaderboard filled from the pre-war era, continue picking winners&hellip; by one run or in a blowout, they all count the same!', 'team', 'streak', 0, 1, 1, NULL, 27, 26, 'New York Giants, 1916', '2024-03-10 17:00:00', '2024-03-20 17:00:00', '2024-07-31 17:00:00', '2024-09-29 17:00:00', '2024-10-13 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 3, 'One (1) selection can be made every day. If that team wins your streak increases by one (1) otherwise it will reset to zero (0).
If no selection is made for that day, your streak will stay as-is and neither be extended nor reset to zero (0).
If a selected team has a Doubleheader that day, they must win both games for your streak to continue and will increase by two (2).
If a selected team has a Doubleheader that day and wins the first game but loses the second, your team will still get credit for the first game won but will still ultimately reset to zero (0).
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time. In the event of Doubleheaders, the selection locks before the first game.
A game must be deemed Final (not Suspended) to be counted. Any team playing a Suspended game will be treated as as if no selection was made.
An individual team can be selected multiple times during a streak.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 3, 1, 'Record', 'Overall Win/Loss record', 1, NULL, NULL),
  (@sport, @season, 3, 2, 'Last 10', 'Win/Loss record over the Last 10 games', 2, NULL, NULL),
  (@sport, @season, 3, 3, 'Home/Road', 'Win/Loss record at Home/on the Road', 3, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_SETUP_SCORING` (`sport`, `season`, `game_id`, `calc_method`) VALUES
  (@sport, @season, 3, 'mlb_team_win_streak');

##
## Game dates
##
# Get the game dates in an order
DROP TEMPORARY TABLE IF EXISTS `tmp_sched_dates`;
CREATE TEMPORARY TABLE `tmp_sched_dates` (
  `period_id` TINYINT UNSIGNED,
  `game_date` DATE,
  PRIMARY KEY (`game_date`)
) ENGINE = MEMORY
  SELECT DISTINCT NULL AS `period_id`, `game_date`
  FROM `debearco_sports`.`SPORTS_MLB_SCHEDULE`
  WHERE `season` = @season
  AND   `game_type` = 'regular'
  ORDER BY `game_date`;

CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpB');
INSERT INTO `tmp_sched_dates` (`game_date`, `period_id`)
  SELECT `A`.`game_date`, COUNT(DISTINCT `B`.`game_date`) + 1 AS `period_id`
  FROM `tmp_sched_dates_cpA` AS `A`
  LEFT JOIN `tmp_sched_dates_cpB` AS `B`
    ON (`B`.`game_date` < `A`.`game_date`)
  GROUP BY `A`.`game_date`
ON DUPLICATE KEY UPDATE `period_id` = VALUES(`period_id`);

CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, `name`, `name_short`, `icon`, `icon_type`, `start_date`, `end_date`, `period_order`, `summarise`)
  SELECT @sport AS `sport`, @season AS `season`,
         `GAME_DATE`.`period_id`,
         NULL AS `name`, NULL AS `name_short`, NULL as `icon`, NULL AS `icon_type`,
         IFNULL(DATE_ADD(`PREV_DATE`.`game_date`, INTERVAL 1 DAY), `GAME_DATE`.`game_date`) AS `start_date`,
         `GAME_DATE`.`game_date` AS `end_date`,
         `GAME_DATE`.`period_id` AS `period_order`,
         0 AS `summarise` # This will be calculated separately
  FROM `tmp_sched_dates` AS `GAME_DATE`
  LEFT JOIN `tmp_sched_dates_cpA` AS `PREV_DATE`
    ON (`PREV_DATE`.`period_id` = `GAME_DATE`.`period_id` - 1)
  GROUP BY `GAME_DATE`.`period_id`
  ORDER BY `GAME_DATE`.`period_id`;

# Process on a Friday, skipping the very early part of the season and the final week
SELECT `end_date` INTO @v_final_week_cutoff
FROM `FANTASY_COMMON_PERIODS_DATES`
WHERE `sport` = @sport
AND   `season` = @season
ORDER BY `end_date` DESC
LIMIT 3, 1;

UPDATE `FANTASY_COMMON_PERIODS_DATES`
SET `summarise` = (
      `period_order` >= 7
  AND `end_date` <= @v_final_week_cutoff
  AND IF(DAYOFWEEK(`start_date`) <= DAYOFWEEK(`end_date`),
        5 BETWEEN DAYOFWEEK(`start_date`) AND DAYOFWEEK(`end_date`),
            5 BETWEEN DAYOFWEEK(`start_date`) AND (DAYOFWEEK(`end_date`) + 7)
        OR 12 BETWEEN DAYOFWEEK(`start_date`) AND (DAYOFWEEK(`end_date`) + 7)
      ))
WHERE `sport` = @sport
AND   `season` = @season;

##
## Teams as selections
##
DROP TEMPORARY TABLE IF EXISTS `tmp_team_list`;
CREATE TEMPORARY TABLE `tmp_team_list` (
  `team_id` CHAR(3),
  PRIMARY KEY (`team_id`)
) ENGINE = MEMORY
  SELECT `team_id`
  FROM `debearco_sports`.`SPORTS_MLB_TEAMS_GROUPINGS`
  WHERE @season BETWEEN `season_from` AND IFNULL(`season_to`, 2099);
CALL _duplicate_tmp_table('tmp_team_list', 'tmp_team_list_cp');

INSERT INTO `FANTASY_COMMON_SELECTIONS_MAPPED` (`sport`, `season`, `raw_id`, `link_id`)
  SELECT @sport, @season, `tmp_team_list`.`team_id`, 65535 - COUNT(`tmp_team_list_cp`.`team_id`)
  FROM `tmp_team_list`
  LEFT JOIN `tmp_team_list_cp`
    ON (`tmp_team_list_cp`.`team_id` > `tmp_team_list`.`team_id`)
  GROUP BY `tmp_team_list`.`team_id`;

##
## Twitter
##

# Upcoming games
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_upcoming'), 1, 'We&#39;re applying the finishing touches to #DeBearRecords #MLB offering &ndash; check back soon for more details.');

# Registration open
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 1, 'The #MLB games in #DeBearRecords are now open. Can you top DiMaggio&#39;s 56 game streak or Bonds&#39; 73 HR season? {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 2, 'Spring Training is nearly over. #MLB games start to count. Take on the #DeBearRecords challenge at {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 3, 'The #MLB season is around the corner. #DeBearRecords is ready for you to tackle some legendary records. {domain}/records');

# Game starting
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_starting'), 1, 'The #MLB quest for October starts tomorrow. Your #DeBearRecords quest for greatness starts here too. {domain}/records');

# Registration closing
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing'), 1, 'Last call for those players wanting to join the #DeBearRecords #MLB games - registration closes tomorrow. {domain}/records'),
  # Customisation: 73 Home Runs (Game 2: earlier deadline)
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing_2'), 1, 'If you&#39;ve not already joined, your last chance to start toppling Barry Bonds&#39; Home Run record in #DeBearRecords is tomorrow. Sign up now at {domain}/records/{slug}');

# Weekly leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 1, 'At {score} game{score_s}, the #DeBearRecords MLB hit streak leader has {periods_remaining_num} week{periods_remaining_num_s} left to chase DiMaggio&#39;s 56 game streak. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 2, 'With {periods_remaining_num} week{periods_remaining_num_s} left of the MLB season, the #DeBearRecords hit streak leader has reached {score} game{score_s}. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 3, 'The #DeBearRecords MLB hit streak leader has a hit in {score} straight game{score_s} after {period_num} week{period_num_s} of the season. Check out {domain}/records/{slug} for the leaderboard'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 1, 'After {period_num} week{period_num_s} of MLB games, the #DeBearRecords leader is up to {score} home run{score_s}. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 2, '{score} home run{score_s} leads #DeBearRecords MLB chase for 73 with {periods_remaining_num} week{periods_remaining_num_s} left of the season. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 3, 'With {periods_remaining_num} week{periods_remaining_num_s} to go, the #DeBearRecords MLB leader has a total of {score} homer{score_s}. Check out {domain}/records/{slug} for the leaderboard'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 1, 'In the chase for 27 straight wins, the #DeBearRecords MLB leader has reached {score} consecutive win{score_s}. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 2, 'With {periods_remaining_num} week{periods_remaining_num_s} to go, the #DeBearRecords MLB leader has wins in {score} straight game{score_s}. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 3, 'The #DeBearRecords MLB leader has a winning streak of {score} game{score_s} with {periods_remaining_num} week{periods_remaining_num_s} left of the season. Check out {domain}/records/{slug} for the leaderboard');

# Record beaten (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_1'), 1, '#DeBearRecords has found a new MLB hit streak champion! {owner} just collected a hit in their {score_ord} consecutive game. {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_2'), 1, 'There&#39;s no doubt about the record any more &ndash; {owner} just had their {score_ord} homer of the season. {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_3'), 1, 'Sorry to the 1916 Giants, but we have a new record in town &ndash; {owner} just won their 27th straight game. {domain}/records/{slug} #DeBearRecords');

# End of season leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_1'), 1, 'Our longest hit streak in {season} was {score} game{score_s}. Congrats {owner}. {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_2'), 1, 'Well done to the #DeBearRecords home run champ in {season}, {owner} with {score} homer{score_s}. Head on over to {domain}/records/{slug} to see the final leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_3'), 1, 'Congratulations to the longest #DeBearRecords MLB team winning streak of {score} game{score_s} by {owner}. Check out {domain}/records/{slug} to see your final position!');

## Schedule
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_open`, INTERVAL 1 DAY)) AS `date`, 'season_soon' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 7 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 5 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 3 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 1 DAY)) AS `date`, 'season_tomorrow' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_close`, INTERVAL 1 DAY)) AS `date`, 'reg_closing' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_end`, INTERVAL -1 DAY)) AS `date`, 'season_ended' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;

##
## Seed with automated players
##
CALL records_autouser_join(@sport, @season, 1, 52, 68); # Between 52% and 68% of automated users
CALL records_autouser_join(@sport, @season, 2, 43, 59); # Between 43% and 59% of automated users
CALL records_autouser_join(@sport, @season, 3, 35, 46); # Between 35% and 46% of automated users

##
## Admin
##
ALTER TABLE `FANTASY_RECORDS_GAMES` ORDER BY `sport`, `season`, `game_id`;
ALTER TABLE `FANTASY_RECORDS_GAMES_POSITIONS` ORDER BY `sport`, `season`, `game_id`, `disp_order`;
ALTER TABLE `FANTASY_RECORDS_GAMES_RULES` ORDER BY `sport`, `season`, `game_id`;
ALTER TABLE `FANTASY_RECORDS_GAMES_STATS` ORDER BY `sport`, `season`, `game_id`, `stat_id`;
ALTER TABLE `FANTASY_RECORDS_ENTRIES_SCORES` ORDER BY `sport`, `season`, `game_id`, `user_id`;
ALTER TABLE `FANTASY_RECORDS_SETUP_SCORING` ORDER BY `sport`, `season`, `game_id`;
ALTER TABLE `FANTASY_COMMON_PERIODS_DATES` ORDER BY `sport`, `season`, `period_id`;
ALTER TABLE `FANTASY_RECORDS_SETUP_TWITTER` ORDER BY `sport`, `season`, `game_id`, `date`, `tweet_type`;
ALTER TABLE `FANTASY_COMMON_SELECTIONS_MAPPED` ORDER BY `sport`, `season`, `link_id`;
