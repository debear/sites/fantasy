##
## AHL
##
SET @sport := 'ahl';
SET @season := 2024;
SET @twitter_app := 'fantasy_records';

CALL records_setup_reset(@sport, @season, @twitter_app);

# 50 in 50 // ind, total, limited period
# - (Use league logo?)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 1, '50 in 50', 'Although there has yet to be an instance of 50 goals in 50 games in the AHL, that doesn&#39;t mean it cannot be achieved &ndash; there are still enough consistent goal scorers to choose from!', 'ind', 'total', 0, 1, 1, 50, 50, NULL, NULL, '2024-10-01 17:00:00', '2024-10-11 17:00:00', '2024-12-31 17:00:00', '2025-04-21 17:00:00', '2025-05-04 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 1, 'One (1) selection can be made every day. All Goals scored by that player will be added to your season total.
A team&#39;s &quot;season total&quot; is deemed to be the number of goals scored by the most recent fifty (50) selections who were credited with a Game Played.
If no selection is made for that day, your team&#39;s total count will not change and will not contribute towards the &quot;most recent fifty (50) selections&quot;.
Unlike the real-life version of this record, the attempt can commence at any point in the season &ndash; it does not have to begin at the start of the season.
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual player can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 1, 'C', 10),
  (@sport, @season, 1, 'LW', 20),
  (@sport, @season, 1, 'RW', 30),
  (@sport, @season, 1, 'F', 38),
  (@sport, @season, 1, 'D', 40);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 1, 1, 'Goals', 'Total Goals scored this season', 1, NULL, NULL),
  (@sport, @season, 1, 2, 'Goals/Gm', 'Average Goals scored per game played', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 'Goal Totals', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 1, 2, 'Multi-Goal Performances', 'Getting one goal is great for the total. Imagine how good a multi-goal game would be??', 'period', 20),
  (@sport, @season, 1, 3, 'Opponents', 'Spread the misery, and target a variety of opponents.', 'opponent', 30),
  (@sport, @season, 1, 4, 'Selection Usage', 'Whilst still choosing a player who pots a goal, make the not-so-obvious selection.', 'sel_usage', 40);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 1, 5, 1, 10),
  (@sport, @season, 1, 1, 2, 10, 2, 20),
  (@sport, @season, 1, 1, 3, 20, 3, 30),
  (@sport, @season, 1, 1, 4, 30, 4, 40),
  (@sport, @season, 1, 1, 5, 40, 5, 50),
  (@sport, @season, 1, 2, 2, 2, 6, 10),
  (@sport, @season, 1, 2, 3, 3, 7, 20),
  (@sport, @season, 1, 2, 5, 4, 8, 30),
  (@sport, @season, 1, 3, 1, 3, 9, 10),
  (@sport, @season, 1, 3, 2, 5, 10, 20),
  (@sport, @season, 1, 3, 3, 10, 11, 30),
  (@sport, @season, 1, 3, 4, 20, 12, 40),
  (@sport, @season, 1, 3, 5, 32, 13, 50),
  (@sport, @season, 1, 4, 2, 3, 14, 10),
  (@sport, @season, 1, 4, 3, 2, 15, 20),
  (@sport, @season, 1, 4, 5, 1, 16, 30);
INSERT INTO `FANTASY_RECORDS_SETUP_SCORING` (`sport`, `season`, `game_id`, `calc_method`) VALUES
  (@sport, @season, 1, 'ahl_ind_50in50_total');

# 39 Game Point Streak (Darren Haydar, 2006-07) // ind, streak
# - http://cluster.leaguestat.com/download.php?client_code=ahl&file_path=media/a9f661e454739b97c84a27547b04b709.jpg
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 2, '39 Game Point Streak', 'Finding a consistent set of point getters to keep a streak going &ndash; but remember, it&#39;s just the number of games that counts not the number of points so one works as well as three!', 'ind', 'streak', 0, 1, 1, NULL, 40, 39, 'Darren Haydar, 2006-07', '2024-10-01 17:00:00', '2024-10-11 17:00:00', '2024-12-31 17:00:00', '2025-04-21 17:00:00', '2025-05-04 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 2, 'One (1) selection can be made every day. If that player scores at least one point (goal or assist), your streak will increase by one (1). If he does not, it will reset to zero (0).
If the selected player is not credited with a Game Played, your team&#39;s streak will stay as-is and neither be extended nor reset to zero (0).
If no player is selected for that day, your team&#39;s streak will stay as-is and neither be extended nor reset to zero (0).
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual player can be selected multiple times during a streak.');
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, `pos_code`, `disp_order`) VALUES
  (@sport, @season, 2, 'C', 10),
  (@sport, @season, 2, 'LW', 20),
  (@sport, @season, 2, 'RW', 30),
  (@sport, @season, 2, 'F', 38),
  (@sport, @season, 2, 'D', 40);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 2, 1, 'Points', 'Total points (goals and assists) scored this season', 1, NULL, NULL),
  (@sport, @season, 2, 2, 'Points/Gm', 'Average points (goals and assists) per game played', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 'Points Streak', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 2, 2, 'Opponents', 'Spread the misery, and target a variety of opponents.', 'opponent', 20),
  (@sport, @season, 2, 3, 'Selection Usage', 'Whilst still choosing a player who nets a point, make the not-so-obvious selection.', 'sel_usage', 30);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 1, 3, 1, 10),
  (@sport, @season, 2, 1, 2, 5, 2, 20),
  (@sport, @season, 2, 1, 3, 10, 3, 30),
  (@sport, @season, 2, 1, 4, 20, 4, 40),
  (@sport, @season, 2, 1, 5, 30, 5, 50),
  (@sport, @season, 2, 2, 1, 3, 6, 10),
  (@sport, @season, 2, 2, 2, 5, 7, 20),
  (@sport, @season, 2, 2, 3, 10, 8, 30),
  (@sport, @season, 2, 2, 4, 20, 9, 40),
  (@sport, @season, 2, 2, 5, 32, 10, 50),
  (@sport, @season, 2, 3, 2, 3, 11, 10),
  (@sport, @season, 2, 3, 3, 2, 12, 20),
  (@sport, @season, 2, 3, 5, 1, 13, 30);
INSERT INTO `FANTASY_RECORDS_SETUP_SCORING` (`sport`, `season`, `game_id`, `calc_method`) VALUES
  (@sport, @season, 2, 'ahl_ind_pts_streak');

# 28 Game Winning Streak (Norfolk Admirals, 2011-12) // team, streak
# - (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `sel_type`, `score_type`, `sel_required`, `sel_reuse`, `game_restart`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 3, '28 Game Winning Streak', 'The crux of the team game is to score more goals than your opponent. Continue to pick winners, until reaching this legendary AHL record!', 'team', 'streak', 0, 1, 1, NULL, 29, 28, 'Norfolk Admirals, 2011-12', '2024-10-01 17:00:00', '2024-10-11 17:00:00', '2024-12-31 17:00:00', '2025-04-21 17:00:00', '2025-05-04 17:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 3, 'One (1) selection can be made every day. If that team wins your streak increases by one (1) otherwise it will reset to zero (0).
A win can come in any scenario &ndash; regulation, overtime and shootout are all considered equal.
If no selection is made for that day, your streak will stay as-is and neither be extended nor reset to zero (0).
Individual selections will lock five (5) minutes prior to their game&#39;s advertised start time.
An individual team can be selected multiple times during a streak.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 3, 1, 'Record', 'Overall Win/Loss record', 1, NULL, NULL),
  (@sport, @season, 3, 2, 'Last 10', 'Win/Loss record over the Last 10 games', 2, NULL, NULL),
  (@sport, @season, 3, 3, 'Home/Road', 'Win/Loss record at Home/on the Road', 3, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 'Win Streak', 'Some milestones to hit along the journey to the record.', 'score', 10),
  (@sport, @season, 3, 2, 'Opponents', 'Spread the misery, and target a variety of opponents.', 'opponent', 20),
  (@sport, @season, 3, 3, 'Selection Usage', 'Whilst still choosing a winning team, make the not-so-obvious selection.', 'sel_usage', 30);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 1, 3, 1, 10),
  (@sport, @season, 3, 1, 2, 5, 2, 20),
  (@sport, @season, 3, 1, 3, 10, 3, 30),
  (@sport, @season, 3, 1, 4, 15, 4, 40),
  (@sport, @season, 3, 1, 5, 20, 5, 50),
  (@sport, @season, 3, 2, 1, 3, 6, 10),
  (@sport, @season, 3, 2, 2, 5, 7, 20),
  (@sport, @season, 3, 2, 3, 10, 8, 30),
  (@sport, @season, 3, 2, 4, 20, 9, 40),
  (@sport, @season, 3, 2, 5, 32, 10, 50),
  (@sport, @season, 3, 3, 2, 3, 11, 10),
  (@sport, @season, 3, 3, 3, 2, 12, 20),
  (@sport, @season, 3, 3, 5, 1, 13, 30);
INSERT INTO `FANTASY_RECORDS_SETUP_SCORING` (`sport`, `season`, `game_id`, `calc_method`) VALUES
  (@sport, @season, 3, 'ahl_team_win_streak');

##
## Game dates
##
# Get the game dates in an order
DROP TEMPORARY TABLE IF EXISTS `tmp_sched_dates`;
CREATE TEMPORARY TABLE `tmp_sched_dates` (
  `period_id` TINYINT UNSIGNED,
  `game_date` DATE,
  PRIMARY KEY (`game_date`)
) ENGINE = MEMORY
  SELECT DISTINCT NULL AS `period_id`, `game_date`
  FROM `debearco_sports`.`SPORTS_AHL_SCHEDULE`
  WHERE `season` = @season
  AND   `game_type` = 'regular'
  ORDER BY `game_date`;

CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpB');
INSERT INTO `tmp_sched_dates` (`game_date`, `period_id`)
  SELECT `A`.`game_date`, COUNT(DISTINCT `B`.`game_date`) + 1 AS `period_id`
  FROM `tmp_sched_dates_cpA` AS `A`
  LEFT JOIN `tmp_sched_dates_cpB` AS `B`
    ON (`B`.`game_date` < `A`.`game_date`)
  GROUP BY `A`.`game_date`
ON DUPLICATE KEY UPDATE `period_id` = VALUES(`period_id`);

CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, `name`, `name_short`, `icon`, `icon_type`, `start_date`, `end_date`, `period_order`, `summarise`)
  SELECT @sport AS `sport`, @season AS `season`,
         `GAME_DATE`.`period_id`,
         NULL AS `name`, NULL AS `name_short`, NULL as `icon`, NULL AS `icon_type`,
         IFNULL(DATE_ADD(`PREV_DATE`.`game_date`, INTERVAL 1 DAY), `GAME_DATE`.`game_date`) AS `start_date`,
         `GAME_DATE`.`game_date` AS `end_date`,
         `GAME_DATE`.`period_id` AS `period_order`,
         0 AS `summarise` # This will be calculated separately
  FROM `tmp_sched_dates` AS `GAME_DATE`
  LEFT JOIN `tmp_sched_dates_cpA` AS `PREV_DATE`
    ON (`PREV_DATE`.`period_id` = `GAME_DATE`.`period_id` - 1)
  ORDER BY `GAME_DATE`.`period_id`;

# Process on a Monday, skipping the very early part of the season and the final week
SELECT `end_date` INTO @v_final_week_cutoff
FROM `FANTASY_COMMON_PERIODS_DATES`
WHERE `sport` = @sport
AND   `season` = @season
ORDER BY `end_date` DESC
LIMIT 3, 1;

UPDATE `FANTASY_COMMON_PERIODS_DATES`
SET `summarise` = (
      `period_order` >= 7
  AND `end_date` <= @v_final_week_cutoff
  AND IF(DAYOFWEEK(`start_date`) <= DAYOFWEEK(`end_date`),
        1 BETWEEN DAYOFWEEK(`start_date`) AND DAYOFWEEK(`end_date`),
           1 BETWEEN DAYOFWEEK(`start_date`) AND (DAYOFWEEK(`end_date`) + 7)
        OR 8 BETWEEN DAYOFWEEK(`start_date`) AND (DAYOFWEEK(`end_date`) + 7)
      ))
WHERE `sport` = @sport
AND   `season` = @season;

##
## Team information
##
DROP TEMPORARY TABLE IF EXISTS `tmp_team_list`;
CREATE TEMPORARY TABLE `tmp_team_list` (
  `team_id` CHAR(3),
  PRIMARY KEY (`team_id`)
) ENGINE = MEMORY
  SELECT `team_id`
  FROM `debearco_sports`.`SPORTS_AHL_TEAMS_GROUPINGS`
  WHERE @season BETWEEN `season_from` AND IFNULL(`season_to`, 2099);
CALL _duplicate_tmp_table('tmp_team_list', 'tmp_team_list_cp');

SELECT COUNT(*) INTO @v_num_teams FROM tmp_team_list;

INSERT INTO `FANTASY_COMMON_SELECTIONS_MAPPED` (`sport`, `season`, `raw_id`, `bit_flag`, `link_id`)
  SELECT @sport, @season, `tmp_team_list`.`team_id`, @v_num_teams - COUNT(`tmp_team_list_cp`.`team_id`), 65535 - COUNT(`tmp_team_list_cp`.`team_id`)
  FROM `tmp_team_list`
  LEFT JOIN `tmp_team_list_cp`
    ON (`tmp_team_list_cp`.`team_id` > `tmp_team_list`.`team_id`)
  GROUP BY `tmp_team_list`.`team_id`;

##
## Twitter
##

# Upcoming games
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_upcoming'), 1, 'The #AHL is gearing up for another season of exciting hockey. #DeBearRecords is getting ready to join in the fun.');

# Registration open
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 1, 'With the #AHL season starting shortly, #DeBearRecords is open. Join up and tackle some legendary records. {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 2, 'Not long until the first puck drop of the #AHL. Join #DeBearRecords and take on some legendary records. {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 3, '#DeBearRecords registration is open for its #AHL record breaking adventures. {domain}/records');

# Game starting
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_starting'), 1, 'The #AHL officially starts tomorrow. Have you joined #DeBearRecords record breaking adventures? {domain}/records');

# Registration closing
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing'), 1, 'Tomorrow is the last chance to join the #AHL action in #DeBearRecords &ndash; register now to join in the fun. {domain}/records');

# Weekly leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 1, 'After the latest AHL games, {score} goal{score_s} ha{score_s-ve} been scored by the #DeBearRecords 50 in 50 AHL leader. Head to {domain}/records/{slug} to see the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 2, 'At the conclusion of the AHL&#39;s {period_num_ord} week&#39;s games, the 50 in 50 #DeBearRecords leader has {score} goal{score_s}. Check out {domain}/records/{slug} to see how your entry is getting on.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 3, 'The #DeBearRecords 50 in 50 AHL leader as {score} goal{score_s} scored with {periods_remaining_num} week{periods_remaining_num_s} to go. Check out {domain}/records/{slug} for the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 1, 'Points in {score} straight game{score_s} leaves the #DeBearRecords AHL points streak leader with {periods_remaining_num} week{periods_remaining_num_s} to improve. Head to {domain}/records/{slug} to see the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 2, 'The #DeBearRecords AHL points streak leader has a point in {score} straight game{score_s} after the {period_num_ord} week of games. Check out {domain}/records/{slug} to see how your entry is getting on.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 3, 'After {period_num} week{period_num_s} of AHL action, the #DeBearRecords points streak leader has a point in {score} consecutive game{score_s}. Check out {domain}/records/{slug} for the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 1, 'Having won {score} consecutive games{score_s}, the #DeBearRecords AHL leader has {periods_remaining_num} week{periods_remaining_num_s} left to catch the Admirals. See the leaderboard at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 2, '{period_num} week{period_num_s} into the AHL season, and the #DeBearRecords leader has {score} consecutive win{score_s}. Check out how your entry is doing at {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 3, 'A {score} game winning streak currently tops the #DeBearRecords AHL leaderboard after {period_num} week{period_num_s}. Check out {domain}/records/{slug} for the leaderboard');

# Record beaten (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_1'), 1, 'For the first time ever, a 50-in-50 season in the AHL was just orchestrated by {owner} - congratulations! {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_2'), 1, 'Congratulations to {owner} for topping the AHL record and scoring a point in their 40th consecutive #DeBearRecords game. {domain}/records/{slug}'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_3'), 1, 'The legendary 2011-12 Norfolk Admirals were just lucky &ndash; {owner} built a streak with their 29th straight win. {domain}/records/{slug} #DeBearRecords');

# End of season leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_1'), 1, 'Congratulations to {owner} for the most goals in 50 AHL games, with a #DeBearRecords score of {score}. {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_2'), 1, 'Well done to {owner} for the longest #DeBearRecords AHL point streak of {score} game{score_s}. Check out {domain}/records/{slug} to see your final position!'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_3'), 1, 'Winning {score} game{score_s} in a row, {owner} built the longest #DeBearRecords AHL win streak. Congratulations! Check out {domain}/records/{slug} to see where you finished!');

## Schedule
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_open`, INTERVAL 1 DAY)) AS `date`, 'season_soon' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 7 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 5 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 3 DAY)) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_start`, INTERVAL 1 DAY)) AS `date`, 'season_tomorrow' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_close`, INTERVAL 1 DAY)) AS `date`, 'reg_closing' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_end`, INTERVAL -1 DAY)) AS `date`, 'season_ended' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;

##
## Seed with automated players
##
CALL records_autouser_join(@sport, @season, 1, 33, 43); # Between 33% and 43% of automated users
CALL records_autouser_join(@sport, @season, 2, 31, 43); # Between 31% and 43% of automated users
CALL records_autouser_join(@sport, @season, 3, 21, 34); # Between 21% and 34% of automated users

##
## Admin
##
CALL records_setup_profiles(@sport, @season);
CALL records_setup_order();
