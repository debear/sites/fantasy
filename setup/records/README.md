# Record Breaker Game Configuration

This file describes how to create the processing logic and setup for a new Record Breakers Game. Not all sections may be
required, as there is an element of commonality between some of the components - for instance, MLB's 'Hit Streak' and
'Home Run Total' games can use the same logic for determining selections and events as they are both contested by the
same group of players.

Substitutions used throughout the examples:
- `{sport}` - "Sport" field of the game
- `{sel_type}` - "Selection Type" field of the game (i.e., `ind` or `team`)
- `{score_type}` - "Score Type" field of the game (i.e., `total`, `streak` or `periods`)
- `{game_code}` - A shorthand reference only used within this code to reference the game

## database/debearco_fantasy/records/routines/setup\_{sport}\_{sel_type}.sql

This file contains the logic for determining the selections and events applicable to an individual game period. It can
be shared across multiple games, especially when coupled with `FANTASY_RECORDS_GAMES_POSITIONS` to allow for the
per-game sub-division of the full selection pool.

These methods expect to produce temporary tables with a core set of columns, as outlined below, but extensible as
required for their subsequent use in per-game processing.

```sql
#
# Selections
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_sel`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_sel`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selection lists for {sport}:{sel_type}'
BEGIN

  -- This method should populate `tmp_links` with the selections to be made available for the game period
  -- See `records_preview_setup()` for the base, but extensible. schema

END $$

DELIMITER ;

#
# Determine the event(s) the selections could take part in
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_events`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_events`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection event(s) for {sport}:{sel_type} selections'
BEGIN

  -- This method should populate `tmp_link_events` with the event(s) each selection in `tmp_links` could take part in
  --  during this game period (with both the raw ID and appropriate date range passed)

  -- As the schema will be affected by the nuances of each sport, it is to be created within this method and should
  --  include the following core fields:
  -- DROP TEMPORARY TABLE IF EXISTS tmp_link_events;
  -- CREATE TEMPORARY TABLE tmp_link_events (
  --   link_id SMALLINT UNSIGNED,
  --     .. additional sport-specific columns ..
  --   stat DECIMAL(5,1) SIGNED,
  --   status ENUM('positive','neutral','negative','na'),
  --   stress TINYINT UNSIGNED,
  --   summary VARCHAR(50),
  --   PRIMARY KEY (link_id, ...)
  -- ) ENGINE=MyISAM
  --   SELECT tmp_links.link_id, ...
  --          NULL AS stat, 'na' AS status, NULL AS stress, NULL AS summary
  --   FROM tmp_links
  --   ...

END $$

DELIMITER ;

#
# Determine which "events" (games) each selection could have played across a variety of periods
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_events_historical`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_events_historical`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker historical games played for {sport}:{sel_type} sel'
BEGIN

  -- This method should determine the list of event(s) each selection in `tmp_entry_sel_selperiod` took part in during
  --  the period stored in the `period_id` column

  -- As the schema will be affected by the nuances of each sport, it is to be created within this method and should
  --  include the following core fields:
  -- DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_events_raw;
  -- CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_events_raw (
  --   link_id SMALLINT UNSIGNED,
  --   period_id TINYINT UNSIGNED,
  --     .. additional sport-specific columns ..
  --   team_id CHAR(3),
  --   opp_team_id CHAR(3),
  --   PRIMARY KEY (link_id, ...)
  -- ) ENGINE=MyISAM
  --   SELECT tmp_entry_sel_selperiod.link_id, tmp_entry_sel_selperiod.period_id, ...
  --          ... AS team_id, ... AS opp_team_id
  --   FROM tmp_entry_sel_selperiod
  --   ...

END $$

DELIMITER ;
```

## database/debearco_fantasy/records/routines/game\_{sport}\_{sel_type}\_{game_code}\_{score_type}.sql

This file contains the heart of the processing logic, and is expected to be in a 1:1 relationship with the games -
whereas a single setup file above can apply to multiple games, this file should be specific to a single game. It
contains methods for both the "preview" phase of a game period (i.e., before it has started) and the subsequent
"processing" phase (i.e., after completion), as outlined below:

__Preview__
| Method | Notes |
|--|--|
| `*_preview` | Determine the appropriate value(s) for each selection in the game's identified statistics |
| `*_selrating` | Produce a numeric rating to indicate the suitability of each selection (higher rating = better selection) |
| `*_opprating` | _(Expected for Major League games only - MLB, NFL, NHL, AHL)_<br />Produce a per-team rating to identify weaker teams to target (higher rating = weaker team, better for the selection) |

__Processing__
| Method | Notes |
|--|--|
| `*_scoring` | The main logic to determine how each selection has performed towards the game's target |
| `*_stress` | Produce a numeric rating to indicate how stressful each selection would have been to make (higher rating = more stressful) |

```sql
#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_{game_code}_{score_type}_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_{game_code}_{score_type}_preview`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for {sport}:{sel_type}:{game_code}:{score_type}'
BEGIN

  -- This method should populate `tmp_link_stats` with a numeric value (`stat_value`) and display version (`stat_text`)
  --  for each statistic this game has linked to it
  -- See `records_preview_setup()` for the base, but extensible. schema

  -- Each statistic should then make following call to apply the standard post-processing logic:
  -- CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, {stat_id}, {sort_order});

END $$

DELIMITER ;

-- Major League games only (for now...!)
#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_{game_code}_{score_type}_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_{game_code}_{score_type}_opprating`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for {sport}:{sel_type}:{game_code}:{score_type}'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, {games_lookback});

  -- Add logic here

  # Merge (split X/Y/Z Stat A/Stat B/Stat C) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    home_opp_rating TINYINT UNSIGNED,
    visitor_opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT ...;
  -- This standard method merges the above results
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_{game_code}_{score_type}_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_{game_code}_{score_type}_selrating`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for {sport}:{sel_type}:{game_code}:{score_type}'
BEGIN

  -- This method should set `FANTASY_RECORDS_SELECTIONS`.`rating` with the numeric rating we are assigning each
  --  selection in this game period

  -- The following helper method is available to standardise values from an absolute range
  -- CALL _statistical_range({table}, {raw_col}, {final_col}, {optional_mean}, {min}, {max}, {sort_order});

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_{game_code}_{score_type}_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_{game_code}_{score_type}_scoring`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for {sport}:{sel_type}:{game_code}:{score_type}'
BEGIN

  -- This method should populate `tmp_link_events` with the numeric outcome (`stat`), final status
  --  (`status` - 'positive' (good!), 'neutral' (indifferent), 'negative' (bad) or 'na' (should not count)) and
  --  display version (`summary`)

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_{sport}_{sel_type}_{game_code}_{score_type}_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_{sport}_{sel_type}_{game_code}_{score_type}_stress`(
  v_sport VARCHAR(6),
  v_season YEAR,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for {sport}:{sel_type}:{game_code}:{score_type}'
BEGIN

  -- This method should set `tmp_link_events`.`stress` with a score to indicate how stressful the selection was (in a
  --  range of 0..255, where 0 is no stress, 255 is maximum)
  -- The expectation is selections with a 'negative' status will have a score of 255

END $$

DELIMITER ;
```

## setup/records/{sport}/{season}.sql

Unlike the above two files which are created for the initial instance of a game and only tweaked once it is established,
this file should be created each season and include references to all the games to take place that season.

```sql
##
## {sport}
##
SET @sport := '{sport}';
SET @season := {season};
SET @twitter_app := 'fantasy_records';

CALL records_setup_reset(@sport, @season, @twitter_app);

-- Specify the core info for each game within this season
# {title} ({holder}, {holder_season}) // {sel_type}, {score_type}
# - {path_to_image}
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, ...) VALUES
  (@sport, @season, {game_id}, ...);
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, ...) VALUES
  (@sport, @season, {game_id}, ...);
INSERT INTO `FANTASY_RECORDS_GAMES_POSITIONS` (`sport`, `season`, `game_id`, ...) VALUES
  (@sport, @season, {game_id}, ...);
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, ...) VALUES
  (@sport, @season, {game_id}, ...);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, ...) VALUES
  (@sport, @season, {game_id}, ...);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, ...) VALUES
  (@sport, @season, {game_id}, ...);

##
## Game periods
##
-- Populate, usually via a single query, the list of game periods (which applies across all games)
INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, ...)
  SELECT ...

##
## Twitter
##
SET @app := 'records';

-- Content for each of the social media posts
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_pp, CONCAT(@sport, '_', @season, '_{tweet_ref}'), ...);

##
## Seed with automated players
##
-- For each game, determine a range of how many automated users may join
CALL records_autouser_join(@sport, @season, {game_id}, x, y); # Between x% and y% of automated users
...

##
## Admin
##
CALL records_setup_profiles(@sport, @season);
CALL records_setup_order();
```
