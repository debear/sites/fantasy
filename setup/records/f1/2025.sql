##
## F1
##
SET @sport := 'f1';
SET @season := 2025;
SET @twitter_app := 'fantasy_records';

CALL records_setup_reset(@sport, @season, @twitter_app);

# 575 Points in a Season (Max Verstappen, 2023) // ind, total
# - (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 1, '575 Point Season', 'Over the course of the season, select a driver and collect as many World Championship points as possible and see if you can topple Verstappen&#39;s all-conquering total from 2023!', 'pts', 'ind', 'total', 'HITRATE_LEADERBOARD', NULL, 576, 575, 'Max Verstappen, 2023', '2025-03-05 12:00:00', '2025-03-16 04:00:00', '2025-05-03 21:00:00', '2025-12-11 00:00:00', '2025-12-22 00:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 1, 'One (1) selection can be made every Grand Prix. All World Championship points scored by that driver will be added to your season total.
World Championship points are determined after penalties are applied, so the final finishing order may differ from the order the drivers cross the finish line.
On weekends with both a Sprint Race and a Grand Prix, World Championship points scored across the whole weekend will be counted.
If no selection is made for a Grand Prix, your team&#39;s total will neither increase nor reset to zero (0).
Selections will lock five (5) minutes prior to the advertised start time of the first Qualifying session (Q1).
An individual driver can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 1, 1, 'Avg Start', 'avg-start', 'Average starting position (after grid penalties applied)', 1, NULL, NULL),
  (@sport, @season, 1, 2, 'Avg Finish', 'avg-finish', 'Average position of races completed', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 'Championship Totals', 'Some milestones to hit along the journey to the record.', 'score', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 1, 1, 1, 100, 1, 10),
  (@sport, @season, 1, 1, 2, 200, 2, 20),
  (@sport, @season, 1, 1, 3, 300, 3, 30),
  (@sport, @season, 1, 1, 4, 400, 4, 40),
  (@sport, @season, 1, 1, 5, 500, 5, 50);

# 15 Pole Positions in a Season (Sebastian Vettel, 2011) // ind, periods
# - (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 2, '15 Pole Position Season', 'The fastest car doesn&#39;t always win the race, but it does do well in qualifying. Pick a pole sitter and tackle a record from Vettel&#39;s dominant 2011!', 'poles', 'ind', 'periods', '', NULL, 16, 15, 'Sebastian Vettel, 2011', '2025-03-05 12:00:00', '2025-03-16 05:00:00', '2025-05-03 21:00:00', '2025-12-11 00:00:00', '2025-12-22 00:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 2, 'One (1) selection can be made every Grand Prix. If that driver is credited with Pole Position, your total will increase by one (1). If he does not, your score will remain unchanged.
Pole Position is determined after grid penalties are applied, so the driver who sets the fastest time in the final Qualifying session (Q3) may not be credited with Pole Position.
On weekends with both a Sprint Race and a Grand Prix, only Pole Position for the Grand Prix will be counted.
If no selection is made for a Grand Prix, your team&#39;s total will neither increase nor reset to zero (0).
Selections will lock five (5) minutes prior to the advertised start time of the first Qualifying session (Q1).
An individual driver can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 2, 1, 'Avg Start', 'avg-start', 'Average starting position (after grid penalties applied)', 1, NULL, NULL),
  (@sport, @season, 2, 2, 'Q3 App', 'q3-app', 'Appearances in the Q3 session', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 'Pole Positions', 'Some milestones to hit along the journey to the record.', 'score', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 2, 1, 1, 1, 1, 10),
  (@sport, @season, 2, 1, 2, 3, 2, 20),
  (@sport, @season, 2, 1, 3, 6, 3, 30),
  (@sport, @season, 2, 1, 4, 9, 4, 40),
  (@sport, @season, 2, 1, 5, 12, 5, 50);

# 10 Fastest Laps in a Season (Michael Schumacher, 2004; Kimi Raikkonen 2005, 2008) // ind, periods
# - Schumacher: https://static.independent.co.uk/2021/09/16/09/GettyImages-52964558.jpg?quality=75&width=640&auto=webp
# - Raikkonen: (Use own)
INSERT INTO `FANTASY_RECORDS_GAMES` (`sport`, `season`, `game_id`, `name`, `description`, `game_code`, `sel_type`, `score_type`, `config_flags`, `active_limit`, `target`, `record`, `record_holder`, `date_open`, `date_start`, `date_close`, `date_end`, `date_complete`) VALUES
  (@sport, @season, 3, '10 Fastest Lap Season', 'Although the big points are handed out for being consistently fast, can you identify the winner of the race-within-a-race to be the ultimate fastest?', 'fl', 'ind', 'periods', '', NULL, 11, 10, 'Michael Schumacher, 2004; Kimi R&auml;ikk&ouml;nen, 2005, 2008', '2025-03-05 12:00:00', '2025-03-16 04:00:00', '2025-05-03 21:00:00', '2025-12-11 00:00:00', '2025-12-22 00:00:00');
INSERT INTO `FANTASY_RECORDS_GAMES_RULES` (`sport`, `season`, `game_id`, `rules`) VALUES
  (@sport, @season, 3, 'One (1) selection can be made every Grand Prix. If that driver is credited with the Fastest Lap, your total will increase by one (1). If he does not, your score will remain unchanged.
Fastest Laps are determined after &quot;track limit&quot; penalties are applied, so lap times that are subsequently deleted will not be counted when determining the race&#39;s Fastest Lap.
On weekends with both a Sprint Race and a Grand Prix, only the Fastest Lap from the Grand Prix will be counted.
If no selection is made for a Grand Prix, your team&#39;s total will neither increase nor reset to zero (0).
Selections will lock five (5) minutes prior to the advertised start time of the Grand Prix.
An individual driver can be selected multiple times during the season.');
INSERT INTO `FANTASY_RECORDS_GAMES_STATS` (`sport`, `season`, `game_id`, `stat_id`, `title`, `code`, `description`, `disp_order`, `last_period`, `last_calced`) VALUES
  (@sport, @season, 3, 1, 'Fastest Laps', 'fastest-laps', 'Fastest Laps achieved', 1, NULL, NULL),
  (@sport, @season, 3, 2, 'Avg Finish', 'avg-finish', 'Average position of races completed', 2, NULL, NULL);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (`sport`, `season`, `game_id`, `achieve_id`, `name`, `descrip`, `type`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 'Fastest Laps', 'Some milestones to hit along the journey to the record.', 'score', 10);
INSERT INTO `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (`sport`, `season`, `game_id`, `achieve_id`, `level`, `value`, `bit_flag`, `disp_order`) VALUES
  (@sport, @season, 3, 1, 1, 1, 1, 10),
  (@sport, @season, 3, 1, 2, 3, 2, 20),
  (@sport, @season, 3, 1, 3, 5, 3, 30),
  (@sport, @season, 3, 1, 4, 7, 4, 40),
  (@sport, @season, 3, 1, 5, 9, 5, 50);

##
## Game dates (storing as race day)
##
SELECT `round` INTO @v_final_race
FROM `debearco_sports`.`SPORTS_FIA_RACES`
WHERE `season` = @season
AND   `series` = @sport
ORDER BY `race_time` DESC
LIMIT 1;

INSERT INTO `FANTASY_COMMON_PERIODS_DATES` (`sport`, `season`, `period_id`, `name`, `name_short`, `icon`, `icon_type`, `start_date`, `end_date`, `period_order`, `summarise`)
  SELECT `GAME`.`sport`,
         `BASE`.`season`,
         `BASE`.`round` AS `period_id`,
         `BASE`.`name_full` AS `name`,
         `BASE`.`name_short`,
         `BASE`.`flag` AS `icon`,
         'flag' AS `icon_type`,
         DATE(IFNULL(DATE_ADD(IFNULL(`PREV`.`race2_time`, `PREV`.`race_time`), INTERVAL 3 DAY), `GAME`.`date_start`)) AS `start_date`,
         DATE(IF(`NEXT`.`round_order` IS NOT NULL, DATE_ADD(IFNULL(`BASE`.`race2_time`, `BASE`.`race_time`), INTERVAL 2 DAY), DATE_SUB(`GAME`.`date_end`, INTERVAL 1 SECOND))) AS `end_date`,
         `BASE`.`round_order` AS `period_order`,
         `BASE`.`round` <> @v_final_race AS `summarise` -- Not the final race
  FROM `debearco_sports`.`SPORTS_FIA_RACES` AS `BASE`
  LEFT JOIN `debearco_sports`.`SPORTS_FIA_RACES` AS `PREV`
    ON (`PREV`.`season` = `BASE`.`season`
    AND `PREV`.`series` = `BASE`.`series`
    AND `PREV`.`round_order` = `BASE`.`round_order` - 1)
  LEFT JOIN `debearco_sports`.`SPORTS_FIA_RACES` AS `NEXT`
    ON (`NEXT`.`season` = `BASE`.`season`
    AND `NEXT`.`series` = `BASE`.`series`
    AND `NEXT`.`round_order` = `BASE`.`round_order` + 1)
  LEFT JOIN `FANTASY_RECORDS_GAMES` AS `GAME`
    ON (`GAME`.`sport` = `BASE`.`series`
    AND `GAME`.`season` = `BASE`.`season`
    AND `GAME`.`game_id` = 1)
  WHERE `BASE`.`season` = @season
  AND   `BASE`.`series` = @sport
  ORDER BY `BASE`.`round_order`;

##
## Twitter
##

# Upcoming games
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_upcoming'), 1, 'As #F1 pre-season testing starts ticking over, we&#39;re gearing up for an action packed season chasing records in #DeBearRecords.');

# Registration open
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 1, 'As the first #F1 race of the season approaches, #DeBearRecords has opened its F1 championship. {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 2, 'From Championship Points, Pole Positions and Fastest Laps, there&#39;s an F1 #DeBearRecords challenge for everyone! {domain}/records'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_opening'), 3, 'The registration window is open for the various #DeBearRecords F1 challenges - see the full list at {domain}/records and good luck!');

# Game starting
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_starting'), 1, 'It&#39;s the opening race weekend for the {season} #F1 season. Have you joined #DeBearRecords quest for greatness? {domain}/records');

# Registration closing
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_closing'), 1, 'It&#39;s the last chance to create an #F1 #DeBearRecords entry. If you haven&#39;t signed up yet, do so at {domain}/records!');

# Weekly leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 1, 'After the {period_name}, the #DeBearRecords F1 points leader has {score}pt{score_s}. See the leaderboard {domain}/records/{slug} and check out how you&#39;re getting on.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 2, 'The #DeBearRecords F1 points leader has {score}pt{score_s} after the {period_num_ord} race of the season. Check out {domain}/records/{slug} to see how your entry is doing.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_1'), 3, 'With {periods_remaining_num} race{periods_remaining_num_s} to go, the #DeBearRecords F1 points leader has {score}pt{score_s}. Check out {domain}/records/{slug} for the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 1, 'With {score} pole position{score_s} after the {period_name}, there are {periods_remaining_num} #DeBearRecords race{periods_remaining_num_s} to improve. See {domain}/records/{slug} for the full leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 2, 'Check out {domain}/records/{slug} to see the #DeBearRecords F1 Pole Position leaderboard. How close are you to the leader&#39;s {score} after the {period_name}?'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_2'), 3, 'Leading the #DeBearRecords grid away with {score} pole position{score_s} after the {period_num_ord} race of the season, how does your entry compare? Check {domain}/records/{slug} for the leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 1, 'The #DeBearRecords leader has {score} fastest lap{score_s} after the {period_name}. See {domain}/records/{slug} for where you need to catch-up over the final {periods_remaining_num} race{periods_remaining_num_s}.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 2, 'At the conclusion of the {period_name}, head to {domain}/records/{slug} to see where your entry ranks behind the leader&#39;s #DeBearRecords {score} fastest lap{score_s}.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_leader_3'), 3, 'See how the #DeBearRecords Fastest Lap leaders are getting on after the {period_name} at {domain}/records/{slug}. Can you catch the leader&#39;s {score}?');

# Record beaten (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_1'), 1, 'Congratulations to {owner} for reaching {score}pts and bettering Verstappen&#39;s record of 575pts! {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_2'), 1, 'By selecting their {score_ord} pole position, {owner} has beaten Vettel&#39;s 2011 record - congratulations! {domain}/records/{slug} #DeBearRecords'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_beaten_3'), 1, 'With the {score_ord} fastest lap of the season, {owner} is the new record holder for fastest laps in a season! {domain}/records/{slug} #DeBearRecords');

# End of season leaders (per game)
INSERT INTO `debearco_common`.`COMMS_TWITTER_TEMPLATE` (`app`, `tweet_type`, `template_id`, `template_body`) VALUES
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_1'), 1, '{owner} had the most #DeBearRecords F1 championship points with {score}pt{score_s}. Congratulations! Head on over to {domain}/records/{slug} to see the final leaderboard.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_2'), 1, 'Congratulations to {owner} for leading the way with {score} #DeBearRecords pole position{score_s} this season! The final leaderboard can be seen at {domain}/records/{slug}.'),
  (@twitter_app, CONCAT(@sport, '_', @season, '_ended_3'), 1, 'Finishing with {score} fastest lap{score_s}, {owner} has top scored the #DeBearRecords Fastest Lap contest. See {domain}/records/{slug} for the full leaderboard.');

## Schedule
SET @date_start := '2025-03-15';
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_open`, INTERVAL 1 DAY)) AS `date`, 'season_soon' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 7 DAY) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 5 DAY) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 3 DAY) AS `date`, 'reg_opening' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE_SUB(@date_start, INTERVAL 1 DAY) AS `date`, 'season_tomorrow' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_close`, INTERVAL 1 DAY)) AS `date`, 'reg_closing' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;
INSERT INTO `FANTASY_RECORDS_SETUP_TWITTER` (`sport`, `season`, `game_id`, `date`, `tweet_type`)
  SELECT `sport`, `season`, `game_id`, DATE(DATE_SUB(`date_end`, INTERVAL 1 DAY)) AS `date`, 'season_ended' AS `tweet_type` FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = @sport AND `season` = @season;

##
## Seed with automated players
##
CALL records_autouser_join(@sport, @season, 1, 15, 22); # Between 15% and 22% of automated users
CALL records_autouser_join(@sport, @season, 2, 12, 19); # Between 12% and 19% of automated users
CALL records_autouser_join(@sport, @season, 3,  8, 16); # Between  8% and 16% of automated users

##
## Admin
##
CALL records_setup_profiles(@sport, @season);
CALL records_setup_order();
