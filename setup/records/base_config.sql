SET @section := 'records';
SET @season := '0000'; # Season agnostic approach
SET @email_app := 'fantasy_records';

#
# Remove any previous setup
#
DELETE FROM `FANTASY_PROFILE_GAMES` WHERE `game` = @section AND `season` = @season;
DELETE FROM `FANTASY_COMMON_ANNOUNCEMENTS` WHERE `game` = @section AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_ARTICLES` WHERE `game` = @section AND `season` = @season;
DELETE FROM `FANTASY_COMMON_HELP_SECTIONS` WHERE `game` = @section AND `season` = @season;
DELETE FROM `debearco_common`.`COMMS_EMAIL_TEMPLATE` WHERE `app` = @email_app;

#
# Profile Header Row
#
INSERT INTO `FANTASY_PROFILE_GAMES` (`game`, `game_type`, `season`, `in_progress`) VALUES
  (@section, 'record', @season, 1);

#
# Announcements
#
INSERT INTO `FANTASY_COMMON_ANNOUNCEMENTS` (`game`, `season`, `announce_id`, `section`, `type`, `subject`, `body`, `disp_start`, `disp_end`) VALUES
  (@section, @season, 1, NULL, 'success', 'Hello and welcome to DeBear Record Breakers!', '<p>Welcome to the latest addition to the DeBear Fantasy Sports portfolio! Please take the time to check out the <a href="/records/help">Help and Rules</a> section to familiarise yourselves with the rules and get ready for an action packed adventure!</p>', '2023-10-01 00:00:00', '2023-12-31 23:59:59');

#
# Help articles
#
INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`game`, `season`, `section_id`, `name`, `order`, `active`) VALUES
  (@section, @season, 1, 'Rules', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 1, 1, 'What is the aim of DeBear Record Breakers?', '<p>DeBear Record Breakers allows you to tackle some of sport&#39;s greatest records. DiMaggio&#39;s 56 game hitting streak in 1941, Dickerson&#39;s 2,105 yards rushing in 1984 or scoring 50 goals in 50 NHL games have all stood the test of time. By picking players from real-life games, can you build up streaks or totals that equal &ndash; or hopefully better &ndash; these legendary yardsticks?</p>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 1, 2, 'How do my selections contribute towards my entry&#39;s score?', '<p>Our games fall in to two categories: Totals and Streaks. In a Totals game, your selection&#39;s score in that game will be added to your entry&#39;s current total. If the selection didn&#39;t score then your entry&#39;s score won&#39;t reset to zero, it will just stay where it was. In a Streak game, your selection must score appropriately (a hit, a point, a win, etc) in that game for your streak to be extended and stay active, whereas a lack of such a score will reset your entry&#39;s score to zero. If your selection doesn&#39;t play, your entry&#39;s score won&#39t change &ndash; your Total won&#39;t increase and your Streak will neither increase nor be reset to zero.</p>\r\n<p>There are a couple of deviations from this pattern for some of our games: <em>The Perfect Season</em> is an elimination game &ndash; lose and your participation in the game is over; The <em>50 in 50</em> games only cover your selection&#39;s previous 50 games, unlike the official record which is based on their team&#39;s first 50 games.</p>', 2, 1);

INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`game`, `season`, `section_id`, `name`, `order`, `active`) VALUES
  (@section, @season, 2, 'Registration', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 2, 1, 'How do I create an entry?', '<p>To create an entry click the &quot;Create Entry&quot; link on the DeBear Record Breakers homepage and follow the instructions.</p>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 2, 2, 'How many entries can I create?', '<p>As DeBear Record Breakers is composed of several sub-games, each DeBear.uk user account is limited to a single Record Breakers entry. You can pick and choose which game(s) you wish to play &ndash; there is no requirement to play all the available games &ndash; but each game can only be entered once.</p>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 2, 3, 'When can I create an entry?', '<p>Entries can be created at any time, however each sub-game will have its own registration window in which you can start that game&#39;s challenge.</p>', 3, 1);

INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`game`, `season`, `section_id`, `name`, `order`, `active`) VALUES
  (@section, @season, 3, 'Entry Management', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 3, 1, 'When can I start editing my entry?', '<p>Entries can be edited soon after they join a game! In some instances the available information may not be available until closer to the start of the season, but we are aiming for games to launch when all the relevant information is available.</p>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 3, 2, 'Why am I unable to edit my entry?', '<p>Each selection locks five (5) minutes prior to the start of their event (game, race, etc). If a selection has been included in your entry at this time, it will be fixed in to your entry for that period, however if you have not included it then it will no longer be available to you.</p>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 3, 3, 'How many times can I use an individual selection?', '<p>Except for <em>The Perfect Season</em>, where each team can only be selected once, selections can be used as many times as you want.</p>', 3, 1);

INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`game`, `season`, `section_id`, `name`, `order`, `active`) VALUES
  (@section, @season, 4, 'Groups', 4, 0);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 4, 1, 'What are &quot;Groups&quot;?', '<p>Groups are a great way of comparing the progress of your entry(ies) against that of your friends and other like-minded fans. Instead of being ranked against every other player in the World, your progress will be compared &ndash; and more importantly ranked! &ndash; against the other entries who have joined your Group.</p>', 1, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 4, 2, 'How do I create a Group?', '<p>Groups are created from the &quot;Groups&quot; link at the top of each page. Select the &quot;Create New Group&quot; option, give your Group a name and click &quot;Create&quot;. That&#39;s it!</p>', 2, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 4, 3, 'How do I invite my friends to a Group I have just created?', '<p>Friends can be invited to your group by clicking the &quot;Invite Friends&quot; link on the <em>Group</em> page and entering the email address of the friends you would like to join. They will receive an email inviting them to join your group, and if applicable, create an entry first.</p>', 3, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 4, 4, 'How do I add my entry to an existing Group?', '<p>There are two ways to join a group a friend has created:</p>\r\n<h5>1. Using the e-mail you were sent</h5>\r\n<p>Simply click the link in the e-mail and click &quot;Add Entries&quot; to confirm.</p>\r\n\r\n<h5>2. Using the Group PIN</h5>\r\n<p>Each Group has a unique PIN &ndash; having been provided this PIN by the group&#39;s creator, go to the <em>Groups</em> page and enter the PIN and the Group&#39;s password in the &quot;Join Group via PIN&quot; section.</p>', 4, 1);

INSERT INTO `FANTASY_COMMON_HELP_SECTIONS` (`game`, `season`, `section_id`, `name`, `order`, `active`) VALUES
  (@section, @season, 5, 'Contact Us', 5, 1);
INSERT INTO `FANTASY_COMMON_HELP_ARTICLES` (`game`, `season`, `section_id`, `article_id`, `title`, `body`, `section_order`, `active`) VALUES
  (@section, @season, 5, 1, 'I still have a problem or unanswered question, what do I do?', '<p>The best thing to do in this case is to email us on <a href="mailto:support@debear.uk">support@debear.uk</a>. We will get back to you as soon as we can, however if you are having problems it would really help us if you could provide as much information as possible &ndash; things like what you were trying to do and any error messages that appeared will help us better understand your problem and how we can resolve it. Please don&#39;t worry if you feel unable to provide such information though, your problem will not go unresolved if you do not provide it!</p>', 1, 1);

#
# Email templates
#
INSERT INTO `debearco_common`.`COMMS_EMAIL_TEMPLATE` (`app`, `template_id`, `ref`, `wrapper`, `from`, `subject`, `summary`, `message`, `clause`) VALUES
  (@email_app, 1, 'Fantasy Records: Create Entry', 'fantasy.records.entry.create', 'DeBear Fantasy Sports <{email-raw|noreply}>', 'Welcome to {config|debear.names.external}', NULL, NULL, NULL),
  (@email_app, 2, 'Fantasy Records: End-of-Game', 'fantasy.records.entry.game-end', 'DeBear Fantasy Sports <{email-raw|noreply}>', 'Your {config|debear.names.external} {data|sport} Update', NULL, NULL, NULL);
