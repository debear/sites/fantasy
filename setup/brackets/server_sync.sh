#!/bin/bash
# Create/Update a server sync file for a Bracket Challenge game

dir_base=$(dirname $0)
dir_helpers=$(realpath $dir_base/../../tests/_scripts)
source $dir_helpers/helpers.sh

# Wrapper mode to automate creation of all sync scripts for all the latest games
if [ ! -z "$1" ] && [ "x$1" == 'x--all' ]; then
    me=$(basename $0)
    display_title "Processing server_sync scripts for all available games"
    for proc_sport in $(find $dir_base -mindepth 1 -maxdepth 1 -type d -exec basename -a {} \+ | sort -n); do
        echo; display_section -n "$proc_sport: "
        # And re-call ourselves with the sport argument
        $dir_base/$me $proc_sport
        retcode=$?
        if [ $retcode -gt 0 ]; then
            exit $retcode
        fi
    done
    # Upon success, we exit the wrapper functionality here
    exit
fi

# Processing an individual combination
sport="$1"

# Quick argument validation
if [ -z "$sport" ] || [ ! -e "$dir_base/$sport" ]; then
    echo -e "Invalid arguments.\n$0 sport" >&2
    exit 1
fi

# Determine the season
season=$(date +'%Y')
curr_month=$(date +'%m' | sed 's/^0//')
if [[ "${sport}" == 'nfl' && $curr_month -le 4 ]] || [[ "${sport}" == 'nhl' && $curr_month -le 8 ]] || [[ "${sport}" == 'ahl' && $curr_month -le 8 ]]; then
    season=$(( $season - 1 ))
fi

# Generate the file
file="$dir_base/$sport/server_sync.sql"
cat << EOF >$file
#
# Bracket Challenge scoring prep
#
#SET @sync_app := 'fantasy_brackets_${sport}_selections';
#
#INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
#    (@sync_app, 'Fantasy ${sport^^} Bracket Challenge (Down)', 'live', 'dev,data', 1);
##INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES

#
# Bracket Challenge scoring upload
#
SET @sync_app := 'fantasy_brackets_${sport}_scoring';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy ${sport^^} Bracket Challenge (Up)', 'dev,data', 'data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Setup
    (@sync_app, 1, '__DIR__ FANTASY_BRACKETS_SETUP_DATES', 10, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Setup
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_BRACKETS_SETUP_DATES', 'sport = ''${sport}'' AND season = ''${season}''');

#
# Bracket Challenge dev sync
#
SET @sync_app := 'fantasy_brackets_${sport}_dev';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy ${sport^^} Bracket Challenge (Dev)', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Setup
    (@sync_app, 1, '__DIR__ FANTASY_BRACKETS_SETUP_DATES', 10, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Setup
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_BRACKETS_SETUP_DATES', 'sport = ''${sport}'' AND season = ''${season}''');

#
# Bracket Challenge game setup
#
SET @sync_app := 'fantasy_brackets_${sport}_setup';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy ${sport^^} Bracket Challenge (Setup)', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Setup
    (@sync_app, 1, '__DIR__ FANTASY_BRACKETS_SETUP_DATES', 10, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Setup
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_BRACKETS_SETUP_DATES', 'sport = ''${sport}'' AND season = ''${season}''');
EOF

# Feed back
display_info -n "$sport"; echo -n " server_sync written to "; display_info -n $file; echo ":"
ls -l $file
