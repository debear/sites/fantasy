#
# Bracket Challenge scoring prep
#
#SET @sync_app := 'fantasy_brackets_nfl_selections';
#
#INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
#    (@sync_app, 'Fantasy NFL Bracket Challenge (Down)', 'live', 'dev,data', 1);
##INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES

#
# Bracket Challenge scoring upload
#
SET @sync_app := 'fantasy_brackets_nfl_scoring';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL Bracket Challenge (Up)', 'dev,data', 'data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Setup
    (@sync_app, 1, '__DIR__ FANTASY_BRACKETS_SETUP_DATES', 10, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Setup
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_BRACKETS_SETUP_DATES', 'sport = ''nfl'' AND season = ''2024''');

#
# Bracket Challenge dev sync
#
SET @sync_app := 'fantasy_brackets_nfl_dev';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL Bracket Challenge (Dev)', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Setup
    (@sync_app, 1, '__DIR__ FANTASY_BRACKETS_SETUP_DATES', 10, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Setup
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_BRACKETS_SETUP_DATES', 'sport = ''nfl'' AND season = ''2024''');

#
# Bracket Challenge game setup
#
SET @sync_app := 'fantasy_brackets_nfl_setup';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active) VALUES
    (@sync_app, 'Fantasy NFL Bracket Challenge (Setup)', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type) VALUES
    -- Setup
    (@sync_app, 1, '__DIR__ FANTASY_BRACKETS_SETUP_DATES', 10, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause) VALUES
    -- Setup
    (@sync_app, 1, 'debearco_fantasy', 'FANTASY_BRACKETS_SETUP_DATES', 'sport = ''nfl'' AND season = ''2024''');
