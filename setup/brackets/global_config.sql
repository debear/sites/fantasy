SET @email_app := 'fantasy_brackets';

#
# Remove any previous setup
#
DELETE FROM `debearco_common`.`COMMS_EMAIL_TEMPLATE` WHERE `app` = @email_app;

#
# Email templates
#
INSERT INTO `debearco_common`.`COMMS_EMAIL_TEMPLATE` (`app`, `template_id`, `ref`, `wrapper`, `from`, `subject`, `summary`, `message`, `clause`) VALUES
  (@email_app, 1, 'Fantasy Brackets: Create Entry', 'fantasy.brackets.entry.create', 'DeBear Fantasy Sports <{email-raw|noreply}>', 'Welcome to {config|debear.names.external}', NULL, NULL, NULL);
