<?php

/**
 * MLB playoff bracket config
 */

// Use the current date to determine default game info and timings.
$season = intval(date('Y')); // Base on calendar years.

return [
    'enabled' => false,

    /*
     * Auto-calced game timings (in US/Eastern equivalents), which may be updated within _env.php
     */
    'dates' => [
        'visible' => "$season-09-01 05:00:00", // From the final month of the season.
        'hide' => "$season-12-01 04:59:59", // Until the end of November.
    ],

    /*
     * General setup about the game
     */
    'setup' => [
        'sport_ref' => 'mlb',
        // Standard box colour.
        'box' => 'grass',
        // Season information.
        'season' => [
            'max' => $season,
        ],
        // Bracket templates / logic by season.
        'templates' => [
            'mlb-1' => @require 'templates/mlb-1.php',
        ],
        // Column (suffix) by which the playoff series is scored.
        'series_score_col' => 'games',
    ],

    /*
     * Site name details
     */
    'names' => [
        'external' => 'DeBear MLB Bracket Challenge',
        'section' => 'MLB Bracket Challenge',
    ],

    /*
     * Game resource additions
     */
    'resources' => [
        'css' => [
            'brackets/layout/mlb.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/mlb-bracket.png',
        ],
        'theme_colour' => '#324f17',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'descrip' => 'Can you predict how the MLB playoff bracket will finish, and who will be crowned the next'
                . ' World Series champion?',
            'summary' => 'Decide who you think will win each matchup during the MLB playoffs, culminating in your World'
                . ' Series champion, and see how accurate you have been.',
        ],
    ],
];
