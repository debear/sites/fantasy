<?php

/**
 * NFL playoff bracket logic: Version 1
 */

return [
    // Applies from 2020 onwards.
    'from' => 2020,
    'to' => 2099,
    // The rounds and series within each round.
    'rounds' => [
        // Wild Card Round, with prefix '1'.
        1 => [
            'grouping' => 'conf',
            'matchups' => [
                1 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 7],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 3],
                        'lower' => ['type' => 'seed', 'rank' => 6],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                ],
                0 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 7],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 3],
                        'lower' => ['type' => 'seed', 'rank' => 6],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                ],
            ],
        ],
        // Divisional Round, with prefix '2'.
        2 => [
            'grouping' => 'conf',
            'matchups' => [
                1 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'winner', 'rank' => 2],
                        'lower' => ['type' => 'winner', 'rank' => 3],
                    ],
                ],
                0 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'winner', 'rank' => 2],
                        'lower' => ['type' => 'winner', 'rank' => 3],
                    ],
                ],
            ],
        ],
        // Conference Championship, with prefix '3'.
        3 => [
            'grouping' => 'conf',
            'matchups' => [
                1 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                0 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
            ],
        ],
        // Super Bowl, with prefix '4'.
        4 => [
            'grouping' => 'champ',
            'matchup' => [
                'higher' => ['group_id' => 1, 'type' => 'winner', 'rank' => 1],
                'lower' => ['group_id' => 0, 'type' => 'winner', 'rank' => 1],
            ],
        ],
    ],
];
