<?php

/**
 * NHL playoff bracket logic: Version 1
 */

return [
    // Applies from 2013/14 onwards.
    'from' => 2013,
    'to' => 2099,
    // There are three teams per division, anyone else is classed as a "wildcard".
    'div_teams' => 3,
    // The rounds and series within each round.
    'rounds' => [
        // Divisional Semi-Finals, with prefix '1'.
        1 => [
            'grouping' => 'div',
            'alt_round_num' => 0,
            'alt_codes' => range(1, 8),
            'matchups' => [
                12 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'seed', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 3],
                    ],
                ],
                24 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'seed', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 3],
                    ],
                ],
                15 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'seed', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 3],
                    ],
                ],
                17 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'seed', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 3],
                    ],
                ],
            ],
        ],
        // Divisional Finals, with prefix '2'.
        2 => [
            'grouping' => 'div',
            'matchups' => [
                12 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                24 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                15 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                17 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
            ],
        ],
        // Conference Finals, with prefix '3'.
        3 => [
            'grouping' => 'conf',
            'matchups' => [
                4 => [
                    [
                        'higher' => ['group_id' => 12, 'type' => 'winner', 'rank' => 1],
                        'lower' => ['group_id' => 24, 'type' => 'winner', 'rank' => 1],
                    ],
                ],
                5 => [
                    [
                        'higher' => ['group_id' => 15, 'type' => 'winner', 'rank' => 1],
                        'lower' => ['group_id' => 17, 'type' => 'winner', 'rank' => 1],
                    ],
                ],
            ],
        ],
        // Calder Cup Finals, with prefix '4'.
        4 => [
            'grouping' => 'champ',
            'matchup' => [
                'higher' => ['group_id' => 5, 'type' => 'winner', 'rank' => 1],
                'lower' => ['group_id' => 4, 'type' => 'winner', 'rank' => 1],
            ],
        ],
    ],
];
