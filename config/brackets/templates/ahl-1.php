<?php

/**
 * AHL playoff bracket logic: Version 1
 */

return [
    // Applies from 2021/22 onwards.
    'from' => 2021,
    'to' => 2099,
    // The rounds and series within each round.
    'rounds' => [
        // First round, with prefix '0'.
        0 => [
            'grouping' => 'div',
            'matchups' => [
                44 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 3],
                        'lower' => ['type' => 'seed', 'rank' => 6],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                ],
                47 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                ],
                56 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                ],
                57 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 7],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 3],
                        'lower' => ['type' => 'seed', 'rank' => 6],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                ],
            ],
        ],
        // Divisional Semi-Finals, with prefix '1'.
        1 => [
            'grouping' => 'div',
            'matchups' => [
                44 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'winner', 'rank' => 3],
                    ],
                ],
                47 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 3],
                    ],
                ],
                56 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'seed', 'rank' => 3],
                    ],
                ],
                57 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'winner', 'rank' => 2],
                        'lower' => ['type' => 'winner', 'rank' => 3],
                    ],
                ],
            ],
        ],
        // Divisional Finals, with prefix '2'.
        2 => [
            'grouping' => 'div',
            'matchups' => [
                44 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                47 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                56 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                57 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
            ],
        ],
        // Conference Finals, with prefix '3'.
        3 => [
            'grouping' => 'conf',
            'matchups' => [
                42 => [
                    [
                        'higher' => ['group_id' => 44, 'type' => 'winner', 'rank' => 1],
                        'lower' => ['group_id' => 47, 'type' => 'winner', 'rank' => 1],
                    ],
                ],
                43 => [
                    [
                        'higher' => ['group_id' => 56, 'type' => 'winner', 'rank' => 1],
                        'lower' => ['group_id' => 57, 'type' => 'winner', 'rank' => 1],
                    ],
                ],
            ],
        ],
        // Calder Cup Finals, with prefix '4'.
        4 => [
            'grouping' => 'champ',
            'matchup' => [
                'higher' => ['group_id' => 43, 'type' => 'winner', 'rank' => 1],
                'lower' => ['group_id' => 42, 'type' => 'winner', 'rank' => 1],
            ],
        ],
    ],
];
