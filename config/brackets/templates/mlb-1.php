<?php

/**
 * MLB playoff bracket logic: Version 1
 */

return [
    // Applies from 2022 onwards.
    'from' => 2022,
    'to' => 2099,
    // The rounds and series within each round.
    'rounds' => [
        // Wild Card Series, with prefix '1'.
        1 => [
            'grouping' => 'conf',
            'matchups' => [
                2 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 3],
                        'lower' => ['type' => 'seed', 'rank' => 6],
                    ],
                ],
                3 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 4],
                        'lower' => ['type' => 'seed', 'rank' => 5],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 3],
                        'lower' => ['type' => 'seed', 'rank' => 6],
                    ],
                ],
            ],
        ],
        // Divisional Series, with prefix '2'.
        2 => [
            'grouping' => 'conf',
            'matchups' => [
                2 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'winner', 'rank' => 3],
                    ],
                ],
                3 => [
                    [
                        'higher' => ['type' => 'seed', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 4],
                    ],
                    [
                        'higher' => ['type' => 'seed', 'rank' => 2],
                        'lower' => ['type' => 'winner', 'rank' => 3],
                    ],
                ],
            ],
        ],
        // League Championship Series, with prefix '3'.
        3 => [
            'grouping' => 'conf',
            'matchups' => [
                2 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
                3 => [
                    [
                        'higher' => ['type' => 'winner', 'rank' => 1],
                        'lower' => ['type' => 'winner', 'rank' => 2],
                    ],
                ],
            ],
        ],
        // World Series, with prefix '4'.
        4 => [
            'grouping' => 'champ',
            'matchup' => [
                'higher' => ['group_id' => 2, 'type' => 'winner', 'rank' => 1],
                'lower' => ['group_id' => 3, 'type' => 'winner', 'rank' => 1],
            ],
        ],
    ],
];
