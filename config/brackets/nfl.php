<?php

/**
 * NFL playoff bracket config
 */

// Use the current date to determine default game info and timings.
list($y, $m) = explode('-', date('Y-n'));
$season = ($m >= 4 ? $y : $y - 1); // Use April as the divide between seasons.

return [
    'enabled' => false,

    /*
     * Auto-calced game timings (in US/Eastern equivalents), which may be updated within _env.php
     */
    'dates' => [
        'visible' => "$season-12-01 05:00:00", // From the final month of the season.
        'hide' => ($season + 1) . '-03-01 04:59:59', // Until the end of February.
    ],

    /*
     * General setup about the game
     */
    'setup' => [
        'sport_ref' => 'nfl',
        // Standard box colour.
        'box' => 'grass',
        // Season information.
        'season' => [
            'max' => $season,
        ],
        // Bracket templates / logic by season.
        'templates' => [
            'nfl-1' => @require 'templates/nfl-1.php',
        ],
        // Column (suffix) by which the playoff series is scored.
        'series_score_col' => 'score',
    ],

    /*
     * Site name details
     */
    'names' => [
        'external' => 'DeBear NFL Bracket Challenge',
        'section' => 'NFL Bracket Challenge',
    ],

    /*
     * Game resource additions
     */
    'resources' => [
        'css' => [
            'brackets/layout/nfl.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/nfl-bracket.png',
        ],
        'theme_colour' => '#324f17',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'descrip' => 'Can you predict how the NFL playoff bracket will finish, and who will be crowned the next'
                . ' Super Bowl champion?',
            'summary' => 'Decide who you think will win each matchup during the NFL playoffs, culminating in your Super'
                . ' Bowl champion, and see how accurate you have been.',
        ],
    ],
];
