<?php

/**
 * NHL playoff bracket config
 */

// Use the current date to determine default game info and timings.
list($y, $m) = explode('-', date('Y-n'));
$season = ($m >= 8 ? $y : $y - 1); // Use August as the divide between seasons.
$season_n = ($season + 1);

return [
    'enabled' => false,

    /*
     * Auto-calced game timings (in US/Eastern equivalents), which may be updated within _env.php
     */
    'dates' => [
        'visible' => "$season_n-03-15 04:00:00", // From the final month of the season (when US is 4 hrs behind GMT).
        'hide' => "$season_n-08-01 04:59:59", // Until the end of July.
    ],

    /*
     * General setup about the game
     */
    'setup' => [
        'sport_ref' => 'nhl',
        // Standard box colour.
        'box' => 'ice',
        // Season information.
        'season' => [
            'max' => $season,
        ],
        // Bracket templates / logic by season.
        'templates' => [
            'nhl-1' => @require 'templates/nhl-1.php',
        ],
        // Column (suffix) by which the playoff series is scored.
        'series_score_col' => 'games',
    ],

    /*
     * Site name details
     */
    'names' => [
        'external' => 'DeBear NHL Bracket Challenge',
        'section' => 'NHL Bracket Challenge',
    ],

    /*
     * Game resource additions
     */
    'resources' => [
        'css' => [
            'brackets/layout/nhl.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/nhl-bracket.png',
        ],
        'theme_colour' => '#4f94cd',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'descrip' => 'Can you predict how the NHL playoff bracket will finish, and who will be crowned the next'
                . ' Stanley Cup champion?',
            'summary' => 'Decide who you think will win each matchup during the NHL playoffs, culminating in your'
                . ' Stanley Cup champion, and see how accurate you have been.',
        ],
    ],
];
