<?php

/**
 * AHL playoff bracket config
 */

// Use the current date to determine default game info and timings.
list($y, $m) = explode('-', date('Y-n'));
$season = ($m >= 8 ? $y : $y - 1); // Use August as the divide between seasons.
$season_n = ($season + 1);

return [
    'enabled' => false,

    /*
     * Auto-calced game timings (in US/Eastern equivalents), which may be updated within _env.php
     */
    'dates' => [
        'visible' => "$season_n-03-22 04:00:00", // From the final month of the season (when US is 4 hrs behind GMT).
        'hide' => "$season_n-08-01 04:59:59", // Until the end of July.
    ],

    /*
     * General setup about the game
     */
    'setup' => [
        'sport_ref' => 'ahl',
        // Standard box colour.
        'box' => 'ice',
        // Season information.
        'season' => [
            'max' => $season,
        ],
        // Bracket templates / logic by season.
        'templates' => [
            'ahl-1' => @require 'templates/ahl-1.php',
        ],
        // Column (suffix) by which the playoff series is scored.
        'series_score_col' => 'games',
    ],

    /*
     * Site name details
     */
    'names' => [
        'external' => 'DeBear AHL Bracket Challenge',
        'section' => 'AHL Bracket Challenge',
    ],

    /*
     * Game resource additions
     */
    'resources' => [
        'css' => [
            'brackets/layout/ahl.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/ahl-bracket.png',
        ],
        'theme_colour' => '#4f94cd',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'descrip' => 'Can you predict how the AHL playoff bracket will finish, and who will be crowned the next'
                . ' Calder Cup champion?',
            'summary' => 'Decide who you think will win each matchup during the AHL playoffs, culminating in your'
                . ' Calder Cup champion, and see how accurate you have been.',
        ],
    ],
];
