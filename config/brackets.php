<?php

/**
 * Global playoff bracket config
 */

return [
    /*
     * Date/Time configuration
     */
    'datetime' => [
        // Database operates in US/Eastern.
        'timezone_db' => 'US/Eastern',
        'timezone_app' => 'US/Eastern',
    ],

    /*
     * General setup about the game
     */
    'setup' => [
        'season' => [
            'min' => 2025, // We first launched in time for the 2025 playoffs.
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'my-entry' => [
            'url' => '/my-entry',
            'label' => 'My Entry',
            'order' => 88,
            'url-section' => true,
            'policy' => 'user:unrestricted', // Do not include for Restricted users.
            'header' => true,
            'navigation' => [
                'enabled' => true,
                'class' => 'mobile-only',
                'order' => 50,
            ],
        ],
    ],

    /*
     * Form customisations
     */
    'forms' => [
        'namespaces' => [
            'models' => 'Brackets',
            'cache' => 'brackets',
            'email' => 'Fantasy Brackets',
            'audit' => 'brackets',
            'session' => 'brackets',
        ],
        'overlays' => [
            'entry' => [
                'interpolate' => [
                    'mapping' => [
                        'DeBear\Models\Fantasy\Brackets\Entry' => [
                            'fixed_fields' => [
                                'sport' => [
                                    'string' => '{config|debear.setup.sport_ref}',
                                ],
                                'season' => [
                                    'string' => '{config|debear.setup.season.viewing}',
                                ],
                            ],
                        ],
                    ],
                ],
                'raw' => [
                    'next' => [
                        'redirect' => '{session|fantasy.brackets.entry-form.redirect}',
                    ],
                ],
                'views' => [
                    'form_complete' => 'fantasy.brackets.common.actions.success',
                ],
            ],
        ],
    ],

    /*
     * Audit types
     */
    'logging' => [
        'user_activity' => [
            'brackets_entry_create' => 50,
            'brackets_entry_update' => 51,
        ],
    ],
];
