<?php

use DeBear\Helpers\Arrays;

/* Global config */
$subconfig = [
    /*
     * Site name details
     */
    'names' => [
        'site' => 'DeBear Fantasy Sports',
        'section' => 'DeBear Fantasy Sports',
    ],

    /*
     * Site-specific resource additions
     */
    'resources' => [
        'css' => [
            '_common/layout.css',
            '_common/general.css',
            'logo_icons.css',
        ],
        'js' => [
            'skel/widgets/storage.js',
        ],
    ],

    /*
     * General / Global setup
     */
    'setup' => [
        // Automated players.
        'automated_players' => [
            'user_ids' => [
                'min' => 60351,
                'max' => 65535,
                'batch' => 30,
            ],
            // Approximate ratio of personality types.
            'personalities' => [
                'stats' => 5,
                'rating' => 7,
                'opp_rating' => 2, // For motorsport games, this is the equivalent to 'rating'.
                'random' => 3,
                'variable' => 3,
            ],
            'locales' => include __DIR__ . '/autouser/locales.php',
        ],
        // Player profiles.
        'profile' => [
            'unit' => ' <i class="fas fa-search-dollar"></i>',
            'levels' => [
                25 => 'Prospect',
                50 => 'Regular',
                65 => 'Veteran',
                80 => 'Superstar',
                100 => 'Legend!'
            ],
        ],
        // Available medals to be won.
        'medals' => [
            'overall-winner' => [
                'title' => 'Overall Winner',
                'info'  => 'Awarded to the top team in the overall standings',
                'icon'  => 'medal_overall_1',
                'instances' => [1],
                'rank'  => 10,
            ],
            'top-5-finisher' => [
                'title' => 'Top 5 Finisher',
                'info'  => 'Awarded to teams that finish in the Top 5 Overall',
                'icon'  => 'medal_overall_5',
                'instances' => [1, 3, 10],
                'rank'  => 20,
            ],
            'top-30-finisher' => [
                'title' => 'Top 30 Finisher',
                'info'  => 'Awarded to teams that finish in the Top 30 Overall',
                'icon'  => 'medal_overall_30',
                'instances' => [1, 5, 15],
                'rank'  => 30,
            ],
            'group-winner' => [
                'title' => 'Group Winner',
                'info'  => 'Awarded to the top team in each group',
                'icon'  => 'medal_group_1',
                'instances' => [1, 10, 30],
                'rank'  => 40,
            ],
            'group-top-3' => [
                'title' => 'Top 3 Group Finisher',
                'info'  => 'Awarded to teams that finish in the Top 3 of a group',
                'icon'  => 'medal_group_3',
                'instances' => [1, 20, 60],
                'rank'  => 50,
            ],
        ],
        // Event status code to display mappings.
        'event_statuses' => [
            'PPD' => 'Postponed',
            'CNC' => 'Cancelled',
            'SSP' => 'Suspended',
            'SCH' => 'Scheduled',
        ],
        // By default, events lock five minutes before the advertised start time.
        'lock_offset' => 5,
    ],

    /*
     * Sport-specific config
     */
    'sports' => [
        // NFL.
        'nfl' => [
            'name_short' => 'NFL',
            'box' => 'grass',
            'namespace' => 'MajorLeague',
            'section' => 'NFL',
            'contest_type' => 'season',
            'event_type' => 'game',
            'weather' => [
                'type' => 'sel',
                'unit' => 'f'
            ],
            'period' => [
                'class' => 'Schedule',
                'ref_col' => 'period_id',
                'sport_col' => 'week',
                'lock_time' => 'CONCAT(TBL.game_date, " ", TBL.game_time)',
                'format' => 'D, H:i',
                'lineup_refresh' => 0,
            ],
            'ind' => [
                'name' => 'player',
                'class' => 'Player',
                'icon' => 'icon team-nfl-',
                'sports_path' => 'players',
            ],
            'team' => [
                'name' => 'team',
                'class' => 'Team',
                'icon' => 'icon team-nfl-',
                'sports_path' => 'teams',
            ],
        ],
        // MLB.
        'mlb' => [
            'name_short' => 'MLB',
            'box' => 'grass',
            'namespace' => 'MajorLeague',
            'section' => 'MLB',
            'contest_type' => 'season',
            'event_type' => 'game',
            'weather' => [
                'type' => 'sel',
                'unit' => 'f'
            ],
            'period' => [
                'class' => 'Schedule',
                'ref_col' => 'end_date',
                'sport_col' => 'game_date',
                'lock_time' => 'CONCAT(TBL.game_date, " ", TBL.game_time)',
                'format' => 'H:i',
                'lineup_refresh' => 60,
            ],
            'ind' => [
                'name' => 'player',
                'class' => 'Player',
                'icon' => 'icon team-mlb-',
                'sports_path' => 'players',
            ],
            'team' => [
                'name' => 'team',
                'class' => 'Team',
                'icon' => 'icon team-mlb-',
                'sports_path' => 'teams',
            ],
        ],
        // NHL.
        'nhl' => [
            'name_short' => 'NHL',
            'box' => 'ice',
            'namespace' => 'MajorLeague',
            'section' => 'NHL',
            'contest_type' => 'season',
            'event_type' => 'game',
            'weather' => false,
            'period' => [
                'class' => 'Schedule',
                'ref_col' => 'end_date',
                'sport_col' => 'game_date',
                'lock_time' => 'CONCAT(TBL.game_date, " ", TBL.game_time)',
                'format' => 'H:i',
                'lineup_refresh' => 30,
            ],
            'ind' => [
                'name' => 'player',
                'class' => 'Player',
                'icon' => 'icon team-nhl-',
                'sports_path' => 'players',
            ],
            'team' => [
                'name' => 'team',
                'class' => 'Team',
                'icon' => 'icon team-nhl-',
                'sports_path' => 'teams',
            ],
        ],
        // AHL.
        'ahl' => [
            'name_short' => 'AHL',
            'box' => 'ice',
            'namespace' => 'MajorLeague',
            'section' => 'AHL',
            'contest_type' => 'season',
            'event_type' => 'game',
            'weather' => false,
            'period' => [
                'class' => 'Schedule',
                'ref_col' => 'end_date',
                'sport_col' => 'game_date',
                'lock_time' => 'CONCAT(TBL.game_date, " ", TBL.game_time)',
                'format' => 'H:i',
                'lineup_refresh' => 0,
            ],
            'ind' => [
                'name' => 'player',
                'class' => 'Player',
                'icon' => 'icon team-ahl-',
                'sports_path' => 'players',
            ],
            'team' => [
                'name' => 'team',
                'class' => 'Team',
                'icon' => 'icon team-ahl-',
                'sports_path' => 'teams',
            ],
        ],
        // Formula One.
        'f1' => [
            'name_short' => 'F1',
            'box' => 'track',
            'namespace' => 'Motorsport',
            'section' => 'F1',
            'sports_class' => 'FIA',
            'contest_type' => 'championship',
            'event_type' => 'race',
            'weather' => [
                'type' => 'period',
                'unit' => 'c',
            ],
            'period' => [
                'class' => 'Race',
                'ref_col' => 'period_id',
                'sport_col' => 'round',
                'lock_time' => [
                    'pts' => 'TBL.qual_time',
                    'poles' => 'TBL.qual_time',
                    'fl' => 'IFNULL(TBL.race2_time, TBL.race_time)',
                ],
                'format' => 'D jS, H:i',
                'lineup_refresh' => 0,
            ],
            'ind' => [
                'name' => 'driver',
                'class' => 'Participant',
                'icon' => 'flag_right flag16_right_',
                'sports_path' => 'drivers',
            ],
            'status' => [
                'inj' => ['short' => 'Inj', 'long' => 'Injured'],
                'ne' => ['short' => 'NE', 'long' => 'Not Entered'],
                'out' => ['short' => 'Out', 'long' => 'Out'],
                'sub' => ['short' => 'Sub', 'long' => 'Substitute'],
                'susp' => ['short' => 'Susp', 'long' => 'Suspended'],
            ],
        ],
        // MotoGP - No longer available.
        'motogp' => [
            'disabled' => true,
            'name_short' => 'MotoGP',
            'box' => 'track',
            'namespace' => 'Motorsport',
            'section' => 'MotoGP',
            'contest_type' => 'championship',
            'event_type' => 'race',
            'weather' => [
                'type' => 'period',
                'unit' => 'c',
            ],
            'period' => [
                'class' => 'Race',
                'ref_col' => 'period_id',
                'sport_col' => 'round',
                'lock_time' => 'TBL.qual_time',
                'format' => 'D jS, H:i',
                'lineup_refresh' => 0,
            ],
            'ind' => [
                'name' => 'rider',
                'class' => 'Participant',
                'icon' => 'flag_right flag16_right_',
                'sports_path' => 'riders',
            ],
            'status' => [
                'inj' => ['short' => 'Inj', 'long' => 'Injured'],
                'ne' => ['short' => 'NE', 'long' => 'Not Entered'],
                'out' => ['short' => 'Out', 'long' => 'Out'],
                'sub' => ['short' => 'Sub', 'long' => 'Substitute'],
                'susp' => ['short' => 'Susp', 'long' => 'Suspended'],
            ],
        ],
        // Speedway Grand Prix.
        'sgp' => [
            'name_short' => 'SGP',
            'box' => 'shale',
            'namespace' => 'Motorsport',
            'section' => 'SGP',
            'contest_type' => 'championship',
            'event_type' => 'meeting',
            'weather' => [
                'type' => 'period',
                'unit' => 'c',
                'lineup_refresh' => 0,
            ],
            'period' => [
                'class' => 'Race',
                'ref_col' => 'period_id',
                'sport_col' => 'round',
                'lock_time' => 'race_time',
                'format' => 'D jS, H:i',
            ],
            'ind' => [
                'name' => 'rider',
                'class' => 'Participant',
                'icon' => 'flag_right flag16_right_',
                'sports_path' => 'riders',
            ],
            'status' => [
                'inj' => ['short' => 'Inj', 'long' => 'Injured'],
                'ne' => ['short' => 'NE', 'long' => 'Not Entered'],
                'out' => ['short' => 'Out', 'long' => 'Out'],
                'sub' => ['short' => 'Sub', 'long' => 'Substitute'],
                'susp' => ['short' => 'Susp', 'long' => 'Suspended'],
            ],
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'navigation' => [
                'enabled' => false,
            ],
            'footer' => [
                'url-section' => false,
            ],
            'descrip' => 'The home portal to the suite of fantasy games on DeBear Fantasy Sports.',
        ],
    ],

    /*
     * Subsite endpoints
     */
    'dirs' => [],

    /*
     * Site revision info
     */
    'version' => [
        'breakdown' => [
            'major' => 3,
            'minor' => 0,
        ],
    ],

    /*
     * Google Analytics
     */
    'analytics' => [
        'urchins' => [
            'tracking' => [ 'UA-48789782-7' ],
        ],
    ],

    /*
     * Social Media details
     */
    'social' => [
        'twitter' => [
            'enabled' => true,
            'creds' => [
                'live' => [
                    'app' => 'fantasy',
                    'master' => 'DeBearSports',
                ],
            ],
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/fantasy.png',
        ],
    ],

    /*
     * Page content
     */
    'content' => [
        'sitemap' => [
            'view' => [
                'override' => 'fantasy._global.sitemap',
            ],
        ],
    ],

    /*
     * Subsites (in to which we will shortly load the config)
     */
    'subsites' => [
        // Ungrouped / Standalone games.
        ['records'],
        // Playoff brackets.
        'brackets' => [
            'nfl' => ['url' => 'nfl-bracket'],
            'mlb' => ['url' => 'mlb-bracket'],
            'nhl' => ['url' => 'nhl-bracket'],
            'ahl' => ['url' => 'ahl-bracket'],
        ],
        // Legacy games.
        'budget' => [
            'f1',
            'motogp',
            'sgp',
            'nfl' => ['url' => 'nfl-cap'],
        ],
    ],
];

/* Load the section-specific config */
$sitemap_order = 20;
foreach ($subconfig['subsites'] as $group => $sites) {
    if (is_string($group)) {
        // If we are part of a grouped site, load the section config.
        $group_config = (file_exists(__DIR__ . "/$group.php") ? require "$group.php" : []);
        $section_config = Arrays::merge(['group' => $group], $group_config);
        $group_path = "$group/";
        $site_prefix = "$group-";
    } else {
        // No section config applies.
        $section_config = [];
        $group_path = $site_prefix = '';
    }
    foreach ($sites as $site => $site_spec) {
        // Standardise the spec.
        if (is_string($site_spec)) {
            $site = $site_spec;
            $site_spec = ['url' => $site];
        }
        // Build the configuration name we'll use for the subsite.
        $site_config = "$site_prefix$site";
        // Load the config.
        $base_config = Arrays::merge($section_config, $site_spec);
        $subconfig['subsites'][$site_config] = Arrays::merge($base_config, require "$group_path$site.php");
        // Add to the sitemap.
        $subconfig['dirs']["subsite-$site_config"] = "/{$site_spec['url']}";
        $subconfig['links']["subsite-$site_config"] = [
            'url' => $subconfig['dirs']["subsite-$site_config"],
            'label' => $subconfig['subsites'][$site_config]['names']['section'],
            'order' => $sitemap_order++,
            'sitemap' => [
                'enabled' => $subconfig['subsites'][$site_config]['enabled'],
                'priority' => 0.8,
                'frequency' => 'daily',
            ],
        ];
    }
    // Flatten the subsites.
    unset($subconfig['subsites'][$group]);
}

/* Return the built config */
return $subconfig;
