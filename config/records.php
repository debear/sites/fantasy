<?php

/**
 * Fantasy Record Breakers config
 */

use DeBear\Helpers\HTTP;

return [
    'enabled' => true,

    /*
     * Various game timings
     */
    'dates' => [
        'visible' => '2023-10-01 00:00:00',
        'hide' => '2099-12-31 23:59:59',
    ],
    /* */

    /*
     * Generic game management
     */
    'games' => [
        'homepage_worker' => ['DeBear\ORM\Fantasy\Records\Game', 'getHomepageGames'],
        'sitemap_worker' => ['DeBear\ORM\Fantasy\Records\Game', 'getSitemapGames'],
        'upcoming_days' => 7,
        'closing_soon' => 7,
    ],

    /*
     * Game resource additions
     */
    'resources' => [
        'css' => [
            'records/layout.css',
            'records/common.css',
            'sports_icons.css',
        ],
    ],

    /*
     * Site name details
     */
    'names' => [
        'external' => 'DeBear Fantasy Record Breakers',
        'section' => 'Record Breakers',
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/records.png',
        ],
        'theme_colour' => '#666666',
        'og' => [
            'create' => [
                'cnv_path' => '/usr/bin/node ' . realpath(__DIR__ . '/../data/_puppeteer/screenshot.js'),
                'img_path' => 'fantasy/records/og',
                'format' => 'png',
                'width' => 1200,
                'height' => 600,
                'min_bytes' => 4098,
                'num_leaders' => 3,
            ],
        ],
    ],

    /*
     * Social Media details
     */
    'social' => [
        'twitter' => [
            'creds' => [
                'live' => [
                    'master' => 'DeBearRecords',
                ],
            ],
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'descrip' => 'Can you make enough selections to topple history and take your place amongst sporting'
                . ' folklore?',
            'summary' => 'Take on some of history&#39;s toughest records, from Joltin Joe&#39;s 56 game hitting streak,'
                . ' The Great One&#39;s 215 point season or the New England Patriots undefeated 2007 (regular'
                . ' season!).',
        ],
        'my-entry' => [
            'url' => '/my-entry',
            'label' => 'My Entry',
            'order' => 88,
            'url-section' => true,
            'policy' => 'user:unrestricted', // Do not include for Restricted users.
            'header' => true,
            'navigation' => [
                'enabled' => true,
                'class' => 'mobile-only',
                'order' => 50,
            ],
        ],
        'help' => [
            'url' => '/help',
            'label' => 'Help',
            'order' => 89,
            'url-section' => true,
            'footer' => true,
            'navigation' => [
                'enabled' => true,
                'class' => 'mobile-only',
                'order' => 55,
            ],
        ],
    ],

    /*
     * Game Setup info
     */
    'setup' => [
        'selections' => [
            'edit_modal_rows' => 10,
            'view_modal_history' => 10,
            'team_modal_history' => 10,
            'entry_hitrate' => 5,
        ],
        'js_cache_ttl' => [
            // In seconds, 0 = disabled, which is default.
            'sel' => 0,
            'info' => 0,
        ],
    ],

    /*
     * Form customisations
     */
    'forms' => [
        'namespaces' => [
            'models' => 'Records',
            'cache' => 'records',
            'email' => 'Fantasy Records',
            'audit' => 'records',
            'session' => 'records',
        ],
        'overlays' => [
            'entry' => [
                'interpolate' => [
                    'next' => [
                        'redirect' => '{config|debear.section.endpoint}',
                    ],
                ],
            ],
        ],
    ],

    /*
     * Audit types
     */
    'logging' => [
        'user_activity' => [
            'records_entry_create' => 40,
            'records_entry_update' => 41,
            'records_games_join' => 43,
            'records_games_leave' => 44,
            'records_sel_create' => 46,
            'records_sel_update' => 47,
            'records_sel_remove' => 48,
        ],
    ],

    /*
     * CDN details
     */
    'cdn' => [
        'mugshots' => [
            'tiny' => ['w' => 30, 'h' => 45],
            'small' => ['w' => 50, 'h' => 75],
            'medium' => ['w' => 66, 'h' => 100],
            'large' => ['w' => 87, 'h' => 130],
        ],
        'holders' => [
            'medium' => ['w' => 50, 'h' => 65],
            'full' => ['w' => 100, 'h' => 130],
        ],
    ],
];
