<?php

// Automated player locale setup.
// Note: All weight should ideally totally 100.
return [
    // Great Britain.
    'en_GB' => [
        'weight' => 15,
        'ahl' => [6, 9],
        'nhl' => [18, 27],
        'nfl' => [52, 74],
        'mlb' => [12, 16],
        'f1' => [68, 85],
        'sgp' => [19, 24],
    ],
    // United States.
    'en_US' => [
        'weight' => 15,
        'ahl' => [38, 51],
        'nhl' => [61, 79],
        'nfl' => [76, 94],
        'mlb' => [68, 93],
        'f1' => [6, 12],
        'sgp' => [0, 2],
    ],
    // Canada.
    'en_CA' => [
        'weight' => 11,
        'ahl' => [19, 42],
        'nhl' => [78, 99],
        'nfl' => [53, 72],
        'mlb' => [42, 59],
        'f1' => [9, 18],
        'sgp' => [0, 1],
    ],
    'fr_CA' => [
        'weight' => 4,
        'ahl' => [9, 27],
        'nhl' => [73, 87],
        'nfl' => [21, 52],
        'mlb' => [19, 36],
        'f1' => [39, 56],
        'sgp' => [0, 1],
    ],
    // France.
    'fr_FR' => [
        'weight' => 8,
        'ahl' => [0, 2],
        'nhl' => [9, 26],
        'nfl' => [4, 17],
        'mlb' => [3, 23],
        'f1' => [32, 68],
        'sgp' => [3, 6],
    ],
    // Germany.
    'de_DE' => [
        'weight' => 4,
        'ahl' => [3, 9],
        'nhl' => [13, 31],
        'nfl' => [58, 81],
        'mlb' => [34, 53],
        'f1' => [48, 79],
        'sgp' => [13, 32],
    ],
    // Netherlands.
    'nl_NL' => [
        'weight' => 3,
        'ahl' => [0, 1],
        'nhl' => [2, 4],
        'nfl' => [12, 16],
        'mlb' => [21, 26],
        'f1' => [57, 72],
        'sgp' => [3, 7],
    ],
    // Austria.
    'de_AT' => [
        'weight' => 2,
        'ahl' => [3, 7],
        'nhl' => [19, 34],
        'nfl' => [6, 12],
        'mlb' => [4, 9],
        'f1' => [48, 72],
        'sgp' => [0, 5],
    ],
    // Poland.
    'pl_PL' => [
        'weight' => 5,
        'ahl' => [0, 1],
        'nhl' => [2, 4],
        'nfl' => [5, 9],
        'mlb' => [2, 5],
        'f1' => [21, 49],
        'sgp' => [79, 97],
    ],
    // Czech Republic.
    'cs_CZ' => [
        'weight' => 1,
        'ahl' => [0, 1],
        'nhl' => [37, 52],
        'nfl' => [17, 34],
        'mlb' => [8, 14],
        'f1' => [32, 49],
        'sgp' => [37, 52],
    ],
    // Sweden.
    'sv_SE' => [
        'weight' => 2,
        'ahl' => [21, 40],
        'nhl' => [65, 78],
        'nfl' => [12, 30],
        'mlb' => [6, 14],
        'f1' => [28, 42],
        'sgp' => [57, 81],
    ],
    // Finland.
    'fi_FI' => [
        'weight' => 1,
        'ahl' => [23, 38],
        'nhl' => [62, 75],
        'nfl' => [36, 51],
        'mlb' => [5, 12],
        'f1' => [37, 48],
        'sgp' => [42, 61],
    ],
    // Denmark.
    'da_DK' => [
        'weight' => 3,
        'ahl' => [16, 22],
        'nhl' => [47, 62],
        'nfl' => [24, 42],
        'mlb' => [3, 5],
        'f1' => [54, 62],
        'sgp' => [45, 72],
    ],
    // Italy.
    'it_IT' => [
        'weight' => 4,
        'ahl' => [0, 1],
        'nhl' => [5, 12],
        'nfl' => [24, 29],
        'mlb' => [3, 5],
        'f1' => [79, 96],
        'sgp' => [24, 42],
    ],
    // Spain.
    'es_ES' => [
        'weight' => 7,
        'ahl' => [0, 1],
        'nhl' => [2, 4],
        'nfl' => [6, 12],
        'mlb' => [2, 4],
        'f1' => [64, 79],
        'sgp' => [2, 5],
    ],
    // Portugal.
    'pt_PT' => [
        'weight' => 2,
        'ahl' => [0, 1],
        'nhl' => [2, 4],
        'nfl' => [7, 13],
        'mlb' => [1, 3],
        'f1' => [47, 58],
        'sgp' => [1, 3],
    ],
    // Mexico.
    'es_MX' => [
        'weight' => 3,
        'ahl' => [0, 1],
        'nhl' => [2, 5],
        'nfl' => [52, 72],
        'mlb' => [26, 43],
        'f1' => [54, 80],
        'sgp' => [0, 1],
    ],
    // Brazil.
    'pt_BR' => [
        'weight' => 2,
        'ahl' => [0, 1],
        'nhl' => [1, 3],
        'nfl' => [6, 9],
        'mlb' => [1, 3],
        'f1' => [68, 89],
        'sgp' => [0, 1],
    ],
    // Argentina.
    'es_AR' => [
        'weight' => 2,
        'ahl' => [0, 1],
        'nhl' => [1, 3],
        'nfl' => [12, 17],
        'mlb' => [1, 3],
        'f1' => [47, 61],
        'sgp' => [0, 1],
    ],
    // Australia.
    'en_AU' => [
        'weight' => 5,
        'ahl' => [0, 1],
        'nhl' => [1, 4],
        'nfl' => [16, 32],
        'mlb' => [2, 7],
        'f1' => [62, 86],
        'sgp' => [35, 61],
    ],
    // New Zealand.
    'en_NZ' => [
        'weight' => 1,
        'ahl' => [0, 1],
        'nhl' => [1, 3],
        'nfl' => [14, 19],
        'mlb' => [1, 3],
        'f1' => [56, 71],
        'sgp' => [10, 17],
    ],
];
