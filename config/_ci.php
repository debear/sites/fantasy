<?php

/**
 * CI-specific over-rides
 */

return [
    'subsites' => [
        /* Record Breakers */
        'records' => [
            'meta' => [
                'og' => [
                    'create' => [
                        // Replace with a predictable stub in CI.
                        'cnv_path' => 'tests/stubs/html2img.sh',
                    ],
                ],
            ],
            'setup' => [
                // Stress test the caching logic, should it be enabled.
                'js_cache_ttl' => [
                    'sel' => 180,
                    'info' => 86400,
                ],
            ],
        ],
        /* NFL Bracket Challenge */
        'brackets-nfl' => [
            'enabled' => true,
            'dates' => [
                'visible' => '2023-12-01 05:00:00',
                'hide' => '2024-03-01 04:59:59',
            ],
            'setup' => [
                'season' => [
                    'min' => 2023,
                    'max' => 2023,
                ],
            ],
        ],
        /* MLB Bracket Challenge */
        'brackets-mlb' => [
            'enabled' => true,
            'dates' => [
                'visible' => '2024-09-01 05:00:00',
                'hide' => '2024-12-01 04:59:59',
            ],
            'setup' => [
                'season' => [
                    'min' => 2024,
                    'max' => 2024,
                ],
            ],
        ],
        /* NHL Bracket Challenge */
        'brackets-nhl' => [
            'enabled' => true,
            'dates' => [
                'visible' => '2024-03-15 04:00:00',
                'hide' => '2024-08-01 04:59:59',
            ],
            'setup' => [
                'season' => [
                    'min' => 2023,
                    'max' => 2023,
                ],
            ],
        ],
        /* AHL Bracket Challenge */
        'brackets-ahl' => [
            'enabled' => true,
            'dates' => [
                'visible' => '2024-03-22 04:00:00',
                'hide' => '2024-08-01 04:59:59',
            ],
            'setup' => [
                'season' => [
                    'min' => 2023,
                    'max' => 2023,
                ],
            ],
        ],
    ],
];
