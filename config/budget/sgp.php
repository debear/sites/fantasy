<?php

/**
 * Legacy: Fantasy Speedway Grand Prix
 */

return [
    'enabled' => false,
    'names' => [
        'section' => 'Fantasy Speedway Grand Prix',
    ],
];
