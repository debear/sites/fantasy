<?php

/**
 * Legacy: Fantasy NFL Salary Cap
 */

return [
    'enabled' => false,
    'names' => [
        'section' => 'Fantasy NFL Salary Cap',
    ],
];
