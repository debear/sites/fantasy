CREATE TABLE `FANTASY_COMMON_HELP_SECTIONS` (
  `game` ENUM('f1','motogp','nfl-cap','sgp','records') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `section_id` TINYINT(3) UNSIGNED NOT NULL,
  `name` VARCHAR(255) COLLATE latin1_general_ci,
  `order` TINYINT(1) UNSIGNED DEFAULT 0,
  `active` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`game`,`season`,`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_COMMON_HELP_ARTICLES` (
  `game` ENUM('f1','motogp','nfl-cap','sgp','records') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `section_id` TINYINT(3) UNSIGNED NOT NULL,
  `article_id` TINYINT(3) UNSIGNED NOT NULL,
  `title` VARCHAR(255) COLLATE latin1_general_ci,
  `body` TEXT COLLATE latin1_general_ci,
  `section_order` TINYINT(1) UNSIGNED DEFAULT 0,
  `active` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`game`,`season`,`section_id`,`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
