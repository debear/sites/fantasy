CREATE TABLE `FANTASY_COMMON_MOTORS_SELECTIONS_STATUS` (
  `sport` ENUM('f1','motogp','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `link_id` INT(11) UNSIGNED NOT NULL,
  `status` ENUM('inj','ne','out','sub','susp') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`sport`,`season`,`period_id`,`link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_COMMON_PERIODS_DATES` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `period_id` TINYINT(3) UNSIGNED NOT NULL,
  `name` VARCHAR(75) COLLATE latin1_general_ci DEFAULT NULL,
  `name_short` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `icon` CHAR(6) COLLATE latin1_general_ci DEFAULT NULL,
  `icon_type` ENUM('flag') COLLATE latin1_general_ci DEFAULT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `period_order` TINYINT(3) UNSIGNED NOT NULL,
  `summarise` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`sport`,`season`,`period_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_COMMON_SELECTIONS_MAPPED` (
  `sport` ENUM('mlb','nfl','nhl','ahl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `raw_id` CHAR(5) COLLATE latin1_general_ci NOT NULL,
  `bit_flag` TINYINT UNSIGNED,
  `link_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`sport`,`season`,`raw_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
