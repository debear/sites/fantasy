CREATE TABLE `FANTASY_AUTOMATED_USER` (
  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `forename` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `surname` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `display_name` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `team_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `locale` ENUM('cs_CZ','da_DK','de_AT','de_DE','en_AU','en_CA','en_GB','en_NZ','en_US','es_AR','es_ES','es_MX','fi_FI','fr_CA','fr_FR','it_IT','nl_NL','pl_PL','pt_BR','pt_PT','sv_SE') COLLATE latin1_general_ci NOT NULL,
  `personality` ENUM('stats','rating','opp_rating','random','variable') COLLATE latin1_general_ci NOT NULL,
  `risk_taking` TINYINT UNSIGNED NOT NULL,
  `interest_ahl` TINYINT UNSIGNED NOT NULL,
  `interest_nhl` TINYINT UNSIGNED NOT NULL,
  `interest_nfl` TINYINT UNSIGNED NOT NULL,
  `interest_mlb` TINYINT UNSIGNED NOT NULL,
  `interest_f1` TINYINT UNSIGNED NOT NULL,
  `interest_sgp` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
