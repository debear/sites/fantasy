CREATE TABLE `FANTASY_COMMON_ANNOUNCEMENTS` (
  `game` ENUM('f1','motogp','nfl-cap','sgp','records') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `announce_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `section` VARCHAR(20) COLLATE latin1_general_ci,
  `type` ENUM('status','error','success') COLLATE latin1_general_ci NOT NULL,
  `subject` VARCHAR(75) COLLATE latin1_general_ci,
  `body` TEXT COLLATE latin1_general_ci NOT NULL,
  `disp_start` DATETIME,
  `disp_end` DATETIME,
  PRIMARY KEY (`game`,`season`,`announce_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
