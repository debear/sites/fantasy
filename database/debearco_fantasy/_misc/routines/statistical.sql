##
## Statistical analysis
##

#
# Convert a column of raw numbers into an equivalent, statisically "significant", range
#
DROP PROCEDURE IF EXISTS `_statistical_range`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `_statistical_range`(
  v_tbl VARCHAR(100),
  v_col_src VARCHAR(100),
  v_col_dst VARCHAR(100),
  v_mean DECIMAL(10, 5) SIGNED,
  v_min SMALLINT SIGNED,
  v_max SMALLINT SIGNED,
  v_stat_dir ENUM('ASC', 'DESC')
)
    COMMENT 'Statistical Analysis: Column range standardisation'
BEGIN

  IF v_mean IS NULL THEN
    # Range is to be built around a mean calculated from the dataset
    CALL _exec(CONCAT('SELECT AVG(', v_col_src, ') - (2 * STDDEV(', v_col_src, ')), AVG(', v_col_src, ') + (2 * STDDEV(', v_col_src, ')) INTO @v_col_min, @v_col_max FROM ', v_tbl, ';'));
  ELSE
    # The range uses a pre-calced mean
    CALL _exec(CONCAT('SELECT ', v_mean, ' - (2 * STDDEV(', v_col_src, ')), ', v_mean, ' + (2 * STDDEV(', v_col_src, ')) INTO @v_col_min, @v_col_max FROM ', v_tbl, ';'));
  END IF;
  IF @v_col_min <> @v_col_max THEN
    # We have a range to calculate
    CALL _exec(CONCAT('UPDATE ', v_tbl, ' SET ', v_col_dst, ' = ', v_min, ' + (((IF(', v_col_src, ' < @v_col_min, @v_col_min, IF(', v_col_src, ' > @v_col_max, @v_col_max, ', v_col_src, ')) - @v_col_min) / (@v_col_max - @v_col_min)) * ', (v_max - v_min), ');'));
  ELSE
    # There is no range (all values are the same), so set this to the mean
    CALL _exec(CONCAT('UPDATE ', v_tbl, ' SET ', v_col_dst, ' = ', ((v_max - v_min) / 2), ';'));
  END IF;

  # If a lower source value is better, flip the destination range over
  IF v_stat_dir = 'ASC' THEN
    CALL _exec(CONCAT('UPDATE ', v_tbl, ' SET ', v_col_dst, ' = ', v_max, ' - ', v_col_dst, ' + ', v_min, ';'));
  END IF;

END $$

DELIMITER ;
