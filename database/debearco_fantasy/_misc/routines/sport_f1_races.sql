#
# Build a table containing the most recent X races
#
DROP PROCEDURE IF EXISTS `sport_f1_recent_races`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `sport_f1_recent_races`(
  v_season SMALLINT UNSIGNED,
  v_start_date DATE,
  v_num_races TINYINT UNSIGNED
)
    COMMENT 'Generic F1 recent race builder'
BEGIN

  # Start by looking back at all games over the last three seasons
  # This works on the expectation v_num_races does go beyond the start of the previous seasons
  DROP TEMPORARY TABLE IF EXISTS tmp_races;
  CREATE TEMPORARY TABLE tmp_races (
    season YEAR,
    series ENUM('f1'),
    round TINYINT UNSIGNED,
    race_time DATETIME,
    race_laps_completed TINYINT UNSIGNED,
    race2_time DATETIME,
    race2_laps_completed TINYINT UNSIGNED,
    recency_order TINYINT UNSIGNED,
    PRIMARY KEY (season, series, round),
    KEY by_race_time (series, race_time)
  ) ENGINE=MyISAM
    SELECT RACE.season, RACE.series, RACE.round, RACE.race_time, RACE.race_laps_completed, RACE.race2_time, RACE.race2_laps_completed,
           NULL AS recency_order
    FROM debearco_sports.SPORTS_FIA_RACES AS RACE
    WHERE RACE.season IN (v_season - 2, v_season - 1, v_season)
    AND   RACE.series = 'f1'
    AND   DATE(RACE.race_time) < v_start_date
    AND   IFNULL(RACE.race_laps_completed, 0) > 0;

  # Fill the recency_order column
  CALL _duplicate_tmp_table('tmp_races', 'tmp_races_cpA');
  CALL _duplicate_tmp_table('tmp_races', 'tmp_races_cpB');
  INSERT INTO tmp_races (season, series, round, race_time, recency_order)
    SELECT SQL_BIG_RESULT tmp_races_cpA.season, tmp_races_cpA.series, tmp_races_cpA.round, tmp_races_cpA.race_time,
           COUNT(tmp_races_cpB.race_time) + 1 AS recency_order
    FROM tmp_races_cpA
    LEFT JOIN tmp_races_cpB
      ON (tmp_races_cpB.series = tmp_races_cpA.series
      AND tmp_races_cpB.race_time > tmp_races_cpA.race_time)
    GROUP BY tmp_races_cpA.season, tmp_races_cpA.series, tmp_races_cpA.round, tmp_races_cpA.race_time
  ON DUPLICATE KEY UPDATE recency_order = VALUES(recency_order);

  DROP TEMPORARY TABLE tmp_races_cpA;
  DROP TEMPORARY TABLE tmp_races_cpB;

  # Limit the table to our requested size
  DELETE FROM tmp_races WHERE recency_order > v_num_races;

END $$

DELIMITER ;
