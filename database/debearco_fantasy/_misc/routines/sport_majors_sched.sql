#
# Build a table containing the most recent X games for each team
#
DROP PROCEDURE IF EXISTS `sport_majorleague_recent_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `sport_majorleague_recent_sched`(
  v_sport CHAR(3),
  v_season YEAR,
  v_start_date DATE,
  v_num_games TINYINT UNSIGNED
)
    COMMENT 'Generic Major League recent game builder'
BEGIN

  CALL sport_majorleague_teams(v_sport, v_season);

  # Start by looking back at all games over the last two seasons
  # This works on the expectation v_num_games does go beyond the start of the previous season
  DROP TEMPORARY TABLE IF EXISTS tmp_team_sched;
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_team_sched (
    team_id CHAR(3),
    season YEAR,
    game_type ENUM("regular", "playoff"),
    game_id SMALLINT UNSIGNED,
    game_time MEDIUMINT UNSIGNED,
    opp_team_id CHAR(3),
    is_home TINYINT UNSIGNED,
    recency_order SMALLINT UNSIGNED,
    PRIMARY KEY (team_id, season, game_type, game_id),
    KEY by_team_time (team_id, game_time)
  ) ENGINE=MyISAM
    SELECT TEAMS.team_id, SCHED.season, SCHED.game_type, SCHED.game_id,
           ((YEAR(SCHED.game_date) - ', (v_season - 2), ') * 1000000)
            + (CAST(DATE_FORMAT(CONCAT(SCHED.game_date, " ", SCHED.game_time), "%j") AS UNSIGNED) * 1000)
            + (CAST(DATE_FORMAT(CONCAT(SCHED.game_date, " ", SCHED.game_time), "%l") AS UNSIGNED) * 60)
            + CAST(DATE_FORMAT(CONCAT(SCHED.game_date, " ", SCHED.game_time), "%i") AS UNSIGNED) AS game_time,
           IF(TEAMS.team_id = SCHED.home, SCHED.visitor, SCHED.home) AS opp_team_id,
           TEAMS.team_id = SCHED.home AS is_home,
           NULL AS recency_order
    FROM tmp_all_teams AS TEAMS
    JOIN debearco_sports.SPORTS_', UPPER(v_sport), '_SCHEDULE AS SCHED
      ON (SCHED.season IN (', (v_season - 1), ', ', v_season, ')
      AND SCHED.game_type = "regular"
      AND SCHED.game_date < "', v_start_date, '"
      AND IFNULL(SCHED.status, "STILLTOPLAY") NOT IN ("PPD", "CNC", "STILLTOPLAY")
      AND TEAMS.team_id IN (SCHED.home, SCHED.visitor));')); # Quote catcher... '

  # Fill the recency_order column
  CALL _duplicate_tmp_table('tmp_team_sched', 'tmp_team_sched_cpA');
  CALL _duplicate_tmp_table('tmp_team_sched', 'tmp_team_sched_cpB');
  INSERT INTO tmp_team_sched (team_id, season, game_type, game_id, recency_order)
    SELECT SQL_BIG_RESULT tmp_team_sched_cpA.team_id, tmp_team_sched_cpA.season, tmp_team_sched_cpA.game_type, tmp_team_sched_cpA.game_id,
           COUNT(tmp_team_sched_cpB.game_time) + 1 AS recency_order
    FROM tmp_team_sched_cpA
    LEFT JOIN tmp_team_sched_cpB
      ON (tmp_team_sched_cpB.team_id = tmp_team_sched_cpA.team_id
      AND tmp_team_sched_cpB.game_time > tmp_team_sched_cpA.game_time)
    GROUP BY tmp_team_sched_cpA.team_id, tmp_team_sched_cpA.season, tmp_team_sched_cpA.game_type, tmp_team_sched_cpA.game_id
  ON DUPLICATE KEY UPDATE recency_order = VALUES(recency_order);

  DROP TEMPORARY TABLE tmp_team_sched_cpA;
  DROP TEMPORARY TABLE tmp_team_sched_cpB;

  # Limit the table to our requested size
  DELETE FROM tmp_team_sched WHERE recency_order > v_num_games;

END $$

DELIMITER ;
