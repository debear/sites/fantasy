##
## Date/Time emulators
##

DROP FUNCTION IF EXISTS `_NOW`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `_NOW`(
) RETURNS DATETIME
  NOT DETERMINISTIC
  COMMENT 'The current (potentially test) time'
BEGIN

  RETURN NOW(); -- Our representation for the current time

END $$

DELIMITER ;

DROP FUNCTION IF EXISTS `_CURDATE`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `_CURDATE`(
) RETURNS DATE
  NOT DETERMINISTIC
  COMMENT 'The current (potentially test) date'
BEGIN

  RETURN DATE(_NOW());

END $$

DELIMITER ;

DROP FUNCTION IF EXISTS `_NOW_MILLISEC`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `_NOW_MILLISEC`(
) RETURNS DECIMAL(8,3) SIGNED
  NOT DETERMINISTIC
  COMMENT 'The current (absolute, never test) date as number of seconds with milli-second granularity'
BEGIN

  DECLARE v_ts DATETIME(3);
  SET v_ts = CURRENT_TIMESTAMP(3);
  RETURN TIME_TO_SEC(v_ts) + ((v_ts + 0.0) % 1.0);

END $$

DELIMITER ;
