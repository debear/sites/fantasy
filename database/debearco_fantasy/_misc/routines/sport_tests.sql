##
## Sport-type processing checks
##
DROP FUNCTION IF EXISTS `sport_is_major_league`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `sport_is_major_league`(
  v_sport VARCHAR(6)
) RETURNS TINYINT(1) UNSIGNED
    DETERMINISTIC
    COMMENT 'Validates whether the passed sport is Major League or not'
BEGIN

    RETURN v_sport REGEXP '^(mlb|nfl|nhl|ahl)$';

END $$

DELIMITER ;

DROP FUNCTION IF EXISTS `sport_is_motorsport`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `sport_is_motorsport`(
  v_sport VARCHAR(6)
) RETURNS TINYINT(1) UNSIGNED
    DETERMINISTIC
    COMMENT 'Validates whether the passed sport is a Motorsport or not'
BEGIN

    RETURN v_sport REGEXP '^(f1|motogp|sgp)$';

END $$

DELIMITER ;

DROP FUNCTION IF EXISTS `sport_is_daily`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `sport_is_daily`(
  v_sport VARCHAR(6)
) RETURNS TINYINT(1) UNSIGNED
    DETERMINISTIC
    COMMENT 'Validates whether the passed sport has daily periods or not'
BEGIN

    RETURN v_sport REGEXP '^(mlb|nhl|ahl)$';

END $$

DELIMITER ;

# Determine if the selection pool is available within a sport
DROP FUNCTION IF EXISTS `sport_selections_available`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `sport_selections_available`(
  v_sport VARCHAR(6),
  v_date DATE
) RETURNS TINYINT(1) UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine if selections are available for a sport'
BEGIN

  DECLARE v_selections_available TINYINT(1) UNSIGNED;
  DECLARE v_season SMALLINT UNSIGNED;

  # Season can be inferred by the date
  SET v_season := YEAR(v_date);
  IF sport_is_major_league(v_sport) AND v_sport <> 'mlb' AND v_date REGEXP '^20[0-9]{2}-0[1-7]' THEN
    # NFL, NHL and AHL seasons (i.e., what we class as "major league" except MLB) are numbered from the start of the
    # regular season but cross multiple calendar years, so games in Jan onwards refer to the previous calendar year
    SET v_season := v_season - 1;
  END IF;

  # Major League is relatively formulaic (though dynamic code is not allowed in FUNCTIONs...)
  IF v_sport = 'mlb' THEN
    SELECT COUNT(*) > 0 INTO v_selections_available
    FROM debearco_sports.SPORTS_MLB_TEAMS_ROSTERS
    WHERE season = v_season;

  ELSEIF v_sport = 'nfl' THEN
    SELECT COUNT(*) > 0 INTO v_selections_available
    FROM debearco_sports.SPORTS_NFL_TEAMS_ROSTERS
    WHERE season = v_season;

  ELSEIF v_sport = 'nhl' THEN
    SELECT COUNT(*) > 0 INTO v_selections_available
    FROM debearco_sports.SPORTS_NHL_TEAMS_ROSTERS
    WHERE season = v_season;

  ELSEIF v_sport = 'ahl' THEN
    SELECT COUNT(*) > 0 INTO v_selections_available
    FROM debearco_sports.SPORTS_AHL_TEAMS_ROSTERS
    WHERE season = v_season;

  # Motorsports have a couple of nuances, depending on the sport
  ELSEIF v_sport = 'f1' THEN
    SELECT COUNT(*) > 0 INTO v_selections_available
    FROM debearco_sports.SPORTS_FIA_RACE_DRIVERS
    WHERE season = v_season
    AND   series = 'f1';

  ELSEIF v_sport = 'sgp' THEN
    SELECT COUNT(*) > 0 INTO v_selections_available
    FROM debearco_sports.SPORTS_SGP_RACE_RIDERS;

  # An unknown sport has been passed
  ELSE
    SET @v_msg = CONCAT('Unable to determine if sport selections are available for sport "', v_sport, '"');
    SIGNAL SQLSTATE 'DB404' SET MESSAGE_TEXT = @v_msg;
  END IF;

  RETURN v_selections_available;

END $$

DELIMITER ;

# Determine if all components events in a sporting period have been completed
DROP FUNCTION IF EXISTS `sport_period_complete`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `sport_period_complete`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE,
  v_proc_date DATE
) RETURNS TINYINT(1) UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine if all events in a period are completed for a sport'
BEGIN

  DECLARE v_all_complete TINYINT(1) UNSIGNED;

  # Major League is relatively formulaic (though dynamic code is not allowed in FUNCTIONs...)
  IF v_sport = 'mlb' THEN
    SELECT MAX(game_date) <= v_proc_date AND SUM(status IS NULL) = 0 INTO v_all_complete
    FROM debearco_sports.SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = "regular"
    AND   game_date BETWEEN v_start_date AND v_end_date;

  ELSEIF v_sport = 'nfl' THEN
    SELECT MAX(game_date) <= v_proc_date AND SUM(status IS NULL) = 0 INTO v_all_complete
    FROM debearco_sports.SPORTS_NFL_SCHEDULE
    WHERE season = v_season
    AND   game_type = "regular"
    AND   game_date BETWEEN v_start_date AND v_end_date;

  ELSEIF v_sport = 'nhl' THEN
    SELECT MAX(game_date) <= v_proc_date AND SUM(status IS NULL) = 0 INTO v_all_complete
    FROM debearco_sports.SPORTS_NHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = "regular"
    AND   game_date BETWEEN v_start_date AND v_end_date;

  ELSEIF v_sport = 'ahl' THEN
    SELECT MAX(game_date) <= v_proc_date AND SUM(status IS NULL) = 0 INTO v_all_complete
    FROM debearco_sports.SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = "regular"
    AND   game_date BETWEEN v_start_date AND v_end_date;

  # Motorsports have a couple of nuances, depending on the sport
  ELSEIF v_sport = 'f1' THEN
    SELECT DATE(IFNULL(race2_time, race_time)) <= v_proc_date AND race_laps_completed IS NOT NULL INTO v_all_complete
    FROM debearco_sports.SPORTS_FIA_RACES
    WHERE season = v_season
    AND   series = 'f1'
    AND   round = v_period_id;

  ELSEIF v_sport = 'sgp' THEN
    SELECT DATE(race_time) <= v_proc_date AND completed_heats IS NOT NULL INTO v_all_complete
    FROM debearco_sports.SPORTS_SGP_RACES
    WHERE season = v_season
    AND   round = v_period_id;

  # An unknown sport has been passed
  ELSE
    SET @v_msg = CONCAT('Unable to determine if sport period is complete for sport "', v_sport, '"');
    SIGNAL SQLSTATE 'DB404' SET MESSAGE_TEXT = @v_msg;
  END IF;

  RETURN v_all_complete;

END $$

DELIMITER ;
