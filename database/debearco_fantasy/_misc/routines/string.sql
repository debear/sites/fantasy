##
## String helpers
##

DROP FUNCTION IF EXISTS `ordinal_suffix`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `ordinal_suffix`(
  v_input VARCHAR(5)
) RETURNS VARCHAR(7)
    DETERMINISTIC
    COMMENT 'Determines the appropriate ordinal suffix for an input string'
BEGIN

    DECLARE v_as_num SMALLINT UNSIGNED;
    DECLARE v_mod_100 TINYINT UNSIGNED;
    DECLARE v_mod_10 TINYINT UNSIGNED;

    # Ensure input is a number. If not, it has no suffix.
    IF v_input NOT REGEXP "^[0-9\.]+$" THEN
        RETURN v_input;
    END IF;

    SET v_as_num := CAST(FLOOR(v_input) AS UNSIGNED);
    SET v_mod_100 := v_as_num MOD 100;
    SET v_mod_10 := v_as_num MOD 10;

    IF v_mod_10 = 1 AND v_mod_100 <> 11 THEN
        RETURN CONCAT(v_input, 'st');
    ELSEIF v_mod_10 = 2 AND v_mod_100 <> 12 THEN
        RETURN CONCAT(v_input, 'nd');
    ELSEIF v_mod_10 = 3 AND v_mod_100 <> 13 THEN
        RETURN CONCAT(v_input, 'rd');
    END IF;
    RETURN CONCAT(v_input, 'th');

END $$

DELIMITER ;

DROP FUNCTION IF EXISTS `pluralise`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `pluralise`(
  v_num SMALLINT SIGNED,
  v_str VARCHAR(5)
) RETURNS VARCHAR(50)
    DETERMINISTIC
    COMMENT 'Pluralise a word given a preceding number'
BEGIN

    RETURN CONCAT(v_num, v_str, IF(v_num <> 1, 's', ''));

END $$

DELIMITER ;

DROP FUNCTION IF EXISTS `name2slug`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `name2slug`(
  v_name VARCHAR(30)
) RETURNS VARCHAR(30)
  DETERMINISTIC
  COMMENT 'Convert a display name to a slugged version'
BEGIN

  RETURN REGEXP_REPLACE(LOWER(v_name), '[^a-z0-9]+', '-');

END $$

DELIMITER ;
