#
# Build a table of all teams in a given Major League season
#
DROP PROCEDURE IF EXISTS `sport_majorleague_teams`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `sport_majorleague_teams`(
  v_sport CHAR(3),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Generic Major League team list builder'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_all_teams;
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_all_teams (
    team_id CHAR(3),
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT team_id
    FROM debearco_sports.SPORTS_', UPPER(v_sport), '_TEAMS_GROUPINGS
    WHERE ', v_season, ' BETWEEN season_from AND IFNULL(season_to, 2099)
    ORDER BY team_id;'));

END $$

DELIMITER ;
