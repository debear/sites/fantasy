CREATE TABLE `FANTASY_RECORDS_GAMES` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `name` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(255) COLLATE latin1_general_ci NOT NULL,
  `game_code` VARCHAR(10) COLLATE latin1_general_ci NOT NULL,
  `sel_type` ENUM('ind','team') COLLATE latin1_general_ci NOT NULL,
  `score_type` ENUM('total','streak','periods') COLLATE latin1_general_ci NOT NULL,
  `config_flags` SET('SEL_REQUIRED','SEL_SINGLE_USE','GAME_ENDS_ON_NEGATIVE','HITRATE_LEADERBOARD','STREAK_BUSTERS','NO_PERIOD_SUMMARY') COLLATE latin1_general_ci NOT NULL,
  `active_limit` TINYINT(3) UNSIGNED DEFAULT NULL,
  `target` SMALLINT(4) UNSIGNED NOT NULL,
  `record` SMALLINT(4) UNSIGNED DEFAULT NULL,
  `record_holder` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `date_open` DATETIME NOT NULL,
  `date_start` DATETIME NOT NULL,
  `date_close` DATETIME NOT NULL,
  `date_end` DATETIME NOT NULL,
  `date_complete` DATETIME NOT NULL,
  `active_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `active_score` DECIMAL(5,1) DEFAULT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_GAMES_RULES` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `rules` TEXT COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_GAMES_POSITIONS` (
  `sport` ENUM('mlb','nfl','nhl','ahl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `pos_code` CHAR(4) COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`pos_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_GAMES_STATS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `stat_id` TINYINT(1) UNSIGNED NOT NULL,
  `title` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `code` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(1) UNSIGNED NOT NULL,
  `last_period` TINYINT(3) UNSIGNED NULL,
  `last_calced` DATETIME NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `achieve_id` TINYINT(2) UNSIGNED NOT NULL,
  `name` VARCHAR(30),
  `descrip` VARCHAR(200),
  `type` ENUM('score','period','opponent','sel_usage','event'),
  `disp_order` TINYINT UNSIGNED,
  PRIMARY KEY (`sport`,`season`,`game_id`,`achieve_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `achieve_id` TINYINT(2) UNSIGNED NOT NULL,
  `level` TINYINT(1) UNSIGNED,
  `value` SMALLINT(4) UNSIGNED NOT NULL,
  `bit_flag` TINYINT(2) UNSIGNED NOT NULL,
  `disp_order` TINYINT UNSIGNED,
  PRIMARY KEY (`sport`,`season`,`game_id`,`achieve_id`,`level`),
  UNIQUE KEY `bit_flag` (`sport`,`season`,`game_id`,`bit_flag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_SETUP_EMAIL` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `date` DATETIME NOT NULL,
  `email_type` ENUM('game_end'),
  PRIMARY KEY (`sport`,`season`,`game_id`,`email_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_SETUP_TWITTER` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  `tweet_type` ENUM('season_soon','reg_opening','season_tomorrow','reg_closing','season_ended'),
  PRIMARY KEY (`sport`,`season`,`game_id`,`date`,`tweet_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
