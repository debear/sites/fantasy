CREATE TABLE `FANTASY_RECORDS_GAMES_PERIODS_SUMMARY` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `entry_pos_pct` DECIMAL(5,2) UNSIGNED,
  `sel_pos_pct` DECIMAL(5,2) UNSIGNED,
  `popular_pos_link_id` SMALLINT(5) UNSIGNED,
  `popular_pos_pct` DECIMAL(5,2) UNSIGNED,
  `popular_neg_link_id` SMALLINT(5) UNSIGNED,
  `popular_neg_pct` DECIMAL(5,2) UNSIGNED,
  `active_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `active_score` DECIMAL(5,1) DEFAULT NULL,
  `when_tweeted` DATETIME,
  `when_synced` DATETIME,
  PRIMARY KEY (`sport`,`season`,`game_id`,`period_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_GAMES_META` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `is_latest` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `path_og` VARCHAR(200),
  `when_processed` DATETIME NOT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`period_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
