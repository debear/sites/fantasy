CREATE TABLE `FANTASY_RECORDS_SELECTIONS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `link_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `rating` TINYINT(1) UNSIGNED NOT NULL,
  `opp_rating` TINYINT(1) UNSIGNED NULL,
  `score` DECIMAL(5,1) DEFAULT NULL,
  `status` ENUM('positive','neutral','negative','na') DEFAULT NULL,
  `stress` TINYINT(3) UNSIGNED DEFAULT NULL,
  `summary` VARCHAR(50) DEFAULT NULL,
  `sel_pct` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `note` VARCHAR(255) COLLATE latin1_general_ci NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`link_id`,`period_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_SELECTIONS_STATS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `link_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `stat_id` TINYINT(1) UNSIGNED NOT NULL,
  `stat_value` VARCHAR(30) COLLATE latin1_general_ci NULL,
  `stat_order` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`link_id`,`period_id`,`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_SELECTIONS_SCORING` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `link_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `event_order` SMALLINT(5) UNSIGNED NOT NULL,
  `stat` DECIMAL(5,1) DEFAULT NULL,
  `status` ENUM('positive','neutral','negative','na') DEFAULT NULL,
  `stress` TINYINT(3) UNSIGNED DEFAULT NULL,
  `summary` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`link_id`,`period_id`,`event_order`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
