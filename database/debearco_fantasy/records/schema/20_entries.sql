CREATE TABLE `FANTASY_RECORDS_ENTRIES` (
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `joined` DATE NOT NULL,
  `email_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `email_type` ENUM('text','html') COLLATE latin1_general_ci DEFAULT 'html',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_ENTRIES_SCORES` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `overall_res` DECIMAL(5,1) NOT NULL,
  `overall_pos` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `current_res` DECIMAL(5,1) NOT NULL,
  `current_pos` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `hitrate_num` INT(10) UNSIGNED DEFAULT NULL,
  `hitrate_pos` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `stress` TINYINT(3) UNSIGNED DEFAULT NULL,
  `opponents` INT UNSIGNED DEFAULT NULL,
  `achievements` SMALLINT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_ENTRIES_BYPERIOD` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period_id` TINYINT(2) UNSIGNED NOT NULL,
  `link_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `sel_saved` DATETIME DEFAULT NULL,
  `result` DECIMAL(5,1) DEFAULT NULL,
  `stress` TINYINT(3) UNSIGNED DEFAULT NULL,
  `opponents` INT UNSIGNED DEFAULT NULL,
  `achievements` SMALLINT UNSIGNED DEFAULT NULL,
  `overall_res` DECIMAL(5,1) DEFAULT NULL,
  `overall_pos` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `current_res` DECIMAL(5,1) DEFAULT NULL,
  `current_pos` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `hitrate_num` INT(10) UNSIGNED DEFAULT NULL,
  `hitrate_pos` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`user_id`,`period_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_ENTRIES_BUSTERS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team1` CHAR(3) NOT NULL,
  `team1_score` TINYINT UNSIGNED NOT NULL,
  `team2` CHAR(3) DEFAULT NULL,
  `team2_score` TINYINT UNSIGNED DEFAULT NULL,
  `team3` CHAR(3) DEFAULT NULL,
  `team3_score` TINYINT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_RECORDS_ENTRIES_DROPOFF` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `score4` DECIMAL(5,1) DEFAULT NULL,
  `score3` DECIMAL(5,1) DEFAULT NULL,
  `score2` DECIMAL(5,1) DEFAULT NULL,
  `score1` DECIMAL(5,1) DEFAULT NULL,
  `score0` DECIMAL(5,1) DEFAULT NULL,
  PRIMARY KEY (`sport`,`season`,`game_id`,`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
