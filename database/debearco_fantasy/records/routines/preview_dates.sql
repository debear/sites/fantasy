##
## Ensure future periods are setup correctly
##
DROP PROCEDURE IF EXISTS `records_validate_dates`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_validate_dates`(
  v_sport VARCHAR(6),
  v_date DATE
)
    COMMENT 'Record Breaker date validation for future periods'
proc_dates: BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_end_date DATE;

  # Only applies to sports with max-daily periods
  IF NOT sport_is_daily(v_sport) THEN
    LEAVE proc_dates;
  END IF;

  # Get the season argument from the date we are processing
  SELECT season INTO v_season
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   v_date BETWEEN start_date AND DATE_ADD(end_date, INTERVAL 3 DAY)
  LIMIT 1;

  # If we're not processing a valid date, skip
  IF v_season IS NULL THEN
    LEAVE proc_dates;
  END IF;

  # Determine the remaining scheduled dates
  DROP TEMPORARY TABLE IF EXISTS tmp_remaining_dates;
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_remaining_dates (
    game_date DATE,
    PRIMARY KEY (game_date)
  ) ENGINE = MEMORY
    SELECT game_date
    FROM debearco_sports.SPORTS_', UPPER(v_sport), '_SCHEDULE
    WHERE season = "', v_season, '"
    AND   game_type = "regular"
    AND   game_date >= "', v_date, '"
    AND   IFNULL(status, "TBP") NOT IN ("PPD", "CNC", "SSP")
    GROUP BY game_date
    ORDER BY game_date;'));

  # First, merge any periods where one no longer contains any games
  CALL records_validate_dates_merge(v_sport, v_season, v_date);
  # Then split any periods which now contain events over multiple days
  CALL records_validate_dates_split(v_sport, v_season, v_date);
  # Finally extend the season if new dates have been added
  CALL records_validate_dates_extend(v_sport, v_season);

  # Ensure if we have updated when the season ends, this propagates to other key columns
  SELECT DATE_ADD(MAX(end_date), INTERVAL 1 DAY) INTO v_end_date
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season;
  # Game end date
  UPDATE FANTASY_RECORDS_GAMES
  SET date_end = CONCAT(v_end_date, ' ', TIME(date_end))
  WHERE sport = v_sport
  AND   season = v_season;
  # Season ended tweets
  UPDATE FANTASY_RECORDS_SETUP_TWITTER
  SET `date` = v_end_date
  WHERE sport = v_sport
  AND   season = v_season
  AND   tweet_type = 'season_ended';

END $$

DELIMITER ;

##
## Merge multiple periods
##
DROP PROCEDURE IF EXISTS `records_validate_dates_merge`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_validate_dates_merge`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_date DATE
)
    COMMENT 'Record Breaker future period merging'
BEGIN

  DECLARE v_period_id TINYINT UNSIGNED;
  DECLARE v_period_order TINYINT UNSIGNED;
  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;
  DECLARE v_summarise TINYINT UNSIGNED;
  DECLARE v_period_merge TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Cursor for looping through the now-empty dates to be processed
  DECLARE cur_periods CURSOR FOR
    SELECT PERIOD_DATE.period_id, PERIOD_DATE.period_order, PERIOD_DATE.start_date, PERIOD_DATE.end_date, PERIOD_DATE.summarise
    FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD_DATE
    LEFT JOIN tmp_remaining_dates AS SCHED
      ON (SCHED.game_date BETWEEN PERIOD_DATE.start_date AND PERIOD_DATE.end_date)
    WHERE PERIOD_DATE.sport = v_sport
    AND   PERIOD_DATE.season = v_season
    AND   PERIOD_DATE.end_date >= v_date
    AND   SCHED.game_date IS NULL
    GROUP BY PERIOD_DATE.sport, PERIOD_DATE.season, PERIOD_DATE.period_id
    ORDER BY PERIOD_DATE.sport, PERIOD_DATE.season, PERIOD_DATE.period_order DESC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Loop through any periods this check finds
  OPEN cur_periods;
  loop_periods: LOOP

    FETCH cur_periods INTO v_period_id, v_period_order, v_start_date, v_end_date, v_summarise;
    IF v_done = 1 THEN LEAVE loop_periods; END IF;

    # Get the ID of the period we'll be merging in to
    SELECT period_id INTO v_period_merge
    FROM FANTASY_COMMON_PERIODS_DATES
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_order = (v_period_order + 1);

    # If this was the last period, the following steps won't work and will have a greater effect, so throw an error to catch this
    IF v_period_merge IS NULL THEN
      SET @v_msg = CONCAT('Final period in the schedule for ', v_sport, '-', v_season, ' (', v_period_id, ') cannot be merged.');
      SIGNAL SQLSTATE 'DB501' SET MESSAGE_TEXT = @v_msg;
    END IF;

    # Update the dates for the period we're merging in to (and port across the summarise flag, if it was set)
    UPDATE FANTASY_COMMON_PERIODS_DATES
    SET start_date = v_start_date,
        summarise = GREATEST(summarise, v_summarise)
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_id = v_period_merge;

    # Adjust the period_order flag accordingly
    UPDATE FANTASY_COMMON_PERIODS_DATES
    SET period_order = period_order - 1
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_order > v_period_order;

    # Finally, remove the now-emtpy date
    DELETE FROM FANTASY_COMMON_PERIODS_DATES
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_id = v_period_id;

    # Add some logging
    CALL _log(CONCAT('Merged ', v_sport, '-', v_season, ' period ', v_period_id, ' (Start: ', v_start_date, ', End: ', v_end_date,
      ', Order: ', v_period_order, ', Summarise: ', v_summarise, ') into period ', v_period_merge));

  END LOOP loop_periods;
  CLOSE cur_periods;

END $$

DELIMITER ;

##
## Split a single period with multiple day's events into multiple periods
##
DROP PROCEDURE IF EXISTS `records_validate_dates_split`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_validate_dates_split`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_date DATE
)
    COMMENT 'Record Breaker future period splitting'
BEGIN

  DECLARE v_period_id TINYINT UNSIGNED;
  DECLARE v_period_order TINYINT UNSIGNED;
  DECLARE v_period_order_split TINYINT UNSIGNED;
  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;
  DECLARE v_summarise TINYINT UNSIGNED;
  DECLARE v_num_dates TINYINT UNSIGNED;
  DECLARE v_split_date DATE;
  DECLARE v_split_next_start DATE;
  DECLARE v_period_split TINYINT UNSIGNED;
  DECLARE v_period_summarise TINYINT UNSIGNED;
  DECLARE v_period_display VARCHAR(255);
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Cursor for looping through the now-empty dates to be processed
  DECLARE cur_periods CURSOR FOR
    SELECT PERIOD_DATE.period_id, PERIOD_DATE.period_order, PERIOD_DATE.period_order, PERIOD_DATE.start_date, PERIOD_DATE.end_date, PERIOD_DATE.summarise, COUNT(DISTINCT SCHED.game_date) AS num_dates
    FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD_DATE
    LEFT JOIN tmp_remaining_dates AS SCHED
      ON (SCHED.game_date BETWEEN PERIOD_DATE.start_date AND PERIOD_DATE.end_date)
    WHERE PERIOD_DATE.sport = v_sport
    AND   PERIOD_DATE.season = v_season
    AND   PERIOD_DATE.end_date >= v_date
    GROUP BY PERIOD_DATE.sport, PERIOD_DATE.season, PERIOD_DATE.period_id
    HAVING num_dates > 1
    ORDER BY PERIOD_DATE.sport, PERIOD_DATE.season, PERIOD_DATE.period_order;
  # Cursor for identifying the specific dates in the period
  DECLARE cur_dates CURSOR FOR
    SELECT game_date
    FROM tmp_remaining_dates
    WHERE game_date BETWEEN v_start_date AND v_end_date
    ORDER BY game_date;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  SELECT MAX(period_id) INTO v_period_split
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season;

  # Loop through any periods this check finds
  OPEN cur_periods;
  loop_periods: LOOP

    FETCH cur_periods INTO v_period_id, v_period_order, v_period_order_split, v_start_date, v_end_date, v_summarise, v_num_dates;
    IF v_done = 1 THEN LEAVE loop_periods; END IF;
    SET v_period_display := '';

    # Bump the order of future periods
    UPDATE FANTASY_COMMON_PERIODS_DATES
    SET period_order = period_order + v_num_dates - 1
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_order > v_period_order;

    OPEN cur_dates;
    loop_dates: LOOP

      FETCH cur_dates INTO v_split_date;
      IF v_done = 1 THEN LEAVE loop_dates; END IF;

      # First date updates the existing period
      IF v_period_display = '' THEN
        SET v_period_display := CONCAT(v_period_id, ' (E:', v_split_date, ',O:', v_period_order, ',Su:0)');

        UPDATE FANTASY_COMMON_PERIODS_DATES
        SET end_date = v_split_date, summarise = 0
        WHERE sport = v_sport
        AND   season = v_season
        AND   period_id = v_period_id;

        SET v_split_next_start := DATE_ADD(v_split_date, INTERVAL 1 DAY);

      # Subsequent dates are new periods (preserving 'summarise' at the end_date)
      ELSE
        SET v_period_split := v_period_split + 1;
        SET v_period_order_split := v_period_order_split + 1;
        SET v_period_summarise := IF(v_split_date = v_end_date, v_summarise, 0);

        INSERT INTO FANTASY_COMMON_PERIODS_DATES (sport, season, period_id, start_date, end_date, period_order, summarise)
          VALUES (v_sport, v_season, v_period_split, v_split_next_start, v_split_date, v_period_order_split, v_period_summarise);

        SET v_period_display := CONCAT(v_period_display, ', ', v_period_split, ' (S:', v_split_next_start, ',E:', v_split_date, ',O:', v_period_order_split, '.Su:', v_period_summarise, ')');
        SET v_split_next_start := DATE_ADD(v_split_date, INTERVAL 1 DAY);
      END IF;

    END LOOP loop_dates;
    CLOSE cur_dates;

    # Add some logging
    CALL _log(CONCAT('Split ', v_sport, '-', v_season, ' period ', v_period_id, ' (S:', v_start_date, ',E:', v_end_date, ',O:', v_period_order, ',Su:', v_summarise, ') into periods ', v_period_display));

  END LOOP loop_periods;
  CLOSE cur_periods;

END $$

DELIMITER ;

##
## Create new periods when the season has been extended
##
DROP PROCEDURE IF EXISTS `records_validate_dates_extend`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_validate_dates_extend`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Record Breaker future period creation'
BEGIN

  DECLARE v_curr_season_end DATE;
  DECLARE v_period_id TINYINT UNSIGNED;
  DECLARE v_period_order TINYINT UNSIGNED;
  DECLARE v_new_date DATE;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Cursor for looping through the new dates to be processed
  DECLARE cur_new_periods CURSOR FOR
    SELECT game_date
    FROM tmp_remaining_dates
    WHERE game_date > v_curr_season_end
    ORDER BY game_date;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  SELECT MAX(end_date), MAX(period_id), MAX(period_order) INTO v_curr_season_end, v_period_id, v_period_order
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season;

  # Loop through the new periods this check finds
  OPEN cur_new_periods;
  loop_new_periods: LOOP

    FETCH cur_new_periods INTO v_new_date;
    IF v_done = 1 THEN LEAVE loop_new_periods; END IF;

    # Bump our counters
    SET v_period_id := v_period_id + 1;
    SET v_period_order := v_period_order + 1;

    # Now add the new date
    INSERT INTO FANTASY_COMMON_PERIODS_DATES (sport, season, period_id, start_date, end_date, period_order, summarise)
      VALUES (v_sport, v_season, v_period_id, v_new_date, v_new_date, v_period_order, 0);

    # Add some logging
    CALL _log(CONCAT('Created new ', v_sport, '-', v_season, ' period ', v_period_id, ' (S:', v_new_date, ',E:', v_new_date, ',O:', v_period_order, ',Su:0)'));

  END LOOP loop_new_periods;
  CLOSE cur_new_periods;

END $$

DELIMITER ;
