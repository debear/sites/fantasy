##
## Preview Stat/Rating Processing
##
DROP PROCEDURE IF EXISTS `records_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview`(
  v_sport VARCHAR(6),
  v_date DATE
)
    COMMENT 'Record Breaker selection preview calcs for a given period'
proc_preview: BEGIN

  DECLARE v_curr_period_order TINYINT UNSIGNED;
  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_game_id TINYINT UNSIGNED;
  DECLARE v_period_id TINYINT UNSIGNED;
  DECLARE v_no_sel TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_ts DECIMAL(8,3) UNSIGNED;

  # Loop through the games to be calculated
  DECLARE cur_games CURSOR FOR
    SELECT GAME.season, GAME.game_id, PERIOD.period_id, v_date < DATE(GAME.date_start) AS is_before_start
    FROM FANTASY_RECORDS_GAMES AS GAME
    JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
      ON (PERIOD.sport = GAME.sport
      AND PERIOD.season = GAME.season
      AND PERIOD.period_order IN (v_curr_period_order, v_curr_period_order + 1))
    LEFT JOIN FANTASY_RECORDS_GAMES_PERIODS_SUMMARY AS SUMMARY
      ON (SUMMARY.sport = GAME.sport
      AND SUMMARY.season = GAME.season
      AND SUMMARY.game_id = GAME.game_id
      AND SUMMARY.period_id = PERIOD.period_id)
    WHERE GAME.sport = v_sport
    AND   SUMMARY.period_id IS NULL
    ORDER BY PERIOD.period_order, GAME.game_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  CALL _log(CONCAT('Starting the preview calcs for sport ', v_sport, ' on ', v_date));

  # If we have no selection pool available, we cannot calculate a preview
  IF !sport_selections_available(v_sport, v_date) THEN
    CALL _log('Skipping - no selection pool is available yet');
    LEAVE proc_preview;
  END IF;

  # Determine the current period, from which our calcs originate
  SELECT IFNULL(MIN(period_order), 0) INTO v_curr_period_order
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   v_date BETWEEN start_date AND end_date;

  # Loop through the active games, and calc:
  OPEN cur_games;
  loop_games: LOOP

    FETCH cur_games INTO v_season, v_game_id, v_period_id, v_no_sel;
    IF v_done = 1 THEN LEAVE loop_games; END IF;

    CALL _log(CONCAT('Previewing ', v_sport, '-', v_season, '-', v_game_id, ' for period ', v_period_id, IF(v_no_sel, ' (pre-season calculations)', '')));
    SET v_ts = _NOW_MILLISEC();
    CALL records_preview_game(v_sport, v_season, v_game_id, v_period_id, 'all');
    CALL _log(CONCAT('Previewing ', v_sport, '-', v_season, '-', v_game_id, ' period ', v_period_id, ' complete (', (_NOW_MILLISEC() - v_ts), 's)'));

  END LOOP loop_games;
  CLOSE cur_games;

  IF v_season IS NULL THEN
    # Log if no games found to preview
    CALL _log(CONCAT('Skipping - no games found for previewing on ', v_date));
  END IF;

END $$

DELIMITER ;

##
## Preview calcs for a specific game
##
DROP PROCEDURE IF EXISTS `records_preview_game`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_game`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_sel_mode ENUM('all', 'missing-only')
)
    COMMENT 'Record Breaker selection preview of a specific game'
proc: BEGIN

  DECLARE v_start_date DATE;
  DECLARE v_sel_type ENUM('ind', 'team');
  DECLARE v_fn VARCHAR(50);
  DECLARE v_sel_num_proc SMALLINT UNSIGNED;
  DECLARE v_ts_a DECIMAL(8,3) SIGNED;
  DECLARE v_ts_b DECIMAL(8,3) SIGNED;
  SET v_ts_a = _NOW_MILLISEC();

  # Determine additional info for our processing
  SELECT PERIOD.start_date, GAME.sel_type, CONCAT(GAME.sport, '_', GAME.sel_type, '_', GAME.game_code, '_', GAME.score_type) AS calc_method
    INTO v_start_date, v_sel_type, v_fn
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
    ON (PERIOD.sport = GAME.sport
    AND PERIOD.season = GAME.season
    AND PERIOD.period_id = v_period_id)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id
  ORDER BY GAME.game_id;

  # Prune a previous run, though in a way that preserves selections that may have dropped off
  CALL records_preview_reset(v_sport, v_season, v_game_id, v_period_id);

  # Populate the link identification table
  CALL records_preview_setup();
  CALL records_preview_step(CONCAT(v_sport, '_', v_sel_type), 'sel', v_sport, v_season, v_game_id, v_period_id, v_start_date);
  SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Setup complete (', (v_ts_b - v_ts_a), 's)'));
  # If we're only previewing 'missing' selections in a post-processing run, and there are no new selections, there is no need to continue
  IF v_sel_mode = 'missing-only' THEN
    SELECT COUNT(*) INTO v_sel_num_proc
    FROM tmp_links
    LEFT JOIN tmp_previous_sel USING (link_id)
    WHERE tmp_previous_sel.link_id IS NULL;
    IF v_sel_num_proc = 0 THEN
      CALL records_preview_restore_previous(); # We still need to restore the selections we pruned during the reset though!
      CALL _log('- No missing selections, skipping preview run');
      LEAVE proc;
    END IF;
  END IF;

  # These will become our selections
  INSERT INTO FANTASY_RECORDS_SELECTIONS (sport, season, game_id, link_id, period_id, rating)
    SELECT v_sport, v_season, v_game_id, link_id, v_period_id, 0 AS rating
    FROM tmp_links;

  # Stats
  CALL records_preview_step(v_fn, 'preview', v_sport, v_season, v_game_id, v_period_id, v_start_date);
  SET v_ts_a = _NOW_MILLISEC(); CALL _log(CONCAT('- Stats complete (', (v_ts_a - v_ts_b), 's)'));
  # Opponent Ratings
  IF sport_is_major_league(v_sport) THEN
    CALL records_preview_step(v_fn, 'opprating', v_sport, v_season, v_game_id, v_period_id, v_start_date);
    SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Opp Rating complete (', (v_ts_b - v_ts_a), 's)'));
  ELSE
    SET v_ts_b = v_ts_a; # Preserve A/B benchmarking timestamps
  END IF;
  # Selection Ratings
  CALL records_preview_step(v_fn, 'selrating', v_sport, v_season, v_game_id, v_period_id, v_start_date);
  SET v_ts_a = _NOW_MILLISEC(); CALL _log(CONCAT('- Sel Rating complete (', (v_ts_a - v_ts_b), 's)'));

  IF v_sel_mode = 'all' THEN
    # Restore any previously calculated selections that were (for whatever reason) not included above
    CALL records_preview_restore_pruned(v_sport, v_season, v_game_id, v_period_id);
    SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Pruned Restoration complete (', (v_ts_b - v_ts_a), 's)'));
  ELSEIF v_sel_mode = 'missing-only' THEN
    # Restore the previously calculated selections that we preserve above these fresh calculations
    CALL records_preview_restore_previous();
    SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Previous Calc Restoration complete (', (v_ts_b - v_ts_a), 's)'));
  END IF;

END $$

DELIMITER ;

##
## Reset a previous run, temporarily preserving to ensure we can propagate selections that may have dropped off
##
DROP PROCEDURE IF EXISTS `records_preview_reset`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_reset`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker selection preview reset'
BEGIN

  # Selections
  DROP TEMPORARY TABLE IF EXISTS tmp_previous_sel;
  CREATE TEMPORARY TABLE tmp_previous_sel LIKE FANTASY_RECORDS_SELECTIONS;
  INSERT INTO tmp_previous_sel
    SELECT *
    FROM FANTASY_RECORDS_SELECTIONS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   period_id = v_period_id;

  DELETE FROM FANTASY_RECORDS_SELECTIONS WHERE sport = v_sport AND season = v_season AND game_id = v_game_id AND period_id = v_period_id;

  # Selection Stats
  DROP TEMPORARY TABLE IF EXISTS tmp_previous_sel_stats;
  CREATE TEMPORARY TABLE tmp_previous_sel_stats LIKE FANTASY_RECORDS_SELECTIONS_STATS;
  INSERT INTO tmp_previous_sel_stats
    SELECT *
    FROM FANTASY_RECORDS_SELECTIONS_STATS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   period_id = v_period_id;

  DELETE FROM FANTASY_RECORDS_SELECTIONS_STATS WHERE sport = v_sport AND season = v_season AND game_id = v_game_id AND period_id = v_period_id;

END $$

DELIMITER ;

##
## Reset a previous run, temporarily preserving to ensure we can propagate selections that may have dropped off
##
DROP PROCEDURE IF EXISTS `records_preview_restore_pruned`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_restore_pruned`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker restore pruned selection previews'
BEGIN

  # Selections
  INSERT INTO FANTASY_RECORDS_SELECTIONS
    SELECT PREV.*
    FROM tmp_previous_sel AS PREV
    LEFT JOIN FANTASY_RECORDS_SELECTIONS AS CALC
      ON (CALC.sport = PREV.sport
      AND CALC.season = PREV.season
      AND CALC.game_id = PREV.game_id
      AND CALC.period_id = PREV.period_id)
    WHERE PREV.sport = v_sport
    AND   PREV.season = v_season
    AND   PREV.game_id = v_game_id
    AND   PREV.period_id = v_period_id
    AND   CALC.period_id IS NULL;

  # Selection Stats
  INSERT INTO FANTASY_RECORDS_SELECTIONS_STATS
    SELECT PREV.*
    FROM tmp_previous_sel_stats AS PREV
    LEFT JOIN FANTASY_RECORDS_SELECTIONS_STATS AS CALC
      ON (CALC.sport = PREV.sport
      AND CALC.season = PREV.season
      AND CALC.game_id = PREV.game_id
      AND CALC.period_id = PREV.period_id)
    WHERE PREV.sport = v_sport
    AND   PREV.season = v_season
    AND   PREV.game_id = v_game_id
    AND   PREV.period_id = v_period_id
    AND   CALC.period_id IS NULL;

END $$

DELIMITER ;

##
## Restore the previosly calculated preview details, when only processing 'missing' selections
##
DROP PROCEDURE IF EXISTS `records_preview_restore_previous`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_restore_previous`()
    COMMENT 'Record Breaker restore previous selection previews'
BEGIN

  # Selections
  INSERT INTO FANTASY_RECORDS_SELECTIONS
    SELECT *
    FROM tmp_previous_sel
  ON DUPLICATE KEY UPDATE rating = VALUES(rating), opp_rating = VALUES(opp_rating),
                          score = VALUES(score), status = VALUES(status), stress = VALUES(stress),
                          summary = VALUES(summary), sel_pct = VALUES(sel_pct), note = VALUES(note);

  # Selection Stats
  # - stat_order will be preserved from the _new_ calcs, as we want the missing selections to be listed in the correct place
  INSERT INTO FANTASY_RECORDS_SELECTIONS_STATS
    SELECT *
    FROM tmp_previous_sel_stats
  ON DUPLICATE KEY UPDATE stat_id = VALUES(stat_id), stat_value = VALUES(stat_value);

END $$

DELIMITER ;

##
## Stat calc worker processing method
##
DROP PROCEDURE IF EXISTS `records_preview_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_setup`()
    COMMENT 'Record Breaker selection preview internal setup'
BEGIN

  # Build our temporary tables
  DROP TEMPORARY TABLE IF EXISTS tmp_links;
  CREATE TEMPORARY TABLE tmp_links (
    link_id SMALLINT UNSIGNED,
    link_sort VARCHAR(101),
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM;

  DROP TEMPORARY TABLE IF EXISTS tmp_link_stats;
  CREATE TEMPORARY TABLE tmp_link_stats (
    link_id SMALLINT UNSIGNED,
    link_sort VARCHAR(101),
    stat_value DOUBLE,
    stat_text VARCHAR(30),
    stat_order SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM;

END $$

DELIMITER ;

##
## Worker method for a specific step
##
DROP PROCEDURE IF EXISTS `records_preview_step`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_step`(
  v_fn VARCHAR(100),
  v_step VARCHAR(30),
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selection preview worker step'
BEGIN

  # This essentially duplicates an _exec call, but without the recursive limit
  SET @v_sql := CONCAT('CALL records_', v_fn, '_', v_step, '("', v_sport, '", "', v_season, '", "', v_game_id, '", "', v_period_id, '", "', v_start_date, '");');
  PREPARE sth_step FROM @v_sql;
  EXECUTE sth_step;
  DEALLOCATE PREPARE sth_step;

END $$

DELIMITER ;

##
## Stat calc worker processing method
##
DROP PROCEDURE IF EXISTS `records_preview_proc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_preview_proc`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_stat_id TINYINT UNSIGNED,
  v_stat_sort ENUM('ASC', 'DESC')
)
    COMMENT 'Record Breaker selection preview generic processing calcs'
BEGIN

  # Ensure our stats table includes those selections that may not have any stats yet
  INSERT IGNORE INTO tmp_link_stats (link_id, link_sort)
    SELECT link_id, link_sort FROM tmp_links;

  UPDATE tmp_links
  JOIN tmp_link_stats USING (link_id)
  SET tmp_link_stats.link_sort = tmp_links.link_sort;

  DELETE FROM tmp_link_stats WHERE link_sort IS NULL;

  UPDATE tmp_link_stats
  SET stat_value = IF(v_stat_sort = 'ASC', 99999, -99999)
  WHERE stat_value IS NULL;

  # Sort the stats
  UPDATE tmp_link_stats SET stat_order = NULL;
  CALL _duplicate_tmp_table('tmp_link_stats', 'tmp_link_stats_cpA');
  CALL _exec(CONCAT('INSERT INTO tmp_link_stats (link_id, stat_order)
    SELECT SQL_BIG_RESULT link_id, RANK() OVER w AS stat_order
    FROM tmp_link_stats_cpA
    WINDOW w AS (ORDER BY stat_value ', v_stat_sort, ')
  ON DUPLICATE KEY UPDATE stat_order = VALUES(stat_order);'));

  # And now store
  INSERT INTO FANTASY_RECORDS_SELECTIONS_STATS (sport, season, game_id, link_id, period_id, stat_id, stat_value, stat_order)
    SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id, link_id, v_period_id AS period_id, v_stat_id AS stat_id,
          stat_text AS stat_value, stat_order
    FROM tmp_link_stats;

END $$

DELIMITER ;

##
## Opponent rating calc worker processing method
##
DROP PROCEDURE IF EXISTS `records_opprating_proc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_opprating_proc`(
  v_venue_ratio DECIMAL(2, 1) UNSIGNED,
  v_stat_dir ENUM('ASC', 'DESC')
)
    COMMENT 'Record Breaker opponent rating generic processing calcs'
BEGIN

  DECLARE v_per_game_calc TINYINT UNSIGNED;

  # In most (but not all) instances we should calculate the per-game values
  SELECT COUNT(*) INTO v_per_game_calc FROM tmp_team_oppcalcs WHERE gp IS NOT NULL;
  IF v_per_game_calc > 0 THEN
    UPDATE tmp_team_oppcalcs
    SET stat_gm = IF(gp, (home_stat + visitor_stat) / gp, 0),
        home_stat_gm = IF(home_gp, home_stat / home_gp, 0),
        visitor_stat_gm = IF(visitor_gp, visitor_stat / visitor_gp, 0);
  END IF;

  # Apply a semblence of a statistical range from the mean
  ALTER TABLE tmp_team_oppcalcs
    ADD COLUMN stat_gm_rel TINYINT UNSIGNED AFTER stat_gm,
    ADD COLUMN home_stat_gm_rel TINYINT UNSIGNED AFTER home_stat_gm,
    ADD COLUMN visitor_stat_gm_rel TINYINT UNSIGNED AFTER visitor_stat_gm;

  CALL _statistical_range('tmp_team_oppcalcs', 'stat_gm', 'stat_gm_rel', NULL, 0, 100, v_stat_dir);
  CALL _statistical_range('tmp_team_oppcalcs', 'home_stat_gm', 'home_stat_gm_rel', NULL, 0, 100, v_stat_dir);
  CALL _statistical_range('tmp_team_oppcalcs', 'visitor_stat_gm', 'visitor_stat_gm_rel', NULL, 0, 100, v_stat_dir);

  # Now combine into a rating (overall + home/visitor, as appropriate)
  ALTER TABLE tmp_team_oppcalcs
    ADD COLUMN home_opp_rating TINYINT UNSIGNED AFTER team_id,
    ADD COLUMN visitor_opp_rating TINYINT UNSIGNED AFTER home_opp_rating;

  UPDATE tmp_team_oppcalcs
    SET home_opp_rating = ((1 - v_venue_ratio) * stat_gm_rel) + (v_venue_ratio * home_stat_gm_rel),
        visitor_opp_rating = ((1 - v_venue_ratio) * stat_gm_rel) + (v_venue_ratio * visitor_stat_gm_rel);

END $$

DELIMITER ;

##
## Opponent rating calc worker storing method
##
DROP PROCEDURE IF EXISTS `records_opprating_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_opprating_store`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker opponent rating storing'
BEGIN

  UPDATE FANTASY_RECORDS_SELECTIONS AS SEL
  JOIN tmp_links
    ON (tmp_links.link_id = SEL.link_id)
  JOIN tmp_team_oppcalcs
    ON (tmp_team_oppcalcs.team_id = tmp_links.opp_team_id)
  SET SEL.opp_rating = IF(tmp_links.is_home, tmp_team_oppcalcs.visitor_opp_rating, tmp_team_oppcalcs.home_opp_rating)
  WHERE SEL.sport = v_sport
  AND   SEL.season = v_season
  AND   SEL.game_id = v_game_id
  AND   SEL.period_id = v_period_id;

END $$

DELIMITER ;

##
## Selection rating calc worker processing method
##
DROP PROCEDURE IF EXISTS `records_selrating_proc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_selrating_proc`(
  v_tbl VARCHAR(25),
  v_stat_dir ENUM('ASC', 'DESC'),
  v_flags SET('none', 'calc_per60', 'calc_ypa', 'calc_mlbpythag', 'calc_nflpythag', 'calc_nhlpythag', 'calc_ahlpythag', 'apply_least')
)
    COMMENT 'Record Breaker selection rating generic processing calcs'
BEGIN

  DECLARE v_set_default VARCHAR(250);

  SELECT NULL INTO @v_mean;
  SELECT CONCAT('tmp_rating_', v_tbl) INTO v_tbl;
  SELECT IFNULL(v_flags, 'none') INTO v_flags;

  # Calculate the mean from the overall dataset
  CALL _exec(CONCAT('SELECT LEAST(1, COUNT(*)) INTO @v_calc_mean FROM ', v_tbl, ' WHERE stat IS NOT NULL AND stat_denom IS NOT NULL;'));
  IF @v_calc_mean > 0 THEN
    IF FIND_IN_SET('calc_per60', v_flags) > 0 THEN
      # Mean is calculated per 60mins (defined as 3600sec) of the denominator
      CALL _exec(CONCAT('SELECT CAST(SUM(stat_nom) * (3600 / SUM(stat_denom)) AS DECIMAL(10, 5)) INTO @v_mean FROM ', v_tbl, ';'));
    ELSEIF FIND_IN_SET('calc_ypa', v_flags) > 0 THEN
      # Mean is calculated as a manipulation of the nominator
      CALL _exec(CONCAT('SELECT POW(AVG(stat_nom), 0.8) / POW(AVG(stat_denom), 0.4) INTO @v_mean FROM ', v_tbl, ';'));
    ELSEIF (FIND_IN_SET('calc_mlbpythag', v_flags) > 0)
      OR (FIND_IN_SET('calc_nflpythag', v_flags) > 0)
      OR (FIND_IN_SET('calc_nhlpythag', v_flags) > 0)
      OR (FIND_IN_SET('calc_ahlpythag', v_flags) > 0)
    THEN
      # Mean is calculated for a pythagorean process
      IF FIND_IN_SET('calc_mlbpythag', v_flags) > 0 THEN
        SET @v_exponent := 1.83;
      ELSEIF FIND_IN_SET('calc_nflpythag', v_flags) > 0 THEN
        SET @v_exponent := 2.37;
      ELSEIF FIND_IN_SET('calc_nhlpythag', v_flags) > 0 THEN
        SET @v_exponent := 2.11;
      ELSEIF FIND_IN_SET('calc_ahlpythag', v_flags) > 0 THEN
        SET @v_exponent := 2.11;
      END IF;
      CALL _exec(CONCAT('SELECT POW(SUM(stat_nom), ', @v_exponent, ') / (POW(SUM(stat_nom), ', @v_exponent, ') + POW(SUM(stat_denom), ', @v_exponent, ')) INTO @v_mean FROM ', v_tbl, ';'));
    ELSE
      # Straight mean
      CALL _exec(CONCAT('SELECT CAST(SUM(stat_nom) / SUM(stat_denom) AS DECIMAL(10, 5)) INTO @v_mean FROM ', v_tbl, ';'));
    END IF;
  END IF;

  # We need to NULL values to a representative value (Mean minus half a STDDEV?)
  CALL _exec(CONCAT('SELECT AVG(stat) INTO @v_rows_mean FROM ', v_tbl, ';'));
  CALL _exec(CONCAT('SELECT STDDEV(stat) INTO @v_rows_stddev FROM ', v_tbl, ';'));
  # Some versions need to apply an upper limit (as >100 is essentially no different to 100)
  SELECT CONCAT('IFNULL(@v_mean, @v_rows_mean) ', IF(v_stat_dir = 'DESC', '-', '+'), ' (@v_rows_stddev / 2)') INTO v_set_default;
  CALL _exec(CONCAT('UPDATE ', v_tbl, '
    SET stat = ', IF(FIND_IN_SET('apply_least', v_flags) > 0, CONCAT('LEAST(99.99999, ', v_set_default, ')'), v_set_default), '
    WHERE stat IS NULL;'));

  # Apply a semblence of a statistical range from the (calculated) mean
  CALL _exec(CONCAT('ALTER TABLE ', v_tbl, '
    ADD COLUMN rating TINYINT UNSIGNED AFTER stat_denom;'));
  CALL _statistical_range(v_tbl, 'stat', 'rating', @v_mean, 0, 100, v_stat_dir);

END $$

DELIMITER ;

##
## Selection rating calc worker storing method
##
DROP PROCEDURE IF EXISTS `records_selrating_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_selrating_store`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker opponent rating storing'
BEGIN

  UPDATE FANTASY_RECORDS_SELECTIONS AS SEL
  JOIN tmp_links AS LINK
    ON (LINK.link_id = SEL.link_id)
  JOIN tmp_ratings AS RATING
    ON (RATING.link_id = SEL.link_id)
  SET SEL.rating = RATING.rating * LINK.status_multiplier
  WHERE SEL.sport = v_sport
  AND   SEL.season = v_season
  AND   SEL.game_id = v_game_id
  AND   SEL.period_id = v_period_id;

END $$

DELIMITER ;
