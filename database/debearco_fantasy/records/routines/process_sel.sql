#
# Prepare selection scoring updates for the game
#
DROP PROCEDURE IF EXISTS `records_scoring_sel_prep`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_sel_prep`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection process preparation'
BEGIN

  # Ensure all selections appear in the events table
  CALL _duplicate_tmp_table('tmp_link_events', 'tmp_link_events_cp');
  INSERT IGNORE INTO tmp_link_events (link_id, event_order, stat, status, stress, summary)
    SELECT LINK.link_id, 1 AS event_order, NULL AS stat, 'na' AS status, NULL AS stress, NULL AS summary
    FROM tmp_links AS LINK
    LEFT JOIN tmp_link_events_cp LINK_EVENT
      ON (LINK_EVENT.link_id = LINK.link_id)
    WHERE LINK_EVENT.event_order IS NULL
    GROUP BY LINK.link_id;

END $$

DELIMITER ;

#
# Process selection usage updates for the game
#
DROP PROCEDURE IF EXISTS `records_scoring_sel_usage`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_sel_usage`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection usage calcs'
BEGIN

  DECLARE v_tot_sel SMALLINT UNSIGNED;
  SELECT COUNT(*) INTO v_tot_sel
  FROM FANTASY_RECORDS_ENTRIES_BYPERIOD
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  AND   period_id = v_period_id
  AND   link_id IS NOT NULL;

  INSERT INTO tmp_links (link_id, sel_pct)
    SELECT link_id, 100 * (COUNT(*) / v_tot_sel) AS sel_pct
    FROM FANTASY_RECORDS_ENTRIES_BYPERIOD
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   period_id = v_period_id
    AND   link_id IS NOT NULL
    GROUP BY link_id
  ON DUPLICATE KEY UPDATE sel_pct = VALUES(sel_pct);

END $$

DELIMITER ;

#
# Process selections for games with a 'total' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_sel_proc_total`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_sel_proc_total`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection process for total score type'
BEGIN

  # Merge the totals into a per-period value
  INSERT INTO tmp_links (link_id, score, status, stress, summary)
    SELECT link_id,
           SUM(IFNULL(stat, 0)) AS score,
           MAX(status) AS status,
           MAX(stress) AS stress,
           GROUP_CONCAT(summary ORDER BY event_order SEPARATOR '; ') AS summary
    FROM tmp_link_events
    GROUP BY link_id
  ON DUPLICATE KEY UPDATE score = VALUES(score),
                          status = VALUES(status),
                          stress = VALUES(stress),
                          summary = VALUES(summary);

END $$

DELIMITER ;

#
# Process selections for games with a 'streak' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_sel_proc_streak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_sel_proc_streak`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection process for streak score type'
BEGIN

  # Merge the totals into a per-period value
  INSERT INTO tmp_links (link_id, score, status, stress, summary)
    SELECT link_id,
           IFNULL(IF(SUM(status = 'negative') > 0, 0, SUM(stat)), 0) AS score,
           IFNULL(IF(SUM(status = 'negative') > 0, 'negative', MAX(status)), 'na') AS status,
           MAX(stress) AS stress,
           GROUP_CONCAT(summary ORDER BY event_order SEPARATOR '; ') AS summary
    FROM tmp_link_events
    GROUP BY link_id
  ON DUPLICATE KEY UPDATE score = VALUES(score),
                          status = VALUES(status),
                          stress = VALUES(stress),
                          summary = VALUES(summary);

END $$

DELIMITER ;

#
# Process selections for games with a 'periods' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_sel_proc_periods`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_sel_proc_periods`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection process for periods score type'
BEGIN

  # Processing-wise, this is an equivalent to a 'total' game
  CALL records_scoring_sel_proc_total(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);

END $$

DELIMITER ;

#
# Store selection scoring updates for the game
#
DROP PROCEDURE IF EXISTS `records_scoring_sel_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_sel_store`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection process for streak score type'
BEGIN

  # First, the individual events
  INSERT INTO FANTASY_RECORDS_SELECTIONS_SCORING (sport, season, game_id, link_id, period_id, event_order, stat, status, stress, summary)
    SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id, link_id, v_period_id AS period_id,
           event_order, stat, status, stress, summary
    FROM tmp_link_events
  ON DUPLICATE KEY UPDATE stat = VALUES(stat),
                          status = VALUES(status),
                          stress = VALUES(stress),
                          summary = VALUES(summary);

  # The merged per-period value
  UPDATE tmp_links AS LINK
  JOIN FANTASY_RECORDS_SELECTIONS AS SEL
    ON (SEL.sport = v_sport
    AND SEL.season = v_season
    AND SEL.game_id = v_game_id
    AND SEL.link_id = LINK.link_id
    AND SEL.period_id = v_period_id)
  SET SEL.score = LINK.score,
      SEL.status = LINK.status,
      SEL.stress = LINK.stress,
      SEL.summary = LINK.summary,
      SEL.sel_pct = LINK.sel_pct;

END $$

DELIMITER ;
