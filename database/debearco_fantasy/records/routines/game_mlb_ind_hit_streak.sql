##
## X Game Hitting Streak
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hit_streak_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hit_streak_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for mlb:ind:hit:streak'
BEGIN

  DECLARE v_split_LHP TINYINT UNSIGNED;
  DECLARE v_split_RHP TINYINT UNSIGNED;
  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN avg_rank DOUBLE,
    ADD COLUMN avg_text VARCHAR(30),
    ADD COLUMN vs_lrhp_value DOUBLE,
    ADD COLUMN vs_lrhp_text VARCHAR(30),
    ADD COLUMN vs_sp_value DOUBLE,
    ADD COLUMN vs_sp_text VARCHAR(30);

  # Pre-calc the Split IDs
  SELECT GROUP_CONCAT(IF(split_label = 'v LHP', split_id, '') SEPARATOR ''),
         GROUP_CONCAT(IF(split_label = 'v RHP', split_id, '') SEPARATOR '')
    INTO v_split_LHP, v_split_RHP
  FROM debearco_sports.SPORTS_MLB_PLAYERS_SPLIT_LABELS
  WHERE split_type = 'pitcher-info'
  AND   split_label IN ('v LHP', 'v RHP');

  # Calculate the Season Avg, vs L/RHP and vs Starter (though we'll use the pre-calced sorting for the ordering as this already factors in qualified hitters)
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, avg_rank, avg_text, vs_lrhp_value, vs_lrhp_text, vs_sp_value, vs_sp_text)
    SELECT tmp_links.link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           IFNULL(SEASON_SORT.avg, 9999) AS avg_rank,
           IF(SEASON.avg IS NOT NULL, CONCAT(TRIM(LEADING '0' FROM FORMAT(SEASON.avg, 3)), ' (', SEASON.h, '/', SEASON.ab, ')'), '&ndash;') AS avg_text,
           IFNULL(SPLITS.avg, -0.1) AS vs_lrhp_value,
           IF(SPLITS.avg IS NOT NULL, CONCAT(TRIM(LEADING '0' FROM FORMAT(SPLITS.avg, 3)), ' (', SPLITS.h, '/', SPLITS.ab, ')'), '&ndash;') AS vs_lrhp_text,
           IFNULL(BVP.avg, -0.1) AS vs_sp_value,
           IF(BVP.avg IS NOT NULL, CONCAT(TRIM(LEADING '0' FROM FORMAT(BVP.avg, 3)), ' (', BVP.h, '/', BVP.ab, ')'), '&ndash;') AS vs_sp_text
    FROM tmp_links
    LEFT JOIN tmp_period_teams
      ON (tmp_period_teams.team_id = tmp_links.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_SEASON_BATTING AS SEASON
      ON (SEASON.season = v_season
      AND SEASON.season_type = 'regular'
      AND SEASON.player_id = tmp_links.link_id
      AND SEASON.is_totals = 1)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED AS SEASON_SORT
      ON (SEASON_SORT.season = SEASON.season
      AND SEASON_SORT.season_type = SEASON.season_type
      AND SEASON_SORT.player_id = SEASON.player_id
      AND SEASON_SORT.is_totals = SEASON.is_totals)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS AS SPLITS
      ON (SPLITS.season = v_season
      AND SPLITS.season_type = 'regular'
      AND SPLITS.player_id = tmp_links.link_id
      AND SPLITS.split_type = 'pitcher-info'
      AND SPLITS.split_label = IF(tmp_period_teams.opp_probable_throws = 'R', v_split_RHP, v_split_LHP))
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER AS BVP
      ON (BVP.batter = tmp_links.link_id
      AND BVP.pitcher = tmp_period_teams.opp_probable_id
      AND BVP.season_type = 'regular');

  # Now process the individual stat elements
  # 1 - Season Avg
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'average') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = avg_rank,
        stat_text = avg_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - vs L/RHP
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'vs-lhp-rhp') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = vs_lrhp_value,
        stat_text = vs_lrhp_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 3 - vs Starter
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'vs-starter') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = vs_sp_value,
        stat_text = vs_sp_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN avg_rank,
    DROP COLUMN avg_text,
    DROP COLUMN vs_lrhp_value,
    DROP COLUMN vs_lrhp_text,
    DROP COLUMN vs_sp_value,
    DROP COLUMN vs_sp_text;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hit_streak_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hit_streak_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for mlb:ind:hit:streak'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 81);

  # Calc 1: Hits Allowed
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.h, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.h, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_GAME_PITCHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'DESC'); # 60/40 Overall-vs-Home/Road, More Hits Allowed makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_hits;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_hits;

  # Calc 2: Game Score
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.game_score, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.game_score, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_GAME_PITCHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'ASC'); # 60/40 Overall-vs-Home/Road, Lower Game Score makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_gamescore;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_gamescore;

  # Calc 3: Batters Faced
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.h, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.h, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_GAME_PITCHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(1.0, 'DESC'); # 0/100 Overall-vs-Home/Road, More Batters Faced makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_batters;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_batters;

  # Merge (split 40/40/20 Hits/Game Score/Batters) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    home_opp_rating TINYINT UNSIGNED,
    visitor_opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id,
           (HITS.home_opp_rating * 0.40) + (GAMESCORE.home_opp_rating * 0.40) + (BATTERS.home_opp_rating * 0.20) AS home_opp_rating,
           (HITS.visitor_opp_rating * 0.40) + (GAMESCORE.visitor_opp_rating * 0.40) + (BATTERS.visitor_opp_rating * 0.20) AS visitor_opp_rating
    FROM tmp_all_teams AS TEAMS
    JOIN tmp_team_oppcalcs_hits AS HITS
      ON (HITS.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_gamescore AS GAMESCORE
      ON (GAMESCORE.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_batters AS BATTERS
      ON (BATTERS.team_id = TEAMS.team_id);
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hit_streak_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hit_streak_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for mlb:ind:hit:streak'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_mlb_ind_sel_recent_sched(v_season, v_start_date, 81);

  # Opportunity 1: Depth Chart (direct rating calc)
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_depth;
  CREATE TEMPORARY TABLE tmp_rating_opportunity_depth (
    link_id SMALLINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           100 * MAX(DEPTH.season IS NOT NULL) AS rating
    FROM tmp_links AS LINK
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_DEPTH AS DEPTH
      ON (DEPTH.season = v_season
      AND DEPTH.player_id = LINK.link_id
      AND DEPTH.depth = 1)
    GROUP BY LINK.link_id;

  # Opportunity 2: PA / GP
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(STAT.pa) AS stat,
           SUM(STAT.pa) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Plate Appearances make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_pagp;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_pagp;

  # Opportunity 3: GS relative to GP
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(ROSTER.played) > 0, SUM(ROSTER.started) / SUM(ROSTER.played), NULL) AS stat,
           SUM(ROSTER.started) AS stat_nom,
           SUM(ROSTER.played) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_GAME_ROSTERS AS ROSTER
      ON (ROSTER.season = RECENT.season
      AND ROSTER.game_type = RECENT.game_type
      AND ROSTER.game_id = RECENT.game_id
      AND ROSTER.player_id = RECENT.link_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Starts make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_starts;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_starts;

  # Metric 1: Avg
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.ab) > 0, SUM(STAT.h) / SUM(STAT.ab), NULL) AS stat,
           SUM(STAT.h) AS stat_nom,
           SUM(STAT.ab) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', NULL); # Higher Average makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_avg;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_avg;

  # Metric 2: Win Prob
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) SIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           SUM(STAT.winprob) / 100 AS stat,
           NULL AS stat_nom,
           NULL AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', NULL); # Higher WinProb makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_winprob;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_winprob;

  # Merge (split opportunity 18/16/16 Depth/PA-GP/GS-GP, metrics 22/13 Avg/WinProb, 15 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_opp_depth TINYINT UNSIGNED,
    rating_opp_pagp TINYINT UNSIGNED,
    rating_opp_starts TINYINT UNSIGNED,
    rating_metric_avg TINYINT UNSIGNED,
    rating_metric_winprob TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           OPP_DEPTH.rating AS rating_opp_depth,
           OPP_PAGP.rating AS rating_opp_pagp,
           OPP_STARTS.rating AS rating_opp_starts,
           METRIC_AVG.rating AS rating_metric_avg,
           METRIC_WINPROB.rating AS rating_metric_winprob,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(OPP_DEPTH.rating, 0) * 0.18) + (IFNULL(OPP_PAGP.rating, 0) * 0.16) + (IFNULL(OPP_STARTS.rating, 0) * 0.16)
             + (IFNULL(METRIC_AVG.rating, 0) * 0.22) + (IFNULL(METRIC_WINPROB.rating, 0) * 0.13)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.15) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_opportunity_depth AS OPP_DEPTH
      ON (OPP_DEPTH.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_pagp AS OPP_PAGP
      ON (OPP_PAGP.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_starts AS OPP_STARTS
      ON (OPP_STARTS.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_avg AS METRIC_AVG
      ON (METRIC_AVG.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_winprob AS METRIC_WINPROB
      ON (METRIC_WINPROB.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hit_streak_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hit_streak_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for mlb:ind:hit:streak'
BEGIN

  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
    ON (STAT.season = SEL.season
    AND STAT.game_type = SEL.game_type
    AND STAT.game_id = SEL.game_id
    AND STAT.player_id = SEL.link_id)
  SET SEL.stat = IF(STAT.ab > 0, STAT.h > 0, NULL),
      SEL.status = CASE
        WHEN STAT.ab = 0 THEN 'na'
        WHEN STAT.h = 0 THEN 'negative'
        ELSE 'positive'
      END,
      SEL.summary = IF(STAT.ab > 0, CONCAT(STAT.h, ' / ', STAT.ab), 'No AB');

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hit_streak_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hit_streak_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for mlb:ind:hit:streak'
BEGIN

  # Determine each hitter's at bats in order
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress_pa;
  CREATE TEMPORARY TABLE tmp_link_stress_pa (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    started TINYINT UNSIGNED,
    play_id TINYINT UNSIGNED,
    ab_order TINYINT UNSIGNED,
    inning TINYINT UNSIGNED,
    is_pa TINYINT UNSIGNED,
    is_ab TINYINT UNSIGNED,
    is_hit TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id, play_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.game_id, SEL.started, ATBAT.play_id, NULL AS ab_order, ATBAT.inning, ATBAT.is_pa, ATBAT.is_ab, ATBAT.is_hit
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_MLB_GAME_ATBAT_FLAGS AS ATBAT
      ON (ATBAT.season = SEL.season
      AND ATBAT.game_type = SEL.game_type
      AND ATBAT.game_id = SEL.game_id
      AND ATBAT.batter = SEL.link_id
      AND ATBAT.is_pa = 1);

  CALL _duplicate_tmp_table('tmp_link_stress_pa', 'tmp_link_stress_pa_cpA');
  INSERT INTO tmp_link_stress_pa (link_id, season, game_type, game_id, play_id, ab_order)
    SELECT SQL_BIG_RESULT link_id, season, game_type, game_id, play_id, ROW_NUMBER() OVER w AS ab_order
    FROM tmp_link_stress_pa_cpA
    WINDOW w AS (PARTITION BY link_id, season, game_type, game_id ORDER BY link_id, season, game_type, game_id, play_id)
  ON DUPLICATE KEY UPDATE ab_order = VALUES(ab_order);
  DROP TEMPORARY TABLE tmp_link_stress_pa_cpA;

  # Use this to determine their first hit
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    started TINYINT UNSIGNED,
    stress TINYINT UNSIGNED,
    pa_total TINYINT UNSIGNED,
    ab_total TINYINT UNSIGNED,
    ab_first_hit TINYINT UNSIGNED,
    inn_first_hit TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT link_id, season, game_type, game_id, started, NULL AS stress,
           SUM(is_pa) AS pa_total, SUM(is_ab) AS ab_total,
           MIN(IF(is_hit = 1, ab_order, 99)) AS ab_first_hit,
           MIN(IF(is_hit = 1, inning, 99)) AS inn_first_hit
    FROM tmp_link_stress_pa
    GROUP BY link_id, season, game_type, game_id;

  # Group 1: Player (starter or pinch hitter) with 0 AB
  # Group 2: Player (starter or pinch hitter) with 1+ AB and 0 Hits
  UPDATE tmp_link_stress
  SET stress = 255
  WHERE ab_total = 0
  OR    inn_first_hit = 99;
  # Group 3: Starter with 1+ Hit
  UPDATE tmp_link_stress
  SET stress = CASE
      WHEN inn_first_hit < 7 THEN ((inn_first_hit / 6) * 155)
      WHEN inn_first_hit < 9 THEN (((inn_first_hit - 6) / 3) * 100) + 155
      ELSE 255
    END
  WHERE started = 1
  AND   inn_first_hit < 99;
  # Group 4: Pinch hitter with 1+ Hit
  UPDATE tmp_link_stress
  SET stress = ((ab_first_hit / pa_total) * 58) + 192
  WHERE started = 0
  AND   ab_first_hit < 99;

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na' OR SEL_STRESS.stress IS NOT NULL, IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
