#
# Creation process for social media updates
#
DROP PROCEDURE IF EXISTS `records_twitter`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter`(
  v_sport VARCHAR(6),
  v_date DATE
)
    COMMENT 'Record Breaker twitter post creation'
BEGIN

  # Our flag variables
  DECLARE v_run_season_soon TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_run_reg_opening TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_run_season_tomorrow TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_run_reg_closing TINYINT UNSIGNED DEFAULT 0;

  # Our games to process
  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_game_id TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_game_name VARCHAR(30);
  DECLARE v_season_soon TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_reg_opening TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_season_tomorrow TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_reg_closing TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_period_id TINYINT UNSIGNED DEFAULT NULL;
  DECLARE v_period_digest TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_season_ended TINYINT UNSIGNED DEFAULT 0;

  # Loop through the games to be processed
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;
  DECLARE cur_games CURSOR FOR
    SELECT GAME.season, GAME.game_id, GAME.name,
           SUM(IFNULL(TWITTER_SCHED.tweet_type, 'NULL') = 'season_soon') > 0 AS season_soon,
           SUM(IFNULL(TWITTER_SCHED.tweet_type, 'NULL') = 'reg_opening') > 0 AS reg_opening,
           SUM(IFNULL(TWITTER_SCHED.tweet_type, 'NULL') = 'season_tomorrow') > 0 AS season_tomorrow,
           SUM(IFNULL(TWITTER_SCHED.tweet_type, 'NULL') = 'reg_closing') > 0 AS reg_closing,
           PERIOD.period_id,
           IFNULL(PERIOD.summarise, 0) = 1
            AND PERIOD_SUMMARY.period_id IS NOT NULL
            AND PERIOD_SUMMARY.when_tweeted IS NULL AS period_digest,
           SUM(IFNULL(TWITTER_SCHED.tweet_type, 'NULL') = 'season_ended') > 0 AS season_ended
    FROM FANTASY_RECORDS_GAMES AS GAME
    LEFT JOIN FANTASY_RECORDS_SETUP_TWITTER AS TWITTER_SCHED
      ON (TWITTER_SCHED.sport = GAME.sport
      AND TWITTER_SCHED.season = GAME.season
      AND TWITTER_SCHED.game_id = GAME.game_id
      AND TWITTER_SCHED.date = v_date)
    LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
      ON (PERIOD.sport = GAME.sport
      AND PERIOD.season = GAME.season
      AND IF(sport_is_major_league(v_sport), DATE_SUB(v_date, INTERVAL 1 DAY), v_date) # Major League processing occurs for games completed the day before
        BETWEEN PERIOD.start_date AND PERIOD.end_date)
    LEFT JOIN FANTASY_RECORDS_GAMES_PERIODS_SUMMARY AS PERIOD_SUMMARY
      ON (PERIOD_SUMMARY.sport = GAME.sport
      AND PERIOD_SUMMARY.season = GAME.season
      AND PERIOD_SUMMARY.game_id = GAME.game_id
      AND PERIOD_SUMMARY.period_id = PERIOD.period_id)
    WHERE GAME.sport = v_sport
    GROUP BY GAME.sport, GAME.season, GAME.game_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  CALL _log(CONCAT('Starting the Twitter posts for sport ', v_sport, ' on ', v_date));

  # Loop through the games and determine what needs processing
  OPEN cur_games;
  loop_games: LOOP

    FETCH cur_games INTO v_season, v_game_id, v_game_name, v_season_soon, v_reg_opening, v_season_tomorrow, v_reg_closing, v_period_id, v_period_digest, v_season_ended;
    IF v_done = 1 THEN LEAVE loop_games; END IF;

    # Season Starting Soon
    IF v_season_soon = 1 AND v_run_season_soon = 0 THEN
      CALL _log(CONCAT('- Creating ', v_sport, '-', v_season, '-', v_game_id, ' "season starting soon" post'));
      CALL records_twitter_season_soon(v_sport, v_season);
      SET v_run_season_soon = 1;
    END IF;
    # Registration Opening
    IF v_reg_opening = 1 AND v_run_reg_opening = 0 THEN
      CALL _log(CONCAT('- Creating ', v_sport, '-', v_season, '-', v_game_id, ' "registration opening" post'));
      CALL records_twitter_registration_opening(v_sport, v_season);
      SET v_run_reg_opening = 1;
    END IF;
    # Season Starting Tomorrow
    IF v_season_tomorrow = 1 AND v_run_season_tomorrow = 0 THEN
      CALL _log(CONCAT('- Creating ', v_sport, '-', v_season, '-', v_game_id, ' "season starting tomorrow" post'));
      CALL records_twitter_season_starting_tomorrow(v_sport, v_season);
      SET v_run_season_tomorrow = 1;
    END IF;
    # Registration Closing
    IF v_reg_closing = 1 AND v_run_reg_closing = 0 THEN
      CALL _log(CONCAT('- Creating ', v_sport, '-', v_season, '-', v_game_id, ' "registration closing" post'));
      CALL records_twitter_registration_closing(v_sport, v_season, v_game_id, v_game_name);
      SET v_run_reg_closing = 1;
    END IF;
    # Weekly Update
    IF v_period_id IS NOT NULL AND v_period_digest = 1 THEN
      CALL _log(CONCAT('- Creating ', v_sport, '-', v_season, '-', v_game_id, ' "weekly update" post'));
      CALL records_twitter_weekly_update(v_sport, v_season, v_game_id, v_game_name, v_period_id);
    END IF;
    # Record Beaten
    IF v_period_id IS NOT NULL THEN
      CALL _log(CONCAT('- Processing ', v_sport, '-', v_season, '-', v_game_id, ' "record beaten" post'));
      CALL records_twitter_record_beaten(v_sport, v_season, v_game_id, v_game_name, v_period_id);
    END IF;
    # Season Ended
    IF v_season_ended = 1 THEN
      CALL _log(CONCAT('- Creating ', v_sport, '-', v_season, '-', v_game_id, ' "season ended" post'));
      CALL records_twitter_season_ended(v_sport, v_season, v_game_id, v_game_name);
    END IF;

  END LOOP loop_games;
  CLOSE cur_games;

  # Log if no games found to process
  IF v_season IS NULL THEN
    CALL _log(CONCAT('Skipping - no games found for processing on ', v_date));
  END IF;

END $$

DELIMITER ;

##
## Season Starting Soon
##
DROP PROCEDURE IF EXISTS `records_twitter_season_soon`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_season_soon`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Record Breaker Season Starting Soon twitter post creation'
BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);

  # Load the template
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_upcoming');
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, NULL, 'error-missing', v_template_id, v_tweet_body);

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

END $$

DELIMITER ;

##
## Registration Opening
##
DROP PROCEDURE IF EXISTS `records_twitter_registration_opening`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_registration_opening`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Record Breaker Registration Opening twitter post creation'
BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);

  # Load the template
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_opening');
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, 17, 'error-missing', v_template_id, v_tweet_body);
  # Perform the interpolation
  SET v_tweet_body = REPLACE(v_tweet_body, '{domain}', records_config('domain'));

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

END $$

DELIMITER ;

##
## Season Starting Tomorrow
##
DROP PROCEDURE IF EXISTS `records_twitter_season_starting_tomorrow`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_season_starting_tomorrow`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Record Breaker Season Starting Tomorrow twitter post creation'
BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);

  # Load the template
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_starting');
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, NULL, 'error-missing', v_template_id, v_tweet_body);
  # Perform the interpolation
  SET v_tweet_body = REPLACE(REPLACE(v_tweet_body,
    '{domain}', records_config('domain')),
      '{season}', v_season);

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

END $$

DELIMITER ;

##
## Registration Closing
##
DROP PROCEDURE IF EXISTS `records_twitter_registration_closing`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_registration_closing`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_game_name VARCHAR(30)
)
    COMMENT 'Record Breaker Registration Closing twitter post creation'
BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);

  # Load the template, which in this scenario could be game-specific
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_closing_', v_game_id);
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, NULL, 'ignore-missing', v_template_id, v_tweet_body);
  IF v_template_id IS NULL THEN
    SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_closing');
    CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, NULL, 'error-missing', v_template_id, v_tweet_body);
  END IF;

  # Perform the interpolation
  SET v_tweet_body = REPLACE(REPLACE(v_tweet_body,
    '{domain}', records_config('domain')),
      '{slug}', CONCAT(v_sport, '/', name2slug(v_game_name), '-', v_season));

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

END $$

DELIMITER ;

##
## Weekly Update
##
DROP PROCEDURE IF EXISTS `records_twitter_weekly_update`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_weekly_update`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_game_name VARCHAR(30),
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker Weekly Update twitter post creation'
proc_leader: BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);
  DECLARE v_media_id TINYINT UNSIGNED;

  DECLARE v_score DECIMAL(5,1);
  DECLARE v_first_start DATE;
  DECLARE v_period_num TINYINT UNSIGNED;
  DECLARE v_proj_ratio DECIMAL(5,4) UNSIGNED;
  DECLARE v_period_name VARCHAR(75);
  DECLARE v_period_rem TINYINT UNSIGNED;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_score = NULL;

  # Load the template
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_leader_', v_game_id);
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, 17, 'error-missing', v_template_id, v_tweet_body);
  # Get the additional info out
  SELECT overall_res
    INTO v_score
  FROM FANTASY_RECORDS_ENTRIES_SCORES
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  AND   overall_pos = 1
  ORDER BY stress
  LIMIT 1;

  # If we have no leader, we have nothing to post about!
  IF v_score IS NULL THEN
    LEAVE proc_leader;
  END IF;

  SELECT MIN(start_date) INTO v_first_start
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season;

  SELECT IF(PERIOD.name IS NOT NULL, PERIOD.period_order, FLOOR(DATEDIFF(PERIOD.end_date, v_first_start) / 7)),
         PERIOD.name,
         COUNT(DISTINCT PERIOD_REM_SUM.period_id) + 1,
         PERIOD.period_order / IFNULL(MAX(PERIOD_REM.period_order), PERIOD.period_order)
    INTO v_period_num,
         v_period_name,
         v_period_rem,
         v_proj_ratio
  FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD
  LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD_REM_SUM
    ON (PERIOD_REM_SUM.sport = PERIOD.sport
    AND PERIOD_REM_SUM.season = PERIOD.season
    AND PERIOD_REM_SUM.summarise = 1
    AND PERIOD_REM_SUM.period_order > PERIOD.period_order)
  LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD_REM
    ON (PERIOD_REM.sport = PERIOD.sport
    AND PERIOD_REM.season = PERIOD.season
    AND PERIOD_REM.period_order > PERIOD.period_order)
  WHERE PERIOD.sport = v_sport
  AND   PERIOD.season = v_season
  AND   PERIOD.period_id = v_period_id;

  # Perform the interpolation
  SET v_tweet_body = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(v_tweet_body,
    '{period_name}', IFNULL(v_period_name, '')),
      '{period_num}', v_period_num),
        '{period_num_s}', IF(v_period_num = 1, '', 's')),
          '{period_num_ord}', ordinal_suffix(v_period_num)),
            '{periods_remaining_num}', v_period_rem),
              '{periods_remaining_num_s}', IF(v_period_rem = 1.0, '', 's')),
                '{projected_score}', records_social_format_score(IF(v_proj_ratio, v_score / v_proj_ratio, v_score))),
                  '{projected_score_s}', IF(IF(v_proj_ratio, v_score / v_proj_ratio, v_score) = 1.0, '', 's')),
                    '{score}', records_social_format_score(v_score)),
                      '{score_s}', IF(v_score = 1.0, '', 's')),
                        '{score_s-ve}', IF(v_score = 1.0, 's', 've')),
                          '{domain}', records_config('domain')),
                            '{slug}', CONCAT(v_sport, '/', name2slug(v_game_name), '-', v_season));

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

  # Open Graph as the media
  CALL debearco_common.twitter_queue_media(
    records_config('twitter_app_internal'),
    v_tweet_id,
    CONCAT(records_config('twitter_og_path'), '/', v_sport, '-', v_season, '-', v_game_id, '/', v_period_id, '.', records_config('twitter_og_ext')),
    CONCAT(v_sport, '-', v_season, '-', v_game_id, '-', v_period_id),
    10,
    v_media_id
  );

  # Stamp the update occurred
  UPDATE FANTASY_RECORDS_GAMES_PERIODS_SUMMARY
  SET when_tweeted = _NOW()
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  AND   period_id = v_period_id;

END $$

DELIMITER ;

##
## Record Beaten
##
DROP PROCEDURE IF EXISTS `records_twitter_record_beaten`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_record_beaten`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_game_name VARCHAR(30),
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker Record Beaten twitter post creation'
proc_recbeaten: BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);
  DECLARE v_media_id TINYINT UNSIGNED;

  DECLARE v_owner VARCHAR(30);
  DECLARE v_score DECIMAL(5,1);

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_score = NULL;

  # Determine if the current leader has matched (or exceeded) the target score
  SELECT IFNULL(AUTOUSER.display_name, USERS.display_name), ENTRY_SCORE.overall_res
    INTO v_owner, v_score
  FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
  JOIN FANTASY_RECORDS_GAMES AS GAME
    ON (GAME.sport = ENTRY_SCORE.sport
    AND GAME.season = ENTRY_SCORE.season
    AND GAME.game_id = ENTRY_SCORE.game_id
    AND GAME.target <= ENTRY_SCORE.overall_res)
  LEFT JOIN debearco_admin.USERS
    ON (USERS.id = ENTRY_SCORE.user_id)
  LEFT JOIN FANTASY_AUTOMATED_USER AS AUTOUSER
    ON (AUTOUSER.id = ENTRY_SCORE.user_id)
  WHERE ENTRY_SCORE.sport = v_sport
  AND   ENTRY_SCORE.season = v_season
  AND   ENTRY_SCORE.game_id = v_game_id
  AND   ENTRY_SCORE.overall_pos = 1
  ORDER BY ENTRY_SCORE.stress
  LIMIT 1;

  IF v_score IS NULL THEN
    # No matching score found, so nothing to post
    LEAVE proc_recbeaten;
  END IF;

  # Load the template
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_beaten_', v_game_id);
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, 999, 'ignore-missing', v_template_id, v_tweet_body);
  # We only want to post this the first time it occurs, so if we have already tweeted this season (no template is loaded) then we will not post this instance
  IF v_template_id IS NULL THEN
    LEAVE proc_recbeaten;
  END IF;

  # Perform the interpolation
  SET v_tweet_body = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(v_tweet_body,
    '{owner}', v_owner),
      '{score}', records_social_format_score(v_score)),
        '{score_ord}', ordinal_suffix(records_social_format_score(v_score))),
          '{domain}', records_config('domain')),
            '{slug}', CONCAT(v_sport, '/', name2slug(v_game_name), '-', v_season));

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

  # Open Graph as the media
  CALL debearco_common.twitter_queue_media(
    records_config('twitter_app_internal'),
    v_tweet_id,
    CONCAT(records_config('twitter_og_path'), '/', v_sport, '-', v_season, '-', v_game_id, '/', v_period_id, '.', records_config('twitter_og_ext')),
    CONCAT(v_sport, '-', v_season, '-', v_game_id, '-', v_period_id),
    10,
    v_media_id
  );

END $$

DELIMITER ;

##
## Season Ended
##
DROP PROCEDURE IF EXISTS `records_twitter_season_ended`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_twitter_season_ended`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_game_name VARCHAR(30)
)
    COMMENT 'Record Breaker Season Ended twitter post creation'
proc_ended: BEGIN

  DECLARE v_template_id TINYINT UNSIGNED;
  DECLARE v_tweet_id INT UNSIGNED;
  DECLARE v_tweet_body VARCHAR(350);
  DECLARE v_tweet_type VARCHAR(30);
  DECLARE v_media_id TINYINT UNSIGNED;

  DECLARE v_period_id TINYINT UNSIGNED;
  DECLARE v_owner VARCHAR(30);
  DECLARE v_score DECIMAL(5,1);

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_score = NULL;

  # Load the template
  SET v_tweet_type = CONCAT(v_sport, '_', v_season, '_ended_', v_game_id);
  CALL debearco_common.twitter_get_template(records_config('twitter_app_internal'), v_tweet_type, NULL, 'error-missing', v_template_id, v_tweet_body);

  # Get the additional info out
  SELECT IFNULL(AUTOUSER.display_name, USERS.display_name), ENTRY_SCORE.overall_res
    INTO v_owner, v_score
  FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
  LEFT JOIN debearco_admin.USERS
    ON (USERS.id = ENTRY_SCORE.user_id)
  LEFT JOIN FANTASY_AUTOMATED_USER AS AUTOUSER
    ON (AUTOUSER.id = ENTRY_SCORE.user_id)
  WHERE ENTRY_SCORE.sport = v_sport
  AND   ENTRY_SCORE.season = v_season
  AND   ENTRY_SCORE.game_id = v_game_id
  AND   ENTRY_SCORE.overall_pos = 1
  ORDER BY ENTRY_SCORE.stress
  LIMIT 1;

  # If we have no winner, we have nothing to post about!
  IF v_score IS NULL THEN
    LEAVE proc_ended;
  END IF;

  # Perform the interpolation
  SET v_tweet_body = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(v_tweet_body,
    '{owner}', v_owner),
      '{score}', records_social_format_score(v_score)),
        '{score_s}', IF(v_score = 1.0, '', 's')),
          '{season}', v_season),
            '{domain}', records_config('domain')),
              '{slug}', CONCAT(v_sport, '/', name2slug(v_game_name), '-', v_season));

  # Queue the tweet
  CALL debearco_common.twitter_queue(
    records_config('twitter_app_internal'),
    records_config('twitter_app_api'),
    records_config('twitter_account'),
    records_config('twitter_account_retweet'),
    v_tweet_type,
    v_template_id,
    v_tweet_body,
    records_config('twitter_queue_time'),
    v_tweet_id
  );

  # Determine the final processed period for the Open Graph image attached to the tweet
  SELECT period_id INTO v_period_id
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  ORDER BY period_order DESC
  LIMIT 1;

  # Open Graph as the media
  CALL debearco_common.twitter_queue_media(
    records_config('twitter_app_internal'),
    v_tweet_id,
    CONCAT(records_config('twitter_og_path'), '/', v_sport, '-', v_season, '-', v_game_id, '/', v_period_id, '.', records_config('twitter_og_ext')),
    CONCAT(v_sport, '-', v_season, '-', v_game_id, '-ended'),
    10,
    v_media_id
  );

END $$

DELIMITER ;
