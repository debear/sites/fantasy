##
## 50 in 50
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_50in50_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_50in50_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for nhl:ind:50in50:total'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN goals_value DOUBLE,
    ADD COLUMN goals_gm_value DOUBLE;

  # Calculate the Goals and Goals/Gm values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, goals_value, goals_gm_value)
    SELECT STAT.player_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(STAT.goals) AS goals_value,
           SUM(STAT.goals) / SUM(STAT.gp) AS goals_gm_value
    FROM debearco_sports.SPORTS_NHL_SCHEDULE AS SCHED
    JOIN debearco_sports.SPORTS_NHL_PLAYERS_GAME_SKATERS AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id)
    WHERE SCHED.season = v_season
    AND   SCHED.game_type = 'regular'
    AND   SCHED.game_date < v_start_date
    AND   IFNULL(SCHED.status, 'DNP') NOT IN ('DNP', 'PPD', 'CNC')
    GROUP BY STAT.player_id;

  # Now process the individual stat elements
  # 1 - Goals
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'goals') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = goals_value,
        stat_text = goals_value;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Goals/Gm
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'goals-per-game') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = goals_gm_value,
        stat_text = FORMAT(goals_gm_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN goals_value,
    DROP COLUMN goals_gm_value;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_50in50_total_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_50in50_total_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for nhl:ind:50in50:total'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 40);

  # Calc 1: Goals
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, GAME.visitor_score, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, GAME.home_score, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_SCHEDULE AS GAME
      ON (GAME.season = SCHED.season
      AND GAME.game_type = SCHED.game_type
      AND GAME.game_id = SCHED.game_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'DESC'); # 60/40 Overall-vs-Home/Road, More Goals makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_goals;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_goals;

  # Calc 2: Shot Attempts Against
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.shots + STAT.missed_shots + STAT.blocked_shots, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.shots + STAT.missed_shots + STAT.blocked_shots, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_TEAMS_GAME_SKATERS AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.team_id = SCHED.team_id
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.stat_dir = 'against')
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'DESC'); # 60/40 Overall-vs-Home/Road, More Shot Attempts makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_shots;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_shots;

  # Calc 3: Team Corsi (Pct)
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, NULL AS gp,
           IF(SUM(STAT.corsi_cf + STAT.corsi_ca) > 0, SUM(STAT.corsi_cf) / SUM(STAT.corsi_cf + STAT.corsi_ca), 0.5) AS stat_gm,
           NULL AS home_gp, NULL AS home_stat,
           IF(SUM(SCHED.is_home = 1), SUM(IF(SCHED.is_home = 1, STAT.corsi_cf, 0)) / SUM(IF(SCHED.is_home = 1, STAT.corsi_cf + STAT.corsi_ca, 0)), 0.5) AS home_stat_gm,
           NULL AS visitor_gp, NULL AS visitor_stat,
           IF(SUM(SCHED.is_home = 0), SUM(IF(SCHED.is_home = 0, STAT.corsi_cf, 0)) / SUM(IF(SCHED.is_home = 0, STAT.corsi_cf + STAT.corsi_ca, 0)), 0.5) AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.team_id = SCHED.team_id
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'ASC'); # 60/40 Overall-vs-Home/Road, Lower Corsi makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_corsi;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_corsi;

  # Merge (split 30/35/35 Goals/Shots/Corsi) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    home_opp_rating TINYINT UNSIGNED,
    visitor_opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id,
           (GOALS.home_opp_rating * 0.30) + (SHOTS.home_opp_rating * 0.35) + (CORSI.home_opp_rating * 0.35) AS home_opp_rating,
           (GOALS.visitor_opp_rating * 0.30) + (SHOTS.visitor_opp_rating * 0.35) + (CORSI.visitor_opp_rating * 0.35) AS visitor_opp_rating
    FROM tmp_all_teams AS TEAMS
    JOIN tmp_team_oppcalcs_goals AS GOALS
      ON (GOALS.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_shots AS SHOTS
      ON (SHOTS.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_corsi AS CORSI
      ON (CORSI.team_id = TEAMS.team_id);
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_50in50_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_50in50_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for nhl:ind:50in50:total'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_nhl_ind_sel_recent_sched(v_season, v_start_date, 40);

  # Opportunity 1: Avg TOI (calculated separately for Forwards and Defencement)
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_base;
  CREATE TEMPORARY TABLE tmp_rating_opportunity_base (
    link_id SMALLINT UNSIGNED,
    pos CHAR(3),
    stat SMALLINT UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id, LINK.pos,
           AVG(TIME_TO_SEC(STAT.toi)) AS stat,
           NULL AS stat_nom,
           NULL AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_PLAYERS_GAME_SKATERS AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  # - Forwards
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity LIKE tmp_rating_opportunity_base;
  INSERT INTO tmp_rating_opportunity
    SELECT *
    FROM tmp_rating_opportunity_base
    WHERE pos <> 'D';
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More TOI makes for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_avgtoi_fwd;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_avgtoi_fwd;
  # - Defencemen
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity LIKE tmp_rating_opportunity_base;
  INSERT INTO tmp_rating_opportunity
    SELECT *
    FROM tmp_rating_opportunity_base
    WHERE pos = 'D';
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More TOI makes for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_avgtoi_def;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_avgtoi_def;
  # - Merge back together
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_avgtoi;
  ALTER TABLE tmp_rating_opportunity_avgtoi_fwd RENAME TO tmp_rating_opportunity_avgtoi;
  INSERT INTO tmp_rating_opportunity_avgtoi SELECT * FROM tmp_rating_opportunity_avgtoi_def;
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_avgtoi_def;

  # Opportunity 2: Zone Starts (Off Pct)
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.zs_off_num + STAT.zs_def_num) > 0, SUM(STAT.zs_off_num) / SUM(STAT.zs_off_num + STAT.zs_def_num), NULL) AS stat,
           SUM(STAT.zs_off_num) AS stat_nom,
           SUM(STAT.zs_off_num + STAT.zs_def_num) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Offensive Zone Starts makes for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_zonestarts;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_zonestarts;

  # Metric 1: Shot Attempts / 60min
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           LEAST(99.99999, SUM(STAT.shots + STAT.missed_shots + STAT.blocked_shots) * (3600 / SUM(TIME_TO_SEC(STAT.toi)))) AS stat,
           SUM(STAT.shots + STAT.missed_shots + STAT.blocked_shots) AS stat_nom,
           SUM(TIME_TO_SEC(STAT.toi)) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_PLAYERS_GAME_SKATERS AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', 'calc_per60,apply_least'); # More Shot Attempts makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_shotatt;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_shotatt;

  # Metric 2: Shooting %age
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.shots) > 0, SUM(STAT.goals) / SUM(STAT.shots), NULL) AS stat,
           SUM(STAT.goals) AS stat_nom,
           SUM(STAT.shots) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NHL_PLAYERS_GAME_SKATERS AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', NULL); # Higher Shooting %age makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_shootpct;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_shootpct;

  # Merge (split opportunity 20/20 TOI/ZoneStart, metrics 25/15 ShotAtt/ShotPct, 20 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_opp_toi TINYINT UNSIGNED,
    rating_opp_zonestart TINYINT UNSIGNED,
    rating_metric_shotatt TINYINT UNSIGNED,
    rating_metric_shotpct TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           OPP_TOI.rating AS rating_opp_toi,
           OPP_ZONESTART.rating AS rating_opp_zonestart,
           METRIC_SHOTATT.rating AS rating_metric_shotatt,
           METRIC_SHOTPCT.rating AS rating_metric_shotpct,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(OPP_TOI.rating, 0) * 0.20) + (IFNULL(OPP_ZONESTART.rating, 0) * 0.20)
             + (IFNULL(METRIC_SHOTATT.rating, 0) * 0.25) + (IFNULL(METRIC_SHOTPCT.rating, 0) * 0.15)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.20) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_opportunity_avgtoi AS OPP_TOI
      ON (OPP_TOI.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_zonestarts AS OPP_ZONESTART
      ON (OPP_ZONESTART.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_shotatt AS METRIC_SHOTATT
      ON (METRIC_SHOTATT.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_shootpct AS METRIC_SHOTPCT
      ON (METRIC_SHOTPCT.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_50in50_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_50in50_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for nhl:ind:50in50:total'
BEGIN

  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_NHL_PLAYERS_GAME_SKATERS AS STAT
    ON (STAT.season = SEL.season
    AND STAT.player_id = SEL.link_id
    AND STAT.game_type = SEL.game_type
    AND STAT.game_id = SEL.game_id
    AND STAT.gp = 1)
  SET SEL.stat = STAT.goals,
      SEL.status = IF(STAT.goals = 0, 'negative', 'positive'),
      SEL.summary = CONCAT(STAT.goals, ' G, ', STAT.assists, ' A');

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_50in50_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_50in50_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for nhl:ind:50in50:total'
BEGIN

  # Determine when each scorer scored their first goal
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    stress TINYINT UNSIGNED,
    first_by_period TINYINT UNSIGNED,
    first_by_time SMALLINT UNSIGNED,
    first_by_period_time SMALLINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.game_id,
           NULL AS stress,
           MIN(EVENT.period) AS first_by_period,
           MIN((IF(EVENT.period < 4, 1200, 300) - TIME_TO_SEC(EVENT.event_time)) + ((EVENT.period - 1) * 1200)) AS first_by_time,
           NULL AS first_by_period_time
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_NHL_GAME_EVENT_GOAL AS EVENT_GOAL
      ON (EVENT_GOAL.season = SEL.season
      AND EVENT_GOAL.game_type = SEL.game_type
      AND EVENT_GOAL.game_id = SEL.game_id
      AND EVENT_GOAL.by_team_id = SEL.team_id
      AND EVENT_GOAL.scorer = SEL.jersey)
    JOIN debearco_sports.SPORTS_NHL_GAME_EVENT AS EVENT
      ON (EVENT.season = EVENT_GOAL.season
      AND EVENT.game_type = EVENT_GOAL.game_type
      AND EVENT.game_id = EVENT_GOAL.game_id
      AND EVENT.event_id = EVENT_GOAL.event_id)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id;

  UPDATE tmp_link_stress
  SET first_by_period_time = first_by_time - (1200 * (first_by_period - 1));

  # Then assign a stress score to each
  UPDATE tmp_link_stress
  SET stress = CASE first_by_period
    WHEN 1 THEN ((first_by_period_time / 1200) * 64)
    WHEN 2 THEN ((first_by_period_time / 1200) * 80) + 64
    WHEN 3 THEN ((first_by_period_time / 1200) * 111) + 144
    ELSE 255
  END;

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
