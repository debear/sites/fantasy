#
# Creation process for social media updates
#
DROP PROCEDURE IF EXISTS `records_social_media`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_social_media`(
  v_sport VARCHAR(6),
  v_date DATE
)
    COMMENT 'Record Breaker social media post creation'
BEGIN

  ##
  ## This is a generic wrapper to each of the platforms we are posting to
  ##

  # Twitter
  CALL records_twitter(v_sport, v_date);

END $$

DELIMITER ;
