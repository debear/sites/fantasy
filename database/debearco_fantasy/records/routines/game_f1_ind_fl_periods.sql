##
## X Fastest Lap Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_fl_periods_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_fl_periods_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for f1:ind:fl:periods'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN fl_value DOUBLE,
    ADD COLUMN finish_value DOUBLE;

  # Calculate the Avg Start and Finish values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, fl_value, finish_value)
    SELECT RACE_DRIVER.driver_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           COUNT(DISTINCT RACE_FL.round) AS fl_value,
           SUM(RACE_RESULT.pos) / COUNT(RACE_RESULT.season) AS finish_value
    FROM debearco_sports.SPORTS_FIA_RACES AS RACE
    JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_FASTEST_LAP AS RACE_FL
      ON (RACE_FL.season = RACE_DRIVER.season
      AND RACE_FL.series = RACE_DRIVER.series
      AND RACE_FL.round = RACE_DRIVER.round
      AND RACE_FL.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_FL.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS RACE_RESULT
      ON (RACE_RESULT.season = RACE_DRIVER.season
      AND RACE_RESULT.series = RACE_DRIVER.series
      AND RACE_RESULT.round = RACE_DRIVER.round
      AND RACE_RESULT.car_no = RACE_DRIVER.car_no
      AND IFNULL(RACE_RESULT.result, '') REGEXP '^[0-9]+$')
    WHERE RACE.season = v_season
    AND   RACE.series = v_sport
    AND   DATE(RACE.race_time) < v_start_date
    AND   RACE.race_laps_completed IS NOT NULL
    GROUP BY RACE_DRIVER.driver_id;

  # Now process the individual stat elements
  # 1 - Fastest Laps
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'fastest-laps') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = fl_value,
        stat_text = fl_value;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Avg Finish
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'avg-finish') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = finish_value,
        stat_text = FORMAT(finish_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'ASC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN fl_value,
    DROP COLUMN finish_value;

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_fl_periods_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_fl_periods_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for f1:ind:fl:periods'
BEGIN

  # Run our generalised helper setup
  CALL records_f1_selrating_setup(v_season, v_start_date);

  # Team calcs over these races
  CALL records_f1_ind_fl_periods_selrating_team();

  # Then driver calcs, compared to their teammate
  CALL records_f1_ind_fl_periods_selrating_driver();

  # Process via the generalised F1 helper
  CALL records_f1_selrating_proc(v_sport, v_season, v_game_id, v_period_id, 1.0, 0.6, 'DESC');

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_fl_periods_selrating_team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_fl_periods_selrating_team`()
    COMMENT 'Record Breaker selrating team calcs for f1:ind:fl:periods'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_team_results;
  CREATE TEMPORARY TABLE tmp_team_results (
    team_id TINYINT UNSIGNED,
    fl_ratio DECIMAL(5, 4) UNSIGNED,
    laps_cmpl_ratio DECIMAL(5, 4) UNSIGNED,
    podium_ratio DECIMAL(5, 4) UNSIGNED,
    nonpodium_ratio DECIMAL(5, 4) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MyISAM
    SELECT RACE_DRIVER.team_id,
           SUM((RACE_FL.lap_num IS NOT NULL) / RACE.race_multiplier) / COUNT(DISTINCT CONCAT(RACE.season, '-', RACE.round)) AS fl_ratio,
           AVG((RACE_RESULT.laps / IFNULL(RACE.race2_laps_completed, RACE.race_laps_completed)) / RACE.race_multiplier) AS laps_cmpl_ratio,
           AVG((RACE_RESULT.pts IS NOT NULL AND RACE_RESULT.pos < 4) / RACE.race_multiplier) AS podium_ratio,
           AVG((RACE_RESULT.pts IS NOT NULL AND RACE_RESULT.pos > 3) / RACE.race_multiplier) AS nonpodium_ratio
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_FASTEST_LAP AS RACE_FL
      ON (RACE_FL.season = RACE_DRIVER.season
      AND RACE_FL.series = RACE_DRIVER.series
      AND RACE_FL.round = RACE_DRIVER.round
      AND RACE_FL.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_FL.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS RACE_RESULT
      ON (RACE_RESULT.season = RACE_DRIVER.season
      AND RACE_RESULT.series = RACE_DRIVER.series
      AND RACE_RESULT.round = RACE_DRIVER.round
      AND RACE_RESULT.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_RESULT.car_no = RACE_DRIVER.car_no)
    GROUP BY RACE_DRIVER.team_id;

  # Merge the components into a single team rating
  ALTER TABLE tmp_team_results
    ADD COLUMN avg_rtg DECIMAL(6, 4) UNSIGNED;
  UPDATE tmp_team_results SET avg_rtg = (fl_ratio * 6) + (laps_cmpl_ratio * 4) + (podium_ratio * 2) + (nonpodium_ratio * 3);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_fl_periods_selrating_driver`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_fl_periods_selrating_driver`()
    COMMENT 'Record Breaker selrating team calcs for f1:ind:fl:periods'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_driver_results;
  CREATE TEMPORARY TABLE tmp_driver_results (
    driver_id SMALLINT UNSIGNED,
    fl_ratio DECIMAL(5, 4) UNSIGNED,
    laps_cmpl_ratio DECIMAL(5, 4) UNSIGNED,
    podium_ratio DECIMAL(5, 4) UNSIGNED,
    nonpodium_ratio DECIMAL(5, 4) UNSIGNED,
    PRIMARY KEY (driver_id)
  ) ENGINE = MyISAM
    SELECT RACE_DRIVER.driver_id,
           AVG((RACE_FL.lap_num IS NOT NULL) / RACE.race_multiplier) AS fl_ratio,
           AVG((RACE_RESULT.laps / IFNULL(RACE.race2_laps_completed, RACE.race_laps_completed)) / RACE.race_multiplier) AS laps_cmpl_ratio,
           AVG((RACE_RESULT.pts IS NOT NULL AND RACE_RESULT.pos < 4) / RACE.race_multiplier) AS podium_ratio,
           AVG((RACE_RESULT.pts IS NOT NULL AND RACE_RESULT.pos > 3) / RACE.race_multiplier) AS nonpodium_ratio
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_FASTEST_LAP AS RACE_FL
      ON (RACE_FL.season = RACE_DRIVER.season
      AND RACE_FL.series = RACE_DRIVER.series
      AND RACE_FL.round = RACE_DRIVER.round
      AND RACE_FL.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_FL.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS RACE_RESULT
      ON (RACE_RESULT.season = RACE_DRIVER.season
      AND RACE_RESULT.series = RACE_DRIVER.series
      AND RACE_RESULT.round = RACE_DRIVER.round
      AND RACE_RESULT.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_RESULT.car_no = RACE_DRIVER.car_no)
    GROUP BY RACE_DRIVER.driver_id;

  # Merge the components into a single driver rating
  ALTER TABLE tmp_driver_results
    ADD COLUMN rel_rtg DECIMAL(6, 4) UNSIGNED;
  UPDATE tmp_driver_results SET rel_rtg = (fl_ratio * 6) + (laps_cmpl_ratio * 4) + (podium_ratio * 2) + (nonpodium_ratio * 3);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_f1_ind_fl_periods_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_fl_periods_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for f1:ind:fl:periods'
BEGIN

  # Get the Grand Prix fastest lap info per-driver
  DROP TEMPORARY TABLE IF EXISTS tmp_link_res;
  CREATE TEMPORARY TABLE tmp_link_res (
    car_no TINYINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    summary VARCHAR(50),
    PRIMARY KEY (car_no)
  ) ENGINE = MyISAM
    SELECT SEL.car_no,
           SEL.car_no = STAT.car_no AS stat,
           CASE
             WHEN RACE_RESULT.result IS NULL THEN 'na'
             WHEN SEL.car_no = STAT.car_no THEN 'positive'
             ELSE 'negative'
           END AS status,
           IF(RACE_RESULT.result IS NULL, 'DNS', STAT_DRIVER.driver_code) AS summary
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_FIA_RACES AS RACE
      ON (RACE.season = SEL.season
      AND RACE.series = v_sport
      AND RACE.round = SEL.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS RACE_RESULT
      ON (RACE_RESULT.season = RACE.season
      AND RACE_RESULT.series = RACE.series
      AND RACE_RESULT.round = RACE.round
      AND RACE_RESULT.race = SEL.race
      AND RACE_RESULT.car_no = SEL.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_FASTEST_LAP AS STAT
      ON (STAT.season = RACE.season
      AND STAT.series = RACE.series
      AND STAT.round = RACE.round
      AND STAT.race = SEL.race)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS STAT_DRIVER
      ON (STAT_DRIVER.season = STAT.season
      AND STAT_DRIVER.series = STAT.series
      AND STAT_DRIVER.round = STAT.round
      AND STAT_DRIVER.car_no = STAT.car_no)
    GROUP BY SEL.car_no;

  # Now perform the update
  UPDATE tmp_link_events AS SEL
  JOIN tmp_link_res AS STAT
    ON (STAT.car_no = SEL.car_no)
  SET SEL.stat = STAT.stat,
      SEL.status = STAT.status,
      SEL.summary = STAT.summary;

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_f1_ind_fl_periods_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_fl_periods_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for f1:ind:fl:periods'
BEGIN

  # Establish each driver's fastest lap info (though this should only apply to one!)
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    round TINYINT UNSIGNED,
    race_laps TINYINT UNSIGNED,
    fl_lap_no TINYINT UNSIGNED,
    fl_lap_ratio DECIMAL(6,5) UNSIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, round)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.round,
           IFNULL(RACE.race2_laps_completed, RACE.race_laps_completed) AS race_laps,
           RACE_FL.lap_num AS fl_lap_no,
           RACE_FL.lap_num / IFNULL(RACE.race2_laps_completed, RACE.race_laps_completed) AS fl_lap_ratio,
           NULL AS stress
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_FIA_RACES AS RACE
      ON (RACE.season = SEL.season
      AND RACE.series = v_sport
      AND RACE.round = SEL.round)
    JOIN debearco_sports.SPORTS_FIA_RACE_FASTEST_LAP AS RACE_FL
      ON (RACE_FL.season = RACE.season
      AND RACE_FL.series = RACE.series
      AND RACE_FL.round = RACE.round
      AND RACE_FL.race = SEL.race
      AND RACE_FL.car_no = SEL.car_no)
    GROUP BY SEL.link_id, SEL.season, SEL.round;

  # Then assign a stress score based on how long in to the race the fastest lap was recorded
  UPDATE tmp_link_stress
  SET stress = IF(fl_lap_ratio < 0.75,
    fl_lap_ratio * 155,
    ((fl_lap_ratio - 0.75) * 100) + 155
  );

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.round = SEL.round)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
