#
# Prepare active sporting scoring updates for the game
#
DROP PROCEDURE IF EXISTS `records_scoring_active_prep`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_active_prep`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker active sporting leader process preparation'
BEGIN

  DECLARE v_period_order TINYINT UNSIGNED;
  SELECT period_order INTO v_period_order
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  AND   period_id = v_period_id;

  # Emulate the temporary tmp_link_events from the selection scoring table
  DROP TEMPORARY TABLE IF EXISTS tmp_active_events;
  CREATE TEMPORARY TABLE tmp_active_events (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    period_order TINYINT UNSIGNED,
    event_order_raw SMALLINT UNSIGNED,
    event_order TINYINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    PRIMARY KEY (link_id, period_id, event_order_raw)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.period_id, PERIOD.period_order, SEL.event_order AS event_order_raw, 0 AS event_order, SEL.stat, SEL.status
    FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD
    JOIN FANTASY_RECORDS_SELECTIONS_SCORING AS SEL
      ON (SEL.sport = PERIOD.sport
      AND SEL.season = PERIOD.season
      AND SEL.game_id = v_game_id
      AND SEL.period_id = PERIOD.period_id)
    WHERE PERIOD.sport = v_sport
    AND   PERIOD.season = v_season
    AND   PERIOD.period_order <= v_period_order;

  # Re-base the event_order to combine period_order and event_order
  CALL _duplicate_tmp_table('tmp_active_events', 'tmp_active_events_cpA');
  INSERT INTO tmp_active_events (link_id, period_id, event_order_raw, event_order)
    SELECT SQL_BIG_RESULT link_id, period_id, event_order_raw, ROW_NUMBER() OVER w AS event_order
    FROM tmp_active_events_cpA
    WINDOW w AS (PARTITION BY link_id ORDER BY link_id, period_order, event_order_raw)
  ON DUPLICATE KEY UPDATE event_order = VALUES(event_order);

  ALTER TABLE tmp_active_events
    DROP PRIMARY KEY,
    DROP COLUMN event_order_raw,
    ADD PRIMARY KEY (link_id, event_order);

END $$

DELIMITER ;

#
# Process active scoring leaders for games with a 'total' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_active_total`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_active_total`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker active scoring leaders for total score type'
BEGIN

  # As some games limit the number of selections to use, apply if that applies here
  DECLARE v_active_limit TINYINT UNSIGNED;
  SELECT active_limit INTO v_active_limit
  FROM FANTASY_RECORDS_GAMES
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id;

  IF v_active_limit IS NOT NULL THEN
    ALTER TABLE tmp_active_events
      ADD COLUMN sel_recency TINYINT UNSIGNED DEFAULT 0 AFTER event_order;

    CALL _duplicate_tmp_table('tmp_active_events', 'tmp_active_events_cpA');
    INSERT INTO tmp_active_events (link_id, period_id, event_order, sel_recency)
      SELECT SQL_BIG_RESULT link_id, period_id, event_order, ROW_NUMBER() OVER w AS sel_recency
      FROM tmp_active_events_cpA
      WINDOW w AS (PARTITION BY link_id ORDER BY link_id, event_order DESC)
    ON DUPLICATE KEY UPDATE sel_recency = VALUES(sel_recency);

    DELETE FROM tmp_active_events WHERE sel_recency > v_active_limit;
  END IF;

  # Perform the update
  DROP TEMPORARY TABLE IF EXISTS tmp_active_scores;
  CREATE TEMPORARY TABLE tmp_active_scores (
    link_id SMALLINT UNSIGNED,
    result DECIMAL(5,1) SIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT link_id, SUM(stat) AS result
    FROM tmp_active_events
    GROUP BY link_id;

END $$

DELIMITER ;

#
# Process active scoring leaders for games with a 'streak' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_active_streak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_active_streak`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker active scoring leaders for streak score type'
BEGIN

  # To determine the current streak, determine when it started based on the last time it ended
  DROP TEMPORARY TABLE IF EXISTS tmp_active_streak_start;
  CREATE TEMPORARY TABLE tmp_active_streak_start (
    link_id SMALLINT UNSIGNED,
    last_negative TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT link_id, MAX(event_order) AS last_negative
    FROM tmp_active_events
    WHERE status = 'negative'
    GROUP BY link_id;

  # Prune the earlier selections
  DELETE tmp_active_events.*
  FROM tmp_active_streak_start
  JOIN tmp_active_events
    ON (tmp_active_events.link_id = tmp_active_streak_start.link_id
    AND tmp_active_events.event_order <= tmp_active_streak_start.last_negative);

  # Perform the update
  DROP TEMPORARY TABLE IF EXISTS tmp_active_scores;
  CREATE TEMPORARY TABLE tmp_active_scores (
    link_id SMALLINT UNSIGNED,
    result DECIMAL(5,1) SIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT link_id, SUM(stat) AS result
    FROM tmp_active_events
    GROUP BY link_id;

END $$

DELIMITER ;

#
# Process active scoring leaders for games with a 'periods' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_active_periods`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_active_periods`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker active scoring leaders for periods score type'
BEGIN

  # Processing-wise, this is an equivalent to a 'total' game
  CALL records_scoring_active_total(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);

END $$

DELIMITER ;

#
# Store the active scoring leaders after a period
#
DROP PROCEDURE IF EXISTS `records_scoring_active_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_active_store`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker active scoring leader storing'
BEGIN

  DECLARE v_leader SMALLINT UNSIGNED;
  DECLARE v_result DECIMAL(5,1) SIGNED;

  # Get the sporting leader and value
  SELECT link_id, result
    INTO v_leader, v_result
  FROM tmp_active_scores
  ORDER BY result DESC
  LIMIT 1;

  # Store in our main game table
  UPDATE FANTASY_RECORDS_GAMES
  SET active_id = v_leader,
      active_score = v_result
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id;

  # But also in the per-period summary
  UPDATE FANTASY_RECORDS_GAMES_PERIODS_SUMMARY
  SET active_id = v_leader,
      active_score = v_result
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  AND   period_id = v_period_id;

END $$

DELIMITER ;
