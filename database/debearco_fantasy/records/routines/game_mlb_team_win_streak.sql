##
## X Game Win Streak
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_mlb_team_win_streak_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_team_win_streak_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for mlb:team:win:streak'
BEGIN

  DECLARE v_standings_date DATE;
  DECLARE v_stat_id TINYINT UNSIGNED;

  SELECT MAX(the_date) INTO v_standings_date
  FROM debearco_sports.SPORTS_MLB_STANDINGS
  WHERE season = v_season
  AND   the_date < v_start_date;

  # Streamline our standings processing
  DROP TEMPORARY TABLE IF EXISTS tmp_standings;
  CREATE TEMPORARY TABLE tmp_standings (
    link_id SMALLINT UNSIGNED,
    overall_pct DOUBLE,
    overall_text VARCHAR(30),
    recent_pct DOUBLE,
    recent_text VARCHAR(30),
    home_pct DOUBLE,
    home_text VARCHAR(30),
    visitor_pct DOUBLE,
    visitor_text VARCHAR(30),
    PRIMARY KEY (link_id)
  ) SELECT SEL_MAP.link_id,
           IF(STAND.wins + STAND.loss + STAND.ties > 0, (STAND.wins + (STAND.ties / 2)) / (STAND.wins + STAND.loss + STAND.ties), NULL) AS overall_pct,
           CONCAT(STAND.wins, '&ndash;', STAND.loss, IF(STAND.ties > 0, CONCAT('&ndash;', STAND.ties), '')) AS overall_text,
           IF(STAND.recent_wins + STAND.recent_loss + STAND.recent_ties > 0, (STAND.recent_wins + (STAND.recent_ties / 2)) / (STAND.recent_wins + STAND.recent_loss + STAND.recent_ties), NULL) AS recent_pct,
           CONCAT(STAND.recent_wins, '&ndash;', STAND.recent_loss, IF(STAND.recent_ties > 0, CONCAT('&ndash;', STAND.recent_ties), '')) AS recent_text,
           IF(STAND.home_wins + STAND.home_loss + STAND.home_ties > 0, (STAND.home_wins + (STAND.home_ties / 2)) / (STAND.home_wins + STAND.home_loss + STAND.home_ties), NULL) AS home_pct,
           CONCAT(STAND.home_wins, '&ndash;', STAND.home_loss, IF(STAND.home_ties > 0, CONCAT('&ndash;', STAND.home_ties), '')) AS home_text,
           IF(STAND.visitor_wins + STAND.visitor_loss + STAND.visitor_ties > 0, (STAND.visitor_wins + (STAND.visitor_ties / 2)) / (STAND.visitor_wins + STAND.visitor_loss + STAND.visitor_ties), NULL) AS visitor_pct,
           CONCAT(STAND.visitor_wins, '&ndash;', STAND.visitor_loss, IF(STAND.visitor_ties > 0, CONCAT('&ndash;', STAND.visitor_ties), '')) AS visitor_text
    FROM FANTASY_COMMON_SELECTIONS_MAPPED AS SEL_MAP
    LEFT JOIN debearco_sports.SPORTS_MLB_STANDINGS AS STAND
      ON (STAND.season = v_season
      AND STAND.the_date = v_standings_date
      AND STAND.team_id = SEL_MAP.raw_id)
    WHERE SEL_MAP.sport = v_sport
    AND   SEL_MAP.season = v_season;

  # Streamline the game odds (default to 2.0 / +100 / 50% if unset)
  CALL records_mlb_team_events(v_sport, v_season, v_game_id, v_period_id, NULL, NULL);
  DROP TEMPORARY TABLE IF EXISTS tmp_link_odds;
  CREATE TEMPORARY TABLE tmp_link_odds (
    link_id SMALLINT UNSIGNED,
    money_line DECIMAL(4,2) UNSIGNED,
    money_line_disp VARCHAR(5),
    win_prob DECIMAL(6,5) UNSIGNED,
    PRIMARY KEY (link_id)
  ) SELECT LINK.link_id,
           MAX(IF(LINK.raw_id = SCHED.home, ODDS.home_line, ODDS.visitor_line)) AS money_line,
           'TBD' AS money_line_disp,
           0.5 AS win_prob
    FROM tmp_link_events AS LINK
    JOIN debearco_sports.SPORTS_MLB_SCHEDULE AS SCHED
      ON (SCHED.season = LINK.season
      AND SCHED.game_type = LINK.game_type
      AND SCHED.game_id = LINK.game_id)
    LEFT JOIN debearco_sports.SPORTS_COMMON_MAJORS_ODDS AS ODDS
      ON (ODDS.sport = v_sport
      AND ODDS.season = SCHED.season
      AND ODDS.game_type = SCHED.game_type
      AND ODDS.game_id = SCHED.game_id)
    GROUP BY LINK.link_id;
  UPDATE tmp_link_odds
  SET win_prob = 1 / money_line,
      money_line_disp = IF(money_line > 2.0,
        CONCAT('+', ROUND((money_line - 1) * 100)), # +xxx
        ROUND(-100 / (money_line - 1)))             # -xxx
  WHERE money_line IS NOT NULL;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN overall_value DOUBLE,
    ADD COLUMN overall_text VARCHAR(30),
    ADD COLUMN recent_value DOUBLE,
    ADD COLUMN recent_text VARCHAR(30),
    ADD COLUMN homeroad_value DOUBLE,
    ADD COLUMN homeroad_text VARCHAR(30),
    ADD COLUMN odds_value DOUBLE,
    ADD COLUMN odds_text VARCHAR(30);

  # Calculate the various team record values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, overall_value, overall_text, recent_value, recent_text, homeroad_value, homeroad_text, odds_value, odds_text)
    SELECT tmp_links.link_id, NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           IFNULL(tmp_standings.overall_pct, -0.1) AS overall_value,
           IFNULL(tmp_standings.overall_text, '0&ndash;0') AS overall_text,
           IFNULL(tmp_standings.recent_pct, -0.1) AS recent_value,
           IFNULL(tmp_standings.recent_text, '0&ndash;0') AS recent_text,
           IFNULL(IF(tmp_links.is_home, tmp_standings.home_pct, tmp_standings.visitor_pct), -0.1) AS homeroad_value,
           IFNULL(IF(tmp_links.is_home, tmp_standings.home_text, tmp_standings.visitor_text), '0&ndash;0') AS homeroad_text,
           IFNULL(tmp_link_odds.win_prob, 0.5) AS odds_value,
           IFNULL(tmp_link_odds.money_line_disp, 'TBD') AS odds_text
    FROM tmp_links
    JOIN tmp_standings
      ON (tmp_standings.link_id = tmp_links.link_id)
    JOIN tmp_link_odds
      ON (tmp_link_odds.link_id = tmp_links.link_id);

  # Now process the individual stat elements
  # 1 - Record
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'record') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = overall_value,
        stat_text = overall_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Recent Games
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'recent') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = recent_value,
        stat_text = recent_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 3 - Home/Road Record
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'home-road') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = homeroad_value,
        stat_text = homeroad_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 4 - Game Odds
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'odds') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = odds_value,
        stat_text = odds_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN overall_value,
    DROP COLUMN overall_text,
    DROP COLUMN recent_value,
    DROP COLUMN recent_text,
    DROP COLUMN homeroad_value,
    DROP COLUMN homeroad_text,
    DROP COLUMN odds_value,
    DROP COLUMN odds_text;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_mlb_team_win_streak_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_team_win_streak_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for mlb:team:win:streak'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 81);

  # Calc 1: Pythogrean record
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_rf SMALLINT UNSIGNED,
    stat_ra SMALLINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_rf SMALLINT UNSIGNED,
    home_stat_ra SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_rf SMALLINT UNSIGNED,
    visitor_stat_ra SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, NULL AS gp, NULL AS stat_rf, NULL AS stat_ra, NULL AS stat_gm,
           NULL AS home_gp, NULL AS home_stat,
           SUM(IF(SCHED.is_home = 1, GAME.home_score, 0)) AS home_stat_rf,
           SUM(IF(SCHED.is_home = 1, GAME.visitor_score, 0)) AS home_stat_ra,
           NULL AS home_stat_gm,
           NULL AS visitor_gp, NULL AS visitor_stat,
           SUM(IF(SCHED.is_home = 0, GAME.visitor_score, 0)) AS visitor_stat_rf,
           SUM(IF(SCHED.is_home = 0, GAME.home_score, 0)) AS visitor_stat_ra,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_SCHEDULE AS GAME
      ON (GAME.season = SCHED.season
      AND GAME.game_type = SCHED.game_type
      AND GAME.game_id = SCHED.game_id)
    GROUP BY TEAMS.team_id;

  # Merge the scoring values
  UPDATE tmp_team_oppcalcs
  SET stat_rf = home_stat_rf + visitor_stat_rf,
      stat_ra = home_stat_ra + visitor_stat_ra;

  # Determine our pythagorean projection
  UPDATE tmp_team_oppcalcs
  SET stat_gm = POW(stat_rf, 1.83) / (POW(stat_rf, 1.83) + POW(stat_ra, 1.83)),
      home_stat_gm = POW(home_stat_rf, 1.83) / (POW(home_stat_rf, 1.83) + POW(home_stat_ra, 1.83)),
      visitor_stat_gm = POW(visitor_stat_rf, 1.83) / (POW(visitor_stat_rf, 1.83) + POW(visitor_stat_ra, 1.83));

  # Streamline our table
  ALTER TABLE tmp_team_oppcalcs
    DROP COLUMN stat_rf,
    DROP COLUMN stat_ra,
    DROP COLUMN home_stat_rf,
    DROP COLUMN home_stat_ra,
    DROP COLUMN visitor_stat_rf,
    DROP COLUMN visitor_stat_ra;

  # Process
  CALL records_opprating_proc(0.4, 'ASC'); # 60/40 Overall-vs-Home/Road, Lower Record makes for a better opponent
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_mlb_team_win_streak_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_team_win_streak_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for mlb:team:win:streak'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_mlb_team_events(v_sport, v_season, v_game_id, v_period_id, NULL, NULL);
  CALL records_mlb_team_sel_recent_sched(v_season, v_start_date, 81);

  # Metric 1: Own pythagorean
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           POW(SUM(IF(RECENT.is_home, SCHED.home_score, SCHED.visitor_score)), 1.83) / (POW(SUM(IF(RECENT.is_home, SCHED.home_score, SCHED.visitor_score)), 1.83) + POW(SUM(IF(RECENT.is_home, SCHED.visitor_score, SCHED.home_score)), 1.83)) AS stat,
           SUM(IF(RECENT.is_home, SCHED.home_score, SCHED.visitor_score)) AS stat_nom,
           SUM(IF(RECENT.is_home, SCHED.visitor_score, SCHED.home_score)) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_SCHEDULE AS SCHED
      ON (SCHED.season = RECENT.season
      AND SCHED.game_type = RECENT.game_type
      AND SCHED.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', 'calc_mlbpythag'); # Better pythagorean makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_pythag;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_pythag;

  # Metric 2: Game odds
  DROP TEMPORARY TABLE IF EXISTS tmp_link_odds;
  CREATE TEMPORARY TABLE tmp_link_odds (
    link_id SMALLINT UNSIGNED,
    money_line DECIMAL(5,3) UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(IF(LINK.raw_id = SCHED.home, STAT.home_line, STAT.visitor_line)) AS money_line
    FROM tmp_link_events AS LINK
    JOIN debearco_sports.SPORTS_MLB_SCHEDULE AS SCHED
      ON (SCHED.season = LINK.season
      AND SCHED.game_type = LINK.game_type
      AND SCHED.game_id = LINK.game_id)
    LEFT JOIN debearco_sports.SPORTS_COMMON_MAJORS_ODDS AS STAT
      ON (STAT.sport = v_sport
      AND STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id)
    GROUP BY LINK.link_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_odds;
  CREATE TEMPORARY TABLE tmp_rating_metrics_odds (
    link_id SMALLINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           ROUND(100 / IF(STAT.money_line IS NOT NULL AND STAT.money_line <> 0, STAT.money_line, 2.0)) AS rating
    FROM tmp_links AS LINK
    LEFT JOIN tmp_link_odds AS STAT
      ON (STAT.link_id = LINK.link_id);

  # Merge (split metrics 30 own pythagorean, 40 odds, 30 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_metric_pythag TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           METRIC_PYTHAG.rating AS rating_metric_pythag,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(METRIC_PYTHAG.rating, 0) * 0.30) + (IFNULL(METRIC_ODDS.rating, 0.5) * 0.40)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.30) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_metrics_pythag AS METRIC_PYTHAG
      ON (METRIC_PYTHAG.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_odds AS METRIC_ODDS
      ON (METRIC_ODDS.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_mlb_team_win_streak_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_team_win_streak_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for mlb:team:win:streak'
BEGIN

  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_MLB_SCHEDULE AS SCHED
    ON (SCHED.season = SEL.season
    AND SCHED.game_type = SEL.game_type
    AND SCHED.game_id = SEL.game_id)
  SET SEL.stat = IF(SCHED.status = 'F', SEL.team_rf > SEL.team_ra, 0),
      SEL.status = IF(SCHED.status <> 'F', 'na',
        IF(SEL.team_rf > SEL.team_ra, 'positive', 'negative')),
      SEL.summary = IF(SCHED.status <> 'F', SCHED.status,
        IF(SEL.team_rf > SEL.team_ra, CONCAT('W ', SEL.team_rf, '&ndash;', SEL.team_ra),
          IF (SEL.team_rf < SEL.team_ra, CONCAT('L ', SEL.team_ra, '&ndash;', SEL.team_rf),
            CONCAT('T ', SEL.team_rf, '&ndash;', SEL.team_ra))));

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_mlb_team_win_streak_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_team_win_streak_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for mlb:team:win:streak'
BEGIN

  # Determine when each winning team took the lead, their biggest lead after that point and the final difference
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    raw_id CHAR(5) DEFAULT NULL,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    is_home TINYINT UNSIGNED,
    last_play_not_winning TINYINT UNSIGNED,
    took_lead TINYINT UNSIGNED,
    widest_margin TINYINT UNSIGNED,
    final_margin TINYINT UNSIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.raw_id, SEL.season, SEL.game_type, SEL.game_id, SCHED.home = SEL.raw_id AS is_home,
           MAX(IF((SCHED.home = SEL.raw_id AND GAME_PLAY.home_score <= GAME_PLAY.visitor_score)
                   OR (SCHED.visitor = SEL.raw_id AND GAME_PLAY.home_score >= GAME_PLAY.visitor_score),
                 GAME_PLAY.play_id, 0)) last_play_not_winning,
           NULL AS took_lead, NULL AS widest_margin,
           CAST(SEL.team_rf AS SIGNED) - CAST(SEL.team_ra AS SIGNED) AS final_margin,
           NULL AS stress
    FROM tmp_link_events AS SEL
    LEFT JOIN debearco_sports.SPORTS_MLB_SCHEDULE AS SCHED
      ON (SCHED.season = SEL.season
      AND SCHED.game_type = SEL.game_type
      AND SCHED.game_id = SEL.game_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_GAME_PLAYS AS GAME_PLAY
      ON (GAME_PLAY.season = SCHED.season
      AND GAME_PLAY.game_type = SCHED.game_type
      AND GAME_PLAY.game_id = SCHED.game_id)
    WHERE SEL.team_rf > SEL.team_ra
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id;

  CALL _duplicate_tmp_table('tmp_link_stress', 'tmp_link_stress_cp');
  INSERT INTO tmp_link_stress (link_id, season, game_type, game_id, took_lead, widest_margin)
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.game_id,
           MIN(GAME_PLAY.inning) AS took_lead,
           MAX(CAST(IF(SEL.is_home, GAME_PLAY.home_score, GAME_PLAY.visitor_score) AS SIGNED) - CAST(IF(SEL.is_home, GAME_PLAY.visitor_score, GAME_PLAY.home_score) AS SIGNED)) AS widest_margin
    FROM tmp_link_stress_cp AS SEL
    LEFT JOIN debearco_sports.SPORTS_MLB_GAME_PLAYS AS GAME_PLAY
      ON (GAME_PLAY.season = SEL.season
      AND GAME_PLAY.game_type = SEL.game_type
      AND GAME_PLAY.game_id = SEL.game_id
      AND GAME_PLAY.play_id > SEL.last_play_not_winning)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id
  ON DUPLICATE KEY UPDATE took_lead = VALUES(took_lead),
                          widest_margin = VALUES(widest_margin);

  # Then assign a stress score to each based on when they took the lead (for good), how much they won by and how much that changed
  UPDATE tmp_link_stress
  SET stress = (((LEAST(took_lead, 9) - 1) / 8) * 185)
             + (((5 - LEAST(final_margin, 5)) / 4) * 40)
             + (IF(widest_margin > 2, (widest_margin - final_margin) / widest_margin, 0) * 30);

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
