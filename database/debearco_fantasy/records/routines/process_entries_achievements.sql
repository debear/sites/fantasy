#
# Entry opponent calculations
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_opponents`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_opponents`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_sel_type ENUM('ind', 'team'),
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry opponent calculations'
BEGIN

  DECLARE v_period_order TINYINT UNSIGNED;

  # Determine the period_order for the processing period
  SELECT period_order INTO v_period_order
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  AND   period_id = v_period_id;

  # Reduce tmp_entry_sel to link_id/period_id combos for appropriate selections
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_sel;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_sel (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    PRIMARY KEY (sport, season, game_id, period_id, link_id)
  ) ENGINE=MEMORY
    SELECT DISTINCT sport, season, game_id, period_id, link_id
    FROM FANTASY_RECORDS_ENTRIES_BYPERIOD
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   link_id IS NOT NULL;

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    PRIMARY KEY (link_id, period_id)
  ) ENGINE = MyISAM
    SELECT DISTINCT SEL.link_id, SEL.period_id
    FROM FANTASY_COMMON_PERIODS_DATES AS PERIODS
    JOIN tmp_entry_sel_selperiod_sel AS ENTRY_SEL
      ON (ENTRY_SEL.sport = PERIODS.sport
      AND ENTRY_SEL.season = PERIODS.season
      AND ENTRY_SEL.game_id = v_game_id
      AND ENTRY_SEL.period_id = PERIODS.period_id)
    JOIN FANTASY_RECORDS_SELECTIONS AS SEL
      ON (SEL.sport = ENTRY_SEL.sport
      AND SEL.season = ENTRY_SEL.season
      AND SEL.game_id = ENTRY_SEL.game_id
      AND SEL.link_id = ENTRY_SEL.link_id
      AND SEL.period_id = ENTRY_SEL.period_id
      AND SEL.status IN ('positive', 'neutral'))
    WHERE PERIODS.sport = v_sport
    AND   PERIODS.season = v_season
    AND   PERIODS.period_order <= v_period_order;
  ALTER TABLE tmp_entry_sel_selperiod
    ADD COLUMN team_id CHAR(3),
    ADD COLUMN opp_team_id CHAR(3);

  # Call our specific sport/sel_type processing logic to determine the above selection's opponents
  SET @v_sql := CONCAT('CALL records_', v_sport, '_', v_sel_type , '_events_historical("', v_sport, '", "', v_season, '", "', v_game_id, '");');
  PREPARE sth_step FROM @v_sql;
  EXECUTE sth_step;
  DEALLOCATE PREPARE sth_step;

  # Reduce the raw event list down
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_events;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_events (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    PRIMARY KEY (link_id, period_id)
  ) ENGINE=MyISAM
    SELECT link_id, period_id, team_id, opp_team_id
    FROM tmp_entry_sel_selperiod_events_raw
    GROUP BY link_id, period_id;

  # Merge back in to tmp_entry_sel_selperiod
  UPDATE tmp_entry_sel_selperiod AS SEL
  JOIN tmp_entry_sel_selperiod_events AS SEL_EVENTS
    ON (SEL_EVENTS.link_id = SEL.link_id
    AND SEL_EVENTS.period_id = SEL.period_id)
  SET SEL.team_id = SEL_EVENTS.team_id,
      SEL.opp_team_id = SEL_EVENTS.opp_team_id;

  # Extend this to include the bit_flag flag info
  ALTER TABLE tmp_entry_sel_selperiod
    ADD COLUMN opp_bit_flag TINYINT UNSIGNED;
  UPDATE tmp_entry_sel_selperiod AS SEL
  JOIN FANTASY_COMMON_SELECTIONS_MAPPED AS SEL_MAP
    ON (SEL_MAP.sport = v_sport
    AND SEL_MAP.season = v_season
    AND SEL_MAP.raw_id = SEL.opp_team_id)
  SET SEL.opp_bit_flag = SEL_MAP.bit_flag;

  # Aggregate this down to a per user list
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_useropps;
  CREATE TEMPORARY TABLE tmp_entry_sel_useropps (
    user_id SMALLINT UNSIGNED,
    opp_team_id CHAR(3),
    opp_bit_flag TINYINT UNSIGNED,
    PRIMARY KEY (user_id, opp_team_id)
  ) ENGINE=MyISAM
    SELECT ENTRY_SEL.user_id, SEL_PERIOD.opp_team_id, SEL_PERIOD.opp_bit_flag
    FROM FANTASY_COMMON_PERIODS_DATES AS PERIODS
    JOIN FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
      ON (ENTRY_SEL.sport = PERIODS.sport
      AND ENTRY_SEL.season = PERIODS.season
      AND ENTRY_SEL.game_id = v_game_id
      AND ENTRY_SEL.period_id = PERIODS.period_id)
    JOIN tmp_entry_sel_selperiod AS SEL_PERIOD
      ON (SEL_PERIOD.link_id = ENTRY_SEL.link_id
      AND SEL_PERIOD.period_id = ENTRY_SEL.period_id)
    WHERE PERIODS.sport = v_sport
    AND   PERIODS.season = v_season
    AND   PERIODS.period_order <= v_period_order
    GROUP BY ENTRY_SEL.user_id, SEL_PERIOD.opp_team_id;

  # Merge down to a single value
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_opp;
  CREATE TEMPORARY TABLE tmp_entry_sel_opp (
    user_id SMALLINT UNSIGNED,
    opponents INT UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE=MyISAM
    SELECT user_id, SUM(POWER(2, opp_bit_flag - 1)) AS opponents
    FROM tmp_entry_sel_useropps
    GROUP BY user_id;

  # And write back, to both the current and period-specific tables
  UPDATE FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
  LEFT JOIN tmp_entry_sel_opp AS ENTRY_OPP
    ON (ENTRY_OPP.user_id = ENTRY_SCORE.user_id)
  SET ENTRY_SCORE.opponents = IFNULL(ENTRY_OPP.opponents, 0)
  WHERE ENTRY_SCORE.sport = v_sport
  AND   ENTRY_SCORE.season = v_season
  AND   ENTRY_SCORE.game_id = v_game_id;

  UPDATE FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
  LEFT JOIN tmp_entry_sel_opp AS ENTRY_OPP
    ON (ENTRY_OPP.user_id = ENTRY_SEL.user_id)
  SET ENTRY_SEL.opponents = IFNULL(ENTRY_OPP.opponents, 0)
  WHERE ENTRY_SEL.sport = v_sport
  AND   ENTRY_SEL.season = v_season
  AND   ENTRY_SEL.game_id = v_game_id
  AND   ENTRY_SEL.period_id = v_period_id;

END $$

DELIMITER ;

#
# Determine the achievements by each entry in a game
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs'
BEGIN

  DECLARE v_achieve_id TINYINT UNSIGNED;
  DECLARE v_type VARCHAR(30);
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Loop through the achievements to be processed
  DECLARE cur_achieve CURSOR FOR
    SELECT achieve_id, type
    FROM FANTASY_RECORDS_GAMES_ACHIEVEMENTS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    ORDER BY disp_order;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Some common prep for subcalcs
  CALL records_scoring_entry_achievements_setup(v_sport, v_season, v_game_id);

  # Create the table in which we'll store per-entry achievements
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_achievements;
  CREATE TEMPORARY TABLE tmp_entry_achievements (
    user_id SMALLINT UNSIGNED NOT NULL,
    bit_flags TINYINT UNSIGNED NOT NULL,
    PRIMARY KEY (user_id, bit_flags)
  ) ENGINE=MyISAM;

  # Loop through the achievements and calculate
  OPEN cur_achieve;
  loop_achieve: LOOP

    FETCH cur_achieve INTO v_achieve_id, v_type;
    IF v_done = 1 THEN LEAVE loop_achieve; END IF;

    # Pass on to the appropriate per-type module
    CASE v_type
      WHEN 'score'     THEN CALL records_scoring_entry_achievements_score(v_sport, v_season, v_game_id, v_achieve_id);
      WHEN 'period'    THEN CALL records_scoring_entry_achievements_period(v_sport, v_season, v_game_id, v_period_id, v_achieve_id);
      WHEN 'opponent'  THEN CALL records_scoring_entry_achievements_opponent(v_sport, v_season, v_game_id, v_achieve_id);
      WHEN 'sel_usage' THEN CALL records_scoring_entry_achievements_sel_usage(v_sport, v_season, v_game_id, v_period_id, v_achieve_id);
      WHEN 'event'     THEN CALL records_scoring_entry_achievements_event(v_sport, v_season, v_game_id, v_period_id, v_achieve_id);
      ELSE
        SET @v_msg = CONCAT('Unknown achievement type "', v_type, '" for ', v_sport, '-', v_season, '-', v_game_id, '-', v_achieve_id, '.');
        SIGNAL SQLSTATE 'DB404' SET MESSAGE_TEXT = @v_msg;
    END CASE;

  END LOOP loop_achieve;
  CLOSE cur_achieve;

  # Aggregate the results into the bit flag
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_achieve_agg;
  CREATE TEMPORARY TABLE tmp_entry_achieve_agg (
    user_id SMALLINT UNSIGNED NOT NULL,
    achievements SMALLINT UNSIGNED NOT NULL,
    PRIMARY KEY (user_id)
  ) ENGINE=MyISAM
    SELECT user_id, SUM(POWER(2, bit_flags - 1)) AS achievements
    FROM tmp_entry_achievements
    GROUP BY user_id;

  # And write back, to both the current and period-specific tables
  UPDATE FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
  JOIN tmp_entry_achieve_agg AS ENTRY_ACHIEVE
    ON (ENTRY_ACHIEVE.user_id = ENTRY_SCORE.user_id)
  SET ENTRY_SCORE.achievements = ENTRY_ACHIEVE.achievements
  WHERE ENTRY_SCORE.sport = v_sport
  AND   ENTRY_SCORE.season = v_season
  AND   ENTRY_SCORE.game_id = v_game_id;

  UPDATE FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
  JOIN tmp_entry_achieve_agg AS ENTRY_ACHIEVE
    ON (ENTRY_ACHIEVE.user_id = ENTRY_SEL.user_id)
  SET ENTRY_SEL.achievements = ENTRY_ACHIEVE.achievements
  WHERE ENTRY_SEL.sport = v_sport
  AND   ENTRY_SEL.season = v_season
  AND   ENTRY_SEL.game_id = v_game_id
  AND   ENTRY_SEL.period_id = v_period_id;

END $$

DELIMITER ;

#
# Common pre-processing for achievement calcs
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements_setup`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calc setup'
BEGIN

  # Up memory limit to 256meg for some of the larger temporary table processing
  SET @@session.max_heap_table_size = 268435456; # 268,435,456 = 256 * 1024 * 1024
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

  DROP TEMPORARY TABLE IF EXISTS tmp_achieve_entry_sel;
  CREATE TEMPORARY TABLE tmp_achieve_entry_sel (
    user_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, period_id),
    KEY link_period (link_id, period_id) USING BTREE
  ) ENGINE=MEMORY
    SELECT user_id, period_id, link_id
    FROM FANTASY_RECORDS_ENTRIES_BYPERIOD
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_achieve_sel;
  CREATE TEMPORARY TABLE tmp_achieve_sel (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    score DECIMAL(5,1) SIGNED,
    sel_pct DECIMAL(6,3) UNSIGNED,
    PRIMARY KEY (link_id, period_id)
  ) ENGINE=MEMORY
    SELECT link_id, period_id, score, sel_pct
    FROM FANTASY_RECORDS_SELECTIONS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   status IN ('positive', 'neutral');

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_achievements_info;
  CREATE TEMPORARY TABLE tmp_entry_achievements_info (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    score DECIMAL(5,1) SIGNED,
    sel_pct DECIMAL(6,3) UNSIGNED,
    PRIMARY KEY (sport, season, game_id, user_id, period_id)
  ) ENGINE=MEMORY
    SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id, ENTRY_SEL.user_id, ENTRY_SEL.period_id,
          SEL.link_id, SEL.score, SEL.sel_pct
    FROM tmp_achieve_entry_sel AS ENTRY_SEL
    JOIN tmp_achieve_sel AS SEL
      ON (SEL.link_id = ENTRY_SEL.link_id
      AND SEL.period_id = ENTRY_SEL.period_id);

  # Revert the memory change
  SET @@session.max_heap_table_size = DEFAULT; # DEFAULT = global value (i.e., what we had before...)
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

END $$

DELIMITER ;

#
# Process 'score' based achievements
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements_score`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements_score`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_achieve_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs for score groups'
BEGIN

  INSERT INTO tmp_entry_achievements (user_id, bit_flags)
    SELECT ENTRY_SCORE.user_id, ACHIEVE.bit_flag
    FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
    JOIN FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS AS ACHIEVE
      ON (ACHIEVE.sport = ENTRY_SCORE.sport
      AND ACHIEVE.season = ENTRY_SCORE.season
      AND ACHIEVE.game_id = ENTRY_SCORE.game_id
      AND ACHIEVE.achieve_id = v_achieve_id
      AND ACHIEVE.value <= ENTRY_SCORE.overall_res)
    WHERE ENTRY_SCORE.sport = v_sport
    AND   ENTRY_SCORE.season = v_season
    AND   ENTRY_SCORE.game_id = v_game_id;

END $$

DELIMITER ;

#
# Process 'period score' based achievements
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements_period`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements_period`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_achieve_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs for period groups'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_achievements_period;
  CREATE TEMPORARY TABLE tmp_entry_achievements_period (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    score DECIMAL(5,1) SIGNED,
    PRIMARY KEY (sport, season, game_id, user_id)
  ) ENGINE=MEMORY
    SELECT sport, season, game_id, user_id, MAX(score) AS score
    FROM tmp_entry_achievements_info
    GROUP BY sport, season, game_id, user_id;

  INSERT INTO tmp_entry_achievements (user_id, bit_flags)
    SELECT ENTRY_SEL.user_id, ACHIEVE.bit_flag
    FROM tmp_entry_achievements_period AS ENTRY_SEL
    JOIN FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS AS ACHIEVE
      ON (ACHIEVE.sport = ENTRY_SEL.sport
      AND ACHIEVE.season = ENTRY_SEL.season
      AND ACHIEVE.game_id = ENTRY_SEL.game_id
      AND ACHIEVE.achieve_id = v_achieve_id
      AND ACHIEVE.value <= ENTRY_SEL.score);

END $$

DELIMITER ;

#
# Process 'opponent' based achievements
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements_opponent`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements_opponent`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_achieve_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs for period groups'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_opps;
  CREATE TEMPORARY TABLE tmp_entry_opps (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    num_opp TINYINT UNSIGNED,
    PRIMARY KEY (sport, season, game_id, user_id)
  ) ENGINE=MyISAM
    SELECT sport, season, game_id, user_id, LENGTH(REPLACE(BIN(opponents), '0', '')) AS num_opp
    FROM FANTASY_RECORDS_ENTRIES_SCORES
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id;

  INSERT INTO tmp_entry_achievements (user_id, bit_flags)
    SELECT ENTRY_OPP.user_id, ACHIEVE.bit_flag
    FROM tmp_entry_opps AS ENTRY_OPP
    JOIN FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS AS ACHIEVE
      ON (ACHIEVE.sport = ENTRY_OPP.sport
      AND ACHIEVE.season = ENTRY_OPP.season
      AND ACHIEVE.game_id = ENTRY_OPP.game_id
      AND ACHIEVE.achieve_id = v_achieve_id
      AND ACHIEVE.value <= ENTRY_OPP.num_opp)
    WHERE ENTRY_OPP.sport = v_sport
    AND   ENTRY_OPP.season = v_season
    AND   ENTRY_OPP.game_id = v_game_id;

END $$

DELIMITER ;

#
# Process 'selection usage' based achievements
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements_sel_usage`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements_sel_usage`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_achieve_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs for sel_usage groups'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_achievements_selpct;
  CREATE TEMPORARY TABLE tmp_entry_achievements_selpct (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    sel_pct DECIMAL(6,3) UNSIGNED,
    PRIMARY KEY (sport, season, game_id, user_id)
  ) ENGINE=MEMORY
    SELECT sport, season, game_id, user_id, MIN(sel_pct) AS sel_pct
    FROM tmp_entry_achievements_info
    GROUP BY sport, season, game_id, user_id;

  INSERT INTO tmp_entry_achievements (user_id, bit_flags)
    SELECT ENTRY_SEL.user_id, ACHIEVE.bit_flag
    FROM tmp_entry_achievements_selpct AS ENTRY_SEL
    JOIN FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS AS ACHIEVE
      ON (ACHIEVE.sport = ENTRY_SEL.sport
      AND ACHIEVE.season = ENTRY_SEL.season
      AND ACHIEVE.game_id = ENTRY_SEL.game_id
      AND ACHIEVE.achieve_id = v_achieve_id
      AND ACHIEVE.value >= ENTRY_SEL.sel_pct);

END $$

DELIMITER ;

#
# Process 'event' / 'custom' based achievements
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_achievements_event`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_achievements_event`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_achieve_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs for ad hoc groups'
BEGIN

  DECLARE v_bit_flag TINYINT UNSIGNED;
  DECLARE v_event TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Loop through the achievements to be processed
  DECLARE cur_event CURSOR FOR
    SELECT bit_flag, value
    FROM FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   achieve_id = v_achieve_id
    ORDER BY disp_order;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Loop through the event, and calc
  OPEN cur_event;
  loop_event: LOOP

    FETCH cur_event INTO v_bit_flag, v_event;
    IF v_done = 1 THEN LEAVE loop_event; END IF;

    # Call the per-event processing logic
    SET @v_sql := CONCAT('CALL records_', v_sport, '_achievement_events_', v_event, '("', v_sport, '", "', v_season, '", "', v_game_id, '", "', v_period_id, '", "', v_bit_flag, '");');
    PREPARE sth_event FROM @v_sql;
    EXECUTE sth_event;
    DEALLOCATE PREPARE sth_event;

  END LOOP loop_event;
  CLOSE cur_event;

END $$

DELIMITER ;
