##
## Social media processing helpers
##

DROP FUNCTION IF EXISTS `records_social_format_score`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `records_social_format_score`(
  v_score DECIMAL(5,1) SIGNED
) RETURNS VARCHAR(10)
  DETERMINISTIC
  COMMENT 'Convert a raw score into a display version'
BEGIN

  RETURN REPLACE(FORMAT(v_score, 1), '.0', '');

END $$

DELIMITER ;
