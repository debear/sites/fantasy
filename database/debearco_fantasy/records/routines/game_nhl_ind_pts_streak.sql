##
## X Game Point Streak
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_pts_streak_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_pts_streak_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for nhl:ind:pts:streak'
BEGIN

  # This is a synonym of nhl:ind:pts:total
  CALL records_nhl_ind_pts_total_preview(v_sport, v_season, v_game_id, v_period_id, v_start_date);

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_pts_streak_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_pts_streak_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for nhl:ind:pts:streak'
BEGIN

  # This is a synonym of nhl:ind:pts:total
  CALL records_nhl_ind_pts_total_opprating(v_sport, v_season, v_game_id, v_period_id, v_start_date);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_pts_streak_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_pts_streak_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for nhl:ind:pts:streak'
BEGIN

  # This is a synonym of nhl:ind:pts:total
  CALL records_nhl_ind_pts_total_selrating(v_sport, v_season, v_game_id, v_period_id, v_start_date);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_pts_streak_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_pts_streak_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for nhl:ind:pts:streak'
BEGIN

  # This is a synonym of nhl:ind:pts:total...
  CALL records_nhl_ind_pts_total_scoring(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  # ...with a small tweak - the stat field is a boolean not a counter
  UPDATE tmp_link_events SET stat = (stat > 0);

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_nhl_ind_pts_streak_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_ind_pts_streak_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for nhl_ind_pts_streak'
BEGIN

  # This is a synonym of nhl:ind:pts:total
  CALL records_nhl_ind_pts_total_stress(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);

END $$

DELIMITER ;
