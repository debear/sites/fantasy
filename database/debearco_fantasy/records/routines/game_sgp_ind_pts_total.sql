##
## X Point Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_sgp_ind_pts_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_sgp_ind_pts_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for sgp:ind:pts:total'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN points_heat_value DOUBLE,
    ADD COLUMN sf_app DOUBLE,
    ADD COLUMN gf_app DOUBLE;

  # Calculate the individual values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, points_heat_value, sf_app, gf_app)
    SELECT RACE_RIDER.rider_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(IF(RACE_HEAT.heat_type = 'main', IFNULL(RACE_HEAT.pts, 0), 0)) / SUM(RACE_HEAT.heat_type = 'main') AS points_heat_value,
           SUM(RACE_HEAT.heat_type = 'sf') AS sf_app,
           SUM(RACE_HEAT.heat_type = 'gf') AS gf_app
    FROM debearco_sports.SPORTS_SGP_RACES AS RACE
    JOIN debearco_sports.SPORTS_SGP_RACE_RIDERS AS RACE_RIDER
      ON (RACE_RIDER.season = RACE.season
      AND RACE_RIDER.round = RACE.round)
    JOIN debearco_sports.SPORTS_SGP_RACE_HEATS AS RACE_HEAT
      ON (RACE_HEAT.season = RACE_RIDER.season
      AND RACE_HEAT.round = RACE_RIDER.round
      AND RACE_HEAT.bib_no = RACE_RIDER.bib_no)
    WHERE RACE.season = v_season
    AND   DATE(RACE.race_time) < v_start_date
    AND   RACE.completed_heats > 0
    GROUP BY RACE_RIDER.rider_id;

  # Now process the individual stat elements
  # 1 - Points/Heat
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'points-per-heat') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = points_heat_value,
        stat_text = FORMAT(points_heat_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - SF App
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'sf-app') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = sf_app,
        stat_text = sf_app;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - GF App
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'gf-app') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = gf_app,
        stat_text = gf_app;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN points_heat_value,
    DROP COLUMN sf_app,
    DROP COLUMN gf_app;

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_sgp_ind_pts_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_sgp_ind_pts_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for sgp:ind:pts:total'
BEGIN

  # Determine the previous races to process
  CALL sport_sgp_recent_races(v_season, v_start_date, 25);

  # Penalise older races by giving these races a lower multiplier
  ALTER TABLE tmp_races
    ADD COLUMN race_multiplier DECIMAL(2, 1) UNSIGNED;
  UPDATE tmp_races SET race_multiplier = 1 - ((v_season - season) * 0.2);

  # Produce the rider calculations on qualifying and relative results
  DROP TEMPORARY TABLE IF EXISTS tmp_rider_results;
  CREATE TEMPORARY TABLE tmp_rider_results (
    rider_id SMALLINT UNSIGNED,
    num_races TINYINT UNSIGNED,
    num_heats TINYINT UNSIGNED,
    pts_tot SMALLINT UNSIGNED,
    pts_avg DECIMAL(5, 4) UNSIGNED,
    PRIMARY KEY (rider_id)
  ) ENGINE = MyISAM
    SELECT RACE_RIDER.rider_id,
           COUNT(DISTINCT CONCAT(RACE.season, ':', RACE.round)) AS num_races,
           COUNT(*) AS num_heats,
           SUM(IFNULL(RIDER_HEATS.pts * RACE.race_multiplier, 0)) AS pts_tot,
           IFNULL(SUM(IFNULL(RIDER_HEATS.pts * RACE.race_multiplier, 0)) / COUNT(*), 0) AS pts_avg
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_SGP_RACE_RIDERS AS RACE_RIDER
      ON (RACE_RIDER.season = RACE.season
      AND RACE_RIDER.round = RACE.round)
    JOIN tmp_links AS SEL
      ON (SEL.link_id = RACE_RIDER.rider_id)
    LEFT JOIN debearco_sports.SPORTS_SGP_RACE_HEATS AS RIDER_HEATS
      ON (RIDER_HEATS.season = RACE_RIDER.season
      AND RIDER_HEATS.round = RACE_RIDER.round
      AND RIDER_HEATS.heat_type = 'main'
      AND RIDER_HEATS.bib_no = RACE_RIDER.bib_no)
    GROUP BY RACE_RIDER.rider_id;

  # Standardise these relative values (to the race count)
  ALTER TABLE tmp_rider_results
    ADD COLUMN pts_rel DECIMAL(5, 4) UNSIGNED;
  UPDATE tmp_rider_results SET pts_rel = pts_avg * POW(0.99, SQRT(125 - num_heats));

  # Ensure the value fits into some form of statistical relevance range and in a 0..100 range
  ALTER TABLE tmp_rider_results
    ADD COLUMN rtg TINYINT UNSIGNED;
  CALL _statistical_range('tmp_rider_results', 'pts_rel', 'rtg', NULL, 0, 100, 'DESC');

  # And then write back
  UPDATE FANTASY_RECORDS_SELECTIONS AS SEL
  JOIN tmp_links
    ON (tmp_links.link_id = SEL.link_id)
  LEFT JOIN tmp_rider_results
    ON (tmp_rider_results.rider_id = SEL.link_id)
  SET SEL.rating = IF(tmp_rider_results.rtg IS NOT NULL, tmp_rider_results.rtg, IF(tmp_links.rider_type = 'regular', 33, IF(tmp_links.rider_type IN ('track_reserve', 'unknown'), 0, 20))) # Final ELSE is Wildcard or Substitute
  WHERE SEL.sport = v_sport
  AND   SEL.season = v_season
  AND   SEL.game_id = v_game_id
  AND   SEL.period_id = v_period_id;

  # Revert our local changes to tmp_races
  ALTER TABLE tmp_races
    DROP COLUMN race_multiplier;

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_sgp_ind_pts_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_sgp_ind_pts_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for sgp:ind:pts:total'
BEGIN

  # Determine what an "acceptable" value is, based on the pts/meeting required
  DECLARE v_per_meeting_lo DECIMAL(5,3) UNSIGNED;
  DECLARE v_per_meeting_hi DECIMAL(5,3) UNSIGNED;
  SELECT 0.75 * (GAME.target / COUNT(DATES.period_id)), GAME.target / COUNT(DATES.period_id)
    INTO v_per_meeting_lo, v_per_meeting_hi
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
    ON (DATES.sport = GAME.sport
    AND DATES.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  # Now perform the update
  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_SGP_RACE_RESULT AS STAT
    ON (STAT.season = SEL.season
    AND STAT.round = SEL.round
    AND STAT.bib_no = SEL.bib_no)
  SET SEL.stat = IFNULL(STAT.pts, 0),
      SEL.status = CASE
        WHEN STAT.pos IS NULL THEN 'na'
        WHEN IFNULL(STAT.pts, 0) < v_per_meeting_lo THEN 'negative'
        WHEN IFNULL(STAT.pts, 0) < v_per_meeting_hi THEN 'neutral'
        ELSE 'positive'
      END,
      SEL.summary = pluralise(IFNULL(STAT.pts, '0'), 'pt');

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_sgp_ind_pts_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_sgp_ind_pts_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for sgp:ind:pts:total'
BEGIN

  # Establish each rider's race progress
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    round TINYINT UNSIGNED,
    main_ride1 TINYINT UNSIGNED,
    main_ride2 TINYINT UNSIGNED,
    main_ride3 TINYINT UNSIGNED,
    main_ride4 TINYINT UNSIGNED,
    main_ride5 TINYINT UNSIGNED,
    sf_pos CHAR(1),
    gf_pos CHAR(1),
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, round)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.round,
           SUM(IF(RACE_HEATS.heat <=  4, IFNULL(RACE_HEATS.pts, 0), 0)) AS main_ride1,
           SUM(IF(RACE_HEATS.heat <=  8, IFNULL(RACE_HEATS.pts, 0), 0)) AS main_ride2,
           SUM(IF(RACE_HEATS.heat <= 12, IFNULL(RACE_HEATS.pts, 0), 0)) AS main_ride3,
           SUM(IF(RACE_HEATS.heat <= 16, IFNULL(RACE_HEATS.pts, 0), 0)) AS main_ride4,
           SUM(IF(RACE_HEATS.heat <= 20, IFNULL(RACE_HEATS.pts, 0), 0)) AS main_ride5,
           RACE_SEMI.result AS sf_pos,
           RACE_FINAL.result AS gf_pos,
           NULL AS stress
    FROM tmp_link_events AS SEL
    LEFT JOIN debearco_sports.SPORTS_SGP_RACE_HEATS AS RACE_HEATS
      ON (RACE_HEATS.season = SEL.season
      AND RACE_HEATS.round = SEL.round
      AND RACE_HEATS.heat_type = 'main'
      AND RACE_HEATS.bib_no = SEL.bib_no)
    LEFT JOIN debearco_sports.SPORTS_SGP_RACE_HEATS AS RACE_SEMI
      ON (RACE_SEMI.season = SEL.season
      AND RACE_SEMI.round = SEL.round
      AND RACE_SEMI.heat_type = 'sf'
      AND RACE_SEMI.bib_no = SEL.bib_no)
    LEFT JOIN debearco_sports.SPORTS_SGP_RACE_HEATS AS RACE_FINAL
      ON (RACE_FINAL.season = SEL.season
      AND RACE_FINAL.round = SEL.round
      AND RACE_FINAL.heat_type = 'gf'
      AND RACE_FINAL.bib_no = SEL.bib_no)
    GROUP BY SEL.link_id, SEL.season, SEL.round;

  # Then assign a stress score to each player based on their total and rushes against the team calcs
  UPDATE tmp_link_stress
  SET stress = ((GREATEST(2 - CAST(main_ride1 AS SIGNED), 0) / 2) * 13) # 2pts after 1 ride
             + ((GREATEST(4 - CAST(main_ride2 AS SIGNED), 0) / 4) * 25) # 4pts after 2 rides
             + ((GREATEST(6 - CAST(main_ride3 AS SIGNED), 0) / 6) * 38) # 6pts after 3 rides
             + ((GREATEST(8 - CAST(main_ride4 AS SIGNED), 0) / 8) * 65) # 8pts after 4 rides
             + CASE IFNULL(sf_pos, '-') # Semi Final pos
                 WHEN '1' THEN 0
                 WHEN '2' THEN 12
                 WHEN '3' THEN 38
                 WHEN '4' THEN 57
                 WHEN '-' THEN 76 # Did not qualify
                 ELSE 57          # Qualified for, but did not complete, the semi final
               END
             + CASE IFNULL(gf_pos, '-') # Grand Final pos
                 WHEN '1' THEN 0
                 WHEN '2' THEN 8
                 WHEN '3' THEN 19
                 WHEN '4' THEN 27
                 WHEN '-' THEN 38 # Did not qualify
                 ELSE 27          # Qualified for, but did not complete, the grand final
               END;

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.round = SEL.round)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
