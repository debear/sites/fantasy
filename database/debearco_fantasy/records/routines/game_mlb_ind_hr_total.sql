##
## X Home Run Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hr_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hr_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for mlb:ind:hr:total'
BEGIN

  DECLARE v_split_LHP TINYINT UNSIGNED;
  DECLARE v_split_RHP TINYINT UNSIGNED;
  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN hr_rank DOUBLE,
    ADD COLUMN hr_text VARCHAR(30),
    ADD COLUMN vs_lrhp_value DOUBLE,
    ADD COLUMN vs_lrhp_text VARCHAR(30),
    ADD COLUMN vs_sp_value DOUBLE,
    ADD COLUMN vs_sp_text VARCHAR(30);

  # Pre-calc the Split IDs
  SELECT GROUP_CONCAT(IF(split_label = 'v LHP', split_id, '') SEPARATOR ''),
         GROUP_CONCAT(IF(split_label = 'v RHP', split_id, '') SEPARATOR '')
    INTO v_split_LHP, v_split_RHP
  FROM debearco_sports.SPORTS_MLB_PLAYERS_SPLIT_LABELS
  WHERE split_type = 'pitcher-info'
  AND   split_label IN ('v LHP', 'v RHP');

  # Calculate the Season HR, vs L/RHP and vs Starter
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, hr_rank, hr_text, vs_lrhp_value, vs_lrhp_text, vs_sp_value, vs_sp_text)
    SELECT tmp_links.link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           IFNULL(SEASON.hr, -0.1) AS hr_rank,
           IF(SEASON.hr IS NOT NULL, CONCAT(SEASON.hr, ' (in ', SEASON.ab, ' AB)'), '&ndash;') AS hr_text,
           IFNULL(SPLITS.hr, -0.1) AS vs_lrhp_value,
           IF(SPLITS.hr IS NOT NULL, CONCAT(SPLITS.hr, ' (in ', SPLITS.ab, ' AB)'), '&ndash;') AS vs_lrhp_text,
           IFNULL(BVP.hr, -0.1) AS vs_sp_value,
           IF(BVP.hr IS NOT NULL, CONCAT(BVP.hr, ' (in ', BVP.ab, ' AB)'), '&ndash;') AS vs_sp_text
    FROM tmp_links
    LEFT JOIN tmp_period_teams
      ON (tmp_period_teams.team_id = tmp_links.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_SEASON_BATTING AS SEASON
      ON (SEASON.season = v_season
      AND SEASON.season_type = 'regular'
      AND SEASON.player_id = tmp_links.link_id
      AND SEASON.is_totals = 1)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS AS SPLITS
      ON (SPLITS.season = v_season
      AND SPLITS.season_type = 'regular'
      AND SPLITS.player_id = tmp_links.link_id
      AND SPLITS.split_type = 'pitcher-info'
      AND SPLITS.split_label = IF(tmp_period_teams.opp_probable_throws = 'R', v_split_RHP, v_split_LHP))
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER AS BVP
      ON (BVP.batter = tmp_links.link_id
      AND BVP.pitcher = tmp_period_teams.opp_probable_id
      AND BVP.season_type = 'regular');

  # Now process the individual stat elements
  # 1 - Season HR
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'home-runs') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = hr_rank,
        stat_text = hr_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - vs L/RHP
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'vs-lhp-rhp') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = vs_lrhp_value,
        stat_text = vs_lrhp_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 3 - vs Starter
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'vs-starter') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = vs_sp_value,
        stat_text = vs_sp_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN hr_rank,
    DROP COLUMN hr_text,
    DROP COLUMN vs_lrhp_value,
    DROP COLUMN vs_lrhp_text,
    DROP COLUMN vs_sp_value,
    DROP COLUMN vs_sp_text;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hr_total_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hr_total_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for mlb:ind:hr:total'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 81);

  # Calc 1: Fly Balls allowed
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.hr + STAT.fo, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.hr + STAT.fo, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_GAME_PITCHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'DESC'); # 60/40 Overall-vs-Home/Road, More Fly Balls makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_flyball;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_flyball;

  # Calc 2: Game Score
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.game_score, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.game_score, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_GAME_PITCHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.4, 'ASC'); # 60/40 Overall-vs-Home/Road, Lower Game Score makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_gamescore;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_gamescore;

  # Calc 3: Batters Faced
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.h, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.h, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_GAME_PITCHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(1.0, 'DESC'); # 0/100 Overall-vs-Home/Road, More Batters Faced makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_batters;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_batters;

  # Merge (split 40/40/20 HR/Game Score/Fly Balls) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    home_opp_rating TINYINT UNSIGNED,
    visitor_opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id,
           (FLYBALL.home_opp_rating * 0.40) + (GAMESCORE.home_opp_rating * 0.40) + (BATTER.home_opp_rating * 0.20) AS home_opp_rating,
           (FLYBALL.visitor_opp_rating * 0.40) + (GAMESCORE.visitor_opp_rating * 0.40) + (BATTER.visitor_opp_rating * 0.20) AS visitor_opp_rating
    FROM tmp_all_teams AS TEAMS
    JOIN tmp_team_oppcalcs_flyball AS FLYBALL
      ON (FLYBALL.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_gamescore AS GAMESCORE
      ON (GAMESCORE.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_batters AS BATTER
      ON (BATTER.team_id = TEAMS.team_id);
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hr_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hr_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for mlb:ind:hr:total'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_mlb_ind_sel_recent_sched(v_season, v_start_date, 81);

  # Opportunity 1: Depth Chart (direct rating calc)
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_depth;
  CREATE TEMPORARY TABLE tmp_rating_opportunity_depth (
    link_id SMALLINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           100 * MAX(DEPTH.season IS NOT NULL) AS rating
    FROM tmp_links AS LINK
    LEFT JOIN debearco_sports.SPORTS_MLB_TEAMS_DEPTH AS DEPTH
      ON (DEPTH.season = v_season
      AND DEPTH.player_id = LINK.link_id
      AND DEPTH.depth = 1)
    GROUP BY LINK.link_id;

  # Opportunity 2: PA / GP
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(STAT.pa) AS stat,
           SUM(STAT.pa) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Plate Appearances make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_pagp;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_pagp;

  # Opportunity 3: GS relative to GP
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(ROSTER.played) > 0, SUM(ROSTER.started) / SUM(ROSTER.played), NULL) AS stat,
           SUM(ROSTER.started) AS stat_nom,
           SUM(ROSTER.played) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_GAME_ROSTERS AS ROSTER
      ON (ROSTER.season = RECENT.season
      AND ROSTER.game_type = RECENT.game_type
      AND ROSTER.game_id = RECENT.game_id
      AND ROSTER.player_id = RECENT.link_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Starts make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_starts;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_starts;

  # Metric 1: PA / HR
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.hr) > 0, LEAST(99.99999, SUM(STAT.pa) / SUM(STAT.hr)), NULL) AS stat,
           SUM(STAT.pa) AS stat_nom,
           SUM(STAT.hr) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'ASC', 'apply_least'); # Fewer PA per HR makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_pahr;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_pahr;

  # Metric 2: ISO
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.ab) > 0, SUM(STAT.tb - STAT.h) / SUM(STAT.ab), NULL) AS stat,
           SUM(STAT.tb - STAT.h) AS stat_nom,
           SUM(STAT.ab) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', NULL); # Higher ISO makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_iso;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_iso;

  # Merge (split opportunity 18/16/16 Depth/PA-GP/GS-GP, metrics 13/22 PA-HR/ISO, 15 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_opp_depth TINYINT UNSIGNED,
    rating_opp_pagp TINYINT UNSIGNED,
    rating_opp_starts TINYINT UNSIGNED,
    rating_metric_pahr TINYINT UNSIGNED,
    rating_metric_iso TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           OPP_DEPTH.rating AS rating_opp_depth,
           OPP_PAGP.rating AS rating_opp_pagp,
           OPP_STARTS.rating AS rating_opp_starts,
           METRIC_PAHR.rating AS rating_metric_pahr,
           METRIC_ISO.rating AS rating_metric_iso,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(OPP_DEPTH.rating, 0) * 0.18) + (IFNULL(OPP_PAGP.rating, 0) * 0.16) + (IFNULL(OPP_STARTS.rating, 0) * 0.16)
             + (IFNULL(METRIC_PAHR.rating, 0) * 0.13) + (IFNULL(METRIC_ISO.rating, 0) * 0.22)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.15) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_opportunity_depth AS OPP_DEPTH
      ON (OPP_DEPTH.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_pagp AS OPP_PAGP
      ON (OPP_PAGP.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_starts AS OPP_STARTS
      ON (OPP_STARTS.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_pahr AS METRIC_PAHR
      ON (METRIC_PAHR.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_iso AS METRIC_ISO
      ON (METRIC_ISO.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hr_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hr_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for mlb:ind:hr:total'
BEGIN

  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_MLB_PLAYERS_GAME_BATTING AS STAT
    ON (STAT.season = SEL.season
    AND STAT.game_type = SEL.game_type
    AND STAT.game_id = SEL.game_id
    AND STAT.player_id = SEL.link_id)
  SET SEL.stat = STAT.hr,
      SEL.status = CASE
        WHEN STAT.ab = 0 THEN 'na'
        WHEN STAT.hr = 0 THEN 'negative'
        ELSE 'positive'
      END,
      SEL.summary = CONCAT(STAT.hr, ' HR');

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_mlb_ind_hr_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_ind_hr_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for mlb:ind:hr:total'
BEGIN

  # Determine when each player hit their first home run
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    stress TINYINT UNSIGNED,
    first_by_inning TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.game_id,
           NULL AS stress,
           MIN(ATBAT.inning) AS first_by_inning
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_MLB_GAME_ATBAT_FLAGS AS ATBAT
      ON (ATBAT.season = SEL.season
      AND ATBAT.game_type = SEL.game_type
      AND ATBAT.game_id = SEL.game_id
      AND ATBAT.batter = SEL.link_id
      AND ATBAT.is_pa = 1
      AND ATBAT.tb = 4)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id;

  # Then assign a stress score to each
  UPDATE tmp_link_stress
  SET stress = CASE
    WHEN first_by_inning < 7 THEN ((first_by_inning / 6) * 155)
    WHEN first_by_inning < 9 THEN (((first_by_inning - 6) / 3) * 100) + 155
    ELSE 255
  END;

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
