#
# Preparatory steps for the entry scoring calculations
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_prep`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_prep`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry scoring prep for all score types'
BEGIN

  DECLARE v_period_order TINYINT UNSIGNED;
  DECLARE v_sel_required TINYINT UNSIGNED;

  # Load each entry's selections
  SELECT period_order INTO v_period_order
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  AND   period_id = v_period_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel;
  CREATE TEMPORARY TABLE tmp_entry_sel (
    user_id INT UNSIGNED,
    period_id TINYINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    score DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    stress TINYINT UNSIGNED,
    period_order TINYINT UNSIGNED,
    PRIMARY KEY (user_id, period_id)
  ) ENGINE = MyISAM
    SELECT ENTRY_SEL.user_id, ENTRY_SEL.period_id, ENTRY_SEL.link_id, SEL.score, SEL.status, SEL.stress, PERIOD.period_order
    FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD
    JOIN FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
      ON (ENTRY_SEL.sport = PERIOD.sport
      AND ENTRY_SEL.season = PERIOD.season
      AND ENTRY_SEL.game_id = v_game_id
      AND ENTRY_SEL.period_id = PERIOD.period_id
      AND ENTRY_SEL.link_id IS NOT NULL)
    JOIN FANTASY_RECORDS_SELECTIONS AS SEL
      ON (SEL.sport = ENTRY_SEL.sport
      AND SEL.season = ENTRY_SEL.season
      AND SEL.game_id = ENTRY_SEL.game_id
      AND SEL.link_id = ENTRY_SEL.link_id
      AND SEL.period_id = ENTRY_SEL.period_id)
    WHERE PERIOD.sport = v_sport
    AND   PERIOD.season = v_season
    AND   PERIOD.period_order <= v_period_order;

  # If the game requires a selection, backport an equivalent 'negative' row
  SELECT FIND_IN_SET('SEL_REQUIRED', config_flags) > 0 INTO v_sel_required
  FROM FANTASY_RECORDS_GAMES
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id;

  IF v_sel_required THEN
    # Identify entries that did not make a selection
    DROP TEMPORARY TABLE IF EXISTS tmp_entry_missing;
    CREATE TEMPORARY TABLE tmp_entry_missing (
      user_id INT UNSIGNED,
      PRIMARY KEY (user_id)
    ) ENGINE = MyISAM
      SELECT ENTRY.user_id
      FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY
      LEFT JOIN tmp_entry_sel AS ENTRY_SEL
        ON (ENTRY_SEL.user_id = ENTRY.user_id
        AND ENTRY_SEL.period_id = v_period_id)
      WHERE ENTRY.sport = v_sport
      AND   ENTRY.season = v_season
      AND   ENTRY.game_id = v_game_id
      AND   ENTRY_SEL.user_id IS NULL;

    # Now add the dummy selection
    INSERT INTO tmp_entry_sel
      SELECT user_id,
             v_period_id AS period_id,
             0 AS link_id,
             0 AS score,
             'negative' AS status,
             255 AS stress,
             v_period_order AS period_order
      FROM tmp_entry_missing;
  END IF;

  # Create a copy of the raw / full selection table for processing which requires the full history
  CALL _duplicate_tmp_table('tmp_entry_sel', 'tmp_entry_sel_full');

END $$

DELIMITER ;

#
# Process entry scoring for games with a 'total' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_total`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_total`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker entry scoring for total score type'
BEGIN

  # As some games limit the number of selections to use, apply if that applies here
  DECLARE v_active_limit TINYINT UNSIGNED;
  SELECT active_limit INTO v_active_limit
  FROM FANTASY_RECORDS_GAMES
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id;

  IF v_active_limit IS NOT NULL THEN
    ALTER TABLE tmp_entry_sel
      ADD COLUMN sel_recency TINYINT UNSIGNED DEFAULT 255;

    CALL _duplicate_tmp_table('tmp_entry_sel', 'tmp_entry_sel_cpA');
    DELETE FROM tmp_entry_sel_cpA WHERE status NOT IN ('positive','neutral','negative');
    INSERT INTO tmp_entry_sel (user_id, period_id, sel_recency)
      SELECT SQL_BIG_RESULT user_id, period_id, ROW_NUMBER() OVER w AS ab_order
      FROM tmp_entry_sel_cpA
      WINDOW w AS (PARTITION BY user_id ORDER BY user_id, period_order DESC)
    ON DUPLICATE KEY UPDATE sel_recency = VALUES(sel_recency);
    DROP TEMPORARY TABLE tmp_entry_sel_cpA;

    DELETE FROM tmp_entry_sel WHERE sel_recency > v_active_limit;
  END IF;

  # Perform the update
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_scores;
  CREATE TEMPORARY TABLE tmp_entry_scores (
    user_id INT UNSIGNED,
    result DECIMAL(5,1) SIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE = MyISAM
    SELECT user_id, SUM(score) AS result, AVG(stress) AS stress
    FROM tmp_entry_sel
    WHERE status IN ('positive','neutral','negative')
    GROUP BY user_id;

END $$

DELIMITER ;

#
# Process entry scoring for games with a 'streak' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_streak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_streak`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker entry scoring for streak score type'
BEGIN

  # To determine the current streak, determine when it started based on the last time it ended
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_streak_start;
  CREATE TEMPORARY TABLE tmp_entry_streak_start (
    user_id INT UNSIGNED,
    last_negative TINYINT UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE = MyISAM
    SELECT user_id, MAX(period_order) AS last_negative
    FROM tmp_entry_sel
    WHERE status = 'negative'
    GROUP BY user_id;

  # Prune the earlier selections
  DELETE tmp_entry_sel.*
  FROM tmp_entry_streak_start
  JOIN tmp_entry_sel
    ON (tmp_entry_sel.user_id = tmp_entry_streak_start.user_id
    AND tmp_entry_sel.period_order < tmp_entry_streak_start.last_negative);

  # Perform the update
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_scores;
  CREATE TEMPORARY TABLE tmp_entry_scores (
    user_id INT UNSIGNED,
    result DECIMAL(5,1) SIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE = MyISAM
    SELECT user_id, SUM(score) AS result, AVG(stress) AS stress
    FROM tmp_entry_sel
    WHERE status IN ('positive','neutral','negative')
    GROUP BY user_id;

END $$

DELIMITER ;

#
# Process entry scoring for games with a 'periods' score type
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_periods`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_periods`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker entry scoring for periods score type'
BEGIN

  # Processing-wise, this is an equivalent to a 'total' game
  CALL records_scoring_entry_total(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);

END $$

DELIMITER ;

#
# Store the entry scores for a period
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_store`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry scoring storing'
BEGIN

  UPDATE FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
  LEFT JOIN tmp_entry_scores AS SCORES
    ON (SCORES.user_id = ENTRY_SEL.user_id)
  SET ENTRY_SEL.result = IFNULL(SCORES.result, 0),
      ENTRY_SEL.stress = SCORES.stress
  WHERE ENTRY_SEL.sport = v_sport
  AND   ENTRY_SEL.season = v_season
  AND   ENTRY_SEL.game_id = v_game_id
  AND   ENTRY_SEL.period_id = v_period_id
  AND   ENTRY_SEL.link_id IS NOT NULL;

END $$

DELIMITER ;

#
# Propagate entry scores from selection to score record
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_scores`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_scores`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry scoring propagation to the entry record'
BEGIN

  # Transfer the current score from the previous selection, and determine if it has increased their best score
  UPDATE FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
  JOIN tmp_entry_scores AS CALC_SCORE
    ON (CALC_SCORE.user_id = ENTRY_SCORE.user_id)
  SET ENTRY_SCORE.current_res = IFNULL(CALC_SCORE.result, 0),
      ENTRY_SCORE.overall_res = GREATEST(ENTRY_SCORE.overall_res, CALC_SCORE.result),
      ENTRY_SCORE.stress = CALC_SCORE.stress
  WHERE ENTRY_SCORE.sport = v_sport
  AND   ENTRY_SCORE.season = v_season
  AND   ENTRY_SCORE.game_id = v_game_id;

END $$

DELIMITER ;

#
# Calculate the entry standings for a given game
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_standings`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_standings`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry standings calculations'
BEGIN

  # Firstly, determine standings for the current score
  INSERT INTO FANTASY_RECORDS_ENTRIES_SCORES (sport, season, game_id, user_id, overall_res, current_res, current_pos)
    SELECT SQL_BIG_RESULT sport, season, game_id, user_id, overall_res, current_res, RANK() OVER w AS current_pos
    FROM FANTASY_RECORDS_ENTRIES_SCORES
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    WINDOW w AS (ORDER BY sport, season, game_id, current_res DESC)
  ON DUPLICATE KEY UPDATE current_pos = VALUES(current_pos);

  # Then, the best score
  INSERT INTO FANTASY_RECORDS_ENTRIES_SCORES (sport, season, game_id, user_id, overall_res, current_res, overall_pos)
    SELECT SQL_BIG_RESULT sport, season, game_id, user_id, overall_res, current_res, RANK() OVER w AS overall_pos
    FROM FANTASY_RECORDS_ENTRIES_SCORES
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    WINDOW w AS (ORDER BY sport, season, game_id, overall_res DESC)
  ON DUPLICATE KEY UPDATE overall_pos = VALUES(overall_pos);

  # Preserve the current instance for the progress/history table
  INSERT INTO FANTASY_RECORDS_ENTRIES_BYPERIOD (sport, season, game_id, user_id, period_id, stress, overall_res, overall_pos, current_res, current_pos)
    SELECT sport, season, game_id, user_id, v_period_id AS period_id, stress, overall_res, overall_pos, current_res, current_pos
    FROM FANTASY_RECORDS_ENTRIES_SCORES
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
  ON DUPLICATE KEY UPDATE overall_res = VALUES(overall_res),
                          overall_pos = VALUES(overall_pos),
                          current_res = VALUES(current_res),
                          current_pos = VALUES(current_pos);

END $$

DELIMITER ;

#
# Determine the per-entry hit rate for their selections
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_hitrate`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_hitrate`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry hit rate calculations'
BEGIN

  # Calculate the per-team hit rate
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_hitrate;
  CREATE TEMPORARY TABLE tmp_entry_hitrate (
    user_id SMALLINT UNSIGNED,
    num_pos SMALLINT UNSIGNED,
    num_neu SMALLINT UNSIGNED,
    num_neg SMALLINT UNSIGNED,
    num_na SMALLINT UNSIGNED,
    tot_sel SMALLINT UNSIGNED,
    tot_valid SMALLINT UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE=MyISAM
    SELECT ENTRIES.user_id,
           SUM(ENTRY_SEL.status = 'positive') AS num_pos,
           SUM(ENTRY_SEL.status = 'neutral') AS num_neu,
           SUM(ENTRY_SEL.status = 'negative') AS num_neg,
           SUM(ENTRY_SEL.status = 'na') AS num_na,
           COUNT(*) AS tot_sel,
           SUM(ENTRY_SEL.status IN ('positive', 'neutral', 'negative')) AS tot_valid
    FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRIES
    LEFT JOIN tmp_entry_sel_full AS ENTRY_SEL
      ON (ENTRY_SEL.user_id = ENTRIES.user_id)
    WHERE ENTRIES.sport = v_sport
    AND   ENTRIES.season = v_season
    AND   ENTRIES.game_id = v_game_id
    GROUP BY ENTRIES.sport, ENTRIES.season, ENTRIES.game_id, ENTRIES.user_id;

  # Rank the teams
  ALTER TABLE tmp_entry_hitrate
    ADD COLUMN pct_pos DECIMAL(6,3) UNSIGNED,
    ADD COLUMN pos SMALLINT UNSIGNED;
  UPDATE tmp_entry_hitrate SET pct_pos = (100 * (num_pos + num_neu)) / tot_valid WHERE tot_valid > 0;

  CALL _duplicate_tmp_table('tmp_entry_hitrate', 'tmp_entry_hitrate_cp');

  INSERT INTO tmp_entry_hitrate (user_id, pct_pos, pos)
    SELECT SQL_BIG_RESULT user_id, pct_pos, RANK() OVER w AS pos
    FROM tmp_entry_hitrate_cp
    WINDOW w AS (ORDER BY IFNULL(pct_pos, -1) DESC)
  ON DUPLICATE KEY UPDATE pos = VALUES(pos);

  # Then store
  INSERT INTO FANTASY_RECORDS_ENTRIES_SCORES (sport, season, game_id, user_id, overall_res, current_res, hitrate_num, hitrate_pos)
    SELECT ENTRY_SCORE.sport, ENTRY_SCORE.season, ENTRY_SCORE.game_id, ENTRY_SCORE.user_id, ENTRY_SCORE.overall_res, ENTRY_SCORE.current_res,
           CONV(CONCAT(
             LPAD(CONV(HITRATE.num_pos, 10, 2), 8, '0'),
             LPAD(CONV(HITRATE.num_neu, 10, 2), 8, '0'),
             LPAD(CONV(HITRATE.num_neg, 10, 2), 8, '0'),
             LPAD(CONV(HITRATE.num_na, 10, 2), 8, '0')
           ), 2, 10) AS hitrate_num,
           HITRATE.pos AS hitrate_pos
    FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
    JOIN tmp_entry_hitrate AS HITRATE
      ON (HITRATE.user_id = ENTRY_SCORE.user_id)
    WHERE ENTRY_SCORE.sport = v_sport
    AND   ENTRY_SCORE.season = v_season
    AND   ENTRY_SCORE.game_id = v_game_id
  ON DUPLICATE KEY UPDATE hitrate_num = VALUES(hitrate_num),
                          hitrate_pos = VALUES(hitrate_pos);

  INSERT INTO FANTASY_RECORDS_ENTRIES_BYPERIOD (sport, season, game_id, user_id, period_id, hitrate_num, hitrate_pos)
    SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id, user_id, v_period_id AS period_id,
           CONV(CONCAT(
             LPAD(CONV(num_pos, 10, 2), 8, '0'),
             LPAD(CONV(num_neu, 10, 2), 8, '0'),
             LPAD(CONV(num_neg, 10, 2), 8, '0'),
             LPAD(CONV(num_na, 10, 2), 8, '0')
           ), 2, 10) AS hitrate_num,
           pos AS hitrate_pos
    FROM tmp_entry_hitrate
  ON DUPLICATE KEY UPDATE hitrate_num = VALUES(hitrate_num),
                          hitrate_pos = VALUES(hitrate_pos);

END $$

DELIMITER ;

#
# Determine the scores about to "drop off" for games with an active selection limit
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_dropoff`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_dropoff`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_active_limit TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry link dropoff calculations'
BEGIN

  DECLARE v_rem_period TINYINT UNSIGNED;

  # Determine the number of remaining periods
  SELECT COUNT(DISTINCT REM_PERIOD.period_id) INTO v_rem_period
  FROM FANTASY_COMMON_PERIODS_DATES AS CURR_PERIOD
  LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS REM_PERIOD
    ON (REM_PERIOD.sport = CURR_PERIOD.sport
    AND REM_PERIOD.season = CURR_PERIOD.season
    AND REM_PERIOD.period_order > CURR_PERIOD.period_order)
  WHERE CURR_PERIOD.sport = v_sport
  AND   CURR_PERIOD.season = v_season
  AND   CURR_PERIOD.period_id = v_period_id;

  # Determine our per-entry dropoff
  INSERT INTO FANTASY_RECORDS_ENTRIES_DROPOFF (`sport`, `season`, `game_id`, `user_id`, `score4`, `score3`, `score2`, `score1`, `score0`)
    SELECT v_sport, v_season, v_game_id, ENTRY_SEL.user_id,
       GROUP_CONCAT(IF(v_rem_period >= 5 AND ENTRY_SEL.sel_recency = v_active_limit - 4, ENTRY_SEL.score, NULL)) AS score4,
       GROUP_CONCAT(IF(v_rem_period >= 4 AND ENTRY_SEL.sel_recency = v_active_limit - 3, ENTRY_SEL.score, NULL)) AS score3,
       GROUP_CONCAT(IF(v_rem_period >= 3 AND ENTRY_SEL.sel_recency = v_active_limit - 2, ENTRY_SEL.score, NULL)) AS score2,
       GROUP_CONCAT(IF(v_rem_period >= 2 AND ENTRY_SEL.sel_recency = v_active_limit - 1, ENTRY_SEL.score, NULL)) AS score1,
       GROUP_CONCAT(IF(v_rem_period >= 1 AND ENTRY_SEL.sel_recency = v_active_limit, ENTRY_SEL.score, NULL)) AS score0
    FROM tmp_entry_sel AS ENTRY_SEL
    LEFT JOIN FANTASY_AUTOMATED_USER AS AUTO_USER
      ON (ENTRY_SEL.user_id = AUTO_USER.id)
    WHERE ENTRY_SEL.sel_recency >= (v_active_limit - 4)
    AND   AUTO_USER.user_id IS NULL
    GROUP BY ENTRY_SEL.user_id
  ON DUPLICATE KEY UPDATE score4 = VALUES (score4),
                          score3 = VALUES (score3),
                          score2 = VALUES (score2),
                          score1 = VALUES (score1),
                          score0 = VALUES (score0);

  # Prune empty dropoff calcs
  DELETE FROM FANTASY_RECORDS_ENTRIES_DROPOFF
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  AND   score4 IS NULL
  AND   score3 IS NULL
  AND   score2 IS NULL
  AND   score1 IS NULL
  AND   score0 IS NULL;

END $$

DELIMITER ;

#
# Update the user profile details after processing a given game
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_profile`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_profile`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry user profile updates'
BEGIN

  # Some pre-calc info getters
  DECLARE v_game_name VARCHAR(30);
  DECLARE v_score_type ENUM('total','streak','periods');
  DECLARE v_game_restart TINYINT(1) UNSIGNED;
  DECLARE v_target SMALLINT(4) UNSIGNED;
  DECLARE v_target_rel DECIMAL(5,1) UNSIGNED;
  DECLARE v_in_progress TINYINT(1) UNSIGNED;
  DECLARE v_tot_periods TINYINT UNSIGNED;

  SELECT GAME.name, GAME.score_type, FIND_IN_SET('GAME_ENDS_ON_NEGATIVE', GAME.config_flags) = 0 AS game_restart, GAME.target,
         (SUM(IF(PERIOD.period_id = v_period_id, LEAST(period_order, IFNULL(GAME.active_limit, 999)), 0)) / LEAST(COUNT(DISTINCT PERIOD.period_id), IFNULL(GAME.active_limit, 999))) * GAME.target,
         SUM(PERIOD.period_id > v_period_id) > 0,
         COUNT(DISTINCT PERIOD.period_id)
    INTO v_game_name, v_score_type, v_game_restart, v_target, v_target_rel, v_in_progress, v_tot_periods
  FROM FANTASY_RECORDS_GAMES AS GAME
  LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
    ON (PERIOD.sport = GAME.sport
    AND PERIOD.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  # Individual team instances
  DROP TEMPORARY TABLE IF EXISTS tmp_profile_entry;
  CREATE TEMPORARY TABLE tmp_profile_entry LIKE FANTASY_PROFILE_RECORD_TEAMS;
  INSERT INTO tmp_profile_entry (user_id, sport, season, game_id, achieved, score, `rank`, achievements, profile_points)
    SELECT user_id, sport, season, game_id,
           overall_res >= v_target AS achieved,
           overall_res AS score,
           overall_pos AS `rank`,
           achievements,
           NULL AS profile_points
    FROM FANTASY_RECORDS_ENTRIES_SCORES
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   overall_pos IS NOT NULL
  ON DUPLICATE KEY UPDATE achieved = VALUES(achieved),
                          score = VALUES(score),
                          `rank` = VALUES(`rank`),
                          profile_points = VALUES(profile_points);

  # Profile Point calculations depend on the scoring type for the game
  IF v_score_type IN ('total', 'periods') THEN
    # Total: Proportionate total to target
    UPDATE tmp_profile_entry
    SET profile_points = (LEAST(LEAST(GREATEST(100 * (score / v_target_rel), 0), 100), 80) / 80) * 100; # Consider 80% success rate our base for max profile points

  ELSE
    # Streak:
    # - With Restart: Better of success rate and proportionate streak to target
    # - No Restart: How far through the season user managed
    # Determine each user's success rate
    DROP TEMPORARY TABLE IF EXISTS tmp_profile_entry_extra;
    CREATE TEMPORARY TABLE tmp_profile_entry_extra (
      user_id SMALLINT UNSIGNED NOT NULL,
      num_sel TINYINT UNSIGNED DEFAULT 0,
      num_pos TINYINT UNSIGNED DEFAULT 0,
      num_neg TINYINT UNSIGNED DEFAULT 0,
      pct_pos DECIMAL(5,1) UNSIGNED,
      pct_tot DECIMAL(5,1) UNSIGNED,
      score_rel DECIMAL(5,1) UNSIGNED,
      PRIMARY KEY (user_id)
    ) ENGINE=MyISAM
      SELECT ENTRY_SEL.user_id,
             COUNT(*) AS num_sel,
             SUM(SEL.status IN ('positive','neutral')) AS num_pos,
             SUM(SEL.status = 'negative') AS num_neg,
             100 * (SUM(SEL.status IN ('positive','neutral')) / COUNT(*)) AS pct_pos,
             100 * ((SUM(SEL.status IN ('positive','neutral')) + (2 * ((v_tot_periods - SUM(SEL.status IN ('positive','neutral'))) / v_tot_periods))) / v_tot_periods) AS pct_tot, # Apply a graceful scale rewarding more at the start of a streak
             NULL AS score_rel
      FROM FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
      JOIN FANTASY_RECORDS_SELECTIONS AS SEL
        ON (SEL.sport = ENTRY_SEL.sport
        AND SEL.season = ENTRY_SEL.season
        AND SEL.game_id = ENTRY_SEL.game_id
        AND SEL.link_id = ENTRY_SEL.link_id
        AND SEL.period_id = ENTRY_SEL.period_id
        AND SEL.status IN ('positive','neutral','negative'))
      WHERE ENTRY_SEL.sport = v_sport
      AND   ENTRY_SEL.season = v_season
      AND   ENTRY_SEL.game_id = v_game_id
      AND   ENTRY_SEL.link_id IS NOT NULL
      GROUP BY ENTRY_SEL.user_id;

      IF v_game_restart = 1 THEN
        # With restart, so determine which is better for each user
        UPDATE tmp_profile_entry AS ENTRY
        LEFT JOIN tmp_profile_entry_extra AS ENTRY_EXTRA
          ON (ENTRY_EXTRA.user_id = ENTRY.user_id)
        SET ENTRY_EXTRA.score_rel = LEAST(GREATEST(100 * (ENTRY.score / v_target), 0), 100);

        UPDATE tmp_profile_entry AS ENTRY
        LEFT JOIN tmp_profile_entry_extra AS ENTRY_EXTRA
          ON (ENTRY_EXTRA.user_id = ENTRY.user_id)
        SET ENTRY.profile_points = IFNULL(GREATEST(
          ENTRY_EXTRA.score_rel,
          (LEAST(ENTRY_EXTRA.pct_pos, 80) / 80) * 100 # Consider 80% success rate our base for max profile points
        ), 0);
      ELSE
        # No restart, so total progress through the season applies
        UPDATE tmp_profile_entry AS ENTRY
        LEFT JOIN tmp_profile_entry_extra AS ENTRY_EXTRA
          ON (ENTRY_EXTRA.user_id = ENTRY.user_id)
        SET ENTRY.profile_points = ENTRY_EXTRA.pct_tot;
      END IF;
  END IF;

  # Write back
  INSERT INTO FANTASY_PROFILE_RECORD_TEAMS (user_id, sport, season, game_id, achieved, score, `rank`, achievements, profile_points)
    SELECT user_id, sport, season, game_id, achieved, score, `rank`, achievements, profile_points
    FROM tmp_profile_entry
  ON DUPLICATE KEY UPDATE achieved = VALUES(achieved),
                          score = VALUES(score),
                          `rank` = VALUES(`rank`),
                          achievements = VALUES(achievements),
                          profile_points = VALUES(profile_points);

  # Finally update (or create, on first run) our overall game status
  INSERT INTO FANTASY_PROFILE_RECORD_GAMES (sport, season, game_id, game_name, target, num_teams, score_mean, profile_points_mean, in_progress)
    SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id, v_game_name AS game_name, v_target AS target,
           COUNT(*) AS num_teams,
           IFNULL(AVG(score), 0) AS score_mean,
           IFNULL(AVG(profile_points), 0) AS profile_points_mean,
           v_in_progress AS in_progress
    FROM FANTASY_PROFILE_RECORD_TEAMS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
  ON DUPLICATE KEY UPDATE num_teams = VALUES(num_teams),
                          score_mean = VALUES(score_mean),
                          profile_points_mean = VALUES(profile_points_mean),
                          in_progress = VALUES(in_progress);

END $$

DELIMITER ;
