##
## X Game Point Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_pts_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_pts_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for ahl:ind:pts:total'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN pts_value DOUBLE,
    ADD COLUMN pts_gm_value DOUBLE;

  # Calculate the Points and Points/Gm values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, pts_value, pts_gm_value)
    SELECT STAT.player_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(STAT.goals) + SUM(STAT.assists) AS pts_value,
           (SUM(STAT.goals) + SUM(STAT.assists)) / SUM(STAT.gp) AS pts_gm_value
    FROM debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
    JOIN debearco_sports.SPORTS_AHL_PLAYERS_GAME_SKATERS AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id)
    WHERE SCHED.season = v_season
    AND   SCHED.game_type = 'regular'
    AND   SCHED.game_date < v_start_date
    AND   IFNULL(SCHED.status, 'DNP') NOT IN ('DNP', 'PPD', 'CNC')
    GROUP BY STAT.player_id;

  # Now process the individual stat elements
  # 1 - Points
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'points') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = pts_value,
        stat_text = pts_value;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Points/Gm
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'points-per-game') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = pts_gm_value,
        stat_text = FORMAT(pts_gm_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN pts_value,
    DROP COLUMN pts_gm_value;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_pts_total_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_pts_total_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for ahl:ind:pts:total'
BEGIN

  # This uses the same logic as ahl:ind:50in50:total
  CALL records_ahl_ind_50in50_total_opprating(v_sport, v_season, v_game_id, v_period_id, v_start_date);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_pts_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_pts_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for ahl:ind:pts:total'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_ahl_ind_sel_recent_sched(v_season, v_start_date, 40);

  # Opportunity 1: Games Played
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           COUNT(RECENT.game_id) / 40 AS stat,
           COUNT(RECENT.game_id) AS stat_nom,
           40 AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Games Played makes for a more reliable player
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_gp;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_gp;

  # Metric 1: Points / Game
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.gp) > 0, SUM(STAT.goals + STAT.assists) / SUM(STAT.gp), NULL) AS stat,
           SUM(STAT.goals + STAT.assists) AS stat_nom,
           SUM(STAT.gp) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_AHL_PLAYERS_GAME_SKATERS AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', NULL); # More Points make for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_points;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_points;

  # Merge (split opp 35 gp, metrics 35 points, 30 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_opp_gp TINYINT UNSIGNED,
    rating_metric_points TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           OPP_GP.rating AS rating_opp_gp,
           METRIC_POINTS.rating AS rating_metric_points,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(OPP_GP.rating, 0) * 0.35) + (IFNULL(METRIC_POINTS.rating, 0) * 0.35)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.30) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_opportunity_gp AS OPP_GP
      ON (OPP_GP.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_points AS METRIC_POINTS
      ON (METRIC_POINTS.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_pts_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_pts_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for ahl:ind:pts:total'
BEGIN

  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_AHL_PLAYERS_GAME_SKATERS AS STAT
    ON (STAT.season = SEL.season
    AND STAT.player_id = SEL.link_id
    AND STAT.game_type = SEL.game_type
    AND STAT.game_id = SEL.game_id
    AND STAT.gp = 1)
  SET SEL.stat = STAT.goals + STAT.assists,
      SEL.status = IF((STAT.goals + STAT.assists) = 0, 'negative', 'positive'),
      SEL.summary = CONCAT(STAT.goals, ' G, ', STAT.assists, ' A');

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_pts_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_pts_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for ahl:ind:pts:total'
BEGIN

  # Determine when each player scored their first point
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    stress TINYINT UNSIGNED,
    first_by_period TINYINT UNSIGNED,
    first_by_time SMALLINT UNSIGNED,
    first_by_period_time SMALLINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.game_id,
           NULL AS stress,
           MIN(EVENT.period) AS first_by_period,
           MIN((IF(EVENT.period < 4, 1200, 300) - TIME_TO_SEC(EVENT.event_time)) + ((EVENT.period - 1) * 1200)) AS first_by_time,
           NULL AS first_by_period_time
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_AHL_GAME_EVENT_GOAL AS EVENT_GOAL
      ON (EVENT_GOAL.season = SEL.season
      AND EVENT_GOAL.game_type = SEL.game_type
      AND EVENT_GOAL.game_id = SEL.game_id
      AND EVENT_GOAL.by_team_id = SEL.team_id
      AND SEL.jersey IN (EVENT_GOAL.scorer, EVENT_GOAL.assist_1, EVENT_GOAL.assist_2))
    JOIN debearco_sports.SPORTS_AHL_GAME_EVENT AS EVENT
      ON (EVENT.season = EVENT_GOAL.season
      AND EVENT.game_type = EVENT_GOAL.game_type
      AND EVENT.game_id = EVENT_GOAL.game_id
      AND EVENT.event_id = EVENT_GOAL.event_id)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id;

  UPDATE tmp_link_stress
  SET first_by_period_time = first_by_time - (1200 * (first_by_period - 1));

  # Then assign a stress score to each
  UPDATE tmp_link_stress
  SET stress = CASE first_by_period
    WHEN 1 THEN ((first_by_period_time / 1200) * 64)
    WHEN 2 THEN ((first_by_period_time / 1200) * 80) + 64
    WHEN 3 THEN ((first_by_period_time / 1200) * 111) + 144
    ELSE 255
  END;

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
