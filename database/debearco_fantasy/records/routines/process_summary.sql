#
# Calculate summary stats for the completed period
#
DROP PROCEDURE IF EXISTS `records_scoring_summary`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_summary`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker period summary stats'
BEGIN

  DECLARE v_count_entry INT UNSIGNED;
  DECLARE v_count_sel INT UNSIGNED;
  DECLARE v_entry_pos_pct DECIMAL(5,2) UNSIGNED DEFAULT NULL;
  DECLARE v_sel_pos_pct DECIMAL(5,2) UNSIGNED DEFAULT NULL;
  DECLARE v_popular_pos_link_id SMALLINT UNSIGNED DEFAULT NULL;
  DECLARE v_popular_pos_pct DECIMAL(5,2) UNSIGNED DEFAULT NULL;
  DECLARE v_popular_neg_link_id SMALLINT UNSIGNED DEFAULT NULL;
  DECLARE v_popular_neg_pct DECIMAL(5,2) UNSIGNED DEFAULT NULL;

  # Our denominator counts
  SELECT COUNT(*) INTO v_count_entry
  FROM tmp_entry_sel
  WHERE period_id = v_period_id
  AND   link_id > 0;

  SELECT COUNT(*) INTO v_count_sel
  FROM tmp_links;

  # Entries with a positive score
  IF v_count_entry > 0 THEN
    SELECT 100 * (SUM(status IN ('positive', 'neutral')) / v_count_entry) INTO v_entry_pos_pct
    FROM tmp_entry_sel
    WHERE period_id = v_period_id;
  END IF;

  # Selections with a positive score
  IF v_count_sel > 0 THEN
    SELECT 100 * (SUM(status IN ('positive', 'neutral')) / v_count_sel) INTO v_sel_pos_pct
    FROM tmp_links;
  END IF;

  # Most popular selections
  IF v_count_entry > 0 THEN
    # Most popular 'positive' selection
    SELECT link_id, 100 * (COUNT(*) / v_count_entry) INTO v_popular_pos_link_id, v_popular_pos_pct
    FROM tmp_entry_sel
    WHERE period_id = v_period_id
    AND   status IN ('positive', 'neutral')
    AND   link_id > 0
    GROUP BY link_id
    ORDER BY COUNT(*) DESC
    LIMIT 1;

    # Most popular 'negative' selection
    SELECT link_id, 100 * (COUNT(*) / v_count_entry) INTO v_popular_neg_link_id, v_popular_neg_pct
    FROM tmp_entry_sel
    WHERE period_id = v_period_id
    AND   status = 'negative'
    AND   link_id > 0
    GROUP BY link_id
    ORDER BY COUNT(*) DESC
    LIMIT 1;
  END IF;

  # Store
  INSERT INTO FANTASY_RECORDS_GAMES_PERIODS_SUMMARY (sport, season, game_id, period_id, entry_pos_pct, sel_pos_pct, popular_pos_link_id, popular_pos_pct, popular_neg_link_id, popular_neg_pct)
    VALUES (v_sport, v_season, v_game_id, v_period_id, v_entry_pos_pct, v_sel_pos_pct, v_popular_pos_link_id, v_popular_pos_pct, v_popular_neg_link_id, v_popular_neg_pct);

END $$

DELIMITER ;
