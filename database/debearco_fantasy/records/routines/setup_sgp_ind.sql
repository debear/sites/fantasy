#
# Selections
#
DROP PROCEDURE IF EXISTS `records_sgp_ind_sel`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_sgp_ind_sel`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selection lists for sgp:ind'
BEGIN

  ALTER TABLE tmp_links
    ADD COLUMN rider_type ENUM('regular', 'wildcard', 'track_reserve', 'substitute', 'unknown'),
    ADD COLUMN status_multiplier DECIMAL(3,2) UNSIGNED;

  # Now populate
  TRUNCATE TABLE tmp_links;
  INSERT INTO tmp_links (link_id, link_sort, rider_type, status_multiplier)
    SELECT RIDER.rider_id AS link_id,
           debearco_sports.fn_basic_html_comparison(CONCAT(RIDER.surname, ',', RIDER.first_name)) AS link_sort,
           CASE RACE_RIDER.status
            WHEN 'wc'   THEN 'wildcard'
            WHEN 'trsv' THEN 'track_reserve'
            WHEN 'sub'  THEN 'substitute'
            ELSE             'regular'
           END AS rider_type,
           IF(SEL_STATUS.status IN ('ne', 'out', 'susp'), 0.15, 1.00) AS status_multiplier
    FROM debearco_sports.SPORTS_SGP_RACE_RIDERS AS RACE_RIDER
    JOIN debearco_sports.SPORTS_SPEEDWAY_RIDERS AS RIDER
      ON (RIDER.rider_id = RACE_RIDER.rider_id)
    LEFT JOIN FANTASY_COMMON_MOTORS_SELECTIONS_STATUS AS SEL_STATUS
      ON (SEL_STATUS.sport = v_sport
      AND SEL_STATUS.season = RACE_RIDER.season
      AND SEL_STATUS.period_id = RACE_RIDER.round
      AND SEL_STATUS.link_id = RACE_RIDER.rider_id)
    WHERE RACE_RIDER.season = v_season
    AND   RACE_RIDER.round = v_period_id;

  # Include selections that are not expected to be entered, but linked via status
  INSERT IGNORE INTO tmp_links (link_id, link_sort, rider_type, status_multiplier)
    SELECT SEL_STATUS.link_id,
           debearco_sports.fn_basic_html_comparison(CONCAT(RIDER.surname, ',', RIDER.first_name))  AS link_sort,
           'unknown' AS rider_type,
           IF(SEL_STATUS.status IN ('ne', 'out', 'susp'), 0.15, 1.00) AS status_multiplier
    FROM FANTASY_COMMON_MOTORS_SELECTIONS_STATUS AS SEL_STATUS
    LEFT JOIN debearco_sports.SPORTS_SPEEDWAY_RIDERS AS RIDER
      ON (RIDER.rider_id = SEL_STATUS.link_id)
    WHERE SEL_STATUS.sport = v_sport
    AND   SEL_STATUS.season = v_season
    AND   SEL_STATUS.period_id = v_period_id;

END $$

DELIMITER ;

#
# Determine which "events" (meetings) each selection could have played
#
DROP PROCEDURE IF EXISTS `records_sgp_ind_events`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_sgp_ind_events`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection games played for sgp:ind sel'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_link_events;
  CREATE TEMPORARY TABLE tmp_link_events (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    round TINYINT UNSIGNED,
    event_order SMALLINT UNSIGNED,
    bib_no SMALLINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    stress TINYINT UNSIGNED,
    summary VARCHAR(50),
    PRIMARY KEY (link_id, season, round)
  ) ENGINE=MyISAM
    SELECT tmp_links.link_id, RACE_RIDER.season, RACE_RIDER.round, 1 AS event_order, RACE_RIDER.bib_no,
           NULL AS stat, 'na' AS status, NULL AS stress, NULL AS summary
    FROM tmp_links
    JOIN debearco_sports.SPORTS_SGP_RACES AS RACE
      ON (RACE.season = v_season
      AND RACE.round = v_period_id)
    JOIN debearco_sports.SPORTS_SGP_RACE_RIDERS AS RACE_RIDER
      ON (RACE_RIDER.season = RACE.season
      AND RACE_RIDER.round = RACE.round
      AND RACE_RIDER.rider_id = tmp_links.link_id);

END $$

DELIMITER ;
