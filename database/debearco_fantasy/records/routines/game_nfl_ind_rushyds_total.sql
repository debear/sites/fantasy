##
## X Yard Rushing Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_rushyds_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_rushyds_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for nfl:ind:rushyd:total'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN rushyds_value DOUBLE,
    ADD COLUMN rushyds_gm_value DOUBLE;

  # Calculate the Rush Yds and Rush Yds/Gm values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, rushyds_value, rushyds_gm_value)
    SELECT STAT.player_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(STAT.yards) AS rushyds_value,
           SUM(STAT.yards) / COUNT(DISTINCT STAT.game_id) AS rushyds_gm_value
    FROM debearco_sports.SPORTS_NFL_SCHEDULE AS SCHED
    JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.week = SCHED.week
      AND STAT.game_id = SCHED.game_id)
    WHERE SCHED.season = v_season
    AND   SCHED.game_type = 'regular'
    AND   SCHED.game_date < v_start_date
    AND   SCHED.status IS NOT NULL
    GROUP BY STAT.player_id;

  # Now process the individual stat elements
  # 1 - Rush Yds
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'rush-yds') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = rushyds_value,
        stat_text = rushyds_value;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Rush Yds/Gm
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'rush-yds-per-game') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = rushyds_gm_value,
        stat_text = FORMAT(rushyds_gm_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN rushyds_value,
    DROP COLUMN rushyds_gm_value;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_rushyds_total_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_rushyds_total_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for nfl:ind:rushyd:total'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 17);

  # Calc 1: Rushing Yards Allowed
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(6, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(6, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(6, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.yards_rush, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.yards_rush, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_STATS_YARDS AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.3, 'DESC'); # 70/30 Overall-vs-Home/Road, More Yards Allowed makes for a better opponent

  # Store
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_rushyds_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_rushyds_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for nfl:ind:rushyd:total'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_nfl_ind_sel_recent_sched(v_season, v_start_date, 17);

  # Opportunity 1: Depth Chart (direct rating calc)
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_depth;
  CREATE TEMPORARY TABLE tmp_rating_opportunity_depth (
    link_id SMALLINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           100 * MAX(DEPTH.season IS NOT NULL) AS rating
    FROM tmp_links AS LINK
    LEFT JOIN debearco_sports.SPORTS_NFL_TEAMS_DEPTH AS DEPTH
      ON (DEPTH.season = v_season
      AND DEPTH.player_id = LINK.link_id
      AND DEPTH.game_type = 'regular'
      AND DEPTH.week = v_period_id
      AND DEPTH.depth = 1)
    GROUP BY LINK.link_id;

  # Opportunity 2: Atts / Gm
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(STAT.atts) AS stat,
           SUM(STAT.atts) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Attempts make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_atts;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_atts;

  # Opportunity 3: Time of Possession
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat SMALLINT UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(TIME_TO_SEC(STAT.time_of_poss)) AS stat,
           SUM(TIME_TO_SEC(STAT.time_of_poss)) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_STATS_MISC AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id
      AND STAT.team_id = RECENT.team_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Time of Possession makes for a better rushing opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_time;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_time;

  # Metric 1: Yards / Att
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.atts) > 0, LEAST(99.99999, POW(GREATEST(SUM(STAT.yards), 0), 0.8) / POW(SUM(STAT.atts), 0.4)), NULL) AS stat,
           GREATEST(SUM(STAT.yards), 0) AS stat_nom,
           SUM(STAT.atts) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', 'calc_ypa,apply_least'); # Higher Yardage makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_yards;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_yards;

  # Merge (split opportunity 20/20/15 Depth/Atts/Time, metrics 25 yardage, 20 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_opp_depth TINYINT UNSIGNED,
    rating_opp_atts TINYINT UNSIGNED,
    rating_opp_time TINYINT UNSIGNED,
    rating_metric_yards TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           OPP_DEPTH.rating AS rating_opp_depth,
           OPP_ATTS.rating AS rating_opp_atts,
           OPP_TIME.rating AS rating_opp_time,
           METRIC_YARDS.rating AS rating_metric_yards,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(OPP_DEPTH.rating, 0) * 0.20) + (IFNULL(OPP_ATTS.rating, 0) * 0.20) + (IFNULL(OPP_TIME.rating, 0) * 0.15)
             + (IFNULL(METRIC_YARDS.rating, 0) * 0.25)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.20) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_opportunity_depth AS OPP_DEPTH
      ON (OPP_DEPTH.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_atts AS OPP_ATTS
      ON (OPP_ATTS.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_time AS OPP_TIME
      ON (OPP_TIME.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_yards AS METRIC_YARDS
      ON (METRIC_YARDS.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_rushyds_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_rushyds_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for nfl:ind:rushyd:total'
BEGIN

  # Determine what an "acceptable" value is, based on the Yds/GP required
  DECLARE v_per_game_lo DECIMAL(6,3) UNSIGNED;
  DECLARE v_per_game_hi DECIMAL(6,3) UNSIGNED;
  SELECT 0.75 * (GAME.target / COUNT(DATES.period_id)), GAME.target / COUNT(DATES.period_id)
    INTO v_per_game_lo, v_per_game_hi
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
    ON (DATES.sport = GAME.sport
    AND DATES.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  # Now perform the update
  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING AS STAT
    ON (STAT.season = SEL.season
    AND STAT.game_type = SEL.game_type
    AND STAT.week = SEL.week
    AND STAT.game_id = SEL.game_id
    AND STAT.player_id = SEL.link_id)
  SET SEL.stat = STAT.yards,
      SEL.status = CASE
        WHEN STAT.yards < v_per_game_lo THEN 'negative'
        WHEN STAT.yards < v_per_game_hi THEN 'neutral'
        ELSE 'positive'
      END,
      SEL.summary = CONCAT(pluralise(STAT.yards, ' yd'));

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_rushyds_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_rushyds_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for nfl:ind:rushyds:total'
BEGIN

  DECLARE v_target DECIMAL(6,3) UNSIGNED;
  DECLARE v_mean_rush_ratio DECIMAL(6,5) UNSIGNED;
  DECLARE v_target_rush_offset SMALLINT UNSIGNED;
  DECLARE v_target_rush_ypc DECIMAL(7,5) SIGNED;

  # Determine the per-game target
  SELECT GAME.target / COUNT(DATES.period_id) INTO v_target
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
    ON (DATES.sport = GAME.sport
    AND DATES.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  # Determine league average (mean) pass/rush ratios over the last three seasons
  SELECT AVG(num_rush / (num_pass_att + num_rush)) INTO v_mean_rush_ratio
  FROM debearco_sports.SPORTS_NFL_GAME_STATS_PLAYS
  WHERE season >= (v_season - 2)
  AND   game_type = 'regular';

  # Determine league average (mean) yards/carry over the last three seasons
  SELECT ROUND(COUNT(DISTINCT atts) * 0.15) INTO v_target_rush_offset
  FROM debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING
  WHERE season >= (v_season - 2)
  AND   game_type = 'regular'
  AND   atts > 0;

  SELECT v_target / atts INTO v_target_rush_ypc
  FROM debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING
  WHERE season >= (v_season - 2)
  AND   game_type = 'regular'
  AND   atts > 0
  GROUP BY atts
  ORDER BY atts DESC
  LIMIT v_target_rush_offset, 1;

  # Establish each player's info, include their team's run/pass play split
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    week TINYINT UNSIGNED,
    game_id SMALLINT UNSIGNED,
    rush_att TINYINT UNSIGNED,
    rush_yds SMALLINT SIGNED,
    rush_att_ratio DECIMAL(6,5) UNSIGNED,
    rush_ypc DECIMAL(8,5) SIGNED,
    team_pass TINYINT UNSIGNED,
    team_rush TINYINT UNSIGNED,
    team_rush_ratio DECIMAL(6,5) UNSIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.week, SEL.game_id,
           GAME_RUSH.atts AS rush_att, GAME_RUSH.yards AS rush_yds, GAME_RUSH.atts / TEAM_PLAYS.num_rush AS rush_att_ratio, GAME_RUSH.yards / GAME_RUSH.atts AS rush_ypc,
           TEAM_PLAYS.num_pass_att AS team_pass, TEAM_PLAYS.num_rush AS team_rush, TEAM_PLAYS.num_rush / (TEAM_PLAYS.num_pass_att + TEAM_PLAYS.num_rush) AS team_rush_ratio,
           NULL AS stress
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_RUSHING AS GAME_RUSH
      ON (GAME_RUSH.season = SEL.season
      AND GAME_RUSH.game_type = SEL.game_type
      AND GAME_RUSH.week = SEL.week
      AND GAME_RUSH.game_id = SEL.game_id
      AND GAME_RUSH.player_id = SEL.link_id
      AND GAME_RUSH.atts > 0)
    JOIN debearco_sports.SPORTS_NFL_GAME_STATS_PLAYS AS TEAM_PLAYS
      ON (TEAM_PLAYS.season = SEL.season
      AND TEAM_PLAYS.game_type = SEL.game_type
      AND TEAM_PLAYS.week = SEL.week
      AND TEAM_PLAYS.game_id = SEL.game_id
      AND TEAM_PLAYS.team_id = SEL.team_id)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.week, SEL.game_id;

  # Then assign a stress score to each player based on their total and rushes against the team calcs
  UPDATE tmp_link_stress
  SET stress = LEAST(255,
      ((1 - ((GREATEST(LEAST(rush_yds, v_target) / v_target, 0.5) - 0.5) * 2)) * 255)
    + ((1 - (LEAST(v_target_rush_ypc, rush_ypc) / v_target_rush_ypc)) * 20)
    + ((1 - rush_att_ratio) * 20)
    + ((1 - ((GREATEST(LEAST(v_mean_rush_ratio, team_rush_ratio), 0.2) - 0.2) / (v_mean_rush_ratio - 0.2))) * 15)
  );

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.week = SEL.week
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
