#
# Selections
#
DROP PROCEDURE IF EXISTS `records_nhl_team_sel`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_team_sel`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selection lists for nhl:team'
BEGIN

  DECLARE v_sort_base SMALLINT UNSIGNED;
  DECLARE v_end_date DATE;

  SELECT end_date INTO v_end_date
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  AND   period_id = v_period_id;

  # Make use of our "mapped" table
  SELECT MIN(link_id) INTO v_sort_base
  FROM FANTASY_COMMON_SELECTIONS_MAPPED
  WHERE sport = v_sport
  AND   season = v_season;

  # Now populate
  ALTER TABLE tmp_links
    ADD COLUMN raw_id CHAR(3),
    ADD COLUMN opp_team_id CHAR(3),
    ADD COLUMN is_home TINYINT UNSIGNED,
    ADD COLUMN status_multiplier DECIMAL(3,2) UNSIGNED;

  TRUNCATE TABLE tmp_links;
  INSERT INTO tmp_links (link_id, link_sort, raw_id, opp_team_id, is_home, status_multiplier)
    SELECT SEL_MAP.link_id, SEL_MAP.link_id - v_sort_base + 1 AS link_sort,
           SEL_MAP.raw_id,
           IF(SEL_MAP.raw_id = SCHED.home, SCHED.visitor, SCHED.home) AS opp_team_id,
           SEL_MAP.raw_id = SCHED.home AS is_home,
           1.00 AS status_multiplier
    FROM FANTASY_COMMON_SELECTIONS_MAPPED AS SEL_MAP
    JOIN debearco_sports.SPORTS_NHL_SCHEDULE AS SCHED
      ON (SCHED.season = SEL_MAP.season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_date BETWEEN v_start_date AND v_end_date
      AND SEL_MAP.raw_id IN (SCHED.home, SCHED.visitor))
    WHERE SEL_MAP.sport = v_sport
    AND   SEL_MAP.season = v_season
    GROUP BY SEL_MAP.link_id;

END $$

DELIMITER ;

#
# Selection recent games
#
DROP PROCEDURE IF EXISTS `records_nhl_team_sel_recent_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_team_sel_recent_sched`(
  v_season SMALLINT UNSIGNED,
  v_start_date DATE,
  v_num_games TINYINT UNSIGNED
)
    COMMENT 'Record Breaker selection recent games for nhl:team'
BEGIN

  # Start by looking back at all games over the last two seasons
  # This works on the expectation v_num_games does go beyond the start of the previous season
  DROP TEMPORARY TABLE IF EXISTS tmp_links_recent;
  CREATE TEMPORARY TABLE tmp_links_recent (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    game_time MEDIUMINT UNSIGNED,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    is_home TINYINT UNSIGNED,
    recency_order SMALLINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id),
    KEY by_link_game (season, link_id, game_type, game_id),
    KEY by_link_time (link_id, game_time)
  ) ENGINE=MyISAM
    SELECT LINK.link_id, SCHED.season, SCHED.game_type, SCHED.game_id,
           ((YEAR(SCHED.game_date) - (v_season - 2)) * 1000000)
            + (CAST(DATE_FORMAT(CONCAT(SCHED.game_date, ' ', SCHED.game_time), '%j') AS UNSIGNED) * 1000)
            + (CAST(DATE_FORMAT(CONCAT(SCHED.game_date, ' ', SCHED.game_time), '%l') AS UNSIGNED) * 60)
            + CAST(DATE_FORMAT(CONCAT(SCHED.game_date, ' ', SCHED.game_time), '%i') AS UNSIGNED) AS game_time,
           LINK.raw_id AS team_id, IF(LINK.raw_id = SCHED.home, SCHED.visitor, SCHED.home) AS opp_team_id,
           LINK.raw_id = SCHED.home AS is_home,
           NULL AS recency_order
    FROM tmp_links AS LINK
    JOIN debearco_sports.SPORTS_NHL_SCHEDULE AS SCHED
      ON (SCHED.season IN (v_season - 1, v_season)
      AND SCHED.game_type = 'regular'
      AND LINK.raw_id IN (SCHED.home, SCHED.visitor)
      AND SCHED.game_date < v_start_date
      AND IFNULL(SCHED.status, 'STILLTOPLAY') NOT IN ('PPD', 'CNC', 'STILLTOPLAY'));

  # Fill the recency_order column
  CALL _duplicate_tmp_table('tmp_links_recent', 'tmp_links_recent_cpA');
  INSERT INTO tmp_links_recent (link_id, season, game_type, game_id, recency_order)
    SELECT SQL_BIG_RESULT link_id, season, game_type, game_id,
           ROW_NUMBER() OVER w AS recency_order
    FROM tmp_links_recent_cpA
    WINDOW w AS (PARTITION BY link_id ORDER BY link_id, game_time DESC)
  ON DUPLICATE KEY UPDATE recency_order = VALUES(recency_order);
  DROP TEMPORARY TABLE tmp_links_recent_cpA;

  # Limit the table to our requested size
  DELETE FROM tmp_links_recent WHERE recency_order > v_num_games;

END $$

DELIMITER ;

#
# Determine which "events" (games) each selection could have played
#
DROP PROCEDURE IF EXISTS `records_nhl_team_events`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_team_events`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection games played for nhl:team sel'
BEGIN

  # If we haven't been provided the date range, get this info out
  IF v_start_date IS NULL OR v_end_date IS NULL THEN
    SELECT start_date, end_date INTO v_start_date, v_end_date
    FROM FANTASY_COMMON_PERIODS_DATES
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_id = v_period_id;
  END IF;

  DROP TEMPORARY TABLE IF EXISTS tmp_link_events;
  CREATE TEMPORARY TABLE tmp_link_events (
    link_id SMALLINT UNSIGNED,
    raw_id CHAR(5) DEFAULT NULL,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    event_order SMALLINT UNSIGNED,
    team_gf TINYINT UNSIGNED,
    team_ga TINYINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    stress TINYINT UNSIGNED,
    summary VARCHAR(50),
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE=MyISAM
    SELECT tmp_links.link_id, tmp_links.raw_id, SCHED.season, SCHED.game_type, SCHED.game_id, FLOOR(TIME_TO_SEC(SCHED.game_time) / 60) AS event_order,
           IF(tmp_links.raw_id = SCHED.home, SCHED.home_score, SCHED.visitor_score) AS team_gf,
           IF(tmp_links.raw_id = SCHED.home, SCHED.visitor_score, SCHED.home_score) AS team_ga,
           NULL AS stat, 'na' AS status, NULL AS stress, NULL AS summary
    FROM tmp_links
    JOIN debearco_sports.SPORTS_NHL_SCHEDULE AS SCHED
      ON (SCHED.season = v_season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_date BETWEEN v_start_date AND v_end_date
      AND tmp_links.raw_id IN (SCHED.home, SCHED.visitor));

END $$

DELIMITER ;

#
# Determine which "events" (games) each selection could have played across a variety of periods
#
DROP PROCEDURE IF EXISTS `records_nhl_team_events_historical`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nhl_team_events_historical`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker historical games played for nhl:team sel'
BEGIN

  # Get the raw list of events for the requested selections in the appropriate period(s)
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_events_raw;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_events_raw (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE=MyISAM
    SELECT tmp_entry_sel_selperiod.link_id, tmp_entry_sel_selperiod.period_id,
           SCHED.season, SCHED.game_type, SCHED.game_id,
           SEL_MAP.raw_id AS team_id, IF(SEL_MAP.raw_id = SCHED.home, SCHED.visitor, SCHED.home) AS opp_team_id
    FROM tmp_entry_sel_selperiod
    JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
      ON (PERIOD.sport = v_sport
      AND PERIOD.season = v_season
      AND PERIOD.period_id = tmp_entry_sel_selperiod.period_id)
    JOIN FANTASY_COMMON_SELECTIONS_MAPPED AS SEL_MAP
      ON (SEL_MAP.sport = PERIOD.sport
      AND SEL_MAP.season = PERIOD.season
      AND SEL_MAP.link_id = tmp_entry_sel_selperiod.link_id)
    JOIN debearco_sports.SPORTS_NHL_SCHEDULE AS SCHED
      ON (SCHED.season = PERIOD.season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_date BETWEEN PERIOD.start_date AND PERIOD.end_date
      AND SEL_MAP.raw_id IN (SCHED.home, SCHED.visitor));

END $$

DELIMITER ;
