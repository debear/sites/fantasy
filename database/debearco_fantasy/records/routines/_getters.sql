##
## ID Getters
##

DROP FUNCTION IF EXISTS `records_get_stat_id`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `records_get_stat_id`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_stat_code VARCHAR(30)
) RETURNS TINYINT UNSIGNED
  DETERMINISTIC
  COMMENT 'Return the Stat ID, if it exists, for a given game stat code'
BEGIN

  DECLARE v_return TINYINT UNSIGNED DEFAULT NULL;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_return = NULL;

  SELECT stat_id INTO v_return
  FROM FANTASY_RECORDS_GAMES_STATS
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  AND   code = v_stat_code
  LIMIT 1;

  RETURN v_return;

END $$

DELIMITER ;
