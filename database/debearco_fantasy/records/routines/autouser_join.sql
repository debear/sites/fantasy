##
## Seed a game with entries from automated users
##
DROP PROCEDURE IF EXISTS `records_autouser_join`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_join`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_pct_min TINYINT UNSIGNED,
  v_pct_max TINYINT UNSIGNED
)
    COMMENT 'Record Breaker automated user entry seeding'
BEGIN

  DECLARE v_num_entries SMALLINT UNSIGNED;

  # Determine the number of entries we will need to create
  SELECT ROUND(COUNT(*) * ((v_pct_min + (RAND() * (v_pct_max - v_pct_min))) / 100)) INTO v_num_entries
  FROM FANTASY_AUTOMATED_USER;

  # Create that many entries for this game
  DROP TEMPORARY TABLE IF EXISTS tmp_new_join;
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_new_join (
    id SMALLINT UNSIGNED,
    PRIMARY KEY (id)
  ) ENGINE = MyISAM
    SELECT id
    FROM FANTASY_AUTOMATED_USER
    ORDER BY RAND() * interest_', v_sport, ' DESC
    LIMIT ', v_num_entries, ';'));

  INSERT INTO FANTASY_RECORDS_ENTRIES_SCORES (sport, season, game_id, user_id, overall_res, current_res)
    SELECT v_sport, v_season, v_game_id, id, 0, 0
    FROM tmp_new_join;

  # Backfill any missing entries to the general Record Breakers entry table
  DROP TEMPORARY TABLE IF EXISTS tmp_new_entry;
  CREATE TEMPORARY TABLE tmp_new_entry (
    id SMALLINT UNSIGNED,
    PRIMARY KEY (id)
  ) ENGINE = MyISAM
    SELECT AUTOUSER.id
    FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
    LEFT JOIN FANTASY_RECORDS_ENTRIES AS ENTRY
      ON (ENTRY.user_id = ENTRY_SCORE.user_id)
    JOIN FANTASY_AUTOMATED_USER AS AUTOUSER
      ON (AUTOUSER.id = ENTRY_SCORE.user_id)
    WHERE ENTRY_SCORE.sport = v_sport
    AND   ENTRY_SCORE.season = v_season
    AND   ENTRY_SCORE.game_id = v_game_id
    AND   ENTRY.user_id IS NULL;

  INSERT IGNORE INTO FANTASY_RECORDS_ENTRIES (user_id, name, joined, email_status, email_type)
    SELECT AUTOUSER.id, AUTOUSER.team_name, CURDATE(), 0, 'text'
    FROM tmp_new_entry
    JOIN FANTASY_AUTOMATED_USER AS AUTOUSER
      ON (AUTOUSER.id = tmp_new_entry.id);

  # Create approrpiate audit details
  IF records_config('audit_autouser') THEN
    # New entries created
    INSERT INTO debearco_admin.USER_ACTIVITY_LOG (activity_id, activity_time, type, user_id, summary, detail)
      SELECT NULL, NOW(), 40, id, 'Automated user entry created',
        CONCAT('{"source": "', v_sport, '-', v_season, '-', v_game_id, '"}')
      FROM tmp_new_entry;

    # Entries joining the game
    INSERT INTO debearco_admin.USER_ACTIVITY_LOG (activity_id, activity_time, type, user_id, summary, detail)
      SELECT NULL, NOW(), 43, id, 'Automated user joined game',
        CONCAT('{"game_ref": "', v_sport, '-', v_season, '-', v_game_id, '"}')
      FROM tmp_new_join;
  END IF;

END $$

DELIMITER ;
