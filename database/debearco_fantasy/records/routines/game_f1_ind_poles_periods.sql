##
## X Pole Position Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_poles_periods_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_poles_periods_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for f1:ind:poles:periods'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN start_value DOUBLE,
    ADD COLUMN q3_value DOUBLE;

  # Calculate the Avg Start and Q3 Appearances
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, start_value, q3_value)
    SELECT RACE_DRIVER.driver_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(RACE_GRID.qual_pos) / COUNT(RACE_GRID.season) AS start_value,
           COUNT(DISTINCT RACE_Q3.round) AS q3_value
    FROM debearco_sports.SPORTS_FIA_RACES AS RACE
    JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS RACE_GRID
      ON (RACE_GRID.season = RACE_DRIVER.season
      AND RACE_GRID.series = RACE_DRIVER.series
      AND RACE_GRID.round = RACE_DRIVER.round
      AND RACE_GRID.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_GRID.car_no = RACE_DRIVER.car_no
      AND RACE_GRID.qual_pos IS NOT NULL)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS RACE_Q3
      ON (RACE_Q3.season = RACE_DRIVER.season
      AND RACE_Q3.series = RACE_DRIVER.series
      AND RACE_Q3.round = RACE_DRIVER.round
      AND RACE_Q3.session = 'q3'
      AND RACE_Q3.car_no = RACE_DRIVER.car_no)
    WHERE RACE.season = v_season
    AND   RACE.series = v_sport
    AND   DATE(RACE.race_time) < v_start_date
    AND   RACE.race_laps_completed IS NOT NULL
    GROUP BY RACE_DRIVER.driver_id;

  # Now process the individual stat elements
  # 1 - Avg Start
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'avg-start') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = start_value,
        stat_text = FORMAT(start_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'ASC');
  END IF;
  # 2 - Q3 App
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'q3-app') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = q3_value,
        stat_text = q3_value;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN start_value,
    DROP COLUMN q3_value;

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_poles_periods_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_poles_periods_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for f1:ind:poles:periods'
BEGIN

  # Run our generalised helper setup
  CALL records_f1_selrating_setup(v_season, v_start_date);

  # Team calcs over these races
  CALL records_f1_ind_poles_periods_selrating_team();

  # Then driver calcs, compared to their teammate
  CALL records_f1_ind_poles_periods_selrating_driver();

  # Process via the generalised F1 helper
  CALL records_f1_selrating_proc(v_sport, v_season, v_game_id, v_period_id, 1.0, 0.6, 'DESC');

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_poles_periods_selrating_team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_poles_periods_selrating_team`()
    COMMENT 'Record Breaker selrating team calcs for f1:ind:poles:periods'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_team_results;
  CREATE TEMPORARY TABLE tmp_team_results (
    team_id TINYINT UNSIGNED,
    pole_ratio DECIMAL(5, 4) UNSIGNED,
    front_row_ratio DECIMAL(5, 4) UNSIGNED,
    q3_ratio DECIMAL(5, 4) UNSIGNED,
    avg_qual DECIMAL(6, 4) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MyISAM
    SELECT RACE_DRIVER.team_id,
           SUM((RACE_GRID.grid_pos = 1) / RACE.race_multiplier) / COUNT(DISTINCT CONCAT(RACE.season, '-', RACE.round)) AS pole_ratio,
           AVG((RACE_GRID.grid_pos IN (1, 2)) / RACE.race_multiplier) AS front_row_ratio,
           AVG((RACE_Q3.pos IS NOT NULL) / RACE.race_multiplier) AS q3_ratio,
           AVG(RACE_GRID.qual_pos * RACE.race_multiplier) AS avg_qual
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS RACE_GRID
      ON (RACE_GRID.season = RACE_DRIVER.season
      AND RACE_GRID.series = RACE_DRIVER.series
      AND RACE_GRID.round = RACE_DRIVER.round
      AND RACE_GRID.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_GRID.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS RACE_Q3
      ON (RACE_Q3.season = RACE_DRIVER.season
      AND RACE_Q3.series = RACE_DRIVER.series
      AND RACE_Q3.round = RACE_DRIVER.round
      AND RACE_Q3.session = 'q3'
      AND RACE_Q3.car_no = RACE_DRIVER.car_no)
    GROUP BY RACE_DRIVER.team_id;

  # Merge the components into a single team rating
  ALTER TABLE tmp_team_results
    ADD COLUMN avg_rtg DECIMAL(6, 4) UNSIGNED;
  UPDATE tmp_team_results SET avg_rtg = (pole_ratio * 7) + (front_row_ratio * 4) + (q3_ratio * 3) + (5 - (SQRT(avg_qual) / 5));

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_poles_periods_selrating_driver`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_poles_periods_selrating_driver`()
    COMMENT 'Record Breaker selrating team calcs for f1:ind:poles:periods'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_driver_results;
  CREATE TEMPORARY TABLE tmp_driver_results (
    driver_id SMALLINT UNSIGNED,
    pole_ratio DECIMAL(6, 4) UNSIGNED,
    front_row_ratio DECIMAL(6, 4) UNSIGNED,
    q3_ratio DECIMAL(6, 4) UNSIGNED,
    avg_qual DECIMAL(6, 4) UNSIGNED,
    PRIMARY KEY (driver_id)
  ) ENGINE = MyISAM
    SELECT RACE_DRIVER.driver_id,
           AVG((RACE_GRID.grid_pos = 1) / RACE.race_multiplier) AS pole_ratio,
           AVG((RACE_GRID.grid_pos IN (1, 2)) / RACE.race_multiplier) AS front_row_ratio,
           AVG((RACE_Q3.pos IS NOT NULL) / RACE.race_multiplier) AS q3_ratio,
           AVG(RACE_GRID.grid_pos * RACE.race_multiplier) AS avg_qual
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS RACE_GRID
      ON (RACE_GRID.season = RACE_DRIVER.season
      AND RACE_GRID.series = RACE_DRIVER.series
      AND RACE_GRID.round = RACE_DRIVER.round
      AND RACE_GRID.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_GRID.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS RACE_Q3
      ON (RACE_Q3.season = RACE_DRIVER.season
      AND RACE_Q3.series = RACE_DRIVER.series
      AND RACE_Q3.round = RACE_DRIVER.round
      AND RACE_Q3.session = 'q3'
      AND RACE_Q3.car_no = RACE_DRIVER.car_no)
    GROUP BY RACE_DRIVER.driver_id;

  # Merge the components into a single driver rating
  ALTER TABLE tmp_driver_results
    ADD COLUMN rel_rtg DECIMAL(6, 4) UNSIGNED;
  UPDATE tmp_driver_results SET rel_rtg = (pole_ratio * 7) + (front_row_ratio * 4) + (q3_ratio * 3) + (5 - (SQRT(avg_qual) / 5));

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_f1_ind_poles_periods_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_poles_periods_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for f1:ind:poles:periods'
BEGIN

  # Get the pole position info per-driver
  DROP TEMPORARY TABLE IF EXISTS tmp_link_res;
  CREATE TEMPORARY TABLE tmp_link_res (
    car_no TINYINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    summary VARCHAR(50),
    PRIMARY KEY (car_no)
  ) ENGINE = MyISAM
    SELECT SEL.car_no,
           IFNULL(STAT.grid_pos, 99) = 1 AS stat,
           CASE
             WHEN STAT.grid_pos IS NULL THEN 'na'
             WHEN STAT.grid_pos = 1 THEN 'positive'
             ELSE 'negative'
           END AS status,
           IF(STAT.grid_pos IS NULL, 'DNQ', ordinal_suffix(STAT.grid_pos)) AS summary
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_FIA_RACES AS RACE
      ON (RACE.season = SEL.season
      AND RACE.series = v_sport
      AND RACE.round = SEL.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS STAT
      ON (STAT.season = RACE.season
      AND STAT.series = RACE.series
      AND STAT.round = RACE.round
      AND STAT.race = SEL.race
      AND STAT.car_no = SEL.car_no)
    GROUP BY SEL.car_no;

  # Now perform the update
  UPDATE tmp_link_events AS SEL
  JOIN tmp_link_res AS STAT
    ON (STAT.car_no = SEL.car_no)
  SET SEL.stat = STAT.stat,
      SEL.status = STAT.status,
      SEL.summary = STAT.summary;

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_f1_ind_poles_periods_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_poles_periods_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for f1:ind:poles:periods'
BEGIN

  # Establish each driver's race info
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    round TINYINT UNSIGNED,
    q1_pos TINYINT UNSIGNED,
    q2_pos TINYINT UNSIGNED,
    q3_pos TINYINT UNSIGNED,
    p2_gap DECIMAL(4,3),
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, round)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.round,
           DRIVER_Q1.pos AS q1_pos,
           DRIVER_Q2.pos AS q2_pos,
           DRIVER_Q3.pos AS q3_pos,
           IF(TIME_TO_SEC(DRIVER_Q3_P2.gap_time) > 9, 9.999, TIME_TO_SEC(DRIVER_Q3_P2.gap_time) + (DRIVER_Q3_P2.gap_time_ms / 1000)) AS p2_gap,
           NULL AS stress
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_FIA_RACES AS RACE
      ON (RACE.season = SEL.season
      AND RACE.series = v_sport
      AND RACE.round = SEL.round)
    JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS DRIVER_GRID
      ON (DRIVER_GRID.season = SEL.season
      AND DRIVER_GRID.series = v_sport
      AND DRIVER_GRID.round = SEL.round
      AND DRIVER_GRID.race = SEL.race
      AND DRIVER_GRID.car_no = SEL.car_no
      AND DRIVER_GRID.qual_pos = 1)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS DRIVER_Q1
      ON (DRIVER_Q1.season = DRIVER_GRID.season
      AND DRIVER_Q1.series = DRIVER_GRID.series
      AND DRIVER_Q1.round = DRIVER_GRID.round
      AND DRIVER_Q1.car_no = DRIVER_GRID.car_no
      AND DRIVER_Q1.session = 'q1')
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS DRIVER_Q2
      ON (DRIVER_Q2.season = DRIVER_GRID.season
      AND DRIVER_Q2.series = DRIVER_GRID.series
      AND DRIVER_Q2.round = DRIVER_GRID.round
      AND DRIVER_Q2.car_no = DRIVER_GRID.car_no
      AND DRIVER_Q2.session = 'q2')
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS DRIVER_Q3
      ON (DRIVER_Q3.season = DRIVER_GRID.season
      AND DRIVER_Q3.series = DRIVER_GRID.series
      AND DRIVER_Q3.round = DRIVER_GRID.round
      AND DRIVER_Q3.car_no = DRIVER_GRID.car_no
      AND DRIVER_Q3.session = 'q3')
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_QUALI AS DRIVER_Q3_P2
      ON (DRIVER_Q3_P2.season = DRIVER_GRID.season
      AND DRIVER_Q3_P2.series = DRIVER_GRID.series
      AND DRIVER_Q3_P2.round = DRIVER_GRID.round
      AND DRIVER_Q3_P2.session = 'q3'
      AND DRIVER_Q3_P2.pos = 2)
    GROUP BY SEL.link_id, SEL.season, SEL.round;

  # Then assign a stress score to each driver based on their grid (not qual) pos, progress in the race and laps led
  UPDATE tmp_link_stress
  SET stress =
    # Q1: Top 10, 11-15 sliding scale
    (IF(q1_pos > 10, (q1_pos - 10) / 5, 0) * 60)
    # Q2: Top 5, 6-10 sliding scale
    + (IF(q2_pos > 5, (q2_pos - 5) / 5, 0) * 95)
    # Q3: Gap to P2
    + ((1 - (POW(LEAST(p2_gap, 0.250), 2) / POW(0.25, 2))) * 100);

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.round = SEL.round)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
