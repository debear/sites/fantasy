##
## X Pass TD Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_passtd_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_passtd_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for nfl:ind:passtd:total'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN passtds_value DOUBLE,
    ADD COLUMN passtds_gm_value DOUBLE;

  # Calculate the Pass TDs and Pass TDs/Gm values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, passtds_value, passtds_gm_value)
    SELECT STAT.player_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(STAT.td) AS passtds_value,
           SUM(STAT.td) / COUNT(DISTINCT STAT.game_id) AS passtds_gm_value
    FROM debearco_sports.SPORTS_NFL_SCHEDULE AS SCHED
    JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_PASSING AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.week = SCHED.week
      AND STAT.game_id = SCHED.game_id)
    WHERE SCHED.season = v_season
    AND   SCHED.game_type = 'regular'
    AND   SCHED.game_date < v_start_date
    AND   SCHED.status IS NOT NULL
    GROUP BY STAT.player_id;

  # Now process the individual stat elements
  # 1 - Pass TDs
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'touchdowns') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = passtds_value,
        stat_text = passtds_value;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Pass TDs/Gm
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'touchdowns-per-game') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = passtds_gm_value,
        stat_text = FORMAT(passtds_gm_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN passtds_value,
    DROP COLUMN passtds_gm_value;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_passtd_total_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_passtd_total_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for nfl:ind:passtd:total'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 17);

  # Calc 1: Passing TDs Allowed
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(6, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(6, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(6, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.num_pass, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.num_pass, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_STATS_TDS AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.opp_team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.3, 'DESC'); # 70/30 Overall-vs-Home/Road, More TDs Allowed makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_passtds;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_passtds;

  # Calc 2: Red Zone Opportunities Faced
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(6, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(6, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(6, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.rz_num, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.rz_num, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_STATS_MISC AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id
      AND STAT.team_id = SCHED.opp_team_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.3, 'DESC'); # 70/30 Overall-vs-Home/Road, More Opps Faced makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_rzopp;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_rzopp;

  # Calc 3: Points Allowed
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gm DECIMAL(6, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(6, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(6, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, COUNT(*) AS gp, NULL AS stat_gm,
           SUM(SCHED.is_home = 1) AS home_gp,
           SUM(IF(SCHED.is_home = 1, STAT.visitor_score, 0)) AS home_stat,
           NULL AS home_stat_gm,
           SUM(SCHED.is_home = 0) AS visitor_gp,
           SUM(IF(SCHED.is_home = 0, STAT.home_score, 0)) AS visitor_stat,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_SCHEDULE AS STAT
      ON (STAT.season = SCHED.season
      AND STAT.game_type = SCHED.game_type
      AND STAT.game_id = SCHED.game_id)
    GROUP BY TEAMS.team_id;
  CALL records_opprating_proc(0.3, 'DESC'); # 70/30 Overall-vs-Home/Road, More Points Allowed makes for a better opponent
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs_pts;
  ALTER TABLE tmp_team_oppcalcs RENAME TO tmp_team_oppcalcs_pts;

  # Merge (split 25/40/35 PassTDs/RZ Opp/Pts-Game) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    home_opp_rating TINYINT UNSIGNED,
    visitor_opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id,
           (PASSTDS.home_opp_rating * 0.25) + (RZOPP.home_opp_rating * 0.40) + (POINTS.home_opp_rating * 0.35) AS home_opp_rating,
           (PASSTDS.visitor_opp_rating * 0.25) + (RZOPP.visitor_opp_rating * 0.40) + (POINTS.visitor_opp_rating * 0.35) AS visitor_opp_rating
    FROM tmp_all_teams AS TEAMS
    JOIN tmp_team_oppcalcs_passtds AS PASSTDS
      ON (PASSTDS.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_rzopp AS RZOPP
      ON (RZOPP.team_id = TEAMS.team_id)
    JOIN tmp_team_oppcalcs_pts AS POINTS
      ON (POINTS.team_id = TEAMS.team_id);
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_passtd_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_passtd_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for nfl:ind:passtd:total'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_nfl_ind_sel_recent_sched(v_season, v_start_date, 17);

  # Opportunity 1: Depth Chart (direct rating calc)
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_depth;
  CREATE TEMPORARY TABLE tmp_rating_opportunity_depth (
    link_id SMALLINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           100 * MAX(DEPTH.season IS NOT NULL) AS rating
    FROM tmp_links AS LINK
    LEFT JOIN debearco_sports.SPORTS_NFL_TEAMS_DEPTH AS DEPTH
      ON (DEPTH.season = v_season
      AND DEPTH.player_id = LINK.link_id
      AND DEPTH.game_type = 'regular'
      AND DEPTH.week = v_period_id
      AND DEPTH.depth = 1)
    GROUP BY LINK.link_id;

  # Opportunity 2: Atts / Gm
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(STAT.atts) AS stat,
           SUM(STAT.atts) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_PASSING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Attempts make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_atts;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_atts;

  # Opportunity 3: Time of Possession
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat SMALLINT UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(TIME_TO_SEC(STAT.time_of_poss)) AS stat,
           SUM(TIME_TO_SEC(STAT.time_of_poss)) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_STATS_MISC AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id
      AND STAT.team_id = RECENT.team_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'ASC', NULL); # Lower Time of Possession makes for a better passing opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_time;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_time;

  # Opportunity 4: Red Zone visits
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity;
  CREATE TEMPORARY TABLE tmp_rating_opportunity (
    link_id SMALLINT UNSIGNED,
    stat SMALLINT UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           AVG(STAT.rz_num) AS stat,
           SUM(STAT.rz_num) AS stat_nom,
           COUNT(*) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_STATS_MISC AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id
      AND STAT.team_id = RECENT.team_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('opportunity', 'DESC', NULL); # More Red Zone visits make for a better opportunity
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_opportunity_rz;
  ALTER TABLE tmp_rating_opportunity RENAME TO tmp_rating_opportunity_rz;

  # Metric 1: Yards / Att
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(7, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           IF(SUM(STAT.atts) > 0, LEAST(99.99999, POW(GREATEST(SUM(STAT.yards), 0), 0.8) / POW(SUM(STAT.atts), 0.4)), NULL) AS stat,
           GREATEST(SUM(STAT.yards), 0) AS stat_nom,
           SUM(STAT.atts) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_PASSING AS STAT
      ON (STAT.season = RECENT.season
      AND STAT.player_id = RECENT.link_id
      AND STAT.game_type = RECENT.game_type
      AND STAT.week = RECENT.week
      AND STAT.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', 'calc_ypa,apply_least'); # Higher Yardage makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_yards;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_yards;

  # Merge (split opportunity 35/15/15/15 Depth/Atts/Time/RZ, metrics 7.5 yardage, 12.5 opp rating) and store
  # Prioritise Depth so much to separate QB1 from QB2+
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_opp_depth TINYINT UNSIGNED,
    rating_opp_atts TINYINT UNSIGNED,
    rating_opp_time TINYINT UNSIGNED,
    rating_opp_rz TINYINT UNSIGNED,
    rating_metric_yards TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           OPP_DEPTH.rating AS rating_opp_depth,
           OPP_ATTS.rating AS rating_opp_atts,
           OPP_TIME.rating AS rating_opp_time,
           OPP_RZ.rating AS rating_opp_rz,
           METRIC_YARDS.rating AS rating_metric_yards,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(OPP_DEPTH.rating, 0) * 0.35) + (IFNULL(OPP_ATTS.rating, 0) * 0.15) + (IFNULL(OPP_TIME.rating, 0) * 0.15) + (IFNULL(OPP_RZ.rating, 0) * 0.15)
             + (IFNULL(METRIC_YARDS.rating, 0) * 0.075)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.125) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_opportunity_depth AS OPP_DEPTH
      ON (OPP_DEPTH.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_atts AS OPP_ATTS
      ON (OPP_ATTS.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_time AS OPP_TIME
      ON (OPP_TIME.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_opportunity_rz AS OPP_RZ
      ON (OPP_RZ.link_id = LINK.link_id)
    LEFT JOIN tmp_rating_metrics_yards AS METRIC_YARDS
      ON (METRIC_YARDS.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_passtd_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_passtd_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for nfl:ind:passtd:total'
BEGIN

  # Determine what an "acceptable" value is, based on the TD/GP required
  DECLARE v_per_game TINYINT UNSIGNED;
  SELECT FLOOR(0.75 * (GAME.target / COUNT(DATES.period_id))) INTO v_per_game
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
    ON (DATES.sport = GAME.sport
    AND DATES.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  # Now perform the update
  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_NFL_PLAYERS_GAME_PASSING AS STAT
    ON (STAT.season = SEL.season
    AND STAT.game_type = SEL.game_type
    AND STAT.week = SEL.week
    AND STAT.game_id = SEL.game_id
    AND STAT.player_id = SEL.link_id)
  SET SEL.stat = STAT.td,
      SEL.status = CASE
        WHEN STAT.td = 0 THEN 'negative'
        WHEN STAT.td < v_per_game THEN 'neutral'
        ELSE 'positive'
      END,
      SEL.summary = CONCAT(STAT.td, ' TD');

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_nfl_ind_passtd_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_nfl_ind_passtd_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for nfl:ind:passtd:total'
BEGIN

  DECLARE v_target DECIMAL(5,3) UNSIGNED;
  DECLARE v_target_q2 DECIMAL(5,3) UNSIGNED;
  DECLARE v_target_q3 DECIMAL(5,3) UNSIGNED;

  # Determine the per-game, and then per-Q2/3/4 target
  SELECT GAME.target / COUNT(DATES.period_id) INTO v_target
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
    ON (DATES.sport = GAME.sport
    AND DATES.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  SET v_target_q2 = (v_target * 0.50);
  SET v_target_q3 = (v_target * 0.75);

  # Then count, by quarter and half (w/OT considered the 4th), the TD passes by each QB
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    week TINYINT UNSIGNED,
    game_id SMALLINT UNSIGNED,
    num_Q1 TINYINT UNSIGNED,
    num_Q2 TINYINT UNSIGNED,
    num_Q3 TINYINT UNSIGNED,
    num_tot TINYINT UNSIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.week, SEL.game_id,
           COUNT(DISTINCT IF(PASS_TD.play_id IS NOT NULL AND EVENT.quarter <= 1, PASS_TD.play_id, NULL)) AS num_Q1,
           COUNT(DISTINCT IF(PASS_TD.play_id IS NOT NULL AND EVENT.quarter <= 2, PASS_TD.play_id, NULL)) AS num_Q2,
           COUNT(DISTINCT IF(PASS_TD.play_id IS NOT NULL AND EVENT.quarter <= 3, PASS_TD.play_id, NULL)) AS num_Q3,
           COUNT(DISTINCT PASS_TD.play_id) AS num_tot,
           NULL AS stress
    FROM tmp_link_events AS SEL
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_SCORING AS EVENT
      ON (EVENT.season = SEL.season
      AND EVENT.game_type = SEL.game_type
      AND EVENT.week = SEL.week
      AND EVENT.game_id = SEL.game_id
      AND EVENT.team_id = SEL.team_id
      AND EVENT.type LIKE 'td%')
    LEFT JOIN debearco_sports.SPORTS_NFL_GAME_SCORING_TD AS PASS_TD
      ON (PASS_TD.season = EVENT.season
      AND PASS_TD.game_type = EVENT.game_type
      AND PASS_TD.week = EVENT.week
      AND PASS_TD.game_id = EVENT.game_id
      AND PASS_TD.play_id = EVENT.play_id
      AND PASS_TD.type = 'pass'
      AND PASS_TD.qb = SEL.jersey)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.week, SEL.game_id;

  # Then assign a stress score to each player based on progress by the end of Q2, Q3 and Q4
  UPDATE tmp_link_stress
  SET stress = (ROUND(GREATEST(CAST(v_target_q2 AS SIGNED) - CAST(num_Q2 AS SIGNED), 0) / v_target_q2) * 60)
             + (ROUND(GREATEST(CAST(v_target_q3 AS SIGNED) - CAST(num_Q3 AS SIGNED), 0) / v_target_q3) * 80)
             + (ROUND(GREATEST(CAST(v_target AS SIGNED) - CAST(num_tot AS SIGNED), 0) / v_target) * 115);

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.week = SEL.week
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
