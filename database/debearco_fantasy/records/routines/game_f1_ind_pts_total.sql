##
## X Point Season
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_pts_total_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_pts_total_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for f1:ind:pts:total'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN start_value DOUBLE,
    ADD COLUMN finish_value DOUBLE;

  # Calculate the Avg Start and Finish values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, start_value, finish_value)
    SELECT RACE_DRIVER.driver_id AS link_id,
           NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           SUM(RACE_GRID.qual_pos) / COUNT(RACE_GRID.season) AS start_value,
           SUM(RACE_RESULT.pos) / COUNT(RACE_RESULT.season) AS finish_value
    FROM debearco_sports.SPORTS_FIA_RACES AS RACE
    JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS RACE_GRID
      ON (RACE_GRID.season = RACE_DRIVER.season
      AND RACE_GRID.series = RACE_DRIVER.series
      AND RACE_GRID.round = RACE_DRIVER.round
      AND RACE_GRID.car_no = RACE_DRIVER.car_no
      AND RACE_GRID.qual_pos IS NOT NULL)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS RACE_RESULT
      ON (RACE_RESULT.season = RACE_DRIVER.season
      AND RACE_RESULT.series = RACE_DRIVER.series
      AND RACE_RESULT.round = RACE_DRIVER.round
      AND RACE_RESULT.car_no = RACE_DRIVER.car_no
      AND IFNULL(RACE_RESULT.result, '') REGEXP '^[0-9]+$')
    WHERE RACE.season = v_season
    AND   RACE.series = v_sport
    AND   DATE(RACE.race_time) < v_start_date
    AND   RACE.race_laps_completed IS NOT NULL
    GROUP BY RACE_DRIVER.driver_id;

  # Now process the individual stat elements
  # 1 - Avg Start
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'avg-start') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = start_value,
        stat_text = FORMAT(start_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'ASC');
  END IF;
  # 2 - Avg Finish
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'avg-finish') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = finish_value,
        stat_text = FORMAT(finish_value, 2);
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'ASC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN start_value,
    DROP COLUMN finish_value;

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_pts_total_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_pts_total_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for f1:ind:pts:total'
BEGIN

  # Run our generalised helper setup
  CALL records_f1_selrating_setup(v_season, v_start_date);

  # Team calcs over these races
  CALL records_f1_ind_pts_total_selrating_team();

  # Then driver calcs, compared to their teammate
  CALL records_f1_ind_pts_total_selrating_driver();

  # Process via the generalised F1 helper
  CALL records_f1_selrating_proc(v_sport, v_season, v_game_id, v_period_id, 1.0, 0.6, 'ASC');

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_pts_total_selrating_team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_pts_total_selrating_team`()
    COMMENT 'Record Breaker selrating team calcs for f1:ind:pts:total'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_team_results;
  CREATE TEMPORARY TABLE tmp_team_results (
    team_id TINYINT UNSIGNED,
    avg_qual DECIMAL(6, 4) UNSIGNED,
    avg_res DECIMAL(6, 4) UNSIGNED,
    avg_chg DECIMAL(6, 4) SIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MyISAM
    SELECT RACE_DRIVER.team_id,
           AVG(RACE_GRID.qual_pos * RACE.race_multiplier) AS avg_qual,
           AVG(RACE_RESULT.pos * RACE.race_multiplier) AS avg_res,
           AVG(IF(RACE_RESULT.pos IS NOT NULL, CAST(RACE_GRID.grid_pos * RACE.race_multiplier AS SIGNED) - CAST(RACE_RESULT.pos * RACE.race_multiplier AS SIGNED), NULL)) AS avg_chg
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS RACE_GRID
      ON (RACE_GRID.season = RACE_DRIVER.season
      AND RACE_GRID.series = RACE_DRIVER.series
      AND RACE_GRID.round = RACE_DRIVER.round
      AND RACE_GRID.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_GRID.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS RACE_RESULT
      ON (RACE_RESULT.season = RACE_DRIVER.season
      AND RACE_RESULT.series = RACE_DRIVER.series
      AND RACE_RESULT.round = RACE_DRIVER.round
      AND RACE_RESULT.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND RACE_RESULT.car_no = RACE_DRIVER.car_no
      AND IFNULL(RACE_RESULT.result, '') REGEXP '^[0-9]+$')
    GROUP BY RACE_DRIVER.team_id;

  # Apply the qual>race improvement as a factor into the team rating
  ALTER TABLE tmp_team_results
    ADD COLUMN avg_rtg DECIMAL(6, 4) UNSIGNED;
  UPDATE tmp_team_results SET avg_rtg = avg_qual - (avg_chg * 0.4);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_f1_ind_pts_total_selrating_driver`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_pts_total_selrating_driver`()
    COMMENT 'Record Breaker selrating team calcs for f1:ind:pts:total'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_driver_results;
  CREATE TEMPORARY TABLE tmp_driver_results (
    driver_id SMALLINT UNSIGNED,
    num_races TINYINT UNSIGNED,
    raw_net TINYINT SIGNED,
    raw_ratio DECIMAL(6, 4) UNSIGNED,
    rel_raw_qual DECIMAL(6, 4) SIGNED,
    rel_raw_res DECIMAL(6, 4) SIGNED,
    PRIMARY KEY (driver_id)
  ) ENGINE = MyISAM
    SELECT RACE_DRIVER.driver_id,
           COUNT(*) AS num_races,
           SUM(DRIVER_GRID.qual_pos < TEAMMATE_GRID.qual_pos) - SUM(DRIVER_GRID.qual_pos > TEAMMATE_GRID.qual_pos) AS raw_net,
           SUM(DRIVER_GRID.qual_pos < TEAMMATE_GRID.qual_pos) / COUNT(*) AS raw_ratio,
           AVG((CAST(DRIVER_GRID.qual_pos AS SIGNED) * CAST(RACE.race_multiplier AS SIGNED)) - (CAST(TEAMMATE_GRID.qual_pos AS SIGNED) * CAST(RACE.race_multiplier AS SIGNED))) AS rel_raw_qual,
           AVG((DRIVER_RESULT.pos * RACE.race_multiplier) - IFNULL(TEAMMATE_RESULT.pos * RACE.race_multiplier, (DRIVER_RESULT.pos * RACE.race_multiplier) + 3)) AS rel_raw_res
    FROM tmp_races AS RACE
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS DRIVER_GRID
      ON (DRIVER_GRID.season = RACE_DRIVER.season
      AND DRIVER_GRID.series = RACE_DRIVER.series
      AND DRIVER_GRID.round = RACE_DRIVER.round
      AND DRIVER_GRID.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND DRIVER_GRID.car_no = RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS DRIVER_RESULT
      ON (DRIVER_RESULT.season = RACE_DRIVER.season
      AND DRIVER_RESULT.series = RACE_DRIVER.series
      AND DRIVER_RESULT.round = RACE_DRIVER.round
      AND DRIVER_RESULT.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND DRIVER_RESULT.car_no = RACE_DRIVER.car_no
      AND IFNULL(DRIVER_RESULT.result, '') REGEXP '^[0-9]+$')
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_TEAMMATE
      ON (RACE_TEAMMATE.season = RACE_DRIVER.season
      AND RACE_TEAMMATE.series = RACE_DRIVER.series
      AND RACE_TEAMMATE.round = RACE_DRIVER.round
      AND RACE_TEAMMATE.team_id = RACE_DRIVER.team_id
      AND RACE_TEAMMATE.car_no <> RACE_DRIVER.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS TEAMMATE_GRID
      ON (TEAMMATE_GRID.season = RACE_TEAMMATE.season
      AND TEAMMATE_GRID.series = RACE_TEAMMATE.series
      AND TEAMMATE_GRID.round = RACE_TEAMMATE.round
      AND TEAMMATE_GRID.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND TEAMMATE_GRID.car_no = RACE_TEAMMATE.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS TEAMMATE_RESULT
      ON (TEAMMATE_RESULT.season = RACE_TEAMMATE.season
      AND TEAMMATE_RESULT.series = RACE_TEAMMATE.series
      AND TEAMMATE_RESULT.round = RACE_TEAMMATE.round
      AND TEAMMATE_RESULT.race = IF(RACE.race2_time IS NULL, 1, 2)
      AND TEAMMATE_RESULT.car_no = RACE_TEAMMATE.car_no
      AND IFNULL(TEAMMATE_RESULT.result, '') REGEXP '^[0-9]+$')
    GROUP BY RACE_DRIVER.driver_id;

  # Standardise these relative values (to the race count)
  ALTER TABLE tmp_driver_results
    ADD COLUMN rel_qual DECIMAL(6, 4) SIGNED,
    ADD COLUMN rel_res DECIMAL(6, 4) SIGNED;
  UPDATE tmp_driver_results
  SET rel_qual = rel_raw_qual * POW(1.02, SQRT(25 - num_races)),
      rel_res = GREATEST(rel_raw_res, rel_raw_res * POW(1.02, SQRT(25 - num_races)));

  # Apply the relative qual>race improvement and against teammates as factors into the driver rating
  ALTER TABLE tmp_driver_results
    ADD COLUMN rel_rtg DECIMAL(6, 4) SIGNED;
  UPDATE tmp_driver_results
  SET rel_rtg = rel_qual - ((rel_qual - rel_res) * 0.4);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_f1_ind_pts_total_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_pts_total_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for f1:ind:pts:total'
BEGIN

  # Determine what an "acceptable" value is, based on the pts/race required
  DECLARE v_per_race_lo DECIMAL(5,3) UNSIGNED;
  DECLARE v_per_race_hi DECIMAL(5,3) UNSIGNED;
  SELECT 0.75 * (GAME.target / COUNT(DATES.period_id)), GAME.target / COUNT(DATES.period_id)
    INTO v_per_race_lo, v_per_race_hi
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS DATES
    ON (DATES.sport = GAME.sport
    AND DATES.season = GAME.season)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id;

  # Get the race info
  DROP TEMPORARY TABLE IF EXISTS tmp_link_res;
  CREATE TEMPORARY TABLE tmp_link_res (
    car_no TINYINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    summary VARCHAR(50),
    PRIMARY KEY (car_no)
  ) ENGINE = MyISAM
    SELECT SEL.car_no,
           SUM(IFNULL(STAT.pts, 0)) AS stat,
           CASE
             WHEN STAT.result IS NULL THEN 'na'
             WHEN SUM(IFNULL(STAT.pts, 0)) < v_per_race_lo THEN 'negative'
             WHEN SUM(IFNULL(STAT.pts, 0)) < v_per_race_hi THEN 'neutral'
             ELSE 'positive'
           END AS status,
           CONCAT(GROUP_CONCAT(IF(SEL.race = STAT.race, ordinal_suffix(STAT.result), NULL)), ', ', pluralise(SUM(IFNULL(STAT.pts, '0')), 'pt')) AS summary
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS STAT
      ON (STAT.season = SEL.season
      AND STAT.series = v_sport
      AND STAT.round = SEL.round
      AND STAT.car_no = SEL.car_no)
    GROUP BY SEL.car_no;

  # Now perform the update
  UPDATE tmp_link_events AS SEL
  JOIN tmp_link_res AS STAT
    ON (STAT.car_no = SEL.car_no)
  SET SEL.stat = STAT.stat,
      SEL.status = STAT.status,
      SEL.summary = STAT.summary;

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_f1_ind_pts_total_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_pts_total_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for f1:ind:pts:total'
BEGIN

  # Establish each driver's race info
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    round TINYINT UNSIGNED,
    grid_pos TINYINT UNSIGNED,
    qual_pos TINYINT UNSIGNED,
    race_pos TINYINT UNSIGNED,
    race_progress TINYINT SIGNED,
    race_laps_led DECIMAL(6,5) UNSIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, round)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.season, SEL.round,
           DRIVER_GRID.grid_pos, DRIVER_GRID.qual_pos, DRIVER_RESULT.pos AS race_pos,
           IF(DRIVER_RESULT.pos IS NOT NULL, CAST(DRIVER_GRID.grid_pos AS SIGNED) - CAST(DRIVER_RESULT.pos AS SIGNED), NULL) AS race_progress,
           IFNULL(SUM(DRIVER_LEADER.lap_to - DRIVER_LEADER.lap_from + 1) / IF(SEL.race = IF(RACE.race2_time IS NULL, 1, 2), RACE.race_laps_completed, RACE.race2_laps_completed), 0) AS race_laps_led,
           NULL AS stress
    FROM tmp_link_events AS SEL
    LEFT JOIN debearco_sports.SPORTS_FIA_RACES AS RACE
      ON (RACE.season = SEL.season
      AND RACE.series = v_sport
      AND RACE.round = SEL.round)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_GRID AS DRIVER_GRID
      ON (DRIVER_GRID.season = SEL.season
      AND DRIVER_GRID.series = v_sport
      AND DRIVER_GRID.round = SEL.round
      AND DRIVER_GRID.race = SEL.race
      AND DRIVER_GRID.car_no = SEL.car_no)
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_RESULT AS DRIVER_RESULT
      ON (DRIVER_RESULT.season = SEL.season
      AND DRIVER_RESULT.series = v_sport
      AND DRIVER_RESULT.round = SEL.round
      AND DRIVER_RESULT.race = SEL.race
      AND DRIVER_RESULT.car_no = SEL.car_no
      AND IFNULL(DRIVER_RESULT.result, '') REGEXP '^[0-9]+$')
    LEFT JOIN debearco_sports.SPORTS_FIA_RACE_LEADERS AS DRIVER_LEADER
      ON (DRIVER_LEADER.season = SEL.season
      AND DRIVER_LEADER.series = v_sport
      AND DRIVER_LEADER.round = SEL.round
      AND DRIVER_LEADER.race = SEL.race
      AND DRIVER_LEADER.car_no = SEL.car_no)
    GROUP BY SEL.link_id, SEL.season, SEL.round;

  # Then assign a stress score to each driver based on their grid (not qual) pos, progress in the race and laps led
  UPDATE tmp_link_stress
  SET stress = GREATEST(0, LEAST(255,
      IF(grid_pos <= 5 AND race_pos IS NOT NULL, (POW(grid_pos - 1, 2) / POW(5, 2)) * 255, 255)
    - IF(race_progress IS NOT NULL AND grid_pos <= 10, POW(GREATEST(LEAST(race_progress, 3), -3), IF(grid_pos <= 5, 3, IF(race_progress > 0, 1.5, 2))), 0)
    + IF(race_pos = 1, (1 - race_laps_led) * 40, 0)
  ));

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.round = SEL.round)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
