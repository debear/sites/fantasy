##
## Make selections for automated users in a game
##
DROP PROCEDURE IF EXISTS `records_autouser_sel`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_sel`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker automated user entry selections'
BEGIN

  DECLARE v_has_opp_rating TINYINT(1) UNSIGNED;
  DECLARE v_allows_sel_reuse TINYINT(1) UNSIGNED;
  DECLARE v_allows_restart TINYINT(1) UNSIGNED;
  DECLARE v_period_last TINYINT(3) UNSIGNED;
  DECLARE v_sel_type ENUM('ind', 'team');

  # Up memory limit to 256meg for some of the larger temporary table processing
  SET @@session.max_heap_table_size = 268435456; # 268,435,456 = 256 * 1024 * 1024
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

  # Determine some processing flags
  SET v_has_opp_rating = sport_is_major_league(v_sport);
  SELECT FIND_IN_SET('SEL_SINGLE_USE', config_flags) = 0 AS sel_reuse,
         FIND_IN_SET('GAME_ENDS_ON_NEGATIVE', config_flags) = 0 AS game_restart,
         sel_type
    INTO v_allows_sel_reuse, v_allows_restart, v_sel_type
  FROM FANTASY_RECORDS_GAMES
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id;

  # Determine the previous period
  SELECT PERIOD_LAST.period_id INTO v_period_last
  FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD_CURR
  LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD_LAST
    ON (PERIOD_LAST.sport = PERIOD_CURR.sport
    AND PERIOD_LAST.season = PERIOD_CURR.season
    AND PERIOD_LAST.period_order = PERIOD_CURR.period_order - 1)
  WHERE PERIOD_CURR.sport = v_sport
  AND   PERIOD_CURR.season = v_season
  AND   PERIOD_CURR.period_id = v_period_id;

  # Determine the entries and their personalities to process
  DROP TEMPORARY TABLE IF EXISTS tmp_entries;
  CREATE TEMPORARY TABLE tmp_entries (
    user_id SMALLINT UNSIGNED,
    personality ENUM('stats','rating','opp_rating','random','variable'),
    risk_taking TINYINT UNSIGNED,
    rand_personality DECIMAL(6,5) UNSIGNED,
    rand_score DECIMAL(6,5) UNSIGNED,
    rand_risk DECIMAL(6,5) UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE = MyISAM
    SELECT AUTOUSER.id AS user_id, AUTOUSER.personality, AUTOUSER.risk_taking,
           RAND() AS rand_personality, RAND() AS rand_score,
           # Risk Rating 0..risk_taking
           (RAND() * risk_taking) / 100 AS rand_risk
    FROM FANTASY_RECORDS_ENTRIES_SCORES AS ENTRY_SCORE
    JOIN FANTASY_AUTOMATED_USER AS AUTOUSER
      ON (AUTOUSER.id = ENTRY_SCORE.user_id)
    LEFT JOIN FANTASY_RECORDS_ENTRIES_BYPERIOD AS LAST_ENTRY_SEL
      ON (LAST_ENTRY_SEL.sport = ENTRY_SCORE.sport
      AND LAST_ENTRY_SEL.season = ENTRY_SCORE.season
      AND LAST_ENTRY_SEL.game_id = ENTRY_SCORE.game_id
      AND LAST_ENTRY_SEL.user_id = ENTRY_SCORE.user_id
      AND LAST_ENTRY_SEL.period_id = v_period_last
      AND LAST_ENTRY_SEL.link_id IS NOT NULL)
    LEFT JOIN FANTASY_RECORDS_SELECTIONS AS LAST_SEL
      ON (LAST_SEL.sport = LAST_ENTRY_SEL.sport
      AND LAST_SEL.season = LAST_ENTRY_SEL.season
      AND LAST_SEL.game_id = LAST_ENTRY_SEL.game_id
      AND LAST_SEL.link_id = LAST_ENTRY_SEL.link_id
      AND LAST_SEL.period_id = LAST_ENTRY_SEL.period_id
      AND LAST_SEL.status IN ('positive', 'neutral', 'na'))
    WHERE ENTRY_SCORE.sport = v_sport
    AND   ENTRY_SCORE.season = v_season
    AND   ENTRY_SCORE.game_id = v_game_id
    # Not every game allows teams to restart when streak ends, so exclude those who are not allowed to proceed
    AND  (v_allows_restart OR v_period_last IS NULL OR LAST_SEL.link_id IS NOT NULL);

  # Update those whose personality is "variable" to one of the other options
  UPDATE tmp_entries
  SET personality = CASE
      WHEN rand_personality < IF(v_has_opp_rating, 0.25, 0.33333) THEN 'stats'
      WHEN rand_personality < IF(v_has_opp_rating, 0.50, 0.66666) THEN 'rating'
      WHEN rand_personality < IF(v_has_opp_rating, 0.75, 1.0) THEN 'random'
      ELSE 'opp_rating'
    END
  WHERE personality = 'variable';

  # Tweak: Motorsport doesn't have an "opponent ranking", so consider these "ranking"
  IF !v_has_opp_rating THEN
    UPDATE tmp_entries
    SET personality = 'rating'
    WHERE personality = 'opp_rating';
  END IF;

  # Where a game does not allow selections to be reused, determine in advance all the current selections used by each entry
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_used;
  CREATE TEMPORARY TABLE tmp_entry_sel_used (
    user_id SMALLINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, link_id)
  ) ENGINE = MyISAM;
  IF !v_allows_sel_reuse THEN
    INSERT INTO tmp_entry_sel_used (user_id, link_id)
      SELECT DISTINCT ENTRY_SEL.user_id, ENTRY_SEL.link_id
      FROM FANTASY_COMMON_PERIODS_DATES AS PERIOD_PROC
      JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD_PREV
        ON (PERIOD_PREV.sport = PERIOD_PROC.sport
        AND PERIOD_PREV.season = PERIOD_PROC.season
        AND PERIOD_PREV.period_order < PERIOD_PROC.period_order)
      JOIN FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
        ON (ENTRY_SEL.sport = PERIOD_PROC.sport
        AND ENTRY_SEL.season = PERIOD_PROC.season
        AND ENTRY_SEL.game_id = v_game_id
        AND ENTRY_SEL.period_id = PERIOD_PREV.period_id
        AND ENTRY_SEL.link_id IS NOT NULL)
      JOIN tmp_entries AS ENTRY
        ON (ENTRY.user_id = ENTRY_SEL.user_id)
      WHERE PERIOD_PROC.sport = v_sport
      AND   PERIOD_PROC.season = v_season
      AND   PERIOD_PROC.period_id = v_period_id;
  END IF;

  # Our table for subsequent storing
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel;
  CREATE TEMPORARY TABLE tmp_entry_sel (
    user_id SMALLINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, link_id)
  ) ENGINE = MyISAM;

  # Iterate through each of the options
  CALL records_autouser_sel_stats(v_sport, v_season, v_game_id, v_period_id, v_sel_type);
  CALL records_autouser_sel_rating(v_sport, v_season, v_game_id, v_period_id, v_sel_type);
  CALL records_autouser_sel_opp_rating(v_sport, v_season, v_game_id, v_period_id, v_sel_type); # Ignored when not appropriate, as list will be empty
  CALL records_autouser_sel_random(v_sport, v_season, v_game_id, v_period_id, v_sel_type);

  # Determine if any autouser has an existing selection (which will affect the audit record created)
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_existing;
  CREATE TEMPORARY TABLE tmp_entry_sel_existing (
    user_id SMALLINT UNSIGNED,
    PRIMARY KEY (user_id)
  ) ENGINE = MyISAM
    SELECT ENTRY_SEL.user_id
    FROM tmp_entries
    JOIN FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
      ON (ENTRY_SEL.sport = v_sport
      AND ENTRY_SEL.season = v_season
      AND ENTRY_SEL.game_id = v_game_id
      AND ENTRY_SEL.user_id = tmp_entries.user_id
      AND ENTRY_SEL.period_id = v_period_id
      AND ENTRY_SEL.link_id IS NOT NULL);

  # Finally store and audit
  INSERT INTO FANTASY_RECORDS_ENTRIES_BYPERIOD (sport, season, game_id, period_id, user_id, link_id, sel_saved)
    SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id, v_period_id AS period_id,
           user_id, link_id, _NOW() AS sel_saved
    FROM tmp_entry_sel
  ON DUPLICATE KEY UPDATE link_id = VALUES(link_id),
                          sel_saved = VALUES(sel_saved);

  # Create approrpiate audit details
  IF records_config('audit_autouser') THEN
    # Audit 46 == Sel Created, 47 == Sel Updated
    INSERT INTO debearco_admin.USER_ACTIVITY_LOG (activity_id, activity_time, type, user_id, summary, detail)
      SELECT NULL, _NOW(), IF(EXISTING_SEL.user_id IS NULL, 46, 47), ENTRY_SEL.user_id,
            CONCAT('Automated user selection ', IF(EXISTING_SEL.user_id IS NULL, 'added', 'updated')),
            CONCAT('{"game_ref": "', v_sport, '-', v_season, '-', v_game_id, '", "period_id": "', v_period_id, '", "link_id": "', ENTRY_SEL.link_id, '"}')
      FROM tmp_entry_sel AS ENTRY_SEL
      LEFT JOIN tmp_entry_sel_existing AS EXISTING_SEL
        ON (EXISTING_SEL.user_id = ENTRY_SEL.user_id);
  END IF;

  # Revert the memory change
  SET @@session.max_heap_table_size = DEFAULT; # DEFAULT = global value (i.e., what we had before...)
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

END $$

DELIMITER ;

##
## Make stat-based selections for automated users in a game
##
DROP PROCEDURE IF EXISTS `records_autouser_sel_stats`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_sel_stats`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_sel_type ENUM('ind', 'team')
)
    COMMENT 'Record Breaker automated user entry stat-based selections'
BEGIN

  # Master table with available selections, mean stat rank and stddev for subsequent processing
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_by_stats;
  CREATE TEMPORARY TABLE tmp_selections_by_stats (
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    rank_avg DECIMAL(8,5) UNSIGNED,
    rank_stddev DECIMAL(8,5) UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id,
           CAST(CONCAT(FORMAT(RAND(), 9), LPAD(SEL.link_id, 5, '0')) AS DECIMAL(15,14)) AS link_rand,
           AVG(SEL_STATS.stat_order) AS rank_avg, STDDEV(SEL_STATS.stat_order) AS rank_stddev
    FROM FANTASY_RECORDS_SELECTIONS AS SEL
    JOIN FANTASY_RECORDS_SELECTIONS_STATS AS SEL_STATS
      ON (SEL_STATS.sport = SEL.sport
      AND SEL_STATS.season = SEL.season
      AND SEL_STATS.game_id = SEL.game_id
      AND SEL_STATS.link_id = SEL.link_id
      AND SEL_STATS.period_id = SEL.period_id)
    WHERE SEL.sport = v_sport
    AND   SEL.season = v_season
    AND   SEL.game_id = v_game_id
    AND   SEL.period_id = v_period_id
    AND   SEL.rating >= IF(v_sel_type = 'ind', 16, 0) # Ensure we are above the max rating for inactive players
    GROUP BY SEL.link_id;

  # Rebase the rank_avg, as we want a "higher is better" approach, whilst stat_order would be the opposite, "lower is better"
  UPDATE tmp_selections_by_stats
  SET rank_avg = 999.99999 - rank_avg;

  # Now score each selection rank using the per-team randomiser
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_per_entry;
  CREATE TEMPORARY TABLE tmp_selections_per_entry (
    user_id SMALLINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    user_score DECIMAL(9,5) UNSIGNED,
    user_rank SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, link_id) USING BTREE
  ) ENGINE = MEMORY
    SELECT tmp_entries.user_id, tmp_selections_by_stats.link_id, tmp_selections_by_stats.link_rand,
           tmp_selections_by_stats.rank_avg - (tmp_entries.rand_score * tmp_selections_by_stats.rank_stddev) AS user_score,
           0 AS user_rank
    FROM tmp_entries
    JOIN tmp_selections_by_stats
      ON (1 = 1)
    WHERE tmp_entries.personality = 'stats';

  CALL records_autouser_sel__proc('stats');

END $$

DELIMITER ;

##
## Make rating-based selections for automated users in a game
##
DROP PROCEDURE IF EXISTS `records_autouser_sel_rating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_sel_rating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_sel_type ENUM('ind', 'team')
)
    COMMENT 'Record Breaker automated user entry rating-based selections'
BEGIN

  # Master table with available selections and rating
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_by_rating;
  CREATE TEMPORARY TABLE tmp_selections_by_rating (
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT link_id,
           CAST(CONCAT(FORMAT(RAND(), 9), LPAD(link_id, 5, '0')) AS DECIMAL(15,14)) AS link_rand,
           rating
    FROM FANTASY_RECORDS_SELECTIONS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   period_id = v_period_id
    AND   rating >= IF(v_sel_type = 'ind', 16, 0); # Ensure we are above the max rating for inactive players

  # Now score each selection rank using the per-team randomiser
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_per_entry;
  CREATE TEMPORARY TABLE tmp_selections_per_entry (
    user_id SMALLINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    user_score DECIMAL(9,5) UNSIGNED,
    user_rank SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, link_id) USING BTREE
  ) ENGINE = MEMORY
    SELECT tmp_entries.user_id, tmp_selections_by_rating.link_id, tmp_selections_by_rating.link_rand,
           (tmp_entries.rand_score * tmp_selections_by_rating.rating) AS user_score,
           0 AS user_rank
    FROM tmp_entries
    JOIN tmp_selections_by_rating
      ON (1 = 1)
    WHERE tmp_entries.personality = 'rating';

  CALL records_autouser_sel__proc('rating');

END $$

DELIMITER ;

##
## Make oppopent rating-based selections for automated users in a game
##
DROP PROCEDURE IF EXISTS `records_autouser_sel_opp_rating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_sel_opp_rating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_sel_type ENUM('ind', 'team')
)
    COMMENT 'Record Breaker automated user entry opp rating-based selections'
BEGIN

  # Master table with available selections and rating
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_by_opp_rating;
  CREATE TEMPORARY TABLE tmp_selections_by_opp_rating (
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT link_id,
           CAST(CONCAT(FORMAT(RAND(), 9), LPAD(link_id, 5, '0')) AS DECIMAL(15,14)) AS link_rand,
           opp_rating
    FROM FANTASY_RECORDS_SELECTIONS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   period_id = v_period_id
    AND   rating >= IF(v_sel_type = 'ind', 16, 0); # Ensure we are above the max rating for inactive players

  # Now score each selection rank using the per-team randomiser
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_per_entry;
  CREATE TEMPORARY TABLE tmp_selections_per_entry (
    user_id SMALLINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    user_score DECIMAL(9,5) UNSIGNED,
    user_rank SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, link_id) USING BTREE
  ) ENGINE = MEMORY
    SELECT tmp_entries.user_id, tmp_selections_by_opp_rating.link_id, tmp_selections_by_opp_rating.link_rand,
           (tmp_entries.rand_score * tmp_selections_by_opp_rating.opp_rating) AS user_score,
           0 AS user_rank
    FROM tmp_entries
    JOIN tmp_selections_by_opp_rating
      ON (1 = 1)
    WHERE tmp_entries.personality = 'opp_rating';

  CALL records_autouser_sel__proc('opp_rating');

END $$

DELIMITER ;

##
## Make random-based selections for automated users in a game
##
DROP PROCEDURE IF EXISTS `records_autouser_sel_random`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_sel_random`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_sel_type ENUM('ind', 'team')
)
    COMMENT 'Record Breaker automated user entry random-based selections'
BEGIN

  DECLARE v_has_opp_rating TINYINT(1) UNSIGNED;
  DECLARE v_num_sel SMALLINT UNSIGNED;

  # Determine some processing flags
  SET v_has_opp_rating = sport_is_major_league(v_sport);

  # Build a picture of the selections from each component table
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_by_random;
  CREATE TEMPORARY TABLE tmp_selections_by_random (
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    stats_avg DECIMAL(8,5) UNSIGNED,
    stats_stddev DECIMAL(8,5) UNSIGNED,
    rating TINYINT UNSIGNED,
    opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT SEL_BY_STATS.link_id, SEL_BY_STATS.link_rand,
           SEL_BY_STATS.rank_avg AS stats_avg,
           SEL_BY_STATS.rank_stddev AS stats_stddev,
           SEL_BY_RATING.rating,
           SEL_BY_OPP_RATING.opp_rating
    FROM tmp_selections_by_stats AS SEL_BY_STATS
    JOIN tmp_selections_by_rating AS SEL_BY_RATING
      ON (SEL_BY_RATING.link_id = SEL_BY_STATS.link_id
      AND SEL_BY_RATING.rating >= IF(v_sel_type = 'ind', 16, 0)) # Ensure we are above the max rating for inactive players
    LEFT JOIN tmp_selections_by_opp_rating AS SEL_BY_OPP_RATING
      ON (SEL_BY_OPP_RATING.link_id = SEL_BY_STATS.link_id);

  # Now score each selection rank using the per-team randomiser
  DROP TEMPORARY TABLE IF EXISTS tmp_selections_per_entry;
  CREATE TEMPORARY TABLE tmp_selections_per_entry (
    user_id SMALLINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    link_rand DECIMAL(15,14) UNSIGNED,
    stats_score DECIMAL(8,5) UNSIGNED,
    stats_rank SMALLINT UNSIGNED,
    rating_score DECIMAL(8,5) UNSIGNED,
    rating_rank SMALLINT UNSIGNED,
    opp_rating_score DECIMAL(8,5) UNSIGNED,
    opp_rating_rank SMALLINT UNSIGNED,
    user_score DECIMAL(9,5) UNSIGNED,
    user_rank SMALLINT UNSIGNED,
    PRIMARY KEY (user_id, link_id) USING BTREE
  ) ENGINE = MEMORY
    SELECT tmp_entries.user_id, tmp_selections_by_random.link_id, tmp_selections_by_random.link_rand,
           tmp_selections_by_random.stats_avg - (tmp_entries.rand_score * tmp_selections_by_random.stats_stddev) AS stats_score,
           0 AS stats_rank,
           (tmp_entries.rand_score * tmp_selections_by_random.rating) AS rating_score,
           0 AS rating_rank,
           (tmp_entries.rand_score * tmp_selections_by_random.opp_rating) AS opp_rating_score,
           0 AS opp_rating_rank,
           0 AS user_score, 0 AS user_rank
    FROM tmp_entries
    JOIN tmp_selections_by_random
      ON (1 = 1)
    WHERE tmp_entries.personality = 'random';

  # Now rank each per-team selection by stat
  CALL _duplicate_tmp_table('tmp_selections_per_entry', 'tmp_selections_per_entry_cpA');
  INSERT INTO tmp_selections_per_entry (user_id, link_id, stats_score, stats_rank)
    SELECT SQL_BIG_RESULT user_id, link_id, stats_score, ROW_NUMBER() OVER w AS stats_rank
    FROM tmp_selections_per_entry_cpA
    WINDOW w AS (PARTITION BY user_id ORDER BY user_id, stats_score DESC, link_rand DESC)
  ON DUPLICATE KEY UPDATE stats_rank = VALUES(stats_rank);
  ALTER TABLE tmp_selections_per_entry
    DROP COLUMN stats_score;

  CALL _duplicate_tmp_table('tmp_selections_per_entry', 'tmp_selections_per_entry_cpA');
  INSERT INTO tmp_selections_per_entry (user_id, link_id, rating_score, rating_rank)
    SELECT SQL_BIG_RESULT user_id, link_id, rating_score, ROW_NUMBER() OVER w AS rating_rank
    FROM tmp_selections_per_entry_cpA
    WINDOW w AS (PARTITION BY user_id ORDER BY user_id, rating_score DESC, link_rand DESC)
  ON DUPLICATE KEY UPDATE rating_rank = VALUES(rating_rank);
  ALTER TABLE tmp_selections_per_entry
    DROP COLUMN rating_score;

  IF v_has_opp_rating THEN
    # Opponent Rating applies to Major League games only
    CALL _duplicate_tmp_table('tmp_selections_per_entry', 'tmp_selections_per_entry_cpA');
    INSERT INTO tmp_selections_per_entry (user_id, link_id, opp_rating_score, opp_rating_rank)
      SELECT SQL_BIG_RESULT user_id, link_id, opp_rating_score, ROW_NUMBER() OVER w AS opp_rating_rank
      FROM tmp_selections_per_entry_cpA
      WINDOW w AS (PARTITION BY user_id ORDER BY user_id, opp_rating_score DESC, link_rand DESC)
    ON DUPLICATE KEY UPDATE opp_rating_rank = VALUES(opp_rating_rank);
  END IF;
  ALTER TABLE tmp_selections_per_entry
    DROP COLUMN opp_rating_score;

  # Combine these ranks in to a single score...
  UPDATE tmp_selections_per_entry
  SET user_score = IF(v_has_opp_rating,
    (stats_rank + rating_rank + opp_rating_rank) / 3,
    (stats_rank + rating_rank) / 2
  );
  # ...which we need to invert, as the selection processing method works of DESC sort
  SELECT COUNT(*) INTO v_num_sel FROM tmp_selections_by_random;
  UPDATE tmp_selections_per_entry
  SET user_score = v_num_sel - user_score;

  ALTER TABLE tmp_selections_per_entry
    DROP COLUMN stats_rank,
    DROP COLUMN rating_rank,
    DROP COLUMN opp_rating_rank;

  CALL records_autouser_sel__proc('random');

END $$

DELIMITER ;

##
## Perform the final stage in selection processing, common amongst the available methods
##
DROP PROCEDURE IF EXISTS `records_autouser_sel__proc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_autouser_sel__proc`(
  v_personality ENUM('stats','rating','opp_rating','random')
)
    COMMENT 'Record Breaker automated user entry selection processing'
BEGIN

  DECLARE v_cutoff SMALLINT UNSIGNED;

  # Remove the selections that were already previously used
  # (In games where this option does not apply, this table is empty so no-ops)
  DELETE tmp_selections_per_entry.*
  FROM tmp_selections_per_entry
  JOIN tmp_entry_sel_used
    ON (tmp_entry_sel_used.user_id = tmp_selections_per_entry.user_id
    AND tmp_entry_sel_used.link_id = tmp_selections_per_entry.link_id);

  # Now rank each per-team selection
  CALL _duplicate_tmp_table('tmp_selections_per_entry', 'tmp_selections_per_entry_cpA');
  INSERT INTO tmp_selections_per_entry (user_id, link_id, user_score, user_rank)
    SELECT SQL_BIG_RESULT user_id, link_id, user_score, ROW_NUMBER() OVER w AS user_rank
    FROM tmp_selections_per_entry_cpA
    WINDOW w AS (PARTITION BY user_id ORDER BY user_id, user_score DESC, link_rand DESC)
  ON DUPLICATE KEY UPDATE user_rank = VALUES(user_rank);

  # Consider the Top ~15% or 60 selections, though ensure bottom 20% excluded
  SELECT LEAST(GREATEST(ROUND(COUNT(*) * 0.15), 60), ROUND(COUNT(*) * 0.80)) INTO v_cutoff FROM tmp_selections_by_stats;

  # Use another randomiser to make each entry's selection
  INSERT INTO tmp_entry_sel (user_id, link_id)
    SELECT tmp_entries.user_id, tmp_selections_per_entry.link_id
    FROM tmp_entries
    LEFT JOIN tmp_selections_per_entry
      ON (tmp_selections_per_entry.user_id = tmp_entries.user_id
      AND tmp_selections_per_entry.user_rank = GREATEST(LEAST(ROUND(tmp_entries.rand_risk * v_cutoff), v_cutoff), 1))
    WHERE tmp_entries.personality = v_personality;

END $$

DELIMITER ;
