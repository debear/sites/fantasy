#
# Selections
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_sel`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_sel`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selection lists for ahl:ind'
BEGIN

  DECLARE v_end_date DATE;
  DECLARE v_pos_ref TINYINT UNSIGNED;
  DECLARE v_roster_date DATE;

  # Standardise our position reference
  DROP TEMPORARY TABLE IF EXISTS tmp_game_pos;
  CREATE TEMPORARY TABLE tmp_game_pos LIKE FANTASY_RECORDS_GAMES_POSITIONS;
  INSERT INTO tmp_game_pos
    SELECT *
    FROM FANTASY_RECORDS_GAMES_POSITIONS
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id;
  SELECT COUNT(*) INTO v_pos_ref FROM tmp_game_pos;

  IF v_pos_ref = 0 THEN
    INSERT INTO tmp_game_pos
      SELECT v_sport AS sport, v_season AS season, v_game_id AS game_id,
             pos_code, 0 AS disp_order
      FROM debearco_sports.SPORTS_AHL_POSITIONS;
  END IF;

  # Determine the appropriate roster date
  DROP TEMPORARY TABLE IF EXISTS tmp_roster_dates;
  CREATE TEMPORARY TABLE tmp_roster_dates (
    season YEAR NOT NULL,
    the_date DATE NOT NULL,
    PRIMARY KEY (season, the_date)
  ) ENGINE = MyISAM
    SELECT DISTINCT season, the_date
    FROM debearco_sports.SPORTS_AHL_TEAMS_ROSTERS
    WHERE season = v_season
    ORDER BY season, the_date;

  SELECT MAX(the_date) INTO v_roster_date
  FROM tmp_roster_dates
  WHERE season = v_season
  AND   the_date <= v_start_date;

  # Determine the teams playing in this period
  SELECT end_date INTO v_end_date
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  AND   period_id = v_period_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_period_teams;
  CREATE TEMPORARY TABLE tmp_period_teams (
    team_id CHAR(3),
    opp_team_id CHAR(3),
    is_home TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY;
  INSERT IGNORE INTO tmp_period_teams (team_id, opp_team_id, is_home)
    SELECT home AS team_id, visitor AS opp_team_id, 1 AS is_home
    FROM debearco_sports.SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    AND   game_date BETWEEN v_start_date AND v_end_date
    GROUP BY home;
  INSERT IGNORE INTO tmp_period_teams (team_id, opp_team_id, is_home)
    SELECT visitor AS team_id, home AS opp_team_id, 0 AS is_home
    FROM debearco_sports.SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    AND   game_date BETWEEN v_start_date AND v_end_date
    GROUP BY visitor;

  # Now populate
  ALTER TABLE tmp_links
    ADD COLUMN team_id CHAR(3),
    ADD COLUMN pos CHAR(2),
    ADD COLUMN opp_team_id CHAR(3),
    ADD COLUMN is_home TINYINT UNSIGNED,
    ADD COLUMN status_multiplier DECIMAL(3,2) UNSIGNED;

  TRUNCATE TABLE tmp_links;
  INSERT INTO tmp_links (link_id, link_sort, team_id, pos, opp_team_id, is_home, status_multiplier)
    SELECT PLAYER.player_id AS link_id,
           debearco_sports.fn_basic_html_comparison(CONCAT(PLAYER.surname, ',', PLAYER.first_name)) AS link_sort,
           SCHED.team_id, ROSTER.pos, SCHED.opp_team_id, SCHED.is_home,
           IF(ROSTER.player_status = 'active', 1.00, 0.15) AS status_multiplier
    FROM debearco_sports.SPORTS_AHL_TEAMS_ROSTERS AS ROSTER
    JOIN tmp_game_pos
      ON (tmp_game_pos.pos_code = ROSTER.pos)
    JOIN tmp_period_teams AS SCHED
      ON (SCHED.team_id = ROSTER.team_id)
    JOIN debearco_sports.SPORTS_AHL_PLAYERS AS PLAYER
      ON (PLAYER.player_id = ROSTER.player_id)
    WHERE ROSTER.season = v_season
    AND   ROSTER.the_date = v_roster_date;

END $$

DELIMITER ;

#
# Selection recent games
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_sel_recent_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_sel_recent_sched`(
  v_season SMALLINT UNSIGNED,
  v_start_date DATE,
  v_num_games TINYINT UNSIGNED
)
    COMMENT 'Record Breaker selection recent games for ahl:ind'
BEGIN

  # Start by looking back at all games over the last two seasons
  # This works on the expectation v_num_games does go beyond the start of the previous season
  DROP TEMPORARY TABLE IF EXISTS tmp_links_recent;
  CREATE TEMPORARY TABLE tmp_links_recent (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    game_time MEDIUMINT UNSIGNED,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    is_home TINYINT UNSIGNED,
    recency_order SMALLINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id),
    KEY by_link_game (season, link_id, game_type, game_id),
    KEY by_link_time (link_id, game_time)
  ) ENGINE=MyISAM
    SELECT LINK.link_id, SCHED.season, SCHED.game_type, SCHED.game_id,
           ((YEAR(SCHED.game_date) - (v_season - 2)) * 1000000)
            + (CAST(DATE_FORMAT(CONCAT(SCHED.game_date, ' ', SCHED.game_time), '%j') AS UNSIGNED) * 1000)
            + (CAST(DATE_FORMAT(CONCAT(SCHED.game_date, ' ', SCHED.game_time), '%l') AS UNSIGNED) * 60)
            + CAST(DATE_FORMAT(CONCAT(SCHED.game_date, ' ', SCHED.game_time), '%i') AS UNSIGNED) AS game_time,
           LINEUP.team_id, IF(LINEUP.team_id = SCHED.home, SCHED.visitor, SCHED.home) AS opp_team_id,
           LINEUP.team_id = SCHED.home AS is_home,
           NULL AS recency_order
    FROM tmp_links AS LINK
    JOIN debearco_sports.SPORTS_AHL_GAME_LINEUP AS LINEUP
      ON (LINEUP.season IN (v_season - 1, v_season)
      AND LINEUP.player_id = LINK.link_id
      AND LINEUP.game_type = 'regular')
    JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = LINEUP.season
      AND SCHED.game_type = LINEUP.game_type
      AND SCHED.game_id = LINEUP.game_id
      AND SCHED.game_date < v_start_date
      AND IFNULL(SCHED.status, 'STILLTOPLAY') NOT IN ('PPD', 'CNC', 'STILLTOPLAY'));

  # Fill the recency_order column
  CALL _duplicate_tmp_table('tmp_links_recent', 'tmp_links_recent_cpA');
  INSERT INTO tmp_links_recent (link_id, season, game_type, game_id, recency_order)
    SELECT SQL_BIG_RESULT link_id, season, game_type, game_id,
           ROW_NUMBER() OVER w AS recency_order
    FROM tmp_links_recent_cpA
    WINDOW w AS (PARTITION BY link_id ORDER BY link_id, game_time DESC)
  ON DUPLICATE KEY UPDATE recency_order = VALUES(recency_order);
  DROP TEMPORARY TABLE tmp_links_recent_cpA;

  # Limit the table to our requested size
  DELETE FROM tmp_links_recent WHERE recency_order > v_num_games;

END $$

DELIMITER ;

#
# Determine which "events" (games) each selection could have played
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_events`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_events`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection games played for ahl:ind sel'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_link_events;
  CREATE TEMPORARY TABLE tmp_link_events (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    jersey TINYINT UNSIGNED,
    event_order SMALLINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    stress TINYINT UNSIGNED,
    summary VARCHAR(50),
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE=MyISAM
    SELECT tmp_links.link_id, SCHED.season, SCHED.game_type, SCHED.game_id, LINEUP.team_id, LINEUP.jersey,
           (100 * (DATEDIFF(SCHED.game_date, CONCAT(SCHED.season, '-09-01')) + 1)) + DATE_FORMAT(SCHED.game_time, '%k') AS event_order,
           NULL AS stat, 'na' AS status, NULL AS stress, NULL AS summary
    FROM tmp_links
    JOIN debearco_sports.SPORTS_AHL_TEAMS_ROSTERS AS ROSTER
      ON (ROSTER.season = v_season
      AND ROSTER.the_date BETWEEN v_start_date AND v_end_date
      AND ROSTER.player_id = tmp_links.link_id)
    JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = ROSTER.season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_date = ROSTER.the_date
      AND ROSTER.team_id IN (SCHED.home, SCHED.visitor))
    JOIN debearco_sports.SPORTS_AHL_GAME_LINEUP AS LINEUP
      ON (LINEUP.season = SCHED.season
      AND LINEUP.game_type = SCHED.game_type
      AND LINEUP.game_id = SCHED.game_id
      AND LINEUP.player_id = ROSTER.player_id);

END $$

DELIMITER ;

#
# Determine which "events" (games) each selection could have played across a variety of periods
#
DROP PROCEDURE IF EXISTS `records_ahl_ind_events_historical`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_ind_events_historical`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker historical games played for ahl:ind sel'
BEGIN

  # Get the raw list of events for the requested selections in the appropriate period(s) by component part
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_periods;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_periods (
    period_id TINYINT UNSIGNED,
    start_date DATE,
    end_date DATE,
    PRIMARY KEY (period_id)
  ) ENGINE=MEMORY
    SELECT PERIOD.period_id, PERIOD.start_date, PERIOD.end_date
    FROM tmp_entry_sel_selperiod
    JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
      ON (PERIOD.sport = v_sport
      AND PERIOD.season = v_season
      AND PERIOD.period_id = tmp_entry_sel_selperiod.period_id)
    GROUP BY PERIOD.period_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_roster;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_roster (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    the_date DATE,
    team_id CHAR(3),
    PRIMARY KEY (link_id, period_id, the_date)
  ) ENGINE=MEMORY
    SELECT LINK.link_id, LINK.period_id, ROSTER.the_date, ROSTER.team_id
    FROM tmp_entry_sel_selperiod AS LINK
    JOIN tmp_entry_sel_selperiod_periods AS PERIOD
      ON (PERIOD.period_id = LINK.period_id)
    JOIN debearco_sports.SPORTS_AHL_TEAMS_ROSTERS AS ROSTER
      ON (ROSTER.season = v_season
      AND ROSTER.the_date BETWEEN PERIOD.start_date AND PERIOD.end_date
      AND ROSTER.player_id = LINK.link_id);

  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_sched;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_sched (
    period_id TINYINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    PRIMARY KEY (season, game_type, game_id, team_id),
    KEY by_period_team (period_id, team_id) USING BTREE
  ) ENGINE=MEMORY;
  INSERT INTO tmp_entry_sel_selperiod_sched
    SELECT PERIOD.period_id, SCHED.season, SCHED.game_type, SCHED.game_id, SCHED.home AS team_id, SCHED.visitor AS opp_team_id
    FROM tmp_entry_sel_selperiod_periods AS PERIOD
    JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = v_season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_date BETWEEN PERIOD.start_date AND PERIOD.end_date);
  INSERT IGNORE INTO tmp_entry_sel_selperiod_sched
    SELECT PERIOD.period_id, SCHED.season, SCHED.game_type, SCHED.game_id, SCHED.visitor AS team_id, SCHED.home AS opp_team_id
    FROM tmp_entry_sel_selperiod_periods AS PERIOD
    JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = v_season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_date BETWEEN PERIOD.start_date AND PERIOD.end_date);

  # Merge back together
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_sel_selperiod_events_raw;
  CREATE TEMPORARY TABLE tmp_entry_sel_selperiod_events_raw (
    link_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id, LINK.period_id,
           SCHED.season, SCHED.game_type, SCHED.game_id, SCHED.team_id, SCHED.opp_team_id
    FROM tmp_entry_sel_selperiod AS LINK
    JOIN tmp_entry_sel_selperiod_roster AS ROSTER
      ON (ROSTER.link_id = LINK.link_id
      AND ROSTER.period_id = LINK.period_id)
    JOIN tmp_entry_sel_selperiod_sched AS SCHED
      ON (SCHED.period_id = ROSTER.period_id
      AND SCHED.team_id = ROSTER.team_id)
    GROUP BY LINK.link_id, LINK.period_id, SCHED.game_id;

END $$

DELIMITER ;
