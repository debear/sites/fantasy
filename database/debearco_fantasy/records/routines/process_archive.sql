#
# Active leader in archived mode is the best across the whole season, not just the final one
#
DROP PROCEDURE IF EXISTS `records_archive_active`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_archive_active`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker game archiving processing'
BEGIN

  DECLARE v_leader SMALLINT UNSIGNED;
  DECLARE v_result DECIMAL(5,1) SIGNED;

  # Get the sporting leader and value
  SELECT active_id, active_score
    INTO v_leader, v_result
  FROM FANTASY_RECORDS_GAMES_PERIODS_SUMMARY
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id
  ORDER BY active_score DESC
  LIMIT 1;

  # Over-write this in our main game table
  UPDATE FANTASY_RECORDS_GAMES
  SET active_id = v_leader,
      active_score = v_result
  WHERE sport = v_sport
  AND   season = v_season
  AND   game_id = v_game_id;

END $$

DELIMITER ;
