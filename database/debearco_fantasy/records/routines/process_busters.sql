#
# Streak buster calculations
#
DROP PROCEDURE IF EXISTS `records_scoring_entry_busters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_scoring_entry_busters`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_sel_type ENUM('ind', 'team'),
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker streak buster calculations'
BEGIN

  DECLARE v_period_order TINYINT UNSIGNED;
  # Some thresholds in our calculations
  DECLARE v_threshold_min_opp_rating TINYINT UNSIGNED DEFAULT 70;
  DECLARE v_threshold_cutoff_user DECIMAL(3, 2) UNSIGNED DEFAULT 0.08;
  DECLARE v_threshold_cutoff_global DECIMAL(3, 2) UNSIGNED DEFAULT 0.04;
  DECLARE v_threshold_min_buster_ratio DECIMAL(3, 2) UNSIGNED DEFAULT 0.25;
  DECLARE v_min_autouser_id SMALLINT UNSIGNED DEFAULT 60351;

  # Up memory limit to 256meg for the temporary tables
  SET @@session.max_heap_table_size = 268435456; # 268,435,456 = 256 * 1024 * 1024
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

  # Determine the order this period is, as we're looking backwards from this point in time
  SELECT period_order INTO v_period_order
  FROM FANTASY_COMMON_PERIODS_DATES
  WHERE sport = v_sport
  AND   season = v_season
  AND   period_id = v_period_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_busters_periods;
  CREATE TEMPORARY TABLE tmp_busters_periods LIKE FANTASY_COMMON_PERIODS_DATES;
  ALTER TABLE tmp_busters_periods ENGINE = MEMORY;
  INSERT INTO tmp_busters_periods
    SELECT *
    FROM FANTASY_COMMON_PERIODS_DATES
    WHERE sport = v_sport
    AND   season = v_season
    AND   period_order <= v_period_order;

  # Build a selection-team mapping, which we'll process later
  DROP TEMPORARY TABLE IF EXISTS tmp_busters_sel;
  CREATE TEMPORARY TABLE tmp_busters_sel (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    status ENUM('positive','neutral','negative','na'),
    rating TINYINT UNSIGNED,
    opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (sport, season, game_id, period_id, link_id)
  ) ENGINE = MEMORY;
  IF v_sel_type = 'ind' THEN
    # Take a season snapshot of the roster table with standardised name
    DROP TEMPORARY TABLE IF EXISTS tmp_busters_roster;
    CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_busters_roster LIKE debearco_sports.SPORTS_', UPPER(v_sport), '_TEAMS_ROSTERS;'));
    CALL _exec(CONCAT('INSERT INTO tmp_busters_roster
      SELECT *
      FROM debearco_sports.SPORTS_', UPPER(v_sport), '_TEAMS_ROSTERS
      WHERE season = "', v_season, '";'));

    # Map individual players via the team roster
    INSERT INTO tmp_busters_sel
      SELECT SEL.sport, SEL.season, SEL.game_id, SEL.period_id, SEL.link_id, TEAM_ROSTER.team_id, SEL.status, SEL.rating, SEL.opp_rating
      FROM tmp_busters_periods AS PERIOD
      JOIN FANTASY_RECORDS_SELECTIONS AS SEL
        ON (SEL.sport = PERIOD.sport
        AND SEL.season = PERIOD.season
        AND SEL.game_id = v_game_id
        AND SEL.period_id = PERIOD.period_id
        AND SEL.status IS NOT NULL)
      JOIN tmp_busters_roster AS TEAM_ROSTER
        ON (TEAM_ROSTER.season = PERIOD.season
        AND TEAM_ROSTER.the_date = PERIOD.end_date
        AND TEAM_ROSTER.player_id = SEL.link_id);

  ELSE
    # Map teams via the existing ID>Team mapping table
    INSERT INTO tmp_busters_sel
      SELECT SEL.sport, SEL.season, SEL.game_id, SEL.period_id, SEL.link_id, SEL_MAP.raw_id AS team_id, SEL.status, SEL.rating, SEL.opp_rating
      FROM tmp_busters_periods AS PERIOD
      JOIN FANTASY_RECORDS_SELECTIONS AS SEL
        ON (SEL.sport = PERIOD.sport
        AND SEL.season = PERIOD.season
        AND SEL.game_id = v_game_id
        AND SEL.period_id = PERIOD.period_id
        AND SEL.status IS NOT NULL)
      JOIN FANTASY_COMMON_SELECTIONS_MAPPED AS SEL_MAP
        ON (SEL_MAP.sport = SEL.sport
        AND SEL_MAP.season = SEL.season
        AND SEL_MAP.link_id = SEL.link_id);
  END IF;
  ALTER TABLE tmp_busters_sel
    ADD KEY by_period (sport, season, period_id) USING BTREE;

  # Take a season snapshot of the schedule table with standardised name
  DROP TEMPORARY TABLE IF EXISTS tmp_busters_sched;
  CREATE TEMPORARY TABLE tmp_busters_sched (
    season YEAR,
    game_date DATE,
    team_id CHAR(3),
    opp_team_id CHAR(3),
    PRIMARY KEY (season, game_date, team_id)
  ) ENGINE = MEMORY;
  CALL _exec(CONCAT('INSERT INTO tmp_busters_sched (season, game_date, team_id, opp_team_id)
    SELECT season, game_date, home AS team_id, visitor AS opp_team_id
    FROM debearco_sports.SPORTS_', UPPER(v_sport), '_SCHEDULE
    WHERE season = "', v_season, '"
    AND   game_type = "regular"
    GROUP BY season, game_date, home;'));
  CALL _exec(CONCAT('INSERT IGNORE INTO tmp_busters_sched (season, game_date, team_id, opp_team_id)
    SELECT season, game_date, visitor AS team_id, home AS opp_team_id
    FROM debearco_sports.SPORTS_', UPPER(v_sport), '_SCHEDULE
    WHERE season = "', v_season, '"
    AND   game_type = "regular"
    GROUP BY season, game_date, visitor;'));

  # Convert game_date to the period_id
  ALTER TABLE tmp_busters_sched
    ADD COLUMN period_id TINYINT UNSIGNED AFTER season,
    ADD KEY by_period (season, period_id, team_id) USING BTREE;
  UPDATE tmp_busters_periods AS PERIOD
  JOIN tmp_busters_sched AS SCHED
    ON (SCHED.season = PERIOD.season
    AND SCHED.game_date BETWEEN PERIOD.start_date AND PERIOD.end_date)
  SET SCHED.period_id = PERIOD.period_id;

  # Append the opponent info to the selection list
  ALTER TABLE tmp_busters_sel ADD COLUMN opp_team_id VARCHAR(6);
  UPDATE tmp_busters_sel AS SEL
  JOIN tmp_busters_sched AS SCHED
    ON (SCHED.season = SEL.season
    AND SCHED.period_id = SEL.period_id
    AND SCHED.team_id = SEL.team_id)
  SET SEL.opp_team_id = SCHED.opp_team_id;

  # Correlate together to form the per-entry stats
  DROP TEMPORARY TABLE IF EXISTS tmp_busters_per_entry_sel;
  CREATE TEMPORARY TABLE tmp_busters_per_entry_sel (
    sport CHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    period_id TINYINT UNSIGNED,
    link_id SMALLINT UNSIGNED,
    status ENUM('positive','neutral','negative','na'),
    team_id CHAR(3),
    opp_team_id CHAR(3),
    rating TINYINT UNSIGNED,
    opp_rating TINYINT UNSIGNED,
    PRIMARY KEY (user_id, period_id)
  ) ENGINE = MEMORY
    SELECT ENTRY_SEL.sport, ENTRY_SEL.season, ENTRY_SEL.game_id, ENTRY_SEL.user_id,
           ENTRY_SEL.period_id, ENTRY_SEL.link_id, SEL.status,
           SEL.team_id, SEL.opp_team_id, SEL.rating, SEL.opp_rating
    FROM tmp_busters_periods AS PERIOD
    JOIN FANTASY_RECORDS_ENTRIES_BYPERIOD AS ENTRY_SEL
      ON (ENTRY_SEL.sport = PERIOD.sport
      AND ENTRY_SEL.season = PERIOD.season
      AND ENTRY_SEL.game_id = v_game_id
      AND ENTRY_SEL.period_id = PERIOD.period_id)
    JOIN tmp_busters_sel AS SEL
      ON (SEL.sport = ENTRY_SEL.sport
      AND SEL.season = ENTRY_SEL.season
      AND SEL.game_id = ENTRY_SEL.game_id
      AND SEL.period_id = ENTRY_SEL.period_id
      AND SEL.link_id = ENTRY_SEL.link_id);

  # Aggregate the counts
  DROP TEMPORARY TABLE IF EXISTS tmp_busters;
  CREATE TEMPORARY TABLE tmp_busters (
    sport CHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    opp_team_id CHAR(3),
    sel_tot MEDIUMINT UNSIGNED,
    sel_num MEDIUMINT UNSIGNED,
    sel_underdog MEDIUMINT UNSIGNED,
    PRIMARY KEY (sport, season, game_id, user_id, opp_team_id)
  ) SELECT sport, season, game_id, user_id, opp_team_id, COUNT(*) AS sel_tot, SUM(status = 'negative') AS sel_num, SUM(status = 'negative' AND opp_rating >= v_threshold_min_opp_rating) AS sel_underdog
    FROM tmp_busters_per_entry_sel
    GROUP BY sport, season, game_id, user_id, opp_team_id;
  # Determine a high-level overall count
  INSERT INTO tmp_busters
    SELECT sport, season, game_id, 0 AS user_id, opp_team_id, COUNT(*) AS sel_tot, SUM(status = 'negative') AS sel_num, SUM(status = 'negative' AND opp_rating >= v_threshold_min_opp_rating) AS sel_underdog
    FROM tmp_busters_per_entry_sel
    GROUP BY sport, season, game_id, opp_team_id;

  # Establish a base count level for which we will continue to analyse
  DROP TEMPORARY TABLE IF EXISTS tmp_busters_cutoff;
  CREATE TEMPORARY TABLE tmp_busters_cutoff (
    sport VARCHAR(6),
    season YEAR,
    game_id TINYINT UNSIGNED,
    user_id SMALLINT UNSIGNED,
    sel_count MEDIUMINT UNSIGNED,
    sel_cutoff MEDIUMINT UNSIGNED,
    PRIMARY KEY (sport, season, game_id, user_id)
  ) ENGINE = MEMORY
    SELECT sport, season, game_id, user_id,
          SUM(sel_tot) AS sel_count,
          FLOOR(SUM(sel_tot) * IF(user_id > 0, v_threshold_cutoff_user, v_threshold_cutoff_global)) AS sel_cutoff
    FROM tmp_busters
    GROUP BY sport, season, game_id, user_id;

  # Reduce our entry counts to only those with significant usage
  DELETE ENTRY.*
  FROM tmp_busters_cutoff AS CUTOFF
  JOIN tmp_busters AS ENTRY
    ON (ENTRY.sport = CUTOFF.sport
    AND ENTRY.season = CUTOFF.season
    AND ENTRY.game_id = CUTOFF.game_id
    AND ENTRY.user_id = CUTOFF.user_id)
  WHERE ENTRY.sel_underdog = 0
  OR    ENTRY.sel_tot < CUTOFF.sel_cutoff;

  # Convert to a ratio for laster analysis
  ALTER TABLE tmp_busters
    ADD COLUMN buster_ratio DECIMAL(6, 5) UNSIGNED;
  UPDATE tmp_busters SET buster_ratio = sel_underdog / sel_tot;

  # Include a lower bound for our ratio
  DELETE FROM tmp_busters WHERE buster_ratio <= v_threshold_min_buster_ratio;

  # Rank these remaining opponents
  ALTER TABLE tmp_busters ADD COLUMN sel_rank TINYINT UNSIGNED;
  CALL _duplicate_tmp_table('tmp_busters', 'tmp_busters_cp');
  INSERT INTO tmp_busters (sport, season, game_id, user_id, opp_team_id, sel_tot, sel_num, sel_underdog, sel_rank)
    SELECT SQL_BIG_RESULT sport, season, game_id, user_id, opp_team_id, sel_tot, sel_num, sel_underdog, RANK() OVER w AS sel_rank
    FROM tmp_busters_cp
    WINDOW w AS (PARTITION BY sport, season, game_id, user_id ORDER BY buster_ratio DESC, sel_tot DESC, opp_team_id)
  ON DUPLICATE KEY UPDATE sel_rank = VALUES(sel_rank);

  # We only consider the Top 3 per user/global, so strip any remaining entries we will no longer use
  DELETE FROM tmp_busters WHERE sel_rank > 3;

  # Write this back to our longer-lived table
  CALL _duplicate_tmp_table('tmp_busters', 'tmp_busters_cpA');
  CALL _duplicate_tmp_table('tmp_busters', 'tmp_busters_cpB');
  DELETE FROM FANTASY_RECORDS_ENTRIES_BUSTERS WHERE sport = v_sport AND season = v_season AND game_id = v_game_id;
  INSERT INTO FANTASY_RECORDS_ENTRIES_BUSTERS (sport, season, game_id, user_id, team1, team1_score, team2, team2_score, team3, team3_score)
    SELECT team1.sport, team1.season, team1.game_id, team1.user_id,
          team1.opp_team_id AS team1, ROUND(team1.buster_ratio * 100) AS team1_score,
          team2.opp_team_id AS team2, ROUND(team2.buster_ratio * 100) AS team2_score,
          team3.opp_team_id AS team3, ROUND(team3.buster_ratio * 100) AS team3_score
    FROM tmp_busters AS team1
    LEFT JOIN tmp_busters_cpA AS team2
      ON (team2.sport = team1.sport
      AND team2.season = team1.season
      AND team2.game_id = team1.game_id
      AND team2.user_id = team1.user_id
      AND team2.sel_rank = 2)
    LEFT JOIN tmp_busters_cpB AS team3
      ON (team3.sport = team1.sport
      AND team3.season = team1.season
      AND team3.game_id = team1.game_id
      AND team3.user_id = team1.user_id
      AND team3.sel_rank = 3)
    WHERE team1.sport = v_sport
    AND   team1.season = v_season
    AND   team1.game_id = v_game_id
    AND   team1.user_id < v_min_autouser_id
    AND   team1.sel_rank = 1;
  ALTER TABLE FANTASY_RECORDS_ENTRIES_BUSTERS ORDER BY sport, season, game_id, user_id;

  # Revert the memory change
  SET @@session.max_heap_table_size = DEFAULT; # DEFAULT = global value (i.e., what we had before...)
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

END $$

DELIMITER ;
