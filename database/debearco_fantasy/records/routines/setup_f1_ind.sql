#
# Selections
#
DROP PROCEDURE IF EXISTS `records_f1_ind_sel`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_sel`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selection lists for f1:ind'
BEGIN

  ALTER TABLE tmp_links
    ADD COLUMN status_multiplier DECIMAL(3,2) UNSIGNED;

  # Now populate
  TRUNCATE TABLE tmp_links;
  INSERT INTO tmp_links (link_id, link_sort, status_multiplier)
    SELECT DRIVER.driver_id AS link_id,
           debearco_sports.fn_basic_html_comparison(CONCAT(DRIVER.surname, ',', DRIVER.first_name)) AS link_sort,
           IF(SEL_STATUS.status IN ('ne', 'out', 'susp'), 0.15, 1.00) AS status_multiplier
    FROM debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
    JOIN debearco_sports.SPORTS_FIA_DRIVERS AS DRIVER
      ON (DRIVER.driver_id = RACE_DRIVER.driver_id)
    LEFT JOIN FANTASY_COMMON_MOTORS_SELECTIONS_STATUS AS SEL_STATUS
      ON (SEL_STATUS.sport = RACE_DRIVER.series
      AND SEL_STATUS.season = RACE_DRIVER.season
      AND SEL_STATUS.period_id = RACE_DRIVER.round
      AND SEL_STATUS.link_id = RACE_DRIVER.driver_id)
    WHERE RACE_DRIVER.season = v_season
    AND   RACE_DRIVER.series = v_sport
    AND   RACE_DRIVER.round = v_period_id;

  # Include selections that are not expected to be entered, but linked via status
  INSERT IGNORE INTO tmp_links (link_id, link_sort, status_multiplier)
    SELECT SEL_STATUS.link_id,
           debearco_sports.fn_basic_html_comparison(CONCAT(DRIVER.surname, ',', DRIVER.first_name))  AS link_sort,
           IF(SEL_STATUS.status IN ('ne', 'out', 'susp'), 0.15, 1.00) AS status_multiplier
    FROM FANTASY_COMMON_MOTORS_SELECTIONS_STATUS AS SEL_STATUS
    LEFT JOIN debearco_sports.SPORTS_FIA_DRIVERS AS DRIVER
      ON (DRIVER.driver_id = SEL_STATUS.link_id)
    WHERE SEL_STATUS.sport = v_sport
    AND   SEL_STATUS.season = v_season
    AND   SEL_STATUS.period_id = v_period_id;

END $$

DELIMITER ;

#
# Determine which "events" (races) each selection could have played
#
DROP PROCEDURE IF EXISTS `records_f1_ind_events`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_ind_events`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection games played for f1:ind sel'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_link_events;
  CREATE TEMPORARY TABLE tmp_link_events (
    link_id SMALLINT UNSIGNED,
    season YEAR,
    round TINYINT UNSIGNED,
    race TINYINT UNSIGNED,
    event_order SMALLINT UNSIGNED,
    car_no TINYINT UNSIGNED,
    stat DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    stress TINYINT UNSIGNED,
    summary VARCHAR(50),
    PRIMARY KEY (link_id, season, round)
  ) ENGINE=MyISAM
    SELECT tmp_links.link_id, RACE_DRIVER.season, RACE_DRIVER.round, IF(RACE.race2_time IS NOT NULL, 2, 1) AS race, 1 AS event_order, RACE_DRIVER.car_no,
           NULL AS stat, 'na' AS status, NULL AS stress, NULL AS summary
    FROM tmp_links
    JOIN debearco_sports.SPORTS_FIA_RACES AS RACE
      ON (RACE.season = v_season
      AND RACE.series = v_sport
      AND RACE.round = v_period_id)
    JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = RACE.season
      AND RACE_DRIVER.series = RACE.series
      AND RACE_DRIVER.round = RACE.round
      AND RACE_DRIVER.driver_id = tmp_links.link_id);

END $$

DELIMITER ;
