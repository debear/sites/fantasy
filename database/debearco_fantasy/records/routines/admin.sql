##
## Prune a game from the database when setting up
##
DROP PROCEDURE IF EXISTS `records_setup_reset`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_setup_reset`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_twitter_app VARCHAR(30)
)
    COMMENT 'Record Breaker game pruning when setting up a new game'
BEGIN

  # Game agnostic calls
  DELETE FROM `FANTASY_RECORDS_GAMES` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_META` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_PERIODS_SUMMARY` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_RULES` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_POSITIONS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_STATS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_ENTRIES_SCORES` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_ENTRIES_BYPERIOD` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_ENTRIES_BUSTERS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_ENTRIES_DROPOFF` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_SELECTIONS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_SELECTIONS_SCORING` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_SELECTIONS_STATS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_SETUP_EMAIL` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_RECORDS_SETUP_TWITTER` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_COMMON_PERIODS_DATES` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_COMMON_SELECTIONS_MAPPED` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `debearco_common`.`COMMS_TWITTER_TEMPLATE` WHERE `app` = v_twitter_app AND `tweet_type` LIKE CONCAT(v_sport, '\_', v_season, '%');
  DELETE FROM `FANTASY_PROFILE_RECORD_GAMES` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS` WHERE `sport` = v_sport AND `season` = v_season;
  DELETE FROM `FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS_LEVELS` WHERE `sport` = v_sport AND `season` = v_season;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `records_setup_profiles`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_setup_profiles`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Record Breaker profile config when setting up a new game'
BEGIN

  # Games
  INSERT INTO FANTASY_PROFILE_RECORD_GAMES (sport, season, game_id, game_name, target, in_progress)
    SELECT sport, season, game_id, name AS game_name, target, 1 AS in_progress
    FROM FANTASY_RECORDS_GAMES
    WHERE sport = v_sport
    AND   season = v_season;

  # Achievements
  INSERT INTO FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS (sport, season, game_id, achieve_id, name, disp_order)
    SELECT sport, season, game_id, achieve_id, name, disp_order
    FROM FANTASY_RECORDS_GAMES_ACHIEVEMENTS
    WHERE sport = v_sport
    AND   season = v_season;
  INSERT INTO FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS_LEVELS (sport, season, game_id, achieve_id, level, value, bit_flag, disp_order)
    SELECT sport, season, game_id, achieve_id, level, value, bit_flag, disp_order
    FROM FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS
    WHERE sport = v_sport
    AND   season = v_season;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `records_setup_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_setup_order`()
    COMMENT 'Record Breaker game pruning when setting up a new game'
BEGIN

  ALTER TABLE `FANTASY_RECORDS_GAMES` ORDER BY `sport`, `season`, `game_id`;
  ALTER TABLE `FANTASY_RECORDS_GAMES_RULES` ORDER BY `sport`, `season`, `game_id`;
  ALTER TABLE `FANTASY_RECORDS_GAMES_POSITIONS` ORDER BY `sport`, `season`, `game_id`, `disp_order`;
  ALTER TABLE `FANTASY_RECORDS_GAMES_STATS` ORDER BY `sport`, `season`, `game_id`, `stat_id`;
  ALTER TABLE `FANTASY_RECORDS_GAMES_ACHIEVEMENTS` ORDER BY `sport`, `season`, `game_id`, `achieve_id`;
  ALTER TABLE `FANTASY_RECORDS_GAMES_ACHIEVEMENTS_LEVELS` ORDER BY `sport`, `season`, `game_id`, `bit_flag`;
  ALTER TABLE `FANTASY_RECORDS_ENTRIES_SCORES` ORDER BY `sport`, `season`, `game_id`, `user_id`;
  ALTER TABLE `FANTASY_RECORDS_SETUP_TWITTER` ORDER BY `sport`, `season`, `game_id`, `date`, `tweet_type`;
  ALTER TABLE `FANTASY_COMMON_PERIODS_DATES` ORDER BY `sport`, `season`, `period_id`;
  ALTER TABLE `FANTASY_COMMON_SELECTIONS_MAPPED` ORDER BY `sport`, `season`, `link_id`;
  ALTER TABLE `debearco_common`.`COMMS_TWITTER_TEMPLATE` ORDER BY `app`, `tweet_type`, `template_id`;
  ALTER TABLE `FANTASY_PROFILE_RECORD_GAMES` ORDER BY `sport`, `season`, `game_id`;
  ALTER TABLE `FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS` ORDER BY `sport`, `season`, `game_id`, `disp_order`;
  ALTER TABLE `FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS_LEVELS` ORDER BY `sport`, `season`, `game_id`, `achieve_id`, `disp_order`;

END $$

DELIMITER ;
