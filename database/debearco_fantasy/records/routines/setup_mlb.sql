#
# MLB game-agnostic logic
#

#
# Doubleheader Achievements
#
DROP PROCEDURE IF EXISTS `records_mlb_achievement_events_1`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_mlb_achievement_events_1`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_bit_flag TINYINT UNSIGNED
)
    COMMENT 'Record Breaker entry achievement calcs for MLB doubleheaders'
BEGIN

  # Determine the successful selections
  DROP TEMPORARY TABLE IF EXISTS tmp_entry_achieve_dh_success;
  CREATE TEMPORARY TABLE tmp_entry_achieve_dh_success (
    sport VARCHAR(6) NOT NULL,
    season YEAR NOT NULL,
    game_id TINYINT UNSIGNED NOT NULL,
    link_id SMALLINT UNSIGNED NOT NULL,
    period_id TINYINT UNSIGNED NOT NULL,
    PRIMARY KEY (sport, season, game_id, link_id, period_id)
  ) ENGINE=MyISAM
    SELECT sport, season, game_id, link_id, period_id
    FROM FANTASY_RECORDS_SELECTIONS_SCORING
    WHERE sport = v_sport
    AND   season = v_season
    AND   game_id = v_game_id
    AND   status IN ('positive', 'neutral')
    GROUP BY sport, season, game_id, link_id, period_id
    HAVING COUNT(DISTINCT event_order) > 1;

  # And merge back on a per-user
  INSERT INTO tmp_entry_achievements (user_id, bit_flags)
    SELECT DISTINCT ENTRY_SEL.user_id, v_bit_flag
    FROM tmp_entry_achieve_dh_success AS SEL
    JOIN tmp_entry_achievements_info AS ENTRY_SEL
      ON (ENTRY_SEL.sport = SEL.sport
      AND ENTRY_SEL.season = SEL.season
      AND ENTRY_SEL.game_id = SEL.game_id
      AND ENTRY_SEL.period_id = SEL.period_id
      AND ENTRY_SEL.link_id = SEL.link_id);

END $$

DELIMITER ;
