##
## Game Post-processing
##
DROP PROCEDURE IF EXISTS `records_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_process`(
  v_sport VARCHAR(6),
  v_date DATE
)
    COMMENT 'Record Breaker selection processing calcs for a given period'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_game_id TINYINT UNSIGNED;
  DECLARE v_period_id TINYINT UNSIGNED;
  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_ts DECIMAL(8,3) UNSIGNED;

  # Loop through the games to be calculated
  DECLARE cur_games CURSOR FOR
    SELECT GAME.season, GAME.game_id, PERIOD.period_id, PERIOD.start_date, PERIOD.end_date
    FROM FANTASY_RECORDS_GAMES AS GAME
    JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
      ON (PERIOD.sport = GAME.sport
      AND PERIOD.season = GAME.season
      AND v_date BETWEEN PERIOD.start_date AND PERIOD.end_date)
    LEFT JOIN FANTASY_RECORDS_GAMES_PERIODS_SUMMARY AS SUMMARY
      ON (SUMMARY.sport = GAME.sport
      AND SUMMARY.season = GAME.season
      AND SUMMARY.game_id = GAME.game_id
      AND SUMMARY.period_id = PERIOD.period_id)
    WHERE GAME.sport = v_sport
    AND   SUMMARY.period_id IS NULL
    ORDER BY GAME.game_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Major League processing occurs for games completed the day before
  IF sport_is_major_league(v_sport) THEN
    SET v_date := DATE_SUB(v_date, INTERVAL 1 DAY);
  END IF;

  CALL _log(CONCAT('Starting the processing calcs for sport ', v_sport, ' on ', v_date));

  # Loop through the active games, and calc:
  OPEN cur_games;
  loop_games: LOOP

    FETCH cur_games INTO v_season, v_game_id, v_period_id, v_start_date, v_end_date;
    IF v_done = 1 THEN LEAVE loop_games; END IF;

    # Process if this period has completed
    IF sport_period_complete(v_sport, v_season, v_period_id, v_start_date, v_end_date, v_date) THEN
      CALL _log(CONCAT('Processing ', v_sport, '-', v_season, '-', v_game_id, ' period ', v_period_id));
      SET v_ts = _NOW_MILLISEC();
      CALL records_process_game(v_sport, v_season, v_game_id, v_period_id);
      CALL _log(CONCAT('Processing ', v_sport, '-', v_season, '-', v_game_id, ' period ', v_period_id, ' complete (', (_NOW_MILLISEC() - v_ts), 's)'));
    ELSE
      CALL _log(CONCAT('Skipping as ', v_sport, '-', v_season, '-', v_game_id, ' period ', v_period_id, ' is still not complete'));
    END IF;

  END LOOP loop_games;
  CLOSE cur_games;

  # Log if no games found to process
  IF v_season IS NULL THEN
    CALL _log(CONCAT('Skipping - no games found for processing on ', v_date));
  END IF;

END $$

DELIMITER ;

##
## Post-processing calcs for a specific game
##
DROP PROCEDURE IF EXISTS `records_process_game`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_process_game`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker selection processing calcs for a given game'
BEGIN

  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;
  DECLARE v_sel_type ENUM('ind', 'team');
  DECLARE v_score_type ENUM('total', 'streak', 'periods');
  DECLARE v_sel_reuse TINYINT UNSIGNED;
  DECLARE v_active_limit TINYINT UNSIGNED;
  DECLARE v_fn VARCHAR(50);
  DECLARE v_period_rem TINYINT UNSIGNED;
  DECLARE v_ts_a DECIMAL(8,3) SIGNED;
  DECLARE v_ts_b DECIMAL(8,3) SIGNED;
  SET v_ts_a = _NOW_MILLISEC();

  # Determine additional info for our processing
  SELECT PERIOD.start_date, PERIOD.end_date, GAME.sel_type, GAME.score_type, GAME.active_limit,
         FIND_IN_SET('SEL_SINGLE_USE', GAME.config_flags) = 0 AS sel_reuse,
         CONCAT(GAME.sport, '_', GAME.sel_type, '_', GAME.game_code, '_', GAME.score_type) AS calc_method,
         COUNT(DISTINCT PERIOD_REM.period_id) AS period_rem
    INTO v_start_date, v_end_date, v_sel_type, v_score_type, v_active_limit, v_sel_reuse, v_fn, v_period_rem
  FROM FANTASY_RECORDS_GAMES AS GAME
  JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD
    ON (PERIOD.sport = GAME.sport
    AND PERIOD.season = GAME.season
    AND PERIOD.period_id = v_period_id)
  LEFT JOIN FANTASY_COMMON_PERIODS_DATES AS PERIOD_REM
    ON (PERIOD_REM.sport = PERIOD.sport
    AND PERIOD_REM.season = PERIOD.season
    AND PERIOD_REM.period_order > PERIOD.period_order)
  WHERE GAME.sport = v_sport
  AND   GAME.season = v_season
  AND   GAME.game_id = v_game_id
  GROUP BY GAME.game_id;

  # Determine the selections for our automated users in this round
  CALL records_autouser_sel(v_sport, v_season, v_game_id, v_period_id);
  SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- AutoUser Selection complete (', (v_ts_b - v_ts_a), 's)'));

  # Backfill preview stats (for historical reference) of selections added as part of the sports processing of the period (e.g., new players added to rosters)
  IF sport_is_major_league(v_sport) THEN
    CALL _log('- Backfilling missing selection previews');
    CALL records_preview_game(v_sport, v_season, v_game_id, v_period_id, 'missing-only');
    SET v_ts_a = _NOW_MILLISEC(); CALL _log(CONCAT('- Backfill Selections complete (', (v_ts_a - v_ts_b), 's)'));
  ELSE
    SET v_ts_a = v_ts_b; # Preserve A/B benchmarking timestamps
  END IF;

  # Some preparatory tasks
  CALL records_process_setup(v_sport, v_season, v_game_id, v_period_id);
  CALL records_process_step(CONCAT(v_sport, '_', v_sel_type), 'events', v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Setup complete (', (v_ts_b - v_ts_a), 's)'));

  # Individual selection scoring update
  CALL records_process_step(v_fn, 'scoring', v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_process_step(v_fn, 'stress', v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_scoring_sel_prep(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_scoring_sel_usage(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_process_step('scoring_sel', CONCAT('proc_', v_score_type), v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_scoring_sel_store(v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  SET v_ts_a = _NOW_MILLISEC(); CALL _log(CONCAT('- Selections complete (', (v_ts_a - v_ts_b), 's)'));

  # Entry scoring update
  CALL records_scoring_entry_prep(v_sport, v_season, v_game_id, v_period_id);
  CALL records_process_step('scoring_entry', v_score_type, v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_scoring_entry_store(v_sport, v_season, v_game_id, v_period_id);
  CALL records_scoring_entry_scores(v_sport, v_season, v_game_id, v_period_id);
  IF v_sel_reuse AND v_score_type IN ('total', 'streak') THEN
    CALL records_scoring_entry_hitrate(v_sport, v_season, v_game_id, v_period_id);
  END IF;
  IF v_active_limit IS NOT NULL THEN
    CALL records_scoring_entry_dropoff(v_sport, v_season, v_game_id, v_period_id, v_active_limit);
  END IF;
  IF sport_is_major_league(v_sport) AND v_score_type = 'streak' THEN
    CALL records_scoring_entry_busters(v_sport, v_season, v_game_id, v_sel_type, v_period_id);
  END IF;
  IF sport_is_major_league(v_sport) THEN
    CALL records_scoring_entry_opponents(v_sport, v_season, v_game_id, v_sel_type, v_period_id);
  END IF;
  CALL records_scoring_entry_achievements(v_sport, v_season, v_game_id, v_period_id);
  CALL records_scoring_entry_standings(v_sport, v_season, v_game_id, v_period_id);
  CALL records_scoring_entry_profile(v_sport, v_season, v_game_id, v_period_id);
  SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Entries complete (', (v_ts_b - v_ts_a), 's)'));

  # Period summary
  CALL records_scoring_summary(v_sport, v_season, v_game_id, v_period_id);
  SET v_ts_a = _NOW_MILLISEC(); CALL _log(CONCAT('- Period complete (', (v_ts_a - v_ts_b), 's)'));

  # Sporting leaders
  CALL records_scoring_active_prep(v_sport, v_season, v_game_id, v_period_id);
  CALL records_process_step('scoring_active', v_score_type, v_sport, v_season, v_game_id, v_period_id, v_start_date, v_end_date);
  CALL records_scoring_active_store(v_sport, v_season, v_game_id, v_period_id);
  SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Sporting complete (', (v_ts_b - v_ts_a), 's)'));

  # Post-processing steps that occur after processing the final round
  IF v_period_rem = 0 THEN
    CALL records_archive_active(v_sport, v_season, v_game_id);
  END IF;

END $$

DELIMITER ;

##
## Scoring calc worker processing method
##
DROP PROCEDURE IF EXISTS `records_process_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_process_setup`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED
)
    COMMENT 'Record Breaker post-processing internal setup'
BEGIN

  # Reset a previous scoring update
  UPDATE FANTASY_RECORDS_SELECTIONS
  SET score = NULL, status = NULL, stress = NULL, summary = NULL
  WHERE sport = v_sport AND season = v_season AND game_id = v_game_id AND period_id = v_period_id;

  DELETE FROM FANTASY_RECORDS_SELECTIONS_SCORING
  WHERE sport = v_sport AND season = v_season AND game_id = v_game_id AND period_id = v_period_id;

  UPDATE FANTASY_RECORDS_ENTRIES_BYPERIOD
  SET result = NULL, stress = NULL
  WHERE sport = v_sport AND season = v_season AND game_id = v_game_id AND period_id = v_period_id;

  DELETE FROM FANTASY_RECORDS_GAMES_PERIODS_SUMMARY
  WHERE sport = v_sport AND season = v_season AND game_id = v_game_id AND period_id = v_period_id;

  # Build our temporary table, populated with our original selections
  DROP TEMPORARY TABLE IF EXISTS tmp_links;
  CREATE TEMPORARY TABLE tmp_links (
    link_id SMALLINT UNSIGNED,
    raw_id CHAR(5) DEFAULT NULL,
    score DECIMAL(5,1) SIGNED,
    status ENUM('positive','neutral','negative','na'),
    stress TINYINT(3) UNSIGNED,
    summary VARCHAR(50),
    sel_pct DECIMAL(6,3) UNSIGNED,
    PRIMARY KEY (link_id)
  ) SELECT SEL.link_id, MAP.raw_id, NULL AS score, NULL AS status, NULL AS stress, NULL AS summary, NULL AS sel_pct
    FROM FANTASY_RECORDS_SELECTIONS AS SEL
    LEFT JOIN FANTASY_COMMON_SELECTIONS_MAPPED AS MAP
      ON (MAP.sport = SEL.sport
      AND MAP.season = SEL.season
      AND MAP.link_id = SEL.link_id)
    WHERE SEL.sport = v_sport
    AND   SEL.season = v_season
    AND   SEL.game_id = v_game_id
    AND   SEL.period_id = v_period_id;

END $$

DELIMITER ;

##
## Worker method for a specific step
##
DROP PROCEDURE IF EXISTS `records_process_step`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_process_step`(
  v_fn VARCHAR(100),
  v_step VARCHAR(30),
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker post-processing worker step'
BEGIN

  # This essentially duplicates an _exec call, but without the recursive limit
  SET @v_sql := CONCAT('CALL records_', v_fn , '_', v_step, '("', v_sport, '", "', v_season, '", "', v_game_id, '", "', v_period_id, '", "', v_start_date, '", "', v_end_date, '");');
  PREPARE sth_step FROM @v_sql;
  EXECUTE sth_step;
  DEALLOCATE PREPARE sth_step;

END $$

DELIMITER ;
