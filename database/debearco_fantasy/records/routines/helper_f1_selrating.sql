#
# Selection Rating preview helper methods
#
DROP PROCEDURE IF EXISTS `records_f1_selrating_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_selrating_setup`(
  v_season SMALLINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating helper setup'
BEGIN

  # Determine the previous races to process
  CALL sport_f1_recent_races(v_season, v_start_date, 25);

  # Penalise older races by giving these races a higher multiplier
  ALTER TABLE tmp_races
    ADD COLUMN race_multiplier DECIMAL(2, 1) UNSIGNED;
  UPDATE tmp_races SET race_multiplier = 1 + ((v_season - season) * 0.3);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `records_f1_selrating_proc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_f1_selrating_proc`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_team_rtg_ratio DECIMAL(3, 2) UNSIGNED,
  v_driver_rtg_ratio DECIMAL(3, 2) UNSIGNED,
  v_stat_dir ENUM('ASC', 'DESC')
)
    COMMENT 'Record Breaker selrating helper processor'
BEGIN

  DECLARE v_no_team_rtg DECIMAL(6, 4) UNSIGNED;

  # Determine our fallback team rating for new teams
  SELECT MAX(avg_rtg) * 1.05 INTO v_no_team_rtg FROM tmp_team_results;

  # Merge together, ensuring all
  DROP TEMPORARY TABLE IF EXISTS tmp_link_rating;
  CREATE TEMPORARY TABLE tmp_link_rating (
    link_id SMALLINT UNSIGNED,
    team_rtg DECIMAL(6, 4) UNSIGNED,
    driver_rtg DECIMAL(6, 4) SIGNED,
    abs_rtg DECIMAL(6, 4) SIGNED,
    rtg TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE = MyISAM
    SELECT tmp_links.link_id,
           IFNULL(tmp_team_results.avg_rtg, v_no_team_rtg) AS team_rtg,
           IFNULL(tmp_driver_results.rel_rtg, 0) AS driver_rtg,
           NULL AS abs_rtg,
           NULL AS rtg
    FROM tmp_links
    JOIN debearco_sports.SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
      ON (RACE_DRIVER.season = v_season
      AND RACE_DRIVER.series = v_sport
      AND RACE_DRIVER.round = v_period_id
      AND RACE_DRIVER.driver_id = tmp_links.link_id)
    LEFT JOIN tmp_team_results
      ON (tmp_team_results.team_id = RACE_DRIVER.team_id)
    LEFT JOIN tmp_driver_results
      ON (tmp_driver_results.driver_id = RACE_DRIVER.driver_id);

  # Combine team and driver components into an absolute value
  UPDATE tmp_link_rating SET abs_rtg = (team_rtg * v_team_rtg_ratio) + (driver_rtg * v_driver_rtg_ratio);

  # Ensure the value fits into some form of statistical relevance range and in a 0..100 range
  CALL _statistical_range('tmp_link_rating', 'abs_rtg', 'rtg', NULL, 0, 100, v_stat_dir);

  # And then write back
  UPDATE FANTASY_RECORDS_SELECTIONS AS SEL
  LEFT JOIN tmp_link_rating
    ON (tmp_link_rating.link_id = SEL.link_id)
  SET SEL.rating = IFNULL(tmp_link_rating.rtg, 33)
  WHERE SEL.sport = v_sport
  AND   SEL.season = v_season
  AND   SEL.game_id = v_game_id
  AND   SEL.period_id = v_period_id;

  # Revert our local changes to tmp_races
  ALTER TABLE tmp_races
    DROP COLUMN race_multiplier;

END $$

DELIMITER ;
