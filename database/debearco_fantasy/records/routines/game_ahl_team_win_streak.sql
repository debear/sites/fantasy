##
## X Game Win Streak
##

#
# Stats preview
#
DROP PROCEDURE IF EXISTS `records_ahl_team_win_streak_preview`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_team_win_streak_preview`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker preview stat calcs for ahl:team:win:streak'
BEGIN

  DECLARE v_standings_date DATE;
  DECLARE v_end_date DATE;
  DECLARE v_stat_id TINYINT UNSIGNED;

  SELECT MAX(the_date) INTO v_standings_date
  FROM debearco_sports.SPORTS_AHL_STANDINGS
  WHERE season = v_season
  AND   the_date < v_start_date;

  # Streamline our standings processing
  DROP TEMPORARY TABLE IF EXISTS tmp_standings;
  CREATE TEMPORARY TABLE tmp_standings (
    link_id SMALLINT UNSIGNED,
    overall_pct DOUBLE,
    overall_text VARCHAR(30),
    recent_pct DOUBLE,
    recent_text VARCHAR(30),
    home_pct DOUBLE,
    home_text VARCHAR(30),
    visitor_pct DOUBLE,
    visitor_text VARCHAR(30),
    PRIMARY KEY (link_id)
  ) SELECT SEL_MAP.link_id,
           IF(STAND.wins + STAND.loss + STAND.ot_loss + STAND.so_loss = 0, NULL,
              ((2 * STAND.wins) + STAND.ot_loss + STAND.so_loss) / (STAND.wins + STAND.loss + STAND.ot_loss + STAND.so_loss)) AS overall_pct,
           CONCAT(STAND.wins, '&ndash;', STAND.loss, '&ndash;', STAND.ot_loss, '&ndash;', STAND.so_loss) AS overall_text,
           IF(STAND.recent_wins + STAND.recent_loss + STAND.recent_ot_loss + STAND.recent_so_loss = 0, NULL,
              ((2 * STAND.recent_wins) + STAND.recent_ot_loss + STAND.recent_so_loss) / (STAND.recent_wins + STAND.recent_loss + STAND.recent_ot_loss + STAND.recent_so_loss)) AS recent_pct,
           CONCAT(STAND.recent_wins, '&ndash;', STAND.recent_loss, '&ndash;', STAND.recent_ot_loss, '&ndash;', STAND.recent_so_loss) AS recent_text,
           IF(STAND.home_wins + STAND.home_loss + STAND.home_ot_loss + STAND.home_so_loss = 0, NULL,
              ((2 * STAND.home_wins) + STAND.home_ot_loss + STAND.home_so_loss) / (STAND.home_wins + STAND.home_loss + STAND.home_ot_loss + STAND.home_so_loss)) AS home_pct,
           CONCAT(STAND.home_wins, '&ndash;', STAND.home_loss, '&ndash;', STAND.home_ot_loss, '&ndash;', STAND.home_so_loss) AS home_text,
           IF(STAND.visitor_wins + STAND.visitor_loss + STAND.visitor_ot_loss + STAND.visitor_so_loss = 0, NULL,
              ((2 * STAND.visitor_wins) + STAND.visitor_ot_loss + STAND.visitor_so_loss) / (STAND.visitor_wins + STAND.visitor_loss + STAND.visitor_ot_loss + STAND.visitor_so_loss)) AS visitor_pct,
           CONCAT(STAND.visitor_wins, '&ndash;', STAND.visitor_loss, '&ndash;', STAND.visitor_ot_loss, '&ndash;', STAND.visitor_so_loss) AS visitor_text
    FROM FANTASY_COMMON_SELECTIONS_MAPPED AS SEL_MAP
    LEFT JOIN debearco_sports.SPORTS_AHL_STANDINGS AS STAND
      ON (STAND.season = v_season
      AND STAND.the_date = v_standings_date
      AND STAND.team_id = SEL_MAP.raw_id)
    WHERE SEL_MAP.sport = v_sport
    AND   SEL_MAP.season = v_season;

  # Add our extra columns to the temporary processing table
  ALTER TABLE tmp_link_stats
    ADD COLUMN overall_value DOUBLE,
    ADD COLUMN overall_text VARCHAR(30),
    ADD COLUMN recent_value DOUBLE,
    ADD COLUMN recent_text VARCHAR(30),
    ADD COLUMN homeroad_value DOUBLE,
    ADD COLUMN homeroad_text VARCHAR(30);

  # Calculate the various team record values
  INSERT INTO tmp_link_stats (link_id, stat_value, stat_text, stat_order, overall_value, overall_text, recent_value, recent_text, homeroad_value, homeroad_text)
    SELECT tmp_links.link_id, NULL AS stat_value, NULL AS stat_text, NULL AS stat_order,
           IFNULL(tmp_standings.overall_pct, -0.1) AS overall_value,
           IFNULL(tmp_standings.overall_text, '0&ndash;0') AS overall_text,
           IFNULL(tmp_standings.recent_pct, -0.1) AS recent_value,
           IFNULL(tmp_standings.recent_text, '0&ndash;0') AS recent_text,
           IFNULL(IF(tmp_links.is_home, tmp_standings.home_pct, tmp_standings.visitor_pct), -0.1) AS homeroad_value,
           IFNULL(IF(tmp_links.is_home, tmp_standings.home_text, tmp_standings.visitor_text), '0&ndash;0') AS homeroad_text
    FROM tmp_links
    JOIN tmp_standings
      ON (tmp_standings.link_id = tmp_links.link_id);

  # Now process the individual stat elements
  # 1 - Record
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'record') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = overall_value,
        stat_text = overall_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 2 - Recent Games
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'recent') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = recent_value,
        stat_text = recent_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;
  # 3 - Home/Road Record
  SELECT records_get_stat_id(v_sport, v_season, v_game_id, 'home-road') INTO v_stat_id;
  IF v_stat_id IS NOT NULL THEN
    UPDATE tmp_link_stats
    SET stat_value = homeroad_value,
        stat_text = homeroad_text;
    CALL records_preview_proc(v_sport, v_season, v_game_id, v_period_id, v_stat_id, 'DESC');
  END IF;

  # Restore our temporary table structure
  ALTER TABLE tmp_link_stats
    DROP COLUMN overall_value,
    DROP COLUMN overall_text,
    DROP COLUMN recent_value,
    DROP COLUMN recent_text,
    DROP COLUMN homeroad_value,
    DROP COLUMN homeroad_text;

END $$

DELIMITER ;

#
# Opponent Rating preview
#
DROP PROCEDURE IF EXISTS `records_ahl_team_win_streak_opprating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_team_win_streak_opprating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker opprating stat calcs for ahl:team:win:streak'
BEGIN

  # Determine the recent games for each team we will use in our calculations
  CALL sport_majorleague_recent_sched(v_sport, v_season, v_start_date, 40);

  # Calc 1: Pythogrean record
  DROP TEMPORARY TABLE IF EXISTS tmp_team_oppcalcs;
  CREATE TEMPORARY TABLE tmp_team_oppcalcs (
    team_id CHAR(3),
    gp TINYINT UNSIGNED,
    stat_gf SMALLINT UNSIGNED,
    stat_ga SMALLINT UNSIGNED,
    stat_gm DECIMAL(5, 3) UNSIGNED,
    home_gp TINYINT UNSIGNED,
    home_stat SMALLINT UNSIGNED,
    home_stat_gf SMALLINT UNSIGNED,
    home_stat_ga SMALLINT UNSIGNED,
    home_stat_gm DECIMAL(5, 3) UNSIGNED,
    visitor_gp TINYINT UNSIGNED,
    visitor_stat SMALLINT UNSIGNED,
    visitor_stat_gf SMALLINT UNSIGNED,
    visitor_stat_ga SMALLINT UNSIGNED,
    visitor_stat_gm DECIMAL(5, 3) UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE=MEMORY
    SELECT TEAMS.team_id, NULL AS gp, NULL AS stat_gf, NULL AS stat_ga, NULL AS stat_gm,
           NULL AS home_gp, NULL AS home_stat,
           SUM(IF(SCHED.is_home = 1, GAME.home_score, 0)) AS home_stat_gf,
           SUM(IF(SCHED.is_home = 1, GAME.visitor_score, 0)) AS home_stat_ga,
           NULL AS home_stat_gm,
           NULL AS visitor_gp, NULL AS visitor_stat,
           SUM(IF(SCHED.is_home = 0, GAME.visitor_score, 0)) AS visitor_stat_gf,
           SUM(IF(SCHED.is_home = 0, GAME.home_score, 0)) AS visitor_stat_ga,
           NULL AS visitor_stat_gm
    FROM tmp_all_teams AS TEAMS
    LEFT JOIN tmp_team_sched AS SCHED
      ON (SCHED.team_id = TEAMS.team_id)
    LEFT JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS GAME
      ON (GAME.season = SCHED.season
      AND GAME.game_type = SCHED.game_type
      AND GAME.game_id = SCHED.game_id)
    GROUP BY TEAMS.team_id;

  # Merge the scoring values
  UPDATE tmp_team_oppcalcs
  SET stat_gf = home_stat_gf + visitor_stat_gf,
      stat_ga = home_stat_ga + visitor_stat_ga;

  # Determine our pythagorean projection
  UPDATE tmp_team_oppcalcs
  SET stat_gm = IF(stat_gf + stat_ga > 0, POW(stat_gf, 2.11) / (POW(stat_gf, 2.11) + POW(stat_ga, 2.11)), 0),
      home_stat_gm = IF(home_stat_gf + home_stat_ga > 0, POW(home_stat_gf, 2.11) / (POW(home_stat_gf, 2.11) + POW(home_stat_ga, 2.11)), 0),
      visitor_stat_gm = IF(visitor_stat_gf + visitor_stat_ga > 0, POW(visitor_stat_gf, 2.11) / (POW(visitor_stat_gf, 2.11) + POW(visitor_stat_ga, 2.11)), 0);

  # Streamline our table
  ALTER TABLE tmp_team_oppcalcs
    DROP COLUMN stat_gf,
    DROP COLUMN stat_ga,
    DROP COLUMN home_stat_gf,
    DROP COLUMN home_stat_ga,
    DROP COLUMN visitor_stat_gf,
    DROP COLUMN visitor_stat_ga;

  # Process
  CALL records_opprating_proc(0.4, 'ASC'); # 60/40 Overall-vs-Home/Road, Lower Record makes for a better opponent
  CALL records_opprating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection Rating preview
#
DROP PROCEDURE IF EXISTS `records_ahl_team_win_streak_selrating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_team_win_streak_selrating`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE
)
    COMMENT 'Record Breaker selrating stat calcs for ahl:team:win:streak'
BEGIN

  # Determine the recent games for each selection we will use in our calculations
  CALL records_ahl_team_sel_recent_sched(v_season, v_start_date, 40);

  # Metric 1: Own pythagorean
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics;
  CREATE TEMPORARY TABLE tmp_rating_metrics (
    link_id SMALLINT UNSIGNED,
    stat DECIMAL(6, 5) UNSIGNED,
    stat_nom SMALLINT UNSIGNED,
    stat_denom SMALLINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MyISAM
    SELECT LINK.link_id,
           POW(SUM(IF(RECENT.is_home, SCHED.home_score, SCHED.visitor_score)), 2.11) / (POW(SUM(IF(RECENT.is_home, SCHED.home_score, SCHED.visitor_score)), 2.11) + POW(SUM(IF(RECENT.is_home, SCHED.visitor_score, SCHED.home_score)), 2.11)) AS stat,
           SUM(IF(RECENT.is_home, SCHED.home_score, SCHED.visitor_score)) AS stat_nom,
           SUM(IF(RECENT.is_home, SCHED.visitor_score, SCHED.home_score)) AS stat_denom
    FROM tmp_links AS LINK
    LEFT JOIN tmp_links_recent AS RECENT
      ON (RECENT.link_id = LINK.link_id)
    LEFT JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = RECENT.season
      AND SCHED.game_type = RECENT.game_type
      AND SCHED.game_id = RECENT.game_id)
    GROUP BY LINK.link_id;
  CALL records_selrating_proc('metrics', 'DESC', 'calc_ahlpythag'); # Better pythagorean makes for a better selection
  DROP TEMPORARY TABLE IF EXISTS tmp_rating_metrics_pythag;
  ALTER TABLE tmp_rating_metrics RENAME TO tmp_rating_metrics_pythag;

  # Merge (split metrics 60 own pythagorean, 40 opp rating) and store
  DROP TEMPORARY TABLE IF EXISTS tmp_ratings;
  CREATE TEMPORARY TABLE tmp_ratings (
    link_id SMALLINT UNSIGNED,
    rating_metric_pythag TINYINT UNSIGNED,
    rating_opp_rating TINYINT UNSIGNED,
    rating TINYINT UNSIGNED,
    PRIMARY KEY (link_id)
  ) ENGINE=MEMORY
    SELECT LINK.link_id,
           METRIC_PYTHAG.rating AS rating_metric_pythag,
           IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating) AS rating_opp_rating,
           (IFNULL(METRIC_PYTHAG.rating, 0) * 0.60)
             + (IFNULL(IF(LINK.is_home = 0, OPP_RATING.home_opp_rating, OPP_RATING.visitor_opp_rating), 0) * 0.40) AS rating # Link is home means opponent is the visitor
    FROM tmp_links AS LINK
    LEFT JOIN tmp_rating_metrics_pythag AS METRIC_PYTHAG
      ON (METRIC_PYTHAG.link_id = LINK.link_id)
    LEFT JOIN tmp_team_oppcalcs AS OPP_RATING
      ON (OPP_RATING.team_id = LINK.opp_team_id);
  CALL records_selrating_store(v_sport, v_season, v_game_id, v_period_id);

END $$

DELIMITER ;

#
# Selection scoring
#
DROP PROCEDURE IF EXISTS `records_ahl_team_win_streak_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_team_win_streak_scoring`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection scoring calcs for ahl:team:win:streak'
BEGIN

  UPDATE tmp_link_events AS SEL
  JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
    ON (SCHED.season = SEL.season
    AND SCHED.game_type = SEL.game_type
    AND SCHED.game_id = SEL.game_id)
  SET SEL.stat = IF(SCHED.status IN ('F','OT','2OT','3OT','4OT','5OT','SO'), SEL.team_gf > SEL.team_ga, 0),
      SEL.status = IF(SCHED.status NOT IN ('F','OT','2OT','3OT','4OT','5OT','SO'), 'na',
        IF(SEL.team_gf > SEL.team_ga, 'positive', 'negative')),
      SEL.summary = IF(SCHED.status NOT IN ('F','OT','2OT','3OT','4OT','5OT','SO'), SCHED.status,
        IF(SEL.team_gf > SEL.team_ga, CONCAT('W ', SEL.team_gf, '&ndash;', SEL.team_ga),
          IF (SEL.team_gf < SEL.team_ga, CONCAT('L ', SEL.team_ga, '&ndash;', SEL.team_gf),
            CONCAT('T ', SEL.team_gf, '&ndash;', SEL.team_ga))));

END $$

DELIMITER ;

#
# Selection stress rating
#
DROP PROCEDURE IF EXISTS `records_ahl_team_win_streak_stress`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `records_ahl_team_win_streak_stress`(
  v_sport VARCHAR(6),
  v_season SMALLINT UNSIGNED,
  v_game_id TINYINT UNSIGNED,
  v_period_id TINYINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Record Breaker selection stress calcs for ahl:team:win:streak'
BEGIN

  # Backport the point-in-time scores
  DROP TEMPORARY TABLE IF EXISTS tmp_link_events_goals;
  CREATE TEMPORARY TABLE tmp_link_events_goals (
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    event_id SMALLINT UNSIGNED,
    period TINYINT UNSIGNED,
    event_time TIME,
    team_id CHAR(3),
    is_home TINYINT UNSIGNED DEFAULT 0,
    is_visitor TINYINT UNSIGNED DEFAULT 0,
    home_score TINYINT UNSIGNED DEFAULT 0,
    visitor_score TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (season, game_type, game_id, event_id)
  ) ENGINE = MEMORY
    SELECT DISTINCT GAME_PLAY.season, GAME_PLAY.game_type, GAME_PLAY.game_id, GAME_PLAY.event_id,
           GAME_PLAY.period, GAME_PLAY.event_time, GAME_PLAY.team_id,
           SCHED.home = GAME_PLAY.team_id AS is_home,
           SCHED.visitor = GAME_PLAY.team_id AS is_visitor,
           0 AS home_score, 0 AS visitor_score
    FROM tmp_link_events AS SEL
    LEFT JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = SEL.season
      AND SCHED.game_type = SEL.game_type
      AND SCHED.game_id = SEL.game_id)
    LEFT JOIN debearco_sports.SPORTS_AHL_GAME_EVENT AS GAME_PLAY
      ON (GAME_PLAY.season = SCHED.season
      AND GAME_PLAY.game_type = SCHED.game_type
      AND GAME_PLAY.game_id = SCHED.game_id
      AND GAME_PLAY.event_type = 'GOAL');
  # For games that went to a Shootout, include an additional row to represent the pseudo-goal
  INSERT INTO tmp_link_events_goals (season, game_type, game_id, event_id, period, event_time, team_id, is_home, is_visitor, home_score, visitor_score)
    SELECT DISTINCT SCHED.season, SCHED.game_type, SCHED.game_id,
           65535 AS event_id, 5 AS period, '00:00:01' AS event_time,
           IF(SCHED.home_score > SCHED.visitor_score, SCHED.home, SCHED.visitor) AS team_id,
           SCHED.home_score > SCHED.visitor_score AS is_home,
           SCHED.home_score < SCHED.visitor_score AS is_visitor,
           0 AS home_score, 0 AS visitor_score
    FROM tmp_link_events AS SEL
    JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = SEL.season
      AND SCHED.game_type = SEL.game_type
      AND SCHED.game_id = SEL.game_id
      AND SCHED.status = 'SO');
  # Now calculate the point-in-time scores
  CALL _duplicate_tmp_table('tmp_link_events_goals', 'tmp_link_events_goals_cpA');
  CALL _duplicate_tmp_table('tmp_link_events_goals', 'tmp_link_events_goals_cpB');
  INSERT INTO tmp_link_events_goals (season, game_type, game_id, event_id, period, event_time, team_id, home_score, visitor_score)
    SELECT GOALS_cpA.season, GOALS_cpA.game_type, GOALS_cpA.game_id, GOALS_cpA.event_id, GOALS_cpA.period, GOALS_cpA.event_time, GOALS_cpA.team_id,
           IFNULL(SUM(GOALS_cpB.is_home), 0) + GOALS_cpA.is_home AS home_score,
           IFNULL(SUM(GOALS_cpB.is_visitor), 0) + GOALS_cpA.is_visitor AS visitor_score
    FROM tmp_link_events_goals_cpA AS GOALS_cpA
    LEFT JOIN tmp_link_events_goals_cpB AS GOALS_cpB
      ON (GOALS_cpB.season = GOALS_cpA.season
      AND GOALS_cpB.game_type = GOALS_cpA.game_type
      AND GOALS_cpB.game_id = GOALS_cpA.game_id
      AND GOALS_cpB.event_id < GOALS_cpA.event_id)
    GROUP BY GOALS_cpA.season, GOALS_cpA.game_type, GOALS_cpA.game_id, GOALS_cpA.event_id, GOALS_cpA.period, GOALS_cpA.event_id, GOALS_cpA.team_id
  ON DUPLICATE KEY UPDATE home_score = VALUES(home_score),
                          visitor_score = VALUES(visitor_score);
  ALTER TABLE tmp_link_events_goals
    DROP COLUMN is_home,
    DROP COLUMN is_visitor;

  # Determine when each winning team took the lead, their biggest lead after that point and the final difference
  DROP TEMPORARY TABLE IF EXISTS tmp_link_stress;
  CREATE TEMPORARY TABLE tmp_link_stress (
    link_id SMALLINT UNSIGNED,
    raw_id CHAR(5) DEFAULT NULL,
    season YEAR,
    game_type ENUM('regular'),
    game_id SMALLINT UNSIGNED,
    is_home TINYINT UNSIGNED,
    last_event_not_winning SMALLINT UNSIGNED,
    took_lead SMALLINT UNSIGNED,
    widest_margin TINYINT UNSIGNED,
    final_margin TINYINT UNSIGNED,
    stress TINYINT UNSIGNED,
    PRIMARY KEY (link_id, season, game_type, game_id)
  ) ENGINE = MyISAM
    SELECT SEL.link_id, SEL.raw_id, SEL.season, SEL.game_type, SEL.game_id, SCHED.home = SEL.raw_id AS is_home,
           MAX(IF((SCHED.home = SEL.raw_id AND GAME_PLAY.home_score <= GAME_PLAY.visitor_score)
                   OR (SCHED.visitor = SEL.raw_id AND GAME_PLAY.home_score >= GAME_PLAY.visitor_score),
                 GAME_PLAY.event_id, 0)) last_event_not_winning,
           NULL AS took_lead, NULL AS widest_margin,
           CAST(SEL.team_gf AS SIGNED) - CAST(SEL.team_ga AS SIGNED) AS final_margin,
           NULL AS stress
    FROM tmp_link_events AS SEL
    LEFT JOIN debearco_sports.SPORTS_AHL_SCHEDULE AS SCHED
      ON (SCHED.season = SEL.season
      AND SCHED.game_type = SEL.game_type
      AND SCHED.game_id = SEL.game_id)
    LEFT JOIN tmp_link_events_goals AS GAME_PLAY
      ON (GAME_PLAY.season = SCHED.season
      AND GAME_PLAY.game_type = SCHED.game_type
      AND GAME_PLAY.game_id = SCHED.game_id)
    WHERE SEL.team_gf > SEL.team_ga
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id;

  CALL _duplicate_tmp_table('tmp_link_stress', 'tmp_link_stress_cp');
  INSERT INTO tmp_link_stress (link_id, season, game_type, game_id, took_lead, widest_margin)
    SELECT SEL.link_id, SEL.season, SEL.game_type, SEL.game_id,
           MIN(IF(GAME_PLAY.period <= 4, ((GAME_PLAY.period - 1) * 1200) + (IF(GAME_PLAY.period <= 3, 1200, 300) - TIME_TO_SEC(GAME_PLAY.event_time)), 3900)) AS took_lead,
           MAX(CAST(IF(SEL.is_home, GAME_PLAY.home_score, GAME_PLAY.visitor_score) AS SIGNED) - CAST(IF(SEL.is_home, GAME_PLAY.visitor_score, GAME_PLAY.home_score) AS SIGNED)) AS widest_margin
    FROM tmp_link_stress_cp AS SEL
    LEFT JOIN tmp_link_events_goals AS GAME_PLAY
      ON (GAME_PLAY.season = SEL.season
      AND GAME_PLAY.game_type = SEL.game_type
      AND GAME_PLAY.game_id = SEL.game_id
      AND GAME_PLAY.event_id > SEL.last_event_not_winning)
    GROUP BY SEL.link_id, SEL.season, SEL.game_type, SEL.game_id
  ON DUPLICATE KEY UPDATE took_lead = VALUES(took_lead),
                          widest_margin = VALUES(widest_margin);

  # Then assign a stress score to each based on when they took the lead (for good), how much they won by and how much that changed
  UPDATE tmp_link_stress
  SET stress = (((LEAST(took_lead, 3600) - 1) / 3600) * 185)
             + (((3 - LEAST(final_margin, 3)) / 2) * 40)
             + (IF(widest_margin > 2, (widest_margin - final_margin) / widest_margin, 0) * 30);

  # Write back, with a maximum stress score if no join found
  UPDATE tmp_link_events AS SEL
  LEFT JOIN tmp_link_stress AS SEL_STRESS
    ON (SEL_STRESS.link_id = SEL.link_id
    AND SEL_STRESS.season = SEL.season
    AND SEL_STRESS.game_type = SEL.game_type
    AND SEL_STRESS.game_id = SEL.game_id)
  SET SEL.stress = IF(SEL.status <> 'na', IFNULL(SEL_STRESS.stress, 255), NULL);

END $$

DELIMITER ;
