##
## Twitter processing helpers
##

DROP FUNCTION IF EXISTS `records_config`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `records_config`(
  v_cfg VARCHAR(30)
) RETURNS VARCHAR(90)
  DETERMINISTIC
  COMMENT 'Return configuration from a mapped value'
BEGIN

  DECLARE v_return VARCHAR(90) DEFAULT NULL;

  # First, from a known list
  SET v_return = CASE v_cfg
    WHEN 'audit_autouser'          THEN 0
    WHEN 'domain'                  THEN 'https://fantasy.debear.uk'
    WHEN 'twitter_app_internal'    THEN 'fantasy_records'
    WHEN 'twitter_app_api'         THEN 'fantasy'
    WHEN 'twitter_account'         THEN 'DeBearRecords'
    WHEN 'twitter_account_retweet' THEN 'DeBearSports'
    WHEN 'twitter_queue_time'      THEN DATE_ADD(_NOW(), INTERVAL 10 MINUTE)
    WHEN 'twitter_og_path'         THEN '/var/www/f6a0812f-78d0-45a6-bec0-c060de245116/debear/sites/cdn/htdocs/fantasy/records/og'
    WHEN 'twitter_og_ext'          THEN 'png'
  END;
  IF v_return IS NOT NULL THEN
    RETURN v_return;
  END IF;
  # If not in the list, an unknown option
  SET @v_msg = CONCAT('Unmapped configuration option "', v_cfg, '"');
  SIGNAL SQLSTATE 'DB404' SET MESSAGE_TEXT = @v_msg;

END $$

DELIMITER ;
