CREATE TABLE `FANTASY_PROFILE_RECORD_GAMES` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','motogp','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `game_name` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `target` SMALLINT(4) UNSIGNED DEFAULT 0,
  `num_teams` INT(11) UNSIGNED DEFAULT 0,
  `score_mean` DECIMAL(5,1) NOT NULL DEFAULT 0.0,
  `profile_points_mean` DECIMAL(6,2) UNSIGNED DEFAULT 0.00,
  `in_progress` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`sport`, `season`, `game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_PROFILE_RECORD_TEAMS` (
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','motogp','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `achieved` TINYINT(1) UNSIGNED DEFAULT 0,
  `score` DECIMAL(5,1) NOT NULL DEFAULT 0.0,
  `rank` INT(11) UNSIGNED DEFAULT 0,
  `achievements` SMALLINT UNSIGNED DEFAULT NULL,
  `profile_points` DECIMAL(5,1) UNSIGNED DEFAULT 0.0,
  PRIMARY KEY (`user_id`,`sport`,`season`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `achieve_id` TINYINT(2) UNSIGNED NOT NULL,
  `name` VARCHAR(30),
  `disp_order` TINYINT UNSIGNED,
  PRIMARY KEY (`sport`,`season`,`game_id`,`achieve_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_PROFILE_RECORD_GAMES_ACHIEVEMENTS_LEVELS` (
  `sport` ENUM('mlb','nfl','nhl','ahl','f1','sgp') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_id` TINYINT(2) UNSIGNED NOT NULL,
  `achieve_id` TINYINT(2) UNSIGNED NOT NULL,
  `level` TINYINT(1) UNSIGNED,
  `value` SMALLINT(4) UNSIGNED NOT NULL,
  `bit_flag` TINYINT(2) UNSIGNED NOT NULL,
  `disp_order` TINYINT UNSIGNED,
  PRIMARY KEY (`sport`,`season`,`game_id`,`achieve_id`,`level`),
  UNIQUE KEY `bit_flag` (`sport`,`season`,`game_id`,`bit_flag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
