CREATE TABLE `FANTASY_PROFILE_GAMES` (
  `game` ENUM('f1','motogp','sgp','nfl','records') COLLATE latin1_general_ci NOT NULL,
  `game_type` ENUM('budget','record') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `in_progress` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`game`,`game_type`,`season`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
