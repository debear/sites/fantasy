CREATE TABLE `FANTASY_PROFILE_BUDGET_SCORING` (
  `game` ENUM('f1','motogp','sgp','nfl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `combination` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `max_score` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_teams` INT(11) UNSIGNED DEFAULT 0,
  `profile_points_mean` DECIMAL(6,2) UNSIGNED DEFAULT 0.00,
  PRIMARY KEY (`game`,`season`,`combination`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_PROFILE_BUDGET_TEAMS` (
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `game` ENUM('f1','motogp','sgp','nfl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `team_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(50) COLLATE latin1_general_ci DEFAULT '',
  `combination` VARCHAR(20) COLLATE latin1_general_ci DEFAULT '',
  `score` SMALLINT(5) UNSIGNED DEFAULT 0,
  `rank` INT(11) UNSIGNED DEFAULT 0,
  `profile_points` DECIMAL(5,1) UNSIGNED DEFAULT 0.0,
  PRIMARY KEY (`user_id`,`game`,`season`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_PROFILE_BUDGET_GROUPS` (
  `game` ENUM('f1','motogp','sgp','nfl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `group_id` INT(11) UNSIGNED NOT NULL,
  `type` ENUM('fanleague', 'group') NOT NULL,
  `name` VARCHAR(150) COLLATE latin1_general_ci DEFAULT '',
  `num_teams` INT(11) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`game`,`season`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `FANTASY_PROFILE_BUDGET_GROUPS_TEAMS` (
  `game` ENUM('f1','motogp','sgp','nfl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `group_id` INT(11) UNSIGNED NOT NULL,
  `team_id` INT(11) UNSIGNED NOT NULL,
  `rank` INT(11) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`game`,`season`,`group_id`,`team_id`),
  KEY `by_team` (`game`,`season`,`team_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
