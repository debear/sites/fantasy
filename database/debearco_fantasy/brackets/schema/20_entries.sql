CREATE TABLE `FANTASY_BRACKETS_ENTRIES` (
  `sport` ENUM('mlb','nfl','nhl','ahl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `joined` DATE NOT NULL,
  `email_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `email_type` ENUM('text','html') COLLATE latin1_general_ci DEFAULT 'html',
  PRIMARY KEY (`sport`, `season`, `user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
