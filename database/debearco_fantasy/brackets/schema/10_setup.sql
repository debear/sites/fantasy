CREATE TABLE `FANTASY_BRACKETS_SETUP_DATES` (
  `sport` ENUM('mlb','nfl','nhl','ahl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `reg_end` DATETIME NOT NULL,
  `po_start` DATETIME NULL,
  `po_end` DATETIME NULL,
  PRIMARY KEY (`sport`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
