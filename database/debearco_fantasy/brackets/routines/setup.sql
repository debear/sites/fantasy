#
# Internal setup processing
#
DROP PROCEDURE IF EXISTS `brackets_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `brackets_setup`(
  v_sport CHAR(3),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Bracket Challenge game setup processing'
BEGIN

  DECLARE v_ts_a DECIMAL(8,3) SIGNED;
  DECLARE v_ts_b DECIMAL(8,3) SIGNED;

  CALL _log(CONCAT('Starting the setup calcs for sport ', v_sport));

  SET v_ts_a = _NOW_MILLISEC();
  CALL brackets_setup_dates(v_sport, v_season);
  SET v_ts_b = _NOW_MILLISEC(); CALL _log(CONCAT('- Date processing complete (', (v_ts_b - v_ts_a), 's)'));

  CALL _log(CONCAT('Setup calcs complete'));

END $$

DELIMITER ;
