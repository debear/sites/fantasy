#
# Game dates (sport agnostic)
#
DROP PROCEDURE IF EXISTS `brackets_setup_dates`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `brackets_setup_dates`(
  v_sport CHAR(3),
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Bracket Challenge game date config'
BEGIN

  # Get the info from the sports database
  CALL _exec(CONCAT('SELECT MAX(IF(game_type = "regular", CONCAT(game_date, " ", game_time), NULL)),
    MIN(IF(game_type = "playoff", CONCAT(game_date, " ", game_time), NULL)),
    MAX(IF(game_type = "playoff" AND game_id LIKE "4__", CONCAT(game_date, " ", game_time), NULL))
      INTO @v_reg_end, @v_po_start, @v_po_end
  FROM debearco_sports.SPORTS_', UPPER(v_sport), '_SCHEDULE
  WHERE season = "', v_season, '";'));

  # Store
  INSERT INTO FANTASY_BRACKETS_SETUP_DATES (sport, season, reg_end, po_start, po_end)
    VALUES (v_sport, v_season, @v_reg_end, @v_po_start, @v_po_end)
  ON DUPLICATE KEY UPDATE reg_end = VALUES(reg_end),
                          po_start = VALUES(po_start),
                          po_end = VALUES(po_end);

END $$

DELIMITER ;
