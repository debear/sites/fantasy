get('dom:load').push(() => {
    /* Team group accordion */
    Events.attach(DOM.child('.teams'), 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('group-toggle')) {
            var id = DOM.getAttribute(ele, 'id');
            DOM.child(`.groups[data-id="${id}"]`).classList.toggle('hidden');
            if (ele.classList.contains('icon_nav_expand')) {
                // Show
                ele.classList.remove('icon_nav_expand');
                ele.classList.add('icon_nav_contract');
            } else {
                // Hide
                ele.classList.remove('icon_nav_contract');
                ele.classList.add('icon_nav_expand');
            }
        }
    });
});
