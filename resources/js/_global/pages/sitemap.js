get('dom:load').push(() => {
    /* Team group accordion */
    Events.attach(DOM.child('.subsites'), 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('toggle')) {
            var eleList = ele.closest('.subgames');
            if (eleList.classList.contains('closed')) {
                // Open
                eleList.classList.remove('closed');
                ele.innerHTML = 'Close';
            } else {
                // Close
                eleList.classList.add('closed');
                ele.innerHTML = 'Open';
            }
        }
    });
});
