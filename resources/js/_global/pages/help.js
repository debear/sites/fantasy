class HelpArticles {
    static switchSection(eleSectionId) {
        var currSelectionId = DOM.getAttribute(DOM.child('articles li.section.sel'), 'section-id');
        if (currSelectionId == eleSectionId) {
            // Skip if no actual change
            return;
        }
        // Hide the current and display the selected section
        [ currSelectionId, eleSectionId ].forEach((sectionId) => {
            DOM.child(`articles li.section[data-section-id="${sectionId}"]`).classList.toggle('sel');
            DOM.child(`articles li.section[data-section-id="${sectionId}"]`).classList.toggle('unsel');
            DOM.child(`articles div.section[data-section-id="${sectionId}"]`).classList.toggle('hidden');
        });
    }

    static toggleArticle(articleId) {
        DOM.child(`articles .article dt[data-article-id="${articleId}"]`).classList.toggle('icon_toggle_plus');
        DOM.child(`articles .article dt[data-article-id="${articleId}"]`).classList.toggle('icon_toggle_minus');
        DOM.child(`articles .article dd[data-article-id="${articleId}"]`).classList.toggle('hidden');
    }
}

get('dom:load').push(() => {
    // Click handlers for section and article toggling
    Events.attach(DOM.child('articles'), 'click', (e) => {
        var ele = e.target;
        if (ele.matches('li.section')) {
            // Section toggle
            HelpArticles.switchSection(DOM.getAttribute(ele, 'section-id'));
        } else if (ele.matches('.article dt')) {
            // Article toggle
            HelpArticles.toggleArticle(DOM.getAttribute(ele, 'article-id'));
        }
    });
    // Dropdown handlers
    Events.attach(document, 'debear:dropdown-set', (e) => {
        if (e.detail.id == 'article-section') {
            HelpArticles.switchSection(e.detail.value);
        }
    });
});
