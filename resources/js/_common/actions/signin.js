get('dom:load').push(() => {
    // Login toggle for the user action sign-in link (viewport-sensitive as to which sign-in is opened)
    Events.attach(DOM.child('body:not(.viewport-mobile) .user-action a:not([href])'), 'click', () => {
        var isDesktop = DOM.isDisplayed('.header_login');
        Login.toggle(isDesktop ? DOM.child('.header_login') : DOM.child('#nav_login_link').closest('nav'));
    });
});
