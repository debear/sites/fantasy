get('dom:load').push(() => {
    // Toggle enabled/disabled email type
    Events.attach(DOM.child('#email_status'), 'change', (e) => {
        get('dropdowns')['email_type'].setEnabled(e.target.checked);
    });
});
