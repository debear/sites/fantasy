class ModalStatus { // eslint-disable-line no-unused-vars
    static render(widget, msg, css) {
        ModalStatus.reset(widget);
        var eleStatus = DOM.child(`modal.${widget} .status-msg`);
        DOM.setAttribute(eleStatus, 'status-type', css);
        eleStatus.innerHTML = msg;
        eleStatus.classList.add(...css.split(' '));
        eleStatus.classList.remove('hidden');
    }

    static reset(widget) {
        var eleStatus = DOM.child(`modal.${widget} .status-msg`);
        var existingStatus = DOM.getAttribute(eleStatus, 'status-type');
        if (existingStatus) {
            eleStatus.classList.remove(...existingStatus.split(' '));
            DOM.setAttribute(eleStatus, 'status-type', '');
        }
    }

    static hide(widget) {
        ModalStatus.reset(widget);
        DOM.child(`modal.${widget} .status-msg`).classList.add('hidden');
    }

    static hideAfterTimeout(widget, timeout) {
        window.setTimeout(() => { ModalStatus.hide(widget); }, timeout);
    }

    static parseAJAXFailure(xmlHTTP, action) {
        if ((xmlHTTP.getResponseHeader('Content-Type') == 'application/json') && Input.isDefined(xmlHTTP.responseParsed.msg)) {
            // A specific JSON message is to be used
            return `Unable to ${action}: ${xmlHTTP.responseParsed.msg}`;
        }
        // An unknown error, so attempt an appropriate generic message, but if not fallback to a general message
        var statusErrMsg = xmlHTTP.statusErrorMessage();
        return Input.isDefined(statusErrMsg)
            ? `Unable to ${action}: ${statusErrMsg}`
            : 'An error occurred, please try again later. If the problem persists, please let us know!';
    }
}

class CommonHelpers { // eslint-disable-line no-unused-vars
    static displayRating(rating) {
        var numHalves = rating * 2;
        var numWhole = Math.floor(numHalves / 2);
        var numOutline = 5 - parseInt(Math.ceil(numHalves / 2));
        return `<i class="fa-solid fa-star sel-halves-${numHalves}"></i>`.repeat(numWhole)
            + ((numHalves % 2) == 1 ? `<i class="fa-solid fa-star-half-stroke sel-halves-${numHalves}"></i>` : '')
            + `<i class="fa-regular fa-star sel-halves-${numHalves}"></i>`.repeat(numOutline);
    }
}

/* Highcharts Hover */
successRateHover = (chart) => {
    var ret = hitrate.tooltip.options.template.
        replace(`<row-${chart.colorIndex}>`, '<strong>').
        replace(`</row-${chart.colorIndex}>`, '</strong>').
        replace(/<(\/)?row-\d>/g, '');

    return `<tooltip>${ret}</tooltip>`;
}

get('dom:load').push(() => {
    // Other game toggle
    Events.attach(DOM.child('.other_games dt'), 'click', (e) => {
        e.target.closest('dl').classList.toggle('open');
    });
    // Rules toggle
    Events.attach(DOM.child('fieldset.rules span a'), 'click', (e) => {
        var ele = e.target;
        ele.closest('.rules').classList.toggle('closed');
        ele.innerHTML = (ele.innerHTML == 'Show' ? 'Hide' : 'Show');
    });
    // Data stores
    var eleH1 = DOM.child('h1');
    set('game:ref', DOM.getAttribute(eleH1, 'ref'));
    set('game:key', DOM.getAttribute(eleH1, 'key'));
    set('game:cache-ttl-sel', parseInt(DOM.getAttribute(eleH1, 'cache_ttl_sel')));
    set('game:cache-ttl-info', parseInt(DOM.getAttribute(eleH1, 'cache_ttl_info')));
    // Force cache expiry for entries that have been superceded by a new integration run
    var gamePrefix = `records:${get('game:ref')}:`; var gamePrefixLen = gamePrefix.length;
    var cachePrefix = `${gamePrefix}${get('game:key')}:`; var cachePrefixLen = cachePrefix.length;
    Object.entries(localStorage ?? {}).forEach(([key]) => {
        if (key.substring(0, gamePrefixLen) == gamePrefix && key.substring(0, cachePrefixLen) != cachePrefix) {
            // This relates to an old version of the integration data and so can be pruned
            Storage.removeItem(key);
        }
    });
    // Use this as the prefix for all cache entries stored by this page
    set('game:cache-prefix', cachePrefix);
});
