class Selection { // eslint-disable-line no-unused-vars
    static toggleRemove() {
        DOM.child('li.actions').classList.toggle('rmv-confirm');
    }

    static save(linkId) {
        ModalStatus.render('sel-edit', 'Please wait, saving selection...', 'section loading');
        // Process via Ajax
        get('edit-modal:ajax').request({
            'url': get('edit-modal:url'),
            'method': (get('edit-modal:curr') == '' ? 'POST' : 'PATCH'),
            'content_type': 'application/json',
            'args': `{"link_id": ${linkId}}`,
            'success': () => {
                ModalStatus.render('sel-edit', 'Selection saved successfully. Reloading page...', 'success icon_valid');
                window.setTimeout(() => { location.reload(); }, 2000);
            },
            'failure': (resp) => {
                ModalStatus.render('sel-edit', ModalStatus.parseAJAXFailure(resp, 'save selection'), 'error icon_error');
                window.setTimeout(() => { ModalStatus.hide('sel-edit'); }, 3000);
            }
        });
    }

    static remove() {
        Selection.renderStatus('Please wait, removing existing selection...', 'section loading');
        // Process via Ajax
        get('edit-modal:ajax').request({
            'url': get('edit-modal:url'),
            'method': 'DELETE',
            'success': () => {
                Selection.renderStatus('Selection removed successfully. Reloading page...', 'success icon_valid');
                window.setTimeout(() => { location.reload(); }, 2000);
            },
            'failure': (resp) => {
                Selection.renderStatus(ModalStatus.parseAJAXFailure(resp, 'remove selection'), 'error icon_error');
                window.setTimeout(() => { Selection.hideStatus(); }, 3000);
            }
        });
    }

    static renderStatus(msg, css) {
        var eleStatus = DOM.child('.action-status');
        if (!Input.isDefined(eleStatus)) {
            eleStatus = document.createElement('fieldset');
            eleStatus.classList.add('action-status', 'box');
            eleStatus.innerHTML = msg;
            DOM.child('h1').after(eleStatus);
        }
        Selection.resetStatus();
        DOM.setAttribute(eleStatus, 'status-type', css);
        eleStatus.innerHTML = msg;
        eleStatus.classList.add(...css.split(' '));
        eleStatus.classList.remove('hidden');
    }

    static resetStatus() {
        var eleStatus = DOM.child('.action-status');
        var existingStatus = DOM.getAttribute(eleStatus, 'status-type');
        if (existingStatus) {
            eleStatus.classList.remove(...existingStatus.split(' '));
            DOM.setAttribute(eleStatus, 'status-type', '');
        }
    }

    static hideStatus() {
        Selection.resetStatus();
        DOM.child('.action-status').classList.add('hidden');
    }

    static displayLink(entity, periodId) {
        var periodAttr = (Input.isDefined(periodId) ? ` data-period-id="${periodId}"` : '');
        return entity.name.replace(/(<span[^>]+>)(.*?)(<\/span>)/, `$1<a class="view-sel-summary" data-slug="${entity.slug}"${periodAttr}>$2</a>$3`);
    }
}
