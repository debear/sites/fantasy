class SelectionEditModal {
    static open() {
        // Display a current selection?
        if (Input.isDefined(DOM.child('modal.sel-edit').querySelector('current'))) {
            SelectionEditModal.showCurrent();
        }
        // Trigger the data request to load the options
        SelectionEditModal.loadSelections(1); // First page
        // Finally show the modal
        SelectionEditModal.toggle();
    }

    static toggle() {
        DOM.child('modal.sel-edit').classList.toggle('hidden');
        DOM.child('body').classList.toggle('modal');
    }

    static showCurrent() {
        // Get and add the markup
        var eleParent = DOM.child('modal.sel-edit');
        var eleSelCurr = DOM.child('fieldset.sel.curr');
        var eleCurr = eleParent.querySelector('current');
        var tmpl = eleParent.querySelector('template').innerHTML;
        eleCurr.innerHTML = tmpl;
        eleCurr.querySelector('.name').innerHTML = eleSelCurr.querySelector('.name').innerHTML;
        eleCurr.querySelector('.mugshot').style.backgroundImage = getComputedStyle(eleSelCurr.querySelector('.mugshot')).backgroundImage;
        // Then update the info
        var eleInfo = eleSelCurr.querySelector('li.info span.info');
        if (Input.isDefined(eleInfo)) {
            eleCurr.querySelectorAll('.info').forEach((eleModalInfo) => { eleModalInfo.innerHTML = eleInfo.innerHTML; });
        } else {
            eleCurr.classList.add('no-info');
        }
        // Any seelection flags?
        SelectionEditModal.showSelectionFlags(eleCurr.querySelectorAll('.info'), {
            'buster': DOM.getAttribute(eleSelCurr, 'flags-buster'),
            'achieve_opp': DOM.getAttribute(eleSelCurr, 'flags-achieve_opp')
        });
        // Any weather info?
        var eleWeather = eleSelCurr.querySelector('li.info span.weather');
        if (Input.isDefined(eleWeather)) {
            eleCurr.querySelector('.name').innerHTML += eleWeather.outerHTML;
        }
        // Remaining info
        eleCurr.querySelector('.action').classList.add('row-1');
        eleCurr.querySelector('.action').innerHTML = eleParent.querySelector('actions .sel-save').innerHTML.replace('${lockTime}', DOM.getAttribute(eleSelCurr.querySelector('.locks'), 'lock-time'));
        // Stats and ratings
        Events.attach(eleCurr.querySelector('stats'), 'scroll', (e) => {
            SelectionEditModal.setStatUpdatedScrollPos(e);
        });
        eleCurr.querySelectorAll('stats .value').forEach((eleStat) => {
            var statId = DOM.getAttribute(eleStat, 'stat-id');
            var statValueRaw = DOM.getAttribute(eleSelCurr, `stat-${statId}`);
            var statValue = (Input.isNumber(statId) ? statValueRaw : CommonHelpers.displayRating(statValueRaw));
            eleStat.innerHTML = ((statValue ?? '') != '' ? statValue : '&ndash;');
        });
    }

    static resetSelections() {
        var eleModal = DOM.child('modal.sel-edit');
        var eleList = eleModal.querySelector('.sel-list');
        eleList.innerHTML = '';
        var tmpl = eleModal.querySelector('template').innerHTML;
        for (var i = 0; i < get('edit-modal:maxRows'); i++) {
            var eleNode = document.createElement('li');
            eleNode.classList.add('sel-row', 'template', `icons-${get('edit-modal:infoIcons')}`);
            eleNode.innerHTML = tmpl;
            eleList.appendChild(eleNode);
        }
        var elePagination = eleModal.querySelector('pagination');
        elePagination.classList.add('template');
        elePagination.classList.remove('hidden');
    }

    static buildCacheKey(pageNum) {
        var eleFilters = DOM.child('modal.sel-edit selfilters');
        return [
            Input.isDefined(eleFilters.querySelector('#filter_locked')) ? 'l:' + (!eleFilters.querySelector('#filter_locked').checked).toString().substring(0, 1) : null,
            Input.isDefined(eleFilters.querySelector('#filter_beaten_opp')) ? 'o:' + (!eleFilters.querySelector('#filter_beaten_opp').checked).toString().substring(0, 1) : null,
            Input.isDefined(eleFilters.querySelector('#selection-team')) ? 't:' + eleFilters.querySelector('#selection-team').value : null,
            Input.isDefined(eleFilters.querySelector('#selection-sort')) ? 's:' + eleFilters.querySelector('#selection-sort').value : null,
            `p:${pageNum}`
        ].filter((a) => { return Input.isDefined(a); }).join(';');
    }

    static loadSelections(pageNum) {
        // Reset the selections listed
        SelectionEditModal.resetSelections();
        // Formulate our cache key
        var cacheKey = `${get('game:cache-prefix')}edit:` + SelectionEditModal.buildCacheKey(pageNum);
        // If we've already loaded the data, skip straight to rendering
        var cachedList = Storage.getItem(cacheKey);
        if (Input.isDefined(cachedList)) {
            SelectionEditModal.renderSelections(cachedList);
            return;
        }
        ModalStatus.render('sel-edit', 'Please wait, loading...', 'section loading');
        // Build our query arguments
        var eleFilters = DOM.child('modal.sel-edit selfilters');
        var args = [
            Input.isDefined(eleFilters.querySelector('#filter_locked')) ? 'locked=' + (!eleFilters.querySelector('#filter_locked').checked) : null,
            Input.isDefined(eleFilters.querySelector('#filter_beaten_opp')) ? 'beaten_opp=' + (!eleFilters.querySelector('#filter_beaten_opp').checked) : null,
            Input.isDefined(eleFilters.querySelector('#selection-team')) ? 'team_id=' + eleFilters.querySelector('#selection-team').value : null,
            Input.isDefined(eleFilters.querySelector('#selection-sort')) ? 'sort=' + eleFilters.querySelector('#selection-sort').value : null,
            `page=${pageNum}`
        ].filter((a) => { return Input.isDefined(a); }).join('&');
        // Load via Ajax
        get('edit-modal:ajax').request({
            'url': get('edit-modal:url') + `?${args}`,
            'success': (retValue, config) => {
                // Should we cache this response?
                var cacheTTL = 0;
                if (get('game:cache-ttl-sel') && Input.isDefined(config.responseHeaders['x-next-locktime'])) {
                    cacheTTL = Math.min(config.responseHeaders['x-next-locktime'], get('game:cache-ttl-sel')); // Cache for no more than a specific period, but respect the time advised within the response
                }
                Storage.setItem(cacheKey, retValue, cacheTTL ? Date.now() + (cacheTTL * 1000) : -1);
                SelectionEditModal.renderSelections(retValue);
            },
            'failure': (resp) => {
                ModalStatus.render('sel-edit', ModalStatus.parseAJAXFailure(resp, 'load selections'), 'error icon_error');
                ModalStatus.hideAfterTimeout('sel-edit', 3000);
            }
        });
    }

    static renderSelections(selList) {
        // Now loop through the loaded selections
        DOM.child('modal.sel-edit .sel-list').querySelectorAll('.sel-row').forEach((eleRow, i) => {
            // Remove a template row that doesn't have a corresponding data element
            if (!Input.isDefined(selList.sel[i])) {
                eleRow.remove();
                return;
            }
            // Render the info we have
            SelectionEditModal.showSelection(eleRow, selList.sel[i]);
        });
        // If no results matched, add a message with no need to continue processing
        if (!selList.rows.total) {
            ModalStatus.render('sel-edit', 'No selections were found matching these search options.', 'info icon_info');
            return;
        }
        // Any viewport-related adjustments?
        SelectionEditModal.setStatInitialScrollPos();
        // Any pagination of note?
        var elePagination = DOM.child('modal.sel-edit pagination');
        if (selList.pages.total > 1) {
            // Prev button?
            var elePrev = elePagination.querySelector('a.prev');
            if (selList.pages.prev) {
                DOM.setAttribute(elePrev, 'page', selList.pages.curr - 1);
                elePrev.classList.remove('hidden');
            } else if (!elePrev.classList.contains('hidden')) {
                elePrev.classList.add('hidden');
            }
            // Next button?
            var eleNext = elePagination.querySelector('a.next');
            if (selList.pages.next) {
                DOM.setAttribute(eleNext, 'page', selList.pages.curr + 1);
                eleNext.classList.remove('hidden');
            } else if (!eleNext.classList.contains('hidden')) {
                eleNext.classList.add('hidden');
            }
            // Counters
            elePagination.querySelector('.num-from').innerHTML = selList.rows.from;
            elePagination.querySelector('.num-to').innerHTML = selList.rows.to;
            elePagination.querySelector('.num-total').innerHTML = selList.rows.total;
            elePagination.classList.remove('template', 'hidden');
        } else if (!elePagination.classList.contains('hidden')) {
            elePagination.classList.add('hidden');
        }
        // Ensure the status message is hidden
        ModalStatus.hide('sel-edit');
    }

    static showSelection(eleRow, sel) {
        // The core info
        eleRow.querySelector('.name').innerHTML = Selection.displayLink(sel.link) + sel.link.status;
        eleRow.querySelector('.mugshot').style.backgroundImage = `url(${sel.link.image})`;
        var eleInfo = eleRow.querySelector('.info');
        if (Input.isDefined(eleInfo) && Input.isDefined(sel.link.info)) {
            eleRow.querySelectorAll('.info').forEach((eleInfo) => { eleInfo.innerHTML = sel.link.info; });
        } else {
            eleRow.classList.add('no-info');
        }
        // Any seelection flags?
        SelectionEditModal.showSelectionFlags(eleRow.querySelectorAll('.info'), sel.flags);
        // Any weather info?
        if (Input.isDefined(sel.weather?.symbol)) {
            eleRow.querySelector('.name').innerHTML += DOM.child('modal.sel-edit actions').querySelector('.weather').outerHTML.
                replace('${icon}', sel.weather.symbol).
                replace('${temp}', sel.weather.temp ?? '');
        }
        // Selection actions
        var selAction;
        var selLockObj = new Date(sel.lock_time.raw);
        if (sel.link_id == get('edit-modal:curr') || !sel.flags.available) {
            // This is the current selection
            selAction = DOM.child('modal.sel-edit actions').querySelector('.sel-existing').outerHTML;
        } else if (Input.isDefined(sel.lock_time.status)) {
            // The selection's event is no longer available
            selAction = DOM.child('modal.sel-edit actions').querySelector('.sel-unavailable').outerHTML.replace('${reason}', sel.lock_time.disp);
        } else if (selLockObj.toJSON() > get('edit-modal:now')) {
            // This selection is available
            selAction = DOM.child('modal.sel-edit actions').querySelector('.sel-save').outerHTML.replace('${lockTime}', sel.lock_time.disp);
        } else {
            // This selection is locked
            selAction = DOM.child('modal.sel-edit actions').querySelector('.sel-unavailable').outerHTML.replace('${reason}', 'Locked');
        }
        eleRow.querySelector('.action').innerHTML = selAction;
        DOM.setAttribute(eleRow.querySelector('.action button'), 'link-id', sel.link_id);
        // Stats and ratings
        Events.attach(eleRow.querySelector('stats'), 'scroll', (e) => { SelectionEditModal.setStatUpdatedScrollPos(e); });
        eleRow.querySelectorAll('stats .value').forEach((eleStat) => {
            var statId = DOM.getAttribute(eleStat, 'stat-id');
            var statValue = (Input.isNumber(statId) ? sel.stats[statId] : CommonHelpers.displayRating(sel[statId]));
            eleStat.innerHTML = ((statValue ?? '') != '' ? statValue : '&ndash;');
        });
        // Remove the 'template' flag on the row
        eleRow.classList.remove('template');
    }

    static showSelectionFlags(eleInfo, selFlags) {
        var flags = [];
        // Opponent has/has not already been defeated towards the opponent achievement
        if (Input.isDefined(selFlags.achieve_opp)) {
            flags.push({
                'status': Input.isTrue(selFlags.achieve_opp),
                'cssClass': 'achieve-done',
                'titles': [
                    'This opponent has yet to be defeated',
                    'This opponent has already been defeated'
                ]
            });
        }
        // Opponent is a streak buster (only - do not render greyed out if not)
        if (Input.isDefined(selFlags.buster) && Input.isTrue(selFlags.buster)) {
            flags.push({
                'status': Input.isTrue(selFlags.buster),
                'cssClass': 'streak-buster',
                'titles': [
                    'This opponent is not considered a &quot;Streak Buster&quot;',
                    'This opponent is considered a &quot;Streak Buster&quot;'
                ]
            });
        }
        // Loop through the appropriate flags and render
        flags.forEach((flag) => {
            var cssClassList = ['info_flag', flag.cssClass, `icon_${flag.cssClass.replace('-', '_')}`, flag.status ? 'set' : false].filter((css) => { return Input.isString(css); });
            eleInfo.forEach((eleCell) => {
                var eleFlag = document.createElement('span');
                eleFlag.classList.add(...cssClassList);
                eleFlag.title = flag.titles[flag.status ? 1 : 0];
                eleCell.appendChild(eleFlag);
            });
        });
    }

    static setStatInitialScrollPos() {
        // Only applies in a tablet viewport
        if (!DOM.child('body').classList.contains('viewport-tablet')) {
            return;
        }
        // Determine how far along the list this item is
        var index = 0;
        var isFound = false;
        var eleList = DOM.children(`dl.dropdown[data-id="selection-sort"] .inline_list li`);
        var curSel = DOM.child('#selection-sort').value;
        eleList.forEach((ele) => {
            if (!isFound) {
                index++;
            }
            isFound = isFound || (DOM.getAttribute(ele, 'id') == curSel);
        });
        // Now determine the appropriate scroll position
        var eleInner = DOM.child('.edit-modal inner'); // An arbitrary element, they're the same
        var eleOuter = eleInner.parentNode;
        var widthContainer = eleOuter.offsetWidth + parseInt(getComputedStyle(eleOuter).marginLeft);
        var widthPerStat = Math.ceil(eleInner.offsetWidth / eleList.length);
        var numStatsWhenScrollZero = Math.floor(widthContainer / widthPerStat);
        var scrollLeft = 0;
        if (index > numStatsWhenScrollZero) {
            // We only want to scroll when the (full) stat wouldn't appear when the scroll position is zero
            var baseOffset = widthPerStat - (widthContainer - (numStatsWhenScrollZero * widthPerStat));
            scrollLeft = ((index - numStatsWhenScrollZero - 1) * widthPerStat) + baseOffset;
        }
        // Apply the scroll to each position
        DOM.children('.edit-modal stats').forEach((eleStats) => {
            eleStats.scrollLeft = scrollLeft;
        });
    }

    static setStatUpdatedScrollPos(e) {
        DOM.children('.edit-modal stats').forEach((eleStats) => {
            if (eleStats != e.target) { // Do not apply to the stat list being scrolled
                eleStats.scrollLeft = e.target.scrollLeft;
            }
        });
    }
}

/* Click/Event handlers */
get('dom:load').push(() => {
    // Selection management from main screen
    Events.attach(DOM.child('fieldset.sel.curr .actions'), 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('sel-make')) {
            SelectionEditModal.open();
        } else if (ele.classList.contains('sel-rmv') || ele.classList.contains('sel-rmv-no')) {
            Selection.toggleRemove();
        } else if (ele.classList.contains('sel-rmv-yes')) {
            Selection.remove();
            ele.disabled = true;
            ele.innerHTML = 'Removing...';
        }
    });
    // Selection action
    Events.attach(DOM.child('.edit-modal'), 'click', (e) => {
        var ele = e.target;
        if (Input.isDefined(ele.closest('.sel-save'))) {
            Selection.save(DOM.getAttribute(ele.closest('button'), 'link-id'));
        }
    });
    // Toggle filters
    Events.attach(DOM.child('toggle'), 'click', (e) => {
        var eleFilters = DOM.child('filters');
        eleFilters.classList.toggle('show');
        e.target.classList.toggle('icon_right_nav_expand');
        e.target.classList.toggle('icon_right_nav_contract');
    });
    // Change in Team filter
    Events.attach(document, 'debear:dropdown-set', (e) => {
        if (e.detail.id == 'selection-team') {
            SelectionEditModal.loadSelections(1); // Back to first page
        }
    });
    // Change in Hide Locked toggle
    Events.attach(DOM.child('#filter_locked'), 'change', () => {
        SelectionEditModal.loadSelections(1); // Back to first page
    });
    // Change in Hide Beaten Opponent toggle
    Events.attach(DOM.child('#filter_beaten_opp'), 'change', () => {
        SelectionEditModal.loadSelections(1); // Back to first page
    });
    // Change in selection sorting
    Events.attach(document, 'debear:dropdown-set', (e) => {
        if (e.detail.id != 'selection-sort') {
            return;
        }
        // Mobile view
        get('dropdowns')['selection-view'].setValue(e.detail.value);
        // Table sorting
        SelectionEditModal.loadSelections(1); // Back to first page
    });
    // Change in selection pagination
    Events.attach(DOM.child('modal.sel-edit pagination'), 'click', (e) => {
        var eleTarget = e.target;
        if (eleTarget.classList.contains('page')) {
            SelectionEditModal.loadSelections(DOM.getAttribute(eleTarget, 'page'));
        }
    });
    // Change in selection display
    Events.attach(document, 'debear:dropdown-set', (e) => {
        if (e.detail.id == 'selection-view') {
            DOM.setAttribute(DOM.child('.edit-modal'), 'mobile', `stat-${e.detail.value}`);
        }
    });
    // Close modal
    Events.attach(DOM.child('modal.sel-edit bottom button'), 'click', () => {
        // In this case, toggle => close
        SelectionEditModal.toggle();
    });
    // Data stores
    set('edit-modal:url', `${window.location.pathname}/periods/` + DOM.getAttribute(DOM.child('modal.sel-edit'), 'period-id'));
    set('edit-modal:ajax', new Ajax());
    set('edit-modal:now', (new Date(DOM.getAttribute(DOM.child('modal.sel-edit'), 'now'))).toJSON());
    set('edit-modal:curr', DOM.getAttribute(DOM.child('fieldset.sel.curr'), 'link-id'));
    set('edit-modal:maxRows', parseInt(DOM.getAttribute(DOM.child('modal.sel-edit'), 'max-rows')));
    set('edit-modal:infoIcons', parseInt(DOM.getAttribute(DOM.child('modal.sel-edit'), 'info-icons')));
});
