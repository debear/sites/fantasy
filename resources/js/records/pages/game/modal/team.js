class TeamViewModal {
    static open(eleLink) {
        var slug = DOM.getAttribute(eleLink, 'slug');
        DOM.child('.team-modal').querySelector('h3').innerHTML = eleLink.innerHTML;
        // Reset the selection listed
        TeamViewModal.reset();
        // If we've already loaded the data, skip straight to rendering
        TeamViewModal.toggle();
        var cacheKey = `${get('game:cache-prefix')}team:${slug}`;
        var cachedTeam = Storage.getItem(cacheKey);
        if (Input.isDefined(cachedTeam)) {
            TeamViewModal.render(cachedTeam);
            return;
        }
        ModalStatus.render('team-view', 'Please wait, loading...', 'section loading');
        // Load via Ajax
        get('team-modal:ajax').request({
            'url': get('team-modal:url') + slug,
            'success': (retValue) => {
                Storage.setItem(cacheKey, retValue, get('game:cache-ttl-info') ? Date.now() + (get('game:cache-ttl-info') * 1000) : -1); // May be expired via get('game:key') first
                TeamViewModal.render(retValue);
            },
            'failure': (resp) => {
                ModalStatus.render('team-view', ModalStatus.parseAJAXFailure(resp, 'load the team'), 'error icon_error');
                ModalStatus.hideAfterTimeout('team-view', 3000);
            }
        });
    }

    static toggle() {
        DOM.child('modal.team-view').classList.toggle('hidden');
        DOM.child('body').classList.toggle('modal');
    }

    static reset() {
        // Extract the results row template
        var eleModal = DOM.child('modal.team-view');
        eleModal.querySelector('.team-modal').classList.add('template');
        eleModal.querySelector('.bar.stress marker').classList.add('invisible');
        var eleTmpl = eleModal.querySelector('template');
        // Add each row to the history table
        var eleHistory = eleModal.querySelector('.history');
        // Remove any rows from previous rending
        eleHistory.querySelectorAll('[data-row]').forEach((eleExtra) => { eleExtra.remove(); });
        for (var i = 0; i < get('team-modal:maxRecent'); i++) {
            eleHistory.insertAdjacentHTML('beforeend', eleTmpl.innerHTML.replace(/\${row}/g, i));
        }
    }

    static render(team) {
        // Main selection info
        var eleModal = DOM.child('.team-modal');
        eleModal.querySelector('.user').innerHTML = (team.user.name != team.user.id ? `${team.user.name} (${team.user.id})` : team.user.name);
        // Ranks
        eleModal.querySelector('.score-overall').innerHTML = team.result;
        eleModal.querySelector('.rank-overall').innerHTML = Numbers.ordinal(team.overall.pos);
        var eleCurrent = eleModal.querySelector('.highlight-current');
        if (Input.isDefined(eleCurrent)) {
            eleCurrent.querySelector('.score-current').innerHTML = team.current.result;
            eleCurrent.querySelector('.rank-current').innerHTML = Numbers.ordinal(team.current.pos);
        }
        // Stress score
        if (Input.isDefined(team.stress)) {
            eleModal.querySelector('.bar.stress marker').style.width = eleModal.querySelector('.value-stress').innerHTML = `${team.stress}%`;
            eleModal.querySelector('.bar.stress stress-bar').style.clipPath = 'inset(0 ' + (100 - team.stress) + '% 0 0)';
            eleModal.querySelector('.bar.stress marker').classList.remove('invisible');
        } else {
            eleModal.querySelector('.value-stress').innerHTML = '&ndash;';
        }
        // Each recent history entry
        team.recent.forEach((recent, i) => {
            eleModal.querySelector(`.period[data-row="${i}"]`).innerHTML = recent.period.short;
            eleModal.querySelector(`.score[data-row="${i}"]`).innerHTML = recent.result ?? '&ndash;';
            // If no selection was made, we do not need the expanded info fields
            var eleSel = eleModal.querySelector(`.selection[data-row="${i}"]`);
            if (Input.isDefined(recent.sel.name)) {
                eleSel.innerHTML = Input.isDefined(recent.sel.name)
                    ? Selection.displayLink(recent.sel, recent.period_id) + recent.sel.status
                    : '&ndash;';
                eleModal.querySelector(`.rating[data-row="${i}"]`).innerHTML = Input.isDefined(recent.rating) ? CommonHelpers.displayRating(recent.rating) : '&ndash;';
                eleModal.querySelector(`.summary[data-row="${i}"]`).classList.add('result', recent.status ?? 'neutral');
                eleModal.querySelector(`.summary[data-row="${i}"]`).innerHTML = !Input.isDefined(recent.status) ? '<em>TBD</em>' : (recent.summary ?? '&ndash;');
            } else {
                eleSel.classList.add('no-sel');
                var selLabel = ((recent?.sel?.status ?? 'unknown') == 'hidden' ? 'Selection Hidden' : 'No Selection Made');
                eleSel.innerHTML = `<em>${selLabel}</em>`;
                ['rating', 'summary', 'summary'].forEach((eleRmv) => {
                    var ele = eleModal.querySelector(`.${eleRmv}[data-row="${i}"]`);
                    if (Input.isDefined(ele)) {
                        ele.remove();
                    }
                });
            }
        });
        // Remove superfluous rows
        for (var i = team.recent.length; i < get('team-modal:maxRecent'); i++) {
            eleModal.querySelectorAll(`[data-row="${i}"]`).forEach((eleExtra) => { eleExtra.remove(); });
        }

        // Remove the 'template' flag on the fields
        eleModal.classList.remove('template');
        // Ensure the status message is hidden
        ModalStatus.hide('team-view');
    }
}

/* Click/Event handlers */
get('dom:load').push(() => {
    // Open a link
    Events.attach(DOM.child('content'), 'click', (e) => {
        if (e.target.classList.contains('view-team-summary')) {
            TeamViewModal.open(e.target);
        }
    });
    // Close modal
    Events.attach(DOM.child('modal.team-view button'), 'click', () => {
        // In this case, toggle => close
        TeamViewModal.toggle();
    });
    // Data stores
    set('team-modal:url', `${window.location.pathname}/teams/`);
    set('team-modal:ajax', new Ajax());
    set('team-modal:maxRecent', parseInt(DOM.getAttribute(DOM.child('modal.team-view'), 'recent-rows')));
});
