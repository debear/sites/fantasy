class SelectionViewModal {
    static open(eleLink) {
        var slug = DOM.getAttribute(eleLink, 'slug');
        var periodId = DOM.getAttribute(eleLink, 'period-id');
        DOM.child('.view-modal').querySelector('h3').innerHTML = eleLink.closest('span').outerHTML.replace(/<a[^>]+>(.*?)<\/a>/, '$1');
        // Reset the selection listed
        SelectionViewModal.reset();
        // If we've already loaded the data, skip straight to rendering
        SelectionViewModal.toggle();
        var cacheKey = `${get('game:cache-prefix')}view:${slug}` + (Input.isDefined(periodId) ? `-${periodId}` : '');
        var cachedSel = Storage.getItem(cacheKey);
        if (Input.isDefined(cachedSel)) {
            SelectionViewModal.render(cachedSel);
            return;
        }
        ModalStatus.render('sel-view', 'Please wait, loading...', 'section loading');
        // Load via Ajax
        get('view-modal:ajax').request({
            'url': get('view-modal:url') + slug + (Input.isDefined(periodId) ? `/${periodId}` : ''),
            'success': (retValue) => {
                Storage.setItem(cacheKey, retValue, get('game:cache-ttl-info') ? Date.now() + (get('game:cache-ttl-info') * 1000) : -1); // May be expired via get('game:key') first
                SelectionViewModal.render(retValue);
            },
            'failure': (resp) => {
                ModalStatus.render('sel-view', ModalStatus.parseAJAXFailure(resp, 'load the selection'), 'error icon_error');
                ModalStatus.hideAfterTimeout('sel-view', 3000);
            }
        });
    }

    static toggle() {
        DOM.child('modal.sel-view').classList.toggle('hidden');
        DOM.child('body').classList.toggle('modal');
    }

    static reset() {
        var eleModal = DOM.child('modal.sel-view');
        var eleDetail = eleModal.querySelector('.sel-detail');
        eleDetail.innerHTML = eleModal.querySelector('template').innerHTML;
        eleDetail.classList.add('template');
        // Extract the results row template
        var eleTmpl = eleDetail.querySelector('.history row');
        eleTmpl.remove();
        // Add each row to the history table
        var eleHistory = eleDetail.querySelector('.history');
        for (var i = 0; i < get('view-modal:maxRecent'); i++) {
            eleHistory.insertAdjacentHTML('beforeend', eleTmpl.innerHTML.replace(/\${row}/g, i));
        }
    }

    static render(sel) {
        // Main selection info
        var eleModal = DOM.child('.view-modal');
        if (Input.isDefined(sel.link.status)) {
            eleModal.querySelector('h3').innerHTML += sel.link.status;
        }
        if (Input.isDefined(sel.link.profile)) {
            eleModal.querySelector('h3').innerHTML += `<a class="profile icon icon_sports_profile no_underline" href="${sel.link.profile}" rel="noopener noreferrer" target="_blank" title="View Profile"></a>`;
        }
        eleModal.querySelector('mugshot').style.backgroundImage = `url(${sel.link.image})`;
        eleModal.querySelector('.stat-rating').innerHTML = CommonHelpers.displayRating(sel.rating);
        if (Input.isDefined(sel.opp_rating)) {
            eleModal.querySelector('.stat-opp_rating').innerHTML = CommonHelpers.displayRating(sel.opp_rating);
        }
        Object.keys(sel.stats).forEach((statID) => {
            eleModal.querySelector(`.stat-${statID}`).innerHTML = sel.stats[statID] ?? '&ndash;';
        });
        // Each recent history entry
        sel.recent.forEach((recent, i) => {
            eleModal.querySelector(`.period[data-row="${i}"]`).innerHTML = recent.period.short;
            eleModal.querySelector(`.period-info[data-row="${i}"]`).innerHTML = recent.link.info ?? (!Input.isDefined(recent.link.status) ? '<em>PPD</em>' : '');
            eleModal.querySelector(`.rating[data-row="${i}"]`).innerHTML = CommonHelpers.displayRating(recent.rating);
            if (Input.isDefined(recent.opp_rating)) {
                eleModal.querySelector(`.opp_rating[data-row="${i}"]`).innerHTML = CommonHelpers.displayRating(recent.opp_rating);
            }
            eleModal.querySelector(`.summary[data-row="${i}"]`).classList.add('result', recent.status);
            eleModal.querySelector(`.summary[data-row="${i}"]`).innerHTML = recent.summary ?? (recent.link.status ?? '&ndash;');
            eleModal.querySelector(`.usage[data-row="${i}"]`).innerHTML = Input.isDefined(recent.usage) ? Number(recent.usage).toFixed(2) + '%' : '&ndash;';
        });
        // Remove superfluous rows
        for (var i = sel.recent.length; i < get('view-modal:maxRecent'); i++) {
            eleModal.querySelectorAll(`[data-row="${i}"]`).forEach((eleExtra) => { eleExtra.remove(); });
        }

        // Remove the 'template' flag on the fields
        eleModal.querySelector('.sel-detail').classList.remove('template');
        // Ensure the status message is hidden
        ModalStatus.hide('sel-view');
    }
}

/* Click/Event handlers */
get('dom:load').push(() => {
    // Open a link
    Events.attach(DOM.child('content'), 'click', (e) => {
        if (e.target.classList.contains('view-sel-summary')) {
            SelectionViewModal.open(e.target);
        }
    });
    // Close modal
    Events.attach(DOM.child('modal.sel-view button'), 'click', () => {
        // In this case, toggle => close
        SelectionViewModal.toggle();
    });
    // Data stores
    set('view-modal:url', `${window.location.pathname}/links/`);
    set('view-modal:ajax', new Ajax());
    set('view-modal:maxRecent', parseInt(DOM.getAttribute(DOM.child('modal.sel-view'), 'recent-rows')));
});
