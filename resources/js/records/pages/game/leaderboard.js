class Leaderboards {
    static toggleTabs(ele) {
        // Tab switching
        var eleParent = ele.closest('dl');
        var tab = DOM.getAttribute(ele, 'tab');
        var tbl = eleParent.querySelector(`.table.${tab}`);
        var tabOpp = DOM.getAttribute(eleParent.querySelector('.tab.sel'), 'tab');
        var eleOpp = eleParent.querySelector(`dt[data-tab=${tabOpp}]`);
        var tblOpp = eleParent.querySelector(`.table.${tabOpp}`);
        // Hide current
        eleOpp.classList.remove('sel', 'row-1');
        eleOpp.classList.add('unsel', 'row-0');
        tblOpp.classList.add('hidden');
        // Show selected
        ele.classList.add('sel', 'row-1');
        ele.classList.remove('unsel', 'row-0');
        tbl.classList.remove('hidden');
    }

    static openModal() {
        // Trigger the data request to load the options
        Leaderboards.loadData();
        // Finally show the modal
        Leaderboards.toggleModal();
    }

    static toggleModal() {
        DOM.child('modal.leader').classList.toggle('hidden');
        DOM.child('body').classList.toggle('modal');
    }

    static loadData() {
        // Reset the entries listed
        Leaderboards.resetModal();
        // Formulate our cache key
        var currTab = Input.isDefined(DOM.child('dl.leaderboard .tab.sel'))
            ? DOM.getAttribute(DOM.child('dl.leaderboard .tab.sel'), 'tab')
            : DOM.getAttribute(DOM.child('dl.leaderboard'), 'default-tab');
        // If we've already loaded the data, skip straight to rendering
        var cacheKey = `${get('game:cache-prefix')}leaders:${currTab}`;
        if (Input.isDefined(Storage.getItem(cacheKey))) {
            Leaderboards.renderModal(currTab);
            return;
        }
        ModalStatus.render('leader', 'Please wait, loading...', 'section loading');
        // Load via Ajax
        get('leaders:ajax').request({
            'url': get('leaders:url'),
            'success': (retValue) => {
                Storage.setItem(cacheKey, retValue, get('game:cache-ttl-info') ? Date.now() + (get('game:cache-ttl-info') * 1000) : -1); // May be expired via get('game:key') first
                Leaderboards.renderModal(currTab);
            },
            'failure': (resp) => {
                ModalStatus.render('leader', ModalStatus.parseAJAXFailure(resp, 'load leaderboards'), 'error icon_error');
                ModalStatus.hideAfterTimeout('leader', 3000);
            }
        });
    }

    static resetModal() {
        var eleModal = DOM.child('modal.leader');
        var eleList = eleModal.querySelector('.leaderboard');
        eleList.innerHTML = '';
        eleList.classList.add('template');
        // Add our template / loading blocks
        var eleTemplate = eleModal.querySelector('template');
        var tmpl = eleTemplate.innerHTML;
        for (var i = 0; i < get('leaders:maxRows'); i++) {
            eleList.insertAdjacentHTML('beforeend', tmpl.replace(/tmpl-X/g, `tmpl-cell tmpl-${i}`));
        }
        // Finally a separator, then place for user's entry
        eleList.insertAdjacentHTML('beforeend', DOM.getAttribute(eleTemplate, 'separator'));
        eleList.insertAdjacentHTML('beforeend', tmpl.replace(/tmpl-X/g, 'tmpl-cell tmpl-user'));
    }

    static renderModal(currTab) {
        // Display the detailed leaderboard
        DOM.child('modal.leader h3 .type').innerHTML = currTab.ucfirst();
        // Now loop through the loaded entries
        var cachedList = Storage.getItem(`${get('game:cache-prefix')}leaders:${currTab}`);
        var posList = [];
        var renderedUser = false;
        cachedList[currTab].list.forEach((row, i) => {
            var mergeRows;
            if (row.pos.disp != '=' && row.pos.num > 3) {
                posList.push(row.pos.disp);
            }
            if ((i < (get('leaders:maxRows') - 1)) || (cachedList[currTab].list.length == get('leaders:maxRows'))) {
                // A "regular" row
                mergeRows = 0;
            } else if (i == (get('leaders:maxRows') - 1)) {
                // A "final" row to summarise subsequent ties
                mergeRows = cachedList[currTab].list.length - i;
            }
            // Skip rows that do not fit either criteria, as they would have been merged
            if (Input.isDefined(mergeRows)) {
                Leaderboards.renderRow(row, i, mergeRows);
                renderedUser = renderedUser || (row.css == 'row-user');
            }
        });
        // Is there a user row to render?
        if (Input.isDefined(cachedList[currTab].user) && !renderedUser) {
            DOM.child('modal.leader .horiz-sep').classList.add('row-' + ((posList.length + 1) % 2));
            Leaderboards.renderRow(cachedList[currTab].user, 'user');
        } else {
            DOM.child('modal.leader .horiz-sep').remove()
        }
        // Remove any remaining template cells
        DOM.children('modal.leader .tmpl-cell').forEach((eleTmpl) => { eleTmpl.remove(); });
        // Switch from loading mode to data displaying
        DOM.child('modal.leader .leaderboard').classList.remove('template');
        // Ensure the status message is hidden
        ModalStatus.hide('leader');
    }

    static renderRow(row, i, merges) {
        DOM.child(`modal.leader .tmpl-${i}.pos`).innerHTML = (!merges
            ? `<span class="${row.pos.chg}">${row.pos.disp}</span>`
            : `<span class="multiple-ties">${row.pos.disp}</span>`);
        DOM.child(`modal.leader .tmpl-${i}.name`).innerHTML = (!merges
            ? `<a class="view-team-summary" data-slug="${row.slug}">${row.entry_name}</a>`
            : `<em>${merges} ${row.pos.disp == '=' ? 'More ' : ''}Entries Tied</em>`);
        DOM.child(`modal.leader .tmpl-${i}.user`).innerHTML = (!merges
            ? `<a href="/profile/${row.user.id}" target="_blank" title="Opens in new tab/window">${row.user.name}</a>`
            : '<em>Multiple Users</em>');
        DOM.child(`modal.leader .tmpl-${i}.score`).innerHTML = row.result;
        DOM.child(`modal.leader .tmpl-${i}.last-sel`).innerHTML = (!merges
            ? Input.isDefined(row.sel.entity)
                ? Selection.displayLink(row.sel.entity, get('leaders:period-id')) + row.sel.entity.status
                : ((row?.sel?.status ?? 'unknown') == 'hidden'
                    ? '<em>Selection Hidden</em>'
                    : '<em>No Selection Made</em>')
            : '&ndash;');
        DOM.child(`modal.leader .tmpl-${i}.last-result`).innerHTML = (!merges && !Input.isDefined(row.sel.status)
            ? '<em>TBD</em>'
            : (!merges && Input.isDefined(row.sel.summary)
                ? row.sel.summary
                : '&ndash;'));
        // Set the row format of each of the cells
        DOM.children(`modal.leader .tmpl-${i}`).forEach((eleRow) => {
            eleRow.classList.remove(`tmpl-${i}`, 'tmpl-cell');
            eleRow.classList.add(row.css);
        });
    }
}

get('dom:load').push(() => {
    // Click handler
    Events.attach(DOM.child('.leaderboard'), 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('unsel')) {
            Leaderboards.toggleTabs(ele);
        } else if (ele.matches('.view-leaders a')) {
            Leaderboards.openModal();
        }
    });
    // Close modal
    Events.attach(DOM.child('modal.leader bottom button'), 'click', () => {
        // In this case, toggle => close
        Leaderboards.toggleModal();
    });
    // Data stores
    set('leaders:url', `${window.location.pathname}/leaderboard`);
    set('leaders:ajax', new Ajax());
    set('leaders:period-id', DOM.getAttribute(DOM.child('fieldset.leaderboard dl.leaderboard'), 'period-id'));
    set('leaders:maxRows', 10);
});
