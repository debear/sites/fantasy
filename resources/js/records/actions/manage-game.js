get('dom:load').push(() => {
    // Join / Leave a game
    set('entry:game:ajax', new Ajax());
    Events.attach(DOM.child('content'), 'click', (e) => {
        var ele = e.target;
        if (!ele.classList.contains('game-join') && !ele.classList.contains('game-leave')) {
            return;
        }
        // Wanting to join or leave a game.
        var isJoining = ele.classList.contains('game-join');
        var action = (isJoining ? 'joined' : 'left');
        // Determine what we're processing.
        var gameRef = DOM.getAttribute(ele.closest('.game'), 'ref');
        // Update the state to indicate progress.
        if (ele.tagName.toLowerCase() == 'button') {
            ele.closest('.game-action').classList.add('processing');
            ele.innerHTML = 'Please wait...';
            ele.disabled = true;
        } else {
            displayActionStatus(ele, 'section', 'icon_loading_section', 'Please waiting, processing...');
        }
        // Make the request and handle the response accordingly.
        get('entry:game:ajax').request({
            'method': (isJoining ? 'POST' : 'DELETE'),
            'url': `/records/my-entry/${gameRef}`,
            'headers': {
                'Accept': 'application/json'
            },
            'success': () => {
                // React to the successful joining.
                displayActionStatus(ele, 'success', 'icon_valid', `Game ${action} successfully; Reloading page...`);
            },
            'failure': (resp) => {
                // Display the error state.
                var statusErrMsg = resp.statusErrorMessage();
                displayActionStatus(ele, 'error', 'icon_delete', Input.isDefined(statusErrMsg)
                    ? `Unable to ${action} game: ${statusErrMsg}`
                    : 'An error occurred. Reloading page to try again?');
            }
        });
    });
});

displayActionStatus = (ele, cssBox, cssIcon, msg) => {
    var eleBase = ele.closest('.game-action');
    var isList = (eleBase.tagName.toLowerCase() == 'ul');
    // Delete a previous status message, if one was set.
    var eleExisting = eleBase.parentNode.querySelector('.action-status.box');
    if (Input.isDefined(eleExisting)) {
        eleExisting.remove();
    }
    // Build our new status.
    var eleStatus = document.createElement(isList ? 'li' : 'fieldset');
    eleStatus.classList.add('action-status', 'box', cssBox, cssIcon);
    eleStatus.innerHTML = msg;
    if (isList) {
        eleBase.appendChild(eleStatus);
    } else {
        eleBase.insertAdjacentElement('afterend', eleStatus);
    }
    if (cssBox != 'section') {
        window.setTimeout(() => { location.reload(); }, 3000);
    }
}
