var cacheName = 'uk.debear.fantasy.basic.v1.0';
var cacheCore = [ '/favicon.ico' ];

// Install and cache our Offline page on load
self.addEventListener('install', (e) => {
    var offlineRequest = new Request('/offline.htm');
    e.waitUntil(
        caches.open(cacheName).then((cache) => { return cache.addAll(cacheCore); })
    );
    e.waitUntil(
        fetch(offlineRequest).then((response) => {
            return caches.open(cacheName).then((cache) => {
                return cache.put(offlineRequest, response); }
            );
        })
    );
});

// Ensure the cache is up to date
self.addEventListener('activate', (e) => {
    e.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(
                keyList.map((key) => {
                    if (key !== cacheName) {
                        return caches.delete(key);
                    }
                })
            )
        })
    );
    return self.clients.claim();
});

// Return the Offline page if we are deemed offline
self.addEventListener('fetch', (e) => {
    var request = e.request;
    if (request.method === 'GET') {
        e.respondWith(
            caches.match(e.request).then((response) => {
                return response || fetch(e.request).catch(() => {
                    return caches.open(cacheName).then((cache) => {
                        return cache.match('/offline.htm');
                    });
                });
            })
        );
    }
});
