## Build v3.0.1952 - 958bb182 - 2025-03-06 - Feature: Bracket Challenge Signup
* 958bb182: Merge the entry forms for Records and Brackets, given such their similarity (_Thierry Draper_)
* 8bbe3b50: Merge the separate Brackets controller traits into a single callable method (_Thierry Draper_)
* 0260f04a: Fix the sign-in toggle on game homepages in mobile views (_Thierry Draper_)
* fc702269: Include CTAs on the homepage at appropriate points in the Bracket Challenge game (_Thierry Draper_)
* 9f5619b5: Improve season validation for archived/current season distinction (_Thierry Draper_)
* dd99bbbd: Apply missing image compression to Bracket Challenge logos (_Thierry Draper_)
* 6ba99ffa: Remove some stray extra chars in an assertDontSee() unit test (_Thierry Draper_)
* 4d9cf3d0: Backport enforcement the email opt-in/out during creation of Record Breakers entry, to match its footer wording (_Thierry Draper_)
* 1658557c: Backport the section endpoint setting to the Record Breakers entry form redirect (_Thierry Draper_)
* af4e4c8b: Configure the Bracket Challenge entry creation email (_Thierry Draper_)
* 974e940a: Set up the form for Bracket Challenge entry management (_Thierry Draper_)
* 2ea63e35: Update the manifests according to the base endpoint including the season (_Thierry Draper_)
* d70da341: Append, as standard, the season to the base URL used within Bracket Challenge (_Thierry Draper_)
* 0caa705f: Setup the initial ORM models for the new Brackets entry table (_Thierry Draper_)

## Build v3.0.1938 - 7302cfe4 - 2025-02-26 - Improvement: Bracket Challenge Manifest
* 7302cfe4: Fix the Bracket Challenge manifests to use the correct endpoint (_Thierry Draper_)

## Build v3.0.1937 - 743adf8e - 2025-02-24 - Hotfix: Preview Next Periods
* 743adf8e: Fix the identification of the correct "next" period in preview calcs (_Thierry Draper_)

## Build v3.0.1936 - 6215c853 - 2025-02-22 - SoftDep: February 2025
* 6215c853: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* 45920bea: Upgrade Highcharts to v12 (_Thierry Draper_)
* 1fd570b8: Migrate config getting to the framework Config class (_Thierry Draper_)
* f3889d97: Auto: NPM Update for February 2025 (_Thierry Draper_)
* 9e409fb1: Auto: Composer Update for February 2025 (_Thierry Draper_)

## Build v3.0.1933 - 164ee413 - 2025-02-18 - Feature: Bracket Challenge Homepage
* 164ee413: Generate an initial draft of the per-sport brackets (_Thierry Draper_)
* a000ccdb: Include per-sport link to various CDN resources required by the Bracket Challenge (_Thierry Draper_)
* 1ba6dad7: Start an initial version of the Brackets homepage (_Thierry Draper_)

## Build v3.0.1930 - 09ab33df - 2025-02-11 - Hotfix: Minor Record Breakers Fixes
* 09ab33df: Standardise the game status parsing between main homepage and Record Breaker homepage (_Thierry Draper_)
* 1bd944f5: Centralise the recently moved sport logo CSS loading, to apply across all pages that used it (_Thierry Draper_)

## Build v3.0.1928 - ba53f345 - 2025-02-09 - Hotfix: MLB 2025 Changelog
* (Auto-commits only)

## Build v3.0.1928 - 3e84fed7 - 2025-02-09 - Setup: MLB 2025 season
* 3e84fed7: MLB game launch for 2025 (_Thierry Draper_)

## Build v3.0.1927 - b0bd6621 - 2025-02-06 - Setup: F1 2025 season
* b0bd6621: F1 game launch for 2025 (_Thierry Draper_)

## Build v3.0.1926 - d09328c9 - 2025-01-28 - Setup: SGP 2025 season
* d09328c9: SGP game launch for 2025 (_Thierry Draper_)

## Build v3.0.1925 - 6ccbc3c7 - 2025-01-26 - Feature: Brackets Data Setup
* 6ccbc3c7: Create an initial version of the brackets processing scripts (_Thierry Draper_)
* 6153f14f: Create an initial version of the brackets server sync process (_Thierry Draper_)
* b3c05b91: Calculate per-game deadlines and store in the database (_Thierry Draper_)
* 41abcf99: Define a season-to-bracket logic mapping which allows for slightly more nuance than the existing Sport subsite mapping (_Thierry Draper_)
* ff31e5fd: Switch our planned CI seasons from those inline with Records to last season, due to the AHL cancelling the 2020 Calder Cup Finals (_Thierry Draper_)
* 38ee9cf1: Include a direct accessor to the sport setup config, without having to access it by name (_Thierry Draper_)
* aec46864: Standardise season config with the Sports setup array (_Thierry Draper_)

## Build v3.0.1918 - 09ac9a85 - 2025-01-25 - Improvement: Archived Game Navigation
* 09ac9a85: Include, ad hoc, archived games in the desktop page (_Thierry Draper_)
* 2430b2c1: Include, ad hoc, archived games in the mobile nav (_Thierry Draper_)

## Build v3.0.1916 - 9816ff0f - 2025-01-25 - Hotfix: Selection Modal Info
* 9816ff0f: Ensure the selection modal filters are not half hidden behind selection info (_Thierry Draper_)

## Build v3.0.1915 - d4e87801 - 2025-01-25 - Feature: Bracket Challenge Core Setup
* d4e87801: Add new logos for the individual bracket games (_Thierry Draper_)
* 0b7eebbd: Setup basic routing, globally across all sports, for the Bracket Challenge games (_Thierry Draper_)
* 0153b610: Dynamically determine Bracket Challenge availability dates, to not require per-season setup scripts (_Thierry Draper_)

## Build v3.0.1912 - 5c8e2a5c - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build v3.0.1912 - 5446e3dc - 2025-01-13 - Hotfix: NHL Streak Buster Calcs
* 5446e3dc: Handle postponed NHL games in Streak Buster cals for the team winning streak game (_Thierry Draper_)

## Build v3.0.1911 - d7aea62d - 2025-01-13 - Hotfix: Homepage Selection Status
* d7aea62d: Fix selection status on the homepage for those that do not take part in their event (_Thierry Draper_)

## Build v3.0.1910 - adce4f6a - 2025-01-13 - Improvement: Test Harness
* adce4f6a: Allow for game data archiving emulation during the test harness run (_Thierry Draper_)
* eb884a81: Colour the main test harness run log output, preserved to the generated log file (_Thierry Draper_)
* eee0de67: Report on potential issues generated during the test harness run (_Thierry Draper_)
* f5e04753: Move application logs to the per-run location (_Thierry Draper_)
* fd5c5a95: Colour the help/usage text to improve readability (_Thierry Draper_)
* b6da7492: Add a flag for excluding sports from the processing run, even if they match the supplied date range (_Thierry Draper_)
* 4cc1b993: Automatically determine and validate the run and sport loading dates based on the dates within the SQL file (_Thierry Draper_)
* d1b0bc97: Break down the helper files into more semantic components (_Thierry Draper_)
* e68204d0: Start the test harness with a summary of run parameters and a chance for the user to back out (_Thierry Draper_)
* 87ce221c: Merge the master and test setup files to avoid unnecessary duplication (and errors crept in) (_Thierry Draper_)
* 77596a8a: Preserve for subsequent reverting, the original datetime override set prior to the test harness being triggered (_Thierry Draper_)

## Build v3.0.1899 - 3532c2f9 - 2025-01-08 - Hotfix: Leaderboard Optimisations
* 3532c2f9: Optimise Leaderboard loading to only operate on those appropriate for that game (_Thierry Draper_)

## Build v3.0.1898 - 5284b045 - 2025-01-08 - Improvement: NFL Retrospective
* 5284b045: Refine, after review, selection usage targets across all other games (_Thierry Draper_)
* 9941d97d: Remove the Selection Usage achievement from NFL games where it does not work within the game (_Thierry Draper_)
* c3fdc7be: Remove the Opponent-based achievements from the NFL games (_Thierry Draper_)
* f47c6630: Make the round summary section a per-game optional field (_Thierry Draper_)
* 63ef6b79: Switch the definition of a neutral NFL Passing TD selection at-least-three-quarters-of-prorata-target to 1+ (_Thierry Draper_)
* fe71df7b: Move the hit rate leaderboard and streak buster config options from deterministically calculated to a specific per-game flag (_Thierry Draper_)
* 1206d065: Merge w/score flags (_Thierry Draper_)
* 959aa154: Switch per-game config flags to a more future-proof SET instead of inidividual columns (_Thierry Draper_)
* 4214db96: Switch a game flag logic use from direct column to its pre-existing generalised getter (_Thierry Draper_)
* a7347a73: Remove duplicated is-selection-type methods within the game model (_Thierry Draper_)

## Build v3.0.1888 - b92e6d66 - 2024-12-31 - Hotfix: Achievement Notification Wording
* b92e6d66: Shorten a too-long opponent beaten notification wording (_Thierry Draper_)
* 7ba11399: Hide the outdated achievements alert, along with opponents, in the time between the next period starting and the previous period being processed (_Thierry Draper_)

## Build v3.0.1886 - 5bc0ee06 - 2024-12-31 - Improvement: Achievements in API
* 5bc0ee06: Optimise the leaderboard API endpoints (_Thierry Draper_)
* d3ff27f7: Include changes in achievements within the selection history API endpoint (_Thierry Draper_)
* c51c6441: Include changes in achievements within the period selection API endpoint (_Thierry Draper_)
* f4229456: Include changes in achievements within the entry API endpoint (_Thierry Draper_)
* ef9adb8c: Ensure selection flags are consistent across selection endpoints (_Thierry Draper_)
* cc86c6ae: Ensure the entry selection summary API endpoint does not include achievement or opponent details (_Thierry Draper_)
* be6266cc: Ensure the entry game list summary API endpoint does not include achievement or opponent details (_Thierry Draper_)

## Build v3.0.1879 - eee0a8ba - 2024-12-29 - Hotfix: Defeated Opponent Notifications
* eee0a8ba: Improve defeated opponent messaging (_Thierry Draper_)
* 5f11235e: Hide the opponent beaten message after a successful game on the following gap between rollover and processing (_Thierry Draper_)

## Build v3.0.1877 - f3f50e74 - 2024-12-27 - SysAdmin: Dynamic Relative Values
* f3f50e74: Merge the separate hitrate number columns into a single, composite, integer column (_Thierry Draper_)
* 6f08fa12: Make the position changes between rounds dynamically calculated (_Thierry Draper_)
* e8856ae3: Fix off-season record breaker game calcs (_Thierry Draper_)
* a14a5ebf: Hide the final position change on the archive leaderboards, due to the lost context (_Thierry Draper_)

## Build v3.0.1873 - a9ea36a8 - 2024-12-22 - Feature: Highlight New Achievements
* a9ea36a8: Congratulate users when their entry ticks off another achievement or opponent in the previous period (_Thierry Draper_)

## Build v3.0.1872 - ae909502 - 2024-12-20 - Improvement: Motorsport Game Ends
* ae909502: Fix API selections within the test harness, following an earlier breaking API change (_Thierry Draper_)
* 5b4f8039: Remove some of the absolute-now date/time references to use the dynamic, test-harness compatible, method (_Thierry Draper_)
* c5f8b392: Report on number of queued tweets and emails on a daily basis within the test harness (_Thierry Draper_)
* 3c77bb15: Include game end email processing within the test harness (_Thierry Draper_)
* 6edc0505: Match the game end email template progress bars with those rendered in the web version (_Thierry Draper_)
* 5686dcac: Include the boundary game end date within the period we attempt to send the game end email (_Thierry Draper_)
* c1c9aa2a: Fix game end dates for motorsports with non-daily periods, especially around how they handle the final tweet (_Thierry Draper_)

## Build v3.0.1865 - e3a596ed - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build v3.0.1865 - b4e4aade - 2024-12-12 - SysAdmin: Records Setup Files
* b4e4aade: Separate the up-to-date game config (with potential future changes) from the actual version used in the current/latest season (_Thierry Draper_)
* 9c78d978: Move the test harness setup files to explicit, not seasonal, filenames (_Thierry Draper_)

## Build v3.0.1863 - 9399ce10 - 2024-12-11 - Improvements: XML Sitemaps
* 9399ce10: Include an XML sitemap to cover the individual Record Breaker games (and secondary pages) (_Thierry Draper_)
* 0344fb15: Include an XML sitemap index file to cover the sitemaps for the individual games (_Thierry Draper_)

## Build v3.0.1861 - 2eaf3f2e - 2024-12-09 - Improvement: Record Breakers Homepage
* 2eaf3f2e: Include latest selection info on the active player games on the Records homepage (_Thierry Draper_)
* 53cdc97f: Improve the selection loading and getting method interface (_Thierry Draper_)
* bb425350: Split game loading logic into, the slightly different, highlighted and current (_Thierry Draper_)
* 0b05202d: Exclude the best score progess bar where the user has not joined the game (_Thierry Draper_)

## Build v3.0.1857 - bb69995c - 2024-11-30 - Improvement: Homepage
* bb69995c: Expand the sitemap to include the individual Record Breaker games (_Thierry Draper_)
* cb2a9c30: Update the game archiving script to keep the mapped selections data, which is needed for further archives (_Thierry Draper_)
* e2c82baf: Redesign the main site homepage to contain more useful info and be less empty (_Thierry Draper_)
* 4eb45bc2: Standardise the progress bar logic between the records game and home pages (_Thierry Draper_)
* d7ff3c3d: Allow for resizable record holder mugshots (_Thierry Draper_)
* 7174dac2: Generalise the sport logo sprite from Record Breakers to global namespace (_Thierry Draper_)

## Build v3.0.1851 - a7227aa4 - 2024-11-18 - Improvement: Open Graph Showing Current Leaders
* a7227aa4: Split out the Game is/has/can tests to their own trait to reduce the class size (_Thierry Draper_)
* 03567090: Include Current Leaders on the Open Graph images for appropriate games (_Thierry Draper_)

## Build v3.0.1849 - a1782b0f - 2024-11-16 - Hotfix: Social Media
* a1782b0f: Tweet wording and punctuation fixes (_Thierry Draper_)
* d34d9032: Include the OG image in Record Broken tweets (_Thierry Draper_)

## Build v3.0.1847 - 6e10aae1 - 2024-11-16 - Hotfix: Missing Game Odds
* 6e10aae1: Relabel missing/unknown game odds as such, rather than an implied even (_Thierry Draper_)

## Build v3.0.1846 - d6df6028 - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build v3.0.1846 - c8aacea7 - 2024-11-11 - Feature: Opponent Status Flags
* c8aacea7: Inform users when selection searches return no results (_Thierry Draper_)
* e06d1f4a: Render the individual selection flags on the main page and edit modal (_Thierry Draper_)
* 65f81ca8: Render the beaten opponents checkbox on the add/edit modal (_Thierry Draper_)
* 59ecff1b: Filter out achieved opponent selections in the selection lookups (_Thierry Draper_)
* ba9f8d62: Fix minor typo in the leaderboard modal (_Thierry Draper_)
* ce751755: Include the achieved opponent flag in the selection lists (_Thierry Draper_)
* 3bfdfc69: Standardise the logic in some of the selection flag getters and setters (_Thierry Draper_)
* f7716310: Render streak buster status on existing selections, not just available selections on the edit modal (_Thierry Draper_)
* 0dc5699e: Move the game flag checks from direct value comparisons to object methods (_Thierry Draper_)
* cec57655: Move selection flags into their own API section (_Thierry Draper_)

## Build v3.0.1836 - 363f26cc - 2024-10-29 - Hotfix: NHL Team Win Streak Name
* 363f26cc: Correct the target value in NHL Game Win Streak game (oops!) (_Thierry Draper_)

## Build v3.0.1835 - b93d1d0f - 2024-10-29 - Feature: Selection Flag API Change Prep
* b93d1d0f: Handle a future breaking API change for selection flags (_Thierry Draper_)

## Build v3.0.1834 - 6c448738 - 2024-10-28 - Hotfix: NHL Comms Template
* 6c448738: Fix the week counter in an NHL weekly comms template (_Thierry Draper_)

## Build v3.0.1833 - 423f9844 - 2024-10-22 - Improvement: Mobile Navigation
* 423f9844: Re-order the entry and help links above the user account page on the mobile nav only (_Thierry Draper_)
* f922eefd: Include the user game list on the mobile homepage (_Thierry Draper_)

## Build v3.0.1831 - f8e341be - 2024-10-21 - SoftDep: October 2024
* f8e341be: Move away from the apparently non-deterministic, and now broken, array-based orWhere clause (_Thierry Draper_)
* 2f1427a8: Auto: NPM Update for October 2024 (_Thierry Draper_)
* 26df97a5: Auto: Composer Update for October 2024 (_Thierry Draper_)

## Build v3.0.1830 - 3cf6016c - 2024-10-17 - SysAdmin: Game Archiving
* 3cf6016c: Standardise the selection download sync script name (_Thierry Draper_)
* 429e4fb1: Include Open Graph images in the dev data downloads (_Thierry Draper_)
* 07de05f8: Validate the no-longer-commited archive symlink is in place when archiving data (_Thierry Draper_)
* 231a9f18: Remove the committed archive location links (_Thierry Draper_)

## Build v3.0.1826 - e3104ab6 - 2024-10-15 - Hotfix: Leaderboard JS
* e3104ab6: Fix the loading of Leaderboard JS relative to the view (_Thierry Draper_)

## Build v3.0.1825 - 07dc295d - 2024-10-15 - Feature: Major League Game Odds
* 07dc295d: Incorporate game odds into the team game selection rating calcs (_Thierry Draper_)
* 0a79aa40: Future proof stat calculations by de-linking IDs from within the calc methods (_Thierry Draper_)
* ed32d12a: Improve the layout and flow of ratings/stats on the selection modal (_Thierry Draper_)
* a699e3e8: Handle rendering a wider number of selection stats (_Thierry Draper_)
* 16f551da: Include game odds as a selection stat in major league team games (_Thierry Draper_)

## Build v3.0.1820 - e316a109 - 2024-10-07 - Feature: Sports Profile Link
* e316a109: Re-base the per-sport game CI tests to match the general test (_Thierry Draper_)
* 8b0cca2c: Include a link to the Sports profile page on the individual selection modal (_Thierry Draper_)
* 914dabe1: Add the Sports profile page URL to the web selection list (_Thierry Draper_)

## Build v3.0.1817 - 51fd2029 - 2024-10-04 - Hotfix: Records Homepage
* 51fd2029: Fix subsite homepage parsing of game active leaders (_Thierry Draper_)

## Build v3.0.1816 - 58cacf1f - 2024-10-04 - Improvement: F1 Lock Times
* 58cacf1f: Rebase, and widen, the server sync order values (_Thierry Draper_)
* fe3fc6f8: Add some lock time unit tests (_Thierry Draper_)
* 56c516e6: Move the default lock offset config value to the global scope, as it is used in the common getter (_Thierry Draper_)
* 43c6523b: Allow for per-game, not per-sport, level lock times (_Thierry Draper_)
* dcd186ff: Remove the formulaic scoring calc method table/field into a per-game code (_Thierry Draper_)

## Build v3.0.1811 - 29e166b6 - 2024-09-29 - Improvement: NFL Calcs
* 29e166b6: Re-work the passing TD opponent ratings to include additional inputs (_Thierry Draper_)
* a8cd4f10: Include Red Zone opportunities in a passing TDs selection rating calc (_Thierry Draper_)
* fda82ae0: Cater for negative total rushing yards in the calcs (_Thierry Draper_)

## Build v3.0.1808 - b9949216 - 2024-09-29 - Hotfix: Extended Season Date Recalc
* b9949216: Extend the season dates during the date recalcs if date(s) are added beyond the current boundary (_Thierry Draper_)

## Build v3.0.1807 - 0347ea0a - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build v3.0.1807 - 36cf4dbe - 2024-09-15 - SysAdmin: CI Tweaks
* 36cf4dbe: Catch Phan run errors that could cause cyclical self-calling (_Thierry Draper_)
* 5308ce31: Fix first run setup for downloading seed data (_Thierry Draper_)
* 70f6fee1: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build v3.0.1804 - f88f33b7 - 2024-09-01 - Hotfix: Multiple Mini Fixes
* f88f33b7: Tweak sync entry upload script to match available permissions on remote server (_Thierry Draper_)
* f4fd7713: Migrate Twitter links to the new domain (_Thierry Draper_)
* 6366577c: Fix a MySQL 8.0 missing IFNULL which results in blank weekly tweets (_Thierry Draper_)
* 04e82a8b: Fix when the leaderboard JS is loaded, as now needed after completion does not mean it is needed all season (_Thierry Draper_)

## Build v3.0.1800 - fea98996 - 2024-09-01 - Feature: Period Score Type
* fea98996: Include the new season arg in the test harness OG preview image creator, and improve the unit test for that arg (_Thierry Draper_)
* 025a8cfb: Add the new periods score type to the game setup README, and backport additional missing changes (_Thierry Draper_)
* aa648e00: Migrate the F1 Pole Position and Fastest Lap games to the new periods score_type (_Thierry Draper_)
* aec36477: Hide the Hit Rate table from the new periods score type games (_Thierry Draper_)
* 48714164: Add a new score type, periods, which is a pseudo-totals game for counting successful periods (_Thierry Draper_)

## Build v3.0.1795 - 0049f375 - 2024-08-21 - Setup: AHL and NHL 2024/25 seasons
* 0049f375: Fix the Open Graph game preview script operating on only the new season (_Thierry Draper_)
* f6da9618: AHL game launch for 2024 (_Thierry Draper_)
* 1acac05f: NHL game launch for 2024 (_Thierry Draper_)
* 4f8342dc: Move the default test harness start date a little closer to the first action date (_Thierry Draper_)
* 047920a5: Capture the user aborting the Record Breakers test harness to reset the temporary changes it applies (_Thierry Draper_)
* c3e10d83: Improved error handling in the Record Breakers test harness (_Thierry Draper_)
* 64026d6e: Add the new AHL and NHL team win streak game to our test processing season (_Thierry Draper_)
* 57193b0a: Processing logic for an AHL and NHL team win streak game (_Thierry Draper_)

## Build v3.0.1787 - a096f5b9 - 2024-08-19 - SoftDep: August 2024
* a096f5b9: Fix warnings for PHPs upcoming implicit nullable param deprecation (_Thierry Draper_)
* 3646e641: Auto: NPM Update for August 2024 (_Thierry Draper_)
* 9b4a776a: Auto: Composer Update for August 2024 (_Thierry Draper_)

## Build v3.0.1786 - 07eb32b6 - 2024-08-16 - Setup: NFL 2024 season
* 07eb32b6: NFL game launch for 2024 (_Thierry Draper_)

## Build v3.0.1785 - 3ea55cdd - 2024-08-13 - Improvement: Record Breakers MySQL Migration
* 3ea55cdd: Apply some per-sport game processing casting fixes (_Thierry Draper_)
* d2413591: Widen the Streak Buster selection instance counts, given SMALLINT could be too small (_Thierry Draper_)
* 0067297b: Migrate the preview and process calc benchmarking timestamps to a MySQL 8.0 friendly wrapper function (_Thierry Draper_)
* cbb1c271: Fix an instance of a temporary table re-use (_Thierry Draper_)
* fca1a9d3: Fix the generic statement preparation queries (_Thierry Draper_)
* c1853651: Migrate the season variables in the stored procedures to SMALLINTs, because YEAR does not work in MySQL (_Thierry Draper_)
* c5ef247a: Migrate our YEAR column schema definition to the deprecated non-width version (_Thierry Draper_)

## Build v3.0.1778 - 9d36ba80 - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* 9d36ba80: Fix the handling of Record Breaker profile centiles (_Thierry Draper_)
* a6c1fcb7: Switch the replace syntax of REGEXP_REPLACE to MySQLs version (_Thierry Draper_)
* e84b3a76: Fix the GROUP BY clauses in Record Breaker game setup due to the ONLY_FULL_GROUP_BY sql_mode (_Thierry Draper_)
* eade0493: Handle the (random) way MySQL seems to convert a YEAR parameter argument to a DATEIME (_Thierry Draper_)
* 660ece42: Handle the way MySQL considers rank as a keyword (_Thierry Draper_)
* 834165dc: Switch to the MySQL way of processing PREPARE statement handles (_Thierry Draper_)
* 4fdc7723: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build v3.0.1771 - c8f198b1 - 2024-08-08 - Improvement: Achievements
* c8f198b1: Correctly parse opponent counts for motorsport games (_Thierry Draper_)
* eaf1529c: Include the new opponent and achievement columns in the entry sync script (_Thierry Draper_)
* 59db0ef6: Hide the Opponent List once they have all been completed (_Thierry Draper_)
* dec91637: Clarify visually the boundary condition of the selection usage achievement values (_Thierry Draper_)

## Build v3.0.1767 - d341ba00 - 2024-07-18 - Hotfix: NFL Perfect Season
* d341ba00: Remove the Streak Busters feature from games which do not restart after the first busted selection (_Thierry Draper_)
* 089fafa5: Fix the ordering used by the last period selection loading method (_Thierry Draper_)

## Build v3.0.1765 - 6273efd6 - 2024-07-17 - Hotfix: SAST Reporting
* 6273efd6: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build v3.0.1764 - 0faea497 - 2024-07-17 - Feature: CI Server Sync Job
* 0faea497: Fix the syntactic issues within the Record Breaker sync scripts (_Thierry Draper_)
* 1f9fe705: Add a new CI lint script for our individual Record Breaker syncs (_Thierry Draper_)
* 7985991e: Include the server sync database to our CI job list (_Thierry Draper_)

## Build v3.0.1761 - 2dee7089 - 2024-07-16 - SoftDep: July 2024
* (Auto-commits only)

## Build v3.0.1761 - 0d772735 - 2024-07-14 - Feature: User Achievements
* 0d772735: Handle an MLB scheduling quirk where teams could be home-and-visitor on the same day (_Thierry Draper_)
* 35e03835: Include achievement progress in the profiles API endpoint (_Thierry Draper_)
* 9404f181: Propagate the entry achievement calcs to their profile entry (_Thierry Draper_)
* 716e0697: Render the achievements, and opponent list if available, for logged in players (_Thierry Draper_)
* 1bac893d: Add a new API endpoint showing the opponent statii for an entry within a game (_Thierry Draper_)
* 1b4c113a: Add a new API endpoint showing the achievements for an entry within a game (_Thierry Draper_)
* 70ca9850: Add a new API endpoint listing the available achievements within a game (_Thierry Draper_)
* 3c95654d: Calculate the per-entry achievements (_Thierry Draper_)
* e2cd50b4: Determine the per-user list of opponents targetted in Major League games (_Thierry Draper_)
* d1b16b66: Include the original per-game achievement list (_Thierry Draper_)
* 338ca5b5: Add the schema for per-user achievement processing (_Thierry Draper_)

## Build v3.0.1750 - ef50cec9 - 2024-06-28 - Hotfix: Secondary Integration Flags
* ef50cec9: Exclude secondary integration checks that have failed for a sport (_Thierry Draper_)

## Build v3.0.1749 - ccbf63de - 2024-06-28 - Hotfix: SGP Entity Status
* ccbf63de: Apply the motorsport status fix to the SGP entities (_Thierry Draper_)

## Build v3.0.1748 - 20af0f1e - 2024-06-28 - Hotfix: Records Database Calcs
* 20af0f1e: Tie motorsport selection statuses to the correct game period when determining selection lists (_Thierry Draper_)
* e3a0300c: Temorarily increase the maximum size of temp tables for Streak Buster calcs (_Thierry Draper_)

## Build v3.0.1746 - bfaeaa1f - 2024-06-21 - Hotfix: Reported Errors
* bfaeaa1f: Fix the team entity loading process to dedupe the requested links (_Thierry Draper_)
* 0b144237: Re-define our npm version lock on puppeteer-core, and upgrade it to the latest version (_Thierry Draper_)
* d248abf7: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build v3.0.1743 - 74ea1093 - 2024-06-18 - SoftDep: June 2024
* (Auto-commits only)

## Build v3.0.1743 - cc28c537 - 2024-06-17 - Feature: Streak Busters
* cc28c537: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* fc7187a6: Improve the location of the CI download trust store cert (_Thierry Draper_)
* 666aae1e: Include the Streak Buster flag within API selection calls (_Thierry Draper_)
* dd08605b: Render the entry streak busters on their summary page (_Thierry Draper_)
* 54c80502: Render the Streak Busters alongside appropriate selection options (_Thierry Draper_)
* 05a89c84: Add the calculations for performing the per-game Streak Busters (_Thierry Draper_)
* 72a8a14f: Move our per-game setup table resets to a stored procedure, and include our new Streak Busters table (_Thierry Draper_)
* 41bbbfed: Add the database table to hold our streak buster info (_Thierry Draper_)

## Build v3.0.1735 - 5b06404a - 2024-06-01 - Improvement: Selection CTAs
* 5b06404a: Improve the layout of the selection CTAs (_Thierry Draper_)

## Build v3.0.1734 - 6c20dd27 - 2024-06-01 - SysAdmin: Record Breaker Uploads
* 6c20dd27: Move the creds location to the new per-env location (_Thierry Draper_)

## Build v3.0.1733 - c4c714ba - 2024-05-22 - Hotfix: MLB Team Doubleheaders
* c4c714ba: Correctly identify MLB Doubleheaders in team games, not just individual player games (_Thierry Draper_)

## Build v3.0.1732 - a5199390 - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build v3.0.1732 - 55d98457 - 2024-05-16 - Improvement: Archived Game
* 55d98457: Restore the per-entry results when procesesed before the periods end_date (_Thierry Draper_)
* 46359ae6: Restore the leaderboard tab switching upon game completion (_Thierry Draper_)
* 0aafd1ea: Store the active leader per-period, and use the best season-long leader value upon game completion rather than leader after the final period (_Thierry Draper_)
* 24150b59: Limit the timeframe in which Game End emails are sent, to be between the final period and our complete date (_Thierry Draper_)

## Build v3.0.1728 - b166e197 - 2024-05-13 - Security: CI Jobs
* b166e197: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* 3e5ae7b1: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* 69bff810: Modifications following output of the initial SAST artifacts (_Thierry Draper_)
* f0a04a04: Implement an initial SAST reporting tool (_Thierry Draper_)
* 5f905fd9: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* ebb7ce5f: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* 77958626: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build v3.0.1721 - bd88d2eb - 2024-04-27 - Improvement: Entry Game Order
* bd88d2eb: Define an improved entry game order when in-season listed (_Thierry Draper_)

## Build v3.0.1720 - c1fdd507 - 2024-04-27 - Improvement: Team Silhouette
* c1fdd507: Preserve aspect ratio of the fallback team silhouettes, not just an actual selection (_Thierry Draper_)

## Build v3.0.1719 - 01a6fcb7 - 2024-04-27 - Hotfix: Autouser Selections
* 01a6fcb7: Fix autouser selections by-stat, which were ranked by the inverse metric (_Thierry Draper_)
* 0ef97f11: Exclude unsuitable (individual) selections from the autouser selection pool (_Thierry Draper_)

## Build v3.0.1717 - 237d0121 - 2024-04-27 - Improvement: CI Downloads
* 237d0121: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build v3.0.1716 - cc9e305c - 2024-04-24 - Improvement: JavaScript Formatting
* cc9e305c: Apply some minor eslint hint tweaks to the JavaScript (_Thierry Draper_)
* 4e4c800f: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* cdff0b1f: Migrate to eslints flat config format (_Thierry Draper_)

## Build v3.0.1713 - 6d1b4684 - 2024-04-21 - SoftDep: April 2024
* (Auto-commits only)

## Build v3.0.1713 - 59a5b97d - 2024-04-20 - Hotfix: Season End Handling
* 59a5b97d: Handle uploading end-of-season Open Graph images in the top-level CDN location (_Thierry Draper_)
* 89ebd2aa: Clarify the game end date should be the day after the final period, not the day that it ends (_Thierry Draper_)
* 479b28ce: Propagate the end date of the final period to secondary locations when re-calcing period dates (_Thierry Draper_)
* d5dbc1d9: Include the final period when rendering team selections at the conclusion of the season (_Thierry Draper_)

## Build v3.0.1709 - dc5ff75e - 2024-04-18 - Improvement: Client-side Processing
* dc5ff75e: Make the JavaScript AJAX response cache TTLs customisable (_Thierry Draper_)
* 4debfae5: Ensure a plausible fallback error message is produced on AJAX requests based on HTTP response code (_Thierry Draper_)
* 74be109c: Update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)

## Build v3.0.1706 - bfff6b82 - 2024-04-14 - Setup: SGP 2024 season
* bfff6b82: Backport our simplified per-email date setup logic to the other F1 and SGP setup scripts (_Thierry Draper_)
* ef90c618: SGP game launch for 2024 (_Thierry Draper_)

## Build v3.0.1704 - 83b59c53 - 2024-04-14 - Hotfix: Current/Overall Pos and Res
* 83b59c53: Incrementally count team selection scores from either the overall, or if applicable current, value (_Thierry Draper_)
* d01a9e10: Render the correct position field in the Current Rank team modal field (_Thierry Draper_)

## Build v3.0.1702 - f455aa1f - 2024-04-14 - Feature: Game End Email
* f455aa1f: Send, via artisan command, end-of-game email updates to users upon game completion (_Thierry Draper_)
* 67a62b61: Include the email audit table in the game sync process (_Thierry Draper_)
* e6a284ec: Implement a new per-sport email audit log, along with equivalent models (_Thierry Draper_)

## Build v3.0.1699 - b76a6b70 - 2024-04-09 - Improvement: Season End Dropoff Calcs
* b76a6b70: Correctly render the Entry Dropoff when reaching the end of the season (_Thierry Draper_)
* a711c2a5: Consider how the Entry Dropoff calcs need to adapt to the final periods of the season (_Thierry Draper_)

## Build v3.0.1697 - e7b0adfb - 2024-04-09 - Improvement: Major League Schedule Status
* e7b0adfb: Prioritise the Major League game status over the individual selection status (_Thierry Draper_)

## Build v3.0.1696 - 58eea949 - 2024-04-09 - Hotfix: AutoUser Sel Calcs
* 58eea949: Widen the AutoUser selection user_score column to account for larger scores later in a season (_Thierry Draper_)

## Build v3.0.1695 - fe9da731 - 2024-04-09 - Improvement: F1 Fastest Lap Summary
* fe9da731: Make better use of the F1 pre-calced Grand Prix race number (_Thierry Draper_)
* b6f9c56b: Render the driver code of the Fastest Lap holder in each race, rather than per-selection status text (_Thierry Draper_)

## Build v3.0.1693 - 65885a08 - 2024-04-03 - Improvement: Open Graph Retention
* 65885a08: Apply a file retention policy on the Open Graph images on live (_Thierry Draper_)

## Build v3.0.1692 - b3718f1b - 2024-03-30 - Hotfix: MLB vs Opposing SP Stat Calc
* b3718f1b: Fix the MLB vs Opposing SP stat calc (_Thierry Draper_)

## Build v3.0.1691 - 419a89da - 2024-03-30 - Improvement: Selection Editing Cache TTL
* 419a89da: Reduce the selection editing cache TTL to reduce its persistence and reflect improved performance on the new infra (_Thierry Draper_)

## Build v3.0.1690 - 00da45af - 2024-03-24 - Hotfix: Open Graph image path loading
* 00da45af: Fix the Twitter config loading of the new Open Graph image path (_Thierry Draper_)

## Build v3.0.1689 - 3fb8933b - 2024-03-24 - Improvement: MLB Two-Way Players
* 3fb8933b: Include our new Two-Way MLB player code in the Record Breaker selection pool (_Thierry Draper_)

## Build v3.0.1688 - f8d272af - 2024-03-20 - Hotfix: Major League Entity Loading
* f8d272af: Fix entity loading for sports where period references are not ID based (_Thierry Draper_)

## Build v3.0.1687 - 4b5375ab - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* 4b5375ab: Upgrade dependencies to PHPUnit 11 (_Thierry Draper_)
* dee2a263: Switch use of the now-removed $dates property in Eloquent to its $casts equivalent (_Thierry Draper_)
* 1d0d4e3a: Upgrade dependencies to PHPUnit 10 (_Thierry Draper_)

## Build v3.0.1684 - 0b23642c - 2024-03-16 - Improvement: F1
* 0b23642c: Use the updated homedir following the hosting move (_Thierry Draper_)
* fdd84851: Fix the Record Breaker test harness to process initial dates set after 8pm (_Thierry Draper_)
* 9ac5d505: Add a missing closing brace in an F1 tweet placeholder (_Thierry Draper_)
* 1ee393f6: Use the correct period when loading the entity info for those in the period summary (_Thierry Draper_)
* ee5a5670: Correctly render the status of replaced motorsport entrants (_Thierry Draper_)
* 87af8608: Continue to mark selections as locked even after their locktime (_Thierry Draper_)
* 2b16e844: Fall back to the current round when rendering the hit rate of the first period (_Thierry Draper_)
* 5de2228a: Fix the next-period preview logic to account for week-plus long periods calced before the end date (_Thierry Draper_)
* bf7d966a: Fix the logic used to determine the round(s) we need to upload to live (_Thierry Draper_)

## Build v3.0.1675 - 50cfd5f1 - 2024-03-06 - Hotfix: Game Period Date Fixing Redux
* 50cfd5f1: Restore the middle ground for major league period date fixing to account for both future, incomplete, games and the test harness (_Thierry Draper_)

## Build v3.0.1674 - d2f9af5a - 2024-03-06 - Feature: Selection List Caching
* d2f9af5a: Correct some minor comment typos (_Thierry Draper_)
* dbe12d9a: Make use of the selection list caching timeframes (_Thierry Draper_)
* 8af9e0d2: Set selection list caching headers, up to a configurable period per-sport prior to an events lock time (_Thierry Draper_)

## Build v3.0.1671 - 6ae24758 - 2024-02-25 - Hotfix: Game Period Date Fixing
* 6ae24758: Adapt the game period date fixer to be compatible with the test harness (_Thierry Draper_)

## Build v3.0.1670 - ff7cb310 - 2024-02-25 - Hotfix: Leaderboard Tabs
* ff7cb310: Fix rendering of leaderboard tabs when no current table available (_Thierry Draper_)

## Build v3.0.1669 - cab31a30 - 2024-02-25 - Feature: Selection Weather Forecasts
* cab31a30: Incorporate weather forecasts to the per-event edit modal (_Thierry Draper_)
* 089a351c: Render weather on a per-selection basis (_Thierry Draper_)
* 366161ff: Incorporate weather forecasts to the per-period selection lists (_Thierry Draper_)

## Build v3.0.1666 - f59746e1 - 2024-02-24 - Setup: MLB 2024 season


## Build v3.0.1665 - 3b14e9db - 2024-02-22 - Improvement: Leaderboard Rankings
* 3b14e9db: Fix rendering of the hitrate leaderboard modal (_Thierry Draper_)
* fbdddd4a: Improve the way tied rankings are displayed on the leaderboards (_Thierry Draper_)

## Build v3.0.1663 - 56bc901c - 2024-02-20 - Hotfix: Selection Editing Modal
* 56bc901c: Hide the entry hitrate table until it has been calculuated (_Thierry Draper_)
* 8591bb76: Fix the selection filtering dropdown arrow toggling (_Thierry Draper_)
* 68c08348: Add non-standard responsive widths for selection editing stat tables to ensure full width is used based on the number to display (_Thierry Draper_)
* ed2b3ccd: Tidy the selection sorting dropdown on the non-filterable selection editing modal (_Thierry Draper_)

## Build v3.0.1659 - e6e6b66b - 2024-02-18 - Improvement: Game Period Dates
* e6e6b66b: Automatically merge and split future game periods depending on changes to the daily (AHL, NHL, MLB) schedule after initial import (_Thierry Draper_)

## Build v3.0.1658 - b9c748bb - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build v3.0.1658 - 12c624fa - 2024-02-17 - Hotfix: F1 Integration
* 12c624fa: Make a minor F1 season opening wording tweak to account for the shortened pre-season (_Thierry Draper_)
* 98668bab: Move Social Media processing to occur on all processing runs, not just round processing mode (_Thierry Draper_)

## Build v3.0.1656 - 6fb9a7eb - 2024-02-17 - Hotfix: Success Rate calcs
* 6fb9a7eb: Sync the new hitrate columns in the data uploader process (_Thierry Draper_)
* 65106945: Calculate the in-page hit rate by excluding the N/A calcs to match the rating processing (_Thierry Draper_)
* 2eaf1c41: Apply the hitrate calcs against the full selection history, not just the parsed-for-scoring selection list (_Thierry Draper_)

## Build v3.0.1653 - 2ee46b75 - 2024-02-15 - Setup: F1 2024 season
* 2ee46b75: Implement a game importing wrapper script, rather than having the checks and follow-on steps within each game setup script (_Thierry Draper_)
* 8b997dbf: Move the automated Twitter posting schedule to its own table, given we cannot rely on suitable dates from the GAME.date_* columns where periods exceed a single day (_Thierry Draper_)
* 3adc8aca: F1 game launch for 2024 (_Thierry Draper_)

## Build v3.0.1650 - 1d1d9b1a - 2024-02-14 - Feature: Selection Analysis
* 1d1d9b1a: Include a least of the most common selections by an entry in a game (_Thierry Draper_)
* 70debafb: Render the entry success rate as a pseudo-stacked solidgauge chart (_Thierry Draper_)
* 0cfdc087: Fix the rendering of lock times / statuses for PPD games within the API (_Thierry Draper_)
* e750ba9b: Apply the Success Rate leaderboards and fields within the API (_Thierry Draper_)
* dbc060f7: Implement a new Success Rate leaderboard and ordinalise the positions (_Thierry Draper_)
* 891daec4: Calculate the per-entry hitrate within the game processing calcs (_Thierry Draper_)
* 0e9ae8af: Standardise the integer width of the user_id columns (_Thierry Draper_)
* f368cb1e: Fix a minor typo in the test harness helper (_Thierry Draper_)
* 2a64e02c: Merge the entry selection and score progress tables, both 1:1:1 between Entry:Game:Period (_Thierry Draper_)

## Build v3.0.1641 - 3bb240c3 - 2024-02-06 - Hotfix: Cache Prefix Timestamp Calcs
* 3bb240c3: Fix the storage cache prefix last integration timestamp calcs (_Thierry Draper_)

## Build v3.0.1640 - af6ab438 - 2024-02-04 - Improvement: Modal Data Persistent Caching
* af6ab438: Persist modal data caching using the new localStorage wrapper (_Thierry Draper_)

## Build v3.0.1639 - 03412f50 - 2024-01-31 - Improvement: Admin API Roles
* 03412f50: Expand Admin API role to include the getter methods (_Thierry Draper_)

## Build v3.0.1638 - 018e07a6 - 2024-01-31 - Hotfix: Mobile Navigation Layout
* 018e07a6: Introduce a better scope for too-loosely defined in-page styling (_Thierry Draper_)

## Build v3.0.1637 - fb419c12 - 2024-01-31 - Feature: In-Game Switcher
* fb419c12: Apply the game switcher on the other relevant pages within the mobile nav (_Thierry Draper_)
* b05d84ea: Introduce an in-page game switcher between the entered games (_Thierry Draper_)

## Build v3.0.1635 - 64a182f2 - 2024-01-27 - Hotfix: Selection Actions
* 64a182f2: Fix overlapping content on the selection action buttons (_Thierry Draper_)

## Build v3.0.1634 - 0c28eb71 - 2024-01-27 - Improvement: Refactoring JavaScript
* 0c28eb71: Make use of the event attaching instantiation improvements (_Thierry Draper_)
* 78b15fbc: Apply the latest round of micro-optimisations and small improvements (_Thierry Draper_)
* 515152e5: Revert PHPCS "linting" enforced JavaScript standards for curly brace positioning (_Thierry Draper_)
* 4b821de5: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* 07f493bf: Move to greater use of the arrow functions (_Thierry Draper_)
* 70869238: Make better use of template literals in string expressions (_Thierry Draper_)
* 7faa165c: Move away from the deprecated centralised DOM querySelector parent node argument (_Thierry Draper_)
* b90a07df: Replace use of the deprecated DOM.get* methods in favour of the generalised query selector (_Thierry Draper_)

## Build v3.0.1626 - a689d5d2 - 2024-01-19 - Hotfix: Leaderboard Pluralisation
* a689d5d2: Correctly pluralise the game Leaderboard title (_Thierry Draper_)

## Build v3.0.1625 - af993236 - 2024-01-19 - Hotfix: Postponed Game Date Handling
* af993236: Make use of the optional chaining JS operator to simplify the code (_Thierry Draper_)
* 08b10f39: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)
* d373d093: Make use of the nullish coalescing operator to simplify the code (_Thierry Draper_)
* 31799d60: Mark as (implied) PPD a selections info field for a redated Major League schedule row (_Thierry Draper_)

## Build v3.0.1621 - af2d34d7 - 2024-01-18 - Improvement: Game Archive Rendering
* af2d34d7: Add a database backup stage to the record breakers gameflow test harness (_Thierry Draper_)
* bae9a7cd: Fix score formatting in APIs when the value includes thousand separators (_Thierry Draper_)
* ca1cd0f4: Handle within the test harness the scenario where the test user is eliminated from a game and cannot make further selections (_Thierry Draper_)
* 8533cadd: Improve test harness API selection handling to now corretly handle locked (by time or status) or unavailabe scenarios (_Thierry Draper_)
* a462b693: Extend the running period of the test harness to capture each game in its archive view (_Thierry Draper_)
* f99a099c: Fix PHP opcache issues with the dynamic dates in the Record Breaker test harness (_Thierry Draper_)
* ced6c90c: Improve the game archiving script to preserve the database content we need for the archive view (_Thierry Draper_)
* 9ebb688d: Implement a new long-running archive view of the game page once it has completed, rather than hiding and returning a 404 (_Thierry Draper_)
* 094a1062: Store a copy of the final Open Graph image in a fixed, name-by-game_ref location (_Thierry Draper_)

## Build v3.0.1612 - bc283be8 - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build v3.0.1612 - 1aab2bf0 - 2024-01-10 - Improvement: Daily Rollover
* 1aab2bf0: Present user-friendly selection names in the daily rollover ouput (_Thierry Draper_)
* d243e13a: Correctly parse the date flags passed via the CLI to the rollover script (_Thierry Draper_)
* 52fabcc1: Explicitly log users at the end of a record breakers rollover day, now we are preserving the browser dir (_Thierry Draper_)
* 160b3434: Consolidate record breaker rollover error logging into the on-disk log file (_Thierry Draper_)

## Build v3.0.1608 - e1db847d - 2024-01-10 - Feature: F1 Additional Games
* e1db847d: Implement a new F1 game to tackle the Fastest Laps record (_Thierry Draper_)
* b8bc2db1: Implement a new F1 game to tackle the Pole Position record (_Thierry Draper_)

## Build v3.0.1606 - 9f1a5bf2 - 2024-01-10 - Hotfix: Major Leagues calendar crossover calcs
* 9f1a5bf2: Fix the preview season calc for Major Leagues crossing two caledanr years (_Thierry Draper_)

## Build v3.0.1605 - 1bd9ba2b - 2024-01-01 - Hotfix: Hiding Selections
* 1bd9ba2b: Standardise open graph meta image generation to use a single temporary browser dir across all runs (_Thierry Draper_)
* 80cc9487: Allow customisation of the Record Breakers rollover process via command line args (_Thierry Draper_)
* d8691738: Handle hiding other entry selections on the leaderboard modal until the completion of the period processing (_Thierry Draper_)
* f7f23f4b: Handle hiding other entry selections on the team modal until the completion of the period processing (_Thierry Draper_)
* a451f1c3: Fix the Eloquent Compound Key attribute of certain ORM models (_Thierry Draper_)
* 29595513: Reprioritise ratings for selections that are not expected to participate in a period (_Thierry Draper_)

## Build v3.0.1599 - 50e476ed - 2023-12-29 - Improvement: Desktop Selection View
* 50e476ed: Tweak the selection layout to maximise desktop viewport space (_Thierry Draper_)

## Build v3.0.1598 - 52dc28b0 - 2023-12-23 - Hotfix: Tablet Viewport Edit Modal
* 52dc28b0: Sync scroll pos of all selections when scrolling the edit modal in tablet viewport (_Thierry Draper_)
* f08f93df: Fix the auto-scrolling of the edit modal stat list in tablet viewport (_Thierry Draper_)

## Build v3.0.1596 - ee18db00 - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build v3.0.1596 - ec4bdb24 - 2023-12-03 - Hotfix: Dropoff Details
* ec4bdb24: Include the dropoff details when available, not when it takes effect (_Thierry Draper_)

## Build v3.0.1595 - e89f39d6 - 2023-12-03 - Improvement: Silent Error Codes
* e89f39d6: Flip the silent error code from the too-generic 1 to a more appropriate range (_Thierry Draper_)

## Build v3.0.1594 - 852d283b - 2023-11-28 - Feature: Selection Usage Table
* 852d283b: Include the missing sysmon setup in the PHP Unit dev setup scripts (_Thierry Draper_)
* 20c29f4e: Improve the process-tomorrows-period checker in the processing calcs (_Thierry Draper_)
* d3022a33: Render an, initially NFL-centric, selection usage table for active entries in a non-sel_reuse game (_Thierry Draper_)

## Build v3.0.1591 - 793cc3ff - 2023-11-27 - CI: Standardised CSS Standards
* 793cc3ff: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build v3.0.1590 - de0e70fb - 2023-11-27 - Feature: Backfill New Selections
* de0e70fb: Backfill new selections during the processing stage that were added after the last round of preview calcs (_Thierry Draper_)

## Build v3.0.1589 - a0ff7116 - 2023-11-23 - Improvement: Display Historical Selection Inactive Status
* a0ff7116: Identify a reason for a selection not playing in a period, if known (_Thierry Draper_)

## Build v3.0.1588 - c8b333ae - 2023-11-23 - Improvement: Tweet Media
* c8b333ae: Include an all-games wrapper to the server_sync generation process (_Thierry Draper_)
* d1d4c467: Fix the registration closing tweet which may include the game slug (_Thierry Draper_)
* 6bd4574a: Include the Open Graph image as tweet media in the weekly updates (_Thierry Draper_)

## Build v3.0.1585 - 76db98b1 - 2023-11-17 - Improvement: AHL Player Statuses
* 76db98b1: Include the new AHL player statuses (_Thierry Draper_)

## Build v3.0.1584 - 02745f47 - 2023-11-15 - Hotfix: Lockfile Column Switch
* 02745f47: Switch the integration lockfile check from upload to finished (_Thierry Draper_)

## Build v3.0.1583 - 3c9f6735 - 2023-10-26 - Hotfix: Game Page Fixes
* 3c9f6735: Include sports logo on the game title (_Thierry Draper_)
* 5d29c66c: Display the selections stress score on their section, not entry stress score (_Thierry Draper_)
* eaefc17f: Fix template mode of the team modal (_Thierry Draper_)

## Build v3.0.1580 - 813baedd - 2023-10-26 - Improvement: Mobile Edit Selection Layout
* 813baedd: Re-jig the selection info field on edit selection modal to increase space for the name (_Thierry Draper_)

## Build v3.0.1579 - 24f86c46 - 2023-10-26 - Hotfix: Twitter OpenGraph Handling
* 24f86c46: Improve the game name to avoid Fantasy repetition (_Thierry Draper_)
* 791b05cf: Reword certain game Tweets so they do not abrubtly end on a removed URL (_Thierry Draper_)
* 43a282f4: Fix the SQL of the Open Graph preview image game query (_Thierry Draper_)
* e84fef22: Generate Open Graph images at the Twitter ratio (_Thierry Draper_)
* e0978bf2: Drop the Twitter-specific game Open Graph image column (_Thierry Draper_)

## Build v3.0.1574 - b2c9b4ec - 2023-10-26 - Feature: Active Limit Selection Dropoff
* b2c9b4ec: Replace the server_sync use of MySQL variables when using an equivalent in bash (_Thierry Draper_)
* e0cc4275: Include the new dropoff table in the setup scripts (_Thierry Draper_)
* 28e8b039: Indicate on the game page the scores that will shortly stop contributing a team total in games with an active selection limit (_Thierry Draper_)
* b24f933a: Calculate the per-team trailing dropoff scores for games with an active selection limit (_Thierry Draper_)

## Build v3.0.1570 - 212dd6cf - 2023-10-20 - Hotfix: Latest Fixes
* 212dd6cf: Run preview calcs for the next two days (NOT nec. periods) in case of integration issues (_Thierry Draper_)
* 179d39a4: Include a warning on the game page if we have detected a delay to the data integration processing (_Thierry Draper_)
* 3b240fce: Ensure the removing of unused rows in the team/selection modals are applied to the modal in question only (_Thierry Draper_)
* c200a3de: Remove use of the projected total from the hockey point total social media posts, as we are now restricting the total to a set number of periods (_Thierry Draper_)
* cf232e41: Fix the calculation of projected totals in the current leader social media processing (_Thierry Draper_)
* f998a0a6: Fix the (preview) stat sorting calculation window clause (_Thierry Draper_)

## Build v3.0.1564 - 21726587 - 2023-10-18 - Improvement: Daily Rollover
* 21726587: Include the option of screenshotting in the daily rollover script by viewport (_Thierry Draper_)
* 601bf31a: Move aspects of the daily rollover to feature flags (_Thierry Draper_)
* 8bd356da: Improve daily rollover logging and propogation of the tmp dir (_Thierry Draper_)
* e909739c: Preserve the .env file used, as well as version prior, when running the daily rollover (_Thierry Draper_)
* 5566d982: Better render motorsport outcomes (_Thierry Draper_)
* 8dfb2557: Improve game page rendering on a landscape tablet when two periods and the leaderboard are displayed (_Thierry Draper_)
* 58248c25: Render results as soon as they are processed within a period (_Thierry Draper_)

## Build v3.0.1557 - 85a2fed6 - 2023-10-14 - Improvement: Motorsport Injured Selections
* 85a2fed6: Include motorsport selections that are not directly entered to a race/meeting but flagged in our status list (_Thierry Draper_)

## Build v3.0.1556 - 6f8348b4 - 2023-10-14 - Improvement: Latest Record Breaker Tweaks
* 6f8348b4: Switch from the sport database timezone to app timezone (_Thierry Draper_)
* 54f0d9a4: Include a Scores TBD display state when waiting for the results to be processed (_Thierry Draper_)
* 0048147c: Link to the specific DeBearRecords twitter account from within the game (_Thierry Draper_)
* 04035aa8: Use the result, not position, on current best score progress bar (_Thierry Draper_)

## Build v3.0.1552 - 589a4d32 - 2023-10-14 - Composer: October 2023
* (Auto-commits only)

## Build v3.0.1552 - afd53d61 - 2023-10-14 - Improvement: Daily Rollover
* afd53d61: No longer require the ALLOWED env variable when running the daily rollover process, as it is no longer destructive (_Thierry Draper_)
* 1538cae4: Ensure the environment is tidied up after daily rollover errors, as we all as on success (_Thierry Draper_)
* 215d066f: Randomise the Daily Rollover API user selection (_Thierry Draper_)
* d6c18cb5: Backport the Puppeteer screenshot arguments change to the Open Graph image generate processes (_Thierry Draper_)
* 4326b0d3: Migrate the daily rollover script to the new Puppeteer screenshot script (_Thierry Draper_)
* 69709173: Re-work the Puppeteer screenshot script to accept multiple pages per run, including a login option (_Thierry Draper_)
* 6a5092a5: Include API requests and logged in user screenshots as part of the daily rollover process (_Thierry Draper_)
* e7430e77: Move the daily rollover process from overwriting the main dev database, to its own separate environment (_Thierry Draper_)

## Build v3.0.1544 - d76531fe - 2023-10-12 - Hotfix: Post Launch Tidy
* d76531fe: Apply some server-sync fixes, and add explicit option for Data to Dev syncing (_Thierry Draper_)
* 4aa3d905: Switch Open Graph image generation from wkhtmltoimage to puppeteer-core (_Thierry Draper_)
* 86b3ccc9: Ensure Open Graph image generation errors are logged to STDERR (_Thierry Draper_)
* ed8a40fb: Fix the Data to Live entry upload script (_Thierry Draper_)
* e354da68: Fix mobile leaderboard styling widths (_Thierry Draper_)
* fb9b1fdb: Ensure Daily Roster loads the latest rosters, not necessarily that day's explicitly (_Thierry Draper_)

## Build v3.0.1538 - 9b27874a - 2023-10-05 - Hotfix: Email Sport Suggestions Logos
* 9b27874a: Ensure the protocol is included in email game logo suggestions (_Thierry Draper_)

## Build v3.0.1537 - a85dd301 - 2023-10-04 - Hotfix: Server Sync Script Links


## Build v3.0.1536 - 7c80aa9f - 2023-10-04 - Hotfix: Committed Record Breaker Dates
* 7c80aa9f: Fix the (non-env) Record Breaker dates, plus mark as enabled on live (_Thierry Draper_)

## Build v3.0.1535 - 822070d2 - 2023-10-04 - SysAdmin: Record Breakers Launch
* 822070d2: Officially launch the Record Breakers game! (_Thierry Draper_)
* 68ce99bc: Backport recent refinements to the test/dev game setup scripts and process (_Thierry Draper_)
* 9984f00e: AHL and NHL game launches for 2023/24 (_Thierry Draper_)

## Build v3.0.1532 - 1fc250cd - 2023-10-04 - SysAdmin: Game Archive
* 1fc250cd: Implement a process for archiving a Record Breakers sporting season (_Thierry Draper_)

## Build v3.0.1531 - 42ea53d5 - 2023-10-03 - Feature: Game Preview Open Graph Image
* 42ea53d5: Add a version of the Open Graph image generation process for before the game launches (_Thierry Draper_)

## Build v3.0.1530 - 113330b6 - 2023-10-03 - Improvement: Minor Tidying
* 113330b6: Use a secondary library for generating AutoUser team names (_Thierry Draper_)
* 6b68ea89: Fix the logic in processing preview-only calcs (_Thierry Draper_)
* 81a719c7: Make pre-season preview calcs daily, but skipped if no selections available (_Thierry Draper_)
* 583621c0: Move holder images on-disk files to be stored by their game_ref (_Thierry Draper_)

## Build v3.0.1526 - 39e24003 - 2023-09-30 - Hotfix: Data Processing
* 39e24003: Make the OG image creation and caching domain agnostic (_Thierry Draper_)
* 636c7e33: Implement Preview/Processing only processing logic flags (_Thierry Draper_)

## Build v3.0.1524 - bea82f5c - 2023-09-26 - Improvement: Data Sync Process
* bea82f5c: Tidy the game setup scripts following the social media post generation changes (_Thierry Draper_)
* 6872c56f: Add tweaks to the daily processing test harness to include the social media post generation (_Thierry Draper_)
* f7c6663e: Add, generalised, social media post generation, and apply a variety of fixes and improvements to the Twitter processing (_Thierry Draper_)
* 80bc6965: Fix the custom SQL error codes so they do not get caught by the NOT FOUND handlers (_Thierry Draper_)
* 5dbdc378: Add a new column to log when a data in a game period has had its period update Tweets sent (_Thierry Draper_)
* f16ee757: Fix unit tests affected by the Social Media Open Graph meta tag tweaks (_Thierry Draper_)
* d339513b: Advise when selections are not yet available, allowing for a longer game intro period (_Thierry Draper_)
* ecbd2226: Fix processing event calcs for motorsport games (_Thierry Draper_)
* 7eebe911: Include a server-sync emulation step to the daily-processing test harness (_Thierry Draper_)
* d301c834: Define the generic server_sync process and implement for our current games (_Thierry Draper_)
* 9ccd1f9c: Implement a period-sensitive data upload script (_Thierry Draper_)
* d41ab837: Add a new column to log when a data in a game period has been synchronised to live (_Thierry Draper_)
* 4a344f0a: Remove the (unused) Open Graph HTML file from disk after image generation (_Thierry Draper_)

## Build v3.0.1511 - ecd6ff69 - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build v3.0.1511 - 6f786c77 - 2023-09-18 - Improvement: Calculation Process
* 6f786c77: Preserve entry scoring by period (_Thierry Draper_)
* 4910d814: Tweak the display of progress bars (_Thierry Draper_)
* eb65dec8: Correctly render selection summary stats during their first period, when they are all NULL (_Thierry Draper_)
* 98a5ec20: Fix the selection JSON after the period deadline, but before its end date (_Thierry Draper_)
* f939e383: Leaderboard and Team modal JavaScript libraries should load only upon completion of the first period (_Thierry Draper_)
* cdab156a: Major League period-wide lock times should calculated on regular season games only (_Thierry Draper_)
* 7194aa70: Allow period summaries to be displayed before the end date (_Thierry Draper_)
* 3beec22d: Preserve selections that may have been originally calculated (and used) but may have dropped off (_Thierry Draper_)
* e84fad99: Refine the processing and preview logic across the scope of the period (_Thierry Draper_)
* 08e9e6e8: Move AutoUser selection calcs from the preview to processing stages (_Thierry Draper_)
* 28ef3abf: Improved audit detail for AutoUser interactions (_Thierry Draper_)
* c4db837a: Make the auditing of AutoUser selections configurable (_Thierry Draper_)
* 718e858d: Ensure the Overall Leader progress bar is always displayed on the homepage, even when otherwise masked by the Target (_Thierry Draper_)
* f2974f49: Standardise selection summary percentages across the various stores (_Thierry Draper_)
* 6a850eab: Fix entry modal selections when eliminated from a limited-availabilty game (_Thierry Draper_)
* a62c0eb3: Stop AutoUser selections re-using previously used links when game does not allow it (_Thierry Draper_)
* 2eab19b8: Fix selection and entity result display formats (_Thierry Draper_)
* 0fa3fd96: Fix entity loading if the same period_id is being loaded multiple times for a selection (_Thierry Draper_)
* 801cd7e2: Add Current (if appropriate) and Active/Sporting leader to the homepage preview bars (_Thierry Draper_)
* ec41f6b1: Fix the game leader loading query when multiple games are being processed (_Thierry Draper_)
* 5b1044f9: Fix the auto-user selection logic when determining if entries are eligble to continue (_Thierry Draper_)
* f1731ae9: Remove the preview table maintenance steps for performance (_Thierry Draper_)
* 18525f15: Convert the internal ranking mechanisms to MariaDBs native RANK() or ROW_NUMBER() (_Thierry Draper_)
* ecc60e6a: Increase the temporary table memory allowed to the auto-user sel calc script (_Thierry Draper_)
* 77ca68dd: Add benchmarking info to database processing logs (_Thierry Draper_)
* 7aa58bbc: Tidy the Record Breaker game setup scripts (_Thierry Draper_)
* f2e13cef: Implement an automated daily-processing emulator test harness (_Thierry Draper_)
* 3f6d2316: Improve status logging within the Record Breaker database scripts (_Thierry Draper_)
* c16d4440: Implement a Record Breaker game processing wrapper script (_Thierry Draper_)
* caceadcc: Simplify processing triggers to only include the sport and date, as season is implied (and determined) within the date (_Thierry Draper_)
* a777fcd5: Standardise triggering the Open Graph image creation with other processing arguments (_Thierry Draper_)

## Build v3.0.1480 - 1948ae20 - 2023-08-30 - Feature: Open Graph image creation
* 1948ae20: Switch to the new symlink-to-dir CI converter (_Thierry Draper_)
* 67ec9f73: Make use of the new Open Graph images on the game page meta tags (_Thierry Draper_)
* 3e28e547: Dynamically generate appropriate Open Graph images after each Record Breakers period (_Thierry Draper_)
* ccf149dd: Ensure the Record Breaker rendered pages have appropriate HTML meta descriptions set (_Thierry Draper_)

## Build v3.0.1476 - 0688a58c - 2023-08-07 - Decomm: MotoGP
* 0688a58c: Apply corresponding CI job changes (_Thierry Draper_)
* f09dec4c: Remove supporting processing logic (_Thierry Draper_)
* 51784330: Remove MotoGP from the automated user processing (_Thierry Draper_)
* 5ac0a07e: Remove MotoGP supporting database scripts and schema (_Thierry Draper_)
* eba92610: Remove MotoGP from the valid Record Breaker game regex (_Thierry Draper_)

## Build v3.0.1471 - feb4fe56 - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build v3.0.1471 - 3e0d5712 - 2023-08-02 - Feature: Team Summary Modal
* 3e0d5712: Ensure automated users cannot be updated via admin API tokens due to the missing Eloquent User data (_Thierry Draper_)
* 527605a8: Standardise loading of user info to hide the is-automated logic, and improve web-payload request unit tests to catch (_Thierry Draper_)
* 408e6667: Standardise use of the stress bar clipping from the individual selection to team and modal contexts (_Thierry Draper_)
* 803b98ab: Remove duplicate display name and user ID on team modal, where the display name is the user ID (_Thierry Draper_)
* 8affb368: Standardise the selection link span-name to span-anchor-name conversion into helper methods (_Thierry Draper_)
* 05230439: Add the selection and team modals to additional contexts (_Thierry Draper_)
* 86bd6250: Include team selection API endpoint (_Thierry Draper_)
* bd2f54dd: Render an individual team summary modal (_Thierry Draper_)
* cfe28985: Improve the selection summary modal class detector (_Thierry Draper_)

## Build v3.0.1462 - 145675a7 - 2023-07-22 - Feature: Individual Selection Modal
* 145675a7: Fix rendering Record Breaker leaderboard when multiple teams tied for the cut-off (_Thierry Draper_)
* 00c6dcf7: Fix first in-season Record Breaker entry position change calcs (_Thierry Draper_)
* cbaa2bad: Include individual selection API info endpoint (_Thierry Draper_)
* 9fd0feb1: Render an individual selection summary modal (_Thierry Draper_)
* 8ca3eb9b: Rename the Record Breaker Selection Modal as its Edit Modal, given we will be creating a Selection View Modal (_Thierry Draper_)
* 51fe5497: Re-jig the Record Breaker game link per-period URL structure (_Thierry Draper_)
* bd54fc78: Calculate selection usage in Record Breaker processing calcs (_Thierry Draper_)

## Build v3.0.1455 - b32f8c95 - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build v3.0.1455 - 90489da0 - 2023-07-13 - Feature: Automated Users
* 90489da0: Render automated users on the profile page (_Thierry Draper_)
* 9b344264: Implement an entry selection process for automated players (_Thierry Draper_)
* de27fc0e: Implement an entry joining process for automated players (_Thierry Draper_)
* 5b4c0907: Add an Artisan script for creating a bank of automated users (_Thierry Draper_)
* a5180d07: Define our automated user personality types (_Thierry Draper_)
* adb4f696: Introduce a separation of automated user records (_Thierry Draper_)

## Build v3.0.1449 - 8e0ea6ff - 2023-07-01 - Improvement: Entry Creation Email
* 8e0ea6ff: Include a small selection of available games in the email sent to users upon entry creation (_Thierry Draper_)
* a6b1783c: Re-write the Game::loadByStatus() method to be one that actually loads games by the specific statuses, not all games grouped by their status (_Thierry Draper_)

## Build v3.0.1447 - 2a4bea58 - 2023-06-28 - Feature: Record Breaker Auditing
* 2a4bea58: Make use of the new masked object output to redact sensitive details externally (_Thierry Draper_)
* 8b8d355f: Audit users adding, updating and removing Record Breaker game selections (_Thierry Draper_)
* 196a132d: Audit users joining and leaving Record Breaker games (_Thierry Draper_)
* 3822a9ac: Audit users creating and updating Record Breaker entries (_Thierry Draper_)

## Build v3.0.1443 - fab47fbc - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build v3.0.1443 - 53a82a1e - 2023-06-16 - Improvement: Flag when game entries closing
* 53a82a1e: Flag on the Record Breakers game page when it will be closing soon (_Thierry Draper_)
* e1d80122: Flag on the Record Breakers homepage when games are closing soon (_Thierry Draper_)

## Build v3.0.1441 - f57a7fba - 2023-06-15 - Feature: Record Breaker tweet processing
* f57a7fba: Re-factor the exemplar Record Breaker game config following the Twitter changes (_Thierry Draper_)
* 5b7889a4: Add the daily Tweet-processing logic (_Thierry Draper_)
* 43b1af3b: Remove the the fantasy-specific Twitter template database table in favour of the app-agnostic version in debear_common (_Thierry Draper_)

## Build v3.0.1438 - dd7d5b50 - 2023-06-04 - Hotfix: Profile Budget Scoring Schema
* dd7d5b50: Commit the correct version of the new Budget scoring profile table (_Thierry Draper_)

## Build v3.0.1437 - 7b8eb1ee - 2023-06-04 - Feature: Record Breaker User Profiles
* 7b8eb1ee: Make the database resync script sensitive to a prefix to allow for running in a non-default location (_Thierry Draper_)
* 5b7a73bf: Standardise value formatting of some game period summary API fields (_Thierry Draper_)
* 8e3e373b: Update the user profile API with Record Breaker entries (_Thierry Draper_)
* 23631e64: Update the user profile page with Record Breaker entries (_Thierry Draper_)
* 9b14d780: Tidy the user profile schema (_Thierry Draper_)
* 44a8fbc3: Calculate user profile entries during Record Breaker game processing (_Thierry Draper_)

## Build v3.0.1431 - 522e8ae3 - 2023-05-29 - Feature: Announcements
* 522e8ae3: Tidy the base Record Breaker database config with common table schema updates (_Thierry Draper_)
* cfd31e94: Render relevant Record Breaker game announcements on the main homepage (_Thierry Draper_)
* 35d708af: Render relevant Record Breaker game announcements within an individual game (_Thierry Draper_)
* 2c98f70e: Include relevant Record Breaker game announcements within the API (_Thierry Draper_)
* 0cf8ead5: Include a section field in the generic announcement schema for possible sub-game filtering (_Thierry Draper_)

## Build v3.0.1426 - 71c12cc3 - 2023-05-26 - Hotfix: General Record Breaker fixes
* 71c12cc3: Ensure motorsport games are date aware, not just day-of-week (_Thierry Draper_)
* e24cc13a: Correctly handle non-Major League games with their lack of opponent ratings (_Thierry Draper_)
* 5218403b: Correctly determine and render selections with no info attribute available (_Thierry Draper_)
* de4e96ad: Ensure current date definition is aware of the correct timezone for each game (_Thierry Draper_)
* 2facc273: Link to the correct JavaScript class for handling modal statuses (_Thierry Draper_)
* cf4f1259: Use the correct criteria for determining when an entry can load the selection modal assets (_Thierry Draper_)

## Build v3.0.1420 - dd848965 - 2023-05-26 - Hotfix: Postponed or Cancelled event handling
* dd848965: Generalise the Record Breaker selection event status info and checks (_Thierry Draper_)
* 333a674b: Visually show unavailable Record Breaker selections due to postponed or cancelled events (_Thierry Draper_)
* d6cd5534: Handle postponed / cancelled events within the Record Breaker APIs (_Thierry Draper_)
* c4898468: Explicitly ignore N/A selections in Record Breaker calculations (_Thierry Draper_)
* d07d0217: Factor in Postponed and Cancelled events in Record Breaker selection lists (_Thierry Draper_)

## Build v3.0.1415 - 68cd1f23 - 2023-05-21 - Feature: Standings Modal
* 68cd1f23: Render the position change within the Record Breakers API entry scores (_Thierry Draper_)
* 502b3a3c: Render the position change within the Record Breakers leaderboards (_Thierry Draper_)
* ac8e36a5: Calculate change in position for each Record Breakers entry during the processing stage (_Thierry Draper_)
* 7896f93c: Standardise the result and position columns between Record Breaker overall and current scores (_Thierry Draper_)
* 09c9aba9: Apply some fixes to the Record Breaker selection modal (_Thierry Draper_)
* 052b3d86: Add a Leaderboard modal on the Record Breakers game page (_Thierry Draper_)
* c46d0ee4: Format appropriately the final result of a selection on a Record Breakers entry in the per-selection API object, not just its score (_Thierry Draper_)
* 8eb816b0: Return the current and last periods of a Record Breakers game via an associative, not indexed, array (_Thierry Draper_)

## Build v3.0.1407 - 879b0b9c - 2023-05-14 - Improvement: Highcharts v11
* 879b0b9c: Switch Highcharts innvocation to the new v11 format (_Thierry Draper_)
* d5794e7e: Switch from Highcharts v10 to v11 (_Thierry Draper_)

## Build v3.0.1405 - e87f8781 - 2023-05-14 - Composer: May 2023
* (Auto-commits only)

## Build v3.0.1405 - 2cd54644 - 2023-05-10 - Improvement: Record Breakers API Review
* 2cd54644: Remove period summaries from the Record Breaker game periods list if not available (_Thierry Draper_)
* de4bbdc2: Fix rendering of Record Breakers game periods API should no positive or negative selection be made in that period (_Thierry Draper_)
* e651c8ac: Fix rendering of Record Breaker API scores over 1,000 (_Thierry Draper_)
* 441f89a2: Distinguish empty selection results from unavailable in the Record Breakers API (_Thierry Draper_)
* b88ef677: Hide the selection (preview) stats for historical game periods within the Record Breakers API entries selection endpoint (_Thierry Draper_)
* d95045df: Ensure the Record Breakers entry API includes information about their active games (_Thierry Draper_)
* b3e861f9: Localise the Record Breakers API leaderboards to the fields for the appropriate section (_Thierry Draper_)
* c63a97fa: Include the appropriate User ID in the Record Breaker API game leaderboard, but not individual entry (_Thierry Draper_)
* 3f828358: Remove the ambiguity between Record Breaker game state boundaries being Date or Date/Time (_Thierry Draper_)
* 9ff1052f: Tidy the use of Major League event time in the Record Breakers API (_Thierry Draper_)
* 7e395232: Add an internal tool for testing (primarily API) requests (_Thierry Draper_)
* 22983457: Fix the placement of the Record Breakers help API route (_Thierry Draper_)

## Build v3.0.1393 - 7eb2b433 - 2023-05-06 - SysAdmin: Document Game Configuration
* 7eb2b433: Document the database scripts required to create/update a Record Breakers game (_Thierry Draper_)

## Build v3.0.1392 - 8262f5fe - 2023-05-05 - Feature: Record Breakers Help
* 8262f5fe: Create a game-agnostic Help section API endpoint and use within Record Breakers (_Thierry Draper_)
* 5daba6f6: Create a game-agnostic Help section widget and use within Record Breakers (_Thierry Draper_)

## Build v3.0.1390 - 05f21728 - 2023-05-02 - SysAdmin: Deprecations
* 05f21728: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build v3.0.1389 - f2eb2c7f - 2023-05-02 - Feature: Stressometer
* f2eb2c7f: Ensure the stress score fields appear in the Record Breakers API (_Thierry Draper_)
* d03d2c08: Render the selection and entry stress score on the Record Breaker entry page (_Thierry Draper_)
* 30f169d6: Calculate a stress score for Record Breaker selections (_Thierry Draper_)

## Build v3.0.1386 - 102f0cc7 - 2023-04-15 - Composer: April 2023
* (Auto-commits only)

## Build v3.0.1386 - 770a1fe1 - 2023-03-16 - Feature: Record Breakers period summary
* 770a1fe1: Return performance stats in the Record Breakers game period API calls (_Thierry Draper_)
* 8063e687: Display some performance stats on the Record Breakers game page (_Thierry Draper_)
* 5fa0b9bc: Calculate some basic summary stats at the conclusion of a Record Breakers period (_Thierry Draper_)

## Build v3.0.1383 - 4ea0a8cd - 2023-03-11 - Hotfix: Record Breaker Progress Bars
* 4ea0a8cd: Fix the progress bar ordering on the individual game page (_Thierry Draper_)
* 2fd4ba8f: Hide the Current entry score when a team is eliminated from a Record Breakers game (_Thierry Draper_)

## Build v3.0.1381 - 57fe6690 - 2023-03-11 - Composer: March 2023
* (Auto-commits only)

## Build v3.0.1381 - 80669ae8 - 2023-03-10 - Improvement: Standardise Record Breaker data input method
* 80669ae8: Standardise data input method away from POST and in to JSON in all instances (_Thierry Draper_)

## Build v3.0.1380 - 48cc7479 - 2023-03-10 - Feature: Selection Reuse and Required flags
* 48cc7479: Treat missing Record Breaker selections as negative in games where selections are required (_Thierry Draper_)
* 9d14cd78: Prevent selections from being reused where Record Breaker games use that option (_Thierry Draper_)
* ba94d921: Fix NFL team scoring status calcs (_Thierry Draper_)
* 92a4c3e1: Introduce a Record Breaker modal status message timeout (_Thierry Draper_)

## Build v3.0.1376 - 79da63d9 - 2023-03-08 - Feature: Record Breaker scoring calcs
* 79da63d9: Calculate Record Breaker active leaders for each game on a given date (_Thierry Draper_)
* dc348c55: Calculate Record Breaker entry standings for each game on a given date (_Thierry Draper_)
* 16505ddf: Calculate Record Breaker entry scores for each game on a given date (_Thierry Draper_)
* bcc009ec: Fix the Record Breaker selection modal loading JavaScript logic (_Thierry Draper_)
* f1c43a7c: Normalise the Record Breaker selection score by removing from the entry selection table (_Thierry Draper_)
* cc685e91: Calculate Record Breaker individual selection scores for each game on a given date (_Thierry Draper_)

## Build v3.0.1370 - 529b9e52 - 2023-03-01 - Improvement: CSS Standards to Prettier
* 529b9e52: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* 4e1c866e: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build v3.0.1368 - 195b5b4c - 2023-02-28 - Feature: Record Breaker selection saving
* 195b5b4c: Render more informative Record Breaker selection action error messages (_Thierry Draper_)
* e1f66236: Record Breaker CRUD - selection saving (_Thierry Draper_)
* 42743a41: Record Breaker cRuD - selection removal (_Thierry Draper_)
* 54aed136: Better sub-sort Record Breaker selection stat ordering via rating/opp rating (_Thierry Draper_)
* 58c6c6cf: Standardise the name of the Twitter database table across all use-cases (_Thierry Draper_)
* e296d97e: Fall back to browser selection image optimisation over server-side manipulation (_Thierry Draper_)
* 8d25cc1e: Link the Record Breaker selection filters to back-end processing (_Thierry Draper_)
* 1685dc99: Record Breaker cRud - selection modal listing (_Thierry Draper_)

## Build v3.0.1360 - d238c09f - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build v3.0.1360 - ca4feb8c - 2023-02-12 - Feature: Record Breaker Selection APIs
* ca4feb8c: Extend Record Breaker selection getters to include their calculated stats (_Thierry Draper_)
* c014c1ef: Standardise the name of the GameSel controller with the EntrySel models they fundamentally represent (_Thierry Draper_)
* f7be25e9: Add API endpoints for Record Breaker game selection management (_Thierry Draper_)
* 1d975eec: Update the passing around of data in unit test requests following skeleton improvements (_Thierry Draper_)
* ce387baf: Add endpoints for Record Breaker game selection management (_Thierry Draper_)
* aacb335b: Move away from direct array references when loading / checking game entry (_Thierry Draper_)
* ebebcdc3: Tweak the mechanism by which the users entry is referenced when joining or leaving a game (_Thierry Draper_)
* c10a8ebe: Standardise the logic order of the Record Breaker unit tests (_Thierry Draper_)
* 5e1e55e4: Use the appropriate HTTP status code returned when a processing entity is locked (_Thierry Draper_)

## Build v3.0.1351 - f5cd8e3d - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build v3.0.1351 - 699474ba - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* 699474ba: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build v3.0.1350 - de0eee7e - 2023-01-05 - Feature: Record Breakers selection ratings
* de0eee7e: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* 1a155aec: Archive the setup scripts for the games no longer available (_Thierry Draper_)
* 4f1fbafc: Start linting the setup scipts during our CI (_Thierry Draper_)
* 55e21893: Improve the database resync script to aid debugging (_Thierry Draper_)
* 3d4c2dc0: Calculate Record Breaker selection ratings for each game on a given date (_Thierry Draper_)
* edaecc54: Add additional Record Breaker selection stats to AHL, NFL and NHL games (_Thierry Draper_)
* 9dbdbb79: Generalise the Record Breakers scoring definition (_Thierry Draper_)
* ce1377a6: Make the selection mapping table season-specific (_Thierry Draper_)

## Build v3.0.1342 - 31e3d5f7 - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build v3.0.1342 - 3e4c9163 - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build v3.0.1342 - 0f788750 - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build v3.0.1342 - 01a572e8 - 2022-09-18 - Composer: September 2022
* (Auto-commits only)

## Build v3.0.1342 - 685b498f - 2022-09-17 - Hotfix: Motorsport mugshot cropping
* 685b498f: Move the CDN argument processing in to this repo to allow for per-use-case image parsing rules (_Thierry Draper_)
* d3e71c24: Generalise the Major League / Motorsport test logic (_Thierry Draper_)

## Build v3.0.1340 - 87bd7c7c - 2022-09-14 - Feature: Records Breakers selection modal
* 87bd7c7c: Install wget as a CI environment dependency (_Thierry Draper_)
* fa7cda76: Make the Record Breakers selection modal stat list dynamic, defined per-game (_Thierry Draper_)
* 0d971ca4: Update Record Breaker game setup scripts with entry scores and selection tables (_Thierry Draper_)
* 849c30f1: Create an initial Record Breakers selection modal (_Thierry Draper_)
* b9d53fed: Fix the joining/leaving Record Breaker game status message (_Thierry Draper_)

## Build v3.0.1335 - 060234d7 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build v3.0.1335 - 66feb043 - 2022-08-09 - Feature: Record Breaker homepage progress
* 66feb043: Display game progress on the Record Breakers homepage (_Thierry Draper_)
* aed3c5fe: Tweak the general selection locking (by period time, not individual selection) to fit date/time combos (_Thierry Draper_)
* 7501bab6: Factor in historical changes to Major League team entity city/franchise changes (_Thierry Draper_)

## Build v3.0.1332 - 2b52a822 - 2022-08-01 - Hotfix: Record Breakers manifest
* 2b52a822: Add the missing Record Breakers manifest.json (_Thierry Draper_)

## Build v3.0.1331 - 6d3a5b33 - 2022-08-01 - Hotfix: API standardisation
* 6d3a5b33: Standardise the value types in the Record Breakers API (_Thierry Draper_)
* c8cd481b: Standardise the singularity of API controller filenames (_Thierry Draper_)

## Build v3.0.1329 - eeb5575e - 2022-07-30 - Improvement: Record Breakers single game view
* eeb5575e: Add strict Record Breakers date boundary testing, including confirming date_complete is exclusive (_Thierry Draper_)
* 6f5a657e: Re-work the game grouping and date management to be more predictable (_Thierry Draper_)
* b6617308: Load and display entity statuses in Record Breakers game view (_Thierry Draper_)
* aeec2ae7: Include a mechanism for storing fantasy-specific Motorsport selection statuses (_Thierry Draper_)
* d968d9e7: Remove Last/Current Record Breaker selections from logged in users who have not joined a game (_Thierry Draper_)
* 6ebd2312: Allow logged in users to join/leave a game from its main view (_Thierry Draper_)

## Build v3.0.1323 - c15a39d7 - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* c15a39d7: Standardise an API season value format between PHP 8.1 and 8.0 (_Thierry Draper_)
* 7c0095ae: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build v3.0.1321 - 4c576455 - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build v3.0.1321 - 1556f311 - 2022-07-16 - Feature: Record Breakers single game view
* 1556f311: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)
* f4edaf78: Add an API endpoint for loading the current period (generically) for a Record Breakers game (_Thierry Draper_)
* 67ff43aa: Add an API endpoint for listing Record Breaker game periods for a game (_Thierry Draper_)
* bd6425ab: Add an API endpoint for Record Breaker selections in a given period (_Thierry Draper_)
* 19ab38d5: Record Breakers endpoint for game entry current status getter (_Thierry Draper_)
* 2da7c354: Add Record Breaker API endpoints for game rules and leaderboards (_Thierry Draper_)
* 629c0517: Render a single game view for a Record Breakers game (_Thierry Draper_)
* de078171: Remove the SGP flag aliases from the meeting info, as it is a Sports-specific unique field not Fantasy (_Thierry Draper_)
* 3e265708: Clarify the Record Breaker game loading method, which is a specific subset, not general loader (_Thierry Draper_)
* 227359c5: Add schema and models for individual Record Breaker selections (_Thierry Draper_)
* 60eaf749: Better handle selection loading, when same entity requested over multiple periods (_Thierry Draper_)
* 144f9238: Convert the Record Breaker entry model from a compound key with single column to the semantically correct primary key column (_Thierry Draper_)
* bb9a425e: Add a fantasy game-agnostic loading method for player/team details (_Thierry Draper_)
* 61651da3: Move the defintion of game dates and periods from Record Breakers context to Common (_Thierry Draper_)
* 8994eb6c: Move the Major League team_id mapping from Record Breakers context to Common (_Thierry Draper_)
* 8b50c43e: Include a period ordering column in the Record Breaker dates, matching the pattern used in Motorsport (_Thierry Draper_)
* e7ce9c23: Switch our unit test data to match what is available in the Sports CI jobs (_Thierry Draper_)

## Build v3.0.1304 - 5d5a7fba - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build v3.0.1304 - d778edfb - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* d778edfb: Move handling of no existing Record Breakers record holder from game to template (_Thierry Draper_)
* 5887412f: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 711bf077: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* 9264a5e1: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* 11a9ac6f: Restore Blade linting using the new dependency (_Thierry Draper_)

## Build v3.0.1299 - 9594e43d - 2022-06-01 - Feature: Create Record Breakers entry when joining a game with no entry
* 9594e43d: Create a Record Breakers entry when a user joins a game with no existing entry (_Thierry Draper_)

## Build v3.0.1298 - 708683d3 - 2022-05-31 - Improvement: Policy checks in middleware
* 708683d3: Move controller policy checking to the middleware (_Thierry Draper_)

## Build v3.0.1297 - 24c0478b - 2022-05-30 - SysAdmin: Record Breaker game date granularity
* 24c0478b: Go to a finer granularity of Record Breaker game definition times (_Thierry Draper_)

## Build v3.0.1296 - 9f74b2d2 - 2022-05-27 - Improvement: Highcharts v10
* 9f74b2d2: Switch from HighCharts v9 to v10 (_Thierry Draper_)

## Build v3.0.1295 - 8b8a1419 - 2022-05-27 - Improvement: User entry loading
* 8b8a1419: Simplify how user entries are loaded, by moving away from a session-based approach (_Thierry Draper_)

## Build v3.0.1294 - 91fe8991 - 2022-05-27 - CI: PHP Code Coverage streamlining
* 91fe8991: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build v3.0.1293 - a53c1c18 - 2022-05-26 - CI: Report PHP Unit code coverage on master
* a53c1c18: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build v3.0.1292 - 43494b1a - 2022-05-22 - CI: Switch Code Coverage Driver
* 43494b1a: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)

## Build v3.0.1291 - ba9592e8 - 2022-05-22 - Feature: Record Breakers signup
* ba9592e8: Ensure games are listed on the homepage in an appropriate order (_Thierry Draper_)
* e7d7fa09: Standardise the mechansim for triggering unthrottled API unit tests (_Thierry Draper_)
* 0d442317: Add API endpoints for entry game management (_Thierry Draper_)
* 2bec4009: Allow users to join and leave an individual game (_Thierry Draper_)
* 2f841731: Add API endpoints for entry management (_Thierry Draper_)
* 3493d704: Allow users to modify their entry (_Thierry Draper_)
* 58c9f4c0: Allow users to create an entry (_Thierry Draper_)
* 1afebbfd: Give logged in, but unregistered, users a registration action on the Record Breakers homepage (_Thierry Draper_)
* ea396572: Give logged out users a signin/up action on the Record Breakers homepage (_Thierry Draper_)

## Build v3.0.1282 - 9f46e017 - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* 9f46e017: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* e00398f0: Refine our Unit Tests according to localci tweaks made to the skeleton (_Thierry Draper_)
* 332ad9e5: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)

## Build v3.0.1279 - d04c6633 - 2022-05-22 - Improvement: Laravel 9
* d04c6633: Update Unit Test expectations following the Laravel 9 upgrade (_Thierry Draper_)
* caf22012: Remove Blade linting that is no longer available following the Laravel 9 upgrade (_Thierry Draper_)

## Build v3.0.1277 - c6bf319f - 2022-05-21 - Feature: Motorsport Record Breakers config for 2022
* c6bf319f: Motorsport Record Breakers config for 2022 (_Thierry Draper_)

## Build v3.0.1276 - c0ec1ab0 - 2022-05-21 - SysAdmin: PHP 8
* c0ec1ab0: Auto: Composer Update for April 2022 (_Thierry Draper_)
* c15ad26d: Auto: Composer Update for March 2022 (_Thierry Draper_)
* 7e2c53d2: Auto: Composer Update for February 2022 (_Thierry Draper_)
* 661d2cce: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* b08ad0dc: Auto: Composer Update for January 2022 (_Thierry Draper_)
* 56ed656f: Auto: Composer Update for December 2021 (_Thierry Draper_)
* 03db0b9c: Add a new CI step to lint Blade templates (_Thierry Draper_)
* ba07786f: Move away from our Arrays::hasIndex wrapper (_Thierry Draper_)
* bfeda489: Implement PHP 8s new union param/return types (_Thierry Draper_)
* 9e5d08d8: Include a repo-specific version of the Phan PHP Standards CI job (_Thierry Draper_)
* e92682e6: Apply the final fixes suggested by Phan (_Thierry Draper_)
* bf3a4df4: Apply several type-hinting related fixes (_Thierry Draper_)
* 03840089: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)

## Build v3.0.1268 - cacba86e - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build v3.0.1268 - 64e12533 - 2021-11-12 - CI: CSS Property Standards
* 64e12533: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* 5f89992d: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)

## Build v3.0.1266 - 0669aca0 - 2021-11-10 - Feature: User Profile API
* 0669aca0: Create a User Profile API endpoint (_Thierry Draper_)

## Build v3.0.1265 - 7a222ea6 - 2021-11-10 - Feature: Record Breakers homepage
* 7a222ea6: Setup the API, with the game list as the first endpoint (_Thierry Draper_)
* 6cba8722: Remove a recently deprecated stylelint rule (_Thierry Draper_)
* 5dfba451: Build the Record Breakers homepage as a list of available/active games (_Thierry Draper_)
* d6fe7b05: Restore game database setup, including re-jigged game config from initial dev (_Thierry Draper_)
* 1c17f29f: Remove the legacy game code, which we don't need here anymore (_Thierry Draper_)

## Build v3.0.1260 - 430338b7 - 2021-10-19 - Feature: User Profile
* 430338b7: Re-implement the player profile page (_Thierry Draper_)

## Build v3.0.1259 - a14530b9 - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build v3.0.1259 - 62124cee - 2021-09-18 - Composer: September 2021
* (Auto-commits only)

## Build v3.0.1259 - 03638093 - 2021-09-16 - Feature: Homepage Games List
* 03638093: Update the sitemap with the list of available games (_Thierry Draper_)
* 70d8ff66: Re-work the homepage with our initially planned games factored in (_Thierry Draper_)
* ee2b2817: Remove legacy application imagery (_Thierry Draper_)
* 1422b913: Add the remaining elements to the CI jobs (_Thierry Draper_)
* ca4111d3: Correct a linting fail in legacy app (_Thierry Draper_)

## Build v3.0.1254 - 13cb045f - 2021-08-13 - Composer: August 2021
* (Auto-commits only)

## Build v3.0.1254 - 736f31b8 - 2021-08-13 - Hotfix: PHPUnit XML .env spec
* 736f31b8: Add missing .env values to the PHPUnit test XML spec (_Thierry Draper_)

## Build v3.0.1253 - 44e32a93 - 2021-07-27 - Feature: Sitemap URL tester
* 44e32a93: Sitemap parser and processor for URL testing (_Thierry Draper_)

## Build v3.0.1252 - 3bdc3e2e - 2021-07-27 - SysAdmin: Rebase database schema
* 3bdc3e2e: Strip out legacy database details from the new master base (_Thierry Draper_)

## Build v3.0.1251 - e8a78e98 - 2021-07-17 - Composer: July 2021
* (Auto-commits only)

## Build v3.0.1251 - a2da86d1 - 2021-06-21 - Composer: June 2021
* (Auto-commits only)

## Build v3.0.1251 - c5640102 - 2021-05-15 - Composer: May 2021
* (Auto-commits only)

## Build v3.0.1251 - d580e162 - 2021-04-30 - CI: Pipeline Tweaks
* d580e162: Make use of the CI's needs: option to streamline job and stage links (_Thierry Draper_)

## Build v3.0.1250 - bd937deb - 2021-04-17 - Composer: April 2021
* (Auto-commits only)

## Build v3.0.1250 - 574f3c3c - 2021-03-13 - Composer: March 2021
* (Auto-commits only)

## Build v3.0.1250 - ab51f52b - 2021-02-13 - Composer: February 2021
* (Auto-commits only)

## Build v3.0.1250 - 20e046fc - 2021-01-16 - Composer: January 2021
* (Auto-commits only)

## Build v3.0.1250 - 91be399c - 2021-01-11 - Improvement: Password Policy Skeleton Changes
* 91be399c: Rely on an appropriate version of guzzlehttp/guzzle after adding the password policy (_Thierry Draper_)
* 2bcf9e3f: Start using the new @config Blade directive for rending config values (_Thierry Draper_)

## Build v3.0.1248 - 13cc9af6 - 2021-01-03 - CI: Only deploy on master branch
* 13cc9af6: Only deploy during CI/AD of the master branch (_Thierry Draper_)

## Build v3.0.1247 - 1a408935 - 2020-12-20 - Composer: December 2020
* (Auto-commits only)

## Build v3.0.1247 - bb57957a - 2020-12-05 - Improvement: Move to Xdebug 3
* bb57957a: Fixes for the switch from Xdebug 2 to 3 (_Thierry Draper_)

## Build v3.0.1246 - 101ca9de - 2020-11-14 - Composer: November 2020
* (Auto-commits only)

## Build v3.0.1246 - 3b8194e3 - 2020-10-09 - Composer: Upgrade to Laravel 8
* 3b8194e3: Update our CI rules following the Laravel 8 (+deps) upgrade (_Thierry Draper_)
* 08e402f4: Update dependencies from upgrading from Laravel 7 to 8 (_Thierry Draper_)

## Build v3.0.1244 - a87d88e2 - 2020-09-26 - Improvement: Migrations to Schema
* a87d88e2: Switch from Migrations to Schemas updated via schema-sync (_Thierry Draper_)

## Build v3.0.1243 - 64056165 - 2020-09-19 - Composer: September 2020
* (Auto-commits only)

## Build v3.0.1243 - 62a2c832 - 2020-09-18 - CI: Merge PHP Standards Jobs
* 62a2c832: Merge the PHP Mess Detector and Standards tests into a single job (_Thierry Draper_)

## Build v3.0.1242 - e4c52df5 - 2020-08-15 - Composer: August 2020
* (Auto-commits only)

## Build v3.0.1242 - 0209f14b - 2020-07-12 - Composer: July 2020
* (Auto-commits only)

## Build v3.0.1242 - fe5b439d - 2020-06-28 - Hotfix: Return a 501 Not Implemented
* fe5b439d: Return a 501 (Not Implemented), not a 503 (Service Unavailable), until re-built (_Thierry Draper_)

## Build v3.0.1241 - aa0d44b2 - 2020-06-17 - Hotfix: PHPUnit Test Locale
* aa0d44b2: Fix PHPUnit tests on CI as the locale does not match dev/prod environments (_Thierry Draper_)

## Build v3.0.1240 - dc224592 - 2020-06-14 - Composer: June 2020
* (Auto-commits only)

## Build v3.0.1240 - 5ad2f29e - 2020-06-13 - SysAdmin: Reduce Release Overlap
* 5ad2f29e: Reduce the overlap time between releases when performing the release (_Thierry Draper_)

## Build v3.0.1239 - 4a280390 - 2020-06-06 - Improvement: PSR-4-like Standards Check
* 4a280390: Add PSR-4 like standards checks, given recent class and file name mis-matches (_Thierry Draper_)

## Build v3.0.1238 - 844f922a - 2020-06-02 - CI: Local PHPUnit Dev Setup
* 844f922a: Add scripts to setup a local dev environment for working on PHPUnit tests (_Thierry Draper_)

## Build v3.0.1237 - 2750fdb5 - 2020-05-18 - Composer: May 2020
* (Auto-commits only)

## Build v3.0.1237 - 7cbe8423 - 2020-04-21 - Composer: April 2020
* (Auto-commits only)

## Build v3.0.1237 - b9b5d85a - 2020-04-07 - Improvement: Dynamic PHPUnit Setup Components
* b9b5d85a: Standarise and split the PHPUnit setup script into dynamic components (_Thierry Draper_)

## Build v3.0.1236 - 9fe42764 - 2020-04-06 - Hotfix: PHPUnit Error Handling
* 9fe42764: Ensure the new PHPUnit run script errors on failure at the appropriate stage (_Thierry Draper_)

## Build v3.0.1235 - 0bfc0027 - 2020-04-04 - Composer: Replacing PHP Linter
* 0bfc0027: Replace a deprecated PHP Linting package with its replacement (_Thierry Draper_)

## Build v3.0.1234 - e471de1c - 2020-04-04 - Improvement: PHPUnit Efficiency
* e471de1c: Split the PHPUnit run in to a more efficient process (_Thierry Draper_)

## Build v3.0.1233 - 5fc81110 - 2020-03-14 - Composer: March 2020
* (Auto-commits only)

## Build v3.0.1233 - 4ebc9dd5 - 2020-02-14 - Composer: February 2020
* (Auto-commits only)

## Build v3.0.1233 - f5e4f58c - 2020-02-10 - Improvement: Footer Links
* f5e4f58c: Include a DeBearSports social media link in the page footer (_Thierry Draper_)

## Build v3.0.1232 - 7ca0ba0b - 2020-01-31 - SysAdmin: .gitignore for env config
* 7ca0ba0b: Updated gitignore to hide a local env config file (_Thierry Draper_)

## Build v3.0.1231 - 5a694834 - 2020-01-12 - Composer: January 2020
* (Auto-commits only)

## Build v3.0.1231 - 73597117 - 2019-12-31 - SysAdmin: Prune SCHEMA server-sync steps
* 73597117: Prune the code/schema server-sync steps we've new mechanisms for (_Thierry Draper_)

## Build v3.0.1230 - 0ae7dae1 - 2019-12-26 - Hotfix: PHPUnit CI Job Shell Errors
* 0ae7dae1: Fix tput/clear errors during the PHPUnit CI job (_Thierry Draper_)

## Build v3.0.1229 - 8ffb8d78 - 2019-12-15 - Composer: December 2019
* (Auto-commits only)

## Build v3.0.1229 - 5fdaed06 - 2019-11-19 - CI: PHP Comments Standards
* 5fdaed06: Fall-out from the new PHP comments standards being enforced (_Thierry Draper_)
* 9a712e9e: Extend our PSR-12 standards check to include the format of comments (_Thierry Draper_)

## Build v3.0.1227 - f8fcd609 - 2019-11-16 - Composer: November 2019
* (Auto-commits only)

## Build v3.0.1227 - 97f486f7 - 2019-11-14 - CI: YAML Fixes and Improvements
* 97f486f7: CI YAML fixes and improvements, such as moving the PHP/MySQL versions used to GitLab (_Thierry Draper_)

## Build v3.0.1226 - c54de4d9 - 2019-10-23 - Composer: October 2019
* c54de4d9: PSR-12 whitespace fixes (_Thierry Draper_)
* 5054db82: Auto: Composer Update for October 2019 (_Thierry Draper_)

## Build v3.0.1225 - 331942d3 - 2019-10-22 - Improvement: CSS Linting
* 331942d3: CSS tweaks from new linting and standards checks (_Thierry Draper_)
* 406c14de: Switch the CSS Linting test to use stylelint, and split in to both linting and standards (_Thierry Draper_)

## Build v3.0.1223 - f56992af - 2019-09-25 - Composer: September 2019
* f56992af: PHPUnit CI job needed further setup for MySQL testing (_Thierry Draper_)
* 68451304: PHPUnit tests now need a seeded skeleton database (_Thierry Draper_)
* 99a699c6: Auto: Composer Update for September 2019 (_Thierry Draper_)

## Build v3.0.1221 - b272e648 - 2019-09-02 - CI: Fix 'Clean' Local Runs
* b272e648: Fix the 'clean' running of our CI locally (_Thierry Draper_)

## Build v3.0.1220 - 3f92d508 - 2019-08-27 - Composer: August 2019
* 3f92d508: PHPUnit tests now need the MySQL environment vars set (which were not previously set as not required in the repo yet) (_Thierry Draper_)
* 6622ceda: Auto: Composer Update for August 2019 (_Thierry Draper_)

## Build v3.0.1219 - abfdf59e - 2019-08-09 - CD: Move Server Details to Env Vars
* abfdf59e: Move the deploy server access details to environment variables (_Thierry Draper_)

## Build v3.0.1218 - c64fc2ca - 2019-07-21 - Composer: July 2019
* (Auto-commits only)

## Build v3.0.1218 - 83d3285e - 2019-07-19 - SysAdmin: Update Scripts Path
* 83d3285e: Officially move all references from /git to /var/www/debear (_Thierry Draper_)

## Build v3.0.1217 - 119b25aa - 2019-07-17 - Feature: Monitor PHP Code Coverage
* 119b25aa: Enable PHP code coverage report during CI (_Thierry Draper_)

## Build v3.0.1216 - bc86f361 - 2019-07-10 - Hotfix: Skip Deploy in LocalCI
* bc86f361: The new deploy stage should not be included in our localci run script (which also requires the script: rule to be last) (_Thierry Draper_)

## Build v3.0.1215 - 84802ea8 - 2019-07-05 - Feature: Enable Continuous Delivery
* 84802ea8: Envoy script for implementing our Continuous Delivery (_Thierry Draper_)

## Build v3.0.1214 - f78670c8 - 2019-06-18 - CI: PHPUnit vendor simplification
* f78670c8: Pre-load the test setup files, standardised with the other repos (_Thierry Draper_)
* 57b5cd1f: May need to create the base CPAN CI dir (_Thierry Draper_)
* 104d1e27: Fix, and where appropriate merge, vendor folder creation within the CI (_Thierry Draper_)
* 9519ffe5: Installing the skeleton for CI is a simpler process (_Thierry Draper_)
* 2592963f: Ensure composer has a vendor folder to be installed into (_Thierry Draper_)
* caad97ed: Propagate the CI_FAUX flag when running CI tests locally, but from clean (_Thierry Draper_)
* f6879c6e: Auto: Composer Update for June 2019 (_Thierry Draper_)

## Build v3.0.1208 - 81ea7a28 - 2019-06-09 - SysAdmin: Sprites to CDN
* 81ea7a28: Move the sprites to the CDN (_Thierry Draper_)

## Build v3.0.1207 - fbe8d386 - 2019-06-08 - CI: Handle Perl Upgrades
* fbe8d386: Handle changing versions of perl in the CI (_Thierry Draper_)

## Build v3.0.1206 - 4873e52e - 2019-06-07 - CI: Data Download Errors
* 4873e52e: When downloading test CI data return an error if the download fails, rather than assume it was a 204/skip (_Thierry Draper_)

## Build v3.0.1205 - 77b03d07 - 2019-05-19 - Composer: May 2019 updates
* 77b03d07: rsync management of PHPUnit test setup fix (_Thierry Draper_)
* 9aab2bfb: Auto: Composer Update for May 2019 (_Thierry Draper_)

## Build v3.0.1204 - 5457151f - 2019-05-02 - Migrations: Merge w/Stored Procedures
* 5457151f: Another attempt at getting the correct Sports location (_Thierry Draper_)
* 470fedb8: Missed some new database location fixes in the CI-version of the lint checks (_Thierry Draper_)
* b33d5a8b: Re-organise the database migrations layouts to keep all sub-grouped migrations/stored procs together (_Thierry Draper_)

## Build v3.0.1201 - bc7fb9d3 - 2019-04-22 - CI: Testing Migrations
* bc7fb9d3: Include running the database migrations properly as part of the MySQL Linting CI job (plus testing their rollbacks) (_Thierry Draper_)

## Build v3.0.1200 - 3ee0fc4e - 2019-04-16 - Composer: April 2019 updates
* 3ee0fc4e: Some local 'clean' CI test fixes and code optimisations (_Thierry Draper_)
* 5e828538: Tweak the way we install composer so it is available in later CI jobs (_Thierry Draper_)
* 77fa0a32: When cloning another repo during CI, ensure its vendor folder is up to date (_Thierry Draper_)
* 5a30b4b6: Minor MySQL lint debugging fixes (_Thierry Draper_)
* eea41853: Auto: Composer Update for April 2019 (_Thierry Draper_)

## Build v3.0.1196 - 5e5c22aa - 2019-04-09 - CI: Include migrations
* 5e5c22aa: Fixed the git clone's path-to-skeleton (_Thierry Draper_)
* 7d9220d3: Automatically fix the logs when cloning the skeleton within CI (_Thierry Draper_)
* 19521608: Correctly link to the Sports site's migrations (_Thierry Draper_)
* a0af1662: Re-jig how we run Laravel's migrations script should we sub-filter our migrations (_Thierry Draper_)

## Build v3.0.1192 - 805637a0 - 2019-03-29 - CI: Exclude Tags
* 805637a0: Prevent CI when pushing tags (_Thierry Draper_)

## Build v3.0.1191 - 78301496 - 2019-03-26 - CI: Re-grouped Git remote
* 78301496: Reflect re-grouping of the git remotes in our CI scripts (_Thierry Draper_)

## Build v3.0.1190 - 117ca311 - 2019-03-22 - CI: MySQL Linter
* 117ca311: Fix the MySQL lint preperation script to access the correct Stored Proc dir (_Thierry Draper_)
* 3e8bb984: Creating a MySQL linter (_Thierry Draper_)
* 89f55f02: Copy across the appropriate stored procedures from legacy (_Thierry Draper_)

## Build v3.0.1187 - 2cff5545 - 2019-03-16 - Hotfix: Post Launch
* 2cff5545: Auto: Composer Update, including upgrading PHPUnit from 7.5 to 8.0 (_Thierry Draper_)
* f95d023f: Start using a singular composer cache to try and cut down on CI pipeline build time (_Thierry Draper_)
* fb542d9a: Revise the way test databases are created, and include some post-test cleanup of them (_Thierry Draper_)
* 1ec20524: Fixes from running the Perl linter (_Thierry Draper_)
* 762c057e: CI refinements to the Composer vendor caching rules and include a new Perl-linting process (_Thierry Draper_)
* 50c44039: Updated version number (not sure why it wasn't commited with the build?) (_Thierry Draper_)

## Build v3.0.1182 - 028756a1 - 2019-02-28 - Launching Laravel - [![Version: 3.0](https://img.shields.io/badge/Version-3.0-brightgreen.svg)](https://gitlab.com/debear/fantasy/tree/v3.0)
* 028756a1: Merge branch 'laravel' (_Thierry Draper_)
* f06221bf: Fixed the CI, which shouldn't do a PHP standards check on the Blade templates (_Thierry Draper_)
* 6f244dd9: Git tidy pre merge of Laravel branch (_Thierry Draper_)
* bf9eb38e: An initial holding page, explaining lack of updates... accessible on all URLs for now (_Thierry Draper_)
* 6ea61d45: Update the .gitignore file to handle the integration data, as we previously did manually in repo-admin (_Thierry Draper_)
* c7e77d32: Merge branch 'master' into laravel (_Thierry Draper_)
* f113b56f: Remove database backups from the repo, given the bloat (_Thierry Draper_)
* 10f4510d: Make the unit tests compatible with some skeleton filesystem tweaks (_Thierry Draper_)
* 7674d0d2: Test script tweaks to attempt repo-name parity between repo and skeleton (_Thierry Draper_)
* 0b822b1b: Root-level merged folder isn't actually required (_Thierry Draper_)
* b8f88447: Add symlinks required from live (_Thierry Draper_)
* 8a6333cb: Skeleton branch now running on the master branch (_Thierry Draper_)
* 37136d72: Make the VERSION info part of the repo (_Thierry Draper_)
* d0324bd1: Copy across the version info (_Thierry Draper_)
* 7131c067: Minor CI tidying (_Thierry Draper_)
* cfeca51c: The old db folder is legacy, so preserve for now, but in a folder we'll later nuke (_Thierry Draper_)
* 18940998: Tidy missing test script (_Thierry Draper_)
* a89aa60f: Copy across latest test database scripts (_Thierry Draper_)
* 45712d22: Regular Composer update (_Thierry Draper_)
* fc38c2b4: Minor CI tweaks copied from "fuller" WWW changes (_Thierry Draper_)
* 6dc5f814: Minor permissions fix on the PHPUnit test setup (_Thierry Draper_)
* eb388ce8: Implement a "feature" PHPUnit test, testing access to the homepage (_Thierry Draper_)
* 82909058: Refine the way sub-site routes are loaded (_Thierry Draper_)
* 65eb7881: CI composer fix to only install missing/out-of-date packages rather than update outside of composer.lock context (_Thierry Draper_)
* bdab7ccf: Fixes from the new JS standards test (_Thierry Draper_)
* b4a4c13c: Include extended JS standards checking in the CI (_Thierry Draper_)
* 94d7b4dd: Script to run the CI tests locally (and accessing the composer libs directly, rather than through a git-runner) (_Thierry Draper_)
* ea4c84b5: Add PSR and Pipeline Status flags to the README (_Thierry Draper_)
* 76efc349: Update the CONTRIBUTING.md file following confirmation of PSR use (_Thierry Draper_)
* d9a228f8: Add PSR-12 compliance testing to our CI (_Thierry Draper_)
* 2dc63575: Minor PSR tidying following a phpcs run (_Thierry Draper_)
* 347e46ac: JS linting fixes using phpcbf (_Thierry Draper_)
* 5b3c2da7: Add some CSS and JS linting to our CI (_Thierry Draper_)
* e40d2a4a: PHPMD definition tweaks (_Thierry Draper_)
* 37a8f21d: Switch PHP we are testing against from 7.3 to 7.2 (_Thierry Draper_)
* 5c4c9448: Initial project CI (_Thierry Draper_)
* 66e04400: Minor email logo location re-jig (_Thierry Draper_)
* 68348425: Minor LICENSE file tidying (_Thierry Draper_)
* d46cc17d: Tweaked iconset CSS naming (_Thierry Draper_)
* 4032b803: Remove dead symlink in scripts folder that will need to be reincorporated when upgraded to new framework (_Thierry Draper_)
* 2fd3da81: Fix symlinks to the logos that are located in the Sports repo (_Thierry Draper_)
* 702b24fb: Adding initial version of a .gitignore (_Thierry Draper_)
* 4cc8ae2e: Copy the service worked from the legacy site (_Thierry Draper_)
* a3948be1: Base resources and config from www setup work (_Thierry Draper_)
* 5f40e00f: Proof-of-concept route (_Thierry Draper_)
* 2aa7d156: Start of process of switching from Homebrew skeleton to Laravel (_Thierry Draper_)
* 8e6f94d6: Merge branch 'master' into records (_Thierry Draper_)
* 974882b9: Removal of the 'hosted dev' environment on production (_Thierry Draper_)
* 5e0cfe06: Initial CHANGELOG file (_Thierry Draper_)
* fc379b56: Repo overview files (_Thierry Draper_)
* a2a5cb98: PHP 7 removal of the 'split' function (_Thierry Draper_)
* b1a35040: Move database password to an external non-sourced file (_Thierry Draper_)
* 861a9295: Merge branch 'master' into records (_Thierry Draper_)
* 59bf17f1: Upgrade Highcharts to v6.1 (_Thierry Draper_)
* fd0a42e7: Merge branch 'master' into records (_Thierry Draper_)
* bfbb8388: Basic Progessive Web App service worker (_Thierry Draper_)
* 833bd714: Menu bar colouring for Chrome on mobile (_Thierry Draper_)
* 5c51863d: Tweak NFL SalCap projection downloads to not over-write pre-season projections at the end of the season (_Thierry Draper_)
* 0211ebce: NFL SalCap config for 2018, including some fixes to calcs identified during the 2017 season (2pt conversions and Return TDs for players) (_Thierry Draper_)
* 9646914d: SalCap scoring calc fix for some stats that refer to implicit link info rather than the actual link ID (_Thierry Draper_)
* 17af9d57: Display the Record Breakers selection's result in place of the lock time when processing has been completed (_Thierry Draper_)
* f3d0bac2: Move the Record Breakers selection saving status bar to the game box (_Thierry Draper_)
* 371d6912: Record Breakers team selection history for a game and modal (_Thierry Draper_)
* a69ef1e8: Record Breaker image fallback for Major League players (_Thierry Draper_)
* 1aff8f70: "End" Record Breakers games early when selection loses without a restart option (_Thierry Draper_)
* b5ccb203: Record Breakers sporting leader fix for those "total streak" games (_Thierry Draper_)
* 6ed458a0: Flag sporting leader for each Record Breakers game (_Thierry Draper_)
* 36c32773: Record Breakers test data generation mechanism, including removing the "grouped dates" concept that we'll now use across all games, even daily (_Thierry Draper_)
* 4bea3822: Record Breaker calculation fixes for MLB's double headers (_Thierry Draper_)
* 42ee1b8f: Updated MLB team logos for 2018 (_Thierry Draper_)
* 01b76ed1: Ensure record breaker eliminated teams can no longer make selections (_Thierry Draper_)
* 5170739d: Determine current sporting leaders for each Record Breaker games (_Thierry Draper_)
* 65c1c590: Calculate team PBs and records as part of the Record Breaker processing (_Thierry Draper_)
* c308ffa6: Flag to the user when a game requires a selection (_Thierry Draper_)
* 38551a52: Record Breakers fix to stop locked selections from being selected! (_Thierry Draper_)
* 1db4f36f: Record Breakers selection scoring and team standings calcs (_Thierry Draper_)
* 0369c54e: Merge branch 'master' into records (_Thierry Draper_)
* 819dd483: Mobile homepage template deadline box fix for when no deadline required (_Thierry Draper_)
* 8c8e28d5: Minor JS error fix in SalCap schedule lists (_Thierry Draper_)
* eb66d8ca: Enable Daily Tunnel mode in SalCap processing (_Thierry Draper_)
* 395e8843: Image compression (_Thierry Draper_)
* 88979096: Merge branch 'master' into records (_Thierry Draper_)
* 6a079286: Image compression (_Thierry Draper_)
* 26f592e3: Handle end-of-season date management (_Thierry Draper_)
* 3d71d0a6: Validate Record Breaker date for ungrouped games to select the next available dat (e.g., all-star breaks) (_Thierry Draper_)
* 61e3d7c2: Flag Record Breaker games as completed if no takers by the time the joining period ends (_Thierry Draper_)
* 044b2861: Record Breaker selection saving (_Thierry Draper_)
* f0ee1a05: Flag MLB doubleheaders in record breakers (_Thierry Draper_)
* 3a78d409: Record Breaker rules as part of new joining popup (and associated leave) and linked within active games (_Thierry Draper_)
* 68c5c077: Record Breaker selection modal displaying the available options, as well as summary info for each option (_Thierry Draper_)
* 3326720d: Flag that the Record Breakers homepage template isn't index.php (affecting logo) (_Thierry Draper_)
* 63f4a94b: Major League record breakers roster date fixes, as we could be checking rosters on the (historical) date of the selection not the latest date (_Thierry Draper_)
* eb3c678a: Merge branch 'master' into records (_Thierry Draper_)
* 8403066c: Logo changes to reflect the new F1 logo (_Thierry Draper_)
* 0958d9cd: Enable a mobile version of the site (_Thierry Draper_)
* 50d7e5d5: Minor SalCap end-of-season results display tweaks (_Thierry Draper_)
* 9e557ff8: Prevent processing of inactive sub-sites (_Thierry Draper_)
* 0e5fe4e5: Fix JavaScript bug handler class name error in ad hoc raises (_Thierry Draper_)
* 6a51dda7: Merge branch 'master' into records (_Thierry Draper_)
* a5a5167f: Fix JavaScript bug handler class name error in ad hoc raises (_Thierry Draper_)
* 6fd45ae7: Internal NFL projection parsing tweak to allow optional third argument to limit number of weeks to be processed (_Thierry Draper_)
* cde269ac: Merge branch 'master' into records (_Thierry Draper_)
* 2d2e0972: JavaScript bug handling (_Thierry Draper_)
* 71c91853: JavaScript bug handling (_Thierry Draper_)
* 73495c14: Merge branch 'master' into records (_Thierry Draper_)
* af410b22: JSON display fixes, using the proper functions rather than manually building the text (_Thierry Draper_)
* 01b00071: Merge branch 'master' into records (_Thierry Draper_)
* 059bdf1c: Social Media class re-jig (_Thierry Draper_)
* 177d9637: Social Media class re-jig (_Thierry Draper_)
* befd46e6: Post-launch fixes (_Thierry Draper_)
* 03e2e7f8: Merge branch 'master' into records (_Thierry Draper_)
* 32e2352d: Renamed entry file for the skeleton (_Thierry Draper_)
* 138f0314: Renamed entry file for the skeleton (_Thierry Draper_)
* fcb07890: Remove unnecessary mod_rewrite rules from the main html .htaccess file (_Thierry Draper_)
* 327088fd: Removal of the identify_css_rules function, plus some curly brace pruning (_Thierry Draper_)
* 4192cd7e: CSP 'style-src' compliance (_Thierry Draper_)
* 02de4926: Merge branch 'master' into records (_Thierry Draper_)
* 5727cd3b: CSP 'style-src' compliance (_Thierry Draper_)
* 0e5535d3: Merge branch 'master' into records (_Thierry Draper_)
* 6d67875a: CSP 'script-src' compliance (_Thierry Draper_)
* 6d6d72d7: CSP 'image-src' compliance (_Thierry Draper_)
* 8d0cf142: WIP Record Breaker selections (_Thierry Draper_)
* 602f1e24: Merge branch 'master' into records (_Thierry Draper_)
* 748e2bfc: CSP 'script-src' compliance (_Thierry Draper_)
* 361f7f80: Changes to script tag declarations for CSP compliance (_Thierry Draper_)
* bcc90f8e: Merge branch 'master' into records (_Thierry Draper_)
* 84413d1b: Changes to script tag declarations for CSP compliance (_Thierry Draper_)
* 2c2e207e: Record Breaker individual team selection logic within the JSON bundle passed to the selection modal (_Thierry Draper_)
* 969e4e35: Record Breaker dated selection getters for Major League teams (_Thierry Draper_)
* 9b4ef2c0: NFL SalCap eligibility fix, where players now require 3 appearances in alternate position for secondary eligibility (_Thierry Draper_)
* c5a7f142: Minor Record Breakers filesystem re-jig, as the homepage is actually the team page and should be reflected in filenames (_Thierry Draper_)
* da61c66c: NFL SalCap mobile site error fix, where the icon was being displayed as the error message if random error occurred (_Thierry Draper_)
* b6c860d8: Fix the NFL SalCap projection calcs that were incorrectly calculating for players on IR (as identified by Totals col) (_Thierry Draper_)
* d9316fcd: Better fix to the NFL SalCap zero GP problem, as previous fix broke selection saving (_Thierry Draper_)
* e4da96f1: Add the Major League image processing args to the Record Breakers no selection image (_Thierry Draper_)
* 5e7fc8bd: In Record Breaker sports objects, move the Sports DBH reference into an instance variable to remove the need for constantly using "global $_SPORTS_DBH" (_Thierry Draper_)
* 2210f3b7: Player team and opponent info for Major League games (including date fudge for dev...) (_Thierry Draper_)
* 0e10ea4b: Record Breakers sports class over-haul as part of adding team games to the list, plus imagery additions to better control difference between Motorsport mugshots (130x130), Major League mugshot (100x150) and Major League team logos (128x128) that all need to fit in to the same 100x130 image (_Thierry Draper_)
* 1af1b60c: Minor non-stadard port fix for related domains (_Thierry Draper_)
* 6bccd684: NFL SalCap mobile team edit selection listing fix, where Cost/Name sorted lists do not appear correctly (_Thierry Draper_)
* f55d7893: NFL SalCap fix to selection stats, in particular around average stats for selections with zero GP (_Thierry Draper_)
* 6aa09985: NFL SalCap re-valuation boundary check fix (_Thierry Draper_)
* 1e520126: NFL SalCap Tweet wording fix (_Thierry Draper_)
* ca4cb5e7: NFL SalCap sports data checker to prevent processing if there was a sports error (though written sport agnostically) (_Thierry Draper_)
* 2225feab: Initial WIP to record breaker selection display (_Thierry Draper_)
* d3604a66: Record Breakers homepage date processing and link to manage the team (_Thierry Draper_)
* 9205d1b2: Minor addition of border-radius to Record Breaker boxes (_Thierry Draper_)
* 1805f305: Record Breakers game join/leave (plus tweak to make all JSON returned objects the correct mime type) (_Thierry Draper_)
* f6d1d632: Initial Record Breakers homepage, with several core components still missing (_Thierry Draper_)
* 56480a7d: Input button CSS modernisation (_Thierry Draper_)
* c03c3ac2: Record Breakers team editing page fixes (_Thierry Draper_)
* e1ddd419: Record Breakers Site Manager homepage listing the games and some status info (_Thierry Draper_)
* d433ee19: Record Breaker setup fixes, including updating the MLB team winning streak to be the Giants 26 game streak in 1916 (_Thierry Draper_)
* 2bf4fef4: Processing changes for single scoring view in NFL SalCap (_Thierry Draper_)
* 5d5a76f1: Mobile changes for NFL SalCap single scoring views (_Thierry Draper_)
* 313f6d6a: NFL SalCap removal of %age relative score if only a single scoring system available (desktop only for now) (_Thierry Draper_)
* e68c1c95: Processing script fix to correctly identify we're accessing live site (thus HTTPS) from data server (_Thierry Draper_)
* b8df9e23: Team / Group viewer Site Manager links (_Thierry Draper_)
* 46893c44: List of Groups within the Site Manager for all three apps (_Thierry Draper_)
* 8c757297: Final tweak for the templating CSS/JS, which should follow the PHP folder convention (_Thierry Draper_)
* b7e0b771: Record Breakers group page tweaks and create/join logic (though need to tweak process) (_Thierry Draper_)
* b64a9fc7: Record Breakers team list admin page (_Thierry Draper_)
* 9e6fe624: (Re-)Include the team config option as a sub option to the Record Breakers Picks nav item (_Thierry Draper_)
* e12e361d: Record Breakers team create validation checks (_Thierry Draper_)
* d6ecffb7: Minor templating refactor to have PHP, JS and CSS all in the same section, like we do the apps (_Thierry Draper_)
* c8d88534: Record Breakers team creation (_Thierry Draper_)
* efa8edf0: Research template fix where lists of a single size required the value to be available within JS even if the dropdown is not needed (_Thierry Draper_)
* 76de40ea: Minor modernisation of redirects (_Thierry Draper_)
* 5661eea5: Record Breakers team object, plumbed in to the nav object (_Thierry Draper_)
* 629837aa: Record Breakers navigation object and interace updates (_Thierry Draper_)
* f2919018: Record Breakers Groups page, plus base object and group action templates copied from SalCap (wip...) (_Thierry Draper_)
* 4211e65e: Include required lockfile sync config option (_Thierry Draper_)
* 6936b756: NFL SalCap script fixes from last season (_Thierry Draper_)
* dca34e4e: Record Breakers Help section (_Thierry Draper_)
* 45c22282: Record Breakers game config and schema (_Thierry Draper_)

## Build v2.1.1052 - f113b56f - 2018-06-10 - Homebrew
* _Changelog truncated for initial build_
