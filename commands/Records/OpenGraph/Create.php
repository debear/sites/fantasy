<?php

namespace DeBear\Console\Commands\Fantasy\Records\OpenGraph;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Fantasy\Commands\Traits\OpenGraphImage as TraitOpenGraphImage;
use DateTime;
use DeBear\Helpers\Config;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTTP;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\ORM\Fantasy\Records\GameMeta;
use DeBear\ORM\Fantasy\Records\GamePeriodSummary;

class Create extends Command
{
    use TraitOpenGraphImage;

    // Rather than generate phan errors, we will explicitly copy and use Symfony's Command constants.
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'debear:fantasy:records-og-create'
        . ' {--sport= : The sport to be processed}'
        . ' {--date= : The date to be processed}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create updated open graph images at the conclusion of a period';

    /**
     * Execute the console command.
     * @return integer A return status
     */
    public function handle()
    {
        $_SERVER['REQUEST_URI'] = '/records/open-graph-command'; // Force triggering of Records subsite config loading.
        Config::loadSite('fantasy');
        $games = [];
        $success = true; // Start by assuming success until we hit a failure.
        $count = 0;

        // Get and validate the sport.
        $sport = $this->option('sport');
        $sport_name = FrameworkConfig::get("debear.sports.$sport.name_short");
        if (!isset($sport_name)) {
            // Unknown argument passed.
            $this->error("'$sport' is an unknown sport - aborting.");
            return static::FAILURE;
        }
        // Get and validate the date.
        $date = $this->option('date');
        $dt = DateTime::createFromFormat('Y-m-d', $date);
        if (!$dt || $dt->format('Y-m-d') != $date) {
            // Invalid argument passed.
            $this->error("'$date' is an invalid date - aborting.");
            return static::FAILURE;
        }
        $proc_date = $dt->modify("-1 day")->format('Y-m-d');

        // Determine the games to be processed having completed a game period.
        $this->info("Processing $sport_name Open Graph images for $proc_date");
        $tbl_period = GamePeriod::getTable();
        $tbl_game = Game::getTable();
        $tbl_summary = GamePeriodSummary::getTable();
        $tbl_meta = GameMeta::getTable();
        $process = GamePeriod::query("$tbl_period AS PERIODS_BASE")
            ->select(['PERIODS_BASE.*', "$tbl_game.game_id"])
            ->selectRaw('COUNT(PERIODS_ALL.period_id) AS periods_total')
            ->selectRaw('PERIODS_BASE.period_order = MAX(PERIODS_ALL.period_order) AS is_last')
            ->join($tbl_game, function ($join) use ($tbl_game, $proc_date) {
                $join->on("$tbl_game.sport", '=', 'PERIODS_BASE.sport')
                    ->on("$tbl_game.season", '=', 'PERIODS_BASE.season')
                    ->whereRaw("? BETWEEN DATE($tbl_game.date_start) AND DATE($tbl_game.date_end)", $proc_date);
            })->join($tbl_summary, function ($join) use ($tbl_summary, $tbl_game) {
                $join->on("$tbl_summary.sport", '=', "$tbl_game.sport")
                    ->on("$tbl_summary.season", '=', "$tbl_game.season")
                    ->on("$tbl_summary.game_id", '=', "$tbl_game.game_id")
                    ->on("$tbl_summary.period_id", '=', 'PERIODS_BASE.period_id');
            })->leftJoin($tbl_meta, function ($join) use ($tbl_meta, $tbl_game) {
                $join->on("$tbl_meta.sport", '=', "$tbl_game.sport")
                    ->on("$tbl_meta.season", '=', "$tbl_game.season")
                    ->on("$tbl_meta.game_id", '=', "$tbl_game.game_id")
                    ->on("$tbl_meta.period_id", '=', 'PERIODS_BASE.period_id');
            })->join("$tbl_period AS PERIODS_ALL", function ($join) {
                $join->on('PERIODS_ALL.sport', '=', 'PERIODS_BASE.sport')
                    ->on('PERIODS_ALL.season', '=', 'PERIODS_BASE.season');
            })->where('PERIODS_BASE.sport', $sport)
            ->whereRaw("? BETWEEN PERIODS_BASE.start_date AND PERIODS_BASE.end_date", $proc_date)
            ->whereNull("$tbl_meta.period_id")
            ->groupBy(['PERIODS_BASE.sport', 'PERIODS_BASE.season', "$tbl_game.game_id"])
            ->get();
        // Summarise the output of this query.
        $this->newLine();
        $this->info(Format::pluralise($process->count(), ' game') . ' to process' . ($process->count() ? ':' : '.'));
        foreach ($process as $period) {
            $game = Game::loadByGameRef($period->sport, $period->season, $period->game_id);
            $game->loadSecondary(['leaderboard']);
            $games[] = $game;
            $this->info("- {$game->game_ref}, '{$game->name}' (Period {$period->period_id})");
        }
        $this->newLine();
        // Now process each game individually.
        foreach ($process as $i => $period) {
            $ret = $this->create($games[$i], $period);
            $count += intval($ret);
            $success = $success && $ret;
            $this->newLine();
        }
        $m = ($success ? 'info' : 'error');
        $this->$m('Overall Status: ' . ($success ? 'SUCCESS' : 'FAILED'));
        $this->info("- Success: $count");
        $this->info('- Failed:  ' . ($process->count() - $count));
        return ($success ? static::SUCCESS : static::FAILURE);
    }

    // Due to an issue with compatibility with the base versions, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment
    /**
     * Overload the error handling, as we want the error to be written to STDERR.
     * @param string              $string    The error message.
     * @param integer|string|null $verbosity A verbosity rating.
     * @return void
     */
    public function error($string, $verbosity = null)
    {
        fwrite(STDERR, !HTTP::isTest() ? "$string\n" : ''); // Silence in CI.
        parent::error($string, $verbosity);
    }
    // phpcs:enable Squiz.Commenting.FunctionComment
}
