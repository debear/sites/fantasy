<?php

namespace DeBear\Console\Commands\Fantasy\Records\OpenGraph;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Fantasy\Commands\Traits\OpenGraphImage as TraitOpenGraphImage;
use DeBear\Helpers\Config;
use DeBear\Helpers\Format;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\ORM\Fantasy\Records\GameMeta;

class Preview extends Command
{
    use TraitOpenGraphImage;

    // Rather than generate phan errors, we will explicitly copy and use Symfony's Command constants.
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'debear:fantasy:records-og-preview'
        . ' {--sport= : The sport to be processed}'
        . ' {--season= : The season to be processed}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create a "preview" open graph images before the start of a game';

    /**
     * Execute the console command.
     * @return integer A return status
     */
    public function handle()
    {
        $_SERVER['REQUEST_URI'] = '/records/open-graph-command'; // Force triggering of Records subsite config loading.
        Config::loadSite('fantasy');
        $games = [];
        $success = true; // Start by assuming success until we hit a failure.
        $count = 0;

        // Get and validate the sport.
        $sport = $this->option('sport');
        $season = $this->option('season');
        $sport_name = FrameworkConfig::get("debear.sports.$sport.name_short");
        if (!isset($sport_name)) {
            // Unknown argument passed.
            $this->error("'$sport' is an unknown sport - aborting.");
            return static::FAILURE;
        } elseif (!isset($season) || !preg_match('/^20[1-3]\d$/', $season)) {
            // Unknown argument passed.
            $this->error("Season argument is missing or invalid - aborting.");
            return static::FAILURE;
        }

        // Determine the games to be processed having not yet started.
        $this->info("Processing $sport_name Open Graph preview images");
        $tbl_game = Game::getTable();
        $tbl_meta = GameMeta::getTable();
        $process = Game::query()
            ->select("$tbl_game.*")
            ->leftJoin($tbl_meta, function ($join) use ($tbl_meta, $tbl_game) {
                $join->on("$tbl_meta.sport", '=', "$tbl_game.sport")
                    ->on("$tbl_meta.season", '=', "$tbl_game.season")
                    ->on("$tbl_meta.game_id", '=', "$tbl_game.game_id")
                    ->where("$tbl_meta.period_id", '=', 1);
            })->where("$tbl_game.sport", $sport)
            ->where("$tbl_game.season", $season)
            ->whereNull("$tbl_meta.period_id")
            ->get();
        // Validate the request.
        if (!$process->count()) {
            // Unknown argument passed.
            $this->error("No valid games found for sport '$sport' in season '$season' - aborting.");
            return static::FAILURE;
        }
        // Summarise the output of this query.
        $this->newLine();
        $this->info(Format::pluralise($process->count(), ' game') . ' to process' . ($process->count() ? ':' : '.'));
        foreach ($process as $row) {
            $game = Game::loadByGameRef($row->sport, $row->season, $row->game_id);
            $games[] = $game;
            $this->info("- {$game->game_ref}, '{$game->name}'");
        }
        $this->newLine();
        // Now process each game individually.
        foreach ($games as $game) {
            $period = new GamePeriod([
                'sport' => $game->sport,
                'season' => $game->season,
                'game_id' => $game->game_id,
                'period_id' => 0,
                'name' => 'Season Preview',
            ]);
            $ret = $this->create($game, $period);
            $count += intval($ret);
            $success = $success && $ret;
            $this->newLine();
        }
        $m = ($success ? 'info' : 'error');
        $this->$m('Overall Status: ' . ($success ? 'SUCCESS' : 'FAILED'));
        $this->info("- Success: $count");
        $this->info('- Failed:  ' . ($process->count() - $count));
        return ($success ? static::SUCCESS : static::FAILURE);
    }
}
