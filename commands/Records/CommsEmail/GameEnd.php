<?php

namespace DeBear\Console\Commands\Fantasy\Records\CommsEmail;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Config;
use DeBear\Helpers\Format;
use DeBear\Helpers\Comms\Email;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\ORM\Fantasy\Records\GamePeriodSummary;
use DeBear\ORM\Fantasy\Records\GameCommsEmail;
use DeBear\ORM\Fantasy\Records\Entry;
use DeBear\ORM\Fantasy\Records\EntryScore;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class GameEnd extends Command
{
    // Rather than generate phan errors, we will explicitly copy and use Symfony's Command constants.
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'debear:fantasy:records-email-gameend';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Determine and send, if applicable, end-of-game email updates';

    /**
     * Execute the console command.
     * @return integer A return status
     */
    public function handle()
    {
        FrameworkConfig::set(['debear.url.sub' => 'fantasy']);
        $_SERVER['REQUEST_URI'] = '/records/email-command'; // Force triggering of Records subsite config loading.
        Config::loadSite('fantasy');

        $proc_date = Time::object()->getServerCurDate();
        $this->info("Processing game end email updates for $proc_date");

        // Determine which sport/season combos, if any, are applicable.
        $tbl_g = Game::getTable();
        $tbl_p = GamePeriod::getTable();
        $tbl_ps = GamePeriodSummary::getTable();
        $tbl_gc = GameCommsEmail::getTable();
        $games = Game::query()
            ->select(["$tbl_g.sport", "$tbl_g.season", "$tbl_g.game_id"])
            ->selectRaw("CONCAT($tbl_g.sport, '-', $tbl_g.season) AS sport_ref")
            ->leftJoin($tbl_p, function ($join) use ($tbl_p, $tbl_g) {
                $join->on("$tbl_p.sport", '=', "$tbl_g.sport")
                    ->on("$tbl_p.season", '=', "$tbl_g.season");
            })->leftJoin($tbl_ps, function ($join) use ($tbl_ps, $tbl_g, $tbl_p) {
                $join->on("$tbl_ps.sport", '=', "$tbl_g.sport")
                    ->on("$tbl_ps.season", '=', "$tbl_g.season")
                    ->on("$tbl_ps.game_id", '=', "$tbl_g.game_id")
                    ->on("$tbl_ps.period_id", '=', "$tbl_p.period_id");
            })->leftJoin($tbl_gc, function ($join) use ($tbl_gc, $tbl_g) {
                $join->on("$tbl_gc.sport", '=', "$tbl_g.sport")
                    ->on("$tbl_gc.season", '=', "$tbl_g.season")
                    ->on("$tbl_gc.game_id", '=', "$tbl_g.game_id")
                    ->where("$tbl_gc.email_type", '=', 'game_end');
            })->whereRaw("? BETWEEN DATE($tbl_g.date_end) AND DATE($tbl_g.date_complete)", [$proc_date])
            ->whereNull("$tbl_gc.email_type")
            ->groupBy("$tbl_g.sport")->groupBy("$tbl_g.season")->groupBy("$tbl_g.game_id")
            ->havingRaw("COUNT(DISTINCT $tbl_p.period_id) = COUNT(DISTINCT $tbl_ps.period_id)")
            ->orderBy("$tbl_g.sport")->orderBy("$tbl_g.season")->orderBy("$tbl_g.game_id")
            ->get();

        // Summarise the output of this query.
        $this->newLine();
        $sports = $games->unique('sport_ref');
        $this->info(Format::pluralise(count($sports), ' sport') . ' to process' . (count($sports) ? ':' : '.'));
        foreach ($sports as $sport_ref) {
            $this->info("- $sport_ref");
            list($sport, $season) = explode('-', $sport_ref);
            $this->process($sport, intval($season), $games->where('sport_ref', $sport_ref)->unique('game_id'));
        }

        return static::SUCCESS;
    }

    /**
     * Perform the email sending for the supplied sport/season combinations
     * @param string  $sport    The sport to be processed.
     * @param integer $season   The season of the sport to be processed.
     * @param array   $game_ids IDs of the individual games being processed.
     * @return void
     */
    protected function process(string $sport, int $season, array $game_ids): void
    {
        // Get our game data.
        $tbl_g = Game::getTable();
        $tbl_es = EntryScore::getTable(); // Re-used in later getter.
        $games = Game::query()
            ->select("$tbl_g.*")
            ->selectRaw("CONCAT($tbl_g.sport, '-', $tbl_g.season, '-', $tbl_g.game_id) AS game_ref")
            ->selectRaw("$tbl_es.overall_res AS leader_raw")
            ->selectRaw("replace(FORMAT($tbl_es.overall_res, 1), '.0', '') AS leader_res")
            ->join($tbl_es, function ($join) use ($tbl_es, $tbl_g) {
                $join->on("$tbl_es.sport", '=', "$tbl_g.sport")
                    ->on("$tbl_es.season", '=', "$tbl_g.season")
                    ->on("$tbl_es.game_id", '=', "$tbl_g.game_id")
                    ->where("$tbl_es.overall_pos", '=', 1);
            })->where([
                ["$tbl_g.sport", '=', $sport],
                ["$tbl_g.season", '=', $season],
            ])->groupBy("$tbl_g.game_id")
            ->get();

        // Get our user data.
        $tbl_e = Entry::getTable();
        $hitrate_bin = "LPAD(BIN($tbl_es.hitrate_num), 32, '0')";
        $all_entries = EntryScore::query()
            ->select(["$tbl_es.*", "$tbl_e.email_type"])
            ->selectRaw("CONCAT($tbl_es.sport, '-', $tbl_es.season, '-', $tbl_es.game_id) AS game_ref")
            ->selectRaw("$tbl_es.overall_res AS overall_raw")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 1, 8), 2, 10) AS hitrate_num_pos")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 9, 8), 2, 10) AS hitrate_num_neu")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 17, 8), 2, 10) AS hitrate_num_neg")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 25, 8), 2, 10) AS hitrate_num_na")
            ->selectRaw("replace(FORMAT($tbl_es.overall_res, 1), '.0', '') AS overall_res")
            ->join($tbl_e, function ($join) use ($tbl_e, $tbl_es) {
                $join->on("$tbl_e.user_id", '=', "$tbl_es.user_id")
                    ->where("$tbl_e.email_status", '=', 1);
            })->where([
                ["$tbl_es.sport", '=', $sport],
                ["$tbl_es.season", '=', $season],
                ["$tbl_es.user_id", '<', FrameworkConfig::get('debear.setup.automated_players.user_ids.min')],
            ])->orderBy("$tbl_es.user_id")
            ->orderBy("$tbl_es.game_id")
            ->get();
        $users = User::query()->whereIn('id', $all_entries->unique('user_id'))->get();

        // Process our email data block.
        $data = [
            'sport' => FrameworkConfig::get("debear.sports.$sport.name_short"),
            'contest' => FrameworkConfig::get("debear.sports.$sport.contest_type"),
            'social_handle' => FrameworkConfig::get('debear.subsites.records.social.twitter.creds.live.master'),
            'games' => $games,
        ];
        foreach ($all_entries->unique('user_id') as $user_id) {
            $data['entry'] = $all_entries->where('user_id', $user_id);
            $this->info("  - User $user_id (Type: {$data['entry']->email_type}; "
                . "Game(s): " . join(', ', $data['entry']->unique('game_ref')) . ')');
            Email::send('Fantasy Records: End-of-Game', $users->find($user_id), [
                'data' => $data,
                'reason' => 'game_end',
                'type' => $data['entry']->email_type,
            ]);
        }

        // Record that we have processed these games.
        $data = [];
        foreach ($game_ids as $game_id) {
            $data[] = [
                'sport' => $sport,
                'season' => $season,
                'game_id' => $game_id,
                'date' => Time::object()->getNowFmt(),
                'email_type' => 'game_end',
            ];
        }
        GameCommsEmail::createMany($data);
    }
}
