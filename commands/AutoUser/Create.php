<?php

namespace DeBear\Console\Commands\Fantasy\AutoUser;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use PDO;
use DeBear\Helpers\Config;
use DeBear\ORM\Fantasy\Admin\AutomatedUser;

class Create extends Command
{
    // Rather than generate phan errors, we will explicitly copy and use Symfony's Command constants.
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'debear:fantasy:autouser-create';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create the core details for automated users';

    /**
     * Execute the console command.
     * @return integer A return status
     */
    public function handle()
    {
        // Get our automated user config.
        Config::loadSite('fantasy');
        $config = FrameworkConfig::get('debear.setup.automated_players.user_ids');

        // Prepare our database query.
        $query = AutomatedUser::query()->select('id')->whereBetween('id', [0, 0]);
        $sth = $query->getConnection()->getPdo()->prepare($query->toSQL());

        // Loop through our batches.
        $totals = ['proc' => 0, 'skipped' => 0, 'failed' => 0];
        $batch_num = 1;
        for ($batch_min = $config['min']; $batch_min <= $config['max']; $batch_min += $config['batch'], $batch_num++) {
            // Confirm our bounds.
            $batch_max = min($batch_min + $config['batch'] - 1, $config['max']);
            $proc = $skipped = [];

            // Determine if any already exist in this range, and if so skip.
            $sth->execute([$batch_min, $batch_max]);
            $skipped = array_column($sth->fetchAll(PDO::FETCH_ASSOC), 'id');
            $proc = array_diff(range($batch_min, $batch_max), $skipped);

            // Create the new users as in-memory data.
            $data = [];
            if (count($proc)) {
                foreach ($proc as $user_id) {
                    $data[] = AutomatedUser::createWithFakerData($user_id);
                }

                // Write as a single database operation.
                AutomatedUser::createMany($data);
            }

            // Post-insert analysis.
            $sth->execute([$batch_min, $batch_max]);
            $after = array_column($sth->fetchAll(PDO::FETCH_ASSOC), 'id');
            $after_count = count($after);
            $failed = array_diff($proc, $after);
            $failed_ids = array_unique(array_intersect_key(array_column($data, 'user_id', 'id'), array_flip($failed)));
            $failed_info = ($failed ? "\n- Failed IDs: " . join(', ', $failed_ids) : ''); // Display version.

            // Report back on progress.
            $proc_count = count($proc);
            $skipped_count = count($skipped);
            $failed_count = $batch_max - $batch_min + 1 - $after_count;
            $m = ($failed_count ? 'error' : 'info');
            $this->$m("Batch $batch_num: $batch_min -> $batch_max => $proc_count processed, $skipped_count skipped, "
                . "$failed_count failed$failed_info");
            // Aggregate the totals.
            $totals['proc'] += $proc_count;
            $totals['skipped'] += $skipped_count;
            $totals['failed'] += $failed_count;
        }

        $m = ($totals['failed'] ? 'error' : 'info');
        $this->$m("TOTALS: {$totals['proc']} processed, {$totals['skipped']} skipped, {$totals['failed']} failed.");
        return (!$totals['failed'] ? static::SUCCESS : static::FAILURE);
    }
}
