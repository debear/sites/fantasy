@php
    // What mode are we operating in?
    $action = empty($form->getValue('name')) ? 'Create' : 'Update';
    $action_lc = strtolower($action);
    $action_ing = substr($action_lc, 0, -1) . 'ing';
@endphp

<h1>{!! $action !!} Your Entry</h1>

@includeWhen(($view = FrameworkConfig::get('debear.forms.overlays.entry.views.form_complete')) && ($message = session('form_complete')), $view)

<fieldset class="section">
    {{-- Team Name --}}
    @php
        $form->validateField('name');
    @endphp
    <ul class="inline_list field name clearfix">
        <li class="label {!! !$form->isFieldValid('name') ? 'error' : '' !!}" id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="{!! $form->getValue('name') !!}" {!! $form->getElementAttributes('name') !!} {!! $form->addTabindex() !!}>
        </li>
        <li class="status {!! $form->formFieldStatus('name') !!}" id="name_status"></li>
        <li class="info">Must be between 5 and 35 characters</li>
        <li class="error details {!! $form->isFieldValid('name') ? 'hidden' : '' !!}" id="name_error">{!! !$form->isFieldValid('name') ? $form->getFieldError('name') : '' !!}</li>
    </ul>

    {{-- Email Updates --}}
    @php
        $email_type_opt = [
            ['id' => 'html', 'label' => '<span class="icon icon_email_html">HTML</span>'],
            ['id' => 'text', 'label' => '<span class="icon icon_email_text">Text Only</span>'],
        ];
        $form->validateField('email_status');
        $form->validateField('email_type', [ 'opt_list' => array_column($email_type_opt, 'id') ]);
        // The by-status dropdown
        $email_type_dropdown = new Dropdown('email_type', $email_type_opt, Arrays::merge(
            [
                'form' => $form,
                'select' => false,
                'value' => $form->getValue('email_type'),
                'disabled' => !$form->getValue('email_status'),
            ],
            $form->getElementValidation('email_type')
        ));
    @endphp
    <ul class="inline_list field email_updates clearfix">
        <li class="label {!! !$form->isFieldValid('email_status') ? 'error' : '' !!}" id="email_updates_label">
            <label for="email_type">Email Updates:</label>&nbsp;
        </li>
        <li class="value email_status">
            <input type="hidden" id="email_status__cb" name="email_status__cb" value="{!! $form->getValue('email_status') ? '1' : '0' !!}">
            <input type="checkbox" id="email_status" name="email_status" {!! $form->getElementAttributes('email_status') !!} {!! $form->getValue('email_status') ? 'checked="checked"' : '' !!}>
            <label for="email_status" data-id="email_status" {!! $form->addTabindex() !!}>Receive Email Updates?</label>
        </li>
        <li class="error details {!! $form->isFieldValid('email_status') ? 'hidden' : '' !!}" id="email_status_error">{!! !$form->isFieldValid('email_status') ? $form->getFieldError('email_status') : '' !!}</li>
        <li class="value email_type">
            {!! $email_type_dropdown->render() !!}
        </li>
        <li class="status {!! $form->formFieldStatus('email_type') !!}" id="email_type_status"></li>
        <li class="error details {!! $form->isFieldValid('email_type') ? 'hidden' : '' !!}" id="email_type_error">{!! !$form->isFieldValid('email_type') ? $form->getFieldError('email_type') : '' !!}</li>
    </ul>
</fieldset>

<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, {!! $action_ing !!} your entry...
    </div>
    <div class="box error icon_error {!! $form->validationPassed() ? 'hidden' : '' !!}">
      <strong>Unable to {!! $action_lc !!} entry</strong>: There were errors when attempting to {!! $action_lc !!} your entry. The fields marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before we can {!! $action_lc !!} your entry. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" {!! $form->addTabindex() !!}>{!! $action !!} Entry</button>
</div>
