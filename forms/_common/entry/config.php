<?php

$custom = config('debear.forms');
return Arrays::merge([
    // If there is no logged in user, give them a 401 response instead.
    'policies' => [
        'list' => 'logged_in',
    ],

    // Default values to load in to the form.
    'defaults' => [
        'internal-cache' => "fantasy.{$custom['namespaces']['cache']}.entry",
    ],

    // Visual steps.
    'steps' => [
        'render',
    ],

    // How to handle the fields.
    'fields' => [
        'name' => [
            'required' => true,
            'minlength' => 5,
            'maxlength' => 35,
            'faker' => 'firstName',
        ],
        'email_status' => [
            'type' => 'checkbox',
            'faker' => true,
        ],
        'email_type' => [
            'faker' => ['_value', 'html'],
            'opt_list' => ['text', 'html'],
        ],
    ],

    // Where and how to save the data.
    'mapping' => [
        "DeBear\\Models\\Fantasy\\{$custom['namespaces']['models']}\\Entry" => [
            'key' => [
                'user_id',
            ],
            'user_fields' => [
                'user_id' => 'id',
            ],
            'form_fields' => [
                'name',
                'email_status',
                'email_type',
            ],
            'fixed_fields' => [
                'joined' => [
                    'timestamp' => 'server_now',
                ],
            ],
            'actions' => [
                'create' => [
                    // Send the user an email confirming their entry.
                    'email-registered' => [
                        'name' => "{$custom['namespaces']['email']}: Create Entry",
                        'reason' => 'entry_create',
                        'send_time' => 'now',
                        'callback' => [
                            "DeBear\\Helpers\\Fantasy\\{$custom['namespaces']['models']}\\Entry",
                            'createEmailAdditionalData'
                        ],
                    ],
                    // Save an audit log.
                    'audit' => [
                        'type' => "{$custom['namespaces']['audit']}_entry_create",
                        'summary' => 'Entry created',
                        'fields' => ['name', 'email_status', 'email_type'],
                    ],
                    // Custom updates when the entry was created.
                    ["DeBear\\Helpers\\Fantasy\\{$custom['namespaces']['models']}\\Entry", 'entryFormCreated'],
                ],
                'update' => [
                    // Save an audit log.
                    'audit' => [
                        'type' => "{$custom['namespaces']['audit']}_entry_update",
                        'summary' => 'Entry updated',
                        'fields' => ['name', 'email_status', 'email_type'],
                    ],
                    // Custom updates when the entry was updated.
                    ["DeBear\\Helpers\\Fantasy\\{$custom['namespaces']['models']}\\Entry", 'entryFormUpdated'],
                ],
            ],
        ],
    ],

    // Where to go upon completion.
    'next' => [
        'redirect' => false, // Probably overlayed.
        'message' => 'Your entry to ' . FrameworkConfig::get('debear.names.section') . ' has been'
            . " {session|fantasy.{$custom['namespaces']['session']}.entry.message}",
    ],
], Arrays::merge(
    Interpolate::array($custom['overlays']['entry']['interpolate']),
    $custom['overlays']['entry']['raw'] ?? []
));
