<?php

namespace DeBear\Helpers\Fantasy\Entities;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Fantasy\Entities;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Skeleton\WeatherForecast;
use Illuminate\Support\Facades\DB;

class MajorLeague extends Entities
{
    /**
     * Load the sport-specific player information for the provided periods
     * @param QueryBuilder $query      The player loading query to be extended.
     * @param string       $sport      The sport of the players being loaded.
     * @param integer      $season     The season of the game being loaded.
     * @param array        $player_ids The Link/Period IDs of players to be loaded.
     * @return void
     */
    protected static function loadPlayersInfo(QueryBuilder &$query, string $sport, int $season, array $player_ids): void
    {
        $tbl_tr = Namespaces::callStatic($sport, 'TeamRoster', 'getTable');
        $tbl_s = Namespaces::callStatic($sport, 'Schedule', 'getTable');
        // Determine the appropriate roster date for each selection.
        $roster_dates = Namespaces::callStatic($sport, 'TeamRoster', 'query', ['tmp_entities'])
            ->select('tmp_entities.entity_ref')
            ->selectRaw("MAX($tbl_tr.the_date) AS roster_date")
            ->join($tbl_tr, function ($join) use ($tbl_tr, $season) {
                $join->where("$tbl_tr.season", '=', $season)
                    ->on("$tbl_tr.player_id", '=', 'tmp_entities.player_id')
                    ->on("$tbl_tr.the_date", '<=', 'tmp_entities.period_date');
            })->groupBy('tmp_entities.entity_ref')->get();
        // Then modify and update our temporary entity table.
        $dbh = DB::connection('mysql_sports');
        $dbh->statement('ALTER TABLE tmp_entities ADD COLUMN roster_date DATE;');
        $sql = 'UPDATE tmp_entities SET roster_date = CASE entity_ref'
            . str_repeat(' WHEN ? THEN ?', count($roster_dates)) . ' END;';
        $args = [];
        foreach ($roster_dates as $entity) {
            $args[] = $entity->entity_ref;
            $args[] = $entity->roster_date;
        }
        if (count($args)) {
            $dbh->statement($sql, $args);
        }
        // Fill out the rest of the query.
        $query->selectRaw("$tbl_tr.team_id AS icon")
            ->selectRaw("GROUP_CONCAT(DISTINCT IF($tbl_tr.team_id = $tbl_s.home, 'v',
                IF($tbl_tr.team_id = $tbl_s.visitor, '@', NULL))) AS info")
            // This relies on any double header being against the same team,
            // though the above still works if the 'home'/'visitor' designation is technically flipped.
            ->selectRaw("IF($tbl_tr.team_id = $tbl_s.home, $tbl_s.visitor,
                IF($tbl_tr.team_id = $tbl_s.visitor, $tbl_s.home, NULL)) AS info_icon")
            ->selectRaw("COUNT(DISTINCT $tbl_s.game_id) > 1 AS info_dh")
            ->selectRaw("CONCAT($tbl_s.game_date, ' ', MIN($tbl_s.game_time)) AS event_time")
            ->selectRaw("IF($tbl_s.status IN ('PPD','CNC','SSP'), $tbl_s.status, 'SCH') AS event_status")
            ->leftJoin($tbl_tr, function ($join) use ($tbl_tr, $season) {
                $join->where("$tbl_tr.season", '=', $season)
                    ->on("$tbl_tr.the_date", '=', 'tmp_entities.roster_date')
                    ->on("$tbl_tr.player_id", '=', 'tmp_entities.player_id');
            })->leftJoin($tbl_s, function ($join) use ($tbl_s, $tbl_tr, $season) {
                $join->where("$tbl_s.season", '=', $season)
                    ->where("$tbl_s.game_type", '=', 'regular')
                    ->on("$tbl_s.game_date", '=', 'tmp_entities.period_date')
                    ->whereRaw("$tbl_tr.team_id IN ($tbl_s.home, $tbl_s.visitor)");
            });
        // Sport-specific info.
        if (method_exists(static::class, 'loadPlayersInfoStatus')) {
            static::loadPlayersInfoStatus($query, $sport);
        }
        // Per-selection weather.
        if (FrameworkConfig::get("debear.sports.$sport.weather.type") == 'sel') {
            static::loadWeatherInfo($query, $sport);
        }
    }

    /**
     * Load the team data for teams within the link list
     * @param string  $sport     The sport of the teams being loaded.
     * @param integer $season    The season of the game being loaded.
     * @param array   $team_ids  The Link/Period IDs of teams to be loaded.
     * @param array   $team_maps The mapping of sport's Team ID to fantasy's Link ID.
     * @return array The array of raw ORM data for teams in the supplied list of links
     */
    protected static function loadTeams(string $sport, int $season, array $team_ids, array $team_maps): array
    {
        $teams_by_raw = array_flip($team_maps);
        // Start by flattening our team/period combo into a temporary tale.
        $dbh = DB::connection('mysql_sports');
        $dbh->statement('DROP TEMPORARY TABLE IF EXISTS tmp_entities;');
        $dbh->statement("CREATE TEMPORARY TABLE tmp_entities (
            entity_ref VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
            team_id CHAR(3) NOT NULL,
            period_ref VARCHAR(10) COLLATE latin1_general_ci,
            period_id TINYINT(3) UNSIGNED NULL,
            period_date DATE DEFAULT NULL,
            PRIMARY KEY (entity_ref)
        ) ENGINE = MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");
        foreach ($team_ids as $team_id => $periods) {
            foreach (array_unique((is_array($periods) ? $periods : [$periods])) as $period) {
                $args = [$teams_by_raw[$team_id] . ":$period", $team_id, $period, $period, $period];
                $dbh->insert('INSERT INTO tmp_entities VALUES (?, ?, ?, CAST(? AS UNSIGNED), CAST(? AS DATE));', $args);
            }
        }
        // Then start our base query to extend out from this.
        $tbl_t = Namespaces::callStatic($sport, 'Team', 'getTable');
        $tbl_tn = Namespaces::callStatic($sport, 'TeamNaming', 'getTable');
        $query = Namespaces::callStatic($sport, 'Team', 'query', ['tmp_entities'])
            ->selectRaw('? AS sport', [$sport])
            ->selectRaw('? AS season', [$season])
            ->selectRaw('"team" AS type')
            ->selectRaw("tmp_entities.entity_ref")
            ->selectRaw('NULL AS link_id')
            ->selectRaw("$tbl_t.team_id AS raw_id")
            ->selectRaw("$tbl_t.team_id")
            ->selectRaw("CONCAT(IFNULL($tbl_tn.alt_city, $tbl_t.city), ' ',
                IFNULL($tbl_tn.alt_franchise, $tbl_t.franchise)) AS name")
            ->join($tbl_t, "$tbl_t.team_id", '=', 'tmp_entities.team_id')
            ->leftJoin($tbl_tn, function ($join) use ($tbl_tn, $tbl_t, $season) {
                $join->whereRaw("IFNULL($tbl_tn.alt_team_id, $tbl_tn.team_id) = $tbl_t.team_id")
                    ->where("$tbl_tn.season_from", '<=', $season)
                    ->where(DB::raw("IFNULL($tbl_tn.season_to, 2099)"), '>=', $season);
            });
        // Customise the query to add icon and info.
        static::loadTeamsInfo($query, $sport, $season, $team_ids);
        // Per-selection weather.
        if (FrameworkConfig::get("debear.sports.$sport.weather.type") == 'sel') {
            static::loadWeatherInfo($query, $sport);
        }
        // Run and process.
        $teams_model = $query->groupBy('tmp_entities.entity_ref')->get();
        $teams = $teams_model->getData();
        array_walk($teams, function (&$team) use ($teams_model, $teams_by_raw) {
            $team['link_id'] = $teams_by_raw[$team['raw_id']];
            $team['model'] = $teams_model->where('team_id', $team['team_id']);
        });
        return $teams;
    }

    /**
     * Load the sport-specific team information for the provided periods
     * @param QueryBuilder $query    The team loading query to be extended.
     * @param string       $sport    The sport of the teams being loaded.
     * @param integer      $season   The season of the game being loaded.
     * @param array        $team_ids The Link/Period IDs of teams to be loaded.
     * @return void
     */
    protected static function loadTeamsInfo(QueryBuilder &$query, string $sport, int $season, array $team_ids): void
    {
        // Determine if we operate by a date or week model.
        $dt_week = (FrameworkConfig::get("debear.sports.$sport.period.sport_col") == 'week');
        $dt_sch = !$dt_week ? 'game_date' : 'week';
        $dt_ent = !$dt_week ? 'period_date' : 'period_id';
        // Add to our query.
        $t = Namespaces::callStatic($sport, 'Team', 'getTable');
        $tbl_s = Namespaces::callStatic($sport, 'Schedule', 'getTable');
        $query->selectRaw("$t.team_id AS icon")
            ->selectRaw("GROUP_CONCAT(DISTINCT IF($t.team_id = $tbl_s.home, 'v',
                IF($t.team_id = $tbl_s.visitor, '@', NULL))) AS info")
            // This relies on any double header being against the same team,
            // though the above still works if the 'home'/'visitor' designation is technically flipped.
            ->selectRaw("IF($t.team_id = $tbl_s.home, $tbl_s.visitor,
                IF($t.team_id = $tbl_s.visitor, $tbl_s.home, NULL)) AS info_icon")
            ->selectRaw("COUNT(DISTINCT $tbl_s.game_id) > 1 AS info_dh")
            ->selectRaw("CONCAT($tbl_s.game_date, ' ', MIN($tbl_s.game_time)) AS event_time")
            ->selectRaw("IF($tbl_s.status IN ('PPD','CNC','SSP'), $tbl_s.status, 'SCH') AS event_status")
            ->leftJoin($tbl_s, function ($join) use ($tbl_s, $t, $season, $dt_sch, $dt_ent) {
                $join->where("$tbl_s.season", '=', $season)
                    ->where("$tbl_s.game_type", '=', 'regular')
                    ->on("$tbl_s.$dt_sch", '=', "tmp_entities.$dt_ent")
                    ->whereRaw("$t.team_id IN ($tbl_s.home, $tbl_s.visitor)");
            });
    }

    /**
     * Load the weather forecast information to the query
     * @param QueryBuilder $query The team loading query to be extended.
     * @param string       $sport The sport of the teams being loaded.
     * @return void
     */
    protected static function loadWeatherInfo(QueryBuilder &$query, string $sport): void
    {
        $tbl_s = Namespaces::callStatic($sport, 'Schedule', 'getTable');
        $db_w = WeatherForecast::getDatabase();
        $tbl_w = WeatherForecast::getTable();
        $tbl_v = Namespaces::callStatic($sport, 'TeamVenue', 'getTable');
        $unit = FrameworkConfig::get("debear.sports.$sport.weather.unit");
        $col_temp = ($unit == 'c' ? "$tbl_w.temp" : "($tbl_w.temp * (9 / 5)) + 32");
        $query->selectRaw("IF($tbl_v.weather_spec IS NULL, 'dome',
                IF($tbl_w.symbol IS NOT NULL, 'set', 'unset')) AS weather_status")
            ->selectRaw("$tbl_w.symbol AS weather_symbol")
            ->selectRaw("$tbl_w.summary AS weather_summary")
            ->selectRaw("ROUND($col_temp) AS weather_temp")
            ->selectRaw("$tbl_w.precipitation AS weather_precip")
            ->selectRaw("$tbl_w.wind_speed AS weather_windsp")
            ->selectRaw("$tbl_w.wind_dir AS weather_winddir")
            ->leftJoin("$db_w.$tbl_w", function ($join) use ($tbl_w, $tbl_s, $sport) {
                $join->where("$tbl_w.app", '=', FrameworkConfig::get("debear.sports.subsites.$sport.weather.app"))
                    ->where("$tbl_w.instance_key", '=', DB::raw('CONCAT(' . join(', "-", ', [
                        "$tbl_s.season", "$tbl_s.game_type", "$tbl_s.game_id"
                    ]) . ')'));
            })->leftJoin($tbl_v, function ($join) use ($tbl_v, $tbl_s) {
                $join->on("$tbl_v.team_id", '=', DB::raw("IFNULL($tbl_s.home, '_ALT')"))
                    ->whereRaw("$tbl_s.season BETWEEN $tbl_v.season_from AND IFNULL($tbl_v.season_to, 2099)")
                    ->where(function ($q) use ($tbl_v, $tbl_s) {
                        $q->whereNull("$tbl_s.alt_venue")
                            ->orOn("$tbl_v.building_id", '=', "$tbl_s.alt_venue");
                    });
            });
    }
}
