<?php

namespace DeBear\Helpers\Fantasy\Entities\Motorsport;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Fantasy\Entities\Motorsport;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Fantasy\Common\MotorsSelectionStatus;

class F1 extends Motorsport
{
    /**
     * Load the sport-specific player information for the provided periods
     * @param QueryBuilder $query      The player loading query to be extended.
     * @param string       $sport      The sport of the players being loaded.
     * @param integer      $season     The season of the game being loaded.
     * @param array        $player_ids The Link/Period IDs of players to be loaded.
     * @return void
     */
    protected static function loadPlayersInfo(QueryBuilder &$query, string $sport, int $season, array $player_ids): void
    {
        $tbl_p = Namespaces::callStatic($sport, 'Participant', 'getTable');
        $tbl_rd = Namespaces::callStatic($sport, 'RaceParticipant', 'getTable');
        $tbl_t = Namespaces::callStatic($sport, 'Team', 'getTable');
        $tbl_r = Namespaces::callStatic($sport, 'Race', 'getTable');
        $tbl_st = MotorsSelectionStatus::getTable();
        $db_st = MotorsSelectionStatus::getDatabase();
        $col_time = str_replace('TBL.', "$tbl_r.", FrameworkConfig::get("debear.sports.$sport.period.lock_time"));
        $query->selectRaw("$tbl_p.nationality AS icon")
            ->selectRaw("$tbl_t.team_name AS info")
            ->selectRaw("$tbl_t.team_country AS info_icon")
            ->selectRaw("MIN($col_time) AS event_time")
            ->selectRaw("IF($tbl_r.race_laps_completed = 0, 'CNC', 'SCH') AS event_status")
            ->selectRaw("IFNULL($tbl_st.status, IF($tbl_rd.season IS NULL, 'ne', NULL)) AS status")
            ->leftJoin($tbl_rd, function ($join) use ($tbl_rd, $tbl_p, $sport, $season) {
                $join->where("$tbl_rd.season", '=', $season)
                    ->where("$tbl_rd.series", '=', $sport)
                    ->on("$tbl_rd.round", '=', 'tmp_entities.period_id')
                    ->on("$tbl_rd.driver_id", '=', "$tbl_p.driver_id");
            })->leftJoin($tbl_t, function ($join) use ($tbl_t, $tbl_rd) {
                $join->on("$tbl_t.season", '=', "$tbl_rd.season")
                    ->on("$tbl_t.series", '=', "$tbl_rd.series")
                    ->on("$tbl_t.team_id", '=', "$tbl_rd.team_id");
            })->leftJoin($tbl_r, function ($join) use ($tbl_r, $sport, $season) {
                $join->where("$tbl_r.season", '=', $season)
                    ->where("$tbl_r.series", '=', $sport)
                    ->on("$tbl_r.round", '=', 'tmp_entities.period_id');
            })->leftJoin("$db_st.$tbl_st", function ($join) use ($tbl_st, $tbl_p, $sport, $season) {
                $join->where("$tbl_st.sport", '=', $sport)
                    ->where("$tbl_st.season", '=', $season)
                    ->on("$tbl_st.period_id", '=', 'tmp_entities.period_id')
                    ->on("$tbl_st.link_id", '=', "$tbl_p.driver_id");
            });
    }
}
