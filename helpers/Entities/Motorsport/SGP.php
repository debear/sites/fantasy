<?php

namespace DeBear\Helpers\Fantasy\Entities\Motorsport;

use DeBear\Helpers\Fantasy\Entities\Motorsport;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Fantasy\Common\MotorsSelectionStatus;

class SGP extends Motorsport
{
    /**
     * Load the sport-specific player information for the provided periods
     * @param QueryBuilder $query      The player loading query to be extended.
     * @param string       $sport      The sport of the players being loaded.
     * @param integer      $season     The season of the game being loaded.
     * @param array        $player_ids The Link/Period IDs of players to be loaded.
     * @return void
     */
    protected static function loadPlayersInfo(QueryBuilder &$query, string $sport, int $season, array $player_ids): void
    {
        $tbl_p = Namespaces::callStatic($sport, 'Participant', 'getTable');
        $tbl_rd = Namespaces::callStatic($sport, 'RaceParticipant', 'getTable');
        $tbl_r = Namespaces::callStatic($sport, 'Race', 'getTable');
        $tbl_st = MotorsSelectionStatus::getTable();
        $db_st = MotorsSelectionStatus::getDatabase();
        $query->selectRaw("$tbl_p.nationality AS icon")
            ->selectRaw("NULL AS info")
            ->selectRaw("NULL AS info_icon")
            ->selectRaw("MIN($tbl_r.race_time) AS event_time")
            ->selectRaw("'SCH' AS event_status") // Currently no way of handling Cancelled events.
            ->selectRaw("IFNULL($tbl_st.status, IF($tbl_rd.season IS NULL, 'ne', NULL)) AS status")
            ->leftJoin($tbl_rd, function ($join) use ($tbl_rd, $tbl_p, $season) {
                $join->where("$tbl_rd.season", '=', $season)
                    ->on("$tbl_rd.round", '=', 'tmp_entities.period_id')
                    ->on("$tbl_rd.rider_id", '=', "$tbl_p.rider_id");
            })->leftJoin($tbl_r, function ($join) use ($tbl_r, $sport, $season) {
                $join->where("$tbl_r.season", '=', $season)
                    ->on("$tbl_r.round", '=', 'tmp_entities.period_id');
            })->leftJoin("$db_st.$tbl_st", function ($join) use ($tbl_st, $tbl_p, $sport, $season) {
                $join->where("$tbl_st.sport", '=', $sport)
                    ->where("$tbl_st.season", '=', $season)
                    ->on("$tbl_st.period_id", '=', 'tmp_entities.period_id')
                    ->on("$tbl_st.link_id", '=', "$tbl_p.rider_id");
            });
    }
}
