<?php

namespace DeBear\Helpers\Fantasy\Entities\MajorLeague;

use DeBear\Helpers\Fantasy\Entities\MajorLeague;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;

class AHL extends MajorLeague
{
    /**
     * Logic for determining a player's status
     * @param QueryBuilder $query The player loading query to be extended.
     * @param string       $sport The sport of the players being loaded.
     * @return void
     */
    protected static function loadPlayersInfoStatus(QueryBuilder &$query, string $sport): void
    {
        // Ignore an 'active' player status.
        $tbl_tr = Namespaces::callStatic($sport, 'TeamRoster', 'getTable');
        $query->selectRaw("IF($tbl_tr.player_status <> 'active', $tbl_tr.player_status, NULL) AS status");
    }
}
