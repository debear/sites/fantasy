<?php

namespace DeBear\Helpers\Fantasy\Entities\MajorLeague;

use DeBear\Helpers\Fantasy\Entities\MajorLeague;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use Illuminate\Support\Facades\DB;

class NFL extends MajorLeague
{
    /**
     * Load the sport-specific player information for the provided periods
     * @param QueryBuilder $query      The player loading query to be extended.
     * @param string       $sport      The sport of the players being loaded.
     * @param integer      $season     The season of the game being loaded.
     * @param array        $player_ids The Link/Period IDs of players to be loaded.
     * @return void
     */
    protected static function loadPlayersInfo(QueryBuilder &$query, string $sport, int $season, array $player_ids): void
    {
        $tbl_tr = Namespaces::callStatic($sport, 'TeamRoster', 'getTable');
        $tbl_s = Namespaces::callStatic($sport, 'Schedule', 'getTable');
        $tbl_pi = Namespaces::callStatic($sport, 'PlayerInjury', 'getTable');
        $tbl_gi = Namespaces::callStatic($sport, 'GameInactive', 'getTable');
        // Determine the appropriate roster date for each selection.
        $roster_weeks = Namespaces::callStatic($sport, 'TeamRoster', 'query', ['tmp_entities'])
            ->select('tmp_entities.entity_ref')
            ->selectRaw("MAX($tbl_tr.week) AS roster_week")
            ->join($tbl_tr, function ($join) use ($tbl_tr, $season) {
                $join->where("$tbl_tr.season", '=', $season)
                    ->where("$tbl_tr.game_type", '=', 'regular')
                    ->on("$tbl_tr.player_id", '=', 'tmp_entities.player_id')
                    ->on("$tbl_tr.week", '<=', 'tmp_entities.period_id');
            })->groupBy('tmp_entities.entity_ref')->get();
        // Then modify and update our temporary entity table.
        $dbh = DB::connection('mysql_sports');
        $dbh->statement('ALTER TABLE tmp_entities ADD COLUMN roster_week TINYINT(2) UNSIGNED;');
        $sql = 'UPDATE tmp_entities SET roster_week = CASE entity_ref'
            . str_repeat(' WHEN ? THEN ?', count($roster_weeks)) . ' END;';
        $args = [];
        foreach ($roster_weeks as $entity) {
            $args[] = $entity->entity_ref;
            $args[] = $entity->roster_week;
        }
        if (count($args)) {
            $dbh->statement($sql, $args);
        }
        // Fill out the rest of the query.
        $query->selectRaw("$tbl_tr.team_id AS icon")
            ->selectRaw("GROUP_CONCAT(DISTINCT IF($tbl_tr.team_id = $tbl_s.home, 'v',
                IF($tbl_tr.team_id = $tbl_s.visitor, '@', NULL))) AS info")
            // This relies on any double header being against the same team,
            // though the above still works if the 'home'/'visitor' designation is technically flipped.
            ->selectRaw("IF($tbl_tr.team_id = $tbl_s.home, $tbl_s.visitor,
                IF($tbl_tr.team_id = $tbl_s.visitor, $tbl_s.home, NULL)) AS info_icon")
            ->selectRaw("COUNT(DISTINCT $tbl_s.game_id) > 1 AS info_dh")
            ->selectRaw("CONCAT($tbl_s.game_date, ' ', MIN($tbl_s.game_time)) AS event_time")
            ->selectRaw("IF($tbl_s.status IN ('PPD','CNC','SSP'), $tbl_s.status, 'SCH') AS event_status")
            ->selectRaw("IF($tbl_tr.player_status <> 'active', $tbl_tr.player_status,
                IF($tbl_gi.game_id IS NOT NULL, 'inj-o',
                    IF($tbl_pi.status IS NOT NULL, CONCAT('inj-', $tbl_pi.status), NULL))) AS status")
            ->leftJoin($tbl_tr, function ($join) use ($tbl_tr, $season) {
                $join->where("$tbl_tr.season", '=', $season)
                    ->where("$tbl_tr.game_type", '=', 'regular')
                    ->on("$tbl_tr.week", '=', 'tmp_entities.roster_week')
                    ->on("$tbl_tr.player_id", '=', 'tmp_entities.player_id');
            })->leftJoin($tbl_s, function ($join) use ($tbl_s, $tbl_tr, $season) {
                $join->where("$tbl_s.season", '=', $season)
                    ->where("$tbl_s.game_type", '=', 'regular')
                    ->on("$tbl_s.week", '=', 'tmp_entities.period_id')
                    ->whereRaw("$tbl_tr.team_id IN ($tbl_s.home, $tbl_s.visitor)");
            })->leftJoin($tbl_pi, function ($join) use ($tbl_pi, $season) {
                $join->where("$tbl_pi.season", '=', $season)
                    ->where("$tbl_pi.game_type", '=', 'regular')
                    ->on("$tbl_pi.week", '=', 'tmp_entities.period_id')
                    ->on("$tbl_pi.player_id", '=', 'tmp_entities.player_id');
            })->leftJoin($tbl_gi, function ($join) use ($tbl_gi, $season) {
                $join->where("$tbl_gi.season", '=', $season)
                    ->where("$tbl_gi.game_type", '=', 'regular')
                    ->on("$tbl_gi.week", '=', 'tmp_entities.period_id')
                    ->on("$tbl_gi.player_id", '=', 'tmp_entities.player_id');
            });
    }
}
