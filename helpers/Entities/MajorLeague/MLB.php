<?php

namespace DeBear\Helpers\Fantasy\Entities\MajorLeague;

use DeBear\Helpers\Fantasy\Entities\MajorLeague;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;

class MLB extends MajorLeague
{
    /**
     * Logic for determining a player's status
     * @param QueryBuilder $query The player loading query to be extended.
     * @param string       $sport The sport of the players being loaded.
     * @return void
     */
    protected static function loadPlayersInfoStatus(QueryBuilder &$query, string $sport): void
    {
        // Ignore an 'active' player status, and flag the starters.
        $tbl_tr = Namespaces::callStatic($sport, 'TeamRoster', 'getTable');
        $tbl_s = Namespaces::callStatic($sport, 'Schedule', 'getTable');
        $tbl_gl = Namespaces::callStatic($sport, 'GameLineup', 'getTable');
        $query->selectRaw("IF($tbl_tr.player_status <> 'active', $tbl_tr.player_status,
            IF($tbl_gl.game_id IS NOT NULL, CONCAT('pos-', $tbl_gl.spot),
                IF({$tbl_gl}_lo.game_id IS NOT NULL, 'pos-dns', NULL))) AS status")
            // The player's lineup position.
            ->leftJoin($tbl_gl, function ($join) use ($tbl_gl, $tbl_s) {
                $join->on("$tbl_gl.season", '=', "$tbl_s.season")
                    ->on("$tbl_gl.game_type", '=', "$tbl_s.game_type")
                    ->on("$tbl_gl.game_id", '=', "$tbl_s.game_id")
                    ->where("$tbl_gl.spot", '>', 0) // Ignore SP.
                    ->on("$tbl_gl.player_id", '=', 'tmp_entities.player_id');
            })
            // Have we got a lineup set?
            ->leftJoin("$tbl_gl AS {$tbl_gl}_lo", function ($join) use ($tbl_gl, $tbl_s) {
                $join->on("{$tbl_gl}_lo.season", '=', "$tbl_s.season")
                    ->on("{$tbl_gl}_lo.game_type", '=', "$tbl_s.game_type")
                    ->on("{$tbl_gl}_lo.game_id", '=', "$tbl_s.game_id")
                    ->where("{$tbl_gl}_lo.spot", '=', 1);
            });
    }
}
