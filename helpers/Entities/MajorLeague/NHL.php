<?php

namespace DeBear\Helpers\Fantasy\Entities\MajorLeague;

use DeBear\Helpers\Fantasy\Entities\MajorLeague;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\ORM\Base\Helpers\QueryBuilder;

class NHL extends MajorLeague
{
    /**
     * Logic for determining a player's status
     * @param QueryBuilder $query The player loading query to be extended.
     * @param string       $sport The sport of the players being loaded.
     * @return void
     */
    protected static function loadPlayersInfoStatus(QueryBuilder &$query, string $sport): void
    {
        // Ignore an 'active' player status, and flag the scratches.
        $tbl_tr = Namespaces::callStatic($sport, 'TeamRoster', 'getTable');
        $tbl_s = Namespaces::callStatic($sport, 'Schedule', 'getTable');
        $tbl_gi = Namespaces::callStatic($sport, 'GameInactive', 'getTable');
        $query->selectRaw("IF($tbl_tr.player_status <> 'active', $tbl_tr.player_status,
            IF($tbl_gi.game_id IS NOT NULL, 'inj-o', NULL)) AS status")
            ->leftJoin($tbl_gi, function ($join) use ($tbl_gi, $tbl_s) {
                $join->on("$tbl_gi.season", '=', "$tbl_s.season")
                    ->on("$tbl_gi.game_type", '=', "$tbl_s.game_type")
                    ->on("$tbl_gi.game_id", '=', "$tbl_s.game_id")
                    ->on("$tbl_gi.player_id", '=', 'tmp_entities.player_id');
            });
    }
}
