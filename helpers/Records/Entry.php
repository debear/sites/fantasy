<?php

namespace DeBear\Helpers\Fantasy\Records;

use DeBear\ORM\Fantasy\Records\Game as GameModel;

trait Entry
{
    /**
     * Set additional data fields to be used in the email sent to a user upon entry creation
     * @param array $args The arguments we passed to the email processing. (Pass-by-Reference).
     * @return void
     */
    public static function createEmailAdditionalData(array &$args): void
    {
        // Email type comes from the form data.
        $args['type'] = ($args['data']['email_type'] ?? 'html');
        $args['opt_out'] = !boolval($args['data']['email_status']);

        // Which games are currently open for the user to join?
        $args['data']['open_games'] = GameModel::loadByStatus(['open', 'inprogress'])->random(3);
        $args['data']['suggestions_view'] = ($args['data']['open_games']->count() ? 'suggestions' : 'thanks');
    }

    /**
     * Actions to perform when the user's entry was just created
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function entryFormCreated(array $data): void
    {
        session()->flash('fantasy.records.entry.message', 'created &ndash; now join a game and start making your '
            . 'selections!');
    }

    /**
     * Actions to perform when the user's entry was just updated
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function entryFormUpdated(array $data): void
    {
        session()->flash('fantasy.records.entry.message', 'updated.');
    }
}
