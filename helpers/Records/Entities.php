<?php

namespace DeBear\Helpers\Fantasy\Records;

use DeBear\ORM\Fantasy\Common\Selection;

class Entities
{
    /**
     * Render the link for a specific selection, moving the info <span tag to within the link, not wrapping it
     * @param Selection $entity    The selection object to be processed.
     * @param integer   $period_id An optional period_id to which the modal should focus.
     * @return string The HTML tags containing the link and selection info within the anchor tag.
     */
    public static function displayLink(Selection $entity, ?int $period_id = null): string
    {
        $period_attr = (isset($period_id) ? ' data-period-id="' . $period_id . '"' : '');
        return preg_replace(
            '/(<span[^>]+>)(.*?)(<\/span>)/',
            '$1<a class="view-sel-summary" data-slug="' . $entity->slug . '"' . $period_attr . '>$2</a>$3',
            $entity->display_name
        );
    }
}
