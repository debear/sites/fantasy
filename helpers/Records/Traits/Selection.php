<?php

namespace DeBear\Helpers\Fantasy\Records\Traits;

use DeBear\ORM\Fantasy\Common\Selection as EntityModel;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\ORM\Fantasy\Records\GameStat;
use DeBear\ORM\Fantasy\Records\SelectionStat;
use DeBear\Repositories\Time;
use Illuminate\Database\Query\Builder;

trait Selection
{
    /**
     * As we're creating a blank entry, overload the isset() method by looking for an actual selection
     * @return boolean That the object contains a selection
     */
    public function isset(): bool
    {
        return isset($this->link_id) && $this->link_id;
    }

    /**
     * Render the selection rating as a star rating for display
     * @return float|null The opponent rating converted from number to stars to be displayed
     */
    public function getRatingDispAttribute(): ?float
    {
        return static::convertRating($this->rating);
    }

    /**
     * Render the opponent rating as a star rating for display
     * @return float|null The opponent rating converted from number to stars to be displayed
     */
    public function getOppRatingDispAttribute(): ?float
    {
        return static::convertRating($this->opp_rating);
    }

    /**
     * Check whether the individual selection's lock time has past
     * @return boolean That the selection is now locked
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsLockedAttribute(): bool
    {
        return isset($this->entity->lock_time->system)
            && (Time::object()->getNowFmt() >= $this->entity->lock_time->system->toDateTimeString());
    }

    /**
     * When no selection was made, create an appropriate and processable ORM object
     * @param Game    $game      Information about the game being viewed.
     * @param integer $period_id Reference to the game period being viewed.
     * @return self An empty ORM object constructed as per the expectations for a selection
     */
    public static function createBlank(Game $game, int $period_id): self
    {
        return new static([
            'sport' => $game->sport,
            'season' => $game->season,
            'game_id' => $game->game_id,
            'period_id' => $period_id,
            'entity' => (new EntityModel([
                'sport' => $game->sport,
                'season' => $game->season,
                'type' => $game->sel_type,
            ])),
        ]);
    }

    /**
     * Load from the stat details for the selections found
     * @return void
     */
    public function loadSecondaryStats(): void
    {
        // Get the stat values for our selections.
        $tbl_stat = SelectionStat::getTable();
        $tbl_meta = GameStat::getTable();
        $sel_ref = join(', "-", ', ["$tbl_stat.game_id", "$tbl_stat.link_id", "$tbl_stat.period_id"]);
        $query = SelectionStat::query()
            ->select(["$tbl_stat.stat_id", "$tbl_stat.stat_value"])
            ->selectRaw("CONCAT($sel_ref) AS sel_ref")
            ->join($tbl_meta, function ($join) use ($tbl_stat, $tbl_meta) {
                $join->on("$tbl_meta.sport", '=', "$tbl_stat.sport")
                    ->on("$tbl_meta.season", '=', "$tbl_stat.season")
                    ->on("$tbl_meta.game_id", '=', "$tbl_stat.game_id")
                    ->on("$tbl_meta.stat_id", '=', "$tbl_stat.stat_id");
            })->orderBy("$tbl_stat.link_id")
            ->orderBy("$tbl_stat.period_id")
            ->orderBy("$tbl_meta.disp_order");
        foreach ($this->data as $row) {
            $query->orWhere(function (Builder $where) use ($tbl_stat, $row) {
                $where->where([
                    ["$tbl_stat.sport", '=', $row['sport']],
                    ["$tbl_stat.season", '=', $row['season']],
                    ["$tbl_stat.game_id", '=', $row['game_id']],
                    ["$tbl_stat.link_id", '=', $row['link_id']],
                    ["$tbl_stat.period_id", '=', $row['period_id']],
                ]);
            });
        }
        $stats = $query->get();
        // Now tie back together.
        foreach ($this->data as $i => $row) {
            $sel_ref = join('-', [$row['game_id'], $row['link_id'], $row['period_id']]);
            $this->data[$i]['stats'] = [];
            foreach ($stats->where('sel_ref', $sel_ref) as $stat) {
                $this->data[$i]['stats'][$stat->stat_id] = $stat->stat_value;
            }
        }
    }
}
