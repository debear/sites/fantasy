<?php

namespace DeBear\Helpers\Fantasy\Records\Traits;

use DeBear\Repositories\InternalCache;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;

trait Controller
{
    /**
     * Get the current user's entry
     * @return EntryModel An ORM object representing the user's entry
     */
    protected function getUserEntry(): EntryModel
    {
        $entry = InternalCache::object()->get('fantasy.records.entry');
        if (!is_object($entry)) {
            $entry = EntryModel::load();
            InternalCache::object()->set('fantasy.records.entry', $entry);
        }
        return $entry;
    }
}
