<?php

namespace DeBear\Helpers\Fantasy\Records\Traits\Controller;

use Illuminate\Support\Facades\Request;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;
use DeBear\ORM\Fantasy\Records\Selection as SelectionModel;

trait EntrySel
{
    /**
     * Parse the query arguments to ensure any passed are valid
     * @return array|null The array of parsed and validated arguments, or null upon a validation error
     */
    protected function validateIndexQueryArguments(): ?array
    {
        $ret = [];
        // Pagination.
        $ret['page'] = (Request::has('page') ? Request::input('page') : 1);
        if (!is_numeric($ret['page']) || ($ret['page'] < 1)) {
            return null;
        }
        // Further filtering rules do not apply to all games.
        if ($this->game->canFilterSelections()) {
            // Include locked selections.
            if (Request::has('locked')) {
                $value = Request::input('locked');
                if (!in_array($value, ['true', 'false'])) {
                    // Not a boolean value, so reject the request.
                    return null;
                }
                $ret['locked'] = ($value == 'true'); // Convert text to boolean.
            }
            // Specific team (if "_ALL" it can be ignored).
            if (Request::has('team_id') && Request::input('team_id') != '_ALL') {
                $team_id = Request::input('team_id');
                $all_teams = $this->period->getTeams();
                if (!array_key_exists($team_id, $all_teams)) {
                    // Argument not in the list.
                    return null;
                }
                $ret['team_id'] = $team_id;
            }
            // Include only those selections with an opponent yet to be defeated.
            if (Request::has('beaten_opp') && $this->game->hasAchievement('opponent')) {
                $value = Request::input('beaten_opp');
                if (!in_array($value, ['true', 'false'])) {
                    // Not a boolean value, so reject the request.
                    return null;
                }
                $ret['beaten_opp'] = ($value == 'true'); // Convert text to boolean.
            }
        }
        // Sort order.
        if (Request::has('sort')) {
            $sort = Request::input('sort');
            $all_opt = array_filter(array_merge([
                'rating',
                $this->game->isMajorLeague() ? 'opp_rating' : null,
            ], $this->game->stats->unique('stat_id')));
            if (!in_array($sort, $all_opt)) {
                return null;
            }
            $ret['sort'] = $sort;
        }
        // Return our valid list.
        return $ret;
    }

    /**
     * For the selection(s) returned, process the appropriate selection flags
     * @param SelectionModel $sel The selection(s) to be processed.
     * @return void
     */
    protected function setSelectionFlags(SelectionModel $sel): void
    {
        // Determine which selections, if any, are not available to the user.
        if (!$this->game->canReuseSelection()) {
            $sel->setUnavailable($this->period);
        }
        // Determine if any selections are considered "Streak Busters".
        if ($this->game->hasStreakBuster()) {
            $sel->setStreakBusters();
        }
        // Determine, if applicable, the opponents already beaten by the user.
        if ($this->game->hasAchievement('opponent')) {
            $entry = $this->validateUserAndLoadEntry(UserModel::object()->user_id);
            if ($entry instanceof EntryModel) {
                $sel->setAchievedOpponents($this->game, $entry->getGameScore($this->game));
            }
        }
    }
}
