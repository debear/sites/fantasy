<?php

namespace DeBear\Helpers\Fantasy\Records\Traits\API;

trait Scoring
{
    /**
     * Format the score value appropriately
     * @param integer|float|null $score Original value to be formatted.
     * @return integer|float|null Appropriately formatted value
     */
    protected function formatScore(int|float|null $score): int|float|null
    {
        if (!isset($score)) {
            return $score;
        }
        return strpos((string)$score, '.') === false ? (int)$score : (float)sprintf('%0.01f', $score);
    }

    /**
     * Format the score value appropriately when being rendered within an API
     * @param string|integer|float|null $score Original value to be formatted.
     * @return integer|float|null Appropriately formatted value
     */
    protected function formatScoreAPI(string|int|float|null $score): int|float|null
    {
        // Convert a display value into a numeric value.
        if (isset($score) && strpos((string)$score, ',') !== false) {
            $score = (float)str_replace(',', '', $score);
        }
        return $this->formatScore($score);
    }

    /**
     * Convert a raw rating into a star rating for display
     * @param integer $rating The numeric rating to be converted.
     * @return float|null The raw rating converted from number to stars to be displayed
     */
    protected static function convertRating(?int $rating): float|null
    {
        return isset($rating) ? ceil($rating / 10) / 2 : null;
    }

    /**
     * Render the stress score as a percentage
     * @return integer|null The stress score converted from number to a percentage
     */
    public function getStressPctAttribute(): ?int
    {
        return isset($this->stress) ? intval(100 * ($this->stress / 255)) : null;
    }
}
