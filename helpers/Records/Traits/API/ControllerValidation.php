<?php

namespace DeBear\Helpers\Fantasy\Records\Traits\API;

use Illuminate\Http\JsonResponse;
use DeBear\Helpers\API;
use DeBear\ORM\Skeleton\APIToken;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\ORM\Fantasy\Admin\User as UserORM;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Entry;
use DeBear\ORM\Fantasy\Records\EntryScore;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\Repositories\Time;

trait ControllerValidation
{
    /**
     * Validate the user requested to be actioned via this API token, and return their entry
     * @param string  $user_id  The user for whom we are validating relative to the API token used.
     * @param boolean $required That the user must have an entry for the request to proceed.
     * @return JsonResponse|Entry An API response upon validation error, or the user's entry model on success
     */
    protected function validateUserAndLoadEntry(string $user_id, bool $required = true): JsonResponse|Entry
    {
        // First check if the user has permission to create this user's entry (if not their own scope, they cannot).
        $token = APIToken::object();
        if (!$token->isScopeValid('records-admin') && $token->user->user_id != $user_id) {
            // Not an admin, and not creating for the token's user account.
            return API::sendForbidden();
        }
        // Does the requested user account exist?
        $user = UserORM::loadByStringID($user_id);
        if ($token->isScopeValid('records-admin')) {
            if (!$user->isset()) {
                // An admin is creating an entry for a user that doesn't exist.
                return API::sendNotFound('User could not be found');
            } elseif (UserORM::isAutomatedUser($user->id) && !in_array(request()->method(), ['GET', 'HEAD'])) {
                return API::setStatus(422, 'User cannot be processed');
            } elseif ($user->id != $token->user->user_id) {
                // Override the internal user reference to the passed in user.
                UserModel::setupFromAPI($user->id);
            }
        }
        // Load the entry object for this user, and unless allowed otherwise, ensure one was returned.
        $entry = Entry::load($user->id);
        if ($required && !$entry->isset()) {
            return API::sendBadRequest('The user does not have an entry');
        }
        return $entry;
    }

    /**
     * Validate the game information provided to the API, and return the ORM object
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse|Game An API response upon validation error, or the game ORM object on success
     */
    protected function validateGameAndLoad(string $sport, int $season, int $game_id): JsonResponse|Game
    {
        $game = Game::loadByGameRef($sport, $season, $game_id);
        $now = Time::object()->getNowFmt();
        $tDTS = 'toDateTimeString'; // Shorthand method invocation.
        if (!$game->isset() || ($now < $game->date_open->$tDTS()) || ($now >= $game->date_complete->$tDTS())) {
            // This game is not available.
            return API::sendNotFound('The game could not be found');
        }
        $game->loadSecondary(['achievements']);
        return $game;
    }

    /**
     * Validate the period information provided to the API, and return the ORM object
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse|GamePeriod An API response upon validation error, or the period ORM object on success
     */
    protected function validatePeriodAndLoad(string $sport, int $season, int $period_id): JsonResponse|GamePeriod
    {
        $period = GamePeriod::query()->where([
            ['sport', '=', $sport],
            ['season', '=', $season],
            ['period_id', '=', $period_id],
        ])->get();
        if (!$period->isset()) {
            return API::sendNotFound('Game period not found');
        }
        return $period;
    }

    /**
     * Validate the period information provided to the API, and return the ORM object
     * @param Entry      $entry  The user's entry being processed.
     * @param Game       $game   The game ORM object being referenced.
     * @param GamePeriod $period The period ORM object being referenced.
     * @return JsonResponse|EntryScore An API response upon validation error, or the score ORM object on success
     */
    protected function validateEntryGamePeriod(Entry $entry, Game $game, GamePeriod $period): JsonResponse|EntryScore
    {
        // Is the entry playing in the game?
        $score = $entry->getGameScore($game);
        if (!$score->isset()) {
            return API::sendNotFound('Entry not found');
        }
        // If the user's entry is no longer active, it cannot access periods after it was eliminated.
        if (!$game->canRestartGame() && !$score->isActive()) {
            $last_period = $score->getLastSelectionPeriod($game);
            if ($last_period->isset() && $last_period->period_order < $period->period_order) {
                return API::setStatus(422, 'Period is not available to this entry'); // 422 = Unprocessable Entity.
            }
        }
        return $score;
    }
}
