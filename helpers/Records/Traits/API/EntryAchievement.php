<?php

namespace DeBear\Helpers\Fantasy\Records\Traits\API;

trait EntryAchievement
{
    /**
     * The array of binary to flag mappings
     * @var array
     */
    protected $bitflag_maps = [];

    /**
     * Convert the raw hitrate numbers into a percentage result
     * @return string|null The entry's hitrate as formatted percentage number
     */
    public function getHitrateResAttribute(): ?string
    {
        return ($this->hitrate_num_pos + $this->hitrate_num_neu + $this->hitrate_num_neg) > 0
            ? sprintf('%0.02f', (100 * ($this->hitrate_num_pos + $this->hitrate_num_neu))
                / ($this->hitrate_num_pos + $this->hitrate_num_neu + $this->hitrate_num_neg))
            : null;
    }

    /**
     * Get the array mapped conversion of the raw achievements bit flag
     * @return array An array of achievements achieved by the entry, keyed by the achievement's bitflag
     */
    public function getAchievementsMapAttribute(): array
    {
        return $this->getBitFlagMapping('achievements', 16);
    }

    /**
     * Get the array mapped conversion of the changed achievements bit flag between periods
     * @return array An array of achievements achieved during the previous period, keyed by the achievement's bitflag
     */
    public function getAchievementsChgMapAttribute(): array
    {
        return $this->getBitFlagMapping('achievements_chg', 16);
    }

    /**
     * Get the array mapped conversion of the raw opponents bit flag
     * @return array An array of opponents achieved by the entry, keyed by the opponent's bitflag
     */
    public function getOpponentsMapAttribute(): array
    {
        return $this->getBitFlagMapping('opponents', 32);
    }

    /**
     * Generic worker method for the decimal to bit map column conversion
     * @param string  $key     The source of the raw decimal number.
     * @param integer $max_num The theoretical maximum number of bitflags available.
     * @return array The array of bitflags
     */
    protected function getBitFlagMapping(string $key, int $max_num): array
    {
        if (!isset($this->bitflag_maps[$key])) {
            // Ensure our array is one-indexed, and with the correct number of elements (padded with a leading 'false').
            $flags = str_split('0' . str_pad(strrev(decbin($this->$key ?? 0)), $max_num, '0'));
            $this->bitflag_maps[$key] = array_map('boolval', $flags);
        }
        return $this->bitflag_maps[$key];
    }
}
