<?php

namespace DeBear\Helpers\Fantasy\Records\Traits\Game;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Fantasy\Records\EntryScore;
use DeBear\Repositories\Time;

trait Tests
{
    /**
     * Determine if the selections are made up of individual people
     * @return boolean That the pool of selections is made up of individual people
     */
    public function isSelTypeIndividual(): bool
    {
        return $this->sel_type == 'ind';
    }

    /**
     * Determine if the selections are made up of team entities
     * @return boolean That the pool of selections is made up of team entities
     */
    public function isSelTypeTeam(): bool
    {
        return $this->sel_type == 'team';
    }

    /**
     * Determine if the game scoring type counts stat totals
     * @return boolean That the game scoring type is based on stat totals
     */
    public function isScoreTypeTotal(): bool
    {
        return $this->score_type == 'total';
    }

    /**
     * Determine if the game scoring type counts period streaks
     * @return boolean That the game scoring type is based on a streak of periods
     */
    public function isScoreTypeStreak(): bool
    {
        return $this->score_type == 'streak';
    }

    /**
     * Determine if the game scoring type counts valid periods
     * @return boolean That the game scoring type is based on a count of valid periods
     */
    public function isScoreTypePeriods(): bool
    {
        return $this->score_type == 'periods';
    }

    /**
     * Determine if a selection is required in a given game period
     * @return boolean That an entry is required to make a selection within a game period
     */
    public function isSelectionRequired(): bool
    {
        return $this->config_flags->sel_required;
    }

    /**
     * Determine if the winow for new entries to this game is closing
     * @return boolean That the game is shortly going to close for new entries
     */
    public function isClosingSoon(): bool
    {
        $now = Time::object()->getNowFmt();
        $soon = $this->date_close->copy()->subDays(FrameworkConfig::get('debear.games.closing_soon'));
        return $soon->toDateTimeString() <= $now && $now < $this->date_close->toDateTimeString();
    }

    /**
     * Determine if the game has been completed
     * @return boolean That the game has been completed and is in 'archive' mode
     */
    public function isComplete(): bool
    {
        return Time::object()->getNowFmt() >= $this->date_complete->toDateTimeString();
    }

    /**
     * Deteremine if the game allows selections to be used multiple times
     * @return boolean That a selection can be used more than once
     */
    public function canReuseSelection(): bool
    {
        return !$this->config_flags->sel_single_use;
    }

    /**
     * Determine if a user can continue to play after making a 'negative' selection
     * @return boolean That the user can restart the game after an invalid selection
     */
    public function canRestartGame(): bool
    {
        return !$this->config_flags->game_ends_on_negative;
    }

    /**
     * Determine if the selections in this came can be filtered
     * @return boolean That the selection in this game can be filtered
     */
    public function canFilterSelections(): bool
    {
        return $this->isMajorLeague() && ($this->isSelTypeIndividual() || $this->hasAchievement('opponent'));
    }

    /**
     * Determine if the game has an active lookback limit in place
     * @return boolean That the game applies its totals over a limited number of periods
     */
    public function hasActiveLimit(): bool
    {
        return (($this->active_limit ?? 0) > 0);
    }

    /**
     * Determine if the game contains multiple (real-life) record holders
     * @return boolean That there are multiple current record holders
     */
    public function hasMultipleRecordHolders(): bool
    {
        return strpos($this->record_holder, ';') !== false;
    }

    /**
     * Determine if the leaderboard includes a "Current" list, as well as the "Overall" list
     * @return boolean That there is a "Current" leaderboard for this game
     */
    public function hasCurrentLeaderboard(): bool
    {
        // Completed games do not have a current leaderboard.
        if ($this->isComplete()) {
            return false;
        }
        // Games with an active limit have the current leaderboard at a dynamic point in the season.
        if ($this->hasActiveLimit()) {
            return (bool) EntryScore::query()
                ->selectRaw('SUM(current_pos <> overall_pos) > 0 AS has_current_leaderboard')
                ->where([
                    ['sport', '=', $this->sport],
                    ['season', '=', $this->season],
                    ['game_id', '=', $this->game_id],
                ])->get()->has_current_leaderboard;
        }
        // Otherwise, only restartable streak games have a current leaderboard.
        return $this->isScoreTypeStreak() && $this->canRestartGame();
    }

    /**
     * Determine if the leaderboard includes a "Hit Rate" list
     * @return boolean That there is a "Hit Rate" leaderboard for this game
     */
    public function hasHitRateLeaderboard(): bool
    {
        return $this->config_flags->hitrate_leaderboard;
    }

    /**
     * Determine if the game includes the "Streak Buster" feature
     * @return boolean That we consider "Streak Busters" within this game
     */
    public function hasStreakBuster(): bool
    {
        return $this->config_flags->streak_busters;
    }

    /**
     * Determine if the game includes the "Performance Review" summary
     * @return boolean That there is a "Performance Review" summary for this game
     */
    public function hasPerformanceSummary(): bool
    {
        return !$this->config_flags->no_period_summary;
    }

    /**
     * Determine if the game includes an achievement of the passed type
     * @param string $type The type of achievement being checked.
     * @return boolean That the game includes and appropriate achievement
     */
    public function hasAchievement(string $type): bool
    {
        if (!isset($this->achievements)) {
            $this->loadSecondary(['achievements']);
        }
        return $this->achievements->where('type', $type)->isset();
    }
}
