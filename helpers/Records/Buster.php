<?php

namespace DeBear\Helpers\Fantasy\Records;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as ScoringTrait;
use DeBear\Helpers\Fantasy\Records\Traits\Selection as SelectionTrait;

class Buster
{
    use ScoringTrait;
    use SelectionTrait;

    /**
     * The raw data
     * @var array
     */
    protected $data;

    /**
     * Display the name of this team with a corresponding logo
     * @return string The HTML containing the team name and logo
     */
    protected function getDisplayNameAttribute(): string
    {
        $css = FrameworkConfig::get("debear.sports.{$this->sport}.team.icon");
        return "<span class=\"{$css}{$this->team_id}\">{$this->name}</span>";
    }

    /**
     * Display the buster rating as a series of stars
     * @return string The rating visualised with stars
     */
    protected function getDisplayRatingAttribute(): string
    {
        $num_halves = $this->rating * 2;
        $num_whole = floor($num_halves / 2);
        $num_outline = 5 - intval(ceil($num_halves / 2));
        return str_repeat("<i class=\"fa-solid fa-star sel-halves-bw-{$num_halves}\"></i>", intval($num_whole))
            . (($num_halves % 2) == 1
                ? "<i class=\"fa-solid fa-star-half-stroke sel-halves-bw-{$num_halves}\"></i>"
                : '')
            . str_repeat("<i class=\"fa-regular fa-star sel-halves-bw-{$num_halves}\"></i>", intval($num_outline));
    }

    /**
     * Convert the raw buster score into a five-star rating
     * @return float|null The numeric rating
     */
    protected function getRatingAttribute(): float|null
    {
        return static::convertRating($this->score);
    }

    /**
     * Prepare thedata within the object
     * @param array $data The data being added to the model object.
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get a value for record attribute
     * @param string $name The name of the object attribute.
     * @return mixed The value of the requested attribute in the object
     */
    public function __get(string $name): mixed
    {
        // A direct match in the data.
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        // Fallback to an accessor.
        $accessor = 'get' . str_replace(' ', '', ucwords(preg_replace('/[^a-z0-9]+/i', ' ', $name))) . 'Attribute';
        return $this->$accessor();
    }
}
