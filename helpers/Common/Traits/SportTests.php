<?php

namespace DeBear\Helpers\Fantasy\Common\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait SportTests
{
    /**
     * Determine if the sport this object is based on relies on our "Major League" logic
     * @return boolean That the sport for this object is based on "Major League" sporting logic
     */
    public function isMajorLeague(): bool
    {
        return FrameworkConfig::get("debear.sports.{$this->sport}.namespace") == 'MajorLeague';
    }

    /**
     * Determine if the sport this object is based on relies on our "Motorsport" logic
     * @return boolean That the sport for this object is based on "Motorsport" sporting logic
     */
    public function isMotorsport(): bool
    {
        return FrameworkConfig::get("debear.sports.{$this->sport}.namespace") == 'Motorsport';
    }
}
