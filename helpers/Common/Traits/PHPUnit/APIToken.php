<?php

namespace DeBear\Helpers\Fantasy\Common\Traits\PHPUnit;

trait APIToken
{
    /**
     * The username of the default API Token used in the unit tests
     * @var string
     */
    protected $defaultAPITokenUser = 'fantasy-token';
    /**
     * The password of the default API Token used in the unit tests
     * @var string
     */
    protected $defaultAPITokenPass = 'apitokenpassword';
    /**
     * The username of a custom API Token used in certain unit tests
     * @var string|null
     */
    protected $customAPITokenUser;
    /**
     * The password of a custom API Token used in certain unit tests
     * @var string|null
     */
    protected $customAPITokenPass;

    /**
     * Modify the request by inserting the Authorization header for our Fantasy API token
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Original headers passed in to our test request.
     * @return void
     */
    protected function prepare(string $uri, array &$headers): void
    {
        $token_user = ($this->customAPITokenUser ?? $this->defaultAPITokenUser);
        $token_pass = ($this->customAPITokenPass ?? $this->defaultAPITokenPass);
        $headers['Authorization'] = 'Basic ' . base64_encode("{$token_user}:{$token_pass}");
        parent::prepare($uri, $headers);
    }

    /**
     * Override our API key with a custom key to be used in specific tests(s)
     * @param string $token_user The API token user.
     * @param string $token_pass The API token password.
     * @return void
     */
    protected function setAPIToken(string $token_user, string $token_pass): void
    {
        $this->customAPITokenUser = $token_user;
        $this->customAPITokenPass = $token_pass;
    }

    /**
     * Remove a custom API key so our tests revert to the default key
     * @return void
     */
    protected function revertAPIToken(): void
    {
        $this->customAPITokenUser = $this->customAPITokenPass = null;
    }
}
