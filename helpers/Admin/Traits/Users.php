<?php

namespace DeBear\Helpers\Fantasy\Admin\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Fantasy\Profile\BudgetTeam;
use DeBear\ORM\Fantasy\Profile\RecordTeam;
use DeBear\Helpers\Highcharts;

trait Users
{
    /**
     * Load the list of fantasy teams for the user(s) found
     * @return void
     */
    protected function loadSecondaryTeams(): void
    {
        $budget = BudgetTeam::load($this->unique('id'));
        $record = RecordTeam::load($this->unique('id'));
        $this->secondary['teams'] = $budget->merge($record)->sort(function ($a, $b) {
            // First by season, descending.
            if ($a['season'] != $b['season']) {
                return ($a['season'] < $b['season']) ? 1 : -1;
            }
            // Then by games that are still active.
            if ($a['in_progress'] != $b['in_progress']) {
                return ($a['in_progress'] < $b['in_progress']) ? 1 : -1;
            }
            // Next, by game.
            if ($a['game'] != $b['game']) {
                return ($a['game'] < $b['game']) ? -1 : 1;
            }
            // Final tie breaker, name.
            return $a['name'] <=> $b['name'];
        });
        // Now tie together.
        foreach ($this->data as $i => $user) {
            $user_teams = $this->secondary['teams']->where('user_id', $user['id']);
            // Calculate the profile score for this user.
            if ($user_teams->count()) {
                $this->data[$i]['rating'] = $user_teams->avg('profile_points');
            }
            // Now add (below the rating).
            $this->data[$i]['teams'] = $user_teams;
        }
    }

    /**
     * Derive the list of meads earned by the teams played
     * @return void
     */
    protected function loadSecondaryMedals(): void
    {
        foreach ($this->data as $i => $user) {
            $medals = [];
            foreach ($user['teams'] as $team) {
                foreach (($team->detail?->medals ?? []) as $key => $num) {
                    if (!isset($medals[$key])) {
                        $medals[$key] = 0;
                    }
                    $medals[$key] += $num;
                }
            }
            $this->data[$i]['medals'] = $medals;
        }
    }

    /**
     * Return the list of medals earned by this user
     * @return array The list of medals with appropriate display info.
     */
    public function medalsEarned(): array
    {
        // Get the setup info for the medals this team won.
        $config = array_intersect_key(FrameworkConfig::get('debear.setup.medals'), $this->medals);
        uasort($config, function ($a, $b) {
            return $a['rank'] <=> $b['rank'];
        });
        // Now process the config into values specific to the user.
        $medals = [];
        $instance = 0;
        foreach ($config as $k => $m) {
            foreach (array_reverse($m['instances']) as $i => $cmp) {
                if ($this->medals[$k] >= $cmp) {
                    $instance = $i;
                    break;
                }
            }
            $medals[$k] = [
                'title' => $m['title'],
                'info' => $m['info'],
                'icon' => $m['icon'] . substr('abcdefghijklmnopqrstuvwxyz', $instance, 1),
                'num' => $this->medals[$k],
            ];
        }
        return $medals;
    }

    /**
     * Build the Highcharts configuration for the Team Breakdown chart
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the user's team breakdown
     */
    public function teamBreakdownChart(string $dom_id): array
    {
        // Breakdown as a pie chart.
        $chart = Highcharts::new($dom_id);
        // Customise - type.
        $chart['chart']['type'] = 'pie';
        // Customise - series.
        $chart['series'] = [['name' => 'Game Type', 'colorByPoint' => true, 'data' => []]];
        // Determine the counts - descending order.
        $counts = [];
        $total_teams = $this->teams->count();
        foreach ($this->teams as $team) {
            $key = ($team->game == 'records' ? $team->game : "{$team->game_type}-{$team->game}");
            if (!isset($counts[$key])) {
                $counts[$key] = 0;
            }
            $counts[$key]++;
        }
        arsort($counts);
        // Add to the series.
        foreach ($counts as $key => $num) {
            $chart['series'][0]['data'][] = [
                'name' => FrameworkConfig::get("debear.subsites.$key.names.section"),
                'y' => $num,
            ];
        }
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \' + this.y + \' of '
                . $total_teams . ' team' . ($total_teams == 1 ? '' : 's') . '</tooltip>\'; }',
        ];
        return $chart;
    }

    /**
     * Perform any final post-processing on the API data before sending back
     * @param array $row The rendered data for a single row to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        // Merge country info into a single element.
        if (isset($row['country'])) {
            $row['country'] = [
                'name' => $row['country'],
                'flag' => $row['country_flag'] ?? null,
            ];
            unset($row['country_flag']);
        }
        // Expand medal info.
        $row['medals'] = $this->medalsEarned();
    }
}
