<?php

namespace DeBear\Helpers\Fantasy\Commands\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Exceptions\ExecException;
use DeBear\Helpers\Blade;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTTP;
use DeBear\Repositories\Time;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\ORM\Fantasy\Records\GameMeta;

trait OpenGraphImage
{
    /**
     * Process the Open Graph image for a single game
     * @param Game       $game   An ORM object of the game we will be processing.
     * @param GamePeriod $period An ORM object of the game period we will be processing, with game_id addition.
     * @return boolean An appropriate success or failure state
     * @throws ExecException When an external error occurs preventing creation of the image.
     */
    protected function create(Game $game, GamePeriod $period): bool
    {
        $config = FrameworkConfig::get('debear.meta.og.create');
        $dir_root = resource_path('images');
        $this->info("- Game '{$game->name}' (Ref: {$game->game_ref}), "
            . "Period '{$period->name_disp}' (ID: {$period->period_id})");
        try {
            $rel_img = "/{$config['img_path']}/{$game->game_ref}/{$period->period_id}";
            $file_html = "$dir_root$rel_img.html";
            $file_img = "$dir_root$rel_img.{$config['format']}";
            $dir_img = dirname("$dir_root/{$rel_img}");
            if (!file_exists($dir_img)) {
                $this->info("  - Creating cache dir $dir_img");
                mkdir($dir_img);
            }
            // Generate the HTML on-disk.
            $tmpl = ($period->period_id > 0 ? 'latest' : 'preview');
            file_put_contents($file_html, Blade::render("fantasy.records.og.$tmpl", compact([
                'game',
                'period',
                'config',
            ])));
            // And put it through the tool to generate an image from the HTML.
            $cmd_arg = escapeshellarg(json_encode([
                'descrip' => 'Open Graph',
                'src' => "file://$file_html",
                'dst' => $file_img,
                'width' => $config['width'],
                'height' => $config['height'],
            ]));
            $cmd = 'INSTANCE_REF=' . (getenv('INSTANCE_REF') ?: getmypid()) . ' '
                . escapeshellcmd($config['cnv_path']) . " $cmd_arg 2>&1";
            $this->info("  - Running command: $cmd");
            unset($cmd_out); // Ensure empty, as exec appends to an existing array rather than overwrites.
            // This next line is semgrep noise - for some reason use of escapeshellcmd|arg() does not satisfy it.
            // nosemgrep PHPCS_SecurityAudit.BadFunctions.SystemExecFunctions.WarnSystemExec.
            $ret_val = exec($cmd, $cmd_out, $ret_code);
            $this->info("  - Command output:");
            $this->info("    - " . join("\n      ", explode("\n", join("\n", str_replace("\r", "\n", $cmd_out)))));
            if ((is_bool($ret_val) && !$ret_val) || (is_int($ret_code) && $ret_code > 0)) {
                throw new ExecException("{$config['cnv_path']} failed; V:" . ($ret_val ? 't' : 'f') . ", C:$ret_code");
            }
            // Log info about the image, perform one final check of the output.
            $file_stat = stat($file_img); // We can expect this to exist, otherwise the command would fail.
            $this->info("  - Generated Image: $file_img");
            $this->info("    - Size: " . Format::bytes($file_stat['size']) . " (Raw: {$file_stat['size']} byte/s)");
            $this->info('    - Time: ' . date('r', $file_stat['ctime']));
            if ($file_stat['size'] <= $config['min_bytes']) {
                throw new ExecException('Generated file size is below the expected minimum size');
            }
            // Remove the HTML file.
            unlink($file_html);
            // Store info about this new og image in the database for page loading speed.
            $path_og = HTTP::buildCDNURLs("$rel_img.{$config['format']}");
            GameMeta::create([
                'sport' => $period->sport,
                'season' => $period->season,
                'game_id' => $period->game_id,
                'period_id' => $period->period_id,
                'is_latest' => 1,
                'path_og' => preg_replace('/^\/\/[^\/]+\//', '/', $path_og), // Endpoint only, strip domain.
                'when_processed' => Time::object()->getNowFmt(),
            ]);
            $this->info("    - CDN URL: https:$path_og");
            // For the final period, copy the image to our archived game location.
            if ($period->is_last ?? false) {
                $this->info("  - Copying image to archive location");
                copy($file_img, "$dir_img.{$config['format']}");
            }
            // Ensure this new record is the only instance marked "latest".
            $previous_latest = GameMeta::query()
                ->where([
                    ['sport', '=', $period->sport],
                    ['season', '=', $period->season],
                    ['game_id', '=', $period->game_id],
                    ['period_id', '!=', $period->period_id],
                    ['is_latest', '=', 1],
                ])->get();
            foreach ($previous_latest as $prev) {
                $this->info("  - Unsetting \"latest\" period: {$prev->period_id}");
                $prev->update(['is_latest' => 0])->save();
            }
            // If we're here, the image was generated successfully.
            $success = true;
        } catch (ExecException $e) {
            $this->error('  - An error occurred: ' . $e->getMessage());
            $success = false;
        }

        return $success;
    }
}
