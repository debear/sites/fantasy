<?php

namespace DeBear\Helpers\Fantasy;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\ORM\Fantasy\Common\SelectionMap;
use DeBear\ORM\Fantasy\Common\Selection;
use DeBear\Helpers\Fantasy\Namespaces;
use Carbon\Carbon;
use DeBear\Repositories\Time;

class Entities
{
    /**
     * Sport-agnostic wrapper for loading the given teams/players via the sport-specific processor
     * @param string  $sport    The sport of the teams/players being loaded.
     * @param integer $season   The season of the game being loaded.
     * @param array   $link_ids The Link/Period IDs of teams/players to be loaded.
     * @return Selection An ORM object containing the info for the teams/players requested
     */
    public static function load(string $sport, int $season, array $link_ids): Selection
    {
        $config = FrameworkConfig::get("debear.sports.$sport");
        $class = "\\DeBear\\Helpers\\Fantasy\\Entities\\{$config['namespace']}\\{$config['section']}";
        return call_user_func_array([$class, 'loadWorker'], [$sport, $season, $link_ids]);
    }

    /**
     * More tightly coupled method for loading selections for the given sport
     * @param string  $sport    The sport of the teams/players being loaded.
     * @param integer $season   The season of the game being loaded.
     * @param array   $link_ids The Link/Period IDs of teams/players to be loaded.
     * @return Selection An ORM object containing the info for the teams/players requested
     */
    public static function loadWorker(string $sport, int $season, array $link_ids): Selection
    {
        // Determine which of the list are teams (and so which are players).
        $team_ids = SelectionMap::query()
            ->select(['link_id', 'raw_id'])
            ->where([
                'sport' => $sport,
                'season' => $season,
            ])
            ->whereIn('link_id', array_keys($link_ids))
            ->groupBy('link_id')
            ->get()
            ->getData();
        $team_maps = array_column($team_ids, 'raw_id', 'link_id');
        $team_links = [];
        foreach (array_intersect_key($link_ids, $team_maps) as $link_id => $period_id) {
            $team_links[$team_maps[$link_id]] = $period_id;
        }
        $player_links = array_diff_key($link_ids, $team_maps);
        // Load details for each type, separately.
        $teams = count($team_links) ? static::loadTeams($sport, $season, $team_links, $team_maps) : [];
        $players = count($player_links) ? static::loadPlayers($sport, $season, $player_links) : [];
        // Perform common post-processing and return as a combined ORM object.
        $db_tz = FrameworkConfig::get("debear.sports.subsites.$sport.datetime.timezone_db");
        $entities = array_map(function ($e) use ($db_tz) {
            if ($e['event_status'] == 'SCH' && isset($e['event_time'])) {
                // Selection has an event today, so format accordingly.
                $e['lock_time'] = (object)[
                    'display' => Carbon::createFromFormat('Y-m-d H:i:s', $e['event_time'], $db_tz)
                        ->subMinutes(FrameworkConfig::get('debear.setup.lock_offset'))
                        ->setTimezone(Time::object()->getUserTimezone()),
                    'system' => Carbon::createFromFormat('Y-m-d H:i:s', $e['event_time'], $db_tz)
                        ->subMinutes(FrameworkConfig::get('debear.setup.lock_offset'))
                        ->setTimezone(Time::object()->getTimezone()),
                ];
                unset($e['event_time']);
            } else {
                // No event today.
                $e['event_status'] = ($e['event_status'] != 'SCH' ? $e['event_status'] : 'PPD');
                $e['lock_time'] = (object)['display' => null, 'system' => null];
            }
            return $e;
        }, array_merge($teams, $players));
        return new Selection($entities);
    }

    /**
     * Load the player data for players within the link list
     * @param string  $sport      The sport of the players being loaded.
     * @param integer $season     The season of the game being loaded.
     * @param array   $player_ids The Link/Period IDs of players to be loaded.
     * @return array The array of raw ORM data for players in the supplied list of links
     */
    protected static function loadPlayers(string $sport, int $season, array $player_ids): array
    {
        // Start by flattening our player/period combo into a temporary tale.
        $dbh = DB::connection('mysql_sports');
        $dbh->statement('DROP TEMPORARY TABLE IF EXISTS tmp_entities;');
        $dbh->statement("CREATE TEMPORARY TABLE tmp_entities (
            entity_ref VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
            player_id SMALLINT(5) UNSIGNED NOT NULL,
            period_ref VARCHAR(10) COLLATE latin1_general_ci,
            period_id TINYINT(3) UNSIGNED NULL,
            period_date DATE DEFAULT NULL,
            PRIMARY KEY (entity_ref)
        ) ENGINE = MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");
        foreach ($player_ids as $player_id => $periods) {
            foreach (array_unique((is_array($periods) ? $periods : [$periods])) as $period) {
                $args = ["$player_id:$period", $player_id, $period, $period, $period];
                $dbh->insert('INSERT INTO tmp_entities VALUES (?, ?, ?, CAST(? AS UNSIGNED), CAST(? AS DATE));', $args);
            }
        }
        // Then start our base query to extend out from this.
        $class = FrameworkConfig::get("debear.sports.$sport.ind.class");
        $t = Namespaces::callStatic($sport, $class, 'getTable');
        $key = Namespaces::callStatic($sport, $class, 'getPrimaryKey');
        $query = Namespaces::callStatic($sport, $class, 'query', ['tmp_entities'])
            ->selectRaw('? AS sport', [$sport])
            ->selectRaw('? AS season', [$season])
            ->selectRaw('"ind" AS type')
            ->selectRaw("tmp_entities.entity_ref")
            ->selectRaw("tmp_entities.player_id AS link_id")
            ->selectRaw("tmp_entities.player_id AS $key")
            ->selectRaw("CONCAT($t.first_name, ' ', $t.surname) AS name")
            ->join($t, "$t.$key", '=', 'tmp_entities.player_id');
        // Customise the query to add icon and info.
        static::loadPlayersInfo($query, $sport, $season, $player_ids);
        // Run and process.
        $players_model = $query->groupBy('tmp_entities.entity_ref')->get();
        $players = $players_model->getData();
        array_walk($players, function (&$player) use ($players_model, $key) {
            $player['model'] = $players_model->where('link_id', $player['link_id']);
        });
        return $players;
    }
}
