<?php

namespace DeBear\Helpers\Fantasy;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Profile
{
    /**
     * Standardised formatting of the profile rating score
     * @param float $score The score to be formatted.
     * @return string The formatted score
     */
    public static function formatRating(float $score): string
    {
        return sprintf('%0.01f%s', $score, FrameworkConfig::get('debear.setup.profile.unit'));
    }

    /**
     * Determine the profile level for the given score
     * @param float $score The profile score to be processed.
     * @return array The combination of stars and label
     */
    public static function processProfileScore(float $score): array
    {
        $ret = [];
        $stars = 0;
        foreach (FrameworkConfig::get('debear.setup.profile.levels') as $level => $label) {
            $stars++;
            if ($score <= $level) {
                $ret = ['stars' => $stars, 'label' => $label];
                break;
            }
        }
        return $ret;
    }

    /**
     * Display the appropriate rating level for a provided score
     * @param float $score The score to be grouped and level rendered.
     * @return string The formatted rating level for the provided score
     */
    public static function formatRatingLevel(float $score): string
    {
        $rating = static::processProfileScore($score);
        $count = count(FrameworkConfig::get('debear.setup.profile.levels'));
        return str_repeat('<i class="fas fa-star"></i>', $rating['stars'])
            . str_repeat('<i class="far fa-star"></i>', $count - $rating['stars'])
            . " {$rating['label']}";
    }
}
