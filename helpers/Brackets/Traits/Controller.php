<?php

namespace DeBear\Helpers\Fantasy\Brackets\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\Helpers\Fantasy\Brackets\Game;
use DeBear\ORM\Fantasy\Brackets\Entry as EntryModel;
use DeBear\ORM\Sports\MajorLeague\PlayoffSeries;
use DeBear\ORM\Sports\MajorLeague\Groupings;
use DeBear\Repositories\InternalCache;
use DeBear\Repositories\Time;
use Illuminate\Http\Response;

trait Controller
{
    /**
     * Object containing game setup information
     * @var Game
     */

    protected Game $game;
    /**
     * Validate the request we have been passed, and continue with preparatory steps if valid
     * @param integer $season The requested season being viewed.
     * @return Response|null A response upon validation error, or null on success
     */
    protected function validateAndPrepare(int $season): ?Response
    {
        $range = FrameworkConfig::get('debear.setup.season');
        $dates = FrameworkConfig::get('debear.dates');
        $now = Time::object()->getNowFmt();
        if (
            $season >= $range['min']
            && ($season < $range['max'] || ($season == $range['max'] && $now >= $dates['visible']))
        ) {
            // Valid season passed. Perform the internal object setup.
            $this->internalSetup($season);
            return null;
        }
        // Return a Not Found error code.
        return HTTP::setStatus(404, view('errors.404'));
    }

    /**
     * Perform some internal setting up of config and objects to be used within the game
     * @param integer $season The requested season being viewed.
     * @return void
     */
    protected function internalSetup(int $season): void
    {
        // Standardise the accessor for the Sports site's setup config.
        $sport = FrameworkConfig::get('debear.setup.sport_ref');
        // Determine which version of the template applies.
        $templates = FrameworkConfig::get('debear.setup.templates');
        $ref = array_reduce(array_keys($templates), function ($carry, $ref) use ($season) {
            $tmpl = FrameworkConfig::get("debear.setup.templates.$ref");
            return (($tmpl['from'] <= $season) && ($tmpl['to'] >= $season)) ? $ref : $carry;
        });
        // Load the game setup object for this game.
        $this->game = new Game($sport, $season);
        // Write back the config changes.
        $series = FrameworkConfig::get("debear.sports.subsites.$sport.setup.playoffs.series");
        FrameworkConfig::set([
            'debear.section.endpoint_raw' => FrameworkConfig::get('debear.section.endpoint'),
            'debear.section.endpoint' => FrameworkConfig::get('debear.section.endpoint') . "/$season",
            'debear.setup.season.viewing' => $season,
            'debear.setup.sport' => FrameworkConfig::get("debear.sports.subsites.$sport.setup"),
            'debear.setup.template' => array_merge(
                ['ref' => $ref],
                FrameworkConfig::get("debear.setup.templates.$ref")
            ),
            'debear.setup.playoffs.series' => $series,
        ]);
        // Parse the series details from the sports config, and write back to the config.
        $series = Namespaces::callStatic($sport, 'PlayoffSeries', 'parseConfig', [$season]);
        FrameworkConfig::set(['debear.setup.playoffs' => $series]);
    }

    /**
     * Get the current user's entry
     * @return EntryModel An ORM object representing the user's entry
     */
    protected function getUserEntry(): EntryModel
    {
        $entry = InternalCache::object()->get('fantasy.brackets.entry');
        if (!is_object($entry)) {
            $entry = EntryModel::load();
            InternalCache::object()->set('fantasy.brackets.entry', $entry);
        }
        return $entry;
    }

    /**
     * Get the list of series we already have configured within the database
     * @return PlayoffSeries An ORM object of series / matchups
     */
    protected function getKnownSeries(): PlayoffSeries
    {
        $date_col = FrameworkConfig::get('debear.setup.sport.playoffs.date_col');
        $max_dates = Namespaces::callStatic(FrameworkConfig::get('debear.setup.sport_ref'), 'PlayoffSeries', 'query')
            ->select(['season', 'round_code'])
            ->selectRaw("MAX($date_col) AS max_date")
            ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
            ->groupBy('season')
            ->groupBy('round_code')
            ->get();
        // Now we know the most appropriate database row per series, get the list.
        $sport_ref = FrameworkConfig::get('debear.setup.sport_ref');
        $tbl_series = Namespaces::callStatic($sport_ref, 'PlayoffSeries', 'getTable');
        $tbl_seeds = Namespaces::callStatic($sport_ref, 'PlayoffSeeds', 'getTable');
        $tbl_tmgrp = Namespaces::callStatic($sport_ref, 'TeamGroupings', 'getTable');
        $col_score = FrameworkConfig::get('debear.setup.series_score_col');
        $query = Namespaces::callStatic($sport_ref, 'PlayoffSeries', 'query', [$tbl_series])
            ->select("$tbl_series.round_code")
            ->selectRaw("SUBSTRING(LPAD($tbl_series.round_code, 2, '0'), 1, 1) AS round_num")
            ->selectRaw('SEED_HI.team_id AS higher_seed')
            ->selectRaw('GROUP_HI.grouping_id AS higher_div_id')
            ->selectRaw('SEED_HI.pos_div AS higher_div_seed')
            ->selectRaw('SEED_HI.conf_id AS higher_conf_id')
            ->selectRaw('SEED_HI.pos_conf AS higher_conf_seed')
            ->selectRaw("$tbl_series.higher_{$col_score} AS higher_score")
            ->selectRaw('SEED_LO.team_id AS lower_seed')
            ->selectRaw('GROUP_LO.grouping_id AS lower_div_id')
            ->selectRaw('SEED_LO.pos_div AS lower_div_seed')
            ->selectRaw('SEED_LO.conf_id AS lower_conf_id')
            ->selectRaw('SEED_LO.pos_conf AS lower_conf_seed')
            ->selectRaw("$tbl_series.lower_{$col_score} AS lower_score")
            ->leftJoin("$tbl_seeds AS SEED_HI", function ($join) use ($tbl_series) {
                $join->on('SEED_HI.season', '=', "$tbl_series.season")
                    ->on('SEED_HI.conf_id', '=', "$tbl_series.higher_conf_id")
                    ->on('SEED_HI.seed', '=', "$tbl_series.higher_seed");
            })->leftJoin("$tbl_tmgrp AS GROUP_HI", function ($join) {
                $join->on('GROUP_HI.team_id', '=', 'SEED_HI.team_id')
                    ->whereRaw('SEED_HI.season BETWEEN GROUP_HI.season_from AND IFNULL(GROUP_HI.season_to, 2099)');
            })->leftJoin("$tbl_seeds AS SEED_LO", function ($join) use ($tbl_series) {
                $join->on('SEED_LO.season', '=', "$tbl_series.season")
                    ->on('SEED_LO.conf_id', '=', "$tbl_series.lower_conf_id")
                    ->on('SEED_LO.seed', '=', "$tbl_series.lower_seed");
            })->leftJoin("$tbl_tmgrp AS GROUP_LO", function ($join) {
                $join->on('GROUP_LO.team_id', '=', 'SEED_LO.team_id')
                    ->whereRaw('SEED_LO.season BETWEEN GROUP_LO.season_from AND IFNULL(GROUP_LO.season_to, 2099)');
            });
        foreach ($max_dates as $series) {
            $query->orWhere(function ($where) use ($series, $date_col, $tbl_series) {
                $where->where("$tbl_series.season", '=', $series->season)
                    ->where("$tbl_series.$date_col", '=', $series->max_date)
                    ->where("$tbl_series.round_code", '=', $series->round_code);
            });
        }
        return $query->orderBy("$tbl_series.round_code")->get();
    }

    /**
     * Get the current, and ordered, list of conferences and divisions
     * @return Groupings An ORM object of conferences and divisions
     */
    protected function getGroupings(): Groupings
    {
        // Get the list of raw IDs.
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $sport_ref = FrameworkConfig::get('debear.setup.sport_ref');
        $tbl_tg = Namespaces::callStatic($sport_ref, 'TeamGroupings', 'getTable');
        $tbl_g = Namespaces::callStatic($sport_ref, 'Groupings', 'getTable');
        $ids = Namespaces::callStatic($sport_ref, 'TeamGroupings', 'query', ["$tbl_tg"])
            ->selectRaw("$tbl_tg.grouping_id AS div_id")
            ->selectRaw("$tbl_g.parent_id AS conf_id")
            ->join($tbl_g, "$tbl_g.grouping_id", '=', "$tbl_tg.grouping_id")
            ->whereRaw("? BETWEEN $tbl_tg.season_from AND IFNULL($tbl_tg.season_to, 2099)", [$season])
            ->groupBy("$tbl_tg.grouping_id")
            ->get();
        return Namespaces::callStatic($sport_ref, 'Groupings', 'query')
            ->select(['grouping_id', 'type', 'name', 'name_full', 'parent_id', 'icon'])
            ->whereIn('grouping_id', array_merge($ids->unique('div_id'), $ids->unique('conf_id')))
            ->orderBy('type')
            ->orderBy('parent_id')
            ->orderBy('order')
            ->get();
    }
}
