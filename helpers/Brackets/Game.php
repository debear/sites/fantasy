<?php

namespace DeBear\Helpers\Fantasy\Brackets;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Fantasy\Brackets\SetupDates;
use DeBear\Repositories\Time;

class Game
{
    /**
     * ORM object of dynamically calculated game dates
     * @var SetupDates
     */
    protected SetupDates $dates;
    /**
     * Cached status string
     * @var string
     */
    protected string $status;

    /**
     * Perform the steps required to 'load' the game into memory
     * @param string  $sport  Sport we are loading.
     * @param integer $season Season of the game we are loading.
     * @return void
     */
    public function __construct(string $sport, int $season)
    {
        $this->dates = SetupDates::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
            ])->get();
    }

    /**
     * Based on the combination of static and dynamic dates, return a status for this game
     * @return string Code describing the current game status
     */
    public function getStatus(): string
    {
        $now = Time::object()->getNowFmt();
        $static = FrameworkConfig::get('debear.dates');
        if ($this->dates->season < FrameworkConfig::get('debear.setup.season.max') || $now >= $static['hide']) {
            // An archived verrsion of an old game.
            $status = 'archive';
        } elseif ($now < $static['visible']) {
            // Before the game is visible.
            $status = 'hidden';
        } elseif ($now < $this->dates->reg_end->toDateTimeString()) {
            // Before the seeding is finalised.
            $status = 'pending-seeding';
        } elseif ($now < $this->dates->po_start->toDateTimeString()) {
            // Before the game closes for entries.
            $status = 'before-playoffs';
        } elseif (!isset($this->dates->po_end) || $now < $this->dates->po_end->toDateTimeString()) {
            // Before the playoffs are complete.
            $status = 'during-playoffs';
        } else {
            // After the playoffs have been completed.
            $status = 'after-playoffs';
        }
        return $status;
    }

    /**
     * Return whether the game is from a prior season
     * @return boolean That the game is an archived version of a previous season / playoffs
     */
    public function isArchive(): bool
    {
        return $this->getStatus() == 'archive';
    }

    /**
     * Return whether the game is available
     * @return boolean That the game is unavailable
     */
    public function isHidden(): bool
    {
        return $this->getStatus() == 'hidden';
    }

    /**
     * Return whether the game is open, but waiting for final playoff seeding
     * @return boolean That the playoff seeding is not finalised
     */
    public function isPendingFinalisedSeeds(): bool
    {
        return $this->getStatus() == 'pending-seeding';
    }

    /**
     * Return whether the game is in the period between regular season and playoffs
     * @return boolean That the playoffs are ready to start, but have not yet begun
     */
    public function isBeforePlayoffsStart(): bool
    {
        return $this->getStatus() == 'before-playoffs';
    }

    /**
     * Return whether the game is no longer open for entries, but the playoffs are not yet complete
     * @return boolean That the playoffs are on-going
     */
    public function isDuringPlayoffs(): bool
    {
        return $this->getStatus() == 'during-playoffs';
    }

    /**
     * Return whether the game is still available, but the playoffs have completed
     * @return boolean That the playoffs have now finished
     */
    public function isAfterPlayoffsCompleted(): bool
    {
        return $this->getStatus() == 'after-playoffs';
    }

    /**
     * Return whether the game is at a point where new users can register new entries
     * @return boolean That a (logged in and unregistered) user can register
     */
    public function canRegister(): bool
    {
        return in_array($this->getStatus(), ['pending-seeding', 'before-playoffs']);
    }

    /**
     * Return whether the game is at a point where users can make their selections
     * @return boolean That a (logged in and registered) user can make their selections
     */
    public function canMakeSelections(): bool
    {
        return in_array($this->getStatus(), ['before-playoffs']);
    }
}
