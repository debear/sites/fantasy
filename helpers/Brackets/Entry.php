<?php

namespace DeBear\Helpers\Fantasy\Brackets;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait Entry
{
    /**
     * Set additional data fields to be used in the email sent to a user upon entry creation
     * @param array $args The arguments we passed to the email processing. (Pass-by-Reference).
     * @return void
     */
    public static function createEmailAdditionalData(array &$args): void
    {
        // Email type comes from the form data.
        $args['type'] = ($args['data']['email_type'] ?? 'html');
        $args['opt_out'] = !boolval($args['data']['email_status']);

        // Load the game info to be parsed within the email.
        $sport = FrameworkConfig::get('debear.setup.sport_ref');
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $args['data']['game'] = new Game($sport, $season);
    }

    /**
     * Actions to perform when the user's entry was just created
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function entryFormCreated(array $data): void
    {
        session()->flash('fantasy.brackets.entry.message', 'created &ndash; now start making your '
            . 'selections!');
        // Upon creation of an entry, take the user back to the homepage.
        $endpoint = FrameworkConfig::get('debear.section.endpoint');
        session()->flash('fantasy.brackets.entry-form.redirect', "/$endpoint");
    }

    /**
     * Actions to perform when the user's entry was just updated
     * @param array $data The full array of form data and objects.
     * @return void
     */
    public static function entryFormUpdated(array $data): void
    {
        session()->flash('fantasy.brackets.entry.message', 'updated.');
        // Upon editing an entry, return to the form.
        session()->flash('fantasy.brackets.entry-form.redirect', false);
    }
}
