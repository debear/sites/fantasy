<?php

namespace DeBear\Helpers\Fantasy;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sitemap;
use DeBear\Repositories\Time;

class Homepage
{
    /**
     * Determine how we should group the sports we cover
     * @return array The list of sports, and their games, grouped be where they are in their season
     */
    public static function getSportGroups(): array
    {
        $sport_games = static::getSportGames();
        $ret = array_fill_keys(['active', 'pre-season', 'off-season'], []);
        foreach ($sport_games as $sport => $games) {
            $by_status = array_count_values(array_column($games, 'status'));
            foreach (array_keys($ret) as $ret_status) {
                if ($by_status[$ret_status] ?? 0) {
                    $ret[$ret_status][$sport] = $games;
                    continue;
                }
            }
        }
        return array_filter($ret);
    }

    /**
     * Determine the list of games available for each sport we cover
     * @return array The list of latest games, grouped by their sport
     */
    protected static function getSportGames(): array
    {
        $ret = [];
        $now = Time::object()->getNowFmt();
        foreach (FrameworkConfig::get('debear.subsites') as $site => $config) {
            // We only want to include active games, not legacy games no longer available.
            if (!isset($config['dates'])) {
                continue;
            }
            // Standardise calculation components.
            $enabled = ($config['enabled'] ?? false);
            $visible = ($config['dates']['visible'] <= $now) && ($now <= $config['dates']['hide']);
            if (!$enabled || !$visible) {
                continue;
            }
            // Break down the per-sport games.
            if (isset($config['games']) && is_callable($config['games']['homepage_worker']) ?? false) {
                $games = call_user_func_array($config['games']['homepage_worker'], []);
            } else {
                list($site, $sport) = explode('-', $site);
                $games = [[
                    'sport' => $sport,
                ]];
            }
            foreach ($games as $game) {
                if (!isset($ret[$game['sport']])) {
                    $ret[$game['sport']] = [];
                }
                $ret[$game['sport']][] = array_merge(['code' => $site], $game);
            }
        }
        return $ret;
    }

    /**
     * Perform Fantasy-specific post-processing of the subsites
     * @param array $sitemap The sitemap array, passed-by-reference.
     * @return void
     */
    public static function processSubsites(array &$sitemap): void
    {
        $sitemap = Sitemap::parseSubsites($sitemap);
        foreach (array_keys($sitemap['subsites']) as $key) {
            $sitemap['subsites'][$key]['config-ref'] = preg_replace('/^subsite-/', '', $key);
            $config = FrameworkConfig::get("debear.subsites.{$sitemap['subsites'][$key]['config-ref']}");
            // Determine any sub-games within this subsite.
            $has_worker = isset($config['games']) && is_callable($config['games']['sitemap_worker'] ?? false);
            $sitemap['subsites'][$key]['sub-games'] = $has_worker
                ? call_user_func_array($config['games']['sitemap_worker'], [])
                : [];
        }
    }
}
