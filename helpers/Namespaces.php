<?php

namespace DeBear\Helpers\Fantasy;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Namespaces
{
    /**
     * Determine the full class name for the requested class
     * @param string $sport The sport we want to load.
     * @param string $class Class name we are trying to create a static object for.
     * @return string The fully qualified class name of the sport-specific class
     */
    public static function fullClassName(string $sport, string $class): string
    {
        $config = FrameworkConfig::get("debear.sports.$sport");
        $section = $config['sports_class'] ?? $config['section'];
        $namespace = $config['namespace'];
        return "\\DeBear\\ORM\\Sports\\$namespace\\$section\\$class";
    }

    /**
     * Wrapper to calling the appropriate static method of a Sports class
     * @param string $sport  The sport we want to load.
     * @param string $class  Class name we are trying to create a static object for.
     * @param string $method The method to call on the static class.
     * @param array  $args   Arguments to pass the static method. (Optional).
     * @return mixed Whatever was returned by the static method
     */
    public static function callStatic(string $sport, string $class, string $method, array $args = []): mixed
    {
        return call_user_func_array([static::fullClassName($sport, $class), $method], $args);
    }
}
