@if ($data['game']->isPendingFinalisedSeeds())
    The regular season isn&#39;t quite finished, but that doesn&#39;t mean it&#39;s too early to make your choices.
@elseif ($data['game']->isBeforePlayoffsStart())
    The bracket is set. Each team is planning their route to the {!! FrameworkConfig::get('debear.setup.playoffs.trophy') !!} &ndash; time to say who you think makes it all the way, and who falls short.
@else {{-- $data['game']->isDuringPlayoffs() --}}
    The playoffs are underway, but there&#39;s still a little time left to get your selections in.
@endif
