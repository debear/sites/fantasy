<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta property="og:title" content="{!! $email->email_subject !!}" />
    <title>{!! $email->email_subject !!}</title>
    <style type="text/css">
      /* Client-specific Styles */
      #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
      body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
      body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

      /* Reset Styles */
      body{margin:0; padding:0;}
      img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
      table td{border-collapse:collapse;}
      #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

      /* Template Styles */
      body, #backgroundTable{
        background-color:#FAFAFA;
      }

      #templateContainer{
        border: 1px solid #DDDDDD;
      }

      h1, .h1{
        color:#202020;
        display:block;
        font-family:Arial;
        font-size:34px;
        font-weight:bold;
        line-height:100%;
        margin-top:0;
        margin-right:0;
        margin-bottom:10px;
        margin-left:0;
        text-align:left;
      }

      h2, .h2{
        color:#202020;
        display:block;
        font-family:Arial;
        font-size:30px;
        font-weight:bold;
        line-height:100%;
        margin-top:0;
        margin-right:0;
        margin-bottom:10px;
        margin-left:0;
        text-align:left;
      }

      h3, .h3{
        color:#202020;
        display:block;
        font-family:Arial;
        font-size:26px;
        font-weight:bold;
        line-height:100%;
        margin-top:0;
        margin-right:0;
        margin-bottom:10px;
        margin-left:0;
        text-align:left;
      }

      h4, .h4{
        color:#202020;
        display:block;
        font-family:Arial;
        font-size:22px;
        font-weight:bold;
        line-height:100%;
        margin-top:0;
        margin-right:0;
        margin-bottom:10px;
        margin-left:0;
        text-align:left;
      }

      #templatePreheader{
        background-color:#FAFAFA;
      }

      .preheaderContent div{
        color:#505050;
        font-family:Arial;
        font-size:10px;
        line-height:100%;
        text-align:left;
      }

      .preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
        color:#336699;
        font-weight:normal;
        text-decoration:underline;
      }

      #templateHeader{
        background-color:#FFFFFF;
        border-bottom:0;
      }

      .headerContent{
        color:#202020;
        font-family:Arial;
        font-size:34px;
        font-weight:bold;
        line-height:100%;
        padding:0;
        text-align:center;
        vertical-align:middle;
      }

      .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
        color:#336699;
        font-weight:normal;
        text-decoration:underline;
      }

      #headerImage{
        height:auto;
        max-width:600px !important;
      }

      #templateContainer, .bodyContent{
        background-color:#FFFFFF;
      }

      .bodyContent div{
        color:#505050;
        font-family:Arial;
        font-size:14px;
        line-height:125%;
        text-align:left;
      }
      .bodyContent div p{
        margin-top:0px;
      }

      .bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
        color:#336699;
        font-weight:normal;
        text-decoration:underline;
      }

      .bodyContent img{
        display:inline;
        height:auto;
      }

      #templateFooter{
        background-color:#FFFFFF;
        border-top:0;
      }

      .footerContent div{
        color:#707070;
        font-family:Arial;
        font-size:12px;
        line-height:125%;
        text-align:left;
      }

      .footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
        color:#336699;
        font-weight:normal;
        text-decoration:underline;
      }

      .footerContent img{
        display:inline;
      }

      #utility{
        background-color:#FFFFFF;
        border:0;
      }

      .track_box {
        background-color:#dddddd;
      }
      .track_box th{
        background-color:#999999;
      }
      .shale_box {
        background-color:#ffe4c4;
      }
      .shale_box th{
        background-color:#d2691e;
      }
      .grass_box {
        background-color:#bfffbf;
      }
      .grass_box th{
        background-color:#50c878;
      }
      .ice_box {
        background-color:#bfefff;
      }
      .ice_box th{
        background-color:#72a0c1;
      }

      .game_box, .game_box div {
        color: #333333;
      }
      .game_box .descrip div{
        font-size: 12px;
        margin:3px;
      }
      .game_box .link div, .game_box .deadline div{
        margin-top:0;
        margin-right:3px;
        margin-bottom:3px;
        margin-left:3px;
      }
      .game_box .deadline div{
        text-align:right;
      }
    </style>
  </head>
  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    <center>
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
        <tr>
          <td align="center" valign="top">
            <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
              <tr>
                <td valign="top" class="preheaderContent">
                  <table border="0" cellpadding="10" cellspacing="0" width="100%">
                    <tr>
                      <td valign="top">
                        <div>
                          Your {!! FrameworkConfig::get('debear.names.section') !!} entry has been created
                        </div>
                      </td>
                      <td valign="top" width="190">
                        <div>
                          Is this email not displaying correctly?<br /><a href="https:{!! HTTP::buildDomain() !!}/email-{!! $email->email_code !!}-{!! $email->code_checksum1 !!}-{!! $email->code_checksum2 !!}" target="_blank">View it in your browser</a>.
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                    <tr>
                      <td class="headerContent">
                        <img src="https:{!! HTTP::buildStaticURLs('images', 'logos/email_' . FrameworkConfig::get('debear.section.endpoint_raw') . '.png') !!}" logo alt="{!! FrameworkConfig::get('debear.names.external') !!} Logo" width="600" height="45" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                    <tr>
                      <td valign="top" class="bodyContent">
                        <table border="0" cellpadding="10" cellspacing="0" width="100%">
                          <tr>
                            <td valign="top">
                              <div>
                                <!-- Begin Text Content -->
                                Hi {!! $user->forename !!},<br /><br />
                                Congratulations for creating your {!! FrameworkConfig::get('debear.names.section') !!} entry called <em><a href="{!! 'https:' . HTTP::buildDomain('fantasy') . '/' . FrameworkConfig::get('debear.section.endpoint') !!}" target="_blank">{!! $data['name'] !!}</a></em>.
                                @include('email.fantasy.brackets.entry.create.message')<br /><br />
                                Good luck with your bracket throughout the {!! FrameworkConfig::get('debear.setup.sport.playoffs.title') !!}!<br /><br />
                                The {!! FrameworkConfig::get('debear.names.site') !!} Team<br />
                                  -- <a href="{!! 'https:' . HTTP::buildDomain('fantasy') . '/' . FrameworkConfig::get('debear.section.endpoint') !!}" target="_blank">{!! 'https:' . HTTP::buildDomain('fantasy') . '/' . FrameworkConfig::get('debear.section.endpoint') !!}</a>
                                <!-- End Text Content -->
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                    <tr>
                      <td valign="top" class="footerContent">
                        <!-- Begin Standard Footer -->
                        <table border="0" cellspacing="0" width="100%">
                          <tr>
                            <td valign="top" width="350">
                              <div>
                                <em>Copyright &copy; 2003&ndash;{!! $data['date_year'] !!} {!! FrameworkConfig::get('debear.names.company') !!}, All rights reserved.</em>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign="middle" id="utility">
                              <div>
                                You received this email because you chose to receive notifications via email for {!! FrameworkConfig::get('debear.names.external') !!}. To stop receiving these notifications, <a href="{!! 'https:' . HTTP::buildDomain('fantasy') . '/' . FrameworkConfig::get('debear.section.endpoint') . '/my-entry' !!}" target="_blank">update your subscription preferences</a>.
                              </div>
                            </td>
                          </tr>
                        </table>
                        <!-- End Standard Footer -->
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <br />
          </td>
        </tr>
      </table>
    </center>
  </body>
</html>
