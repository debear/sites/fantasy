Here are some of the current records being challenged:<br /><br />
@foreach ($data['open_games'] as $game)
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="game_box {!! FrameworkConfig::get("debear.sports.{$game->sport}.box") !!}_box">
      <tr>
        <th valign="middle" width="34" height="18">
          <img src="https:{!! HTTP::buildCDNURLs("/fantasy/records/email/{$game->sport}.png") !!}" logo alt="{!! FrameworkConfig::get("debear.sports.{$game->sport}.name_short") !!} Logo" width="34" height="18" />
        </th>
        <th align="center" colspan="2" valign="middle" width="566" height="18">{!! $game->name !!}</th>
      </tr>
      <tr>
        <td valign="top" colspan="3" class="descrip">
          <div><em>{!! $game->description !!}</em></div>
        </td>
      </tr>
      <tr>
        <td valign="middle" align="left" colspan="2" width="200" class="link">
          <div><a href="{!! 'https:' . HTTP::buildDomain('fantasy') . $game->full_url !!}" target="_blank">&raquo; Go to Game Page</a></div>
        </td>
        <td valign="middle" align="right" width="400" class="deadline">
          <div><strong>Closes for entries:</strong> {!! $game->date_close->format('jS F') !!}</div>
        </td>
      </tr>
    </table><br />
@endforeach
