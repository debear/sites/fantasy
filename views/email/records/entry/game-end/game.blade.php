<table border="0" cellpadding="0" cellspacing="0" width="100%" class="game_box game_{!! $game->game_ref !!} {!! FrameworkConfig::get("debear.sports.{$game->sport}.box") !!}_box">
  <tr><th valign="middle" width="34" height="18"><img src="https:{!! HTTP::buildCDNURLs("/fantasy/records/email/{$game->sport}.png") !!}" logo alt="{!! FrameworkConfig::get("debear.sports.{$game->sport}.name_short") !!} Logo" width="34" height="18" /></th><th align="center" colspan="3" valign="middle" width="566" height="18">{!! $game->name !!}</th></tr>
  <tr><td class="field" valign="middle" align="right" width="110"><!-- to>&ndash; <eoto --><strong>Target:</strong></td><td><div class="bar target">&nbsp;</div></td><td class="value" valign="middle" width="125"><strong>{!! $game->target !!}</strong></td></tr>
  <tr><td class="field" valign="middle" align="right"><!-- to>&ndash; <eoto --><strong>Leader:</strong></td><td><div class="bar leader">&nbsp;</div></td><td class="value" valign="middle"><strong>{!! $game->leader_res !!}</strong></td></tr>
  <tr><td class="field" valign="middle" align="right"><!-- to>&ndash; <eoto --><strong>Your Best:</strong></td><td><div class="bar overall">&nbsp;</div></td><td class="value" valign="middle"><strong>{!! $entry_score->overall_res !!}</strong> &ndash; <em>{!! Format::ordinal($entry_score->overall_pos) !!}</em></td></tr>
@if($game->hasHitRateLeaderboard())
  <tr><td class="field" valign="middle" align="right"><!-- to>&ndash; <eoto --><strong>Success Rate:</strong></td><td><div class="bar hitrate">&nbsp;</div></td><td class="value" valign="middle"><strong>{!! $entry_score->hitrate_res !!}%</strong> &ndash; <em>{!! Format::ordinal($entry_score->hitrate_pos) !!}</em></td></tr>
@endif
@isset($entry_score->stress)
  <tr><td class="field" valign="middle" align="right"><!-- to>&ndash; <eoto --><strong>Stress Score:</strong></td><td><div class="bar stress">&nbsp;</div></td><td class="value" valign="middle"><strong>{!! $entry_score->stress_pct !!}%</strong></td></tr>
@endisset
</table><br />
<style type="text/css">
  .game_box.game_{!! $game->game_ref !!} .bar.target { width: 100%; }
  .game_box.game_{!! $game->game_ref !!} .bar.leader { width: {!! sprintf('%0.02f', min(100, ($game->leader_raw / $game->target) * 100)) !!}%; }
  .game_box.game_{!! $game->game_ref !!} .bar.overall { width: {!! sprintf('%0.02f', min(100, ($entry_score->overall_raw / $game->target) * 100)) !!}%; }
@if($game->hasHitRateLeaderboard())
  .game_box.game_{!! $game->game_ref !!} .bar.hitrate { width: {!! $entry_score->hitrate_res !!}%; }
@endif
@isset($entry_score->stress)
  .game_box.game_{!! $game->game_ref !!} .bar.stress { width: {!! sprintf('%0.02f', $entry_score->stress_pct) !!}%; }
@endisset
</style>
