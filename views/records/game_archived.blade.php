@extends('skeleton.layouts.html')

@section('content')

@includeWhen(count($entry?->games_active ?? []) > 1, 'fantasy.records.game.other')

<h1 class="sports_{!! $game->sport !!}_icon">{!! $game->name !!}</h1>

@include('fantasy.records.game.progress_bars')
@include('fantasy.records.game.leaderboard')
@includeWhen($game?->achievements?->isset() ?? false, 'fantasy.records.game.achievements')
@include('fantasy.records.game.rules')

@endsection
