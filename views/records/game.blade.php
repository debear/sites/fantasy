@php
    $attrib = [
        'ref' => $game->game_ref,
        'key' => substr(md5($game?->integration_updated ?? -1), 0, 8),
        'cache_ttl_sel' => FrameworkConfig::get("debear.setup.js_cache_ttl.sel"),
        'cache_ttl_info' => FrameworkConfig::get("debear.setup.js_cache_ttl.info"),
    ];
    $data_attrib = '';
    foreach ($attrib as $key => $value) {
        $data_attrib .= " data-$key=\"$value\"";
    };
@endphp
@extends('skeleton.layouts.html')

@section('content')

@includeWhen(count($entry?->games_active ?? []) > 1, 'fantasy.records.game.other')

<h1 class="sports_{!! $game->sport !!}_icon"{!! $data_attrib !!}>{!! $game->name !!}</h1>

@each('fantasy._widgets.news', $game->news, 'announcement')
@includeWhen(($game?->integration_status ?? 'okay') != 'okay', 'fantasy.records.game.integration_error')
@includeWhen(isset($user_action), "fantasy.records.common.actions.$user_action")
@includeWhen(!isset($entry) && $game->isClosingSoon(), 'fantasy.records.game.closing')

@include('fantasy.records.game.progress_bars')

@php
    $inc_last = isset($entry?->score) && $periods['last']->isset();
    $inc_curr = isset($entry?->score) && $periods['curr']->isset();
    $num_sel = intval($inc_last) + intval($inc_curr);
    $grid_sel = "grid-1-1 grid-tl-1-$num_sel grid-tp-1-1 grid-m-1-1";
@endphp
<ul class="inline_list grid sel-{!! $num_sel !!}">
    {{-- Achievements Attained --}}
    @if ($summary->isset() && ($entry?->score?->achievements_chg ?? 0) > 0)
        <li class="grid-1-1">@include('fantasy.records.game.achievements_attained')</li>
    @endif
    {{-- Active Limit: Warning --}}
    @if (($entry?->dropoff?->score0 ?? 0) > 0)
        <li class="grid-1-1">@include('fantasy.records.game.dropoff_warning', ['dropoff' => $entry?->dropoff])</li>
    @endif
    {{-- Previous / Current Selection --}}
    @if ($inc_last || $inc_curr)
        <li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            @includeWhen($inc_last, 'fantasy.records.game.sel_last', ['period' => $periods['last'], 'inc_opp_beaten' => $game->hasAchievement('opponent') && $curr_editable])
            @includeWhen($inc_curr, 'fantasy.records.game.sel_' . ($curr_editable ? 'curr' : 'last'), ['period' => $periods['curr'], 'inc_opp_beaten' => $game->hasAchievement('opponent') && !$curr_editable])
        </li>
    @endif
    {{-- Leaderboards --}}
    <li class="grid-1-{!! ($inc_last || $inc_curr) ? 2 : 1 !!} grid-t-1-1 grid-m-1-1">@include('fantasy.records.game.leaderboard')</li>
</ul>

{{-- Active Limit: Details --}}
@includeWhen($entry?->dropoff?->isset() ?? false, 'fantasy.records.game.dropoff_block', ['dropoff' => $entry?->dropoff])
{{-- Selection Usage: List --}}
@includeWhen(($entry?->score?->isActive() ?? false) && !$game->canReuseSelection(), 'fantasy.records.game.sel_usage')
{{-- Entry Selection Stats --}}
@includeWhen(isset($entry?->score), 'fantasy.records.game.sel_stats')
{{-- Global Selection Summary --}}
@includeWhen($game->hasPerformanceSummary() && ($summary?->isset() ?? false), 'fantasy.records.game.period_summary')

@includeWhen($game?->achievements?->isset() ?? false, 'fantasy.records.game.achievements')
@include('fantasy.records.game.rules')

@include('fantasy.records.game.modals.selection.view')
@includeWhen($inc_curr, 'fantasy.records.game.modals.selection.edit')
@includeWhen($game->leaderboards['overall']->isset(), 'fantasy.records.game.modals.team.view')
@includeWhen($game->leaderboards['overall']->isset(), 'fantasy.records.game.modals.leaderboard.view')

@endsection
