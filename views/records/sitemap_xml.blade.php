@php
    print '<'.'?xml version="1.0" encoding="UTF-8"?'.'>'; // Quirky to satisfy linting
@endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($games as $game)
        <url>
            <loc>{!! $base_url . $game->section_url !!}</loc>
            <changefreq>{!! !$game->isComplete() ? 'daily' : 'yearly' !!}</changefreq>
            <priority>{!! !$game->isComplete() ? '0.9' : '0.3' !!}</priority>
        </url>
    @endforeach
    <url>
        <loc>{!! $base_url !!}/help</loc>
        <changefreq>monthly</changefreq>
        <priority>0.1</priority>
    </url>
</urlset>
