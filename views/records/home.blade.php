@php
    $group_meta = [
        'upcoming' => ['name' => 'Upcoming', 'description' => 'We&#39;re doing our final tinkering, but these will be ready to play in the next few days!', 'sel' => false, 'progress' => false, 'view' => false],
        'open' => ['name' => 'Available', 'description' => 'These games will be starting shortly, so why not sign up and get ready for the start of the season?', 'sel' => false, 'progress' => false, 'view' => true],
        'inprogress' => ['name' => 'In Progress', 'description' => 'The season may have started, but it&#39;s not too late to get involved!', 'sel' => false, 'progress' => true, 'view' => true],
        'closed' => ['name' => 'In-Season', 'description' => 'Unfortunately it&#39;s too late to join these, but keep an eye on how the current player are getting on!', 'sel' => false, 'progress' => true, 'view' => true],
        'active' => ['name' => 'Current', 'grid-size' => 2, 'grid-tl-size' => 1, 'sel' => true, 'progress' => true, 'view' => true],
        'complete' => ['name' => 'Recently Completed', 'sel' => false, 'progress' => true, 'view' => 'Archive'],
    ];
@endphp
@extends('skeleton.layouts.html')

@section('content')

<h1>Welcome to {!! FrameworkConfig::get('debear.names.section') !!}!</h1>

@each('fantasy._widgets.news', $news, 'announcement')
@includeWhen(isset($user_action), "fantasy.records.common.actions.$user_action")
@includeWhen($message = session('form_complete'), 'fantasy.records.common.actions.success')

@foreach($games->groupByStatus() as $group => $list)
    <h3 class="group">{!! $group_meta[$group]['name'] . ' ' . Format::pluralise($list->count(), 'Game', ['hide-num' => true]) !!}</h3>
    @isset ($group_meta[$group]['description'])
        <p class="group">{!! $group_meta[$group]['description'] !!}</p>
    @endisset
    <ul class="inline_list grid games">
        @foreach($list as $game)
            @php
                $game->configure();
                $inc_selection = $group_meta[$group]['sel'] && isset($entry);
                $inc_progress =  $group_meta[$group]['progress'];
            @endphp
            <li class="grid-1-{!! $group_meta[$group]['grid-size'] ?? 3 !!} grid-tl-1-{!! $group_meta[$group]['grid-tl-size'] ?? 2 !!} grid-tp-1-1 grid-m-1-1">
                <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} game" data-ref="{!! $game->game_ref !!}">
                    <h3 class="invert {!! "sports_{$game->sport}_icon" !!}">{!! $game->name !!}</h3>
                    <p class="descrip">{!! $game->description !!}</p>
                    @if ($inc_selection && $inc_progress)<div class="game-detail">@endif
                    @if ($inc_selection)
                        <div class="sel">@include('fantasy.records.home.widgets.selections')</div>
                    @endif
                    @if ($inc_progress)
                        <div class="progress">@include('fantasy.records.home.widgets.progress_bars')</div>
                    @endif
                    @if ($inc_selection && $inc_progress)</div>@endif
                    @includeIf("fantasy.records.home.actions.$group")
                    @if ($group_meta[$group]['view'])
                        <a class="view cta bottom" href="{!! $game->full_url !!}">View Game {!! is_string($group_meta[$group]['view']) ? $group_meta[$group]['view'] : 'Details' !!}</a>
                    @endif
                </fieldset>
            </li>
            @php
                $game->revertConfigure();
            @endphp
        @endforeach
    </ul>
@endforeach

@endsection
