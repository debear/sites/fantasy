<dl class="other_games inline_list hidden-m">
    <dt><span class="icon icon_right_manage__menu">Your Other Games</span></dt>
    @foreach(array_diff_key($entry->games_active, [$game->game_ref => true]) as $game_ref => $other)
        <dd class="{!! 'row-' . FrameworkConfig::get("debear.sports.{$other->game->sport}.box") !!}"><a href="{!! $other->game->full_url !!}" class="no_underline"><span class="sports_{!! $other->game->sport !!}_icon">{!! $other->game->name !!}</span></a></dd>
    @endforeach
</dl>
