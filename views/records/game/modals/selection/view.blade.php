@php
    $highlights = array_filter([
        'rating' => 'Rating',
        'opp_rating' => $game->isMajorLeague() ? 'Opponent' : false,
    ]);
    foreach ($game->stats as $stat) {
        $highlights[$stat->stat_id] = $stat->title;
        $last_stat_id = $stat->stat_id;
    }
    // Calculate highlight box grid sizes
    $num_boxes = count($highlights);
    $grid_size = intval(ceil($num_boxes / 2));
    $box_grids = array_fill_keys(array_keys($highlights), $grid_size);
    // Handle exceptions for odd-numbered blocks
    if (($num_boxes % 2) != 0) {
        // By default, larger blocks on the top line
        // Except when we have both ratings and a single stat, we want the two ratings on their own line
        $box_keys = array_keys($box_grids);
        if (array_key_exists('opp_rating', $highlights) && $num_boxes == 3) {
            $box_grids[$box_keys[2]] = 1;
        } else {
            for ($i = 0; $i < ($num_boxes - 1) / 2; $i++) {
                $box_grids[$box_keys[$i]] = ($grid_size - 1);
            }
        }
    }
@endphp
<modal class="sel-view hidden" data-recent-rows={!! FrameworkConfig::get('debear.setup.selections.view_modal_history') !!}>
    <cover></cover>
    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} modal-body view-modal {!! $game->isMajorLeague() ? 'major-league' : 'motorsport' !!} {!! $game->sport !!}">
        <h3 class="invert name"></h3>
        <fieldset class="status-msg hidden"></fieldset>
        <selection class="sel-detail"></selection>
        <button class="row btn_small btn_red">Close</button>
    </fieldset>
    <template>
        <mugshot class="cdn-large"></mugshot>
        <ul class="highlights stats-{!! count($highlights) !!} inline_list grid">
            @foreach ($highlights as $id => $title)
                <li class="grid-1-{!! $box_grids[$id] !!}">
                    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!}">
                        <h3 class="invert">{!! $title !!}</h3>
                        <span class="row-0 stat-{!! $id !!}"></span>
                    </fieldset>
                </li>
            @endforeach
        </ul>
        <dl class="history clearfix">
            <dt class="period-info"></dt>
            <dt class="rating row-1">Rating</dt>
            @if ($game->isMajorLeague())
                <dt class="opp_rating row-1">Opponent</dt>
            @endif
            <dt class="summary row-1">Summary</dt>
            <dt class="usage row-1">Usage</dt>
            <row>
                <dt class="period row-1" data-row="${row}"></dt>
                <dd class="period-info row-0" data-row="${row}"></dd>
                <dd class="rating row-0" data-row="${row}"></dd>
                @if ($game->isMajorLeague())
                    <dd class="opp_rating row-0" data-row="${row}"></dd>
                @endif
                <dd class="summary row-0" data-row="${row}"></dd>
                <dd class="usage row-0" data-row="${row}"></dd>
            </row>
        </dl>
    </template>
</modal>
