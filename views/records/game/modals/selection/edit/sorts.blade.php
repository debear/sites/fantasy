@php
    // Add our game stats
    $dropdown_sort = new Dropdown('selection-sort', $stat_list, [
        'value' => 'rating',
        'select' => false,
    ]);
@endphp
<key>Sort By:</key>
<filter class="sort">{!! $dropdown_sort->render() !!}</filter>
