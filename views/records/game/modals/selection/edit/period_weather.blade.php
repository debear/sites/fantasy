@php
    $events = ['quali' => 'Qualifying', 'race' => 'Race', 'race2' => 'Race'];
    $disp = [];
    foreach ($events as $key => $name) {
        if (isset($weather->$key)) {
            $disp[] = $key;
            if ($key == 'race2') {
                $events['race'] = 'Sprint Race'; // If we have two races, the first one is a Sprint.
            }
        }
    }
    $fields = count($disp) + 1;
    $grid = "grid grid-1-$fields";
@endphp
<ul class="inline_list grid period-weather">
    <li class="{!! $grid !!} row-1 title">Weather Forecast</li>
    @foreach ($disp as $key)
        <li class="{!! $grid !!} forecast">
            <strong>{!! $events[$key] !!}:</strong>
            <span class="icon icon_weather_{!! $weather->$key->symbol !!}">{!! $weather->$key->temp !!}&deg;</span>
        </li>
    @endforeach
</ul>
