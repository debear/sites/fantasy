{{-- Mobile display view --}}
@php
    $dropdown_view = new Dropdown('selection-view', $stat_list, [
        'value' => $stat_list[0]['id'],
        'select' => false,
    ]);
@endphp
<view class="hidden-t hidden-d">
    <key>View:</key>
    <filter class="sort">{!! $dropdown_view->render() !!}</filter>
</view>
