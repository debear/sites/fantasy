<toggle class="icon_right icon_right_nav_expand">Filter Selections</toggle>
<filters>
    {{-- Hide Locked --}}
    @if ($game->isMajorLeague() && $game->isSelTypeIndividual())
        <checkbox class="locked">
            <input type="checkbox" id="filter_locked" class="checkbox" checked="checked">
            <label for="filter_locked">Hide Locked Seletions?</label>
        </checkbox>
    @endif

    {{-- Include Beaten Opponents --}}
    @if ($game->hasAchievement('opponent'))
        <checkbox class="beaten-opp">
            <input type="checkbox" id="filter_beaten_opp" class="checkbox">
            <label for="filter_beaten_opp">Hide Beaten Opponents?</label>
        </checkbox>
    @endif

    {{-- Player Team --}}
    @if ($game->isMajorLeague() && $game->isSelTypeIndividual())
        @php
            $icon_prefix = FrameworkConfig::get("debear.sports.{$periods['curr']->sport}.team.icon");
            $teams = $periods['curr']->getTeams();
            array_walk($teams, function (&$t, $team_id) use ($icon_prefix) {
                $t = ['id' => $team_id, 'label' => '<span class="' . $icon_prefix . $team_id. '">' . $team_id . '</span> (' . $t['venue'] . '<span class="opp ' . $icon_prefix . $t['opp_id'] . '">' . $t['opp_id'] . '</span>)'];
            });
            $opt = array_merge([['id' => '_ALL', 'label' => 'All Teams']], $teams);
            $dropdown_team = new Dropdown('selection-team', $opt, [
                'value' => $opt[0]['id'],
                'select' => false,
            ]);
        @endphp
        <key>By Team:</key>
        <filter class="team">{!! $dropdown_team->render() !!}</filter>
    @endif

    {{-- Sorting --}}
    @include('fantasy.records.game.modals.selection.edit.sorts')
</filters>
