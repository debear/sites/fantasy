@php
    $sel_curr = $entry->getSelection($game->game_ref, $periods['curr']->period_id);
    // The list of stats to be used in displaying / sorting
    $stat_list = [];
    foreach ($game->stats as $stat) {
        $stat_list[] = ['id' => $stat->stat_id, 'label' => $stat->title, 'descrip' => $stat->description];
    }
    // And then the standard options
    $stat_list = array_merge(array_filter([
        ['id' => 'rating', 'label' => 'Selection Rating'],
        $game->isMajorLeague() ? ['id' => 'opp_rating', 'label' => 'Opponent Rating'] : false,
    ]), $stat_list);
    // How many icon flags could we display?
    $num_info_icons = intval($game->hasStreakBuster()) + intval($game->hasAchievement('opponent') && isset($entry->score->opponents));
@endphp
<modal class="sel-edit hidden" data-now="{!! \DeBear\Repositories\Time::object()->formatServer('c') !!}" data-period-id="{!! $periods['curr']->period_id !!}" data-max-rows={!! FrameworkConfig::get('debear.setup.selections.edit_modal_rows') !!} data-info-icons="{!! $num_info_icons !!}">
    <cover></cover>
    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} modal-body edit-modal stats-{!! count($stat_list) !!} {!! $sel_curr->isset() ? 'inc-current' : '' !!}" data-mobile="stat-{!! $stat_list[0]['id'] !!}" data-filter="{!! $game->canFilterSelections() ? 'true' : 'false' !!}">
        <h3 class="invert">{!! $periods['curr']->name_disp !!}</h3>
        @includeWhen(isset($periods['curr']->weather), 'fantasy.records.game.modals.selection.edit.period_weather', ['weather' => $periods['curr']->weather])
        @includeWhen($sel_curr->isset(), 'fantasy.records.game.modals.selection.edit.current')
        @include('fantasy.records.game.modals.selection.edit.mobile')
        <selfilters>
            @includeWhen($game->canFilterSelections(), 'fantasy.records.game.modals.selection.edit.filters') {{-- Filters and Sort --}}
            @includeWhen(!$game->canFilterSelections(), 'fantasy.records.game.modals.selection.edit.sorts') {{-- Sort only --}}
        </selfilters>
        <fieldset class="status-msg hidden"></fieldset>
        <options>
            <ul class="inline_list sel-list"></ul>
        </options>
        <pagination class="row-1">
            <a class="page prev">&laquo; Prev</a>
            <num>Showing <span class="num-from"></span> to <span class="num-to"></span> of <span class="num-total"></span></num>
            <a class="page next">Next &raquo;</a>
        </pagination>
        <bottom>
            <deadline class="row invert">
                <span class="icon_padlock_open">Last Selections Lock at {!! $periods['curr']->lock_time->display->format(FrameworkConfig::get("debear.sports.{$game->sport}.period.format")) !!}</span>
            </deadline>
            <button class="row btn_small btn_red">Close</button>
        </bottom>
    </fieldset>
    <template>
        <ele class="mugshot cdn-tiny"></ele>
        <ele class="name row-1"></ele>
        <ele class="info row-1"></ele>
        <ele class="action"></ele>
        <stats>
            <inner>
                <ele class="info row-1"></ele>
                @foreach ($stat_list as $i => $stat)
                    <ele class="stat-{!! $stat['id'] !!} cell-{!! $i !!} stat row-1">{!! isset($stat['descrip']) ? '<abbrev title="' . $stat['descrip'] . '">' : '' !!}{!! $stat['label'] !!}{!! isset($stat['descrip']) ? '</abbrev>' : '' !!}</ele>
                    <ele class="stat-{!! $stat['id'] !!} value row-0" data-stat-id="{!! $stat['id'] !!}"></ele>
                @endforeach
            </inner>
        </stats>
    </template>
    <actions>
        <button class="btn_small btn_red sel-unset"><span class="icon icon_delete">Remove</span></button>
        <button class="btn_small btn_red sel-existing" disabled><span class="icon icon_valid">Selected</span></button>
        <button class="btn_small btn_green sel-save"><span class="icon icon_padlock_open">Locks ${lockTime}</span></button>
        <button class="btn_small btn_blue sel-unavailable" disabled><span class="icon icon_padlock_closed">${reason}</span></button>
        <span class="weather icon icon_weather_${icon}">${temp}</span>
    </actions>
</modal>
