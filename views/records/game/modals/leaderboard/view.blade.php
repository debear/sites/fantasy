<modal class="leader hidden">
    <cover></cover>
    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} modal-body leader-modal">
        <h3 class="invert"><span class="type"></span> Leaderboard after {!! $periods['last']->name_disp !!}</h3>
        <fieldset class="status-msg hidden"></fieldset>
        <dl class="leaderboard clearfix"></dl>
        <bottom>
            <button class="row btn_small btn_red">Close</button>
        </bottom>
    </fieldset>
    <template data-separator="<dd class=&quot;horiz-sep&quot;></dd>">
        <dt class="tmpl-X pos"></dt>
            <dd class="tmpl-X name"></dd>
            <dd class="tmpl-X user"></dd>
            <dd class="tmpl-X score"></dd>
            <dd class="tmpl-X last-sel"></dd>
            <dd class="tmpl-X last-result"></dd>
    </template>
</modal>
