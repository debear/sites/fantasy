@php
    $highlights = ['overall' => 'Overall'];
    if ($game->hasCurrentLeaderboard()) {
        $highlights['current'] = 'Current';
    }
    $grid_size = count($highlights) + 1;
@endphp
<modal class="team-view hidden" data-recent-rows={!! FrameworkConfig::get('debear.setup.selections.team_modal_history') !!}>
    <cover></cover>
    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} modal-body team-modal {!! $game->isMajorLeague() ? 'major-league' : 'motorsport' !!}">
        <h3 class="invert name"></h3>
        <fieldset class="status-msg hidden"></fieldset>
        <em class="row-1 user"></em>
        <ul class="highlights inline_list grid">
            @foreach ($highlights as $section => $title)
                <li class="grid-1-{!! $grid_size !!}">
                    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} highlight-{!! $section !!}">
                        <h3 class="invert">{!! $title !!}</h3>
                        <span class="score score-{!! $section !!}"></span>
                        <span class="row-1 rank rank-{!! $section !!}"></span>
                    </fieldset>
                </li>
            @endforeach
            <li class="grid-1-{!! $grid_size !!}">
                <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} highlight-stress">
                    <h3 class="invert">Stress Score</h3>
                    <bar class="bar stress">
                        <marker></marker>
                        <stress-bar></stress-bar>
                    </bar>
                    <span class="row-1 value-stress"></span>
                </fieldset>
            </li>
        </ul>
        <h3 class="invert">Recent Selections</h3>
        <dl class="history clearfix">
            <dt class="selection row-1">Selection</dt>
            <dt class="rating row-1">Rating</dt>
            <dt class="summary row-1">Summary</dt>
            <dt class="score row-1">{!! ucfirst($game->score_type_disp) !!}</dt>
        </dl>
        <button class="row btn_small btn_red">Close</button>
    </fieldset>
    <template>
        <dt class="period row-1" data-row="${row}"></dt>
        <dd class="selection row-0" data-row="${row}"></dd>
        <dd class="rating row-0" data-row="${row}"></dd>
        <dd class="summary row-0" data-row="${row}"></dd>
        <dd class="score row-0" data-row="${row}"></dd>
    </template>
</modal>
