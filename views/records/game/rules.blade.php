<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>{!! join("</li>\n<li>", $game->rules->as_list) !!}</li>
    </ul>
</fieldset>
