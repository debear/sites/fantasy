<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} leaderboard">
    <h3 class="invert">{!! \DeBear\Helpers\Format::pluralise($game->hasCurrentLeaderboard() ? 2 : 1, 'Leaderboard', ['hide-num' => true]) !!}</h3>
    @includeWhen(!$game->leaderboards['overall']->isset(), 'fantasy.records.game.leaderboard.preseason')
    @includeWhen($game->leaderboards['overall']->isset(), 'fantasy.records.game.leaderboard.tables')
</fieldset>
