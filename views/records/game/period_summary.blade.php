@php
    $inc_popular = isset($summary->popular_pos_pct) || isset($summary->popular_neg_pct);
    $gauge_desktop_grid = ($inc_popular ? 3 : 2);
    $summary_period = ($summary->period_id == $periods['curr']->period_id ? $periods['curr'] : $periods['last']);
@endphp
<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} period-summary">
    <h3 class="invert">Performance Review for {!! $summary_period->name_disp !!}</h3>

    <div class="grid">
        {{-- Entry Success --}}
        <dl class="entry-success-rate grid-1-{!! $gauge_desktop_grid !!} grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <dt>Overall Entry Success Rate</dt>
                <dd class="gauge">
                    <chart id="chart-entry"></chart>
                </dd>
        </dl>

        {{-- Selection Success --}}
        <dl class="sel-success-rate grid-1-{!! $gauge_desktop_grid !!} grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <dt>Overall Selection Success Rate</dt>
                <dd class="gauge">
                    <chart id="chart-sel"></chart>
                </dd>
        </dl>

        {{-- Most Popular Successful/Unsuccessful --}}
        @if ($inc_popular)
            <div class="sel-pop grid-1-3 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
                <div class="grid">
                    @isset($summary->popular_pos_pct)
                        <dl class="pos grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                            <dt>Most Popular Successful Selection</dt>
                                <dd class="sel-pct">{!! sprintf('%0.02f%%', $summary->popular_pos_pct) !!}</dd>
                                <dd class="name">{!! DeBear\Helpers\Fantasy\Records\Entities::displayLink($summary->popular_pos, $summary_period->period_id) !!}{!! $summary->popular_pos->display_status !!}</dd>
                        </dl>
                    @endisset
                    @isset($summary->popular_neg_pct)
                        <dl class="neg grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                            <dt>Most Popular Unsuccessful Selection</dt>
                                <dd class="sel-pct">{!! sprintf('%0.02f%%', $summary->popular_neg_pct) !!}</dd>
                                <dd class="name">{!! DeBear\Helpers\Fantasy\Records\Entities::displayLink($summary->popular_neg, $summary_period->period_id) !!}{!! $summary->popular_neg->display_status !!}</dd>
                        </dl>
                    @endisset
                </div>
            </div>
        @endif
    </div>
</fieldset>
<script {!! HTTP::buildCSPNonceTag() !!}>
    var charts = {
        'entry': new Highcharts.Chart({!! Highcharts::decode($summary->entrySuccessChart('chart-entry')) !!}),
        'sel': new Highcharts.Chart({!! Highcharts::decode($summary->selSuccessChart('chart-sel')) !!}),
    };
</script>
