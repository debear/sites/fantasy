@php
    $class = ($game->integration_status == 'critical' ? 'error' : 'status');
    $icon = 'icon_' . ($game->integration_status == 'critical' ? 'error' : 'warning');
@endphp
<fieldset class="integration-issue {!! "$class $icon" !!}">
    We are aware of an issue with our data provider that may impact processing of results.<br />We are working to get these resolved as soon as possible!
</fieldset>
