@php
    $inc_hitrate = $game->hasHitRateLeaderboard() && isset($entry->score->hitrate_pos);
    $inc_streak_busters = $game->hasStreakBuster() && ($entry?->busters ?? []);
    if (!$inc_hitrate && !$inc_streak_busters) {
        // If neither component is relevant, no need to continue
        return;
    }
    // Our various sizes
    $grid_size = ($inc_hitrate ? 2 : 0) + ($inc_streak_busters ? 1 : 0);
    // Tablet Landscape overrides
    if ($inc_hitrate && $inc_streak_busters) {
        // Both: 1x Gauge, 2x Table
        $grid_size_gauge_tl = 1;
        $grid_size_tables_tl = 2;
    } elseif ($inc_hitrate) {
        // Hitrate only: 1x Gauge, 1x Table
        $grid_size_gauge_tl = 2;
        $grid_size_tables_tl = 2;
    } else {
        // Streak Busters only: 1x Table
        $grid_size_gauge_tl = 1; // Not technically required, but arbitrary fallback
        $grid_size_tables_tl = 1;
    }
@endphp
<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} sel_stats">
    <h3 class="invert">Team Success</h3>
    <div class="grid">
        @if ($inc_hitrate)
            <div class="grid-1-{!! $grid_size !!} grid-tl-1-{!! $grid_size_gauge_tl !!} grid-tp-1-1 grid-m-1-1 gauge">
                <chart id="chart-hitrate"></chart>
            </div>
            <dl class="grid-1-{!! $grid_size !!} grid-tl-1-{!! $grid_size_tables_tl !!} grid-tp-1-1 grid-m-1-1 top-sel">
                <dt>Most Common Selections</dt>
                @foreach ($entry->top_sel as $sel)
                    <dd class="num">{!! $sel->times_sel !!}</dd>
                    <dd class="success">{!! sprintf('%0.02f%%', $sel->success_sel) !!}</dd>
                    <dd class="entity">{!! DeBear\Helpers\Fantasy\Records\Entities::displayLink($sel->entity) !!}</dd>
                @endforeach
            </dl>
        @endif
        @if ($inc_streak_busters)
            <dl class="grid-1-{!! $grid_size !!} grid-tl-1-{!! $grid_size_tables_tl !!} grid-tp-1-1 grid-m-1-1 top-busters">
                <dt><span class="icon_right icon_right_streak_buster">Top Streak Busters</span></dt>
                @foreach ($entry->busters as $sel)
                    <dd class="rating">{!! $sel->display_rating !!}</dd>
                    <dd class="entity">{!! $sel->display_name !!}</dd>
                @endforeach
            </dl>
        @endif
    </div>
</fieldset>
@if ($inc_hitrate)
    <script {!! HTTP::buildCSPNonceTag() !!}>
        var hitrate = new Highcharts.Chart({!! Highcharts::decode($entry->score->hitrateChart('chart-hitrate')) !!});
    </script>
@endif
