@php
    $sel_type = FrameworkConfig::get("debear.sports.{$game->sport}.name_short") . ' ' . FrameworkConfig::get("debear.sports.{$game->sport}.{$game->sel_type}.name");
@endphp
<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} sel_usage">
    <h3 class="invert">Selection Usage</h3>
    <p class="intro">
        As noted in the game rules below, an individual {!! $sel_type !!} may only be selected once across all selections made this season.
        The following table indicates the {!! $sel_type !!} still available to you.
    </p>
    <ul class="inline_list sel_list">
        @foreach ($entry->selections->all as $sel)
            @php
                $when_used = $entry->selections->made->where('link_id', $sel->link_id);
            @endphp
            <li class="{!! $when_used->isset() ? 'used' : 'row-1' !!}"><span class="team {!! FrameworkConfig::get("debear.sports.{$game->sport}.{$game->sel_type}.icon") . $sel->team_id !!}">{!! $sel->team_id !!}</span><span class="usage">{!! $when_used->isset() ? $when_used->period->name_disp : 'Available' !!}</span></li>
        @endforeach
    </ul>
</fieldset>
