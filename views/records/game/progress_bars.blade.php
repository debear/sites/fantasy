@php
    // Determine which progress bars we're displaying
    $progress = $game->generateProgressBars($entry, $periods['curr']);
    $inc_period = (!$game->isScoreTypeStreak() && $periods['last']->period_order) && !$game->isComplete();
@endphp
<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} progress">
    <h3 class="invert">Record Progress</h3>
    <dl class="clearfix">
        @php
            if ($inc_period) {
                $width = max(0, min(100, (100 * ($periods['last']->period_order / $periods['last']->periods_total)))); // Ensure in range of 0..100
                $progress_css[] = "dd.bar period { width: calc({$width}% - 1px); }";
            }
        @endphp
        @foreach ($progress as $bar)
            @php
                $progress_css[] = "dd.bar.{$bar['type']} div.progress { width: {$bar['width']}%; }";
            @endphp
            <dt>{!! $bar['name'] !!}:</dt>
                <dd class="bar {!! $bar['type'] !!}">
                    <div class="progress"></div>
                    <div class="container"></div>
                    @if ($inc_period)
                        <period></period>
                    @endif
                    {!! $bar['type'] != 'active' || !isset($game->active) ? '' :
                        (!$game->isComplete()
                            ? DeBear\Helpers\Fantasy\Records\Entities::displayLink($game->active)
                            : $game->active->display_name) !!}
                </dd>
                <dd class="value">{!! $bar['value'] !!}</dd>
        @endforeach
        {{-- Stress score --}}
        @isset($entry?->score?->stress)
            <dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">{!! $entry?->score?->stress_pct !!}%</dd>
        @endisset
    </dl>
    <style {!! HTTP::buildCSPNonceTag() !!}>
        {!! join("\n", $progress_css) !!}
        @isset($entry?->score?->stress)
            fieldset.progress .stress marker { width: calc({!! $entry->score->stress_pct !!}% - 1px); }
            fieldset.progress .stress stress-bar { clip-path: inset(0 {!! 100 - $entry->score->stress_pct !!}% 0 0); }
            fieldset.progress .stress .container { width: calc({!! $entry->score->stress_pct !!}% + 2px); }
        @endisset
    </style>
</fieldset>
