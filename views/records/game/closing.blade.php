<fieldset class="user-action info icon_info">
    This game will be closing for new entries on {!! $game->date_close->format('jS F') !!}.
</fieldset>
