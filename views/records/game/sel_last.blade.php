@php
    $sel = $entry->getSelection($game->game_ref, $period->period_id);
    $sel_status = ($sel->status ?? 'neutral');
    $sel_info = $sel->entity->display_info;
    // Is this a potential streak buster selection?
    $sel_flags = [];
    if ($game->hasStreakBuster() && isset($entry->busters)) {
        if (isset($entry->busters[$sel->entity?->info_icon ?? '*unset*'])) {
            $sel_flags[] = '<span class="sel_flag icon icon_streak_buster" title="This opponent was considered a &quot;Streak Buster&quot;"></span>';
        }
    }
@endphp
<div class="{!! $grid_sel !!}">
    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} sel last period-{!! $period->period_id !!}">
        <h3 class="invert">{!! $period->name_disp !!}</h3>
        <ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name">{!! isset($sel->link_id) ? DeBear\Helpers\Fantasy\Records\Entities::displayLink($sel->entity, $period->period_id) : $sel->entity->display_name !!}{!! $sel->entity->display_status !!}</li>
            @if ($sel_info)
                <li class="info">
                    <span class="info">{!! $sel_info !!}</span>
                    @if ($sel_flags)
                        <span class="sel_flags">{!! join($sel_flags) !!}</span>
                    @endif
                </li>
            @endif
            @if ($inc_opp_beaten && $summary->isset() && ($entry->score->opponents_chg ?? 0) > 0)
                <li class="opponent_beaten">@include('fantasy.records.game.opponent_beaten')</li>
            @endif
            <li class="subtitle {!! $sel_info ? 'with' : 'no' !!}-info outcome invert">Outcome</li>
            <li class="subtitle {!! $sel_info ? 'with' : 'no' !!}-info score invert">{!! ucfirst($game->score_type_disp) !!}</li>
            <li class="result outcome {!! $sel_status !!}">{!! $sel->summary ?? ($summary->isset() ? '&ndash;' : '<abbrev title="Results due shortly!">TBD</abbrev>') !!}</li>
            <li class="result score {!! $sel_status !!}">{!! $sel->result ?? $entry->score->current_res ?? '0' !!}</li>
            @isset($sel->stress)
                <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>{!! $sel->stress_pct !!}%</marker>
                    <stress-bar></stress-bar>
                </li>
            @endisset
            @if(!$summary->isset())
                <li class="results-soon"><fieldset class="box info icon_info">Results due shortly!</fieldset></li>
            @endif
        </ul>
    </fieldset>
</div>
<style {!! HTTP::buildCSPNonceTag() !!}>
    fieldset.sel.period-{!! $period->period_id !!} .mugshot { background-image: url({!! $sel->entity->imageURL() !!}); {!! $sel->entity->imageCSS() !!} }
    @isset($sel->stress)
        fieldset.sel.period-{!! $period->period_id !!} stress-bar { clip-path: inset(0 {!! 100 - $sel->stress_pct !!}% 0 0); }
        fieldset.sel.period-{!! $period->period_id !!} marker { margin-left: calc(calc(100% - 36px) * {!! $sel->stress_pct / 100 !!}); }
    @endisset
</style>
