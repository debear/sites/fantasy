@php
    $neutral_status = FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box';
    $selections = [];
    $dropoff = $entry->dropoff->toArray();
    $total = $entry->score->current_res;
    // Determine the upper bound of our range
    for ($sel_max = 5; $sel_max > 0; $sel_max--) {
        if (isset($dropoff['score' . ($sel_max - 1)])) {
            break;
        }
    }
    // And now process
    for ($i = 0; $i < $sel_max; $i++) {
        $sel_num = $game->active_limit - $i;
        if (isset($dropoff["score{$i}"])) {
            $status = (!$dropoff["score{$i}"] ? $neutral_status : ($dropoff["score{$i}"] > 0 ? 'error' : 'success'));
            $status_sign = (!$dropoff["score{$i}"] ? '' : ($dropoff["score{$i}"] > 0 ? '&ndash;' : '&plus;'));
            $total -= $dropoff["score{$i}"];
            $selections[] = ['num' => $sel_num, 'score' => $status_sign . abs($dropoff["score{$i}"]), 'status' => $status, 'total' => $total];
        } else {
            $alt = $sel_num;
            if (count($selections)) {
                $alt = $selections[0]['num_alt'] ?? $selections[0]['num'];
                $selections = [];
            }
            $selections[] = ['num' => $sel_num, 'num_alt' => ($alt != $sel_num ? $alt : null), 'score' => null, 'status' => $neutral_status, 'total' => $total];
        }
    }
    $num_cells = count($selections);
    $grid_sizes_tp = [5 => [3, 3, 3, 2, 2], 4 => [2, 2, 2, 2], 3 => [1, 1, 1], 2 => [1, 1], 1 => [1]];
@endphp
<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} dropoff">
    <h3 class="invert">Total Score Dropoff</h3>
    <p class="intro">
        Due to the limit that only your last {!! $game->active_limit !!} selections will count towards to your current {!! $game->score_type_disp !!}, the contributions from some of your earlier selections will soon stop being included in your {!! $game->score_type_disp !!}.
        The following list will show how your score will be reduced over the coming {!! \DeBear\Helpers\Format::pluralise(2, FrameworkConfig::get("debear.sports.{$game->sport}.event_type"), ['hide-num' => true]) !!} as contributions from these earlier selections will be replaced by the new selections you are about to make.
    </p>
    <ul class="inline_list grid">
        @foreach ($selections as $i => $sel)
            <li class="grid-1-{!! $num_cells !!} grid-tl-1-{!! $num_cells !!} grid-tp-1-{!! $grid_sizes_tp[$num_cells][$i] !!} grid-m-1-1">
                <fieldset class="{!! $sel['status'] !!} dropoff-cell clearfix">
                    <h3 class="invert">
                        {!! \DeBear\Helpers\Format::ordinal($sel['num']) . (isset($sel['num_alt']) ? '&ndash;' . \DeBear\Helpers\Format::ordinal($sel['num_alt']) : '') !!} {!! \DeBear\Helpers\Format::pluralise(isset($sel['num_alt']) ? 2 : 1, 'Selection', ['hide-num' => true]) !!}
                    </h3>
                    <div class="score">{!! $sel['score'] ?? '&ndash;' !!}</div>
                    <div class="note">
                        @isset($sel['score'])
                            Effective {!! ucfirst($game->score_type_disp) !!}: <strong>{!! $sel['total'] !!}</strong>
                        @else
                            <em>To Be Determined</em>&hellip;
                        @endisset
                    </div>
                </fieldset>
            </li>
        @endforeach
    </ul>
</fieldset>
