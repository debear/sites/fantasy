@php
    $opp_map = array_diff_key($entry->score->opponents_map, [0 => true]); // Mapping is one-indexed
    $opp_tot = count($opp_map);
    $opp_beaten = count(array_filter($opp_map));
    $opp_left = $opp_tot - $opp_beaten;
    // Use this to format a message
    if ($opp_beaten == 1) {
        $message = 'Congratulations &ndash; your first opponent taken down!';
    } elseif ($opp_beaten == ceil($opp_tot / 2)) {
        $message = "Halfway there &ndash; $opp_beaten opponents down, $opp_left to go!";
    } elseif (!$opp_left) {
        $message = 'All opponents have now been ticked off!';
    } else {
        $messages = [
            'Another opponent ticked off!',
            "$opp_beaten opponents down, $opp_left to go...", // Plural because singular is dealt with above
            'Your ' . Format::ordinal($opp_beaten) . ' opponent has just been taken down.',
            'That&#39;s your ' . Format::ordinal($opp_beaten) . ' opponent ticked off!',
            'Only ' . Format::pluralise($opp_left, ' opponent') . ' left after that selection!',
        ];
        $message = $messages[array_rand($messages)];
    }
@endphp
<fieldset class="opponent-beaten success icon_valid">{!! $message !!}</fieldset>
