@php
    // We always need the "overall" tab.
    $tables = [
        'overall' => ['pos' => 'overall_pos', 'res' => 'overall_res', 'suffix' => ''],
    ];
    // However, we may also need a "current" and/or "hit rate" tab.
    if ($game->hasCurrentLeaderboard()) {
        $tables['current'] = ['pos' => 'current_pos', 'res' => 'current_res', 'suffix' => ''];
    }
    if ($game->hasHitRateLeaderboard()) {
        $tables['hitrate'] = ['pos' => 'hitrate_pos', 'res' => 'hitrate_res', 'suffix' => '%'];
    }
    // What does this mean for our processing?
    $display_tabs = (count($tables) > 1);
    $default_tab = isset($tables['current']) ? 'current' : 'overall';
@endphp
<dl class="leaderboard {!! "tabs-" . count($tables) !!}" data-default-tab="{!! $default_tab !!}" data-period-id="{!! $periods['last']->period_id !!}">
    @if ($display_tabs)
        @isset ($tables['current'])<dt class="tab {!! $default_tab == 'current' ? 'sel row-1' : 'unsel row-0' !!}" data-tab="current">Current</dt>@endisset
        <dt class="tab {!! $default_tab == 'overall' ? 'sel row-1' : 'unsel row-0' !!}" data-tab="overall">Overall</dt>
        @isset ($tables['hitrate'])<dt class="tab {!! $default_tab == 'hitrate' ? 'sel row-1' : 'unsel row-0' !!}" data-tab="hitrate">Success Rate</dt>@endisset
    @endif
    @foreach ($tables as $key => $col)
        <dd class="table {!! $key !!} {!! $key != $default_tab ? 'hidden' : '' !!}">
            <dl class="clearfix">
                @php
                    $rendered_user = $rendering_ties = false;
                    $ties = array_filter(array_count_values($game->leaderboards[$key]->pluck($col['pos'])), function ($i) {
                        return $i > 1;
                    });
                    $by_pos = [];
                @endphp
                @foreach ($game->leaderboards[$key] as $i => $row)
                    @php
                        $rendered_user = $rendered_user || ($row->user_id == $entry?->user_id);
                        $by_pos[$row->{$col['pos']}] = ($by_pos[$row->{$col['pos']}] ?? 0) + 1;
                        $row_css = "num-$i " . $row->rowCSS($col['pos'], count(array_keys($by_pos)));
                        $pos_text = Format::ordinal($row->{$col['pos']});
                        // Identify and adapt to any position ties.
                        if ($by_pos[$row->{$col['pos']}] > 1) {
                            $pos_text = '=';
                        } elseif (isset($ties[$row->{$col['pos']}])) {
                            $pos_text = "T-$pos_text";
                        }
                        // Ensure an appropriate entry name is displayed.
                        if ($loop->iteration < 10 || $game->leaderboards[$key]->count() == 10) {
                            if (!$game->isComplete()) {
                                $name = '<a class="view-team-summary" data-slug="' . $row->slug . '">' . $row->entry_name . '</a>';
                            } else {
                                $name = $row->entry_name;
                            }
                        } else {
                            // There are multiple ties remaining on the last line
                            $name = '<em>' . ($game->leaderboards[$key]->count() - $loop->iteration + 1) . ($by_pos[$row->{$col['pos']}] > 1 ? ' More' : '') . ' Entries Tied</em>';
                            $rendering_ties = true;
                        }
                    @endphp
                    <dt class="{!! $row_css !!} pos"><span class="{!! !$game->isComplete() ? (!$rendering_ties ? $row->posChangeIcon($key) : 'multiple-ties') : '' !!}">{!! $pos_text !!}</span></dt>
                        <dd class="{!! $row_css !!} name">{!! $name !!}</dd>
                        <dd class="{!! $row_css !!} score">{!! $row->{$col['res']} !!}{!! $col['suffix'] !!}</dd>
                    @if ($rendering_ties)
                        @break
                    @endif
                @endforeach
                {{-- Display the user's record, if they're not already in the above list --}}
                @if (isset($entry?->score?->{$col['pos']}) && !$rendered_user)
                        <dd class="row-{!! (count(array_keys($by_pos)) + 1) % 2 !!} horiz-sep"></dd>
                    <dt class="row-user pos"><span class="{!! !$game->isComplete() ? $entry->score->posChangeIcon($key) : '' !!}">{!! Format::ordinal($entry->score->{$col['pos']}) !!}</span></dt>
                        <dd class="row-user name">{!! !$game->isComplete() ? '<a class="view-team-summary" data-slug="' . $entry->slug . '">' : '' !!}{!! $entry->name !!}{!! !$game->isComplete() ? '</a>' : '' !!}</dd>
                        <dd class="row-user score">{!! $entry->score->{$col['res']} !!}{!! $col['suffix'] !!}</dd>
                @endif
            </dl>
        </dd>
    @endforeach
    @if (!$game->isComplete())
        <dd class="view-leaders row-0"><a>View Expanded Leaderboard</a></dd>
    @endif
</dl>
