@php
    $achieve_bit_flags = array_keys(array_filter($entry->score->achievements_chg_map));
@endphp
<fieldset class="achievements-earned success icon_valid">
    Congratulations! Your last selection has successfully completed the following <em>In-Game {!! Format::pluralise(count($achieve_bit_flags), 'Target', ['hide-num' => true]) !!}</em>:
    <ul class="inline_list">
        @foreach ($game->achievements as $achieve)
            @foreach ($achieve->levels->whereIn('bit_flag', $achieve_bit_flags) as $level)
                <li class="icon icon_achieve_level{!! $level->level !!}"><strong>{!! $achieve->name !!}</strong>: {!! $level->value_disp !!}</li>
            @endforeach
        @endforeach
    </ul>
</fieldset>
