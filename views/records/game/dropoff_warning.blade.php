<fieldset class="dropoff-warning status icon_warning">
    In addition to your next selection's score, your {!! $game->score_type_disp !!} will <strong>also {!! $entry->dropoff->score0 > 0 ? 'de' : 'in' !!}crease by {!! $entry->dropoff->score0 !!}</strong>
    as your {!! $game->score_type_disp !!}'s {!! \DeBear\Helpers\Format::ordinal($game->active_limit) !!} selection will be replaced and no longer be included.
</fieldset>
