@php
    $sel = $entry->getSelection($game->game_ref, $period->period_id);
    $lock_time_fmt = FrameworkConfig::get("debear.sports.{$game->sport}.period.format");
    // Is this a potential streak buster selection?
    $sel_flags = [];
    $flags = [];
    if ($game->hasStreakBuster() && isset($entry->busters)) {
        $flags['buster'] = isset($entry->busters[$sel->entity?->info_icon ?? '*unset*']);
        if ($flags['buster']) {
            $sel_flags[] = '<span class="sel_flag icon icon_streak_buster" title="This opponent is considered a &quot;Streak Buster&quot;"></span>';
        }
    }
    // Is this a potential opponent defeated selection?
    if ($game->hasAchievement('opponent') && isset($entry->score->opponents)) {
        $opp = $game->opponents->where('raw_id', $sel->entity->info_icon ?? '*unset*');
        $flags['achieve_opp'] = $opp->isset() && boolval($entry->score->opponents & pow(2, $opp->bit_flag - 1));
    }
    // Is there any weather info to render?
    if (isset($sel->entity->weather_symbol)) {
        $sel_flags[] = '<span class="weather icon icon_weather_' . $sel->entity->weather_symbol . '">' . (isset($sel->entity->weather_temp) ? sprintf('%d&deg;', $sel->entity->weather_temp) : '') . '</span>';
    }
    // Build our stats data attributes.
    $stats = ($sel->stats ?? []);
    $source = array_filter([
        'rating' => $sel->rating_disp,
        'opp_rating' => $sel->opp_rating_disp,
    ] + $stats, function ($a) {
        return isset($a);
    });
    $link_attributes = [];
    foreach ($source as $id => $value) {
        $link_attributes[] = " data-stat-{$id}=\"" . (htmlentities($value) ?? '&ndash;') . '"';
    }
    foreach ($flags as $id => $value) {
        $link_attributes[] = " data-flags-{$id}=\"" . ($value ? 'true' : 'false') . '"';
    }
@endphp
<div class="{!! $grid_sel !!}">
    <fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} sel curr period-{!! $period->period_id !!}" data-link-id="{!! $sel->link_id !!}"{!! join($link_attributes) !!}>
        <h3 class="invert">{!! $period->name_disp !!}</h3>
        <ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name">{!! isset($sel->link_id) ? DeBear\Helpers\Fantasy\Records\Entities::displayLink($sel->entity) : $sel->entity->display_name !!}{!! $sel->entity->display_status !!}</li>
            @if ($info = $sel->entity->display_info)
                <li class="info">
                    <span class="info">{!! $info !!}</span>
                    @if ($sel_flags)
                        <span class="sel_flags">{!! join($sel_flags) !!}</span>
                    @endif
                </li>
            @endif
            @if (!$sel->is_locked && !$period->is_locked)
                <li class="subtitle invert">Actions</li>
                <li class="actions">
                    @if (!$period->sel_available)
                        <fieldset class="box info icon_info">Selections will be available soon!</fieldset>
                    @elseif (isset($sel->link_id))
                        <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                    @else
                        <button class="btn_green sel-add sel-make">Add</button>
                    @endif
                </li>
            @endif
            @if ($period->sel_available)
                <li class="locks icon_padlock_{!! $sel->is_locked ? 'closed' : 'open' !!} invert"{!! isset($sel->link_id) ? ' data-lock-time="' . $sel->entity->lock_time?->display?->format($lock_time_fmt) . '"' : '' !!}>
                    @if ($sel->is_locked)
                        Selection Locked
                    @elseif (isset($sel->link_id) && !$sel->entity->eventActive())
                        {!! ucfirst(FrameworkConfig::get("debear.sports.{$game->sport}.event_type")) !!} {!! $sel->entity->event_status !!}
                    @elseif (isset($sel->link_id))
                        Selection Locks at {!! $sel->entity->lock_time->display->format($lock_time_fmt) !!}
                    @elseif ($period->is_locked)
                        Selections No Longer Available
                    @else
                        Selections Lock at {!! $period->lock_time->display->format($lock_time_fmt) !!}
                    @endif
                </li>
            @endif
        </ul>
    </fieldset>
</div>
<style {!! HTTP::buildCSPNonceTag() !!}>
    fieldset.sel.period-{!! $period->period_id !!} .mugshot { background-image: url({!! $sel->entity->imageURL() !!}); {!! $sel->entity->imageCSS() !!} }
</style>
