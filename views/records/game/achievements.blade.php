@php
    // Decide if we need to / should display the individual list of opponents
    $achieve_opp = $game->achievements->where('type', 'opponent');
    $achieve_opp_num = array_sum($entry->score->opponents_map);
    $achieve_opp_tot = $game->opponents?->count() ?? 0;
    $inc_opp = (!$game->isComplete() && isset($entry->score->opponents) && $achieve_opp->isset() && $achieve_opp_num < $achieve_opp_tot);
    // This affects the width of the grids we'll produce
    $grid_width = ($inc_opp ? 2 : 1);
    // Determine the total number of individual achievements the user can obtain
    $total_levels = 0;
    foreach ($game->achievements as $achieve) {
        $total_levels += $achieve->levels->count();
    }
@endphp
<fieldset class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") . '_box' !!} achievements">
    <h3 class="invert">In-Game Targets &ndash; {!! array_sum($entry->score->achievements_map) !!} / {!! $total_levels !!} achieved</h3>
    @if (!$game->isComplete())
        <p>Whilst the following achievements are not the main objective, why not try and see how many you can check off along the way?</p>
    @endif
    <div class="grid">
        <dl class="grid-1-{!! $grid_width !!} grid-tl-1-{!! $grid_width !!} grid-tp-1-1 grid-m-1-1 list clearfix">
            @foreach ($game->achievements as $achieve)
                <dt>{!! $achieve->name !!}</dt>
                    @php $i = 0; @endphp
                    @foreach ($achieve->levels as $level)
                        <dd class="level{!! !$i++ ? ' first-level' : '' !!}{!! $entry->score->achievements_map[$level->bit_flag] ? ' achieved' : ''!!} icon icon_achieve_level{!! $level->level !!}">{!! $level->value_disp !!}</dd>
                    @endforeach
                    <dd class="descrip">{!! $achieve->descrip !!}</dd>
            @endforeach
        </dl>
        {{-- If we have an opponents list, we should include the current state of play --}}
        @if ($inc_opp)
            <dl class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 opponents clearfix">
                <dt class="icon_right icon_right_achieve_done">Opponents Targeted &ndash; {!! $achieve_opp_num !!} / {!! $achieve_opp_tot !!}</dt>
                    <dd class="descrip">To assist with the <em>{!! $achieve_opp->name !!}</em> achievement, these are the opponents and whether or not they have been targeted successfully.</dd>
                    @foreach ($game->opponents as $opp)
                        <dd class="opp {!! $entry->score->opponents_map[$opp->bit_flag] ? 'achieved' : '' !!}"><span class="team {!! FrameworkConfig::get("debear.sports.{$game->sport}.{$game->sel_type}.icon") . $opp->raw_id !!}">{!! $opp->raw_id !!}</span></dd>
                    @endforeach
        @endif
    </div>
</fieldset>
