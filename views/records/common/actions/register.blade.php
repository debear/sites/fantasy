<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="{!! FrameworkConfig::get('debear.dirs.subsite-records') !!}/my-entry">click here</a> to
    confirm your details, or <em>Join</em> on any of the games below and get picking!
</fieldset>
