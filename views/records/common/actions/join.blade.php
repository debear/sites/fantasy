<fieldset class="user-action game-action status icon_info game" data-ref="{!! $game->game_ref !!}">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>
