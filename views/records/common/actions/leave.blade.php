<fieldset class="user-action game-action status icon_info game" data-ref="{!! $game->game_ref !!}">
    If you&#39;ve changed your mind, <a class="game-leave">click here</a> to leave this game.
</fieldset>
