@extends('fantasy.records.og.wrapper')

@section('content')

<div class="preview">
    <h3>Season Starts: {!! $game->date_start->format('jS F') !!}</h3>

    <div class="holder">
        <style>
            mugshot {
                background: url(https:{!! \DeBear\Helpers\HTTP::buildDomain('static') !!}/fantasy/images/records/holders/{!! $game->sport !!}-{!! $game->season !!}-{!! $game->game_id !!}.{!! $game->hasMultipleRecordHolders() ? 'gif' : 'png' !!});
            }
        </style>
        <mugshot></mugshot>
        <dl>
            <dt>Record Holder{!! $game->hasMultipleRecordHolders() ? 's' : '' !!}:</dt>
            @foreach (explode('; ', $game->record_holder ?? '') as $holder)
                <dd>{!! $holder ?: '<em>No-one yet, become the first!</em>' !!}</dd>
            @endforeach
        </div>
    </div>
</div>

@endsection
