@php
    $static_domain = \DeBear\Helpers\HTTP::buildDomain('static');
@endphp
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/skel/css/fonts.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/skel/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/skel/css/helpers/base.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/skel/css/grids/base.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/fantasy/css/records/common.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/fantasy/css/records/logos/og.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/fantasy/css/_common/general.css" />
        @if ($game->isMajorLeague())
            <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/fantasy/css/_common/{!! $game->sport !!}/teams.css" />
        @endif
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/fantasy/css/records/widgets/og.css" />
        <link rel="stylesheet" type="text/css" href="https:{!! $static_domain !!}/fantasy/css/records/widgets/progress_bars.css" />
        <style>
            @font-face {
                font-family: 'Punk';
                font-style: normal;
                font-weight: normal;
                src: url('file://{!! realpath(resource_path() . '/../../cdn') !!}/setup/fantasy/records/fonts/Punk.ttf') format('truetype');
            }

            body {
                background: url(https:{!! $static_domain !!}/fantasy/images/records/og/backgrounds/{!! $game->sport !!}.jpg);
                max-height: {!! $config['height'] !!}px;
                max-width: {!! $config['width'] !!}px;
            }
        </style>
    </head>
    <body class="{!! FrameworkConfig::get("debear.sports.{$game->sport}.box") !!}_box">
        <h1 class="records-og-{!! $game->sport !!}"><em>{!! $game->name !!}</em></h1>

        @yield('content')

        <div class="hashtag">#DeBearRecords</div>
    </body>
</html>
