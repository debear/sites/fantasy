@php
    $progress = $game->generateProgressBars();
    $progress_css = [];
    foreach ($progress as $bar) {
        $progress_css[] = "dd.bar.{$bar['type']} div.progress { width: {$bar['width']}%; }";
    }
    $inc_period = (!$game->isScoreTypeStreak() && $period->period_order);
    if ($inc_period) {
        $width = max(0, min(100, (100 * ($period->period_order / $period->periods_total)))); // Ensure in range of 0..100
        $progress_css[] = "dd.bar period { width: calc({$width}% - 1px); }";
    }
    $leaderboards = [
        ['title' => $game->hasCurrentLeaderboard() ? 'Overall Leaders' : 'Leaderboard', 'list' => $game->leaderboards['overall'], 'col' => 'overall_pos'],
    ];
    if ($game->hasCurrentLeaderboard()) {
        $leaderboards[] = ['title' => 'Current Leaders', 'list' => $game->leaderboards['current'], 'col' => 'current_pos'];
    }
@endphp
@extends('fantasy.records.og.wrapper')

@section('content')
<div class="period">After {!! $period->name_disp !!}</div>

<style>
    {!! join("\n", $progress_css) !!}
</style>
<div class="progress">
    <dl class="progress bars-{!! count($progress) !!} clearfix">
        @foreach ($progress as $bar)
            <dt>{!! $bar['name'] !!}:</dt>
                <dd class="bar {!! $bar['type'] !!}">
                    <div class="progress"></div>
                    <div class="container"></div>
                    @if ($inc_period)
                        <period></period>
                    @endif
                    {!! $bar['type'] == 'active' && isset($game->active) ? $game->active->display_name : '' !!}
                </dd>
                <dd class="value">{!! $bar['value'] !!}</dd>
        @endforeach
    </dl>
</div>

<ul class="leaderboards grid">
    @foreach ($leaderboards as $leaderboard)
        <li class="leaderboard {!! $leaderboard['col'] !!} grid-1-{!! count($leaderboards) !!}">
            <h3>{!! $leaderboard['title'] !!}</h3>
            <dl class="clearfix">
                @php
                    $last_pos = -1;
                    $rendering_ties = false;
                @endphp
                @foreach ($leaderboard['list']->slice(0, $config['num_leaders']) as $i => $row)
                    @php
                        $row_css = "num-$i " . $row->rowCSS($leaderboard['col'], $i);
                        // Identify ties.
                        $is_tied = ($row->{$leaderboard['col']} == $last_pos);
                        $last_pos = $row->{$leaderboard['col']};
                        // Ensure an appropriate entry name is displayed.
                        if ($loop->iteration < $config['num_leaders'] || $leaderboard['list']->count() == $config['num_leaders']) {
                            $name = $row->entry_name;
                        } else {
                            // There are multiple ties remaining on the last line
                            $name = '<em>' . ($leaderboard['list']->count() - $loop->iteration + 1) . ($is_tied ? ' More' : '') . ' Entries Tied</em>';
                            $rendering_ties = true;
                        }
                    @endphp
                        <dd class="{!! $row_css !!} name">{!! $name !!}</dd>
                        <dd class="{!! $row_css !!} score">{!! $row->overall_res !!}</dd>
                    @if ($rendering_ties)
                        @break
                    @endif
                @endforeach
            </dl>
        </li>
    @endforeach
</ul>
@endsection
