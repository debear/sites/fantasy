{{-- Record Holder --}}
@include('fantasy.records.home.widgets.holder')

{{-- Can the user start playing? --}}
@include('fantasy.records.home.widgets.join')
