{{-- Record Holder --}}
@include('fantasy.records.home.widgets.holder')

{{-- When opens --}}
<div class="opens bottom invert">Opens for entries on {!! $game->date_open->format('jS F') !!}</div>
