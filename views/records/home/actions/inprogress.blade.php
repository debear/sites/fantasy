{{-- Can the user start playing? --}}
@include('fantasy.records.home.widgets.join')

{{-- When closes, if soon --}}
@if ($game->isClosingSoon())
    <div class="closes invert">Entries close on {!! $game->date_close->format('jS F') !!}</div>
@endif
