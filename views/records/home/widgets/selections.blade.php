@php
    $lock_time_fmt = FrameworkConfig::get("debear.sports.{$game->sport}.period.format");
    $periods = $game->getPeriods();
    foreach (['curr', 'last'] as $period_type) {
        $period_id = $periods[$period_type]?->period_id ?? 0;
        if ($period_id) {
            $sel = $entry->getSelection($game->game_ref, $period_id);
            if (isset($sel) && $sel->isset()) {
                $period = $periods[$period_type];
                break;
            }
        }
    }
    if (!isset($sel)) {
        $sel = DeBear\ORM\Fantasy\Records\EntryByPeriod::createBlank($game, $period_id);
    }
    if (!isset($period)) {
        $period = $periods['curr'];
    }
@endphp
<h3 class="invert period">{!! $period->name_disp !!}</h3>
<ul class="inline_list clearfix">
    <li class="mugshot cdn-small"></li>
    <li class="row-1 name">{!! $sel->entity->display_name !!}{!! $sel->entity->display_status !!}</li>
    @if ($info = $sel->entity->display_info)
        <li class="info"><span>{!! $info !!}</span></li>
    @endif
    @isset ($sel->status)
        <li class="result">
            <div class="outcome">
                <h3 class="invert">Outcome</h3>
                <span class="{!! $sel->status ?? 'neutral' !!}">{!! $sel->summary ?? '&ndash;' !!}</span>
            </div>
            <div class="score">
                <h3 class="invert">{!! ucfirst($game->score_type_disp) !!}</h3>
                <span class="{!! $sel->status ?? 'neutral' !!}">{!! $sel->result ?? $entry->score->current_res ?? '0' !!}</span>
            </div>
        </li>
    @elseif ($sel->isset())
        <li class="lock invert"><span class="icon icon_padlock_{!! $sel->is_locked ? 'closed' : 'open' !!}">{!! $sel->is_locked ? 'Selection Locked' : 'Selection Locks at ' . $sel->entity->lock_time->display->format($lock_time_fmt) !!}</span></li>
    @elseif ($period->sel_available)
        <li class="lock invert"><span class="icon icon_padlock_{!! $period->is_locked ? 'closed' : 'open' !!}">{!! $period->is_locked ? 'Selections No Longer Available' : 'Selections Lock at ' . $period->lock_time->display->format($lock_time_fmt) !!}</span></li>
    @endisset
</ul>
<style {!! HTTP::buildCSPNonceTag() !!}>
    fieldset[data-ref="{!! $game->game_ref !!}"] .mugshot { background-image: url({!! $sel->entity->imageURL(['size' => 'small']) !!}); {!! $sel->entity->imageCSS() !!} }
</style>
