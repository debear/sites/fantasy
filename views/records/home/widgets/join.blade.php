@includeWhen(($user_action == 'register') || (isset($entry) && !$entry->getGameScore($game)->isset()), 'fantasy.records.home.widgets.game_action', [
    'label' => 'Up for the challenge?',
    'colour' => 'green',
    'action' => 'join',
    'button' => 'Join',
])
