{{-- Phase 1: Determine the bars to display (if any...) --}}
@php
    if ($entry?->isset()) {
        $entry->score = $entry?->getGameScore($game);
    }
    $bars = $game->generateProgressBars($entry);
    $css = [];
@endphp

{{-- If there are any to display, display them --}}
@if ($bars)
    <h3 class="progress invert">Player Progress</h3>
    <ul class="progress {!! $game->game_ref !!} inline_list clearfix">
        @foreach ($bars as $bar)
            @php
                $css[] = "ul.progress.{$game->game_ref} li.bar.{$bar['type']} div.progress { width: {$bar['width']}%; }";
            @endphp
            <li class="bar {!! $bar['type'] !!}">
                <div class="progress"></div>
                <div class="container"></div>
                <span class="name">{!! $bar['name'] !!}</span>
                <span class="value">{!! $bar['value'] !!}</span>
            </li>
        @endforeach
    </ul>

    <style {!! HTTP::buildCSPNonceTag() !!}>
        {!! join("\n", $css) !!}
    </style>
@endif
