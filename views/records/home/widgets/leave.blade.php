@includeWhen(!$entry->getGameScore($game)->processed, 'fantasy.records.home.widgets.game_action', [
    'label' => 'Joined by mistake?',
    'colour' => 'red',
    'action' => 'leave',
    'button' => 'Hide',
])
