<div class="game-action-container">
    <ul class="inline_list game-action clearfix">
        <li class="label invert">{!! $label !!}</li>
        <li class="action"><button class="btn btn_small btn_{!! $colour !!} game-{!! $action !!}">{!! $button !!}</button></li>
    </ul>
</div>
