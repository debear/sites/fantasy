<ul class="inline_list holders clearfix">
    <li class="mugshot"><img alt="Mugshot of the current Record Holder" src="{!! $game->recordHolderURL('medium') !!}"></li>
    <li class="label invert">Current Record {!! Format::pluralise($game->hasMultipleRecordHolders() ? 2 : 1, 'Holder', ['hide-num' => true]) !!}</li>
    @foreach (explode('; ', $game->record_holder ?? '') as $holder)
        <li {!! $game->hasMultipleRecordHolders() ? 'class="multi"' : '' !!}>{!! $holder ?: '<em>No-one yet, become the first!</em>' !!}</li>
    @endforeach
</ul>
