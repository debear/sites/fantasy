<fieldset class="medals section">
    <h3 class="invert">Medals Earned</h3>
    <ul class="inline_list">
        @forelse ($user->medalsEarned() as $medal)
            <li class="icon {!! "icon_{$medal['icon']}" !!}">{!! "{$medal['num']}x" !!} {!! $medal['title'] !!}</li>
            <li class="info"><em>{!! $medal['info'] !!}</em></li>
        @empty
            <li class="none"><em>This user has not yet earned any medals</em></li>
        @endforelse
    </ul>
</fieldset>
