<dl class="summary">
    <dt class="ranking">Overall Player Score:</dt>
        <dd>{!! \DeBear\Helpers\Fantasy\Profile::formatRating($user->rating) !!}</dd>
        <dd>{!! \DeBear\Helpers\Fantasy\Profile::formatRatingLevel($user->rating) !!}</dd>
    <dt class="since">Player Since:</dt>
        <dd>{!! $user->teams->min('season') !!}</dd>
    <dt class="based">Based:</dt>
        <dd><span class="flag_right {!! "flag16_right_{$user->country_flag}" !!}">{!! $user->country !!}</span></dd>
</dl>
