@php
    $by_game_type = [];
    foreach ($user->teams->unique('game_type_ref') as $game_type_ref) {
        $by_game_type[$game_type_ref] = $user->teams->where('game_type_ref', $game_type_ref);
    }
    uasort($by_game_type, function ($a, $b) {
        $a_max = $a->max('season');
        $b_max = $b->max('season');
        if ($a_max == $b_max) {
            return $a->game_ref <=> $b->game_ref;
        }
        return 1 - ($a_max <=> $b_max); // Reverse order.
    });
@endphp
<dl>
    @foreach ($by_game_type as $game_type_ref => $teams)
        @php
            $config = FrameworkConfig::get("debear.subsites.$game_type_ref");
            $type_rating = $teams->avg('profile_points');
        @endphp
        <dt class="{!! "fantasy-icon-{$config['url']}" !!} type row-1">
            <name>{!! $config['names']['section'] !!}</name>
            <rating>
                {!! \DeBear\Helpers\Fantasy\Profile::formatRating($type_rating) !!}
                &ndash; {!! \DeBear\Helpers\Fantasy\Profile::formatRatingLevel($type_rating) !!}
            </rating>
        </dt>
            <dd class="{!! "type-$game_type_ref" !!}">
                <ul class="inline_list">
                    @foreach ($teams as $team)
                        @php
                            $rank = $team->detail->rank;
                            $team_css = (isset($rank) && $rank < 4 ? "podium-$rank" : 'row-' . ($loop->index % 2));
                            $inc_groups = (isset($team->detail->groups) && $team->detail->groups->count());
                            $team_ref = substr(md5("$game_type_ref-{$loop->index}"), 0, 10);
                        @endphp
                        <li class="{!! $team_css !!} team {!! $team->in_progress ? 'in-progress' : '' !!}">
                            <span class="season">{!! $team->season !!}</span>
                            <span class="name">{!! $team->name !!}</span>
                            <span class="rank">
                                @if (isset($rank) && $rank <= 100)
                                    {!! Format::ordinal($rank) !!} Overall
                                @elseif (isset($team->detail->centile))
                                    {!! Format::ordinal($team->detail->centile) !!} Centile
                                @endif
                            </span>
                            <span class="rating">{!! \DeBear\Helpers\Fantasy\Profile::formatRating($team->profile_points) !!}</span>
                            @if ($inc_groups)
                                <span class="group-toggle icon_nav_expand" data-id="{!! $team_ref !!}"></span>
                            @endif
                        </li>
                        @if ($inc_groups)
                            <li class="groups hidden" data-id="{!! $team_ref !!}">
                                <ul class="inline_list">
                                    @foreach ($team->detail->groups as $group)
                                        @php
                                            $group_css = ($group->rank < 4 ? "podium-{$group->rank}" : 'row-' . ($loop->index % 2));
                                        @endphp
                                        <li class="{!! $group_css !!}">
                                            {!! $group->name !!}
                                            @if ($group->num_teams > 1)
                                                <div class="group-rank">
                                                    {!! Format::ordinal($group->rank) !!} of {!! $group->num_teams !!}
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </dd>
    @endforeach
    @if (count(array_filter($user->teams->unique('in_progress'))))
        <dd class="key in-progress">Game still in progress</dd>
    @endif
</dl>
