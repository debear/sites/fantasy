@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $user->display_name !!} ({!! $user->user_id !!})</h1>

{{-- Summary --}}
@include('fantasy.profile.summary')
<div class="grid">
    <div class="teams grid-cell grid-2-3 grid-t-1-1 grid-m-1-1">
        {{-- Team List, broken down by game --}}
        @include('fantasy.profile.teams')
    </div>
    <div class="secondary grid-cell grid-1-3 grid-t-1-1 grid-m-1-1">
        {{-- Medals --}}
        <div class="grid-tl-1-2">
            @include('fantasy.profile.medals')
        </div>
        {{-- Breakdown Chart --}}
        <div class="grid-tl-1-2">
            @include('fantasy.profile.breakdown')
        </div>
    </div>
</div>

@endsection
