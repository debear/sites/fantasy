<fieldset class="breakdown section">
    <h3 class="invert">Team Breakdown</h3>
    <div class="chart" id="team-breakdown"></div>
</fieldset>
<script {!! HTTP::buildCSPNonceTag() !!}>
    var chart = new Highcharts.Chart({!! Highcharts::decode($user->teamBreakdownChart('team-breakdown')) !!});
</script>
