<fieldset class="news {!! $announcement->type !!}">
    @isset($announcement->subject)
        <h3 class="invert">{!! $announcement->subject !!}</h3>
    @endisset
    {!! $announcement->body !!}
</fieldset>
