<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/{!! FrameworkConfig::get('debear.section.endpoint') !!}/my-entry">click here</a> to
    confirm your details and then start thinking about your bracket!
</fieldset>
