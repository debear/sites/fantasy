<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a {!! FrameworkConfig::get('debear.names.site') !!} user account!
</fieldset>
