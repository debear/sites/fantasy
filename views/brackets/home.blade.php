@extends('skeleton.layouts.html')

@section('content')

<h1>Welcome to {!! FrameworkConfig::get('debear.names.section') !!}!</h1>

@each('fantasy._widgets.news', $news, 'announcement')
@includeWhen($message = session('form_complete'), 'fantasy.brackets.common.actions.success')
@include('fantasy.brackets.home.intro')
@includeWhen(isset($user_action), "fantasy.brackets.common.actions.$user_action")

@include('fantasy.brackets.home.bracket')

@endsection
