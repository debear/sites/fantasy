@php
    // Introduction depends on the game state.
    if ($game->isArchive()) {
        $intro = 'has moved on to the current season, but championships last forever!';
    } elseif ($game->isPendingFinalisedSeeds()) {
        $intro = 'is due to start soon. There may still be some changes to the seeding, and therefore the matchups in the bracket, but it&#39;s time to start thinking about your choices!';
    } elseif ($game->isBeforePlayoffsStart()) {
        $intro = 'is due to start soon! The bracket is now set, so get your choices in before the games start and the game locks!';
    } elseif ($game->isDuringPlayoffs()) {
        $intro = 'goes on, and your bracket is being scored. How are you getting on?';
    } elseif ($game->isAfterPlayoffsCompleted()) {
        $intro = 'is over for another season. Congratulations to the champions, and to all those who took part!';
    }
@endphp
<p class="intro">The quest for the {!! FrameworkConfig::get('debear.setup.sport.playoffs.trophy') !!} {!! $intro !!}</p>
