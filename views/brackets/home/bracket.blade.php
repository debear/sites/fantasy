@php
    // Round meta info
    $tot_rounds = count($template['rounds']);
    $rounds_by_type = array_merge(
        array_fill_keys(['div', 'conf', 'league'], 0),
        array_count_values(array_column($template['rounds'], 'grouping'))
    );
    $conf_rounds = $rounds_by_type['conf'] + $rounds_by_type['div'];
    $div_rounds = $rounds_by_type['div'];
@endphp
<ul class="inline_list bracket clearfix">
    @include('fantasy.brackets.home.bracket.conference', ['conf' => $groupings->where('type', 'conf')->first(), 'pos' => 'left'])
    @include('fantasy.brackets.home.bracket.conference', ['conf' => $groupings->where('type', 'conf')->last(), 'pos' => 'right'])
    @include('fantasy.brackets.home.bracket.championship')
</ul>
