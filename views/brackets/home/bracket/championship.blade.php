@php
    $prefix = array_key_last($template['rounds']);
    $round = $template['rounds'][$prefix];
    $group_id = $code = 0;
@endphp
<li class="rounds-{!! $tot_rounds !!} champ">
    @include('fantasy.brackets.home.bracket.matchup', ['matchup' => $round['matchup']])
    <div class="logo">
        <div class="medium playoffs-{!! FrameworkConfig::get('debear.setup.sport_ref') !!}-medium-0000 playoffs-{!! FrameworkConfig::get('debear.setup.sport_ref') !!}-medium-{!! FrameworkConfig::get('debear.setup.season.viewing') !!}"></div>
    </div>
</li>
