@php
    // Our groupings
    $divs = $groupings->where('type', 'div')->where('parent_id', $conf->grouping_id);
    $grouping_ids = array_merge([$conf->grouping_id], $divs->unique('grouping_id'));
    // The series to process
    $round_list = array_filter($template['rounds'], fn($r) => in_array($r['grouping'], ['div', 'conf']));
    if ($pos == 'right') {
        $round_list = array_reverse($round_list, true);
    }
    // Conference info
    $conf_css = ($conf->icon ?? $conf->grouping_id);
    $conf_icon = isset($conf->icon) ? 'misc-nhl-narrow-small-' . ($pos == 'left' ? '' : 'right-') . $conf->icon : '';
@endphp
<li class="rounds-{!! $tot_rounds !!} conf-{!! $pos !!}">
    <h3 class="conf row_conf-{!! $conf_css !!} {!! $conf_icon !!}"><full>{!! $conf->name_full !!}</full><short>{!! $conf->name !!}</short></h3>
    {{-- Top Division Title --}}
    @if ($div_rounds)
        <h3 class="div-top row_conf-{!! $conf_css !!}"><full>{!! $divs->first()->name_full !!}</full><short>{!! $divs->first()->name !!}</short></h3>
    @endif
    {{-- Matchups --}}
    <ul class="inline_list clearfix">
        @foreach ($round_list as $prefix => $round)
            <li class="round-{!! $prefix !!}">
                @foreach (array_filter($round['matchups'], fn($g) => in_array($g, $grouping_ids), ARRAY_FILTER_USE_KEY) as $group_id => $matchups)
                    @foreach ($matchups as $code => $matchup)
                        @include('fantasy.brackets.home.bracket.matchup')
                    @endforeach
                @endforeach
            </li>
        @endforeach
    </ul>
    {{-- Bottom Division Title --}}
    @if ($div_rounds)
        <h3 class="div-bot row_conf-{!! $conf_css !!}"><full>{!! $divs->last()->name_full !!}</full><short>{!! $divs->last()->name !!}</short></h3>
    @endif
</li>
