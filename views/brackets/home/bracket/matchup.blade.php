@php
    $class = FrameworkConfig::get("debear.sports.$sport.box") . '_box';
    // Identify the series from the matchup parameters
    $matchup_group = $series->whereIn('round_num', [$round['alt_round_num'] ?? -1, $prefix]); // Round number may be derived from an alternative source
    if ($round['grouping'] != 'champ') {
        // In some scenarios the database code may not match the expected round code
        if (isset($matchup['alt_codes'])) {
            $matchup_round = $matchup_group->whereIn('round_code', $matchup['alt_codes']);
        }
        // If not applicable, or the above query didn't return anything, perform a direct lookup from the higher seed
        if (!isset($matchup['alt_codes']) || !$matchup_round->count()) {
            $matchup_round = $matchup_group->where("higher_{$round['grouping']}_id", $group_id);
        }
        // From the list of all matchups in the round, identify this matchup based on the higher seed
        $last_grouping = (array_key_exists($prefix - 1, $round_list) ? $round_list[$prefix - 1]['grouping'] : $round_list[$prefix]['grouping']);
        if ($matchup['higher']['type'] == 'seed') {
            $matchup_db = $matchup_round->where("higher_{$last_grouping}_seed", $matchup['higher']['rank']);
        } else {
            $matchup_db = $matchup_round->sortBy("higher_{$last_grouping}_seed")->slice($matchup['higher']['rank'] - 1, 1);
        }
    } else {
        // For the championship round, we only have a single matchup to parse from
        $last_grouping = 'conf';
        $matchup_round = $matchup_db = $matchup_group;
    }
    // Determine if the higher/lower seeds in the database matches the order we need to render them
    $render_higher = 'higher';
    $render_lower = 'lower';
    if ($matchup_db->isset() && $matchup['higher']['type'] == 'winner') {
        $target = $matchup_round->where("higher_{$last_grouping}_id", $matchup['higher']['group_id'] ?? $group_id)
            ->sortBy("higher_{$last_grouping}_seed")
            ->slice($matchup['higher']['rank'] - 1, 1);
        if (!$target->isset() || $target->higher_seed != $matchup_db->higher_seed) {
            $render_higher = 'lower';
            $render_lower = 'higher';
        }
    }
    // Matchup winner?
    $winner = 'no-one';
    if ($matchup_db->isset() && $matchup_db->higher_score != $matchup_db->lower_score) {
        $high_score = max($matchup_db->higher_score, $matchup_db->lower_score);
        if ($high_score >= FrameworkConfig::get("debear.setup.playoffs.series.$prefix.wins")) {
            $winner = ($matchup_db->higher_score > $matchup_db->lower_score ? $render_higher : $render_lower);
        }
    }
    // Team label
    $labels = array_fill_keys(['higher', 'lower'], '&nbsp;');
    foreach (array_keys($labels) as $key) {
        $key_db = ($key == 'higher' ? $render_higher : $render_lower);
        $col_seed = "{$key_db}_seed";
        // Only proceed if the seed is known
        if ($matchup_db->$col_seed) {
            $col_seed_num = "{$key_db}_" . ($div_rounds ? 'div' : 'conf') . '_seed';
            // Seed prefix
            $seed_prefix = '#';
            $is_wildcard = false;
            if ($div_rounds) {
                // Is this team a wildcard?
                $is_wildcard = isset($template['div_teams']) && ($matchup_db->$col_seed_num > $template['div_teams']);
                // Get their division prefix
                $col_div_id = "{$key_db}_div_id";
                $seed_prefix = strtoupper(substr($groupings->where('grouping_id', $matchup_db->$col_div_id)->name, 0, 1));
            }
            // Main label
            $labels[$key] = (!$is_wildcard ? $seed_prefix . $matchup_db->$col_seed_num : 'WC') . "<team> {$matchup_db->$col_seed}</team>";
        }
    }
    // Matchup meta info
    $round_code = "$group_id-$code";
    $cfg_higher = join('-', [$matchup['higher']['type'], $matchup['higher']['group_id'] ?? $group_id, $matchup['higher']['rank']]);
    $cfg_lower = join('-', [$matchup['lower']['type'], $matchup['lower']['group_id'] ?? $group_id, $matchup['lower']['rank']]);
    $render_higher .= '_seed'; $render_lower .= '_seed';
@endphp
<fieldset class="{!! $class !!}" data-round="{!! $prefix !!}" data-round-code="{!! $round_code !!}" data-higher="{!! $cfg_higher !!}" data-lower="{!! $cfg_lower !!}">
    <dl>
        <dt class="higher {!! $winner == 'higher' ? 'winner' : ($winner == 'lower' ? 'loser' : '') !!}"><span>{!! $labels['higher'] !!}</span></dt>
        <dd class="higher {!! $winner == 'higher' ? 'winner' : ($winner == 'lower' ? 'loser' : '') !!}">{!! $matchup_db->$render_higher ? "<span class=\"team-{$sport}-small-{$matchup_db->$render_higher}\"></span>" : '' !!}</dd>
        <dd class="lower {!! $winner == 'lower' ? 'winner' : ($winner == 'higher' ? 'loser' : '') !!}">{!! $matchup_db->$render_lower ? "<span class=\"team-{$sport}-small-{$matchup_db->$render_lower}\"></span>" : '' !!}</dd>
        <dt class="lower {!! $winner == 'lower' ? 'winner' : ($winner == 'higher' ? 'loser' : '') !!}"><span>{!! $labels['lower'] !!}</span></dt>
    </dl>
</fieldset>
