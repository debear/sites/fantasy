@php
    Resources::addCSS('sports_icons.css');
    Resources::addCSS('_global/pages/sitemap.css');
    Resources::addCSS('_common/box_colours.css');
    Resources::addJS('_global/pages/sitemap.js');
    // Pre-process the sitemap into our split sections
    DeBear\Helpers\Fantasy\Homepage::processSubsites($sitemap);
    $game_statuses = [
        'active' => 'Active',
        'upcoming' => 'Upcoming',
        'archive' => 'Archived',
    ];
    $css = [];
    $logo_base = HTTP::buildDomain('static') . '/' . FrameworkConfig::get('debear.url.sub') . '/images/logos';
@endphp
@extends('skeleton.layouts.html')

@section('content')

<h1>Find your way round {!! FrameworkConfig::get('debear.names.site') !!}!</h1>

{{-- Individual subsites --}}
<ul class="inline_list subsites grid">
    @foreach ($sitemap['subsites'] as $key => $subsite)
        @php
            // Is this game active? If not, skip it.
            $game_box = FrameworkConfig::get("debear.subsites.{$subsite['config-ref']}.setup.box") ?? 'default';
            $url_slug = ltrim($subsite['url'], '/');
            $css[] = ".subsite .game-{$subsite['config-ref']} a.name { background-image: url(\"$logo_base/fantasy_{$url_slug}.png\"); }";
        @endphp
        <li class="subsite grid-cell grid-1-2 grid-tp-1-1 grid-m-1-1">
            <fieldset class="{!! "{$game_box}_box" !!} game-{!! $subsite['config-ref'] !!}">
                <a class="name" href="{!! $subsite['url'] !!}"></a>
                @foreach ($subsite['sub-games'] as $key => $subsite_games)
                    <dl class="subgames{!! $key == 'archive' ? ' closed' : '' !!}">
                        <dt>
                            {!! Format::pluralise($subsite_games->count(), "{$game_statuses[$key]} Game", ['hide-num' => true]) !!}
                            @if ($key == 'archive')
                                <div>[ <a class="toggle">Open</a> ]</div>
                            @endif
                        </dt>
                            @foreach ($subsite_games as $subsite_game)
                                <dd class="sports_{!! $subsite_game->sport !!}_icon"><a class="no_underline" href="{!! $subsite_game->full_url !!}">{!! $subsite_game->season !!}: {!! $subsite_game->name !!}</a></dd>
                            @endforeach
                    </dl>
                @endforeach
            </fieldset>
        </li>
    @endforeach
</ul>
<style {!! HTTP::buildCSPNonceTag() !!}>
    {!! join("\n", $css) !!}
</style>

{{-- Other links (inside the standard page toggle system) --}}
<div id="sitemap">
    <h2>Other Links</h2>
    <dl class="inline_list clearfix other">
        @foreach ($sitemap['admin'] as $admin)
            <dt><a href="{!! $admin['url'] !!}">{!! $admin['label'] !!}</a></dt>
                @if ($admin['title'])
                    <dd>{!! $admin['title'] !!}</dd>
                @endif
        @endforeach
    </dl>
</div>

@endsection
