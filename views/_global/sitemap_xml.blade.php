@php
    print '<'.'?xml version="1.0" encoding="UTF-8"?'.'>'; // Quirky to satisfy linting
@endphp
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($games as $url)
        <sitemap>
            <loc>https:{!! HTTP::buildDomain() !!}/{!! $url !!}/sitemap.xml</loc>
        </sitemap>
    @endforeach
</sitemapindex>
