@php
    $holder_pluralised = Format::pluralise($game['model']->hasMultipleRecordHolders() ? 2 : 1, 'Holder', ['hide-num' => true]);
@endphp
@if ($status != 'pre-season')
    <a class="image-link" href="{!! $game['model']->full_url !!}"></a>
@else
    <div class="image-link"></div>
@endif
<h3 class="record">Record: {!! $game['model']->name !!}</h3>
<p class="descrip">{!! $game['model']->description !!}</p>
<div class="detail">
    {{-- Holder / Leader Mugshot --}}
    @if ($status != 'off-season')
        <img alt="Mugshot of the current Record {!! $holder_pluralised !!}" src="{!! $game['model']->recordHolderURL('medium') !!}">
    @else
        <img alt="Mugshot of the active {!! FrameworkConfig::get("debear.fantasy.sports.{$sport}.name_short") !!} leader" src="{!! $game['model']->active?->imageURL() !!}">
    @endif
    {{-- Game Info --}}
    @if ($status != 'pre-season')
        <div class="progress">
            @include('fantasy.records.home.widgets.progress_bars', ['game' => $game['model'], 'entry' => $game['entry']])
        </div>
    @else
        <div class="holders">
            <h3 class="invert">Current Record {!! $holder_pluralised !!}</h3>
            <ul class="inline_list clearfix">
                @foreach (explode('; ', $game['model']->record_holder ?? '') as $holder)
                    <li {!! $game['model']->hasMultipleRecordHolders() ? 'class="multi"' : '' !!}>{!! $holder ?: '<em>No-one yet, become the first!</em>' !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
{{-- CTA --}}
@if ($status != 'pre-season')
    <div class="text-link cta"><a href="{!! $game['model']->full_url !!}">View Game {!! $status == 'active' ? 'Details' : 'Archive' !!}</a></div>
@endif
