@extends('skeleton.layouts.html')

@section('content')

<h1>Welcome to {!! FrameworkConfig::get('debear.names.site') !!}!</h1>

@foreach ($sports as $status => $status_sports)
    @includeWhen($status != 'active', "fantasy._global.home.titles.$status")
    @foreach ($status_sports as $sport => $games)
        <fieldset class="sport {!! FrameworkConfig::get("debear.sports.$sport.box") !!}_box">
            <h3 class="invert sport sports_{!! $sport !!}_icon">{!! FrameworkConfig::get("debear.sports.subsites.$sport.names.section-full") ?? FrameworkConfig::get("debear.sports.subsites.$sport.names.section") !!}</h3>
            <ul class="inline_list games grid">
                @foreach ($games as $game)
                    <li class="{!! $game['code'] !!} grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                        <fieldset class="sport {!! FrameworkConfig::get("debear.sports.$sport.box") !!}_box">
                            @includeIf("fantasy._global.home.{$game['code']}")
                        </fieldset>
                    </li>
                @endforeach
            </ul>
        </fieldset>
    @endforeach
@endforeach

@endsection
