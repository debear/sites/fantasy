@php
    $default_id = $articles->first()->section_id;
    // Dropdown for mobile/tablet view
    $opt = [];
    foreach ($articles as $section) {
        $opt[] = ['id' => $section->section_id, 'label' => $section->name];
    }
    $dropdown_section = new Dropdown('article-section', $opt, [
        'value' => $default_id,
        'select' => false,
    ]);
@endphp
@extends('skeleton.layouts.html')

@section('content')

<h1>Help and Support</h1>

<articles class="grid">
    {{-- Section headers - side list on desktop --}}
    <ul class="inline_list grid-3-10 hidden-t hidden-m">
        @foreach ($articles as $section)
            <li class="section {!! $section->section_id == $default_id ? 'sel' : 'unsel' !!}" data-section-id="{!! $section->section_id !!}">{!! $section->name !!}</li>
        @endforeach
    </ul>
    {{-- Section headers - dropdown on mobile/tablet --}}
    <section-dropdown class="grid-1-1 hidden-d">
        {!! $dropdown_section->render() !!}
    </section-dropdown>
    {{-- Articles by section --}}
    @foreach ($articles as $section)
        <div class="section grid-7-10 grid-t-1-1 grid-m-1-1 {!! $section->section_id == $default_id ? '' : 'hidden' !!}" data-section-id="{!! $section->section_id !!}">
            <dl class="article">
                @foreach ($section->articles as $article)
                    <dt class="icon_toggle_plus" data-article-id="{!! $article->section_id !!}-{!! $article->article_id !!}">{!! $article->title !!}</dt>
                        <dd class="hidden" data-article-id="{!! $article->section_id !!}-{!! $article->article_id !!}">{!! $article->body !!}</dd>
                @endforeach
            </dl>
        </div>
    @endforeach
</articles>

@endsection
