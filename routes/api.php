<?php

/**
 * API routes for the fantasy sub-domain
 */

// Fantasy Record Breakers.
if (FrameworkConfig::get('debear.fantasy.subsites.records.enabled')) {
    require 'api/records/v1.0.php';
}

// Player profiles.
require 'api/profile/v1.0.php';
