<?php

/**
 * Routes for Fantasy Record Breakers
 */

Route::prefix('records')->group(function () {
    // Our unit tests cover a decade before the live instances.
    $decade_range = (HTTP::isTest() ? '1-3' : '2-3');
    $sport_regex = '(mlb|nfl|nhl|ahl|f1|sgp)'; // Explicit list because it's used as part of the path.

    // The homepage.
    Route::get('/', 'Fantasy\Records@index');

    // A single game view.
    Route::get('/{sport}/{slug}-{season}', 'Fantasy\Records\Game@index')
        ->where('sport', $sport_regex)
        ->where('slug', '[\w\-]+')
        ->where('season', "20[$decade_range]\\d");
    Route::get('/{sport}/{slug}-{season}/leaderboard', 'Fantasy\Records\Game@leaderboard')
        ->where('sport', $sport_regex)
        ->where('slug', '[\w\-]+')
        ->where('season', "20[$decade_range]\\d");

    // Team selections.
    foreach (['get' => 'index', 'post' => 'store', 'patch' => 'update', 'delete' => 'destroy'] as $verb => $action) {
        Route::$verb('/{sport}/{slug}-{season}/periods/{period_id}', 'Fantasy\Records\EntrySel@' . $action)
            ->where('sport', $sport_regex)
            ->where('slug', '[\w\-]+')
            ->where('season', "20[$decade_range]\\d")
            ->where('period_id', '\d{1,3}')
            ->middleware('auth:user');
    }

    // Individual selections.
    Route::get('/{sport}/{slug}-{season}/links/{link_slug}-{link_id}/{period_id?}', 'Fantasy\Records\Selection@show')
        ->where('sport', $sport_regex)
        ->where('slug', '[\w\-]+')
        ->where('season', "20[$decade_range]\\d")
        ->where('link_slug', '[\w\-]+')
        ->where('link_id', '\d+')
        ->where('period_id', '\d{1,3}');

    // Create / Modify an entry.
    Route::match(['get', 'post', 'patch'], '/my-entry', 'Fantasy\Records\Entry@index');

    // Join / Leave a game.
    foreach (['post' => 'store', 'delete' => 'destroy'] as $verb => $action) {
        Route::$verb('/my-entry/{sport}-{season}-{game_id}', 'Fantasy\Records\EntryGame@' . $action)
            ->where('season', "20[$decade_range]\\d")
            ->where('game_id', '\d+')
            ->middleware('auth:user');
    }

    // Team summary.
    Route::get('/{sport}/{slug}-{season}/teams/{team_slug}-{team_id}', 'Fantasy\Records\EntryGame@summary')
        ->where('sport', $sport_regex)
        ->where('slug', '[\w\-]+')
        ->where('season', "20[$decade_range]\\d")
        ->where('team_slug', '[\w\-]+')
        ->where('team_id', '\d+');

    // Game help.
    Route::get('/help', 'Fantasy\Records\Help@index');

    // Site tools.
    Route::get('/sitemap.xml', 'Fantasy\Records\SiteTools@sitemapXml');
    Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');
});
