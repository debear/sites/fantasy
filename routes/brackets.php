<?php

/**
 * Request routing (sport agnostic) for Bracket Challenges
 * Uses $config loaded from the web/routes.php, containing the subsite's config details
 */

// Generic/Secondary game pages.
Route::prefix($config['url'])->group(function () use ($config) {
    // The homepage (without season is redirected to the endpoint including the season).
    Route::get('/', function () use ($config) {
        // Do not consider this as permanent redirect, since the target will change each season.
        return redirect("{$config['url']}/{$config['setup']['season']['max']}", 307);
    });

    // Game help.
    Route::get('/help', 'Fantasy\Brackets\Help@index');

    // Site tools.
    Route::get('/sitemap.xml', 'Fantasy\Brackets\SiteTools@sitemapXml');
    Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');
});

// Per-season game pages.
Route::prefix("{$config['url']}/{season}")->where(['season' => '20[0-3]\d'])->group(function () {
    Route::get('/', 'Fantasy\Brackets@index');

    // Create / Modify an entry.
    Route::match(['get', 'post', 'patch'], '/my-entry', 'Fantasy\Brackets\Entry@index');

    // Site tools.
    Route::get('/manifest.json', 'Fantasy\Brackets\SiteTools@manifest');
});
