<?php

/**
 * Routes for the fantasy sub-domain
 */

// Game-agnostic: Home.
Route::get('/', 'Fantasy\Home@index');
Route::redirect('/html/', '/', 308);
Route::redirect('/html/index', '/', 308);
Route::redirect('/html/index.php', '/', 308);

// Player profiles.
Route::get('/profile/{user_id}', 'Fantasy\Profile@show');

// Game sitemaps.
Route::get('/sitemap.games.xml', 'Fantasy\SiteTools@sitemapXmlGames');

// Sub-games (if relevant).
foreach (FrameworkConfig::get('debear.fantasy.subsites') as $ref => $config) {
    // Only load routes for active games.
    if (!isset($config['enabled']) || !$config['enabled']) {
        continue;
    }
    // Determine the config path based on the config info.
    $path = "$ref.php";
    if (isset($config['group'])) {
        $ref = str_replace("{$config['group']}-", '', $ref);
        $path_ref = "{$config['group']}/$ref.php";
        // Attempt a ref-specific route file, but if none exists then fallback to group-level defined routes.
        $path = file_exists($path) ? $path_ref : "{$config['group']}.php";
    }
    // Load the routes.
    require $path;
}
