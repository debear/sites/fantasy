<?php

/**
 * v1.0 of the Fantasy Record Breakers API
 */

Route::prefix('records/v1.0')->group(function () {
    // Our unit tests cover a decade before the live instances.
    $decade_range = (HTTP::isTest() ? '1-3' : '2-3');
    $sport_regex = '(mlb|nfl|nhl|ahl|f1|sgp)'; // Explicit list because it's part of the path.

    // List of games.
    Route::get('/games', 'Fantasy\Records\API\Game@index')
        ->middleware(['auth.token:records-read', 'throttle:generic_read']);
    Route::get('/games/{status}', 'Fantasy\Records\API\Game@index')
        ->where('status', 'upcoming|open|inprogress|closed|active|complete')
        ->middleware(['auth.token:records-read', 'throttle:generic_read']);

    // Individual games.
    Route::get('/games/{sport}-{season}-{game_id}', 'Fantasy\Records\API\Game@show')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+');
    Route::get('/games/{sport}-{season}-{game_id}/rules', 'Fantasy\Records\API\Game@rules')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+');
    Route::get('/games/{sport}-{season}-{game_id}/achievements', 'Fantasy\Records\API\Game@achievements')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+');
    Route::get('/games/{sport}-{season}-{game_id}/periods/{type?}', 'Fantasy\Records\API\Game@periods')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+')
        ->where('type', '(current)');
    Route::get('/games/{sport}-{season}-{game_id}/periods/{period_id}', 'Fantasy\Records\API\EntrySel@index')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+')
        ->where('period_id', '\d{1,3}');
    Route::get('/games/{sport}-{season}-{game_id}/links/{link_id}/{period_id?}', 'Fantasy\Records\API\Selection@show')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+')
        ->where('link_id', '\d+')
        ->where('period_id', '\d{1,3}');
    Route::get('/games/{sport}-{season}-{game_id}/leaderboard/{type?}', 'Fantasy\Records\API\Game@leaderboard')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+')
        ->where('type', '(current|overall|hitrate)');

    // Entries.
    Route::get('/entries/{user_id}', 'Fantasy\Records\API\Entry@show')
        ->middleware(['auth.token:records-read|records-admin', 'throttle:generic_read']);
    Route::post('/entries/{user_id}', 'Fantasy\Records\API\Entry@store')
        ->middleware(['auth.token:records-write|records-admin', 'throttle:generic_write']);
    Route::patch('/entries/{user_id}', 'Fantasy\Records\API\Entry@update')
        ->middleware(['auth.token:records-write|records-admin', 'throttle:generic_write']);

    // Entry game info.
    Route::get('/entries/{user_id}/{sport}-{season}-{game_id}', 'Fantasy\Records\API\EntryGame@show')
        ->middleware(['auth.token:records-read', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+');
    // Entry game management.
    foreach (['post' => 'store', 'delete' => 'destroy'] as $verb => $action) {
        Route::$verb('/entries/{user_id}/{sport}-{season}-{game_id}', "Fantasy\Records\API\EntryGame@$action")
            ->middleware(['auth.token:records-write|records-admin', 'throttle:generic_write'])
            ->where('sport', $sport_regex)
            ->where('season', "20[$decade_range]\\d")
            ->where('game_id', '\d+');
    }
    // Entry achievements.
    foreach (['achievements', 'opponents'] as $action) {
        Route::get("/entries/{user_id}/{sport}-{season}-{game_id}/$action", "Fantasy\Records\API\EntryGame@$action")
            ->middleware(['auth.token:records-read|records-admin', 'throttle:generic_read'])
            ->where('sport', $sport_regex)
            ->where('season', "20[$decade_range]\\d")
            ->where('game_id', '\d+');
    }
    // Entry selections.
    Route::get('/entries/{user_id}/{sport}-{season}-{game_id}/{period_id}', 'Fantasy\Records\API\EntrySel@show')
        ->middleware(['auth.token:records-read|records-admin', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+')
        ->where('period_id', '\d{1,3}');
    Route::get('/entries/{user_id}/{sport}-{season}-{game_id}/selections', 'Fantasy\Records\API\EntryGame@selections')
        ->middleware(['auth.token:records-read|records-admin', 'throttle:generic_read'])
        ->where('sport', $sport_regex)
        ->where('season', "20[$decade_range]\\d")
        ->where('game_id', '\d+');
    foreach (['post' => 'store', 'patch' => 'update', 'delete' => 'destroy'] as $verb => $action) {
        $method = 'Fantasy\Records\API\EntrySel@' . $action;
        Route::$verb('/entries/{user_id}/{sport}-{season}-{game_id}/{period_id}', $method)
            ->middleware(['auth.token:records-write|records-admin', 'throttle:generic_write'])
            ->where('sport', $sport_regex)
            ->where('season', "20[$decade_range]\\d")
            ->where('game_id', '\d+')
            ->where('period_id', '\d{1,3}');
    }

    // Game help.
    Route::get('/help', 'Fantasy\Records\API\Help@index')
        ->middleware(['auth.token:records-read', 'throttle:generic_read']);
});
