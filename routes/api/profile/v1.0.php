<?php

/**
 * v1.0 of the Player Profile API
 */

Route::prefix('profile/v1.0')->group(function () {
    Route::get('/{user_id}', 'Fantasy\Profile\API\User@show')
        ->middleware(['auth.token:profiles-read', 'throttle:generic_read']);
});
