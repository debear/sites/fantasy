#!/bin/bash
# Script to mock moving Record Breakers games forward a day

# Load our helpers
dir_base=$(realpath $(dirname $0))
. $dir_base/helpers
. $dir_base/config

# Propagate an output dir, if one has been passed
if [ ! -z "$DIR_OUTPUT" ]; then
    dir_output="$DIR_OUTPUT"
    dir_tmp="$dir_output/$subdir_tmp"; export DIR_TMP="$dir_tmp"
    dir_output_api="$dir_output/$subdir_api"
    dir_output_screenshots="$dir_output/$subdir_screenshots"
    dir_output_data="$dir_output/$subdir_data"
    dir_output_log="$dir_output/$subdir_log"; export DIR_LOG="$dir_output_data"
fi

# Roll the date forward in the override
prev_date=$(override_get)
curr_date=$(date '+%F' -d "$prev_date + 1 day")
log_info "- Running as \e[95m$curr_date\e[0m"
override_set "$curr_date"

# Determine affected games
games_running=$(echo "SELECT CONCAT(sport, '-', season, '-', game_id, ':', '/', sport, '/', LOWER(name), '-', season) AS game_info
FROM FANTASY_RECORDS_GAMES
WHERE '$curr_date' >= DATE(date_open)
AND   '$curr_date' < DATE(DATE_ADD(date_complete, INTERVAL 1 DAY))
ORDER BY sport, season, game_id;" | $cmd_mysql_fantasy | sed -r 's/[, ]+/-/g')
if [ -z "$games_running" ]; then
    log_info "  - Skip processing, no active games found"
    exit
fi

# Run the processing script for each sport
sports=$(echo "$games_running" | awk -F'-' '{ print $1 }' | sort -n | uniq)
for sport in $sports; do
    log_info "  - Running process calcs for \e[36m$sport\e[0m"
    [[ ! -z "$dir_output_log" ]] && log_proc="$dir_output_log/$subdir_data/$curr_date-$sport.log.gz" || log_proc="/dev/null"
    cmd_stderr=$($cmd_process $sport $curr_date --log-dir="$curr_date" | gzip 2>&1 >$log_proc)
    log_exit $? "$cmd_stderr"

    # Emulate syncing the changes to remote
    if [ ${ff['server_sync']} -eq 1 ]; then
        sync_app="fantasy_records_${sport}_scoring"
        log_info "  - Running sync upload for \e[36m$sync_app\e[0m"
        cmd_stderr=$($cmd_sync $sync_app data --up 2>&1 >/dev/null)
        log_exit $? "$cmd_stderr"
    fi
done

# Make selections for our user
if [ ${ff['api_selections']} -eq 1 ] && [ ! -z "$dir_output_api" ]; then
    games_active=$(echo "SELECT CONCAT(sport, '-', season, '-', game_id) AS game_info
    FROM FANTASY_RECORDS_GAMES
    WHERE '$curr_date' BETWEEN DATE(date_start) AND DATE(date_end)
    ORDER BY sport, season, game_id;" | $cmd_mysql_fantasy)
    if [ ! -z "$games_active" ]; then
        log_info "  - Processing user selections"
        for game_ref in $games_active; do
            log_info "    - \e[36m$game_ref\e[0m"
            api_make_selection $game_ref
        done
    fi
fi

# Take the appropriate screenshots
if [ ! -z "$dir_output_screenshots" ]; then
    for type in guest user; do
        if [ ${ff["screenshot_$type"]} -eq 0 ]; then
            # Skip if we do not want to take these screenshots
            continue
        fi

        # Proceed
        declare -a cmd_args
        cmd_args=()

        # If in user mode, we need to ensure they are logged in
        cmd_home_extra=
        if [ $type = 'user' ]; then
            cmd_home_extra=", \"login\": \"$api_user_id:$api_user_pass\""
        fi

        # Home
        labels='\e[36mhomepage\e[0m'
        if [ ${ff['screenshots_desktop']} -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url\", \"dst\": \"$dir_output_screenshots/$type/home/desktop/$curr_date.png\", \"width\": 1024, \"descrip\": \"Homepage (Desktop)\"$cmd_home_extra}" ); cmd_home_extra=; fi
        if [ ${ff['screenshots_tabland']} -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url\", \"dst\": \"$dir_output_screenshots/$type/home/tabland/$curr_date.png\", \"width\": 912, \"descrip\": \"Homepage (Landscape Tablet)\"$cmd_home_extra}" ); cmd_home_extra=; fi
        if [ ${ff['screenshots_tabport']} -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url\", \"dst\": \"$dir_output_screenshots/$type/home/tabport/$curr_date.png\", \"width\": 648, \"descrip\": \"Homepage (Portrait Tablet)\"$cmd_home_extra}" ); cmd_home_extra=; fi
        if [ ${ff['screenshots_mobile']}  -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url\", \"dst\": \"$dir_output_screenshots/$type/home/mobile/$curr_date.png\", \"width\": 440, \"descrip\": \"Homepage (Mobile)\"$cmd_home_extra}" ); cmd_home_extra=; fi
        # By game
        for g in $games_running; do
            game_ref=$(echo "$g" | awk -F':' '{ print $1 }')
            game_url=$(echo "$g" | awk -F':' '{ print $2 }')

            labels="$labels, \e[36m$game_ref\e[0m"
            if [ ${ff['screenshots_desktop']} -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url$game_url\", \"dst\": \"$dir_output_screenshots/$type/$game_ref/desktop/$curr_date.png\", \"width\": 1024, \"descrip\": \"$game_ref (Desktop)\"}" ); fi
            if [ ${ff['screenshots_tabland']} -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url$game_url\", \"dst\": \"$dir_output_screenshots/$type/$game_ref/tabland/$curr_date.png\", \"width\": 912, \"descrip\": \"$game_ref (Landscape Tablet)\"}" ); fi
            if [ ${ff['screenshots_tabport']} -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url$game_url\", \"dst\": \"$dir_output_screenshots/$type/$game_ref/tabport/$curr_date.png\", \"width\": 648, \"descrip\": \"$game_ref (Portrait Tablet)\"}" ); fi
            if [ ${ff['screenshots_mobile']}  -eq 1 ]; then cmd_args+=( "{\"src\": \"$records_url$game_url\", \"dst\": \"$dir_output_screenshots/$type/$game_ref/mobile/$curr_date.png\", \"width\": 440, \"descrip\": \"$game_ref (Mobile)\"}" ); fi
        done

        # If we logged in, we should also log out
        if [ $type = 'user' ]; then
            last=$(( ${#cmd_args[@]}-1 ))
            cmd_args[$last]=$(echo "${cmd_args[$last]}" | sed 's/\}$/, "logout": true}/g')
        fi

        # Now make the call to Puppeteer
        log_info "  - Screenshotting $type $labels"
        cmd_stderr=$($cmd_screenshot "${cmd_args[@]}" | gzip 2>&1 >"$dir_output_log/$subdir_screenshots/$curr_date-$type.log.gz")
        log_exit $? "$cmd_stderr"
    done
fi

# Run the game-end email script
if [ ${ff['game_end_email']} -eq 1 ]; then
    log_info "  - Running game-end email script"
    [[ ! -z "$dir_output_log" ]] && log_proc="$dir_output_log/$subdir_email_gameend/$curr_date.log.gz" || log_proc="/dev/null"
    cmd_stderr=$($cmd_artisan debear:fantasy:records-email-gameend | gzip 2>&1 >$log_proc)
    log_exit $? "$cmd_stderr"
fi

# Confirm completion!
log_info "  - Rollover complete!"
