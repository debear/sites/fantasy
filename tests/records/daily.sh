#!/bin/bash
# Test script to mock running through Record Breakers on a daily basis

# Load our helpers and config
dir_base=$(realpath $(dirname $0))
. $dir_base/helpers
. $dir_base/setup
. $dir_base/steps
. $dir_base/config

# Parse command line arguments to customise the run
parse_args "$@"

# Determine which sports and when we'll be running
setup_sports_dates

# State the run parameters and ask user for confirmation before proceeding
run_intro

# Actions to perform upon script exit
function tidy_on_exit() {
    override_revert
    run_rollback_env
    [ -e "$dir_output/run.log" ] && gzip $dir_output/run.log
}

# Define our additional / specific config
dir_output="$dir_base/runs/$cur_date-$$"
dir_tmp="$dir_output/$subdir_tmp"
dir_output_api="$dir_output/$subdir_api"
dir_output_screenshots="$dir_output/$subdir_screenshots"
dir_output_data="$dir_output/$subdir_data"
dir_output_opengraph="$dir_output/$subdir_opengraph"
dir_output_log="$dir_output/$subdir_log"
dir_output_db="$dir_output/$subdir_db"
# Create our standard directories
mkdir -p $dir_tmp \
    $dir_output_data \
    $dir_output_opengraph \
    $dir_output_log/{$subdir_php,$subdir_opengraph,$subdir_data,$subdir_email_gameend} \
    $dir_output_db
chgrp apache $dir_output_log/$subdir_php
chmod g+w $dir_output_log/$subdir_php
# And those directories required by feature flag
if [ ${ff['api_selections']} -eq 1 ]; then mkdir -p $dir_output_api/admin; fi
if [ ${ff['screenshot_guest']} -eq 1 ] || [ ${ff['screenshot_user']} -eq 1 ]; then mkdir -p $dir_output_log/$subdir_screenshots; fi
if [ ${ff['screenshot_guest']} -eq 1 ]; then setup_dir_screenshots 'guest' 'home'; fi
if [ ${ff['screenshot_user']} -eq 1 ]; then setup_dir_screenshots 'user' 'home'; fi

setup_logging

log_info 'Starting'

#
# Step 1: Setup the environment
#
run_step_env

#
# Step 2: Ensure automated users exist
#
run_step_autouser

#
# Step 3: Apply the core Record Breakers config
#
run_step_setup

#
# Step 4: The main event - step through each day, processing as required
#
log_info "Emulating the date range \e[95m$start_date\e[0m to \e[95m$end_date\e[0m"

# As the rollover moves forward from the date in _env.php, set it to the day before $start_date
init_date=$(date '+%F' -d "$start_date - 1 day")
log_info "- Setting initial _env.override from \e[95m$init_override\e[0m to \e[95m$init_date\e[0m"
override_set "$init_date"

# Now loop through the updates
curr_date=$start_date
days_run=0
games_loaded=
while [ $(date -d "$curr_date" +%s) -lt $(date -d "$end_date" +%s) ]; do
    # Run our rollover command
    DIR_OUTPUT="$dir_output" $dir_base/rollover.sh
    retcode=$?
    if [ $retcode -ne 0 ] && [ ${ff['abort_rollover_fail']} -eq 1 ]; then exit_on_error $retcode; fi
    curr_date=$(override_get)

    # Are we loading a sport on this day?
    if [ ! -z "${sports_load[$curr_date]}" ] && [ "x${sports_load[$curr_date]}" != 'x' ]; then
        for sport in ${sports_load[$curr_date]}; do
            season=$(date '+%Y' -d "$curr_date")
            sql_setup="$dir_setup/$sport/master.sql"
            log_info "  - Loading sport \e[36m$sport\e[0m"
            sed -r \
                -e "s/\b(debearco_common)/$db_common/" \
                -e "s/\b(debearco_admin)/$db_admin/" \
                -e 's/^#TEST#//g' $sql_setup | $cmd_mysql_fantasy --table

            # Some directory processing if we're storing screenshots
            games=$(echo "SELECT CONCAT(sport, '-', season, '-', game_id) AS game_ref
            FROM FANTASY_RECORDS_GAMES
            WHERE sport = '$sport'
            AND   season = '$season';" | $cmd_mysql_fantasy)
            for game_ref in $games; do
                games_loaded="$games_loaded$game_ref "
                if [ ${ff['screenshot_guest']} -eq 1 ]; then setup_dir_screenshots 'guest' "$game_ref"; fi
                if [ ${ff['screenshot_user']} -eq 1 ]; then setup_dir_screenshots 'user' "$game_ref"; fi
            done

            # Open Graph preview image
            log_info '    - Open Graph preview image creation'
            cmd_stderr=$(DIR_TMP="$dir_tmp" php $dir_skel/artisan debear:fantasy:records-og-preview --sport="$sport" --season="$season" | gzip 2>&1 >"$dir_output_log/$subdir_opengraph/$sport-$season.log.gz")
            log_exit $? "$cmd_stderr"
        done
    fi

    # Determine if there are any games for our user to join
    if [ ${ff['api_selections']} -eq 1 ]; then
        games=$(echo "SELECT CONCAT(sport, '-', season, '-', game_id) AS game_ref
        FROM FANTASY_RECORDS_GAMES
        WHERE DATE(date_open) = '$curr_date';" | $cmd_mysql_fantasy)
        if [ ! -z "$games" ]; then
            log_info '  - Joining game(s):'
            for game_ref in $games; do
                log_info "    - \e[36m$game_ref\e[0m"
                api_join_game "$game_ref"
            done
        fi
    fi

    # Report on Tweets queued
    daily_counts_tweets

    # Report on emails queued
    daily_counts_emails

    # Archive a game upon its completion
    if [ ${ff['archive_games']} -eq 1 ]; then
        sports=$(echo "SELECT DISTINCT CONCAT(sport, '-', season) AS sport_ref
        FROM FANTASY_RECORDS_GAMES
        WHERE DATE(date_complete) = DATE_SUB('$curr_date', INTERVAL 1 DAY);" | $cmd_mysql_fantasy)
        if [ ! -z "$sports" ]; then
            log_info '  - Archiving game(s):'
            for sport_ref in $sports; do
                log_info "    - \e[36m$sport_ref\e[0m"
                archive_game "$sport_ref"
            done
        fi
    fi

    # Ensure we do not overrun...
    days_run=$(( $days_run + 1 ))
    if [ $days_run -gt 1000 ]; then
        # We are running further than we expect to be, so something appears to be wrong with the loop. Abort, and write to both log and STDERR
        err="ABORTING: This script does not appear to have terminated by the point we would have expected it to"
        log_exit 99 "$err"
    fi
done

# Preserve artefacts that were created centrally by (and for) this run
log_info '- Preserving artefacts:'
if [ ! -z "$games_loaded" ]; then
    log_info "  - Open Graph images"
    for game_ref in $games_loaded; do
        log_info "    - \e[36m$game_ref\e[0m"
        mv $dir_og/$game_ref* $dir_output_opengraph
    done
fi
log_info "  - Databases:"
log_info "    - \e[95m$db_fantasy\e[0m"
$cmd_mysqldump_fantasy | gzip >$dir_output_db/$db_fantasy.sql.gz
log_info "    - \e[95m$db_common\e[0m"
$cmd_mysqldump_common | gzip >$dir_output_db/$db_common.sql.gz

# Report on possible issues in web application logs
report_slow_queries
report_http_responses

# Revert any temporary environment changes we may have made
tidy_on_exit
rm -rf $dir_tmp # We'll only remove this on success - in case of error we should keep it for analysis

# Confirm completion!
log_info 'Run complete!'
