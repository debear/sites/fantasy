#!/bin/bash
# A stub for converting HTML to an image for use in CI jobs

# Some config
file_size=4124

# Loop through the JSON argument(s)
while :; do
    if [ -z "$1" ]; then
        # End of loop
        break
    fi

    # Parse the arguments
    file_in=$(echo "$1" | jq -r '.src' | sed -r s'/^file:\/\///')
    file_out=$(echo "$1" | jq -r '.dst')

    # Present back what we were given
    echo "Converting '$file_in' to '$file_out'"

    # List information about the input file
    echo -e "\nInput file:"
    if [ ! -e $file_in ]; then
        echo "- Not found"
        echo "ERROR: Input file '$file_in' does not exist" >&2
        exit 1
    fi
    ls -l $file_in

    # Generate random output file
    dir_out=$(dirname $file_out)
    echo -e "\nOutput file:"
    if [ ! -e $dir_out ]; then
        echo "- Cannot write to '$dir_out'"
        echo "ERROR: Output file cannot be written to '$dir_out'" >&2
        exit 1
    fi

    head -c $file_size </dev/urandom >$file_out
    if [ ! -e $file_out ]; then
        echo "ERROR: There was an error writing to '$file_out'" >&2
        exit 1
    fi
    ls -l $file_out

    # Iterate to the next option
    shift
done
