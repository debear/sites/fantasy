#!/bin/bash
# Useful functions to be defined outside of the main run file
# TD. 03/11/2010.

#
# Display functions
#
# The main worker method
function _display_worker() {
    # Remaining args
    colour="$1"
    text="$2"
    text_plain="$3"
    echo_flag="$4"

    if [ -t 1 ]
    then
        echo -e ${echo_flag} "${colour}${text}\e[0m"
    else
        echo ${echo_flag} "${text_plain}"
    fi
}

# Display a main title
function display_title() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[1m\e[35m' "$1" "$1" $echo_flag
}

# Display a section title
function display_section() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[1m\e[34m' "$1" "$1" $echo_flag
}

# Display a subsection title
function display_subsection() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[34m' "$1" "$1" $echo_flag
}

# Display an information message
function display_info() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[1m\e[36m' "$1" "$1" $echo_flag
}

# Display a successful message
function display_success() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[1m\e[42m\e[97m' " $1 " "$1" $echo_flag
}

# Display an error message
function display_error() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[1m\e[41m\e[97m' " Error: $1 " "Error: $1" $echo_flag
}

# Display a warning message
function display_warning() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[1m\e[46m\e[97m' " Warning: $1 " "Warning: $1" $echo_flag
}

# Display a message in greep text, default background
function display_green() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[32m' "$1" "$1" $echo_flag
}

# Display a message in yellow text, default background
function display_yellow() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[33m' "$1" "$1" $echo_flag
}

# Display a message in red text, default background
function display_red() {
    if [ "x${1}" = 'x-n' ]; then echo_flag='-n'; shift; else echo_flag=''; fi
    _display_worker '\e[31m' "$1" "$1" $echo_flag
}
