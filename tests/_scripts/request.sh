#!/bin/bash

# Load our standard helper methods
dir_scripts=$(dirname $0)
. $dir_scripts/helpers.sh

git_repo=$($dir_scripts/git.sh repo)
[[ $git_repo != 'www' ]] && subdomain="$git_repo." || subdomain=""
domain="${subdomain}debear.dev"

apikey_dir="/var/www/debear/etc/passwd/api/debear/$git_repo"

# Helper method indicating how to use this script
function usage() {
  fh=$1
  cat <<EOF >$fh
Usage: $0 method endpoint (apikey) (data)

Arguments:
- method:   HTTP Method to be used in the request.
- endpoint: The endpoint to be used as the request.
- apikey:   (Optional) Reference to a specific API Key to be used in this request.
- data:     (Optional) Data to be passed in with the request - either raw data or link to a file. Expects the HTTP Method to be "POST".
EOF
}

function invalid_args() {
  display_error "$1" >&2
  usage /dev/stdout
  exit 1
}

# Get and validate the required arguments
method="$1"; shift
if [ -z "$method" ] || [ $(echo "$method" | grep -Pc '^(GET|POST|PUT|PATCH|DELETE|HEAD)$') -eq 0 ]; then
  invalid_args 'Missing or invalid HTTP Method'
fi
endpoint="$1"; shift
if [ -z "$endpoint" ]; then
  invalid_args 'Missing URL endpoint'
fi

# Load the optional arguments
apikey=default
form_data=
for arg in "$@"; do
  if [ -z "$arg" ]; then
    # Silently ignore empty arguments
    shift
    continue
  fi

  # Load an API Key for API requests?
  if [ "x$apikey" = 'xdefault' ] && [ ! -z $(echo "$endpoint" | grep -P '^\/api\/') ] && [ -e "$apikey_dir/$1" ]; then
    apikey="$1"

  # Load request data
  elif [ -z "$form_data" ]; then
    form_data="$1"

  # Unknown
  else
    invalid_args "Unknown argument '$1'"
  fi

  shift
done

# Configure our output directories (having ensured validation is okay)
outdir="/tmp/debear/api/$$"
mkdir -p $outdir
output_headers="$outdir/request.info"
output_response="$outdir/request.out"

# Manipulate the optional args
accept_json=0
curl_user=
if [ ! -z $(echo "$endpoint" | grep -P '^\/api\/') ]; then
  apikey_file="$apikey_dir/$apikey"
  if [ "x$apikey" = 'xdefault' ]; then
    apikey=$(basename $(realpath $apikey_file))
  fi
  accept_json=1
  curl_user="--user $apikey:$(cat $apikey_file)"
fi
curl_data=
if [ ! -z "$form_data" ]; then
  if [ ! -e "$form_data" ]; then
    # Passing a literal, which we'll save to a temporary file
    input_data="$outdir/request.in"
    echo "$form_data" >$input_data
    form_data=$input_data
  fi
  curl_data="--data @$form_data"
  # If the file contains JSON, set that as the content type
  if [ $(file -b $form_data | grep -Fc 'JSON') -ne 0 ]; then
    accept_json=1
    curl_data="--header Content-Type:application/json $curl_data"
  fi
fi

# Apply some custom headers that could be triggered from multiple sources
curl_headers=
if [ $accept_json -eq 1 ]; then
  curl_headers="$curl_headers --header Accept:application/json"
fi

# State what we'll be doing
display_title -n "Request: "
display_info "$method $endpoint"

# Make our requesst
curl --user-agent 'DeBear Requestor/1.0' --silent \
  --request $method https://${domain}${endpoint} \
  $curl_user $curl_data $curl_headers \
  --output $output_response \
  --write-out '{"response_code": %{response_code}, "time": {"ttfb": %{time_starttransfer}, "total": %{time_total}}, "headers": %{header_json}}' \
  > $output_headers

# Output the headers in the response
echo; display_title "Info:"
display_section -n "Response Code: "; display_info $(jq -r .response_code $output_headers)
display_section -n "Time: "; display_info $(printf '%.05f' $(jq -r .time.total $output_headers))'s'
display_section -n " TTFB: "; display_info $(printf '%.05f' $(jq -r .time.ttfb $output_headers))'s'
display_section -n " Rest: "; display_info $(printf '%.05f' $(jq -r '(.time.total - .time.ttfb)' $output_headers))'s'
display_title "Headers:"
for key in $(jq -r '.headers | keys_unsorted[]' $output_headers); do
  display_section -n "- $key: "; display_info "$(jq -r ".headers["'"'"$key"'"'"][0]" $output_headers)"
done

# And finally the response itself
echo; display_title "Response:"
if [ ! -e $output_response ] || [ ! -s $output_response ]; then
  display_red '(Empty response returned)'
elif [ $(jq -r '.headers["content-type"][0]' $output_headers | grep -Fc 'application/json') -eq 1 ]; then
  # Output as JSON
  jq . $output_response
else
  # Output as text
  cat $output_response
fi
rm -rf $outdir
