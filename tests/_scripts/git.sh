#!/bin/bash
# Determine git info about the repo being processed

mode="$1"

# Name of the Repo / Project
if [ "x$mode" = 'xrepo' ]
then
    if [ -z $CI_PROJECT_NAME ]
    then
        remote=$(git config --get remote.origin.url)
        if [ ! -z `echo "$remote" | grep -F '.git'` ]
        then
            echo "$remote" | sed -r 's/^.+\/([^\/]+)\.git$/\1/'
        else
            echo "$remote" | sed -r 's/^.+\/([^\/]+)$/\1/'
        fi
    else
        echo "$CI_PROJECT_NAME"
    fi

# The current branch we are on
elif [ "x$mode" = 'xbranch' ]
then
    if [ -z $CI_COMMIT_REF_NAME ]
    then
        git symbolic-ref HEAD | sed -r 's/^refs\/heads\///'
    else
        echo "$CI_COMMIT_REF_NAME"
    fi

# The latest commit SHA
elif [ "x$mode" = 'xcommit' ]
then
    if [ -z $CI_COMMIT_SHA ]
    then
        git rev-parse HEAD
    else
        echo "$CI_COMMIT_SHA"
    fi

else
    echo "Unknown mode '$mode'." >&2
    exit 1
fi
