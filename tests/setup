#!/bin/bash
# Setup the cache files for the unit tests
dir_root=$(dirname $0)
dir_scripts=$(realpath $dir_root/_scripts)
dir_vendor="$(realpath $dir_root/..)/vendor" # Do not realpath a potential vendor symlink
dir_setup="$dir_vendor/debear/setup"
mkdir -p $dir_setup

#
# GeoIP cache
#
echo -n "GeoIP: "
geoip_dir="$dir_setup/geoip"
geoip_flag="$geoip_dir/.cache"
geoip_file="$geoip_dir/GeoLite2-City.mmdb"

if [ ! -e $geoip_dir ]
then
    mkdir $geoip_dir
fi

# Pass 1: Check via the MD5 cache if we actually need to download the file?
if [ -e $geoip_flag ]
then
    geoip_md5=$(cat $geoip_flag)
fi
geoip_HEAD=$($dir_scripts/download.sh geoip - - --header "X-Cache:$geoip_md5" --head --output /dev/null --write-out %{response_code}:%header{x-cache} 2>&1)
geoip_status=$(echo "$geoip_HEAD" | awk -F':' '{ print $1 }')
geoip_md5_new=$(echo "$geoip_HEAD" | awk -F':' '{ print $2 }')

# Pass 2: If it returns a 200, we need to download
if [ "x$geoip_status" = 'x200' ]
then
    # Pass 2: Download
    rm -rf $geoip_file
    $dir_scripts/download.sh geoip - - --header "X-Cache:$geoip_md5" >$geoip_file.gz
    if [ ! -s $geoip_file.gz ]
    then
        echo "[ Failed ]"
        exit 1
    fi

    # Process
    gzip -d $geoip_file
    echo -n "$geoip_md5_new" >$geoip_flag
    echo "[ Done ]"
# If a 204 was returned, we don't need to download the file
elif [ "x$geoip_status" = 'x204' ]
then
    echo "[ Up-to-date ($geoip_status) ]"
# Some kind of error occurred that we should consider fatal
else
    echo "[ Failed ($geoip_status) ]"
    exit 1
fi

#
# Browscap
#
echo -n "Browscap: "
browscap_dir="$dir_setup/browscap"
browscap_flag="$browscap_dir/.cache"
browscap_base="$browscap_dir/resources"

if [ ! -e $browscap_dir ]
then
    mkdir $browscap_dir
fi

# Pass 1: Check via the MD5 cache if we actually need to download the file?
if [ -e $browscap_flag ]
then
    browscap_md5=$(cat $browscap_flag)
fi
browscap_HEAD=$($dir_scripts/download.sh browscap - - --header "X-Cache:$browscap_md5" --head --output /dev/null --write-out %{response_code}:%header{x-cache} 2>&1)
browscap_status=$(echo "$browscap_HEAD" | awk -F':' '{ print $1 }')
browscap_md5_new=$(echo "$browscap_HEAD" | awk -F':' '{ print $2 }')

# Pass 2: If it returns a 200, we need to download
if [ "x$browscap_status" = 'x200' ]
then
    # Pass 2: Download
    rm -rf $browscap_base $browscap_base.tar.gz
    $dir_scripts/download.sh browscap - - --header "X-Cache:$browscap_md5" >$browscap_base.tar.gz
    if [ ! -s $browscap_base.tar.gz ]
    then
        echo "[ Failed ]"
        exit 1
    fi

    # Process
    tar -xzf $browscap_base.tar.gz -C $browscap_dir
    rm -rf $browscap_base.tar.gz
    echo -n "$browscap_md5_new" >$browscap_flag
    echo "[ Done ]"
# If a 204 was returned, we don't need to download the file
elif [ "x$browscap_status" = 'x204' ]
then
    echo "[ Up-to-date ($browscap_status) ]"
# Some kind of error occurred that we should consider fatal
else
    echo "[ Failed ($browscap_status) ]"
    exit 1
fi
