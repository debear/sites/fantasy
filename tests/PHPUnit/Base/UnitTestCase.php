<?php

namespace Tests\PHPUnit\Fantasy;

use Tests\PHPUnit\Base\UnitTestCase as BaseUnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Testing\PendingCommand;
use DeBear\Helpers\Config;

class UnitTestCase extends BaseUnitTestCase
{
    /**
     * Original configuration before we modified it per-test.
     * @var array
     */
    protected array $config;

    /**
     * Update internal objects between tests
     * @return void
     */
    protected function setUp(): void
    {
        // Load the standard test setup methods.
        parent::setUp();
        // Tweak the config.
        $this->config = FrameworkConfig::get('debear');
        Config::loadSite();
    }

    /**
     * Restore changes made during the setUp phase
     * @return void
     */
    protected function tearDown(): void
    {
        // Restore our configuration changes.
        FrameworkConfig::set(['debear' => $this->config]);
        // Run our standard restoration.
        parent::tearDown();
    }

    // Due to an issue with compatibility with the base versions, do
    // not apply the Squiz.Commenting.FunctionComment rule to check function argument signatures.
    // phpcs:disable Squiz.Commenting.FunctionComment
    /**
     * Call artisan command and return code.
     * @param  string  $command
     * @param  array  $parameters
     * @return \Illuminate\Testing\PendingCommand|int
     */
    public function artisan($command, $parameters = [])
    {
        unset($_SERVER['REQUEST_ROUTE']); // Ensure our route is reset between calls.
        return parent::artisan($command, $parameters);
    }
    // phpcs:enable Squiz.Commenting.FunctionComment

    /**
     * Call artisan command and return code, without any prior customisation reset.
     * @param  string $command    The command reference to be loaded.
     * @param  array  $parameters The optional arguments to be supplied.
     * @return PendingCommand|integer The native return value from the artisan command
     */
    protected function artisanCustomised(string $command, array $parameters = []): PendingCommand|int
    {
        return parent::artisan($command, $parameters);
    }
}
