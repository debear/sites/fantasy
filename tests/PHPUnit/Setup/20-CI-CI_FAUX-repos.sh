#!/bin/bash
# Clone the secondary repos this uses into the parent directory
echo -e "\e[1mOn CI server, so cloning secondary repos to parent directory:\e[0m"

dir_tests=$(realpath $(dirname $0)/../..)
dir_root=$(realpath $dir_tests/..)
dir_scripts=$(realpath $dir_tests/_scripts)

$dir_scripts/git-clone.sh sports
$dir_scripts/git-clone.sh sysmon
