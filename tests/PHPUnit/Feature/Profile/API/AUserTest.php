<?php

namespace Tests\PHPUnit\Feature\Profile;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;

class AUserTest extends FeatureTestCase
{
    use TraitAPIToken;

    /**
     * The (base) endpoint we'll be targetting in these tests
     * @var string
     */
    protected $endpoint = '/api/profile/v1.0';

    /**
     * A profile continues to hit our unavailable view
     * @return void
     */
    public function testProfile(): void
    {
        $response = $this->get($this->endpoint . '/fantasy_user');
        $response->assertStatus(200);

        // User info.
        $response->assertJSON(['user_id' => 'fantasy_user']);
        $response->assertJSON(['display_name' => 'Fantasy User']);
        $response->assertJSON(['country' => ['name' => 'United Kingdom', 'flag' => 'gb']]);
        $response->assertJSON(['rating' => 57.5]);

        // Team info.
        $response->assertJSONCount(8, 'teams');
        // NFL Salary Cap.
        $i = 0;
        $response->assertJSONPath("teams.$i.season", 2020);
        $response->assertJSONPath("teams.$i.game", 'nfl');
        $response->assertJSONPath("teams.$i.in_progress", true);
        $response->assertJSONPath("teams.$i.game_type", 'budget');
        $response->assertJSONPath("teams.$i.name", 'Tester Football');
        $response->assertJSONPath("teams.$i.detail.combination", 'p1:s1');
        $response->assertJSONPath("teams.$i.detail.score", 512);
        $response->assertJSONPath("teams.$i.detail.rank", 3712);
        $response->assertJSONPath("teams.$i.detail.centile", 12);
        $response->assertJSONCount(3, "teams.$i.detail.groups");
        $response->assertJSONPath("teams.$i.detail.groups.0.name", 'To be the Test, you have to beat the Test');
        $response->assertJSONPath("teams.$i.detail.groups.0.num_teams", 2);
        $response->assertJSONPath("teams.$i.detail.groups.0.rank", 2);
        $response->assertJSONPath("teams.$i.detail.medals", [
            'group-top-3' => 2,
            'group-winner' => 1,
        ]);
        // Record Breakers.
        $i = 2;
        $response->assertJSONPath("teams.$i.season", 2020);
        $response->assertJSONPath("teams.$i.game", 'records');
        $response->assertJSONPath("teams.$i.in_progress", true);
        $response->assertJSONPath("teams.$i.game_type", 'record');
        $response->assertJSONPath("teams.$i.name", '<span class="icon records-sport-mlb">56 Game '
            . 'Hitting Streak</span>');
        $response->assertJSONPath("teams.$i.detail.sport", 'mlb');
        $response->assertJSONPath("teams.$i.detail.game_id", 1);
        $response->assertJSONPath("teams.$i.detail.achieved", false);
        $response->assertJSONPath("teams.$i.detail.score", 7);
        $response->assertJSONPath("teams.$i.detail.rank", 123);
        $response->assertJSONPath("teams.$i.detail.centile", 96);
        $response->assertJSONCount(4, "teams.$i.detail.achievements");
        $response->assertJSONPath("teams.$i.detail.achievements.0.achieve_id", 1);
        $response->assertJSONPath("teams.$i.detail.achievements.0.name", 'Hit Streak');
        $response->assertJSONCount(5, "teams.$i.detail.achievements.0.levels");
        $response->assertJsonMissingPath("teams.$i.detail.achievements.0.levels.0");
        $response->assertJSONPath("teams.$i.detail.achievements.0.levels.1", true);
        $response->assertJSONPath("teams.$i.detail.achievements.0.levels.5", false);
        $response->assertJSONPath("teams.$i.detail.achievements.3.achieve_id", 4);
        $response->assertJSONPath("teams.$i.detail.achievements.3.name", 'Doubleheaders');
        $response->assertJSONCount(1, "teams.$i.detail.achievements.3.levels");
        $response->assertJsonMissingPath("teams.$i.detail.achievements.3.levels.0");
        $response->assertJsonMissingPath("teams.$i.detail.achievements.3.levels.1");
        $response->assertJSONPath("teams.$i.detail.achievements.3.levels.4", true);
        $response->assertJsonMissingPath("teams.$i.detail.groups");
        $response->assertJsonMissingPath("teams.$i.detail.medals");

        // Medal info.
        $response->assertJSONCount(5, 'medals');
        $response->assertJsonFragment(['overall-winner' => [
            'title' => 'Overall Winner',
            'info' => 'Awarded to the top team in the overall standings',
            'icon' => 'medal_overall_1a',
            'num' => 1,
        ]]);
        $response->assertJsonFragment(['group-top-3' => [
            'title' => 'Top 3 Group Finisher',
            'info' => 'Awarded to teams that finish in the Top 3 of a group',
            'icon' => 'medal_group_3c',
            'num' => 6,
        ]]);
    }

    /**
     * A profile view for an automated user
     * @return void
     */
    public function testAutomatedUsers(): void
    {
        $response = $this->get($this->endpoint . '/testbot');
        $response->assertStatus(200);

        // User info.
        $response->assertJSON(['user_id' => 'testbot']);
        $response->assertJSON(['display_name' => 'Testbot 3000']);
        $response->assertJSON(['country' => ['name' => 'United Kingdom', 'flag' => 'gb']]);
        $response->assertJSON(['rating' => 64.4]);

        // Team info.
        $response->assertJSONCount(1, 'teams');
    }

    /**
     * Handling the various scenarios for invalid arguments
     * @return void
     */
    public function testInvalidArguments(): void
    {
        // Inactive user (from a fantasy perspective).
        $response = $this->get($this->endpoint . '/fantasy_inactive');
        $response->assertStatus(404);
        $response->assertSee('{"msg":"Resource Not Found"}');

        // An automated user who exists, but not yet played a game.
        $response = $this->get($this->endpoint . '/classics26864');
        $response->assertStatus(404);
        $response->assertSee('{"msg":"Resource Not Found"}');

        // Unknown user.
        $response = $this->get($this->endpoint . '/unknown_user');
        $response->assertStatus(404);
        $response->assertSee('{"msg":"Resource Not Found"}');
    }
}
