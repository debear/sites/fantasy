<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Implementations\TestResponse;

class AUserTest extends FeatureTestCase
{
    /**
     * A profile view for a specific user
     * @return void
     */
    public function testProfile(): void
    {
        $response = $this->get('/profile/fantasy_user');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Profile &raquo; Fantasy User</title>');
        $response->assertSee('<h1>Fantasy User (fantasy_user)</h1>');

        $response->assertSee('<dl class="summary">
    <dt class="ranking">Overall Player Score:</dt>
        <dd>57.5 <i class="fas fa-search-dollar"></i></dd>
        <dd><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>'
            . '<i class="far fa-star"></i><i class="far fa-star"></i> Veteran</dd>
    <dt class="since">Player Since:</dt>
        <dd>2019</dd>
    <dt class="based">Based:</dt>
        <dd><span class="flag_right flag16_right_gb">United Kingdom</span></dd>
</dl>');

        // Validate game specifics.
        $this->profileMotorsport($response);
        $this->profileNFLSalCap($response);
        $this->profileRecordBreakers($response);

        // Medals.
        $response->assertSee('<fieldset class="medals section">
    <h3 class="invert">Medals Earned</h3>
    <ul class="inline_list">
                    <li class="icon icon_medal_overall_1a">1x Overall Winner</li>
            <li class="info"><em>Awarded to the top team in the overall standings</em></li>
                    <li class="icon icon_medal_overall_5c">1x Top 5 Finisher</li>
            <li class="info"><em>Awarded to teams that finish in the Top 5 Overall</em></li>
                    <li class="icon icon_medal_overall_30c">1x Top 30 Finisher</li>
            <li class="info"><em>Awarded to teams that finish in the Top 30 Overall</em></li>
                    <li class="icon icon_medal_group_1c">5x Group Winner</li>
            <li class="info"><em>Awarded to the top team in each group</em></li>
                    <li class="icon icon_medal_group_3c">6x Top 3 Group Finisher</li>
            <li class="info"><em>Awarded to teams that finish in the Top 3 of a group</em></li>
            </ul>
</fieldset>');

        // Breakdown.
        $response->assertSee('<fieldset class="breakdown section">
    <h3 class="invert">Team Breakdown</h3>
    <div class="chart" id="team-breakdown"></div>
</fieldset>');
        $response->assertSee('"data":[{"name":"Record Breakers","y":6},{"name":"Fantasy NFL Salary Cap","y":1},'
            . '{"name":"Fantasy Formula 1","y":1}]');
    }

    /**
     * Sub-processing for a Motorsport game
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    public function profileMotorsport(TestResponse $response): void
    {
        // Formula One.
        $response->assertSee('<dt class="fantasy-icon-f1 type row-1">
            <name>Fantasy Formula 1</name>
            <rating>
                57.7 <i class="fas fa-search-dollar"></i>
                &ndash; <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>'
            . '<i class="far fa-star"></i><i class="far fa-star"></i> Veteran
            </rating>
        </dt>');
        $response->assertSee('<dd class="type-budget-f1">
                <ul class="inline_list">
                                                                    <li class="podium-1 team ">
                            <span class="season">2019</span>
                            <span class="name">Tester Racing</span>
                            <span class="rank">
                                                                    1st Overall
                                                            </span>
                            <span class="rating">57.7 <i class="fas fa-search-dollar"></i></span>');
        $response->assertSee('<li class="podium-1">
                                            Fans of Driver
                                                                                    </li>');
    }

    /**
     * Sub-processing for an NFL SalCap game
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    public function profileNFLSalCap(TestResponse $response): void
    {
        $response->assertSee('<dt class="fantasy-icon-nfl-cap type row-1">
            <name>Fantasy NFL Salary Cap</name>
            <rating>
                41.5 <i class="fas fa-search-dollar"></i>
                &ndash; <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i>'
            . '<i class="far fa-star"></i><i class="far fa-star"></i> Regular
            </rating>
        </dt>');
        $response->assertSee('<dd class="type-budget-nfl">
                <ul class="inline_list">
                                                                    <li class="row-0 team in-progress">
                            <span class="season">2020</span>
                            <span class="name">Tester Football</span>
                            <span class="rank">
                                                                    12th Centile
                                                            </span>
                            <span class="rating">41.5 <i class="fas fa-search-dollar"></i></span>');
        $response->assertSee('<li class="podium-2">
                                            To be the Test, you have to beat the Test
                                                                                            <div class="group-rank">
                                                    2nd of 2
                                                </div>
                                                                                    </li>');
        $response->assertSee('<li class="row-0">
                                            Fans from A Country
                                                                                            <div class="group-rank">
                                                    194th of 212
                                                </div>
                                                                                    </li>');
        // Still in progress, so we should also have a key.
        $response->assertSee('<dd class="key in-progress">Game still in progress</dd>');
    }

    /**
     * Sub-processing for Record Breaker games
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    public function profileRecordBreakers(TestResponse $response): void
    {
        $response->assertSee('<dt class="fantasy-icon-records type row-1">
            <name>Record Breakers</name>
            <rating>
                60.1 <i class="fas fa-search-dollar"></i>
                &ndash; <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>'
            . '<i class="far fa-star"></i><i class="far fa-star"></i> Veteran
            </rating>
        </dt>');
        $response->assertSee('<dd class="type-records">
                <ul class="inline_list">
                                                                    <li class="row-0 team in-progress">
                            <span class="season">2020</span>
                            <span class="name"><span class="icon records-sport-mlb">26 Game Winning Streak</span></span>
                            <span class="rank">
                                                                    93rd Centile
                                                            </span>
                            <span class="rating">100.0 <i class="fas fa-search-dollar"></i></span>
                                                    </li>');
        $response->assertSee('<li class="podium-3 team in-progress">
                            <span class="season">2019</span>
                            <span class="name"><span class="icon records-sport-nhl">50 in 50</span></span>
                            <span class="rank">
                                                                    3rd Overall
                                                            </span>
                            <span class="rating">37.5 <i class="fas fa-search-dollar"></i></span>
                                                    </li>
                                                            </ul>
            </dd>');
    }

    /**
     * A profile view for an automated user
     * @return void
     */
    public function testAutomatedUsers(): void
    {
        $response = $this->get('/profile/testbot');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Profile &raquo; Testbot 3000</title>');
        $response->assertSee('<h1>Testbot 3000 (testbot)</h1>');

        $response->assertSee('<dl class="summary">
    <dt class="ranking">Overall Player Score:</dt>
        <dd>64.4 <i class="fas fa-search-dollar"></i></dd>
        <dd><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>'
            . '<i class="far fa-star"></i><i class="far fa-star"></i> Veteran</dd>
    <dt class="since">Player Since:</dt>
        <dd>2019</dd>
    <dt class="based">Based:</dt>
        <dd><span class="flag_right flag16_right_gb">United Kingdom</span></dd>
</dl>');
    }

    /**
     * Handling the various scenarios for invalid arguments
     * @return void
     */
    public function testInvalidArguments(): void
    {
        // Inactive user (from a fantasy perspective).
        $response = $this->get('/profile/fantasy_inactive');
        $response->assertStatus(404);

        // An automated user who exists, but not yet played a game.
        $response = $this->get('/profile/classics26864');
        $response->assertStatus(404);

        // Unknown user.
        $response = $this->get('/profile/unknown_user');
        $response->assertStatus(404);
    }
}
