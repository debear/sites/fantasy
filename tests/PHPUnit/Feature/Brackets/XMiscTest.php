<?php

namespace Tests\PHPUnit\Feature\Brackets;

use Tests\PHPUnit\Base\FeatureTestCase;

class XMiscTest extends FeatureTestCase
{
    /**
     * Test loading of help sections and articles
     * @return void
     */
    public function testManifests(): void
    {
        $this->setTestDateTime('2024-03-15 12:34:56');
        $valid = $this->get('/nhl-bracket/2023/manifest.json');
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/manifest+json');
        $valid->assertJsonPath('name', 'DeBear Fantasy Sports NHL Bracket Challenge');
        $valid->assertJsonPath('start_url', '/nhl-bracket/2023/?utm_source=homescreen');

        // A season prior to the game launching.
        $historical = $this->get('/nhl-bracket/2022/manifest.json');
        $historical->assertStatus(404);
    }
}
