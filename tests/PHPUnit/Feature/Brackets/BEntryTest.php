<?php

namespace Tests\PHPUnit\Feature\Brackets;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Skeleton\CommsEmail;

class BEntryTest extends FeatureTestCase
{
    /**
     * The endpoint the game homepage
     * @var string
     */
    protected string $endpoint_home = '/nhl-bracket/2023';
    /**
     * The endpoint the registration form and game actions are accessed on
     * @var string
     */
    protected string $endpoint_form = '/nhl-bracket/2023/my-entry';
    /**
     * Login details of the user being used to complete the forms test
     * @var array
     */
    protected array $forms_login = [
        'username' => 'fantasy_clean',
        'password' => 'testlogindetails',
    ];

    /**
     * Create and edit an entry
     * @return void
     */
    public function testEntryManagement(): void
    {
        $this->setTestDateTime('2024-03-15 12:34:56');
        $this->post('/login', $this->forms_login)->assertStatus(200);

        // Not available on a historical or future season.
        $historical = $this->get(str_replace('2023', '2022', $this->endpoint_form));
        $historical->assertStatus(404);
        $future = $this->get(str_replace('2023', '2029', $this->endpoint_form));
        $future->assertStatus(404);

        // Create a new entry.
        $this->registerActions(); // The actions listed on the homepage.
        $this->registerRender(); // Render the page.
        $this->registerSubmit(); // Submit the form.

        // Now update it.
        $this->updateActions(); // The actions listed on the homepage.
        $this->updateRender(); // Render the page.
        $this->updateSubmit(); // Submit the form.
    }

    /**
     * Registration sub-action: homepage actions
     * @return void
     */
    protected function registerActions(): void
    {
        // Re-run the homepage query, as the flash message should not appear.
        $response = $this->get($this->endpoint_home);
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; NHL Bracket Challenge</title>');
        $response->assertSee('<li class="link header_my-entry">
                        <a href="/nhl-bracket/2023/my-entry" title="">My Entry</a>
        </li>');
        $response->assertSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/nhl-bracket/2023/my-entry">click here</a> to
    confirm your details and then start thinking about your bracket!
</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to NHL Bracket'
            . ' Challenge has been created &ndash; now start making your selections!</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to'
            . ' NHL Bracket Challenge has been updated.</fieldset>');
    }

    /**
     * Registration sub-action: rendering the initial form
     * @return void
     */
    protected function registerRender(): void
    {
        $response = $this->get($this->endpoint_form);
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; NHL Bracket Challenge &raquo; '
            . 'Create Entry</title>');
        $response->assertSee('<form method="post" action="/nhl-bracket/2023/my-entry"');
        $response->assertSee('<h1>Create Your Entry</h1>');
        $response->assertSee('<ul class="inline_list field name clearfix">
        <li class="label " id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="" required minlength="5" maxlength="35" '
            . 'tabindex="1" data-tabindex="1">
        </li>
        <li class="status " id="name_status"></li>
        <li class="info">Must be between 5 and 35 characters</li>
        <li class="error details hidden" id="name_error"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list field email_updates clearfix">
        <li class="label " id="email_updates_label">
            <label for="email_type">Email Updates:</label>&nbsp;
        </li>
        <li class="value email_status">
            <input type="hidden" id="email_status__cb" name="email_status__cb" value="0">
            <input type="checkbox" id="email_status" name="email_status"  >
            <label for="email_status" data-id="email_status" tabindex="2" data-tabindex="2">Receive Email '
            . 'Updates?</label>
        </li>
        <li class="error details hidden" id="email_status_error"></li>
        <li class="value email_type">
            <input type="hidden" id="email_type" name="email_type" value="" data-is-dropdown="true" >
<dl class="dropdown " data-id="email_type" data-disabled="true" tabindex="3" data-tabindex="3">
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span><span class="icon icon_email_html">HTML</span></span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="html"><span class="icon icon_email_html">HTML</span></li>
                            <li  data-id="text"><span class="icon icon_email_text">Text Only</span></li>
                    </ul>');
        $response->assertSee('<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, creating your entry...
    </div>
    <div class="box error icon_error hidden">
      <strong>Unable to create entry</strong>: There were errors when attempting to create your entry. The fields '
            . 'marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before '
            . 'we can create your entry. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" tabindex="4" data-tabindex="4">Create Entry</button>
</div>');
    }

    /**
     * Registration sub-action: submitting the registration form
     * @return void
     */
    protected function registerSubmit(): void
    {
        // Pre-test query to determine the number of emails sent.
        $email_query = $this->getEntryEmailCountQuery();
        $emails_before = $email_query->count();

        // Submit the form.
        $post = $this->post($this->endpoint_form, [
            'form_ref' => '48b5cc',
            'name' => 'Test User Entry',
            'email_status' => 'on',
            'email_status__cb' => '1',
            'email_type' => 'html',
        ]);
        $post->assertStatus(303);
        $post->assertRedirect("https://fantasy.debear.test{$this->endpoint_form}");
        // After the Post/Redirect/Get, we should then redirect to the homepage.
        $redirect = $this->get($this->endpoint_form);
        $redirect->assertStatus(303);
        $redirect->assertRedirect('https://fantasy.debear.test/nhl-bracket/2023');

        // Then check the displayed version.
        $get = $this->get('/nhl-bracket/2023');
        $get->assertStatus(200);
        $get->assertSee('<h1>Welcome to NHL Bracket Challenge!</h1>');
        $get->assertSee('<fieldset class="user-action success icon_valid">Your entry to NHL Bracket Challenge'
            . ' has been created &ndash; now start making your selections!</fieldset>');
        $get->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to'
            . ' NHL Bracket Challenge has been updated.</fieldset>');

        // Number of emails sent - should have increased by one.
        $emails_after = $email_query->count();
        $this->assertEquals($emails_after, $emails_before + 1);
    }

    /**
     * Registration sub-action: homepage actions
     * @return void
     */
    protected function updateActions(): void
    {
        // Re-run the homepage query, as the flash message should not appear.
        $response = $this->get($this->endpoint_home);
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; NHL Bracket Challenge</title>');
        $response->assertSee('<li class="link header_my-entry">
                        <a href="/nhl-bracket/2023/my-entry" title="">My Entry</a>
        </li>');
        $response->assertDontSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/nhl-bracket/2023/my-entry">click here</a> to
    confirm your details and then start thinking about your bracket!
</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to NHL Bracket'
            . ' Challenge has been created &ndash; now start making your selections!</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to'
            . ' NHL Bracket Challenge has been updated.</fieldset>');
    }

    /**
     * Registration sub-action: rendering the initial form
     * @return void
     */
    protected function updateRender(): void
    {
        $response = $this->get($this->endpoint_form);
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; NHL Bracket Challenge &raquo; '
            . 'Update Entry</title>');
        $response->assertSee('<form method="post" action="/nhl-bracket/2023/my-entry"');
        $response->assertSee('<h1>Update Your Entry</h1>');
        $response->assertSee('<ul class="inline_list field name clearfix">
        <li class="label " id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="Test User Entry" required minlength="5" '
            . 'maxlength="35" tabindex="1" data-tabindex="1">
        </li>
        <li class="status icon_valid" id="name_status"></li>
        <li class="info">Must be between 5 and 35 characters</li>
        <li class="error details hidden" id="name_error"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list field email_updates clearfix">
        <li class="label " id="email_updates_label">
            <label for="email_type">Email Updates:</label>&nbsp;
        </li>
        <li class="value email_status">
            <input type="hidden" id="email_status__cb" name="email_status__cb" value="1">
            <input type="checkbox" id="email_status" name="email_status"  checked="checked">
            <label for="email_status" data-id="email_status" tabindex="2" data-tabindex="2">Receive Email '
            . 'Updates?</label>
        </li>
        <li class="error details hidden" id="email_status_error"></li>
        <li class="value email_type">
            <input type="hidden" id="email_type" name="email_type" value="html" data-is-dropdown="true" >
<dl class="dropdown " data-id="email_type"  tabindex="3" data-tabindex="3">
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span><span class="icon icon_email_html">HTML</span></span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="html"><span class="icon icon_email_html">HTML</span></li>
                            <li  data-id="text"><span class="icon icon_email_text">Text Only</span></li>
                    </ul>');
        $response->assertSee('<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, updating your entry...
    </div>
    <div class="box error icon_error hidden">
      <strong>Unable to update entry</strong>: There were errors when attempting to update your entry. The fields '
            . 'marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before '
            . 'we can update your entry. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" tabindex="4" data-tabindex="4">Update Entry</button>
</div>');
    }

    /**
     * Registration sub-action: submitting the registration form
     * @return void
     */
    protected function updateSubmit(): void
    {
        // Pre-test query to determine the number of emails sent.
        $email_query = $this->getEntryEmailCountQuery();
        $emails_before = $email_query->count();

        // Submit the form.
        $post = $this->post($this->endpoint_form, [
            'form_ref' => '48b5cc',
            'name' => 'Perfect Bracket',
            'email_status' => 'on',
            'email_status__cb' => '1',
            'email_type' => 'html',
        ]);
        $post->assertStatus(303);
        $post->assertRedirect("https://fantasy.debear.test{$this->endpoint_form}");
        // After the Post/Redirect/Get, we should then redirect to the homepage.
        $redirect = $this->get($this->endpoint_form);
        $redirect->assertStatus(303);
        $redirect->assertRedirect("https://fantasy.debear.test{$this->endpoint_form}");

        // Then check the displayed version.
        $get = $this->get($this->endpoint_form);
        $get->assertStatus(200);
        $get->assertSee('<title>DeBear Fantasy Sports &raquo; NHL Bracket Challenge &raquo; '
            . 'Update Entry</title>');
        $get->assertSee('<ul class="inline_list field name clearfix">
        <li class="label " id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="Perfect Bracket" required minlength="5" '
            . 'maxlength="35" tabindex="1" data-tabindex="1">
        </li>');

        // Number of emails sent - should NOT have changed.
        $emails_after = $email_query->count();
        $this->assertEquals($emails_after, $emails_before);
    }

    /**
     * Generate the base ORM query (still to be executed) to get the entry emails in the database
     * @return QueryBuilder Query object to load the information around the entry emails in the database
     */
    protected function getEntryEmailCountQuery(): QueryBuilder
    {
        return CommsEmail::query()
            ->where([
                'app' => 'fantasy_brackets',
                'email_reason' => 'entry_create',
                'user_id' => '63',
            ]);
    }
}
