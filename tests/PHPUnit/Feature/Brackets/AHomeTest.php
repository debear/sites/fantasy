<?php

namespace Tests\PHPUnit\Feature\Brackets;

use Tests\PHPUnit\Base\FeatureTestCase;

class AHomeTest extends FeatureTestCase
{
    /**
     * Test season argument parsing and redirects
     * @return void
     */
    public function testSeasonRedirects(): void
    {
        foreach (['mlb' => 2024, 'nfl' => 2023, 'nhl' => 2023, 'ahl' => 2023] as $sport => $season) {
            // Main index page, which redirects to the season page.
            $redirect = $this->get("/$sport-bracket");
            $redirect->assertStatus(307);
            $redirect->assertRedirect("/$sport-bracket/$season");
            // The basic homepage.
            $home = $this->get("/$sport-bracket/$season");
            $home->assertStatus(200);
            // Invalid seasons.
            $invalid_regex = $this->get("/$sport-bracket/2001");
            $invalid_regex->assertStatus(404);
            $invalid_range = $this->get("/$sport-bracket/2029");
            $invalid_range->assertStatus(404);
        }
    }

    /**
     * Test of the NFL homepage.
     * @return void
     */
    public function testHomepageNFL(): void
    {
        $this->setTestDateTime('2023-12-01 12:34:56');
        $response = $this->get('/nfl-bracket/2023');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; NFL Bracket Challenge</title>');
        $response->assertSee('<meta name="Description" content="Can you predict how the NFL playoff bracket '
            . 'will finish, and who will be crowned the next Super Bowl champion?" />');
        $response->assertSee('<link rel="manifest" '
            . 'href="//fantasy.debear.test/nfl-bracket/2023/manifest.json" />');

        $response->assertSee('<h1>Welcome to NFL Bracket Challenge!</h1>');
        $response->assertSee('<p class="intro">The quest for the Super Bowl has moved on to the current season, '
            . 'but championships last forever!</p>');

        // Only applies for 2023 season.
        foreach ([2022, 2024] as $season) {
            $season = $this->get("/nfl-bracket/$season");
            $season->assertStatus(404);
        }
    }

    /**
     * Test of the MLB homepage.
     * @return void
     */
    public function testHomepageMLB(): void
    {
        $this->setTestDateTime('2024-09-01 12:34:56');
        $response = $this->get('/mlb-bracket/2024');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; MLB Bracket Challenge</title>');
        $response->assertSee('<meta name="Description" content="Can you predict how the MLB playoff bracket '
            . 'will finish, and who will be crowned the next World Series champion?" />');
        $response->assertSee('<link rel="manifest" '
            . 'href="//fantasy.debear.test/mlb-bracket/2024/manifest.json" />');

        $response->assertSee('<h1>Welcome to MLB Bracket Challenge!</h1>');
        $response->assertSee('<p class="intro">The quest for the World Series is due to start soon. There may '
            . 'still be some changes to the seeding, and therefore the matchups in the bracket, but it&#39;s time to '
            . 'start thinking about your choices!</p>');

        // Only applies for 2024 season.
        foreach ([2023, 2025] as $season) {
            $season = $this->get("/mlb-bracket/$season");
            $season->assertStatus(404);
        }
    }

    /**
     * Test of the NHL homepage.
     * @return void
     */
    public function testHomepageNHL(): void
    {
        $this->setTestDateTime('2024-03-15 12:34:56');
        $response = $this->get('/nhl-bracket/2023');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; NHL Bracket Challenge</title>');
        $response->assertSee('<meta name="Description" content="Can you predict how the NHL playoff bracket '
            . 'will finish, and who will be crowned the next Stanley Cup champion?" />');
        $response->assertSee('<link rel="manifest" '
            . 'href="//fantasy.debear.test/nhl-bracket/2023/manifest.json" />');

        $response->assertSee('<h1>Welcome to NHL Bracket Challenge!</h1>');
        $response->assertSee('<p class="intro">The quest for the Stanley Cup is due to start soon. There may '
            . 'still be some changes to the seeding, and therefore the matchups in the bracket, but it&#39;s time to '
            . 'start thinking about your choices!</p>');

        // Only applies for 2023 season.
        foreach ([2022, 2024] as $season) {
            $season = $this->get("/nhl-bracket/$season");
            $season->assertStatus(404);
        }
    }

    /**
     * Test of the AHL homepage.
     * @return void
     */
    public function testHomepageAHL(): void
    {
        $this->setTestDateTime('2024-03-22 12:34:56');
        $response = $this->get('/ahl-bracket/2023');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; AHL Bracket Challenge</title>');
        $response->assertSee('<meta name="Description" content="Can you predict how the AHL playoff bracket '
            . 'will finish, and who will be crowned the next Calder Cup champion?" />');
        $response->assertSee('<link rel="manifest" '
            . 'href="//fantasy.debear.test/ahl-bracket/2023/manifest.json" />');

        $response->assertSee('<h1>Welcome to AHL Bracket Challenge!</h1>');
        $response->assertSee('<p class="intro">The quest for the Calder Cup is due to start soon. There may '
            . 'still be some changes to the seeding, and therefore the matchups in the bracket, but it&#39;s time to '
            . 'start thinking about your choices!</p>');

        // Only applies for 2023 season.
        foreach ([2022, 2024] as $season) {
            $season = $this->get("/ahl-bracket/$season");
            $season->assertStatus(404);
        }
    }
}
