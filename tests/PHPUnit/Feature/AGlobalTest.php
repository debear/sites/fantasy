<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\Time;

class AGlobalTest extends FeatureTestCase
{
    /**
     * A basic test the homepage is up.
     * @return void
     */
    public function testHomepage(): void
    {
        // Set the dates to our test date and make the request.
        $orig_time = Time::object()->getServerNowFmt();
        Time::object()->resetCache('2019-12-31 12:34:56');
        $orig_config = FrameworkConfig::get('debear.subsites');
        $this->setTestGameConfig();

        $response = $this->get('/');
        $response->assertStatus(200);

        // Page meta info.
        $response->assertSee('<title>DeBear Fantasy Sports</title>');
        $response->assertSee('<meta name="Description" content="The home portal to the suite of fantasy games on DeBear'
            . ' Fantasy Sports." />');
        $response->assertSee('<meta name="twitter:site" content="@DeBearSports" />');
        $response->assertSee('<meta property="og:title" content="DeBear Fantasy Sports" />');
        $response->assertSee('<meta property="og:image" content="https://static.debear.test/fantasy/images/meta/'
        . 'fantasy.png" />');
        $response->assertSee('<h1>Welcome to DeBear Fantasy Sports!</h1>');

        // The list of games.
        $response->assertSeeInOrder([
            // Active: NHL.
            '<h3 class="invert sport sports_nhl_icon">National Hockey League</h3>',
            '<a class="image-link" href="/records/nhl/50-in-50-2019"></a>',
            // Active: AHL.
            '<h3 class="invert sport sports_ahl_icon">American Hockey League</h3>',
            '<a class="image-link" href="/records/ahl/50-in-50-2019"></a>',
            // New Section: Pre-Season.
            '<p class="section-intro">We&#39;re gearing up for an exciting season of action, '
                . 'and these games will open for entries soon.</p>',
            // Pre-Season: MLB (no link).
            '<h3 class="invert sport sports_mlb_icon">Major League Baseball</h3>',
            '<div class="image-link"></div>
<h3 class="record">Record: 56 Game Hitting Streak</h3>',
            // Pre-Season: SGP (no link).
            '<h3 class="invert sport sports_sgp_icon">Speedway Grand Prix</h3>',
            '<div class="image-link"></div>
<h3 class="record">Record: 192 Point Season</h3>',
            // Pre-Season: F1 (no link).
            '<h3 class="invert sport sports_f1_icon">Formula One</h3>',
            '<div class="image-link"></div>
<h3 class="record">Record: 413 Point Season</h3>',
            // New Section: Off-Season.
            '<p class="section-intro">These games have wound down after a thrilling season of action. The recap from '
            . 'those games are still available below, but we&#39;ll be back after the off-season!</p>',
            // Off-Season: NFL.
            '<h3 class="invert sport sports_nfl_icon">National Football League</h3>',
            '<a class="image-link" href="/records/nfl/2-105-yard-rushing-season-2019"></a>',
        ]);

        // A Record Breakers game.
        $this->assertMatchesRegularExpression('/<li class="records grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                        <fieldset class="sport ice_box">
                            <a class="image-link" href="\/records\/nhl\/51-game-point-streak-2019"><\/a>
<h3 class="record">Record: 51 Game Point Streak<\/h3>
<p class="descrip">[^<]+<\/p>
<div class="detail">
    ' . '
            <img alt="Mugshot of the current Record Holder" src="\/\/cdn.debear.test\/[^"]+">
        ' . '
            <div class="progress">
            <h3 class="progress invert">Player Progress<\/h3>
    <ul class="progress nhl-2019-3 inline_list clearfix">
                                <li class="bar target">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">Target<\/span>
                <span class="value">52<\/span>
            <\/li>
                                <li class="bar active">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">NHL Leader<\/span>
                <span class="value">0<\/span>
            <\/li>
            <\/ul>

    <style nonce="[^"]+">
        ul.progress.nhl-2019-3 li.bar.target div.progress { width: 100%; }
ul.progress.nhl-2019-3 li.bar.active div.progress { width: 0%; }
    <\/style>
        <\/div>
    <\/div>

    <div class="text-link cta"><a href="\/records\/nhl\/51-game-point-streak-2019">View Game Details<\/a><\/div>
                        <\/fieldset>
                    <\/li>/ms', $response->getContent());

        // Bracket Challenge is not yet ready.
        $response->assertDontSee('<li class="brackets-nhl ');

        // Restore the modified time.
        Time::object()->resetCache($orig_time);
        FrameworkConfig::set(['debear.subsites' => $orig_config]);
    }

    /**
     * A test of the main fantasy sitemap.
     * @return void
     */
    public function testSitemap(): void
    {
        // Set the dates to our test date and make the request.
        $orig_time = Time::object()->getServerNowFmt();
        Time::object()->resetCache('2020-07-01 12:34:56');
        $orig_config = FrameworkConfig::get('debear.subsites');
        $this->setTestGameConfig();

        $response = $this->get('/sitemap');
        $response->assertStatus(200);
        $response->assertSee('<h1>Find your way round DeBear Fantasy Sports!</h1>');

        // The list of games.
        $response->assertSeeInOrder([
            '<fieldset class="default_box game-records">',
            '<dt>
                            Active Games
                                                    </dt>',
            '<a class="no_underline" href="/records/f1/413-point-season-2020">2020: 413 Point Season</a>',
            '<a class="no_underline" href="/records/sgp/192-point-season-2020">2020: 192 Point Season</a>',
            '<a class="no_underline" href="/records/nhl/50-in-50-2019">2019: 50 in 50</a>',
            '<dl class="subgames closed">
                        <dt>
                            Archived Games
                                                            <div>[ <a class="toggle">Open</a> ]</div>
                                                    </dt>',
            '<a class="no_underline" href="/records/nfl/2-105-yard-rushing-season-2019">2019: 2,105 Yard '
                . 'Rushing Season</a>',
            '<a class="no_underline" href="/records/ahl/50-in-50-2019">2019: 50 in 50</a>',
        ]);

        // A Record Breakers game.
        $response->assertSee('<fieldset class="default_box game-records">
                <a class="name" href="/records"></a>
                                    <dl class="subgames">
                        <dt>
                            Active Games
                                                    </dt>');

        // Bracket Challenge is not yet ready.
        $response->assertDontSee('<fieldset class="default_box game-brackets');

        // Restore the modified time.
        Time::object()->resetCache($orig_time);
        FrameworkConfig::set(['debear.subsites' => $orig_config]);
    }

    /**
     * A test of the main fantasy sitemap.
     * @return void
     */
    public function testSitemapXml(): void
    {
        // Main site sitemap.
        $main = $this->get('/sitemap.xml');
        $main->assertStatus(200);
        $main->assertSee('<url>
        <loc>https://fantasy.debear.test/</loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>');
        $main->assertSee('<url>
        <loc>https://fantasy.debear.test/sitemap</loc>
        <changefreq>monthly</changefreq>
        <priority>0.2</priority>
    </url>');
        // An active game.
        $main->assertSee('<url>
        <loc>https://fantasy.debear.test/records</loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>');
        // But not an inactive game.
        $main->assertDontSee('<url>
        <loc>https://fantasy.debear.test/nfl-cap</loc>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </url>');

        // Game list sitemap.
        $games = $this->get('/sitemap.games.xml');
        $games->assertStatus(200);
        $games->assertSee('<sitemap>
            <loc>https://fantasy.debear.test/records/sitemap.xml</loc>
        </sitemap>');
        $games->assertDontSee('<sitemap>
            <loc>https://fantasy.debear.test/nfl-cap/sitemap.xml</loc>
        </sitemap>');
    }

    /**
     * An unknown URI that goes back to hitting our 404
     * @return void
     */
    public function testUnknown(): void
    {
        $response = $this->get('/never-existed');
        $response->assertStatus(404);
    }

    /**
     * Set the various dates for our global view of the available games
     * @return void
     */
    protected function setTestGameConfig(): void
    {
        $k = 'debear.fantasy';
        FrameworkConfig::set([
            "$k.subsites.records.dates.visible" => '2019-09-01 00:00:00',
            "$k.subsites.records.dates.reg-open" => '2019-09-01 00:00:00',
            "$k.subsites.records.dates.hide" => '2099-12-31 23:59:59',
            "$k.subsites.records.games.upcoming_days" => 360, // Widen the upcoming period to list all games.
            "$k.subsites.brackets-nfl.enabled" => true,
            "$k.links.subsite-brackets-nfl.sitemap.enabled" => true,
            "$k.subsites.brackets-nfl.dates.visible" => '2019-12-15 00:00:00',
            "$k.subsites.brackets-nfl.dates.reg-open" => '2019-12-29 00:00:00',
            "$k.subsites.brackets-nfl.dates.hide" => '2020-02-28 23:59:59',
            "$k.subsites.brackets-mlb.dates.visible" => '2019-09-01 00:00:00',
            "$k.subsites.brackets-mlb.dates.reg-open" => '2019-09-26 00:00:00',
            "$k.subsites.brackets-mlb.dates.hide" => '2019-12-31 23:59:59',
            "$k.subsites.brackets-nhl.dates.visible" => '2020-04-01 00:00:00',
            "$k.subsites.brackets-nhl.dates.reg-open" => '2020-04-22 00:00:00',
            "$k.subsites.brackets-nhl.dates.hide" => '2020-07-31 23:59:59',
            "$k.subsites.brackets-ahl.enabled" => true,
            "$k.links.subsite-brackets-ahl.sitemap.enabled" => true,
            "$k.subsites.brackets-ahl.dates.visible" => '2020-04-01 00:00:00',
            "$k.subsites.brackets-ahl.dates.reg-open" => '2020-04-22 00:00:00',
            "$k.subsites.brackets-ahl.dates.hide" => '2020-07-31 23:59:59',
        ]);
    }
}
