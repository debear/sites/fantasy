<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;
use DeBear\ORM\Fantasy\Records\EntryScore;
use Illuminate\Support\Facades\DB;

class DEntrySelTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * The (base) endpoint we'll be targetting in game-based tests
     * @var string
     */
    protected $endpoint_games = '/api/records/v1.0/games';
    /**
     * The (base) endpoint we'll be targetting in entry-based tests
     * @var string
     */
    protected $endpoint_entry = '/api/records/v1.0/entries';
    /**
     * The name of the API Token for our records-admin based calls
     * @var string
     */
    protected $token_admin = 'fantasy-admin';

    /**
     * Test loading of individual selections
     * @return void
     */
    public function testSelReadInd(): void
    {
        $this->setTestDateTime('2020-01-17 12:34:56');
        $valid = $this->getUnthrottled("{$this->endpoint_games}/nhl-2019-1/periods/105");
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonCount(10, 'sel');
        $valid->assertJsonPath('sel.0.link_id', 2361);
        $valid->assertJsonPath('sel.0.lock_time.status', null);
        $valid->assertJsonPath('sel.0.lock_time.raw', '2020-01-18T00:25:00+00:00');
        $valid->assertJsonPath('sel.0.rating', 85);
        $valid->assertJsonPath('sel.0.opp_rating', 61);
        $valid->assertJsonPath('sel.0.score', null); // Unset for future dated selection.
        $valid->assertJsonPath('sel.0.stress', null); // Unset for future dated selection.
        $valid->assertJsonMissingPath('sel.0.flags.buster');
        $valid->assertJsonMissingPath('sel.0.flags.achieve_opp');
        $valid->assertJsonPath('sel.0.stats.1', '19');
        $valid->assertJsonMissingPath('sel.0.event_time');
        $valid->assertJsonPath('sel.9.link_id', 2578);
        $valid->assertJsonPath('sel.9.lock_time.status', null);
        $valid->assertJsonPath('sel.9.lock_time.raw', '2020-01-18T00:55:00+00:00');
        $valid->assertJsonPath('sel.9.rating', 70);
        $valid->assertJsonPath('sel.9.opp_rating', 58);
        $valid->assertJsonMissingPath('sel.9.flags.buster');
        $valid->assertJsonMissingPath('sel.9.flags.achieve_opp');
        $valid->assertJsonPath('sel.9.stats.2', '0.41');
        $valid->assertJsonMissingPath('sel.9.event_time');
        $valid->assertJsonPath('rows.from', 1);
        $valid->assertJsonPath('rows.to', 10);
        $valid->assertJsonPath('rows.total', 132);
        $valid->assertJsonPath('pages.curr', 1);
        $valid->assertJsonPath('pages.total', 14);
        $valid->assertJsonPath('pages.prev', false);
        $valid->assertJsonPath('pages.next', true);
        // A game with streak busters.
        $this->setTestDateTime('2020-01-16 12:34:56');
        $streak_buster = $this->getUnthrottled("{$this->endpoint_games}/nhl-2019-3/periods/104");
        $streak_buster->assertStatus(200);
        $streak_buster->assertHeader('Content-Type', 'application/json');
        $streak_buster->assertJsonCount(1, 'sel');
        $streak_buster->assertJsonPath('sel.0.link_id', 2220);
        $streak_buster->assertJsonPath('sel.0.lock_time.status', null);
        $streak_buster->assertJsonPath('sel.0.lock_time.raw', '2020-01-17T00:55:00+00:00');
        $streak_buster->assertJsonPath('sel.0.rating', 84);
        $streak_buster->assertJsonPath('sel.0.opp_rating', 62);
        $streak_buster->assertJsonPath('sel.0.score', null); // Unset for future dated selection.
        $streak_buster->assertJsonPath('sel.0.stress', null); // Unset for future dated selection.
        $streak_buster->assertJsonPath('sel.0.flags.buster', false); // Not a streak buster.
        $streak_buster->assertJsonMissingPath('sel.0.flags.achieve_opp');
        $streak_buster->assertJsonPath('sel.0.stats', []);
        $streak_buster->assertJsonMissingPath('sel.0.event_time');
        // A game with opponents achievement.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $opponents = $this->getUnthrottled("{$this->endpoint_games}/mlb-2020-3/periods/3");
        $opponents->assertStatus(200);
        $opponents->assertHeader('Content-Type', 'application/json');
        $opponents->assertJsonCount(1, 'sel');
        $opponents->assertJsonPath('sel.0.link_id', 65531);
        $opponents->assertJsonPath('sel.0.lock_time.status', null);
        $opponents->assertJsonPath('sel.0.lock_time.raw', '2020-08-11T01:10:00+01:00');
        $opponents->assertJsonPath('sel.0.rating', 93);
        $opponents->assertJsonPath('sel.0.opp_rating', 53);
        $opponents->assertJsonPath('sel.0.score', null); // Unset for future dated selection.
        $opponents->assertJsonPath('sel.0.stress', null); // Unset for future dated selection.
        $opponents->assertJsonPath('sel.0.flags.achieve_opp', false); // Not an achieved opponent.
        $opponents->assertJsonPath('sel.0.stats', []);
        $opponents->assertJsonMissingPath('sel.0.event_time');
    }

    /**
     * Test loading of individual selections
     * @return void
     */
    public function testSelReadIndEdgeCases(): void
    {
        $this->setTestDateTime('2020-01-17 12:34:56');

        // Unknown game.
        $unknown_game = $this->getUnthrottled("{$this->endpoint_games}/nhl-2018-1/periods/105");
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'The game could not be found');
        // Unknown period.
        $unknown_period = $this->getUnthrottled("{$this->endpoint_games}/nhl-2019-1/periods/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Invalid page argument.
        $invalid_page = $this->getUnthrottled("{$this->endpoint_games}/nhl-2019-1/periods/105?page=0");
        $invalid_page->assertStatus(400);
        $invalid_page->assertHeader('Content-Type', 'application/json');
        $invalid_page->assertJsonPath('msg', 'Bad Request');
        // Valid and unprocessed period, with bad pagination.
        $invalid_page_alt = $this->getUnthrottled("{$this->endpoint_games}/nhl-2019-1/periods/105?page=99");
        $invalid_page_alt->assertStatus(400);
        $invalid_page_alt->assertHeader('Content-Type', 'application/json');
        $invalid_page_alt->assertJsonPath('msg', 'Bad Request');
        // Valid, but unprocessed, period.
        $unprocessed_period = $this->getUnthrottled("{$this->endpoint_games}/nhl-2019-1/periods/106");
        $unprocessed_period->assertStatus(422);
        $unprocessed_period->assertHeader('Content-Type', 'application/json');
        $unprocessed_period->assertJsonPath('msg', 'Requested period not yet available');
        // Valid, but unavailable, period.
        $unavailable_period = $this->getUnthrottled("{$this->endpoint_games}/nfl-2019-3/periods/12");
        $unavailable_period->assertStatus(422);
        $unavailable_period->assertHeader('Content-Type', 'application/json');
        $unavailable_period->assertJsonPath('msg', 'Requested period not yet available');
    }

    /**
     * Test loading of individual selections with filters
     * @return void
     */
    public function testSelReadIndFilters(): void
    {
        $this->setTestDateTime('2020-01-18 00:34:12');
        $endpoint = "{$this->endpoint_games}/nhl-2019-1/periods/105";

        // Locked - Invalid.
        $locked_invalid = $this->getUnthrottled("$endpoint?locked=invalid");
        $locked_invalid->assertStatus(400);

        // Locked - Valid.
        $locked_false = $this->getUnthrottled("$endpoint?locked=false");
        $locked_false->assertStatus(200);
        $locked_false->assertJsonPath('rows.total', 44);

        $locked_true = $this->getUnthrottled("$endpoint?locked=true");
        $locked_true->assertStatus(200);
        $locked_true->assertJsonPath('rows.total', 132);

        // Team - Invalid.
        $team_invalid = $this->getUnthrottled("$endpoint?team_id=invalid");
        $team_invalid->assertStatus(400);

        // Team - Valid.
        $team = $this->getUnthrottled("$endpoint?team_id=TB");
        $team->assertStatus(200);
        $team->assertJsonPath('rows.total', 22);

        // Sort - Invalid.
        $sort_invalid = $this->getUnthrottled("$endpoint?sort=invalid");
        $sort_invalid->assertStatus(400);

        // Sort - Valid.
        $sort_rating = $this->getUnthrottled("$endpoint?sort=rating");
        $sort_rating->assertStatus(200);
        $sort_rating->assertJsonPath('sel.0.link_id', 2220);

        $sort_opprating = $this->getUnthrottled("$endpoint?sort=opp_rating");
        $sort_opprating->assertStatus(200);
        $sort_opprating->assertJsonPath('sel.0.link_id', 2220);

        $sort_stat1 = $this->getUnthrottled("$endpoint?sort=1");
        $sort_stat1->assertStatus(200);
        $sort_stat1->assertJsonPath('sel.0.link_id', 2626);

        // Viewed as a historical period.
        $endpoint = "{$this->endpoint_games}/nhl-2019-1/periods/104";
        $historical_empty = $this->getUnthrottled($endpoint);
        $historical_empty->assertStatus(200);
        $historical_empty->assertHeader('Content-Type', 'application/json');
        $historical_empty->assertJsonCount(0, 'sel');
        $historical_empty->assertJsonPath('rows.from', 0);
        $historical_empty->assertJsonPath('rows.to', 0);
        $historical_empty->assertJsonPath('rows.total', 0);
        $historical_empty->assertJsonPath('pages.curr', 0);
        $historical_empty->assertJsonPath('pages.total', 0);
        $historical_empty->assertJsonPath('pages.prev', false);
        $historical_empty->assertJsonPath('pages.next', false);

        $historical_unlocked = $this->getUnthrottled("$endpoint?locked=true");
        $historical_unlocked->assertStatus(200);
        $historical_unlocked->assertHeader('Content-Type', 'application/json');
        $historical_unlocked->assertJsonCount(1, 'sel');
        $historical_unlocked->assertJsonPath('rows.total', 1);

        // Achievement: Beaten Opponents - Invalid.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $endpoint = "{$this->endpoint_games}/mlb-2020-3/periods/3";
        $achieve_opp_invalid = $this->getUnthrottled("$endpoint?beaten_opp=invalid");
        $achieve_opp_invalid->assertStatus(400);

        // Achievement: Beaten Opponents - Valid.
        $achieve_opp_valid = $this->getUnthrottled("$endpoint?beaten_opp=false");
        $achieve_opp_valid->assertStatus(200);
        $achieve_opp_valid->assertJsonCount(1, 'sel');
        $achieve_opp_valid->assertJsonPath('sel.0.link_id', 65531);
        $achieve_opp_valid->assertJsonPath('sel.0.flags.achieve_opp', false);
        $achieve_opp_raw = $this->getUnthrottled("$endpoint?beaten_opp=true");
        $achieve_opp_raw->assertStatus(200);
        $achieve_opp_raw->assertJsonCount(1, 'sel');
        $achieve_opp_raw->assertJsonPath('sel.0.link_id', 65531);
        $achieve_opp_raw->assertJsonPath('sel.0.flags.achieve_opp', false);
    }

    /**
     * Test loading of team selections
     * @return void
     */
    public function testSelReadTeam(): void
    {
        $this->setTestDateTime('2019-09-07 12:34:56'); // Week 1, after the Thursday night game.
        $endpoint = "{$this->endpoint_games}/nfl-2019-3/periods/1";

        // The core request.
        $core = $this->getUnthrottled($endpoint);
        $core->assertStatus(200);
        $core->assertHeader('Content-Type', 'application/json');
        $core->assertJsonCount(10, 'sel');
        $core->assertJsonPath('rows.total', 32);
        $core->assertJsonPath('sel.0.link_id', 65506); // Baltimore is the top ranked selection.
        $core->assertJsonPath('sel.1.link_id', 65509); // Chicago played the Thursday night game.
        $core->assertJsonPath('sel.1.lock_time.status', null);
        $core->assertJsonPath('sel.1.lock_time.raw', '2019-09-06T01:15:00+01:00');
        $core->assertJsonPath('sel.1.rating', 77);
        $core->assertJsonPath('sel.1.opp_rating', 58);
        $core->assertJsonMissingPath('sel.1.buster'); // No streak busters.
        $core->assertJsonMissingPath('sel.1.event_time');

        // Try and exclude locked selections (which should fail to apply).
        $locked = $this->getUnthrottled("$endpoint?locked=false");
        $locked->assertStatus(200);
        $locked->assertHeader('Content-Type', 'application/json');
        $locked->assertJsonCount(10, 'sel');
        $locked->assertJsonPath('rows.total', 32);
        $locked->assertJsonPath('sel.1.link_id', 65509); // Chicago played the Thursday night game but still listed.
        $locked->assertJsonPath('sel.1.lock_time.status', null);
        $locked->assertJsonPath('sel.1.lock_time.raw', '2019-09-06T01:15:00+01:00');
        $locked->assertJsonPath('sel.1.rating', 77);
        $locked->assertJsonPath('sel.1.opp_rating', 58);
        $locked->assertJsonMissingPath('sel.1.buster'); // No streak busters.
        $locked->assertJsonMissingPath('sel.1.event_time');

        // Try and filter by specific team_id (which should fail to apply).
        $by_team = $this->getUnthrottled("$endpoint?team_id=BAL");
        $by_team->assertStatus(200);
        $by_team->assertHeader('Content-Type', 'application/json');
        $by_team->assertJsonCount(10, 'sel');
        $by_team->assertJsonPath('rows.total', 32);
        $by_team->assertJsonPath('sel.1.link_id', 65509); // Chicago still listed.

        // Sort.
        $sort = $this->getUnthrottled("$endpoint?sort=opp_rating");
        $sort->assertStatus(200);
        $sort->assertHeader('Content-Type', 'application/json');
        $sort->assertJsonCount(10, 'sel');
        $sort->assertJsonPath('rows.total', 32);
        $sort->assertJsonPath('sel.0.link_id', 65514); // Detroit is now the top ranked team.
    }

    /**
     * Test storing a selection via a records-write token
     * @return void
     */
    public function testSelCreateViaWrite(): void
    {
        $this->selCreateWorker(false);
    }

    /**
     * Test storing a selection via a records-admin token
     * @return void
     */
    public function testSelCreateViaAdmin(): void
    {
        $this->setAPIToken($this->token_admin, $this->defaultAPITokenPass);
        $this->selCreateWorker(true);
        $this->revertAPIToken();
    }

    /**
     * Worker method for storing a selection
     * @param boolean $as_admin That the processing occurrs via a records-admin token.
     * @return void
     */
    public function selCreateWorker(bool $as_admin): void
    {
        $this->setTestDateTime('2020-01-18 00:45:12');

        // The core endpoint of our requests.
        $endpoint = $this->endpoint_entry . '/fantasy_user';
        $headers = ['Content-Type' => 'application/json'];

        // Unknown game.
        $unknown_game = $this->postUnthrottled("$endpoint/nhl-2018-1/105");
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'The game could not be found');
        // Unknown period.
        $unknown_period = $this->postUnthrottled("$endpoint/nhl-2019-1/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->postUnthrottled("$endpoint/nhl-2019-2/105");
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // No selection passed.
        $no_selection = $this->postUnthrottled("$endpoint/nhl-2019-1/105");
        $no_selection->assertStatus(400);
        $no_selection->assertHeader('Content-Type', 'application/json');
        $no_selection->assertJsonPath('msg', 'Bad Request');
        // Invalid selection passed.
        $bad_selection = $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 'steven-stamkos'], $headers);
        $bad_selection->assertStatus(400);
        $bad_selection->assertHeader('Content-Type', 'application/json');
        $bad_selection->assertJsonPath('msg', 'Bad Request');
        $invalid_selection = $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 560], $headers);
        $invalid_selection->assertStatus(400);
        $invalid_selection->assertHeader('Content-Type', 'application/json');
        $invalid_selection->assertJsonPath('msg', 'Bad Request');
        // Locked selection passed.
        $locked_selection = $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 459], $headers);
        $locked_selection->assertStatus(423);
        $locked_selection->assertHeader('Content-Type', 'application/json');
        $locked_selection->assertJsonPath('msg', 'Requested selection locked');
        // Selection already exists.
        $duplicate = $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 2220], $headers);
        $duplicate->assertStatus(409);
        $duplicate->assertHeader('Content-Type', 'application/json');
        $duplicate->assertJsonPath('msg', 'Existing selection already saved');
        // Valid request (having first removed what was there).
        $this->delete("$endpoint/nhl-2019-1/105", [], ['Accept' => 'application/json'])->assertStatus(200);
        $valid = $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 2220], $headers);
        $valid->assertStatus(201);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('msg', 'Selection saved');

        // Non-Admin cannot process requests for another user.
        if (!$as_admin) {
            $another_user = $this->postUnthrottled("{$this->endpoint_entry}/fantasy_inactive/nhl-2019-1/105");
            $another_user->assertStatus(403);
            $another_user->assertJsonFragment(['msg' => 'Insufficient Privileges']);
        }

        // Admin cannot process requests for a user who doesn't exist.
        if ($as_admin) {
            $unknown_user = $this->postUnthrottled("{$this->endpoint_entry}/unknown_user/nhl-2019-1/105");
            $unknown_user->assertStatus(404);
            $unknown_user->assertJsonFragment(['msg' => 'User could not be found']);
        }
    }

    /**
     * Test updating a selection via a records-write token
     * @return void
     */
    public function testSelUpdateViaWrite(): void
    {
        $this->selUpdateWorker(false);
    }

    /**
     * Test updating a selection via a records-admin token
     * @return void
     */
    public function testSelUpdateViaAdmin(): void
    {
        $this->setAPIToken($this->token_admin, $this->defaultAPITokenPass);
        $this->selUpdateWorker(true);
        $this->revertAPIToken();
    }

    /**
     * Worker method for updating a selection
     * @param boolean $as_admin That the processing occurrs via a records-admin token.
     * @return void
     */
    public function selUpdateWorker(bool $as_admin): void
    {
        $this->setTestDateTime('2020-01-18 00:45:12');

        // The core endpoint of our requests.
        $endpoint = $this->endpoint_entry . '/fantasy_user';
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];

        // Unknown game.
        $unknown_game = $this->patchUnthrottled("$endpoint/nhl-2018-1/105");
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'The game could not be found');
        // Unknown period.
        $unknown_period = $this->patchUnthrottled("$endpoint/nhl-2019-1/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->patchUnthrottled("$endpoint/nhl-2019-2/105");
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // No selection passed.
        $no_selection = $this->patchUnthrottled("$endpoint/nhl-2019-1/105");
        $no_selection->assertStatus(400);
        $no_selection->assertHeader('Content-Type', 'application/json');
        $no_selection->assertJsonPath('msg', 'Bad Request');
        // Invalid selection passed.
        $bad_selection = $this->patchUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 'steven-stamkos']);
        $bad_selection->assertStatus(400);
        $bad_selection->assertHeader('Content-Type', 'application/json');
        $bad_selection->assertJsonPath('msg', 'Bad Request');
        $invalid_selection = $this->patchUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 560]);
        $invalid_selection->assertStatus(400);
        $invalid_selection->assertHeader('Content-Type', 'application/json');
        $invalid_selection->assertJsonPath('msg', 'Bad Request');
        // Locked selection passed.
        $locked_selection = $this->patchUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 459]);
        $locked_selection->assertStatus(423);
        $locked_selection->assertHeader('Content-Type', 'application/json');
        $locked_selection->assertJsonPath('msg', 'Requested selection locked');
        // Valid request.
        $valid = $this->patchUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 729]);
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('msg', 'Selection updated');
        // Does not have an existing selection.
        $this->delete("$endpoint/nhl-2019-1/105", [], $headers)->assertStatus(200);
        $no_existing = $this->patchUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 729]);
        $no_existing->assertStatus(409);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Existing selection not found');
        // Existing selection is locked.
        $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 2220], $headers)->assertStatus(201);
        $this->setTestDateTime('2020-01-18 03:45:56');
        $no_existing = $this->patchUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 729]);
        $no_existing->assertStatus(423);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Requested selection locked');

        // Non-Admin cannot process requests for another user.
        if (!$as_admin) {
            $another_user = $this->patchUnthrottled("{$this->endpoint_entry}/fantasy_inactive/nhl-2019-1/105");
            $another_user->assertStatus(403);
            $another_user->assertJsonFragment(['msg' => 'Insufficient Privileges']);
        }

        // Admin cannot process requests for a user who doesn't exist.
        if ($as_admin) {
            $unknown_user = $this->patchUnthrottled("{$this->endpoint_entry}/unknown_user/nhl-2019-1/105");
            $unknown_user->assertStatus(404);
            $unknown_user->assertJsonFragment(['msg' => 'User could not be found']);
        }
    }

    /**
     * Test removing a selection via a records-write token
     * @return void
     */
    public function testSelDeleteViaWrite(): void
    {
        $this->selDeleteWorker(false);
    }

    /**
     * Test removing a selection via a records-admin token
     * @return void
     */
    public function testSelDeleteViaAdmin(): void
    {
        $this->setAPIToken($this->token_admin, $this->defaultAPITokenPass);
        $this->selDeleteWorker(true);
        $this->revertAPIToken();
    }

    /**
     * Worker method for removing a selection
     * @param boolean $as_admin That the processing occurrs via a records-admin token.
     * @return void
     */
    public function selDeleteWorker(bool $as_admin): void
    {
        $this->setTestDateTime('2020-01-18 00:45:12');

        // The core endpoint of our requests.
        $endpoint = $this->endpoint_entry . '/fantasy_user';
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];

        // Unknown game.
        $unknown_game = $this->deleteUnthrottled("$endpoint/nhl-2018-1/105", [], $headers);
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'The game could not be found');
        // Unknown period.
        $unknown_period = $this->deleteUnthrottled("$endpoint/nhl-2019-1/999", [], $headers);
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->deleteUnthrottled("$endpoint/nhl-2019-2/105", [], $headers);
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // Valid request.
        $valid = $this->deleteUnthrottled("$endpoint/nhl-2019-1/105", [], $headers);
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('msg', 'Selection removed');
        // Does not have an existing selection.
        $no_existing = $this->deleteUnthrottled("$endpoint/nhl-2019-1/105", [], $headers);
        $no_existing->assertStatus(409);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Existing selection not found');
        // Existing selection is locked.
        $this->postUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 2220], $headers)->assertStatus(201);
        $this->setTestDateTime('2020-01-18 03:45:56');
        $no_existing = $this->deleteUnthrottled("$endpoint/nhl-2019-1/105", ['link_id' => 729]);
        $no_existing->assertStatus(423);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Current selection locked');

        // Non-Admin cannot process requests for another user.
        if (!$as_admin) {
            $another_user = $this->deleteUnthrottled("{$this->endpoint_entry}/fantasy_inactive/nhl-2019-1/105");
            $another_user->assertStatus(403);
            $another_user->assertJsonFragment(['msg' => 'Insufficient Privileges']);
        }

        // Admin cannot process requests for a user who doesn't exist.
        if ($as_admin) {
            $unknown_user = $this->deleteUnthrottled("{$this->endpoint_entry}/unknown_user/nhl-2019-1/105");
            $unknown_user->assertStatus(404);
            $unknown_user->assertJsonFragment(['msg' => 'User could not be found']);
        }
    }

    /**
     * Test processing selections for a game with the "non reuse" flag
     * @return void
     */
    public function testSelReuse(): void
    {
        // For this test, we do need to tweak the result from the default.
        $score = EntryScore::query()
            ->where([
                ['sport', '=', 'nfl'],
                ['season', '=', 2019],
                ['game_id', '=', 3],
                ['user_id', '=', 60],
            ])->get();
        $pos_orig = $score->overall_pos;
        $score->overall_pos = null;
        $score->save();

        // Subsequent selection availability.
        $this->setTestDateTime('2019-09-12 12:34:56'); // Week 2, before the Thursday night game.

        $availability = $this->getUnthrottled("{$this->endpoint_games}/nfl-2019-3/periods/2?page=3");
        $availability->assertStatus(200);
        $availability->assertHeader('Content-Type', 'application/json');
        $availability->assertJsonCount(10, 'sel');
        $availability->assertJsonPath('sel.4.link_id', 65533); // Tampa Bay, a previous selection.
        $availability->assertJsonPath('sel.4.flags.available', false); // Not available.
        $availability->assertJsonMissingPath('sel.4.flags.buster'); // No streak busters.
        $availability->assertJsonPath('sel.3.link_id', 65527); // An unused selction.
        $availability->assertJsonPath('sel.3.flags.available', true); // Is available.
        $availability->assertJsonMissingPath('sel.3.flags.buster'); // No streak busters.
        $availability->assertJsonPath('sel.6.link_id', 65514); // An unused selction.
        $availability->assertJsonMissingPath('sel.6.flags.buster'); // No streak busters.

        // Attempt to use this selection.
        $endpoint = "{$this->endpoint_entry}/fantasy_user/nfl-2019-3/2";
        $headers = ['Content-Type' => 'application/json'];
        $use = $this->postUnthrottled($endpoint, ['link_id' => 65533], $headers);
        $use->assertStatus(409);
        $use->assertHeader('Content-Type', 'application/json');
        $use->assertJsonPath('msg', 'Selection cannot be re-used');

        // Restore the entry score.
        $score->overall_pos = $pos_orig;
        $score->save();
    }

    /**
     * Test processing selections whose event is postponed
     * @return void
     */
    public function testSelPostponed(): void
    {
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];
        $endpoint_sel = "{$this->endpoint_games}/nhl-2019-1/periods/105";
        $endpoint_action = "{$this->endpoint_entry}/fantasy_user/nhl-2019-1/105";
        $time_before = '2020-01-17 12:34:56';
        $time_locked = '2020-01-18 00:42:56';
        $sel_ppd = 2361; // PIT (@ DET, 19:30 start time, game 740).
        $sel_alt = 2220; // TB (@ WGP, 20:00 start time, game 742).

        // Set up the 'before lock time' test.
        $this->setTestDateTime($time_before);

        // As this test will need a little data manipulation, prepare our database event updates.
        $query_ppd = 'UPDATE ' . env('DB_SPORTS_DATABASE') . '.SPORTS_NHL_SCHEDULE SET status = "PPD" '
            . 'WHERE season = 2019 AND game_type = "regular" AND game_id = 740;';
        $query_sched = str_replace('"PPD"', '"F"', $query_ppd);

        /* Test 1: Existing selection can be replaced, but not re-added. */
        $this->assertTrue(DB::unprepared($query_sched));
        $this->deleteUnthrottled($endpoint_action, [], $headers);
        // Add the (faux-emabled) selection.
        $this->postUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed and confirm it is now listed appropriately.
        $this->assertTrue(DB::unprepared($query_ppd));
        $sel_list = $this->getUnthrottled($endpoint_sel);
        $sel_list->assertStatus(200);
        $sel_list->assertJsonPath('sel.0.link_id', 2361);
        $sel_list->assertJsonPath('sel.0.lock_time.status', 'PPD');
        $sel_list->assertJsonPath('sel.0.lock_time.raw', null);
        // Selection can be replaced.
        $this->patchUnthrottled($endpoint_action, ['link_id' => $sel_alt], $headers)->assertStatus(200);
        // Selection cannot now be re-added.
        $this->patchUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(410);

        /* Test 2: Existing selection can be removed, but not re-added. */
        $this->assertTrue(DB::unprepared($query_sched));
        $this->deleteUnthrottled($endpoint_action, [], $headers);
        // Add the (faux-emabled) selection.
        $this->postUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed.
        $this->assertTrue(DB::unprepared($query_ppd));
        // Selection can be removed but not re-added.
        $this->deleteUnthrottled($endpoint_action, [], $headers)->assertStatus(200);
        $this->postUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(410);


        /* Test 3: Existing selection can be replaced after the postponed selection's event lock time. */
        $this->setTestDateTime($time_before);
        $this->assertTrue(DB::unprepared($query_sched));
        $this->deleteUnthrottled($endpoint_action, [], $headers);
        // Add the (faux-emabled) selection.
        $this->postUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed.
        $this->assertTrue(DB::unprepared($query_ppd));
        $this->setTestDateTime($time_locked);
        // Selection can be replaced with another (open) selection after the original's lock time.
        $this->patchUnthrottled($endpoint_action, ['link_id' => $sel_alt], $headers)->assertStatus(200);

        /* Test 4: Existing selection can be removed after the postponed selection's event lock time. */
        $this->setTestDateTime($time_before);
        $this->assertTrue(DB::unprepared($query_sched));
        $this->deleteUnthrottled($endpoint_action, [], $headers);
        // Add the (faux-emabled) selection.
        $this->postUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed.
        $this->assertTrue(DB::unprepared($query_ppd));
        $this->setTestDateTime($time_locked);
        // Selection can be removed after the lock time.
        $this->deleteUnthrottled($endpoint_action, [], $headers)->assertStatus(200);
        // Attempting to re-add after the lock time still gives a 'Gone' status, rather than 'Locked'.
        $this->postUnthrottled($endpoint_action, ['link_id' => $sel_ppd], $headers)->assertStatus(410);
    }

    /**
     * Test weather attributes of individual selections
     * @return void
     */
    public function testSelWeather(): void
    {
        $this->setTestDateTime('2019-09-07 12:34:56'); // Week 1, after the Thursday night game.
        $response = $this->getUnthrottled("{$this->endpoint_games}/nfl-2019-3/periods/1");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonCount(10, 'sel');
        $response->assertJsonPath('sel.0.link_id', 65506); // Baltimore: Weather.
        $response->assertJsonPath('sel.0.weather.symbol', 'lightning');
        $response->assertJsonPath('sel.0.weather.temp', 88);
        $response->assertJsonPath('sel.0.weather.precipitation', 90);
        $response->assertJsonPath('sel.0.weather.wind.speed', 5);
        $response->assertJsonPath('sel.0.weather.wind.direction', 'S');
        $response->assertJsonPath('sel.1.link_id', 65509); // Chicago: Weather.
        $response->assertJsonPath('sel.1.weather.symbol', 'cloudy');
        $response->assertJsonPath('sel.1.weather.temp', 88);
        $response->assertJsonPath('sel.1.weather.precipitation', 0);
        $response->assertJsonPath('sel.1.weather.wind.speed', 1);
        $response->assertJsonPath('sel.1.weather.wind.direction', 'SE');
        $response->assertJsonPath('sel.2.link_id', 65519); // Kansas City: Weather.
        $response->assertJsonPath('sel.2.weather.symbol', 'mist');
        $response->assertJsonPath('sel.2.weather.temp', 83);
        $response->assertJsonPath('sel.2.weather.precipitation', 100);
        $response->assertJsonPath('sel.2.weather.wind.speed', 1);
        $response->assertJsonPath('sel.2.weather.wind.direction', 'E');
        $response->assertJsonPath('sel.3.link_id', 65531); // Seattle: Dome.
        $response->assertJsonPath('sel.3.weather.symbol', 'dome');
    }
}
