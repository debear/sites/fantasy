<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;
use DeBear\Repositories\InternalCache;

class CEntryTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * The (base) endpoint we'll be targetting in these tests
     * @var string
     */
    protected $endpoint = '/api/records/v1.0/entries';

    /**
     * Get an existing entry
     * @return void
     */
    public function testGetExisting(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // A user who does have an existing team.
        $valid = $this->getUnthrottled("{$this->endpoint}/fantasy_user");
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('user_id', 'fantasy_user');
        $valid->assertJsonPath('name', 'Record Testers');
        $valid->assertJsonPath('email_status', true);
        $valid->assertJsonCount(5, 'games_active');
        $valid->assertJsonPath('games_active.nfl-2019-1.overall.result', 39);
        $valid->assertJsonPath('games_active.nfl-2019-1.overall.pos.num', 99);
        $valid->assertJsonPath('games_active.nfl-2019-1.overall.pos.chg', null);
        $valid->assertJsonMissingPath('games_active.nfl-2019-1.overall.achievements');
        $valid->assertJsonMissingPath('games_active.nfl-2019-1.overall.opponents');
        $valid->assertJsonPath('games_active.nfl-2019-3.overall.result', 0);
        $valid->assertJsonPath('games_active.nfl-2019-3.overall.pos.num', 1234);
        $valid->assertJsonPath('games_active.nfl-2019-3.overall.pos.chg', null);
        $valid->assertJsonPath('games_active.nhl-2019-1.overall.result', 15);
        $valid->assertJsonPath('games_active.nhl-2019-1.overall.pos.num', 3);
        $valid->assertJsonPath('games_active.nhl-2019-1.overall.pos.chg', 0);
        $valid->assertJsonPath('games_active.nhl-2019-3.overall.result', 7);
        $valid->assertJsonPath('games_active.nhl-2019-3.overall.pos.num', 43);
        $valid->assertJsonPath('games_active.nhl-2019-3.overall.pos.chg', 0);
        $valid->assertJsonPath('games_active.ahl-2019-2.overall.result', 66);
        $valid->assertJsonPath('games_active.ahl-2019-2.overall.pos.num', 11);
        $valid->assertJsonPath('games_active.ahl-2019-2.overall.pos.chg', 2);

        // A user who doesn't exist.
        $unknown_user = $this->getUnthrottled("{$this->endpoint}/fantasy_unknown");
        $unknown_user->assertStatus(404);
        $unknown_user->assertHeader('Content-Type', 'application/json');
        $unknown_user->assertJsonPath('msg', 'User could not be found');

        // A user who doesn't have a team.
        $no_team = $this->getUnthrottled("{$this->endpoint}/fantasy_inactive");
        $no_team->assertStatus(404);
        $no_team->assertHeader('Content-Type', 'application/json');
        $no_team->assertJsonPath('msg', 'Entry could not be found');
    }

    /**
     * Manage an entry (creation and update) via a records-write token
     * @return void
     */
    public function testManageEntryViaWrite(): void
    {
        $this->setAPIToken('fantasy-clean', 'apitokenpassword');
        $this->manageEntryWorker('fantasy_clean', false);
        $this->revertAPIToken();
    }

    /**
     * Manage an entry (creation and update) via a records-admin token
     * @return void
     */
    public function testManageEntryViaAdmin(): void
    {
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $this->manageEntryWorker('fantasy_admin', true);
        $this->revertAPIToken();
    }

    /**
     * Worker method for managing entries
     * @param string  $user_id  The ID of the user to be manipulated.
     * @param boolean $as_admin That the processing occurrs via a records-admin token.
     * @return void
     */
    protected function manageEntryWorker(string $user_id, bool $as_admin): void
    {
        $data = [
            'name' => 'New CI Entry',
            'email_status' => '0',
        ];
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];

        // Non-Admin cannot create an entry for another user.
        if (!$as_admin) {
            $another_user = $this->postUnthrottled("{$this->endpoint}/fantasy_inactive", $data, $headers);
            $another_user->assertStatus(403);
            $another_user->assertJsonFragment(['msg' => 'Insufficient Privileges']);
        }

        // Admin cannot create an entry for a user who doesn't exist.
        if ($as_admin) {
            $unknown_user = $this->postUnthrottled("{$this->endpoint}/unknown_user", $data, $headers);
            $unknown_user->assertStatus(404);
            $unknown_user->assertJsonFragment(['msg' => 'User could not be found']);
        }

        // Cannot update an entry for a user who hasn't joined.
        $not_joined = $this->patchUnthrottled("{$this->endpoint}/$user_id", $data, $headers);
        $not_joined->assertStatus(404);
        $not_joined->assertJsonFragment(['msg' => 'Entry could not be found']);

        // Must pass-in JSON data.
        $orig = InternalCache::object()->get('php://input');
        InternalCache::object()->set('php://input', '');
        $no_data = $this->postUnthrottled("{$this->endpoint}/$user_id", [], $headers);
        $no_data->assertStatus(400);
        $no_data->assertJsonFragment(['msg' => 'Bad Request']);
        InternalCache::object()->set('php://input', $orig);

        // Must match our validation rules.
        $invalid_data = $data;
        unset($invalid_data['name']);
        $invalid = $this->postUnthrottled("{$this->endpoint}/$user_id", $invalid_data, $headers);
        $invalid->assertStatus(400);
        $invalid->assertJsonFragment(['msg' => 'The input data did not pass the required validation rules']);

        // Create an entry.
        $invalid = $this->postUnthrottled("{$this->endpoint}/$user_id", $data, $headers);
        $invalid->assertStatus(201);
        $invalid->assertJsonFragment(['msg' => 'Entry created successfully']);

        // Validate it.
        $validate = $this->getUnthrottled("{$this->endpoint}/$user_id");
        $validate->assertStatus(200);
        $validate->assertHeader('Content-Type', 'application/json');
        $validate->assertJsonPath('user_id', $user_id);
        $validate->assertJsonPath('name', $data['name']);
        $validate->assertJsonPath('email_status', false);

        // Cannot create another entry for the same user.
        $invalid = $this->postUnthrottled("{$this->endpoint}/$user_id", $data, $headers);
        $invalid->assertStatus(409);
        $invalid->assertJsonFragment(['msg' => 'An entry already exists for this user']);

        // Update our new entry.
        $update_data = $data;
        $invalid = $this->patchUnthrottled("{$this->endpoint}/$user_id", $update_data, $headers);
        $invalid->assertStatus(200);
        $invalid->assertJsonFragment(['msg' => 'Entry updated successfully']);
    }

    /**
     * Manage an entry's games (joining and leaving) via a records-write token
     * @return void
     */
    public function testManageGamesViaWrite(): void
    {
        $this->setAPIToken('fantasy-token', 'apitokenpassword');
        $this->manageGamesWorker(false);
        $this->revertAPIToken();
    }

    /**
     * Manage an entry's games (joining and leaving) via a records-admin token
     * @return void
     */
    public function testManageGamesViaAdmin(): void
    {
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $this->manageGamesWorker(true);
        $this->revertAPIToken();
    }

    /**
     * Worker method for managing entries
     * @param boolean $as_admin That the processing occurrs via a records-admin token.
     * @return void
     */
    protected function manageGamesWorker(bool $as_admin): void
    {
        $headers = ['Accept' => 'application/json'];

        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // Non-Admin cannot manage an entry's games for another user.
        if (!$as_admin) {
            $another_user = $this->postUnthrottled("{$this->endpoint}/fantasy_inactive/nfl-2019-1", [], $headers);
            $another_user->assertStatus(403);
            $another_user->assertJsonFragment(['msg' => 'Insufficient Privileges']);
        }

        // Admin cannot create an entry for a user who doesn't exist.
        if ($as_admin) {
            $unknown_user = $this->postUnthrottled("{$this->endpoint}/unknown_user/nfl-2019-1", [], $headers);
            $unknown_user->assertStatus(404);
            $unknown_user->assertJsonFragment(['msg' => 'User could not be found']);
        }

        // User must have an entry.
        if ($as_admin) {
            $no_entry = $this->postUnthrottled("{$this->endpoint}/fantasy_clean/nfl-2019-1", [], $headers);
            $no_entry->assertStatus(400);
            $no_entry->assertJsonFragment(['msg' => 'The user does not have an entry']);
        }

        // Requested game must exist.
        $join_unknown_game = $this->postUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-4", [], $headers);
        $join_unknown_game->assertStatus(404);
        $join_unknown_game->assertJsonFragment(['msg' => 'The game could not be found']);

        $leave_unknown_game = $this->deleteUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-4", [], $headers);
        $leave_unknown_game->assertStatus(404);
        $leave_unknown_game->assertJsonFragment(['msg' => 'The game could not be found']);

        // Requested game must be open.
        $game_not_open = $this->postUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-1", [], $headers);
        $game_not_open->assertStatus(404);
        $game_not_open->assertJsonFragment(['msg' => 'The game could not be found']);

        // Entry cannot leave a game they not yet playing.
        $not_yet_joined = $this->deleteUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2", [], $headers);
        $not_yet_joined->assertStatus(404);
        $not_yet_joined->assertJsonFragment(['msg' => 'The entry has not already joined this game']);

        // Join the game.
        $join_game = $this->postUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2", [], $headers);
        $join_game->assertStatus(201);
        $join_game->assertJsonFragment(['msg' => 'Game joined successfully']);

        // Entry cannot join a game they already playing.
        $rejoin_game = $this->postUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2", [], $headers);
        $rejoin_game->assertStatus(409);
        $rejoin_game->assertJsonFragment(['msg' => 'The entry has already joined this game']);

        // Leave the game.
        $leave_game = $this->deleteUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2", [], $headers);
        $leave_game->assertStatus(200);
        $leave_game->assertJsonFragment(['msg' => 'Game left successfully']);

        // Entry cannot leave a game once it is in progress.
        $leave_started = $this->deleteUnthrottled("{$this->endpoint}/fantasy_user/nhl-2019-1", [], $headers);
        $leave_started->assertStatus(423);
        $leave_started->assertJsonFragment(['msg' => 'The entry has started playing this game']);

        // Cannot join between after date_close - uses a different time.
        $this->setTestDateTime('2019-11-15 12:34:56');
        $rejoin_game = $this->postUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2", [], $headers);
        $rejoin_game->assertStatus(400);
        $rejoin_game->assertJsonFragment(['msg' => 'The action is not available for this game']);
    }

    /**
     * The test of an entry to a single game
     * @return void
     */
    public function testGetSingleGame(): void
    {
        // Totals game.
        $this->setTestDateTime('2019-10-15 12:34:56');
        $totals = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-1");
        $totals->assertStatus(200);
        $totals->assertHeader('Content-Type', 'application/json');
        $totals->assertJsonMissingPath('user_id');
        $totals->assertJsonPath('overall.result', 39);
        $totals->assertJsonPath('overall.pos.num', 99);
        $totals->assertJsonPath('overall.pos.chg', null);
        $totals->assertJsonMissingPath('hitrate');
        $totals->assertJsonPath('stress', 255);
        $totals->assertJsonPath('sel.last.period_id', 5);
        $totals->assertJsonPath('sel.last.link_id', null);
        $totals->assertJsonPath('sel.last.lock_time.status', null);
        $totals->assertJsonPath('sel.last.lock_time.raw', '2019-10-08T01:10:00+01:00');
        $totals->assertJsonPath('sel.curr.period_id', 6);
        $totals->assertJsonPath('sel.curr.link_id', null);
        $totals->assertJsonPath('sel.curr.lock_time.status', null);
        $totals->assertJsonPath('sel.curr.lock_time.raw', '2019-10-15T01:10:00+01:00');

        // Streak game, ended, no hitrate info.
        $this->setTestDateTime('2019-09-15 12:34:56');
        $streak_ended = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-3");
        $streak_ended->assertStatus(200);
        $streak_ended->assertHeader('Content-Type', 'application/json');
        $streak_ended->assertJsonMissingPath('user_id');
        $streak_ended->assertJsonPath('overall.result', 0);
        $streak_ended->assertJsonPath('overall.pos.num', 1234);
        $streak_ended->assertJsonPath('overall.pos.chg', null);
        $streak_ended->assertJsonMissingPath('current');
        $streak_ended->assertJsonMissingPath('hitrate');
        $streak_ended->assertJsonPath('stress', 255);
        $streak_ended->assertJsonPath('sel.last.period_id', 1);
        $streak_ended->assertJsonPath('sel.last.link_id', 65533);
        $streak_ended->assertJsonPath('sel.last.lock_time.status', null);
        $streak_ended->assertJsonPath('sel.last.lock_time.raw', '2019-09-08T21:20:00+01:00');
        $streak_ended->assertJsonPath('sel.last.score', 0);
        $streak_ended->assertJsonPath('sel.last.status', 'negative');
        $streak_ended->assertJsonPath('sel.last.stress', 255);
        $streak_ended->assertJsonPath('sel.last.summary', 'L 31&ndash;17');
        $streak_ended->assertJsonPath('sel.last.entity.name', 'Tampa Bay Buccaneers');
        $streak_ended->assertJsonPath('sel.last.entity.icon', 'TB');
        $streak_ended->assertJsonPath('sel.last.entity.info', 'v');
        $streak_ended->assertJsonPath('sel.last.entity.info_icon', 'SF');
        $streak_ended->assertJsonPath('sel.curr', null);

        // Streak game, on-going.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $streak_active = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-3");
        $streak_active->assertStatus(200);
        $streak_active->assertHeader('Content-Type', 'application/json');
        $streak_active->assertJsonMissingPath('user_id');
        $streak_active->assertJsonPath('overall.result', 4);
        $streak_active->assertJsonPath('overall.pos.num', 132);
        $streak_active->assertJsonPath('overall.pos.chg', 33);
        $streak_active->assertJsonPath('current.result', 1);
        $streak_active->assertJsonPath('current.pos.num', 254);
        $streak_active->assertJsonPath('current.pos.chg', -1);
        $streak_active->assertJsonPath('hitrate.result', '24.00%');
        $streak_active->assertJsonPath('hitrate.pos.num', 95);
        $streak_active->assertJsonPath('hitrate.pos.chg', -3);
        $streak_active->assertJsonPath('hitrate.raw.positive', 12);
        $streak_active->assertJsonPath('hitrate.raw.negative', 38);
        $streak_active->assertJsonPath('stress', 243);
        $streak_active->assertJsonPath('sel.last.stress', 242);
        $streak_active->assertJsonPath('sel.curr.stress', null);

        // Edge cases.
        $unknown_game = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-4");
        $unknown_game->assertStatus(404);
        $not_joined = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2");
        $not_joined->assertStatus(404);
        // Admin user attempts to process a user who does not have an entry.
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $unknown_user = $this->getUnthrottled("{$this->endpoint}/fantasy_unknown/nfl-2019-4");
        $unknown_user->assertStatus(404);
        $this->revertAPIToken();
    }

    /**
     * The test of an entry to the period in a single game
     * @return void
     */
    public function testGetSingleGamePeriod(): void
    {
        // Totals game.
        $this->setTestDateTime('2019-10-15 12:34:56');
        $totals_a = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-1/5");
        $totals_a->assertStatus(200);
        $totals_a->assertHeader('Content-Type', 'application/json');
        $totals_a->assertJsonPath('period_id', 5);
        $totals_a->assertJsonPath('link_id', null);
        $totals_a->assertJsonMissingPath('achievements');
        $totals_a->assertJsonMissingPath('opponents');
        $totals_a->assertJsonPath('lock_time.status', null);
        $totals_a->assertJsonPath('lock_time.raw', '2019-10-08T01:10:00+01:00');
        $totals_b = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-1/6");
        $totals_b->assertStatus(200);
        $totals_b->assertHeader('Content-Type', 'application/json');
        $totals_b->assertJsonPath('period_id', 6);
        $totals_b->assertJsonPath('link_id', null);
        $totals_b->assertJsonMissingPath('achievements');
        $totals_b->assertJsonMissingPath('opponents');
        $totals_b->assertJsonPath('lock_time.status', null);
        $totals_b->assertJsonPath('lock_time.raw', '2019-10-15T01:10:00+01:00');

        // Streak game, ended.
        $this->setTestDateTime('2019-09-15 12:34:56');
        $streak_ended_a = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-3/1");
        $streak_ended_a->assertStatus(200);
        $streak_ended_a->assertHeader('Content-Type', 'application/json');
        $streak_ended_a->assertJsonPath('period_id', 1);
        $streak_ended_a->assertJsonPath('link_id', 65533);
        $streak_ended_a->assertJsonPath('lock_time.status', null);
        $streak_ended_a->assertJsonPath('lock_time.raw', '2019-09-08T21:20:00+01:00');
        $streak_ended_a->assertJsonPath('score', 0);
        $streak_ended_a->assertJsonPath('status', 'negative');
        $streak_ended_a->assertJsonPath('stress', 255);
        $streak_ended_a->assertJsonMissingPath('achievements');
        $streak_ended_a->assertJsonMissingPath('opponents');
        $streak_ended_a->assertJsonPath('summary', 'L 31&ndash;17');
        $streak_ended_a->assertJsonPath('entity.name', 'Tampa Bay Buccaneers');
        $streak_ended_a->assertJsonPath('entity.icon', 'TB');
        $streak_ended_a->assertJsonPath('entity.info', 'v');
        $streak_ended_a->assertJsonPath('entity.info_icon', 'SF');
        $streak_ended_b = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-3/2");
        $streak_ended_b->assertStatus(422);

        // Edge cases.
        $unknown_game = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-4/1");
        $unknown_game->assertStatus(404);
        $not_joined = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2/1");
        $not_joined->assertStatus(404);
        $unknown_period = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-1/20");
        $unknown_period->assertStatus(404);
        // Admin user attempts to process a user who does not have an entry.
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $unknown_user = $this->getUnthrottled("{$this->endpoint}/fantasy_unknown/nfl-2019-4/1");
        $unknown_user->assertStatus(404);
        $this->revertAPIToken();
    }

    /**
     * Test loading of individual entry's achievements
     * @return void
     */
    public function testEntryAchievements(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2020-08-10 12:34:56');

        // Unknown entry.
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $unknown_entry = $this->getUnthrottled("{$this->endpoint}/unknown_user/mlb-2020-1/achievements");
        $unknown_entry->assertStatus(404);
        $this->revertAPIToken();

        // Unknown game.
        $unknown_game = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-4/achievements");
        $unknown_game->assertStatus(404);

        // Not joined the game.
        $not_joined = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-2/achievements");
        $not_joined->assertStatus(404);

        // A regular game.
        $regular = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-1/achievements");
        $regular->assertStatus(200);
        $regular->assertHeader('Content-Type', 'application/json');
        $regular->assertJsonCount(4);
        $regular->assertJsonPath('1.1', true);
        $regular->assertJsonPath('1.2', true);
        $regular->assertJsonPath('1.3', false);
        $regular->assertJsonPath('1.4', false);
        $regular->assertJsonPath('4.4', true);

        // Achievement changes within their current entry record.
        $entry_score = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-1");
        $entry_score->assertStatus(200);
        $entry_score->assertHeader('Content-Type', 'application/json');
        $entry_score->assertJsonPath('achieved.achievements.1.1', true);
        $entry_score->assertJsonMissingPath('achieved.achievements.2');
        $entry_score->assertJsonPath('achieved.opponent', true);

        // Achievement changes within a period's selection.
        $period_sel = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-1/2");
        $period_sel->assertStatus(200);
        $period_sel->assertHeader('Content-Type', 'application/json');
        $period_sel->assertJsonPath('achieved.achievements.1.1', true);
        $period_sel->assertJsonMissingPath('achieved.achievements.2');
        $period_sel->assertJsonPath('achieved.opponent', true);

        // Achievement changes within an entry's selection list.
        $entry_sel = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-1/selections");
        $entry_sel->assertStatus(200);
        $entry_sel->assertHeader('Content-Type', 'application/json');
        $entry_sel->assertJsonPath('0.achieved.achievements.1.1', true);
        $entry_sel->assertJsonMissingPath('0.achieved.achievements.2');
        $entry_sel->assertJsonPath('0.achieved.opponent', true);
    }

    /**
     * Test loading of individual entry's opponents
     * @return void
     */
    public function testEntryOpponents(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2020-08-10 12:34:56');

        // Unknown entry.
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $unknown_entry = $this->getUnthrottled("{$this->endpoint}/unknown_user/mlb-2020-1/opponents");
        $unknown_entry->assertStatus(404);
        $this->revertAPIToken();

        // Unknown game.
        $unknown_game = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-4/opponents");
        $unknown_game->assertStatus(404);

        // Not joined the game.
        $not_joined = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-2/opponents");
        $not_joined->assertStatus(404);

        // A regular game.
        $regular = $this->getUnthrottled("{$this->endpoint}/fantasy_user/mlb-2020-1/opponents");
        $regular->assertStatus(200);
        $regular->assertHeader('Content-Type', 'application/json');
        $regular->assertJsonCount(30);
        $regular->assertJsonPath('ARI', true);
        $regular->assertJsonPath('ATL', false);
        $regular->assertJsonPath('MIL', false);
        $regular->assertJsonPath('OAK', false);
        $regular->assertJsonPath('WSH', false);

        // List does not include an opponent-based achievement.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australian Grand Prix.
        $no_opp = $this->getUnthrottled("{$this->endpoint}/fantasy_user/f1-2020-1/opponents");
        $no_opp->assertStatus(400);
    }

    /**
     * Test loading of individual entry's selections
     * @return void
     */
    public function testEntrySelections(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // Unknown entry.
        $this->setAPIToken('fantasy-admin', 'apitokenpassword');
        $unknown_entry = $this->getUnthrottled("{$this->endpoint}/unknown_user/nfl-2019-1/selections");
        $unknown_entry->assertStatus(404);
        $this->revertAPIToken();

        // Unknown game.
        $unknown_game = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-4/selections");
        $unknown_game->assertStatus(404);

        // Not joined the game.
        $not_joined = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-2/selections");
        $not_joined->assertStatus(404);

        // A regular game.
        $regular = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-1/selections");
        $regular->assertStatus(200);
        $regular->assertHeader('Content-Type', 'application/json');
        $regular->assertJsonCount(5);
        $regular->assertJsonPath('0.period_id', 5);
        $regular->assertJsonPath('0.result', 39);
        $regular->assertJsonPath('0.sel', []);
        $regular->assertJsonPath('3.period_id', 2);
        $regular->assertJsonPath('3.sel.link_id', 5126);
        $regular->assertJsonPath('3.sel.link.name', 'Derrick Henry');
        $regular->assertJsonPath('3.sel.link.info', 'v');
        $regular->assertJsonPath('3.sel.link.info_icon', 'IND');
        $regular->assertJsonPath('3.result', 39);
        $regular->assertJsonPath('4.period_id', 1);
        $regular->assertJsonPath('4.sel.link_id', 5409);
        $regular->assertJsonPath('4.sel.link.name', 'Aaron Jones');
        $regular->assertJsonPath('4.sel.link.info', '@');
        $regular->assertJsonPath('4.sel.link.info_icon', 'CHI');
        $regular->assertJsonPath('4.stress', 255);
        $regular->assertJsonPath('4.result', 39);
        $regular->assertJsonPath('4.status', 'negative');
        $regular->assertJsonPath('4.summary', '39yds');

        // A game with selection restriction flags.
        $restricted = $this->getUnthrottled("{$this->endpoint}/fantasy_user/nfl-2019-3/selections");
        $restricted->assertStatus(200);
        $restricted->assertHeader('Content-Type', 'application/json');
        $restricted->assertJsonCount(1);
        $restricted->assertJsonPath('0.period_id', 1);
        $restricted->assertJsonPath('0.sel.link_id', 65533);
        $restricted->assertJsonPath('0.sel.link.name', 'Tampa Bay Buccaneers');
        $restricted->assertJsonPath('0.sel.link.info', 'v');
        $restricted->assertJsonPath('0.sel.link.info_icon', 'SF');
        $restricted->assertJsonPath('0.stress', 255);
        $restricted->assertJsonPath('0.result', 0);
        $restricted->assertJsonPath('0.status', 'negative');
        $restricted->assertJsonPath('0.summary', 'L 31&ndash;17');
    }
}
