<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;

class VAutoUserTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * Ensure the API endpoints cannot process requests for automated users even using an admin token.
     * @return void
     */
    public function testAutoUserJourney(): void
    {
        // Uses a preset time and an admin token.
        $this->setTestDateTime('2019-09-06 12:34:56');
        $this->setAPIToken('fantasy-admin', $this->defaultAPITokenPass);
        $user_id = 'classics26864';
        $game_id = 'nfl-2019-3'; // The Perfect Season.
        $period_id = 1; // Week 1.
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];

        // Create an entry.
        $create_entry = $this->postUnthrottled("/api/records/v1.0/entries/$user_id", [
            'name' => 'AutoUser CI Entry',
            'email_status' => '1',
        ], $headers);
        $create_entry->assertStatus(422);

        // Update the entry.
        $update_entry = $this->patchUnthrottled("/api/records/v1.0/entries/$user_id", [
            'email_status' => '0',
        ], $headers);
        $update_entry->assertStatus(422);

        // View the entry.
        $view_entry = $this->getUnthrottled("/api/records/v1.0/entries/$user_id");
        $view_entry->assertStatus(404); // Entry does not exist.

        // Join a game.
        $join_game = $this->postUnthrottled("/api/records/v1.0/entries/$user_id/$game_id", [], $headers);
        $join_game->assertStatus(422);

        // View the game entry.
        $view_game = $this->getUnthrottled("/api/records/v1.0/entries/$user_id/$game_id");
        $view_game->assertStatus(400); // Bad request because user has no entry.

        // Make a selection.
        $make_sel = $this->postUnthrottled("/api/records/v1.0/entries/$user_id/$game_id/$period_id", [
            'link_id' => 65533,
        ], $headers);
        $make_sel->assertStatus(422);

        // Update the selection.
        $update_sel = $this->patchUnthrottled("/api/records/v1.0/entries/$user_id/$game_id/$period_id", [
            'link_id' => 65535,
        ], $headers);
        $update_sel->assertStatus(422);

        // View the selection.
        $view_sel = $this->getUnthrottled("/api/records/v1.0/entries/$user_id/$game_id/$period_id");
        $view_sel->assertStatus(400); // Bad request because user has no entry.

        // Remove a selection.
        $remote_sel = $this->deleteUnthrottled("/api/records/v1.0/entries/$user_id/$game_id/$period_id");
        $remote_sel->assertStatus(422);

        // Leave a game.
        $leave_game = $this->deleteUnthrottled("/api/records/v1.0/entries/$user_id/$game_id", [], $headers);
        $leave_game->assertStatus(422);
    }
}
