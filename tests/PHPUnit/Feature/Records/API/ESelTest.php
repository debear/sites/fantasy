<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;

class ESelTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * The name of the API Token for our records-admin based calls
     * @var string
     */
    protected $token_admin = 'fantasy-admin';

    /**
     * Test loading of individual selections summary information
     * @return void
     */
    public function testSummaryModal(): void
    {
        $endpoint = '/api/records/v1.0/games/nhl-2019-1/links/2220';
        $this->setTestDateTime('2020-01-17 12:34:56');

        // Unknown game.
        $unknown_game = $this->getUnthrottled(str_replace('-2019-', '-2018-', $endpoint) . '/105');
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'The game could not be found');

        // Unknown period.
        $unknown_period = $this->getUnthrottled("$endpoint/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');

        // Future period.
        $future_period = $this->getUnthrottled("$endpoint/120");
        $future_period->assertStatus(404);
        $future_period->assertHeader('Content-Type', 'application/json');
        $future_period->assertJsonPath('msg', 'Game period not found');

        // Unknown link.
        $unknown_link = $this->getUnthrottled(str_replace('/2220', '/9999', $endpoint) . '/105');
        $unknown_link->assertStatus(404);
        $unknown_link->assertHeader('Content-Type', 'application/json');
        $unknown_link->assertJsonPath('msg', 'Selection not found or not available');

        // Valid request.
        $valid = $this->getUnthrottled("$endpoint/105");
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('link_id', 2220);
        $valid->assertJsonPath('rating', 80);
        $valid->assertJsonPath('opp_rating', 58);
        $valid->assertJsonPath('flags.available', true);
        $valid->assertJsonPath('stats.1', '20');
        $valid->assertJsonPath('stats.2', '0.43');
        $valid->assertJsonCount(2, 'recent');
        $valid->assertJsonPath('recent.0.period_id', 105);
        $valid->assertJsonPath('recent.0.info', '@');
        $valid->assertJsonPath('recent.0.info_icon', 'WPG');
        $valid->assertJsonPath('recent.0.rating', 80);
        $valid->assertJsonPath('recent.0.opp_rating', 58);
        $valid->assertJsonPath('recent.1.period_id', 104);
        $valid->assertJsonPath('recent.1.info', '@');
        $valid->assertJsonPath('recent.1.info_icon', 'MIN');
        $valid->assertJsonPath('recent.1.score', 2);
        $valid->assertJsonPath('recent.1.status', 'positive');
        $valid->assertJsonPath('recent.1.stress', 63);
        $valid->assertJsonPath('recent.1.summary', '2 G');
    }

    /**
     * Test the selection flags against each selection
     * @return void
     */
    public function testSelectionFlags(): void
    {
        $base = '/api/records/v1.0/games';

        // Available.
        $this->setTestDateTime('2020-01-17 12:34:56');
        $available = $this->getUnthrottled("$base/nhl-2019-1/links/2361/105");
        $available->assertStatus(200);
        $available->assertHeader('Content-Type', 'application/json');
        $available->assertJsonPath('flags.available', true);
        $available->assertJsonMissingPath('flags.buster');
        $available->assertJsonMissingPath('flags.achieve_opp');

        // Streak Buster.
        $this->setTestDateTime('2020-01-16 12:34:56');
        $buster = $this->getUnthrottled("$base/nhl-2019-3/links/2220/104");
        $buster->assertStatus(200);
        $buster->assertHeader('Content-Type', 'application/json');
        $buster->assertJsonPath('flags.available', true);
        $buster->assertJsonPath('flags.buster', false);
        $buster->assertJsonMissingPath('flags.achieve_opp');

        // Opponent.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $achieve_opp = $this->getUnthrottled("$base/mlb-2020-3/links/65531/3");
        $achieve_opp->assertStatus(200);
        $achieve_opp->assertHeader('Content-Type', 'application/json');
        $achieve_opp->assertJsonPath('flags.available', true);
        $achieve_opp->assertJsonPath('flags.buster', false);
        $achieve_opp->assertJsonPath('flags.achieve_opp', false);
    }
}
