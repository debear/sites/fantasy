<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;
use DeBear\Repositories\InternalCache;

class BGameTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * The (base) endpoint we'll be targetting in these tests
     * @var string
     */
    protected $endpoint = '/api/records/v1.0/games';

    /**
     * Testing the various date sequences for a game when the user has not joined
     * @return void
     */
    public function testDateSequenceNotJoined(): void
    {
        $test_game = 'f1-2020-1'; // 413 point season.
        $this->setAPIToken('fantasy-clean', 'apitokenpassword');

        // More than a week before date_open.
        $this->setTestDateTime('2020-02-20 12:34:56');
        $before_list = $this->getUnthrottled($this->endpoint);
        $before_list->assertStatus(200);
        $before_list->assertDontSee('"413 Point Season"');
        $before = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $before->assertStatus(404);

        // Within a week of date_open.
        $this->setTestDateTime('2020-02-28 12:34:56');
        $upcoming_list = $this->getUnthrottled("{$this->endpoint}/upcoming");
        $upcoming_list->assertStatus(200);
        $upcoming_list->assertSee('"413 Point Season"');
        $upcoming = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $upcoming->assertStatus(404);

        // Between date_open and date_start.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australia.
        $preseason_list = $this->getUnthrottled("{$this->endpoint}/open");
        $preseason_list->assertStatus(200);
        $preseason_list->assertSee('"413 Point Season"');
        $preseason = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $preseason->assertStatus(200);

        // Between date_start and date_close.
        $this->setTestDateTime('2020-03-21 12:34:56'); // Bahrain.
        $inprogress_list = $this->getUnthrottled("{$this->endpoint}/inprogress");
        $inprogress_list->assertStatus(200);
        $inprogress_list->assertSee('"413 Point Season"');
        $inprogress = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $inprogress->assertStatus(200);

        // Between date_close and date_end.
        $this->setTestDateTime('2020-06-12 12:34:56'); // Canada.
        $reg_closed_list = $this->getUnthrottled("{$this->endpoint}/closed");
        $reg_closed_list->assertStatus(200);
        $reg_closed_list->assertSee('"413 Point Season"');
        $reg_closed = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $reg_closed->assertStatus(200);

        // Between date_end and date_complete.
        $this->setTestDateTime('2020-12-12 12:34:56'); // After Abu Dhabi.
        $completed_list = $this->getUnthrottled("{$this->endpoint}/complete");
        $completed_list->assertStatus(200);
        $completed_list->assertSee('"413 Point Season"');
        $completed = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $completed->assertStatus(200);

        // After date_complete.
        $this->setTestDateTime('2020-12-31 12:34:56');
        $after_list = $this->getUnthrottled($this->endpoint);
        $after_list->assertStatus(200);
        $after_list->assertDontSee('"413 Point Season"');
        $after = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $after->assertStatus(404);
    }

    /**
     * Testing the various date sequences for a game when a user is logged in
     * @return void
     */
    public function testDateSequenceUser(): void
    {
        $test_game = 'f1-2020-1'; // 413 point season.
        $test_entry = '/api/records/v1.0/entries/fantasy_clean';
        $this->setAPIToken('fantasy-clean', 'apitokenpassword');

        // More than a week before date_open.
        $this->setTestDateTime('2020-02-20 12:34:56');
        $before_list = $this->getUnthrottled($this->endpoint);
        $before_list->assertStatus(200);
        $before_list->assertDontSee('"413 Point Season"');
        $before = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $before->assertStatus(404);

        // Within a week of date_open.
        $this->setTestDateTime('2020-02-28 12:34:56');
        $upcoming_list = $this->getUnthrottled("{$this->endpoint}/upcoming");
        $upcoming_list->assertStatus(200);
        $upcoming_list->assertSee('"413 Point Season"');
        $upcoming = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $upcoming->assertStatus(404);

        // Between date_open and date_start.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australia.
        $preseason_list = $this->getUnthrottled("{$this->endpoint}/open");
        $preseason_list->assertStatus(200);
        $preseason_list->assertSee('"413 Point Season"');
        $preseason = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $preseason->assertStatus(200);

        // Register and join.
        $data = ['name' => 'API Date Sequences', 'email_status' => '0'];
        $headers = ['Content-Type' => 'application/json'];
        $this->postUnthrottled($test_entry, $data, $headers)->assertStatus(201);
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(201);

        // Re-run the preseason queries for the updated info.
        $preseason_open = $this->getUnthrottled("{$this->endpoint}/open");
        $preseason_open->assertStatus(200);
        $preseason_open->assertDontSee('"413 Point Season"');
        $preseason_active = $this->getUnthrottled("{$this->endpoint}/active");
        $preseason_active->assertStatus(200);
        $preseason_active->assertSee('"413 Point Season"');
        $preseason_joined = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $preseason_joined->assertStatus(200);

        // Between date_start and date_close.
        $this->setTestDateTime('2020-03-21 12:34:56'); // Bahrain.
        $inprogress_list = $this->getUnthrottled("{$this->endpoint}/inprogress");
        $inprogress_list->assertStatus(200);
        $inprogress_list->assertDontSee('"413 Point Season"');
        $inprogress_active = $this->getUnthrottled("{$this->endpoint}/active");
        $inprogress_active->assertStatus(200);
        $inprogress_active->assertSee('"413 Point Season"');
        $inprogress = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $inprogress->assertStatus(200);

        // Between date_close and date_end.
        $this->setTestDateTime('2020-06-12 12:34:56'); // Canada.
        $reg_closed_list = $this->getUnthrottled("{$this->endpoint}/closed");
        $reg_closed_list->assertStatus(200);
        $reg_closed_list->assertDontSee('"413 Point Season"');
        $reg_closed_active = $this->getUnthrottled("{$this->endpoint}/active");
        $reg_closed_active->assertStatus(200);
        $reg_closed_active->assertSee('"413 Point Season"');
        $reg_closed = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $reg_closed->assertStatus(200);

        // Between date_end and date_complete.
        $this->setTestDateTime('2020-12-12 12:34:56'); // After Abu Dhabi.
        $completed_list = $this->getUnthrottled("{$this->endpoint}/complete");
        $completed_list->assertStatus(200);
        $completed_list->assertSee('"413 Point Season"');
        $completed = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $completed->assertStatus(200);

        // After date_complete.
        $this->setTestDateTime('2020-12-31 12:34:56');
        $after_list = $this->getUnthrottled($this->endpoint);
        $after_list->assertStatus(200);
        $after_list->assertDontSee('"413 Point Season"');
        $after = $this->getUnthrottled("{$this->endpoint}/{$test_game}");
        $after->assertStatus(404);
    }

    /**
     * Testing the precise actions on the various date boundaries
     * @return void
     */
    public function testDateBoundaries(): void
    {
        $test_game = 'f1-2020-1'; // 413 point season.
        $test_entry = '/api/records/v1.0/entries/fantasy_clean';
        $this->setAPIToken('fantasy-clean', 'apitokenpassword');

        // Register the team.
        $data = ['name' => 'API Date Boundaries', 'email_status' => '0'];
        $headers = ['Content-Type' => 'application/json'];
        $this->postUnthrottled($test_entry, $data, $headers)->assertStatus(201);

        // Testing date_open - 2020-03-01 00:00:00.
        $this->setTestDateTime('2020-02-29 23:59:59');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertStatus(404);
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(404);
        // Game should now be open and allow user entry management.
        $this->setTestDateTime('2020-03-01 00:00:00');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'open');
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(201);
        $this->deleteUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(200);

        // Testing date_start - 2020-03-14 00:00:00.
        $this->setTestDateTime('2020-03-13 23:59:59');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'open');
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(201);
        $this->deleteUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(200);
        // User entry management should still be available.
        $this->setTestDateTime('2020-03-14 00:00:00');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'inprogress');
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(201);
        $this->deleteUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(200);

        // Testing date_close - 2020-04-30 00:00:00.
        $this->setTestDateTime('2020-04-29 23:59:59');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'inprogress');
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(201);
        $this->deleteUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(200);
        // User entry management should no longer be available.
        $this->setTestDateTime('2020-04-30 00:00:00');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'closed');
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(400);
        // Confirm deletion is also no longer available after this time.
        $this->setTestDateTime('2020-04-29 23:59:59');
        $this->postUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(201);
        $this->setTestDateTime('2020-04-30 00:00:00');
        $this->deleteUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(400);
        $this->setTestDateTime('2020-04-29 23:59:59');
        $this->deleteUnthrottled("$test_entry/$test_game", [], $headers)->assertStatus(200);

        // Testing date_end - 2020-11-29 00:00:00.
        $this->setTestDateTime('2020-11-28 23:59:59');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'closed');
        $this->setTestDateTime('2020-11-29 00:00:00');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'complete');

        // Testing date_complete - 2020-12-27 00:00:00.
        $this->setTestDateTime('2020-12-26 23:59:59');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertJsonPath('status', 'complete');
        $this->setTestDateTime('2020-12-27 00:00:00');
        $this->getUnthrottled("{$this->endpoint}/{$test_game}")->assertStatus(404);
    }

    /**
     * Testing the period summary information
     * @return void
     */
    public function testPeriodSummary(): void
    {
        $test_game = 'nfl-2019-3'; // Perfect season.
        $this->setAPIToken('fantasy-clean', 'apitokenpassword');
        $this->setTestDateTime('2019-09-13 12:34:56'); // During Week 2, to look back at Week 1.

        $response = $this->getUnthrottled("{$this->endpoint}/{$test_game}/periods");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('0.period_id', 1);
        $response->assertJsonPath('0.summary.entry_pos_pct', 62.12);
        $response->assertJsonPath('0.summary.sel_pos_pct', 46.88);
        $response->assertJsonPath('0.summary.popular_pos_pct', 23.12);
        $response->assertJsonMissingPath('0.summary.popular_pos_link_id');
        $response->assertJsonPath('0.summary.popular_pos.link_id', 65506);
        $response->assertJsonPath('0.summary.popular_pos.name', 'Baltimore Ravens');
        $response->assertJsonPath('0.summary.popular_neg_pct', 6.12);
        $response->assertJsonMissingPath('0.summary.popular_neg_link_id');
        $response->assertJsonPath('0.summary.popular_neg.link_id', 65516);
        $response->assertJsonPath('0.summary.popular_neg.name', 'Houston Texans');
        // Not available beyond the first period.
        $response->assertJsonPath('1.period_id', 2);
        $response->assertJsonMissingPath('1.summary');
    }
}
