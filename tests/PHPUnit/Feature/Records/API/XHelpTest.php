<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;

class XHelpTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * Test loading of help sections and articles
     * @return void
     */
    public function testArticles(): void
    {
        $response = $this->getUnthrottled('/api/records/v1.0/help');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(4);

        // The sections.
        $response->assertJsonPath('0.name', 'Rules');
        $response->assertJsonPath('1.name', 'Registration');
        $response->assertJsonPath('2.name', 'Entry Management');
        $response->assertJsonPath('3.name', 'Contact Us');

        // Confirmation of an expected article.
        $response->assertJsonPath('0.articles.0.title', 'What is the aim of DeBear Record Breakers?');
        $response->assertJsonPath('0.articles.1.title', 'How do my selections contribute towards my entry&#39;s '
            . 'score?');

        // Inactive section should not been visible.
        $response->assertDontSee('"section_id":4,"name":"Groups"');
        $response->assertDontSee('"article_id":1,"title":"What are &quot;Groups&quot;?"');
    }
}
