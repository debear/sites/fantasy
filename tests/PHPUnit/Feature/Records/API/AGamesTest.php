<?php

namespace Tests\PHPUnit\Feature\Records\API;

use Tests\PHPUnit\Base\APITestCase;
use DeBear\Helpers\Fantasy\Common\Traits\PHPUnit\APIToken as TraitAPIToken;

class AGamesTest extends APITestCase
{
    use TraitAPIToken;

    /**
     * The (base) endpoint we'll be targetting in these tests
     * @var string
     */
    protected $endpoint = '/api/records/v1.0/games';

    /**
     * Listing the current games
     * @return void
     */
    public function testCurrent(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        $response = $this->getUnthrottled($this->endpoint);
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(9);

        // The list info.
        $response->assertJsonPath('0.sport', 'nfl');
        $response->assertJsonPath('0.name', '2,105 Yard Rushing Season');
        $response->assertJsonPath('0.sel_type', 'ind');
        $response->assertJsonPath('0.config_flags.sel_required', false);
        $response->assertJsonPath('0.config_flags.sel_single_use', false);
        $response->assertJsonPath('0.date_open', '2019-08-15T08:00:00+01:00');
        $response->assertJsonPath('0.status', 'active');

        $response->assertJsonPath('1.name', '55 Passing TD Season');
        $response->assertJsonPath('1.status', 'inprogress');

        $response->assertJsonPath('2.name', 'The Perfect Season');
        $response->assertJsonPath('2.sel_type', 'team');
    }

    /**
     * Get the games of a specific status
     * @return void
     */
    public function testSpecificStatus(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // Open games.
        $response = $this->getUnthrottled("{$this->endpoint}/inprogress");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(4);

        // Active games.
        $response = $this->getUnthrottled("{$this->endpoint}/active");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(5);

        // Completed games - an empty list.
        $response = $this->getUnthrottled("{$this->endpoint}/complete");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(0);

        // Invalid status.
        $response = $this->getUnthrottled("{$this->endpoint}/invalid");
        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');
    }

    /**
     * Get the info for a single game
     * @return void
     */
    public function testSingleGame(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // Get the info.
        $game = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1");
        $game->assertStatus(200);
        $game->assertHeader('Content-Type', 'application/json');
        $game->assertJsonPath('sport', 'nfl');
        $game->assertJsonPath('season', 2019);
        $game->assertJsonPath('name', '2,105 Yard Rushing Season');
        $game->assertJsonPath('sel_type', 'ind');
        $game->assertJsonPath('config_flags.sel_required', false);
        $game->assertJsonPath('config_flags.sel_single_use', false);
        $game->assertJsonPath('date_open', '2019-08-15T08:00:00+01:00');
        $game->assertJsonPath('status', 'active');
        $game->assertJsonPath('active.name', 'Marlon Mack');
        $game->assertJsonPath('active.icon', 'IND');
        $game->assertJsonPath('active.score', 174);
        $game->assertJsonPath('overall.name', 'Testing the Limits');
        $game->assertJsonPath('overall.score', 154);
        $game->assertJsonPath('overall.user_id', 10);
        $game->assertJsonMissingPath('current.name');
        $game->assertJsonMissingPath('current.score');
        $game->assertJSONCount(1, 'news');
        $game->assertJsonPath('news.0.type', 'status');
        $game->assertJsonPath('news.0.subject', 'NFL Update');
        $game->assertJsonPath('news.0.disp_end', '2019-10-15T23:59:59+01:00');

        // Announcement not visible the next day.
        $this->setTestDateTime('2019-10-16 12:34:56');
        $news_next_day = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1");
        $news_next_day->assertStatus(200);
        $news_next_day->assertJSONCount(0, 'news');
    }

    /**
     * Get the additional info for a single game
     * @return void
     */
    public function testSingleGameAdditional(): void
    {
        // NFL rules.
        $this->setTestDateTime('2019-10-15 12:34:56');
        $rules = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/rules");
        $rules->assertStatus(200);
        $rules->assertHeader('Content-Type', 'application/json');
        $rules->assertJSONCount(4);
        $rules->assertJsonPath('0', 'One (1) selection should be made every NFL game week. All Rushing Yards gained '
            . '(or lost!) by that player will be added to your season total.');

        // MLB achievements.
        $this->setTestDateTime('2020-08-12 12:34:56');
        $achievements = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1/achievements");
        $achievements->assertStatus(200);
        $achievements->assertHeader('Content-Type', 'application/json');
        $achievements->assertJSONCount(4);
        $achievements->assertJsonPath('0.achieve_id', 1);
        $achievements->assertJsonPath('0.name', 'Hit Streak');
        $achievements->assertJsonCount(5, '0.levels');
        $achievements->assertJsonPath('0.levels.4.level', 5);
        $achievements->assertJsonPath('0.levels.4.value', 40);
        $achievements->assertJsonPath('3.achieve_id', 4);
        $achievements->assertJsonPath('3.name', 'Doubleheaders');
        $achievements->assertJsonCount(1, '3.levels');
        $achievements->assertJsonPath('3.levels.0.level', 4);
        $achievements->assertJsonMissingPath('3.levels.0.value');
    }

    /**
     * Get the period info for a single game
     * @return void
     */
    public function testSingleGamePeriods(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        $periods = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/periods");
        $periods->assertStatus(200);
        $periods->assertHeader('Content-Type', 'application/json');
        $periods->assertJSONCount(17);
        $periods->assertJsonPath('0.period_id', 1);
        $periods->assertJsonPath('0.period_order', 1);
        $periods->assertJsonPath('0.name', 'Week 1');
        $periods->assertJsonPath('0.start_date', '2019-09-04');
        $periods->assertJsonPath('0.end_date', '2019-09-10');
        $periods->assertJsonPath('0.is_current', false);
        $periods->assertJsonMissingPath('0.icon');
        $periods->assertJsonPath('5.period_id', 6);
        $periods->assertJsonPath('5.is_current', true);

        $periods_curr = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/periods/current");
        $periods_curr->assertStatus(200);
        $periods_curr->assertHeader('Content-Type', 'application/json');
        $periods_curr->assertJsonPath('period_id', 6);
        $periods_curr->assertJsonPath('is_current', true);

        $periods_nhl = $this->getUnthrottled("{$this->endpoint}/nhl-2019-1/periods");
        $periods_nhl->assertStatus(200);
        $periods_nhl->assertHeader('Content-Type', 'application/json');
        $periods_nhl->assertJSONCount(179);
        $periods_nhl->assertJsonPath('0.start_date', '2019-10-02');
        $periods_nhl->assertJsonPath('0.end_date', '2019-10-02');
        $periods_nhl->assertJsonMissingPath('0.name');
        $periods_nhl->assertJsonMissingPath('0.icon');
    }

    /**
     * Get the leaderboards for a single game
     * @return void
     */
    public function testSingleGameLeaderboards(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        $leaders_all = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/leaderboard");
        $leaders_all->assertStatus(200);
        $leaders_all->assertHeader('Content-Type', 'application/json');
        $leaders_all->assertJSONCount(1);
        $leaders_all->assertJsonPath('overall.0.user_id', 10);
        $leaders_all->assertJsonPath('overall.0.entry_name', 'Testing the Limits');
        $leaders_all->assertJsonPath('overall.0.pos.num', 1);
        $leaders_all->assertJsonPath('overall.0.pos.chg', null);
        $leaders_all->assertJsonPath('overall.0.result', 154);
        $leaders_all->assertJsonMissingPath('overall.0.current');
        $leaders_all->assertJsonMissingPath('overall.0.current_pos');
        $leaders_all->assertJsonMissingPath('hitrate');

        $leaders_ov = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/leaderboard/overall");
        $leaders_ov->assertStatus(200);
        $leaders_ov->assertHeader('Content-Type', 'application/json');
        $leaders_ov->assertJSONCount(5);
        $leaders_ov->assertJsonPath('0.user_id', 10);
        $leaders_ov->assertJsonPath('0.entry_name', 'Testing the Limits');
        $leaders_ov->assertJsonPath('0.pos.num', 1);
        $leaders_ov->assertJsonPath('0.pos.chg', null);
        $leaders_ov->assertJsonPath('0.result', 154);
        $leaders_ov->assertJsonMissingPath('0.current');
        $leaders_ov->assertJsonMissingPath('0.current_pos');

        // No Hitrate Leaderboard is available.
        $leaders_hr = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/leaderboard/hitrate");
        $leaders_hr->assertStatus(400);

        // Various combinations of sub-leaderboards.
        $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/leaderboard/overall")->assertStatus(200);
        $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/leaderboard/current")->assertStatus(400);
        $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/leaderboard/hitrate")->assertStatus(400);
        $this->getUnthrottled("{$this->endpoint}/nfl-2019-3/leaderboard/overall")->assertStatus(200);
        $this->getUnthrottled("{$this->endpoint}/nfl-2019-3/leaderboard/current")->assertStatus(400);
        $this->getUnthrottled("{$this->endpoint}/nfl-2019-3/leaderboard/hitrate")->assertStatus(400);

        // MLB streak game will have a current leaderboard.
        $this->setTestDateTime('2020-08-12 12:34:56');
        $leaders_curr = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1/leaderboard/current");
        $leaders_curr->assertStatus(200);
        $leaders_curr->assertHeader('Content-Type', 'application/json');
        $leaders_curr->assertJSONCount(4);
        $leaders_curr->assertJsonPath('0.user_id', 55);
        $leaders_curr->assertJsonPath('0.pos.num', 1);
        $leaders_curr->assertJsonPath('0.pos.chg', 8);
        $leaders_curr->assertJsonPath('0.result', 11);
        $leaders_curr->assertJsonMissingPath('0.current');
        $leaders_curr->assertJsonMissingPath('0.current_pos');
    }

    /**
     * Edge-cases (generally, errors!) for the info for a single game
     * @return void
     */
    public function testSingleGameEdgeCases(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // Unknown game.
        $unknown = $this->getUnthrottled("{$this->endpoint}/nfl-2019-4");
        $unknown->assertStatus(404);
        $unknown_rules = $this->getUnthrottled("{$this->endpoint}/nfl-2019-4/rules");
        $unknown_rules->assertStatus(404);
        $unknown_achieve = $this->getUnthrottled("{$this->endpoint}/nfl-2019-4/achievements");
        $unknown_achieve->assertStatus(404);
        $unknown_periods = $this->getUnthrottled("{$this->endpoint}/nfl-2019-4/periods");
        $unknown_periods->assertStatus(404);
        $unknown_leaders = $this->getUnthrottled("{$this->endpoint}/nfl-2019-4/leaderboard");
        $unknown_leaders->assertStatus(404);

        // Unavailable game.
        $unavailable = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1");
        $unavailable->assertStatus(404);
        $unavailable_rules = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1/rules");
        $unavailable_rules->assertStatus(404);
        $unavailable_achieve = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1/achievements");
        $unavailable_achieve->assertStatus(404);
        $unavailable_periods = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1/periods");
        $unavailable_periods->assertStatus(404);
        $unavailable_leaders = $this->getUnthrottled("{$this->endpoint}/mlb-2020-1/leaderboard");
        $unavailable_leaders->assertStatus(404);

        // Game is not active.
        $this->setTestDateTime('2019-09-03 12:34:56');
        $periods_curr_none = $this->getUnthrottled("{$this->endpoint}/nfl-2019-1/periods/current");
        $periods_curr_none->assertStatus(404);
    }
}
