<?php

namespace Tests\PHPUnit\Feature\Records;

use Tests\PHPUnit\Base\FeatureTestCase;

class AHomeTest extends FeatureTestCase
{
    /**
     * Listing the available games to join (potentially amongst others)
     * @return void
     */
    public function testAvailable(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        $response = $this->get('/records');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers</title>');
        $response->assertSee('<h1>Welcome to Record Breakers!</h1>');
        $response->assertSee('<h3 class="group">In Progress Games</h3>
            <p class="group">The season may have started, but it&#39;s not too late to get involved!</p>');

        // An NFL game.
        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <fieldset class="grass_box game" data-ref="nfl-2019-1">
                    <h3 class="invert sports_nfl_icon">2,105 Yard Rushing Season</h3>
                    <p class="descrip">Keep pounding the football, all the way past this record that&#39;s stood for '
            . 'nearly 40 years. Find the plus matchup, but remember&hellip; receiving yards don&#39;t count!</p>');
        $response->assertSee('<h3 class="progress invert">Player Progress</h3>
    <ul class="progress nfl-2019-1 inline_list clearfix">
                                <li class="bar target">
                <div class="progress"></div>
                <div class="container"></div>
                <span class="name">Target</span>
                <span class="value">2,106</span>
            </li>
                                <li class="bar active">
                <div class="progress"></div>
                <div class="container"></div>
                <span class="name">NFL Leader</span>
                <span class="value">174</span>
            </li>
                                <li class="bar overall">
                <div class="progress"></div>
                <div class="container"></div>
                <span class="name">Overall Leader</span>
                <span class="value">154</span>
            </li>
            </ul>');
        $response->assertSee('.progress.nfl-2019-1 li.bar.overall div.progress { width: 7.312440645774%; }');
        // An AHL game.
        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <fieldset class="ice_box game" data-ref="ahl-2019-1">
                    <h3 class="invert sports_ahl_icon">50 in 50</h3>
                    <p class="descrip">Although there has yet to be an instance of 50 goals in 50 games in the AHL, '
            . 'that doesn&#39;t mean it cannot be achieved &ndash; there are still enough consistent goal scorers to '
            . 'choose from!</p>');
        $response->assertDontSee('<li class="label invert">Current Record Holder</li>
            <li ><em>No-one yet, become the first!</em></li>
    </ul>


                </fieldset>
            </li>');
        // An NHL game.
        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <fieldset class="ice_box game" data-ref="nhl-2019-1">
                    <h3 class="invert sports_nhl_icon">50 in 50</h3>
                    <p class="descrip">The gold-standard in NHL goal scoring is the 50 goals in 50 games. First '
            . 'achieved in 1944-45 it saw a boon during the high scoring 1980s, but no individual player has come '
            . 'close since the mid 1990s!</p>');
        $response->assertDontSee('<li class="label invert">Current Record Holders</li>
            <li class="multi">Maurice Richard, 1944-45 (50 games)</li>
            <li class="multi">Mike Bossy, 1980-81 (50 games)</li>
            <li class="multi">Wayne Gretzky, 1981-82 (39 games), 1983-84 (42 games), 1984-85 (49 games)</li>
            <li class="multi">Mario Lemieux, 1988-89 (46 games)</li>
            <li class="multi">Brett Hull, 1990-91 (49 games), 1991-92 (50 games)</li>
    </ul>


                </fieldset>
            </li>');
    }

    /**
     * Various actions available to the user
     * @return void
     */
    public function testUserActions(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2020-10-15 12:34:56');

        // Logged out.
        $response = $this->get('/records');
        $response->assertStatus(200);
        $response->assertSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');

        // Logged in user.
        $this->post('/login', [
            'username' => 'fantasy_inactive',
            'password' => 'testlogindetails',
        ])->assertStatus(200);
        $response = $this->get('/records');
        $response->assertStatus(200);
        $response->assertDontSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');
        $response->assertSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/records/my-entry">click here</a> to
    confirm your details, or <em>Join</em> on any of the games below and get picking!
</fieldset>');
        $this->post('/logout');
    }

    /**
     * Testing the global announcements
     * @return void
     */
    public function testAnnouncements(): void
    {
        $expected = '<fieldset class="news success">
            <h3 class="invert">Hello!</h3>
        Welcome to DeBear Fantasy Record Breakers
</fieldset>';

        // Before.
        $this->setTestDateTime('2019-10-14 23:59:59');
        $before = $this->get('/records');
        $before->assertStatus(200);
        $before->assertDontSee($expected);
        // During - Start.
        $this->setTestDateTime('2019-10-15 00:00:00');
        $during_min = $this->get('/records');
        $during_min->assertStatus(200);
        $during_min->assertSee($expected);
        // During - End.
        $this->setTestDateTime('2019-10-31 23:59:59');
        $during_max = $this->get('/records');
        $during_max->assertStatus(200);
        $during_max->assertSee($expected);
        // After.
        $this->setTestDateTime('2019-11-01 00:00:00');
        $after = $this->get('/records');
        $after->assertStatus(200);
        $after->assertDontSee($expected);
    }
}
