<?php

namespace Tests\PHPUnit\Feature\Records;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\ORM\Fantasy\Records\EntryScore;
use DeBear\ORM\Sports\MajorLeague\MLB\GameLineup as MLBGameLineup;
use DeBear\Helpers\Fantasy\Namespaces;
use Illuminate\Support\Facades\DB;

class DEntrySelTest extends FeatureTestCase
{
    /**
     * The base endpoint for the game being tested that the user has joined
     * @var string
     */
    protected string $endpoint_joined = '/records/nhl/50-in-50-2019/periods';
    /**
     * The base endpoint for the game being tested that the user has not joined
     * @var string
     */
    protected string $endpoint_not_joined = '/records/nhl/215-point-season-2019/periods';
    /**
     * The base endpoint for the team-based game
     * @var string
     */
    protected string $endpoint_team = '/records/nfl/the-perfect-season-2019/periods';
    /**
     * The base endpoint for a Major League based game
     * @var string
     */
    protected string $endpoint_majors = '/records/mlb/56-game-hitting-streak-2020/periods';
    /**
     * Login details of the user being used to complete the tests
     * @var array
     */
    protected array $login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Test loading of individual selections
     * @return void
     */
    public function testSelReadInd(): void
    {
        $this->setTestDateTime('2020-01-17 12:34:56');
        $this->post('/login', $this->login)->assertStatus(200);

        // Unknown game.
        $unknown_game = $this->get(str_replace('-2019/', '-2018/', $this->endpoint_joined) . '/105');
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'Game not found');
        // Unknown period.
        $unknown_period = $this->get("{$this->endpoint_joined}/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->get("{$this->endpoint_not_joined}/105");
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // Invalid page argument.
        $invalid_page = $this->get("{$this->endpoint_joined}/105?page=0");
        $invalid_page->assertStatus(400);
        $invalid_page->assertHeader('Content-Type', 'application/json');
        $invalid_page->assertJsonPath('msg', 'Bad Request');
        // Valid and unprocessed period, with bad pagination.
        $invalid_page_alt = $this->get("{$this->endpoint_joined}/105?page=99");
        $invalid_page_alt->assertStatus(400);
        $invalid_page_alt->assertHeader('Content-Type', 'application/json');
        $invalid_page_alt->assertJsonPath('msg', 'Bad Request');
        // Valid, but unprocessed, period.
        $unprocessed_period = $this->get("{$this->endpoint_joined}/106");
        $unprocessed_period->assertStatus(422);
        $unprocessed_period->assertHeader('Content-Type', 'application/json');
        $unprocessed_period->assertJsonPath('msg', 'Requested period not yet available');
        // Valid, but unavailable, period.
        $unavailable_period = $this->get("{$this->endpoint_team}/12");
        $unavailable_period->assertStatus(422);
        $unavailable_period->assertHeader('Content-Type', 'application/json');
        $unavailable_period->assertJsonPath('msg', 'Period is not available to this entry');
        // Valid request.
        $valid = $this->get("{$this->endpoint_joined}/105");
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonCount(10, 'sel');
        $valid->assertJsonPath('sel.0.link_id', 2361);
        $valid->assertJsonPath('sel.0.weather', null);
        $valid->assertJsonPath('sel.0.lock_time.status', null);
        $valid->assertJsonPath('sel.0.lock_time.raw', '2020-01-18T00:25:00+00:00');
        $valid->assertJsonPath('sel.0.lock_time.disp', '00:25');
        $valid->assertJsonPath('sel.0.rating', 4.5);
        $valid->assertJsonPath('sel.0.opp_rating', 3.5);
        $valid->assertJsonMissingPath('sel.0.flags.buster');
        $valid->assertJsonMissingPath('sel.0.flags.achieve_opp');
        $valid->assertJsonPath('sel.0.score', null); // Unset for future dated selection.
        $valid->assertJsonPath('sel.0.stress', null); // Unset for future dated selection.
        $valid->assertJsonPath('sel.0.stats.1', '19');
        $valid->assertJsonPath('sel.9.link_id', 2578);
        $valid->assertJsonPath('sel.9.weather', null);
        $valid->assertJsonPath('sel.9.lock_time.status', null);
        $valid->assertJsonPath('sel.9.lock_time.raw', '2020-01-18T00:55:00+00:00');
        $valid->assertJsonPath('sel.9.lock_time.disp', '00:55');
        $valid->assertJsonPath('sel.9.rating', 3.5);
        $valid->assertJsonPath('sel.9.opp_rating', 3);
        $valid->assertJsonMissingPath('sel.9.flags.buster');
        $valid->assertJsonMissingPath('sel.9.flags.achieve_opp');
        $valid->assertJsonPath('sel.9.stats.2', '0.41');
        $valid->assertJsonPath('rows.from', 1);
        $valid->assertJsonPath('rows.to', 10);
        $valid->assertJsonPath('rows.total', 132);
        $valid->assertJsonPath('pages.curr', 1);
        $valid->assertJsonPath('pages.total', 14);
        $valid->assertJsonPath('pages.prev', false);
        $valid->assertJsonPath('pages.next', true);
    }

    /**
     * Test loading of individual selections with filters
     * @return void
     */
    public function testSelReadIndFilters(): void
    {
        $this->setTestDateTime('2020-01-18 00:34:12');
        $this->post('/login', $this->login)->assertStatus(200);
        $endpoint = "{$this->endpoint_joined}/105";

        // Locked - Invalid.
        $locked_invalid = $this->get("$endpoint?locked=invalid");
        $locked_invalid->assertStatus(400);

        // Locked - Valid.
        $locked_false = $this->get("$endpoint?locked=false");
        $locked_false->assertStatus(200);
        $locked_false->assertJsonPath('rows.total', 44);

        $locked_true = $this->get("$endpoint?locked=true");
        $locked_true->assertStatus(200);
        $locked_true->assertJsonPath('rows.total', 132);

        // Team - Invalid.
        $team_invalid = $this->get("$endpoint?team_id=invalid");
        $team_invalid->assertStatus(400);

        // Team - Valid.
        $team = $this->get("$endpoint?team_id=TB");
        $team->assertStatus(200);
        $team->assertJsonPath('rows.total', 22);

        // Sort - Invalid.
        $sort_invalid = $this->get("$endpoint?sort=invalid");
        $sort_invalid->assertStatus(400);

        // Sort - Valid.
        $sort_rating = $this->get("$endpoint?sort=rating");
        $sort_rating->assertStatus(200);
        $sort_rating->assertJsonPath('sel.0.link_id', 2220);

        $sort_opprating = $this->get("$endpoint?sort=opp_rating");
        $sort_opprating->assertStatus(200);
        $sort_opprating->assertJsonPath('sel.0.link_id', 2220);

        $sort_stat1 = $this->get("$endpoint?sort=1");
        $sort_stat1->assertStatus(200);
        $sort_stat1->assertJsonPath('sel.0.link_id', 2626);

        // Achievement: Beaten Opponents - Invalid.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $achieve_opp_invalid = $this->get("{$this->endpoint_majors}/3?beaten_opp=invalid");
        $achieve_opp_invalid->assertStatus(400);

        // Achievement: Beaten Opponents - Valid.
        $achieve_opp_valid = $this->get("{$this->endpoint_majors}/3?beaten_opp=false");
        $achieve_opp_valid->assertStatus(200);
        $achieve_opp_valid->assertJsonCount(0, 'sel');
        $achieve_opp_raw = $this->get("{$this->endpoint_majors}/3?beaten_opp=true");
        $achieve_opp_raw->assertStatus(200);
        $achieve_opp_raw->assertJsonCount(1, 'sel');
        $achieve_opp_raw->assertJsonPath('sel.0.flags.achieve_opp', true);
    }

    /**
     * Test loading of team selections
     * @return void
     */
    public function testSelReadTeam(): void
    {
        $this->setTestDateTime('2019-09-07 12:34:56'); // Week 1, after the Thursday night game.
        $this->post('/login', $this->login)->assertStatus(200);

        // The core request.
        $core = $this->get("{$this->endpoint_team}/1");
        $core->assertStatus(200);
        $core->assertHeader('Content-Type', 'application/json');
        $core->assertJsonCount(10, 'sel');
        $core->assertJsonPath('rows.total', 32);
        $core->assertJsonPath('sel.0.link_id', 65506); // Baltimore is the top ranked selection.
        $core->assertJsonPath('sel.1.link_id', 65509); // Chicago played the Thursday night game.
        $core->assertJsonPath('sel.1.weather.symbol', 'cloudy');
        $core->assertJsonPath('sel.1.weather.temp', '88&deg;');
        $core->assertJsonPath('sel.1.weather.wind', '1mph SE');
        $core->assertJsonPath('sel.1.lock_time.raw', '2019-09-06T01:15:00+01:00');
        $core->assertJsonPath('sel.1.rating', 4);
        $core->assertJsonPath('sel.1.opp_rating', 3);
        $core->assertJsonMissingPath('sel.1.flags.buster'); // No streak busters.
        $core->assertJsonMissingPath('sel.1.flags.achieve_opp'); // No achieved opponents.

        // Try and exclude locked selections (which should fail to apply).
        $locked = $this->get("{$this->endpoint_team}/1?locked=false");
        $locked->assertStatus(200);
        $locked->assertHeader('Content-Type', 'application/json');
        $locked->assertJsonCount(10, 'sel');
        $locked->assertJsonPath('rows.total', 32);
        $locked->assertJsonPath('sel.1.link_id', 65509); // Chicago played the Thursday night game but still listed.
        $locked->assertJsonPath('sel.1.lock_time.raw', '2019-09-06T01:15:00+01:00');
        $locked->assertJsonPath('sel.1.rating', 4);
        $locked->assertJsonPath('sel.1.opp_rating', 3);
        $locked->assertJsonMissingPath('sel.1.flags.buster'); // No streak busters.
        $locked->assertJsonMissingPath('sel.1.flags.achieve_opp'); // No achieved opponents.

        // Try and filter by specific team_id (which should fail to apply).
        $by_team = $this->get("{$this->endpoint_team}/1?team_id=BAL");
        $by_team->assertStatus(200);
        $by_team->assertHeader('Content-Type', 'application/json');
        $by_team->assertJsonCount(10, 'sel');
        $by_team->assertJsonPath('rows.total', 32);
        $by_team->assertJsonPath('sel.1.link_id', 65509); // Chicago still listed.

        // Sort.
        $sort = $this->get("{$this->endpoint_team}/1?sort=opp_rating");
        $sort->assertStatus(200);
        $sort->assertHeader('Content-Type', 'application/json');
        $sort->assertJsonCount(10, 'sel');
        $sort->assertJsonPath('rows.total', 32);
        $sort->assertJsonPath('sel.0.link_id', 65514); // Detroit is now the top ranked team.
    }

    /**
     * Test storing a selection
     * @return void
     */
    public function testSelCreate(): void
    {
        $this->setTestDateTime('2020-01-18 00:45:12');
        $this->post('/login', $this->login)->assertStatus(200);
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];

        // Unknown game.
        $unknown_game = $this->post(str_replace('-2019/', '-2018/', $this->endpoint_joined) . '/105');
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'Game not found');
        // Unknown period.
        $unknown_period = $this->post("{$this->endpoint_joined}/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->post("{$this->endpoint_not_joined}/105");
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // No selection passed.
        $no_selection = $this->post("{$this->endpoint_joined}/105");
        $no_selection->assertStatus(400);
        $no_selection->assertHeader('Content-Type', 'application/json');
        $no_selection->assertJsonPath('msg', 'Bad Request');
        // Invalid selection passed.
        $bad_selection = $this->post("{$this->endpoint_joined}/105", ['link_id' => 'steven-stamkos'], $headers);
        $bad_selection->assertStatus(400);
        $bad_selection->assertHeader('Content-Type', 'application/json');
        $bad_selection->assertJsonPath('msg', 'Bad Request');
        $invalid_selection = $this->post("{$this->endpoint_joined}/105", ['link_id' => 560], $headers);
        $invalid_selection->assertStatus(400);
        $invalid_selection->assertHeader('Content-Type', 'application/json');
        $invalid_selection->assertJsonPath('msg', 'Bad Request');
        // Locked selection passed.
        $locked_selection = $this->post("{$this->endpoint_joined}/105", ['link_id' => 459], $headers);
        $locked_selection->assertStatus(423);
        $locked_selection->assertHeader('Content-Type', 'application/json');
        $locked_selection->assertJsonPath('msg', 'Requested selection locked');
        // Selection already exists.
        $duplicate = $this->post("{$this->endpoint_joined}/105", ['link_id' => 2220], $headers);
        $duplicate->assertStatus(409);
        $duplicate->assertHeader('Content-Type', 'application/json');
        $duplicate->assertJsonPath('msg', 'Existing selection already saved');
        // Valid request (having first removed what was there).
        $this->delete("{$this->endpoint_joined}/105", [], $headers)->assertStatus(200);
        $valid = $this->post("{$this->endpoint_joined}/105", ['link_id' => 2220], $headers);
        $valid->assertStatus(201);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('msg', 'Selection saved');
    }

    /**
     * Test updating a selection
     * @return void
     */
    public function testSelUpdate(): void
    {
        $this->setTestDateTime('2020-01-18 00:45:56');
        $this->post('/login', $this->login)->assertStatus(200);
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];

        // Unknown game.
        $unknown_game = $this->patch(str_replace('-2019/', '-2018/', $this->endpoint_joined) . '/105');
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'Game not found');
        // Unknown period.
        $unknown_period = $this->patch("{$this->endpoint_joined}/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->patch("{$this->endpoint_not_joined}/105");
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // No selection passed.
        $no_selection = $this->patch("{$this->endpoint_joined}/105");
        $no_selection->assertStatus(400);
        $no_selection->assertHeader('Content-Type', 'application/json');
        $no_selection->assertJsonPath('msg', 'Bad Request');
        // Invalid selection passed.
        $bad_selection = $this->patch("{$this->endpoint_joined}/105", ['link_id' => 'steven-stamkos']);
        $bad_selection->assertStatus(400);
        $bad_selection->assertHeader('Content-Type', 'application/json');
        $bad_selection->assertJsonPath('msg', 'Bad Request');
        $invalid_selection = $this->patch("{$this->endpoint_joined}/105", ['link_id' => 560]);
        $invalid_selection->assertStatus(400);
        $invalid_selection->assertHeader('Content-Type', 'application/json');
        $invalid_selection->assertJsonPath('msg', 'Bad Request');
        // Locked selection passed.
        $locked_selection = $this->patch("{$this->endpoint_joined}/105", ['link_id' => 459]);
        $locked_selection->assertStatus(423);
        $locked_selection->assertHeader('Content-Type', 'application/json');
        $locked_selection->assertJsonPath('msg', 'Requested selection locked');
        // Valid request.
        $valid = $this->patch("{$this->endpoint_joined}/105", ['link_id' => 729]);
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('msg', 'Selection updated');
        // Does not have an existing selection.
        $this->delete("{$this->endpoint_joined}/105", [], $headers)->assertStatus(200);
        $no_existing = $this->patch("{$this->endpoint_joined}/105", ['link_id' => 729]);
        $no_existing->assertStatus(409);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Existing selection not found');
        // Existing selection is locked.
        $this->post("{$this->endpoint_joined}/105", ['link_id' => 2220], $headers)->assertStatus(201);
        $this->setTestDateTime('2020-01-18 03:45:56');
        $no_existing = $this->patch("{$this->endpoint_joined}/105", ['link_id' => 729]);
        $no_existing->assertStatus(423);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Requested selection locked');
    }

    /**
     * Test removing a selection
     * @return void
     */
    public function testSelDelete(): void
    {
        $this->setTestDateTime('2020-01-17 12:34:56');
        $this->post('/login', $this->login)->assertStatus(200);
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];

        // Unknown game.
        $unknown_game = $this->delete(str_replace('-2019/', '-2018/', $this->endpoint_joined) . '/105', [], $headers);
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'Game not found');
        // Unknown period.
        $unknown_period = $this->delete("{$this->endpoint_joined}/999", [], $headers);
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');
        // Not joined game.
        $not_joined_game = $this->delete("{$this->endpoint_not_joined}/105", [], $headers);
        $not_joined_game->assertStatus(404);
        $not_joined_game->assertHeader('Content-Type', 'application/json');
        $not_joined_game->assertJsonPath('msg', 'Entry not found');
        // Valid request.
        $valid = $this->delete("{$this->endpoint_joined}/105", [], $headers);
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('msg', 'Selection removed');
        // Does not have an existing selection.
        $no_existing = $this->delete("{$this->endpoint_joined}/105", [], $headers);
        $no_existing->assertStatus(409);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Existing selection not found');
        // Existing selection is locked.
        $this->post("{$this->endpoint_joined}/105", ['link_id' => 2220], $headers)->assertStatus(201);
        $this->setTestDateTime('2020-01-18 03:45:56');
        $no_existing = $this->delete("{$this->endpoint_joined}/105", [], $headers);
        $no_existing->assertStatus(423);
        $no_existing->assertHeader('Content-Type', 'application/json');
        $no_existing->assertJsonPath('msg', 'Current selection locked');
    }

    /**
     * Test processing selections for a game with the "non reuse" flag
     * @return void
     */
    public function testSelReuse(): void
    {
        // For this test, we do need to tweak the result from the default.
        $score = EntryScore::query()
            ->where([
                ['sport', '=', 'nfl'],
                ['season', '=', 2019],
                ['game_id', '=', 3],
                ['user_id', '=', 60],
            ])->get();
        $pos_orig = $score->overall_pos;
        $score->overall_pos = null;
        $score->save();

        // Subsequent selection availability.
        $this->setTestDateTime('2019-09-12 12:34:56'); // Week 2, before the Thursday night game.
        $this->post('/login', $this->login)->assertStatus(200);

        $availability = $this->get("{$this->endpoint_team}/2?page=3");
        $availability->assertStatus(200);
        $availability->assertHeader('Content-Type', 'application/json');
        $availability->assertJsonCount(10, 'sel');
        $availability->assertJsonPath('sel.4.link_id', 65533); // Tampa Bay, a previous selection.
        $availability->assertJsonPath('sel.4.flags.available', false); // Not available.
        $availability->assertJsonMissingPath('sel.4.flags.buster'); // No streak busters.
        $availability->assertJsonMissingPath('sel.4.flags.achieve_opp'); // No achieved opponents.
        $availability->assertJsonPath('sel.3.link_id', 65527); // An unused selction.
        $availability->assertJsonPath('sel.3.flags.available', true); // Is available.
        $availability->assertJsonMissingPath('sel.3.flags.buster'); // No streak busters.
        $availability->assertJsonMissingPath('sel.3.flags.achieve_opp'); // No achieved opponents.
        $availability->assertJsonPath('sel.6.link_id', 65514); // An unused selction.
        $availability->assertJsonMissingPath('sel.6.flags.buster'); // No streak busters.
        $availability->assertJsonMissingPath('sel.6.flags.achieve_opp'); // No achieved opponents.

        // Attempt to use this selection.
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];
        $use = $this->post("{$this->endpoint_team}/2", ['link_id' => 65533], $headers);
        $use->assertStatus(409);
        $use->assertHeader('Content-Type', 'application/json');
        $use->assertJsonPath('msg', 'Selection cannot be re-used');

        // Restore the entry score.
        $score->overall_pos = $pos_orig;
        $score->save();
    }

    /**
     * Test processing selections whose event is postponed
     * @return void
     */
    public function testSelPostponed(): void
    {
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json'];
        $endpoint = "{$this->endpoint_joined}/105";
        $time_before = '2020-01-17 12:34:56';
        $time_locked = '2020-01-18 00:42:56';
        $sel_ppd = 2361; // PIT (@ DET, 19:30 start time, game 740).
        $sel_alt = 2220; // TB (@ WGP, 20:00 start time, game 742).

        // Set up the 'before lock time' test.
        $this->setTestDateTime($time_before);
        $this->post('/login', $this->login)->assertStatus(200);

        // As this test will need a little data manipulation, prepare our database event updates.
        $query_ppd = 'UPDATE ' . env('DB_SPORTS_DATABASE') . '.SPORTS_NHL_SCHEDULE SET status = "PPD" '
            . 'WHERE season = 2019 AND game_type = "regular" AND game_id = 740;';
        $query_sched = str_replace('"PPD"', '"F"', $query_ppd);

        /* Test 1: Existing selection can be replaced, but not re-added. */
        $this->assertTrue(DB::unprepared($query_sched));
        $this->delete($endpoint, [], $headers);
        // Add the (faux-emabled) selection.
        $this->post($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed and confirm it is now listed appropriately.
        $this->assertTrue(DB::unprepared($query_ppd));
        $sel_list = $this->get($endpoint);
        $sel_list->assertStatus(200);
        $sel_list->assertJsonPath('sel.0.link_id', 2361);
        $sel_list->assertJsonPath('sel.0.lock_time.status', 'PPD');
        $sel_list->assertJsonPath('sel.0.lock_time.raw', null);
        $sel_list->assertJsonPath('sel.0.lock_time.disp', 'Postponed');
        // Selection can be replaced.
        $this->patch($endpoint, ['link_id' => $sel_alt], $headers)->assertStatus(200);
        // Selection cannot now be re-added.
        $this->patch($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(410);

        /* Test 2: Existing selection can be removed, but not re-added. */
        $this->assertTrue(DB::unprepared($query_sched));
        $this->delete($endpoint, [], $headers);
        // Add the (faux-emabled) selection.
        $this->post($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed.
        $this->assertTrue(DB::unprepared($query_ppd));
        // Selection can be removed but not re-added.
        $this->delete($endpoint, [], $headers)->assertStatus(200);
        $this->post($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(410);


        /* Test 3: Existing selection can be replaced after the postponed selection's event lock time. */
        $this->setTestDateTime($time_before);
        $this->assertTrue(DB::unprepared($query_sched));
        $this->delete($endpoint, [], $headers);
        // Add the (faux-emabled) selection.
        $this->post($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed.
        $this->assertTrue(DB::unprepared($query_ppd));
        $this->setTestDateTime($time_locked);
        // Selection can be replaced with another (open) selection after the original's lock time.
        $this->patch($endpoint, ['link_id' => $sel_alt], $headers)->assertStatus(200);

        /* Test 4: Existing selection can be removed after the postponed selection's event lock time. */
        $this->setTestDateTime($time_before);
        $this->assertTrue(DB::unprepared($query_sched));
        $this->delete($endpoint, [], $headers);
        // Add the (faux-emabled) selection.
        $this->post($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(201);
        // Revert the event to Postponed.
        $this->assertTrue(DB::unprepared($query_ppd));
        $this->setTestDateTime($time_locked);
        // Selection can be removed after the lock time.
        $this->delete($endpoint, [], $headers)->assertStatus(200);
        // Attempting to re-add after the lock time still gives a 'Gone' status, rather than 'Locked'.
        $this->post($endpoint, ['link_id' => $sel_ppd], $headers)->assertStatus(410);
    }

    /**
     * Test processing the combination of selection statuses within the selection list
     * @return void
     */
    public function testSelMajorLeagueStatus(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $this->post('/login', $this->login)->assertStatus(200);

        // Data getters.
        $lineup = MLBGameLineup::query()
            ->where([
                ['season', '=', 2020],
                ['game_type', '=', 'regular'],
                ['game_id', '=', 248],
                ['player_id', '=', 2388],
            ])->get();
        $schedule = Namespaces::callStatic('mlb', 'Schedule', 'query')
            ->where([
                ['season', '=', 2020],
                ['game_type', '=', 'regular'],
                ['game_id', '=', 248],
            ])->get(['info-query' => true]);

        // Position: Starter.
        $pos_starting = $this->get("{$this->endpoint_majors}/3");
        $pos_starting->assertStatus(200);
        $pos_starting->assertJsonPath('sel.0.link.status', '<abbrev class="status status-pos-start" '
            . 'title="Starting 4th">4th</abbrev>');

        // Position: Non-Starter.
        $lineup->update(['player_id' => 2389])->save();
        $pos_bench = $this->get("{$this->endpoint_majors}/3");
        $pos_bench->assertStatus(200);
        $pos_bench->assertJsonPath('sel.0.link.status', '<abbrev class="status status-pos-dns" '
            . 'title="Not in Starting Lineup">Bench</abbrev>');
        $lineup->update(['player_id' => 2388])->save();

        // Schedule status (requires a database tweak).
        $schedule->update(['status' => 'PPD'])->save();
        $sched_ppd = $this->get("{$this->endpoint_majors}/3");
        $sched_ppd->assertStatus(200);
        $sched_ppd->assertJsonPath('sel.0.link.status', '<abbrev class="status status-sched" '
            . 'title="Postponed">PPD</abbrev>');
        $schedule->update(['status' => null])->save();
    }

    /**
     * Test processing the next locktime header we send with the selection list
     * @return void
     */
    public function testSelNextLocktime(): void
    {
        $tests = [
            // NHL games have a 30 minute non-caching window prior to the locktime.
            "{$this->endpoint_joined}/104" => [
                '2020-01-16 12:34:56' => 39004, // Early in day.
                // Games at 00:00 lock at 23:55 cachable until 23:25.
                '2020-01-16 23:24:59' => 1, // Just before the cache window expires.
                '2020-01-16 23:25:00' => 0, // At cache window expiry, so not cachable.
                '2020-01-16 23:25:01' => 0, // Just after cache window expiry, so not cachable.
                '2020-01-16 23:54:59' => 0, // Just before a locktime, within uncachable period.
                '2020-01-16 23:55:00' => 0, // At a locktime, still uncachable.
                '2020-01-16 23:55:01' => 0, // After the locktime, but prevented from caching by another locktime.
                // Games at 02:00 lock at 01:55 cachable until 01:25.
                '2020-01-17 01:24:59' => 0, // Just before this locktime's cache window expires, but in another window.
                '2020-01-17 01:25:00' => 0, // At cache window expiry, so not cachable.
                '2020-01-17 01:25:01' => 0, // Just after cache window expiry, so not cachable.
                '2020-01-17 01:54:59' => 0, // Just before a locktime, within uncachable period.
                '2020-01-17 01:55:00' => 0, // At a locktime, still uncachable.
                '2020-01-17 01:55:01' => 1799, // After the locktime, but cacheable until 02:25.
                // Games at 03:00 lock at 02:55 cachable until 02:25.
                '2020-01-17 02:24:59' => 1, // Just before the cache window expires.
                '2020-01-17 02:25:00' => 0, // At cache window expiry, so not cachable.
                '2020-01-17 02:25:01' => 0, // Just after cache window expiry, so not cachable.
            ],
            // NFL games have no non-caching window prior to the locktime.
            "{$this->endpoint_team}/1" => [
                // Game at 01:20 locks at 01:15, cachable until that time.
                '2019-09-06 01:14:59' => 1, // Just before locktime (and within theoretical cache window).
                '2019-09-06 01:15:00' => 0, // At locktime.
                '2019-09-06 01:15:01' => 232799, // Just after locktime.
                // Game at 18:00 locks at 17:55, cachable until that time.
                '2019-09-08 17:54:59' => 1, // Just before locktime (and within theoretical cache window).
                '2019-09-08 17:55:00' => 0, // At locktime.
                '2019-09-08 17:55:01' => 11099, // Just after locktime.
            ],
        ];
        $this->post('/login', $this->login)->assertStatus(200);
        foreach ($tests as $endpoint => $list) {
            foreach ($list as $dt => $exp) {
                $this->setTestDateTime($dt);
                // Perform the request.
                $response = $this->get($endpoint);
                $response->assertStatus(200);
                if ($exp) {
                    $response->assertHeader('Cache-Control', "max-age=$exp, private");
                    $response->assertHeader('Expires', date('r', strtotime($dt) + $exp));
                    $response->assertHeader('X-Next-Locktime', $exp);
                } else {
                    $response->assertHeader('Cache-Control', 'no-cache, private');
                    $response->assertHeaderMissing('Expires');
                    $response->assertHeaderMissing('X-Next-Locktime');
                }
            }
        }
        $this->post('/logout')->assertStatus(200);
    }
}
