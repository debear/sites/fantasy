<?php

namespace Tests\PHPUnit\Feature\Records;

use Tests\PHPUnit\Base\FeatureTestCase;

class XMiscTest extends FeatureTestCase
{
    /**
     * Test loading of help sections and articles
     * @return void
     */
    public function testArticles(): void
    {
        $response = $this->get('/records/help');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; Help and '
            . 'Support</title>');
        $response->assertSee('<h1>Help and Support</h1>

<articles class="grid">
    ' . '
    <ul class="inline_list grid-3-10 hidden-t hidden-m">
                    <li class="section sel" data-section-id="1">Rules</li>
                    <li class="section unsel" data-section-id="2">Registration</li>
                    <li class="section unsel" data-section-id="3">Entry Management</li>
                    <li class="section unsel" data-section-id="5">Contact Us</li>
            </ul>
    ' . '
    <section-dropdown class="grid-1-1 hidden-d">
        <input type="hidden" id="article-section" name="article-section" value="1" data-is-dropdown="true" >
<dl class="dropdown " data-id="article-section"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Rules</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="1">Rules</li>
                            <li  data-id="2">Registration</li>
                            <li  data-id="3">Entry Management</li>
                            <li  data-id="5">Contact Us</li>
                    </ul>
    </dd>
</dl>

    </section-dropdown>');
        $response->assertSee('<dt class="icon_toggle_plus" data-article-id="1-1">What is the aim of DeBear Record '
            . 'Breakers?</dt>
                        <dd class="hidden" data-article-id="1-1"><p>DeBear Record Breakers allows you to tackle some '
            . 'of sport&#39;s greatest records. DiMaggio&#39;s 56 game hitting streak in 1941, Dickerson&#39;s 2,105 '
            . 'yards rushing in 1984 or scoring 50 goals in 50 NHL games have all stood the test of time. By picking '
            . 'players from real-life games, can you build up streaks or totals that equal &ndash; or hopefully better '
            . '&ndash; these legendary yardsticks?</p></dd>');
        // Inactive section should not been visible.
        $response->assertDontSee('<li class="section unsel" data-section-id="4">Groups</li>');
        $response->assertDontSee('<dt class="icon_toggle_plus" data-article-id="4-1">What are '
            . '&quot;Groups&quot;?</dt>');
    }

    /**
     * A test of the fantasy sitemap.
     * @return void
     */
    public function testSitemapXml(): void
    {
        // Main site sitemap.
        $this->setTestDateTime('2020-03-10 12:34:56');
        $response = $this->get('/records/sitemap.xml');
        $response->assertStatus(200);
        $response->assertSee('<url>
        <loc>https://fantasy.debear.test/records/help</loc>
        <changefreq>monthly</changefreq>
        <priority>0.1</priority>
    </url>');
        // An active game.
        $response->assertSee('<url>
            <loc>https://fantasy.debear.test/records/f1/413-point-season-2020</loc>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>');
        // But not a future-dated game.
        $response->assertDontSee('<url>
            <loc>https://fantasy.debear.test/records/sgp/192-point-season-2020</loc>
            <changefreq>daily</changefreq>
            <priority>0.9</priority>
        </url>');
    }
}
