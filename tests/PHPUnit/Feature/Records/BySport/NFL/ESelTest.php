<?php

namespace Tests\PHPUnit\Feature\Records\NFL;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test sport-specific information loaded in the individual player selection summary
     * @return void
     */
    public function testPlayerSummaryModal(): void
    {
        $this->setTestDateTime('2019-09-15 12:34:56');
        $response = $this->get("/records/nfl/2-105-yard-rushing-season-2019/links/aaron-jones-5409/1");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 5409);
        $response->assertJsonPath('link.slug', 'aaron-jones-5409');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/nfl/players/aaron-jones-5409');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 1);
        $response->assertJsonPath('recent.0.link.status', null);
        $response->assertJsonPath('recent.0.link.info', '@<span class="icon team-nfl-CHI">CHI</span>');
        $response->assertJsonPath('recent.0.score', 39);
        $response->assertJsonPath('recent.0.status', 'negative');
        $response->assertJsonPath('recent.0.stress', 100);
        $response->assertJsonPath('recent.0.summary', '39yds');
    }

    /**
     * Test sport-specific information loaded in the individual team selection summary
     * @return void
     */
    public function testTeamSummaryModal(): void
    {
        $this->setTestDateTime('2019-09-15 12:34:56');
        $response = $this->get("/records/nfl/the-perfect-season-2019/links/tampa-bay-buccaneers-65533/1");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 65533);
        $response->assertJsonPath('link.slug', 'tampa-bay-buccaneers-65533');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/nfl/teams/tampa-bay-buccaneers');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonPath('weather.symbol', 'clouds');
        $response->assertJsonPath('weather.temp', '79&deg;');
        $response->assertJsonPath('weather.precipitation', '41%');
        $response->assertJsonPath('weather.wind', '2mph W');
        $response->assertJsonPath('rating', 2.5);
        $response->assertJsonPath('opp_rating', 4);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 1);
        $response->assertJsonPath('recent.0.link.status', null);
        $response->assertJsonPath('recent.0.link.info', 'v<span class="icon team-nfl-SF">SF</span>');
        $response->assertJsonPath('recent.0.rating', 2.5);
        $response->assertJsonPath('recent.0.opp_rating', 4);
        $response->assertJsonPath('recent.0.score', 0);
        $response->assertJsonPath('recent.0.status', 'negative');
        $response->assertJsonPath('recent.0.stress', 100);
        $response->assertJsonPath('recent.0.summary', 'L 31&ndash;17');
    }
}
