<?php

namespace Tests\PHPUnit\Feature\Records\BySportFL;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Implementations\TestResponse;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing an NFL player-based game
     * @return void
     */
    public function testPlayerBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2019-09-15 12:34:56'); // Week 2.
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/nfl/2-105-yard-rushing-season-2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NFL &raquo; '
            . '2,105 Yard Rushing Season</title>');
        $response->assertSee('<h1 class="sports_nfl_icon" data-ref="nfl-2019-1" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">2,105 Yard Rushing Season</h1>');

        // Open Graph customisations.
        $response->assertSee('<meta name="twitter:card" content="summary_large_image" />');
        $response->assertSee('<meta name="twitter:image" content="https://cdn.debear.test/path/to/og.png" />');
        $response->assertSee('<meta property="og:url" '
            . 'content="https://fantasy.debear.test/records/nfl/2-105-yard-rushing-season-2019" />');
        $response->assertSee('<meta property="og:image" content="https://cdn.debear.test/path/to/og.png" />');
        $response->assertSee('<meta property="og:image:width" content="1200" />');
        $response->assertSee('<meta property="og:image:height" content="600" />');
        $response->assertSee('<meta name="theme-color" content="#324f17" />');

        // Progress.
        $this->playerProgress($response);
        // Selections.
        $this->playerSelections($response);
        // Leaderboard.
        $this->playerLeaderboard($response);
        // Selection Stats.
        $this->playerSelectionStats($response);
        // Game rules.
        $response->assertSee('<fieldset class="grass_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every NFL game week. All Rushing Yards gained (or lost!) by that player '
            . 'will be added to your season total.</li>');
    }

    /**
     * Sub-processing method for player-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="grass_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>NFL Leader:</dt>',
            '<dt>Overall Leader:</dt>',
            '<dt>Current Total:</dt>',
        ]);
        $response->assertDontSee('<dt>Current Leader:</dt>');
        $response->assertDontSee('<dt>Your Best:</dt>');
        $active = '<span class="icon team-nfl-IND"><a class="view-sel-summary" data-slug="marlon-mack-5435">Marlon '
            . 'Mack</a></span>';
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => '2,106'],
            ['title' => 'NFL Leader', 'class' => 'active', 'value' => 174, 'extra' => $active],
            ['title' => 'Overall Leader', 'class' => 'overall', 'value' => 154],
            ['title' => 'Current Total', 'class' => 'current', 'value' => 39],
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                            <period></period>
                                        ' . ($bar['extra'] ?? '') . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // Stress score.
        $response->assertSee('<dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">100%</dd>');
    }

    /**
     * Sub-processing method for player-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerSelections(TestResponse $response): void
    {
        // Previous selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            <div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel last period-1">
        <h3 class="invert">Week 1</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-nfl-GB"><a class="view-sel-summary" '
            . 'data-slug="aaron-jones-5409" data-period-id="1">Aaron Jones</a></span></li>
                            <li class="info">
                    <span class="info">@<span class="icon team-nfl-CHI">CHI</span></span>
                                    </li>
                                    <li class="subtitle with-info outcome invert">Outcome</li>
            <li class="subtitle with-info score invert">Total</li>
            <li class="result outcome negative">39yds</li>
            <li class="result score negative">39</li>
                            <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>100%</marker>
                    <stress-bar></stress-bar>
                </li>
                                </ul>');
        // Current selection.
        $response->assertSee('<div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel curr period-2" data-link-id="5126" data-stat-rating="4" data-stat-opp_rating="4">
        <h3 class="invert">Week 2</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-nfl-TEN"><a class="view-sel-summary" '
            . 'data-slug="derrick-henry-5126">Derrick Henry</a></span></li>
                            <li class="info">
                    <span class="info">v<span class="icon team-nfl-IND">IND</span></span>
                                    </li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                                    </li>
                                        <li class="locks icon_padlock_open invert" data-lock-time="Sun, 17:55">
                                            Selection Locks at Sun, 17:55
                                    </li>
                    </ul>');
    }

    /**
     * Sub-processing method for player-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<li class="grid-1-2 grid-t-1-1 grid-m-1-1"><fieldset class="grass_box '
            . 'leaderboard">
    <h3 class="invert">Leaderboard</h3>');
        $response->assertDontSee('<dd class="table current ">');
        $response->assertSee('<dd class="table overall ">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_new">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="testing-the-limits-10">Testing the Limits</a></dd>
                        <dd class="num-0 podium-1 score">154</dd>');
        $response->assertSee('<dd class="row-0 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_new">99th</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">39</dd>');
    }

    /**
     * Sub-processing method for player-based game selection stats
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerSelectionStats(TestResponse $response): void
    {
        $response->assertDontSee('<fieldset class="grass_box sel_stats">
    <h3 class="invert">Team Success</h3>');
    }

    /**
     * Testing an NFL team-based game
     * @return void
     */
    public function testTeamBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2019-09-15 12:34:56'); // Week 2.
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/nfl/the-perfect-season-2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NFL &raquo; '
            . 'The Perfect Season</title>');
        $response->assertSee('<h1 class="sports_nfl_icon" data-ref="nfl-2019-3" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">The Perfect Season</h1>');

        // Progress.
        $this->teamProgress($response);
        // Selections.
        $this->teamSelections($response);
        // Leaderboard.
        $this->teamLeaderboard($response);
        // Selection Stats.
        $this->teamSelectionStats($response);
        // Game rules.
        $response->assertSee('<fieldset class="grass_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every NFL game week. If that team wins, your streak increases by one (1). '
            . 'If they do not &ndash; and this includes ties &ndash; you will be eliminated and your streak will '
            . 'end.</li>');
    }

    /**
     * Sub-processing method for team-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="grass_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>Overall Leader:</dt>',
            '<dt>NFL Leader:</dt>',
            '<dt>Your Best:</dt>',
        ]);
        $response->assertDontSee('<dt>Current Leader:</dt>');
        $response->assertDontSee('<dt>Current Streak:</dt>');
        $active = '<span class="icon team-nfl-BAL"><a class="view-sel-summary" data-slug="baltimore-ravens-65506">'
            . 'Baltimore Ravens</a></span>';
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 17],
            ['title' => 'Overall Leader', 'class' => 'overall', 'value' => 1],
            ['title' => 'NFL Leader', 'class' => 'active', 'value' => 1, 'extra' => $active],
            ['title' => 'Your Best', 'class' => 'best', 'value' => 0],
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                        ' . ($bar['extra'] ?? '') . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // Stress score.
        $response->assertSee('<dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">100%</dd>');
    }

    /**
     * Sub-processing method for team-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamSelections(TestResponse $response): void
    {
        // Previous selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            <div class="grid-1-1 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel last period-1">
        <h3 class="invert">Week 1</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-nfl-TB"><a class="view-sel-summary" '
            . 'data-slug="tampa-bay-buccaneers-65533" data-period-id="1">Tampa Bay Buccaneers</a></span></li>
                            <li class="info">
                    <span class="info">v<span class="icon team-nfl-SF">SF</span></span>
                                    </li>
                                    <li class="subtitle with-info outcome invert">Outcome</li>
            <li class="subtitle with-info score invert">Streak</li>
            <li class="result outcome negative">L 31&ndash;17</li>
            <li class="result score negative">0</li>
                            <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>100%</marker>
                    <stress-bar></stress-bar>
                </li>
                                </ul>');
        // No current selection available.
        $response->assertDontSee('<fieldset class="grass_box sel curr">');
    }

    /**
     * Sub-processing method for team-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<li class="grid-1-2 grid-t-1-1 grid-m-1-1"><fieldset class="grass_box '
            . 'leaderboard">
    <h3 class="invert">Leaderboard</h3>');
        $response->assertDontSee('<dd class="table current ">');
        $response->assertSee('<dd class="table overall ">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_new">T-1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="golden-oldies-40">Golden Oldies</a></dd>
                        <dd class="num-0 podium-1 score">1</dd>
                                                                            <dt class="num-1 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_new">=</span></dt>
                        <dd class="num-1 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="recording-policy-35">Recording Policy</a></dd>
                        <dd class="num-1 podium-1 score">1</dd>');
        $response->assertSee('<dd class="row-0 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_new">1234th</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">0</dd>');
    }

    /**
     * Sub-processing method for player-based game selection stats
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamSelectionStats(TestResponse $response): void
    {
        $response->assertDontSee('<fieldset class="grass_box sel_stats">
    <h3 class="invert">Team Success</h3>');
        $response->assertDontSee('<dt>Most Common Selections</dt>');
        $response->assertDontSee('<dt>Top Streak Busters</dt>');
    }
}
