<?php

namespace Tests\PHPUnit\Feature\Records\BySport\AHL;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Implementations\TestResponse;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing an AHL player-based game
     * @return void
     */
    public function testPlayerBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-03-12 12:34:56');
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/ahl/138-point-season-2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; AHL &raquo; '
            . '138 Point Season</title>');
        $response->assertSee('<h1 class="sports_ahl_icon" data-ref="ahl-2019-2" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">138 Point Season</h1>');

        // Progress.
        $this->playerProgress($response);
        // Selections.
        $this->playerSelections($response);
        // Leaderboard.
        $this->playerLeaderboard($response);
        // Game rules.
        $response->assertSee('<fieldset class="ice_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every day. All points (goals and/or assists) scored by that player will '
            . 'be added to your season total.</li>');
    }

    /**
     * Sub-processing method for player-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="ice_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>Overall Leader:</dt>',
            '<dt>AHL Leader:</dt>',
            '<dt>Current Total:</dt>',
        ]);
        $response->assertDontSee('<dt>Current Leader:</dt>');
        $response->assertDontSee('<dt>Your Best:</dt>');
        $active = '<span class="icon team-ahl-IAW"><a class="view-sel-summary" data-slug="sam-anas-4959">Sam '
            . 'Anas</a></span>';
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 139],
            ['title' => 'Overall Leader', 'class' => 'overall', 'value' => 75],
            ['title' => 'AHL Leader', 'class' => 'active', 'value' => 70, 'extra' => $active],
            ['title' => 'Current Total', 'class' => 'current', 'value' => 66],
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                            <period></period>
                                        ' . ($bar['extra'] ?? '') . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // Stress score.
        $response->assertSee('<dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">33%</dd>');
    }

    /**
     * Sub-processing method for player-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerSelections(TestResponse $response): void
    {
        // Previous selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            <div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="ice_box sel last period-122">
        <h3 class="invert">11th March 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-ahl-SYR"><a class="view-sel-summary" '
                . 'data-slug="alex-barre-boulet-5591" data-period-id="122">Alex Barre-Boulet</a></span></li>
                            <li class="info">
                    <span class="info">@<span class="icon team-ahl-UTI">UTI</span></span>
                                    </li>
                                    <li class="subtitle with-info outcome invert">Outcome</li>
            <li class="subtitle with-info score invert">Total</li>
            <li class="result outcome negative">0 G, 0 A</li>
            <li class="result score negative">66</li>
                            <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>100%</marker>
                    <stress-bar></stress-bar>
                </li>
                                </ul>');
        // No current selection.
        $response->assertSee('<div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="ice_box sel curr period-123" data-link-id="">
        <h3 class="invert">13th March 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><em>No Selection</em></li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-add sel-make">Add</button>
                                    </li>
                                        <li class="locks icon_padlock_open invert">
                                            Selections Lock at 01:55
                                    </li>
                    </ul>');
    }

    /**
     * Sub-processing method for player-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<li class="grid-1-2 grid-t-1-1 grid-m-1-1"><fieldset class="ice_box '
            . 'leaderboard">
    <h3 class="invert">Leaderboard</h3>');
        $response->assertDontSee('<dd class="table current ">');
        $response->assertSee('<dd class="table overall ">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_up">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="testing-the-limits-10">Testing the Limits</a></dd>
                        <dd class="num-0 podium-1 score">75</dd>');
        $response->assertSee('<dd class="row-1 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_up">11th</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">66</dd>');
    }
}
