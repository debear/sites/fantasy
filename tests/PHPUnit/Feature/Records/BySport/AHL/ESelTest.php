<?php

namespace Tests\PHPUnit\Feature\Records\AHL;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test sport-specific information loaded in the individual player selection summary
     * @return void
     */
    public function testPlayerSummaryModal(): void
    {
        $this->setTestDateTime('2020-03-12 12:34:56');
        $response = $this->get("/records/ahl/138-point-season-2019/links/alex-barre-boulet-5591/122");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 5591);
        $response->assertJsonPath('link.slug', 'alex-barre-boulet-5591');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/ahl/players/alex-barre-boulet-5591');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 122);
        $response->assertJsonPath('recent.0.link.status', null);
        $response->assertJsonPath('recent.0.link.info', '@<span class="icon team-ahl-UTI">UTI</span>');
        $response->assertJsonPath('recent.0.score', 0);
        $response->assertJsonPath('recent.0.status', 'negative');
        $response->assertJsonPath('recent.0.stress', 100);
        $response->assertJsonPath('recent.0.summary', '0 G, 0 A');
    }
}
