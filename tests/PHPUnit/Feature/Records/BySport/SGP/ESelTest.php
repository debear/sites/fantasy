<?php

namespace Tests\PHPUnit\Feature\Records\SGP;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test sport-specific information loaded in the individual rider selection summary
     * @return void
     */
    public function testRiderSummaryModal(): void
    {
        $this->setTestDateTime('2020-03-28 12:34:56');
        $response = $this->get("/records/sgp/192-point-season-2020/links/niels-kristian-iversen-24/1");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 24);
        $response->assertJsonPath('link.slug', 'niels-kristian-iversen-24');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/sgp/riders/niels-kristian-iversen-24');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonPath('rating', 2.5);
        $response->assertJsonPath('opp_rating', null);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 1);
        $response->assertJsonPath('recent.0.link.status', null);
        $response->assertJsonPath('recent.0.link.info', null);
        $response->assertJsonPath('recent.0.rating', 2.5);
        $response->assertJsonPath('recent.0.opp_rating', null);
    }
}
