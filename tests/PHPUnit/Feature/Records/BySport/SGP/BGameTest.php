<?php

namespace Tests\PHPUnit\Feature\Records\BySport\SGP;

use Tests\PHPUnit\Base\FeatureTestCase;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing an SGP rider-based game
     * @return void
     */
    public function testRiderBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-03-28 12:34:56'); // German Grand Prix.
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/sgp/192-point-season-2020');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; SGP &raquo; '
            . '192 Point Season</title>');
        $response->assertSee('<h1 class="sports_sgp_icon" data-ref="sgp-2020-1" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">192 Point Season</h1>');

        // Progress.
        $response->assertSee('<fieldset class="shale_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>Current Total:</dt>',
            '<dt>SGP Leader:</dt>',
        ]);
        $response->assertDontSee('<dt>Overall Leader:</dt>');
        $response->assertDontSee('<dt>Current Leader:</dt>');
        $response->assertDontSee('<dt>Your Best:</dt>');
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 193],
            ['title' => 'Current Total', 'class' => 'current', 'value' => 0],
            ['title' => 'SGP Leader', 'class' => 'active', 'value' => 0], // No active leader.
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                        ' . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // No stress score.
        $response->assertDontSee('<dt>Stress Score:</dt>');
        // No previous selection.
        $response->assertDontSee('<fieldset class="shale_box sel last">');
        // Current selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
                        <div class="grid-1-1 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
    <fieldset class="shale_box sel curr period-1" data-link-id="24" data-stat-rating="2.5">
        <h3 class="invert"><span class="flag_right flag16_right_pl">Warsaw Grand Prix</span></h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="flag_right flag16_right_dk"><a class="view-sel-summary" '
            . 'data-slug="niels-kristian-iversen-24">Niels-Kristian Iversen</a></span></li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                                    </li>
                                        <li class="locks icon_padlock_open invert" data-lock-time="Sat 28th, 16:55">
                                            Selection Locks at Sat 28th, 16:55
                                    </li>
                    </ul>');
        // Leaderboard.
        $response->assertSee('<fieldset class="shale_box leaderboard">
    <h3 class="invert">Leaderboard</h3>
    <fieldset class="box info icon_info">The leaderboard will appear after the season has started.</fieldset>
    </fieldset>');
        // Game rules.
        $response->assertSee('<fieldset class="shale_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every Grand Prix. All World Championship points scored by that selection '
            . 'will be added to your season total.</li>');
    }
}
