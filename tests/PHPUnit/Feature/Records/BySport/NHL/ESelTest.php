<?php

namespace Tests\PHPUnit\Feature\Records\NHL;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test sport-specific information loaded in the individual player selection summary
     * @return void
     */
    public function testPlayerSummaryModal(): void
    {
        $this->setTestDateTime('2020-01-17 12:34:56');
        $valid = $this->get("/records/nhl/50-in-50-2019/links/nikita-kucherov-2220/105");
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('link_id', 2220);
        $valid->assertJsonPath('link.slug', 'nikita-kucherov-2220');
        $valid->assertJsonPath('link.profile', 'https://sports.debear.test/nhl/players/nikita-kucherov-2220');
        $valid->assertJsonPath('flags.available', true);
        $valid->assertJsonPath('rating', 4);
        $valid->assertJsonPath('opp_rating', 3);
        $valid->assertJsonPath('stats.1', '20');
        $valid->assertJsonPath('stats.2', '0.43');
        $valid->assertJsonCount(2, 'recent');
        $valid->assertJsonPath('recent.0.period_id', 105);
        $valid->assertJsonPath('recent.0.link.status', null);
        $valid->assertJsonPath('recent.0.link.info', '@<span class="icon team-nhl-WPG">WPG</span>');
        $valid->assertJsonPath('recent.0.rating', 4);
        $valid->assertJsonPath('recent.0.opp_rating', 3);
        $valid->assertJsonPath('recent.1.period_id', 104);
        $valid->assertJsonPath('recent.1.link.status', null);
        $valid->assertJsonPath('recent.1.link.info', '@<span class="icon team-nhl-MIN">MIN</span>');
        $valid->assertJsonPath('recent.1.score', 2);
        $valid->assertJsonPath('recent.1.status', 'positive');
        $valid->assertJsonPath('recent.1.stress', 24);
        $valid->assertJsonPath('recent.1.summary', '2 G');
    }
}
