<?php

namespace Tests\PHPUnit\Feature\Records\BySport\NHL;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Implementations\TestResponse;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing an NHL player-based game
     * @return void
     */
    public function testPlayerBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-01-17 12:34:56');
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/nhl/50-in-50-2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NHL &raquo; '
            . '50 in 50</title>');
        $response->assertSee('<h1 class="sports_nhl_icon" data-ref="nhl-2019-1" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">50 in 50</h1>');

        // Progress.
        $this->playerProgress($response);
        // Selections.
        $this->playerSelections($response);
        // Leaderboard.
        $this->playerLeaderboard($response);
        // Game rules.
        $response->assertSee('<h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every day. All Goals scored by that player will be added to your season '
            . 'total.</li>');
    }

    /**
     * Sub-processing method for player-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="ice_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>NHL Leader:</dt>',
            '<dt>Overall Leader:</dt>',
            '<dt>Current Total:</dt>',
        ]);
        $response->assertDontSee('<dt>Current Leader:</dt>');
        $response->assertDontSee('<dt>Your Best:</dt>');
        $active = '<span class="icon team-nhl-WSH"><a class="view-sel-summary" data-slug="alex-ovechkin-560">Alex '
            . 'Ovechkin</a></span>';
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 50],
            ['title' => 'NHL Leader', 'class' => 'active', 'value' => 42, 'extra' => $active],
            ['title' => 'Overall Leader', 'class' => 'overall', 'value' => 21],
            ['title' => 'Current Total', 'class' => 'current', 'value' => 15],
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                            <period></period>
                                        ' . ($bar['extra'] ?? '') . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // Stress score.
        $response->assertSee('<dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">40%</dd>');
    }

    /**
     * Sub-processing method for player-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerSelections(TestResponse $response): void
    {
        // Previous selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            <div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="ice_box sel last period-104">
        <h3 class="invert">16th January 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-nhl-TB"><a class="view-sel-summary" '
            . 'data-slug="nikita-kucherov-2220" data-period-id="104">Nikita Kucherov</a></span></li>
                            <li class="info">
                    <span class="info">@<span class="icon team-nhl-MIN">MIN</span></span>
                                    </li>
                                    <li class="subtitle with-info outcome invert">Outcome</li>
            <li class="subtitle with-info score invert">Total</li>
            <li class="result outcome positive">2 G</li>
            <li class="result score positive">15</li>
                            <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>24%</marker>
                    <stress-bar></stress-bar>
                </li>
                                </ul>');
        // Current selection.
        $response->assertSee('<div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="ice_box sel curr period-105" data-link-id="2220" data-stat-rating="4" data-stat-opp_rating="3" '
            . 'data-stat-1="20" data-stat-2="0.43">
        <h3 class="invert">17th January 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-nhl-TB"><a class="view-sel-summary" '
            . 'data-slug="nikita-kucherov-2220">Nikita Kucherov</a></span></li>
                            <li class="info">
                    <span class="info">@<span class="icon team-nhl-WPG">WPG</span></span>
                                    </li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                                    </li>
                                        <li class="locks icon_padlock_open invert" data-lock-time="00:55">
                                            Selection Locks at 00:55
                                    </li>
                    </ul>');
    }

    /**
     * Sub-processing method for player-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<li class="grid-1-2 grid-t-1-1 grid-m-1-1"><fieldset class="ice_box '
            . 'leaderboard">
    <h3 class="invert">Leaderboard</h3>');
        $response->assertSee('<dd class="table overall ">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_nc">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="testing-the-limits-10">Testing the Limits</a></dd>
                        <dd class="num-0 podium-1 score">21</dd>');
        $response->assertSee('<dt class="num-2 podium-3 pos"><span class="icon_right icon_right_pos_nc">3rd</span></dt>
                        <dd class="num-2 podium-3 name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="num-2 podium-3 score">15</dd>');
        $response->assertDontSee('<dd class="table current');
        $response->assertDontSee('<dd class="num-4 row-0 score">13</dd>
                                                    ' . '
                                        <dd class="row-0 horiz-sep"></dd>
                    <dt class="row-user pos">');
    }

    /**
     * Testing an NHL player-based game with Streak Busters
     * @return void
     */
    public function testPlayerStreakBuster(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-01-17 12:34:56');
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/nhl/51-game-point-streak-2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NHL &raquo; '
        . '51 Game Point Streak</title>');

        // Streak buster info.
        $response->assertSee('<fieldset class="ice_box sel_stats">
    <h3 class="invert">Team Success</h3>');
        $response->assertSee('<dl class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 top-busters">
                <dt><span class="icon_right icon_right_streak_buster">Top Streak Busters</span></dt>
                                    <dd class="rating"><i class="fa-solid fa-star sel-halves-bw-5"></i>'
            . '<i class="fa-solid fa-star sel-halves-bw-5"></i>'
            . '<i class="fa-solid fa-star-half-stroke sel-halves-bw-5"></i>'
            . '<i class="fa-regular fa-star sel-halves-bw-5"></i>'
            . '<i class="fa-regular fa-star sel-halves-bw-5"></i></dd>
                    <dd class="entity"><span class="icon team-nhl-BUF">Buffalo Sabres</span></dd>');
    }
}
