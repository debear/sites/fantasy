<?php

namespace Tests\PHPUnit\Feature\Records\BySport\MLB;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Implementations\TestResponse;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing an MLB player-based game
     * @return void
     */
    public function testPlayerBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/mlb/56-game-hitting-streak-2020');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; MLB &raquo; '
            . '56 Game Hitting Streak</title>');
        $response->assertSee('<h1 class="sports_mlb_icon" data-ref="mlb-2020-1" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">56 Game Hitting Streak</h1>');

        // Progress.
        $this->playerProgress($response);
        // Selections.
        $this->playerSelections($response);
        // Leaderboard.
        $this->playerLeaderboard($response);
        // Achievements.
        $this->playerAchievements($response);
        // Game rules.
        $response->assertSee('<fieldset class="grass_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every day. If that player registers a Hit, your streak increases by one '
            . '(1). If he doesn&#39;t it will reset to zero (0).</li>');
    }

    /**
     * Sub-processing method for player-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="grass_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>Overall Leader:</dt>',
            '<dt>MLB Leader:</dt>',
            '<dt>Current Leader:</dt>',
            '<dt>Your Best:</dt>',
            '<dt>Current Streak:</dt>',
        ]);
        $active = '<span class="icon team-mlb-SF"><a class="view-sel-summary" data-slug="donovan-solano-2188">Donovan '
            . 'Solano</a></span>';
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 57],
            ['title' => 'Overall Leader', 'class' => 'overall', 'value' => 24],
            ['title' => 'MLB Leader', 'class' => 'active', 'value' => 14, 'extra' => $active],
            ['title' => 'Current Leader', 'class' => 'leader', 'value' => 11],
            ['title' => 'Your Best', 'class' => 'best', 'value' => 7],
            ['title' => 'Current Streak', 'class' => 'current', 'value' => 5],
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                        ' . ($bar['extra'] ?? '') . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // Stress score.
        $response->assertSee('<dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">64%</dd>');
    }

    /**
     * Sub-processing method for player-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerSelections(TestResponse $response): void
    {
        // Previous selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            <div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel last period-2">
        <h3 class="invert">9th August 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-mlb-CLE"><a class="view-sel-summary" '
            . 'data-slug="cesar-hernandez-2419" data-period-id="2">Cesar Hernandez</a></span>'
            . '<abbrev class="status status-pos-start" title="Starting 1st">1st</abbrev></li>
                            <li class="info">
                    <span class="info">@<span class="icon team-mlb-CWS">CWS</span></span>
                                    </li>
                                        <li class="opponent_beaten"><fieldset class="opponent-beaten success '
            . 'icon_valid">Congratulations &ndash; your first opponent taken down!</fieldset>
</li>
                        <li class="subtitle with-info outcome invert">Outcome</li>
            <li class="subtitle with-info score invert">Streak</li>
            <li class="result outcome positive">2/4</li>
            <li class="result score positive">5</li>
                            <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>15%</marker>
                    <stress-bar></stress-bar>
                </li>
                                </ul>');
        // Current selection.
        $response->assertSee('<div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel curr period-3" data-link-id="2388" data-stat-rating="4.5" '
            . 'data-stat-opp_rating="2.5" data-flags-achieve_opp="true">
        <h3 class="invert">10th August 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-mlb-COL"><a class="view-sel-summary" '
            . 'data-slug="nolan-arenado-2388">Nolan Arenado</a></span>'
            . '<abbrev class="status status-pos-start" title="Starting 4th">4th</abbrev></li>
                            <li class="info">
                    <span class="info">v<span class="icon team-mlb-ARI">ARI</span></span>
                                    </li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                                    </li>
                                        <li class="locks icon_padlock_open invert" data-lock-time="01:35">
                                            Selection Locks at 01:35
                                    </li>
                    </ul>');
    }

    /**
     * Sub-processing method for player-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<li class="grid-1-2 grid-t-1-1 grid-m-1-1"><fieldset class="grass_box '
            . 'leaderboard">
    <h3 class="invert">Leaderboards</h3>');
        $response->assertSee('<dd class="table overall hidden">');
        $response->assertSee('<dd class="table current ">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_up">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="administering-records-55">Administering Records</a></dd>
                        <dd class="num-0 podium-1 score">11</dd>');
        $response->assertSee('<dd class="row-1 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_down">96th</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">5</dd>');
        $response->assertSee('<dd class="table overall hidden">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_nc">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="testing-the-limits-10">Testing the Limits</a></dd>
                        <dd class="num-0 podium-1 score">24</dd>');
        $response->assertSee('<dd class="row-1 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_up">123rd</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">7</dd>');
    }

    /**
     * Sub-processing method for player-based game achievements
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function playerAchievements(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="grass_box achievements">
    <h3 class="invert">In-Game Targets &ndash; 8 / 14 achieved</h3>');
        $response->assertSee('<dl class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 list clearfix">
                            <dt>Hit Streak</dt>
                                                                <dd class="level first-level achieved icon '
            . 'icon_achieve_level1">3</dd>
                                            <dd class="level achieved icon icon_achieve_level2">5</dd>
                                            <dd class="level icon icon_achieve_level3">10</dd>
                                            <dd class="level icon icon_achieve_level4">20</dd>
                                            <dd class="level icon icon_achieve_level5">40</dd>
                                        <dd class="descrip">'
            . 'Some milestones to hit along the journey to the record.</dd>');
        $response->assertSee('<dt>Opponents</dt>
                                                                <dd class="level first-level achieved icon '
            . 'icon_achieve_level1">3</dd>
                                            <dd class="level achieved icon icon_achieve_level2">5</dd>
                                            <dd class="level achieved icon icon_achieve_level3">10</dd>
                                            <dd class="level achieved icon icon_achieve_level4">20</dd>
                                            <dd class="level icon icon_achieve_level5">30</dd>
                                        <dd class="descrip">'
            . 'Spread the misery, and target a variety of opponents.</dd>');
        $response->assertSee('<dl class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 opponents clearfix">
                <dt class="icon_right icon_right_achieve_done">Opponents Targeted &ndash; 1 / 30</dt>
                    <dd class="descrip">To assist with the <em>Opponents</em> achievement, these are the opponents '
            . 'and whether or not they have been targeted successfully.</dd>
                                            <dd class="opp achieved"><span class="team icon team-mlb-ARI">ARI'
            . '</span></dd>
                                            <dd class="opp "><span class="team icon team-mlb-ATL">ATL</span></dd>
                                            <dd class="opp "><span class="team icon team-mlb-BAL">BAL'
            . '</span></dd>
                                            <dd class="opp "><span class="team icon team-mlb-BOS">BOS</span></dd>');
    }

    /**
     * Testing an MLB team-based game
     * @return void
     */
    public function testTeamBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-08-10 12:34:56');
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/mlb/26-game-winning-streak-2020');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; MLB &raquo; '
            . '26 Game Winning Streak</title>');
        $response->assertSee('<h1 class="sports_mlb_icon" data-ref="mlb-2020-3" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">26 Game Winning Streak</h1>');

        // Progress.
        $this->teamProgress($response);
        // Selections.
        $this->teamSelections($response);
        // Leaderboard.
        $this->teamLeaderboard($response);
        // Achievements.
        $this->teamAchievements($response);
        // Game rules.
        $response->assertSee('<fieldset class="grass_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every day. If that team wins your streak increases by one (1) otherwise '
            . 'it will reset to zero (0).</li>');
    }

    /**
     * Sub-processing method for team-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="grass_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>Overall Leader:</dt>',
            '<dt>Current Leader:</dt>',
            '<dt>MLB Leader:</dt>',
            '<dt>Your Best:</dt>',
            '<dt>Current Streak:</dt>',
        ]);
        $active = '<span class="icon team-mlb-HOU"><a class="view-sel-summary" data-slug="houston-astros-65516">'
            . 'Houston Astros</a></span>';
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 27],
            ['title' => 'Overall Leader', 'class' => 'overall', 'value' => 14],
            ['title' => 'Current Leader', 'class' => 'leader', 'value' => 10],
            ['title' => 'MLB Leader', 'class' => 'active', 'value' => 10, 'extra' => $active],
            ['title' => 'Your Best', 'class' => 'best', 'value' => 4],
            ['title' => 'Current Streak', 'class' => 'current', 'value' => 1],
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                        ' . ($bar['extra'] ?? '') . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // Stress score.
        $response->assertSee('<dt>Stress Score:</dt>
                <dd class="bar stress">
                    <marker></marker>
                    <stress-bar></stress-bar>
                    <div class="container"></div>
                </dd>
                <dd class="value">95%</dd>');
    }

    /**
     * Sub-processing method for team-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamSelections(TestResponse $response): void
    {
        // Previous selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
            <div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel last period-2">
        <h3 class="invert">9th August 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-mlb-CLE"><a class="view-sel-summary" '
            . 'data-slug="cleveland-indians-65512" data-period-id="2">Cleveland Indians</a></span></li>
                            <li class="info">
                    <span class="info">@<span class="icon team-mlb-CWS">CWS</span></span>
                                    </li>
                                        <li class="opponent_beaten"><fieldset class="opponent-beaten success '
            . 'icon_valid">Congratulations &ndash; your first opponent taken down!</fieldset>
</li>
                        <li class="subtitle with-info outcome invert">Outcome</li>
            <li class="subtitle with-info score invert">Streak</li>
            <li class="result outcome positive">W 5&ndash;4</li>
            <li class="result score positive">1</li>
                            <li class="subtitle stress invert">Stress Score</li>
                <li class="stress">
                    <marker>94%</marker>
                    <stress-bar></stress-bar>
                </li>
                                </ul>');
        // Current selection.
        $response->assertSee('<div class="grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="grass_box sel curr period-3" data-link-id="65531" data-stat-rating="5" data-stat-opp_rating="3"'
            . ' data-flags-achieve_opp="false">
        <h3 class="invert">10th August 2020</h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="icon team-mlb-STL"><a class="view-sel-summary" '
            . 'data-slug="st-louis-cardinals-65531">St Louis Cardinals</a></span></li>
                            <li class="info">
                    <span class="info">v<span class="icon team-mlb-PIT">PIT</span></span>
                                    </li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                                    </li>
                                        <li class="locks icon_padlock_open invert" data-lock-time="01:10">
                                            Selection Locks at 01:10
                                    </li>
                    </ul>');
    }

    /**
     * Sub-processing method for team-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<li class="grid-1-2 grid-t-1-1 grid-m-1-1"><fieldset class="grass_box '
            . 'leaderboard">
    <h3 class="invert">Leaderboards</h3>');
        $response->assertSee('<dd class="table overall hidden">');
        $response->assertSee('<dd class="table current ">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_nc">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="administering-records-55">Administering Records</a></dd>
                        <dd class="num-0 podium-1 score">10</dd>');
        $response->assertSee('<dd class="row-1 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_down">254th</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">1</dd>');
        $response->assertSee('<dd class="table overall hidden">
            <dl class="clearfix">
                                                                        <dt class="num-0 podium-1 pos"><span '
            . 'class="icon_right icon_right_pos_nc">1st</span></dt>
                        <dd class="num-0 podium-1 name"><a class="view-team-summary" '
            . 'data-slug="testing-the-limits-10">Testing the Limits</a></dd>
                        <dd class="num-0 podium-1 score">14</dd>');
        $response->assertSee('<dd class="row-1 horiz-sep"></dd>
                    <dt class="row-user pos"><span class="icon_right icon_right_pos_up">132nd</span></dt>
                        <dd class="row-user name"><a class="view-team-summary" '
            . 'data-slug="record-testers-60">Record Testers</a></dd>
                        <dd class="row-user score">4</dd>');
    }

    /**
     * Sub-processing method for team-based game achievements
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function teamAchievements(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="grass_box achievements">
    <h3 class="invert">In-Game Targets &ndash; 10 / 14 achieved</h3>');
        $response->assertSee('<dl class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 list clearfix">
                            <dt>Win Streak</dt>
                                                                <dd class="level first-level achieved icon '
            . 'icon_achieve_level1">3</dd>
                                            <dd class="level achieved icon icon_achieve_level2">5</dd>
                                            <dd class="level achieved icon icon_achieve_level3">7</dd>
                                            <dd class="level achieved icon icon_achieve_level4">10</dd>
                                            <dd class="level icon icon_achieve_level5">20</dd>
                                        <dd class="descrip">'
            . 'Some milestones to hit along the journey to the record.</dd>');
        $response->assertSee('<dt>Opponents</dt>
                                                                <dd class="level first-level achieved icon '
            . 'icon_achieve_level1">3</dd>
                                            <dd class="level achieved icon icon_achieve_level2">5</dd>
                                            <dd class="level achieved icon icon_achieve_level3">10</dd>
                                            <dd class="level icon icon_achieve_level4">20</dd>
                                            <dd class="level icon icon_achieve_level5">30</dd>
                                        <dd class="descrip">'
            . 'Spread the misery, and target a variety of opponents.</dd>');
        $response->assertSee('<dl class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 opponents clearfix">
                <dt class="icon_right icon_right_achieve_done">Opponents Targeted &ndash; 1 / 30</dt>
                    <dd class="descrip">To assist with the <em>Opponents</em> achievement, these are the opponents '
            . 'and whether or not they have been targeted successfully.</dd>
                                            <dd class="opp "><span class="team icon team-mlb-ARI">'
            . 'ARI</span></dd>
                                            <dd class="opp "><span class="team icon team-mlb-ATL">ATL</span></dd>
                                            <dd class="opp "><span class="team icon team-mlb-BAL">BAL</span></dd>');
    }
}
