<?php

namespace Tests\PHPUnit\Feature\Records\MLB;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test sport-specific information loaded in the individual player selection summary
     * @return void
     */
    public function testPlayerSummaryModal(): void
    {
        $this->setTestDateTime('2020-08-10 12:34:56');
        $response = $this->get("/records/mlb/56-game-hitting-streak-2020/links/cesar-hernandez-2419/2");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 2419);
        $response->assertJsonPath('link.slug', 'cesar-hernandez-2419');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/mlb/players/cesar-hernandez-2419');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 2);
        $response->assertJsonPath('recent.0.link.status', '<abbrev class="status status-pos-start" '
            . 'title="Starting 1st">1st</abbrev>');
        $response->assertJsonPath('recent.0.link.info', '@<span class="icon team-mlb-CWS">CWS</span>');
        $response->assertJsonPath('recent.0.score', 1);
        $response->assertJsonPath('recent.0.status', 'positive');
        $response->assertJsonPath('recent.0.stress', 15);
        $response->assertJsonPath('recent.0.summary', '2/4');
    }

    /**
     * Test sport-specific information loaded in the individual team selection summary
     * @return void
     */
    public function testTeamSummaryModal(): void
    {
        $this->setTestDateTime('2020-08-10 12:34:56');
        $response = $this->get("/records/mlb/26-game-winning-streak-2020/links/cleveland-indians-65512/2");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 65512);
        $response->assertJsonPath('link.slug', 'cleveland-indians-65512');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/mlb/teams/cleveland-indians');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 2);
        $response->assertJsonPath('recent.0.link.status', null);
        $response->assertJsonPath('recent.0.link.info', '@<span class="icon team-mlb-CWS">CWS</span>');
        $response->assertJsonPath('recent.0.score', 1);
        $response->assertJsonPath('recent.0.status', 'positive');
        $response->assertJsonPath('recent.0.stress', 94);
        $response->assertJsonPath('recent.0.summary', 'W 5&ndash;4');
    }
}
