<?php

namespace Tests\PHPUnit\Feature\Records\F1;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test sport-specific information loaded in the individual driver selection summary
     * @return void
     */
    public function testDriverSummaryModal(): void
    {
        $this->setTestDateTime('2020-03-12 12:34:56');
        $response = $this->get("/records/f1/413-point-season-2020/links/pierre-gasly-71/1");
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('link_id', 71);
        $response->assertJsonPath('link.slug', 'pierre-gasly-71');
        $response->assertJsonPath('link.profile', 'https://sports.debear.test/f1/drivers/pierre-gasly-71');
        $response->assertJsonPath('flags.available', true);
        $response->assertJsonPath('rating', 3.5);
        $response->assertJsonPath('opp_rating', null);
        $response->assertJsonCount(1, 'recent');
        $response->assertJsonPath('recent.0.period_id', 1);
        $response->assertJsonPath('recent.0.link.status', null);
        $response->assertJsonPath('recent.0.link.info', '<span class="flag_right flag16_right_it">AlphaTauri</span>');
        $response->assertJsonPath('recent.0.rating', 3.5);
        $response->assertJsonPath('recent.0.opp_rating', null);
    }
}
