<?php

namespace Tests\PHPUnit\Feature\Records\BySport\F1;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Implementations\TestResponse;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing an F1 driver-based game
     * @return void
     */
    public function testDriverBased(): void
    {
        // Uses a preset time and user.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australian Grand Prix.
        $this->post('/login', $this->user_login)->assertStatus(200);

        // Load the page.
        $response = $this->get('/records/f1/413-point-season-2020');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; F1 &raquo; '
            . '413 Point Season</title>');
        $response->assertSee('<h1 class="sports_f1_icon" data-ref="f1-2020-1" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">413 Point Season</h1>');

        // Progress.
        $this->driverProgress($response);
        // Selections.
        $this->driverSelections($response);
        // Leaderboard.
        $this->driverLeaderboard($response);
        // Achievements.
        $this->driverAchievements($response);

        // Game rules.
        $response->assertSee('<fieldset class="track_box rules closed">
    <h3 class="invert">Game Rules</h3>
    <span class="h3-inner">[ <a>Show</a> ]</span>
    <ul>
        <li>One (1) selection should be made every Grand Prix. All World Championship points scored by that driver '
            . 'will be added to your season total.</li>');
        // Modal weather.
        $response->assertSee('<fieldset class="track_box modal-body edit-modal stats-1 inc-current" '
            . 'data-mobile="stat-rating" data-filter="false">
        <h3 class="invert"><span class="flag_right flag16_right_au">Australian Grand Prix</span></h3>
        <ul class="inline_list grid period-weather">
    <li class="grid grid-1-3 row-1 title">Weather Forecast</li>
            <li class="grid grid-1-3 forecast">
            <strong>Qualifying:</strong>
            <span class="icon icon_weather_clouds">17&deg;</span>
        </li>
            <li class="grid grid-1-3 forecast">
            <strong>Race:</strong>
            <span class="icon icon_weather_rain">18&deg;</span>
        </li>
    </ul>');
    }

    /**
     * Sub-processing method for driver-based game progress bars
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function driverProgress(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="track_box progress">
    <h3 class="invert">Record Progress</h3>');
        $response->assertSeeInOrder([
            '<dt>Target:</dt>',
            '<dt>Current Total:</dt>',
            '<dt>F1 Leader:</dt>',
        ]);
        $response->assertDontSee('<dt>Overall Leader:</dt>');
        $response->assertDontSee('<dt>Current Leader:</dt>');
        $response->assertDontSee('<dt>Your Best:</dt>');
        $bars = [
            ['title' => 'Target', 'class' => 'target', 'value' => 414],
            ['title' => 'Current Total', 'class' => 'current', 'value' => 0],
            ['title' => 'F1 Leader', 'class' => 'active', 'value' => 0], // No active leader.
        ];
        foreach ($bars as $bar) {
            $response->assertSee('<dt>' . $bar['title'] . ':</dt>
                <dd class="bar ' . $bar['class'] . '">
                    <div class="progress"></div>
                    <div class="container"></div>
                                        ' . '
                </dd>
                <dd class="value">' . $bar['value'] . '</dd>');
        }
        // No stress score.
        $response->assertDontSee('<dt>Stress Score:</dt>');
    }

    /**
     * Sub-processing method for driver-based game selections
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function driverSelections(TestResponse $response): void
    {
        // No previous selection.
        $response->assertDontSee('<fieldset class="track_box sel last">');
        // Current selection.
        $response->assertSee('<li class="grid grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
                        <div class="grid-1-1 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
    <fieldset class="track_box sel curr period-1" data-link-id="71" data-stat-rating="3.5">
        <h3 class="invert"><span class="flag_right flag16_right_au">Australian Grand Prix</span></h3>');
        $response->assertSee('<ul class="inline_list">
            <li class="mugshot cdn-large"></li>
            <li class="row-1 name"><span class="flag_right flag16_right_fr"><a class="view-sel-summary" '
            . 'data-slug="pierre-gasly-71">Pierre Gasly</a></span></li>
                            <li class="info">
                    <span class="info"><span class="flag_right flag16_right_it">AlphaTauri</span></span>
                                    </li>
                                        <li class="subtitle invert">Actions</li>
                <li class="actions">
                                            <button class="btn_green sel-edit sel-make">Edit</button>
                        <button class="btn_red sel-rmv">Remove</button>
                        <div class="rmv-actions box status">
                            <div>Are you sure?</div>
                            <button class="btn_red sel-rmv-no">No, go back</button>
                            <button class="btn_green sel-rmv-yes">Yes, remove</button>
                        </div>
                                    </li>
                                        <li class="locks icon_padlock_open invert" data-lock-time="Sat 14th, 05:55">
                                            Selection Locks at Sat 14th, 05:55
                                    </li>
                    </ul>');
    }

    /**
     * Sub-processing method for driver-based game leaderboards
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function driverLeaderboard(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="track_box leaderboard">
    <h3 class="invert">Leaderboard</h3>
    <fieldset class="box info icon_info">The leaderboard will appear after the season has started.</fieldset>
    </fieldset>');
    }

    /**
     * Sub-processing method for driver-based game achievements
     * @param TestResponse $response The response to be analysed.
     * @return void
     */
    protected function driverAchievements(TestResponse $response): void
    {
        $response->assertSee('<fieldset class="track_box achievements">
    <h3 class="invert">In-Game Targets &ndash; 0 / 8 achieved</h3>');
        $response->assertSee('<dl class="grid-1-1 grid-tl-1-1 grid-tp-1-1 grid-m-1-1 list clearfix">
                            <dt>Championship Totals</dt>
                                                                '
            . '<dd class="level first-level icon icon_achieve_level1">100</dd>
                                            <dd class="level icon icon_achieve_level2">200</dd>
                                            <dd class="level icon icon_achieve_level3">300</dd>
                                            <dd class="level icon icon_achieve_level4">400</dd>
                                            <dd class="level icon icon_achieve_level5">500</dd>
                                        <dd class="descrip">'
            . 'Some milestones to hit along the journey to the record.</dd>');
        $response->assertDontSee('<dl class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 opponents clearfix">');
        $response->assertDontSee('<dt class="icon_right icon_right_achieve_done">Opponents Targeted &ndash; ');
    }
}
