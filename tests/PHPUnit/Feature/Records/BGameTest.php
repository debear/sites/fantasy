<?php

namespace Tests\PHPUnit\Feature\Records;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\ORM\Fantasy\Records\Entry;

class BGameTest extends FeatureTestCase
{
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $user_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Testing the various date sequences for a game when no user is logged in
     * @return void
     * It doesn't make sense to break this method down below 100 lines, so add an exception to the code size check.
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testDateSequenceGuest(): void
    {
        $test_game = 'f1/413-point-season-2020';

        // More than a week before date_open.
        $this->setTestDateTime('2020-02-20 12:34:56');
        $before_home = $this->get('/records');
        $before_home->assertStatus(200);
        $before_home->assertDontSee('<h3 class="invert sports_f1_icon">413 Point Season</h3>');
        $before = $this->get("/records/$test_game");
        $before->assertStatus(404);

        // Within a week of date_open.
        $this->setTestDateTime('2020-02-28 12:34:56');
        $upcoming_home = $this->get('/records');
        $upcoming_home->assertStatus(200);
        $upcoming_home->assertSeeInOrder([
            '<h3 class="group">Upcoming Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $upcoming = $this->get("/records/$test_game");
        $upcoming->assertStatus(404);

        // Between date_open and date_start.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australia.
        $preseason_home = $this->get('/records');
        $preseason_home->assertStatus(200);
        $preseason_home->assertSeeInOrder([
            '<h3 class="group">Available Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $preseason = $this->get("/records/$test_game");
        $preseason->assertStatus(200);
        $preseason->assertSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');
        $preseason->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_au">Australian Grand '
            . 'Prix</span></h3>');

        // Between date_start and date_close.
        $this->setTestDateTime('2020-03-21 12:34:56'); // Bahrain.
        $inprogress_home = $this->get('/records');
        $inprogress_home->assertStatus(200);
        $inprogress_home->assertSeeInOrder([
            '<h3 class="group">In Progress Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $inprogress = $this->get("/records/$test_game");
        $inprogress->assertStatus(200);
        $inprogress->assertSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');
        $inprogress->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_bh">Bahrain Grand '
            . 'Prix</span></h3>');

        // Between date_close and date_end.
        $this->setTestDateTime('2020-06-12 12:34:56'); // Canada.
        $reg_closed_home = $this->get('/records');
        $reg_closed_home->assertStatus(200);
        $reg_closed_home->assertSeeInOrder([
            '<h3 class="group">In-Season Games</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $reg_closed = $this->get("/records/$test_game");
        $reg_closed->assertStatus(200);
        $reg_closed->assertDontSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');
        $reg_closed->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_ca">Canadian Grand '
            . 'Prix</span></h3>');

        // Between date_end and date_complete.
        $this->setTestDateTime('2020-12-12 12:34:56'); // After Abu Dhabi.
        $completed_home = $this->get('/records');
        $completed_home->assertStatus(200);
        $completed_home->assertSeeInOrder([
            '<h3 class="group">Recently Completed Games</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $completed = $this->get("/records/$test_game");
        $completed->assertStatus(200);
        $completed->assertDontSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');
        $completed->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_at">Abu Dhabi Grand '
            . 'Prix</span></h3>');

        // After date_complete.
        $this->setTestDateTime('2020-12-31 12:34:56');
        $after_home = $this->get('/records');
        $after_home->assertStatus(200);
        $after_home->assertSeeInOrder([
            '<h3 class="group">Recently Completed Games</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $after = $this->get("/records/$test_game");
        $after->assertStatus(200);
        $after->assertSee('<h1 class="sports_f1_icon">413 Point Season</h1>');
    }

    /**
     * Testing the various date sequences for a game when a user is logged in
     * @return void
     * It doesn't make sense to break this method down below 100 lines, so add an exception to the code size check.
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testDateSequenceUser(): void
    {
        $test_game = 'f1/413-point-season-2020';
        $test_game_ref = 'f1-2020-1';
        $login = array_merge($this->user_login, ['username' => 'fantasy_clean']);
        $this->post('/login', $login)->assertStatus(200);

        // Register the user's team.
        $this->get('/records/my-entry')->assertStatus(200);
        $this->post('/records/my-entry', [
            'form_ref' => '48b5cc',
            'name' => 'Date Sequences',
            'email_status' => 'on',
            'email_status__cb' => '1',
            'email_type' => 'html',
        ])->assertStatus(303);

        // More than a week before date_open.
        $this->setTestDateTime('2020-02-20 12:34:56');
        $before_home = $this->get('/records');
        $before_home->assertStatus(200);
        $before_home->assertDontSee('<h3 class="invert sports_f1_icon">413 Point Season</h3>');
        $before = $this->get("/records/$test_game");
        $before->assertStatus(404);

        // Joining at this point will fail.
        $this->post("/records/my-entry/$test_game_ref", [], ['Accept' => 'application/json'])->assertStatus(404);

        // Within a week of date_open.
        $this->setTestDateTime('2020-02-28 12:34:56');
        $upcoming_home = $this->get('/records');
        $upcoming_home->assertStatus(200);
        $upcoming_home->assertSeeInOrder([
            '<h3 class="group">Upcoming Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $upcoming = $this->get("/records/$test_game");
        $upcoming->assertStatus(404);

        // Joining at this point will fail.
        $this->post("/records/my-entry/$test_game_ref", [], ['Accept' => 'application/json'])->assertStatus(404);

        // Between date_open and date_start.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australia.
        $preseason_home = $this->get('/records');
        $preseason_home->assertStatus(200);
        $preseason_home->assertSeeInOrder([
            '<h3 class="group">Available Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $preseason = $this->get("/records/$test_game");
        $preseason->assertStatus(200);
        $preseason->assertSee('<fieldset class="user-action game-action status icon_info game" data-ref="'
            . $test_game_ref . '">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
        $preseason->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_au">Australian Grand '
            . 'Prix</span></h3>');

        // Between date_start and date_close.
        $this->setTestDateTime('2020-03-21 12:34:56'); // Bahrain.
        $inprogress_home = $this->get('/records');
        $inprogress_home->assertStatus(200);
        $inprogress_home->assertSeeInOrder([
            '<h3 class="group">In Progress Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $inprogress = $this->get("/records/$test_game");
        $inprogress->assertStatus(200);
        $inprogress->assertSee('<fieldset class="user-action game-action status icon_info game" '
            . 'data-ref="' . $test_game_ref . '">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
        $inprogress->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_bh">Bahrain Grand '
            . 'Prix</span></h3>');

        // Between date_close and date_end.
        $this->setTestDateTime('2020-06-12 12:34:56'); // Canada.
        $reg_closed_home = $this->get('/records');
        $reg_closed_home->assertStatus(200);
        $reg_closed_home->assertSeeInOrder([
            '<h3 class="group">In Progress Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $reg_closed = $this->get("/records/$test_game");
        $reg_closed->assertStatus(200);
        $reg_closed->assertDontSee('<fieldset class="user-action game-action status icon_info game" '
            . 'data-ref="' . $test_game_ref . '">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
        $reg_closed->assertDontSee('<h3 class="invert"><span class="flag_right flag16_right_ca">Canadian Grand '
            . 'Prix</span></h3>');

        // Joining at this point will fail.
        $this->post("/records/my-entry/$test_game_ref", [], ['Accept' => 'application/json'])->assertStatus(400);

        // Now double back to between date_open and date_start and try joining.
        $this->setTestDateTime('2020-03-12 12:34:56'); // Australia.
        $this->post("/records/my-entry/$test_game_ref", [], ['Accept' => 'application/json'])->assertStatus(201);

        // Re-run the preseason screen for the updated info.
        $preseason_home_join = $this->get('/records');
        $preseason_home_join->assertStatus(200);
        $preseason_home_join->assertSeeInOrder([
            '<h3 class="group">Current Game</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $preseason_joined = $this->get("/records/$test_game");
        $preseason_joined->assertStatus(200);
        $preseason_joined->assertDontSee('<fieldset class="user-action game-action status icon_info game" '
            . 'data-ref="' . $test_game_ref . '">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
        $preseason_joined->assertSee('<h3 class="invert"><span class="flag_right flag16_right_au">Australian Grand '
            . 'Prix</span></h3>');

        // Between date_end and date_complete.
        $this->setTestDateTime('2020-12-12 12:34:56'); // After Abu Dhabi.
        $completed_home = $this->get('/records');
        $completed_home->assertStatus(200);
        $completed_home->assertSeeInOrder([
            '<h3 class="group">Recently Completed Games</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $completed = $this->get("/records/$test_game");
        $completed->assertStatus(200);
        $completed->assertDontSee('<fieldset class="user-action game-action status icon_info game" '
            . 'data-ref="' . $test_game_ref . '">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
        $completed->assertSee('<h3 class="invert"><span class="flag_right flag16_right_ae">Abu Dhabi Grand '
            . 'Prix</span></h3>');

        // After date_complete.
        $this->setTestDateTime('2020-12-31 12:34:56');
        $after_home = $this->get('/records');
        $after_home->assertStatus(200);
        $after_home->assertSeeInOrder([
            '<h3 class="group">Recently Completed Games</h3>',
            '<h3 class="invert sports_f1_icon">413 Point Season</h3>',
        ]);
        $after = $this->get("/records/$test_game");
        $after->assertStatus(200);
        $after->assertSee('<h1 class="sports_f1_icon">413 Point Season</h1>');
    }

    /**
     * Testing the miscellaneous actions and flows
     * @return void
     */
    public function testMiscActions(): void
    {
        // Unknown slug.
        $unknown = $this->get('/records/nfl/unknown-game-2019');
        $unknown->assertStatus(404);

        // Limited view as a logged out user.
        $this->setTestDateTime('2019-10-15 12:34:56');
        $logged_out = $this->get('/records/nfl/2-105-yard-rushing-season-2019');
        $logged_out->assertStatus(200);
        $logged_out->assertSee('<h1 class="sports_nfl_icon" data-ref="nfl-2019-1" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">2,105 Yard Rushing Season</h1>');
        $logged_out->assertDontSee('<fieldset class="nfl_box sel curr">');
        $logged_out->assertDontSee('<fieldset class="nfl_box sel last">');
        $logged_out->assertSee('<fieldset class="user-action status icon_info">
    To get involved, please <a>sign in</a> or <a href="/register">click here</a> to register
    and create yourself a DeBear Fantasy Sports user account!
</fieldset>');

        // Game not involved in.
        $this->setTestDateTime('2019-09-08 12:34:56'); // NFL Week 1.
        $this->post('/login', $this->user_login)->assertStatus(200);
        $initially = $this->get('/records/nfl/55-passing-td-season-2019');
        $initially->assertStatus(200);
        $initially->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NFL &raquo; '
            . '55 Passing TD Season</title>');
        $initially->assertSee('<h1 class="sports_nfl_icon" data-ref="nfl-2019-2" data-key="6bb61e3b" '
            . 'data-cache_ttl_sel="180" data-cache_ttl_info="86400">55 Passing TD Season</h1>');
        $initially->assertSee('<fieldset class="user-action game-action status icon_info game" data-ref="nfl-2019-2">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
        $initially->assertDontSee('<fieldset class="grass_box sel last">');
        $initially->assertDontSee('<fieldset class="grass_box sel curr">');

        // Join this game.
        $this->post('/records/my-entry/nfl-2019-2')->assertStatus(201);
        $joined = $this->get('/records/nfl/55-passing-td-season-2019');
        $joined->assertStatus(200);
        $joined->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NFL &raquo; '
            . '55 Passing TD Season</title>');
        $joined->assertSee('<fieldset class="user-action game-action status icon_info game" data-ref="nfl-2019-2">
    If you&#39;ve changed your mind, <a class="game-leave">click here</a> to leave this game.
</fieldset>');

        // Leave this game.
        $this->delete('/records/my-entry/nfl-2019-2')->assertStatus(200);
        $left = $this->get('/records/nfl/55-passing-td-season-2019');
        $left->assertStatus(200);
        $left->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NFL &raquo; '
            . '55 Passing TD Season</title>');
        $left->assertSee('<fieldset class="user-action game-action status icon_info game" data-ref="nfl-2019-2">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');

        // Try again after the joining period has closed.
        $this->post('/logout')->assertStatus(200);
        $this->setTestDateTime('2019-11-08 12:34:56'); // NFL Week 8.
        $this->post('/login', $this->user_login)->assertStatus(200);
        $closed = $this->get('/records/nfl/55-passing-td-season-2019');
        $closed->assertStatus(200);
        $closed->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; NFL &raquo; '
            . '55 Passing TD Season</title>');
        $closed->assertDontSee('<fieldset class="user-action game-action status icon_info game" data-ref="nfl-2019-2">
    If you&#39;re up for the challenge, <a class="game-join">click here</a> to join and start making your selections!
</fieldset>');
    }

    /**
     * Testing the period summary information
     * @return void
     */
    public function testPeriodSummary(): void
    {
        $this->setTestDateTime('2019-09-13 12:34:56'); // During Week 2, to look back at Week 1.
        $response = $this->get('/records/nfl/the-perfect-season-2019');
        $response->assertStatus(200);
        $response->assertSee('<fieldset class="grass_box period-summary">
    <h3 class="invert">Performance Review for Week 1</h3>');
        $response->assertSee('<dl class="sel-success-rate grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <dt>Overall Selection Success Rate</dt>
                <dd class="gauge">
                    <chart id="chart-sel"></chart>
                </dd>
        </dl>');
        $response->assertSeeInOrder([
            '\'entry\': new Highcharts.Chart({"chart":{"styledMode":false,"renderTo":"chart-entry","type":"solidgauge"',
            '"data":[62.12]',
            '\'sel\': new Highcharts.Chart({"chart":{"styledMode":false,"renderTo":"chart-sel","type":"solidgauge"',
            '"data":[46.88]',
        ]);
        $response->assertSee('<dl class="pos grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                            <dt>Most Popular Successful Selection</dt>
                                <dd class="sel-pct">23.12%</dd>
                                <dd class="name"><span class="icon team-nfl-BAL"><a class="view-sel-summary" '
            . 'data-slug="baltimore-ravens-65506" data-period-id="1">Baltimore Ravens</a></span></dd>
                        </dl>');
        $response->assertSee('<dl class="neg grid-1-1 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                            <dt>Most Popular Unsuccessful Selection</dt>
                                <dd class="sel-pct">6.12%</dd>
                                <dd class="name"><span class="icon team-nfl-HOU"><a class="view-sel-summary" '
            . 'data-slug="houston-texans-65516" data-period-id="1">Houston Texans</a></span></dd>
                        </dl>');
    }

    /**
     * Testing the leaderboard information
     * @return void
     */
    public function testLeaderboards(): void
    {
        $this->setTestDateTime('2019-09-13 12:34:56'); // During Week 2, to look back at Week 1.
        $endpoint = '/records/nfl/2-105-yard-rushing-season-2019/leaderboard';

        // Unknown slug.
        $unknown = $this->get('/records/nfl/unknown-game-2019/leaderboard');
        $unknown->assertStatus(404);
        $unknown->assertHeader('Content-Type', 'application/json');

        // The leaderboards, as a logged out user.
        $guest = $this->get($endpoint);
        $guest->assertStatus(200);
        $guest->assertHeader('Content-Type', 'application/json');
        $guest->assertJsonCount(5, 'overall.list');
        $guest->assertJsonPath('overall.list.0.css', 'podium-1');
        $guest->assertJsonPath('overall.list.0.entry_id', 10);
        $guest->assertJsonPath('overall.list.0.user.id', 'test_user');
        $guest->assertJsonPath('overall.list.1.css', 'podium-2');
        $guest->assertJsonPath('overall.list.1.entry_id', 35);
        $guest->assertJsonPath('overall.list.1.user.id', 'test_policy');
        $guest->assertJsonPath('overall.list.2.css', 'podium-3');
        $guest->assertJsonPath('overall.list.2.entry_id', 40);
        $guest->assertJsonPath('overall.list.2.user.id', 'test_legacy');
        $guest->assertJsonPath('overall.list.3.css', 'row-0');
        $guest->assertJsonPath('overall.list.3.entry_id', 55);
        $guest->assertJsonPath('overall.list.3.user.id', 'sports_admin');
        $guest->assertJsonPath('overall.list.4.css', 'row-1');
        $guest->assertJsonPath('overall.list.4.entry_id', 65000);
        $guest->assertJsonPath('overall.list.4.user.id', 'testbot');
        $guest->assertJsonPath('overall.user', null);
        $guest->assertJsonMissingPath('current');

        // The leaderboards, as a logged in user.
        $this->post('/login', $this->user_login)->assertStatus(200);
        $as_user = $this->get($endpoint);
        $as_user->assertStatus(200);
        $as_user->assertHeader('Content-Type', 'application/json');
        $as_user->assertJsonCount(5, 'overall.list');
        $as_user->assertJsonPath('overall.list.0.entry_id', 10);
        $as_user->assertJsonPath('overall.list.1.entry_id', 35);
        $as_user->assertJsonPath('overall.list.2.entry_id', 40);
        $as_user->assertJsonPath('overall.list.3.entry_id', 55);
        $as_user->assertJsonPath('overall.list.4.entry_id', 65000);
        $as_user->assertJsonPath('overall.user.entry_id', 60);
        $as_user->assertJsonPath('overall.user.pos.num', 99);
        $as_user->assertJsonPath('overall.user.pos.disp', '99th');
        $as_user->assertJsonPath('overall.user.pos.chg', 'icon_right icon_right_pos_new');
        $as_user->assertJsonPath('overall.user.result', '39');
        $as_user->assertJsonMissingPath('current');
        $as_user->assertJsonMissingPath('hitrate');
    }

    /**
     * Testing the per-game announcements
     * @return void
     */
    public function testAnnouncements(): void
    {
        $endpoint = '/records/nfl/2-105-yard-rushing-season-2019';
        $expected = '<fieldset class="news status">
            <h3 class="invert">NFL Update</h3>
        An announcement specific to NFL&#39;s <em>2,105 Yard Rushing Season</em> game.
</fieldset>';

        // Before.
        $this->setTestDateTime('2019-10-14 12:34:56');
        $before = $this->get($endpoint);
        $before->assertStatus(200);
        $before->assertDontSee($expected);
        // During.
        $this->setTestDateTime('2019-10-15 12:34:56');
        $during = $this->get($endpoint);
        $during->assertStatus(200);
        $during->assertSee($expected);
        // After (But within individual game's timezone).
        $this->setTestDateTime('2019-10-16 00:12:34');
        $afterA = $this->get($endpoint);
        $afterA->assertStatus(200);
        $afterA->assertDontSee($expected);
        // After (Also outside the individual game's timezone).
        $this->setTestDateTime('2019-10-16 12:34:56');
        $afterB = $this->get($endpoint);
        $afterB->assertStatus(200);
        $afterB->assertDontSee($expected);
    }

    /**
     * Test loading of individual entry's summary information
     * @return void
     */
    public function testTeamSummaryModal(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // Unknown game.
        $unknown_game = $this->get('/records/nfl/unknown-game-2019/teams/record-testers-60');
        $unknown_game->assertStatus(404);

        // Unknown entry.
        $unknown_entry = $this->get('/records/nfl/2-105-yard-rushing-season-2019/teams/unknown-entry-99');
        $unknown_entry->assertStatus(404);

        // Not joined the game.
        $not_joined = $this->get('/records/nfl/55-passing-td-season-2019/teams/record-testers-60');
        $not_joined->assertStatus(404);

        // A regular game.
        $regular = $this->get('/records/nfl/2-105-yard-rushing-season-2019/teams/record-testers-60');
        $regular->assertStatus(200);
        $regular->assertHeader('Content-Type', 'application/json');
        $regular->assertJsonPath('entry_id', 60);
        $regular->assertJsonPath('entry_name', 'Record Testers');
        $regular->assertJsonPath('user.id', 'fantasy_user');
        $regular->assertJsonPath('result', '39');
        $regular->assertJsonPath('overall.pos', 99);
        $regular->assertJsonMissingPath('current.pos');
        $regular->assertJsonPath('stress', 100);
        $regular->assertJsonCount(5, 'recent');
        $regular->assertJsonPath('recent.0.period_id', 5);
        $regular->assertJsonPath('recent.0.result', '39');
        $regular->assertJsonPath('recent.0.sel.status', 'hidden');
        $regular->assertJsonPath('recent.3.period_id', 2);
        $regular->assertJsonPath('recent.3.sel.slug', 'derrick-henry-5126');
        $regular->assertJsonPath('recent.3.result', '39');
        $regular->assertJsonPath('recent.4.period_id', 1);
        $regular->assertJsonPath('recent.4.sel.slug', 'aaron-jones-5409');
        $regular->assertJsonPath('recent.4.stress', 100);
        $regular->assertJsonPath('recent.4.result', '39');
        $regular->assertJsonPath('recent.4.status', 'negative');
        $regular->assertJsonPath('recent.4.summary', '39yds');

        // A game with selection restriction flags.
        $restricted = $this->get('/records/nfl/the-perfect-season-2019/teams/record-testers-60');
        $restricted->assertStatus(200);
        $restricted->assertHeader('Content-Type', 'application/json');
        $restricted->assertJsonPath('entry_id', 60);
        $restricted->assertJsonPath('user.id', 'fantasy_user');
        $restricted->assertJsonPath('result', '0');
        $restricted->assertJsonPath('overall.pos', 1234);
        $restricted->assertJsonMissingPath('current.pos');
        $restricted->assertJsonPath('stress', 100);
        $restricted->assertJsonCount(1, 'recent');
        $restricted->assertJsonPath('recent.0.period_id', 1);
        $restricted->assertJsonPath('recent.0.sel.slug', 'tampa-bay-buccaneers-65533');
        $restricted->assertJsonPath('recent.0.stress', 100);
        $restricted->assertJsonPath('recent.0.result', '0');
        $restricted->assertJsonPath('recent.0.status', 'negative');
        $restricted->assertJsonPath('recent.0.summary', 'L 31&ndash;17');

        // For an automated user.
        $automated = $this->get('/records/nfl/2-105-yard-rushing-season-2019/teams/testbot-3000-65000');
        $automated->assertStatus(200);
        $automated->assertHeader('Content-Type', 'application/json');
        $automated->assertJsonPath('entry_id', 65000);
        $automated->assertJsonPath('entry_name', 'Testbot 3000');
        $automated->assertJsonPath('user.id', 'testbot');
        $automated->assertJsonPath('result', '67');
        $automated->assertJsonPath('overall.pos', 5);
    }
}
