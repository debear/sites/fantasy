<?php

namespace Tests\PHPUnit\Feature\Records;

use Tests\PHPUnit\Base\FeatureTestCase;

class ESelTest extends FeatureTestCase
{
    /**
     * Test the main loading of individual selections summary information - specifics will be tested per-sport
     * @return void
     */
    public function testSummaryModal(): void
    {
        $endpoint_base = '/records/nhl/50-in-50-2019/links';
        $endpoint = "$endpoint_base/nikita-kucherov-2220";
        $this->setTestDateTime('2020-01-17 12:34:56');

        // Unknown game.
        $unknown_game = $this->get(str_replace('-2019/', '-2018/', $endpoint) . '/105');
        $unknown_game->assertStatus(404);
        $unknown_game->assertHeader('Content-Type', 'application/json');
        $unknown_game->assertJsonPath('msg', 'Game not found');

        // Unknown period.
        $unknown_period = $this->get("$endpoint/999");
        $unknown_period->assertStatus(404);
        $unknown_period->assertHeader('Content-Type', 'application/json');
        $unknown_period->assertJsonPath('msg', 'Game period not found');

        // Unknown link.
        $unknown_link = $this->get("$endpoint_base/unknown-9999");
        $unknown_link->assertStatus(404);
        $unknown_link->assertHeader('Content-Type', 'application/json');
        $unknown_link->assertJsonPath('msg', 'Selection not found or not available');

        // Incorrect link ID for slug.
        $unknown_link_id = $this->get(str_replace('-2220', '-2221', $endpoint) . '/105');
        $unknown_link_id->assertStatus(404);
        $unknown_link_id->assertHeader('Content-Type', 'application/json');
        $unknown_link_id->assertJsonPath('msg', 'Selection not found or not available');

        // Valid request.
        $valid = $this->get("$endpoint/105");
        $valid->assertStatus(200);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('link_id', 2220);
    }
}
