<?php

namespace Tests\PHPUnit\Feature\Records;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Skeleton\CommsEmail;

class CEntryTest extends FeatureTestCase
{
    /**
     * The endpoint the registration form and game actions are accessed on
     * @var string
     */
    protected string $endpoint = '/records/my-entry';
    /**
     * Login details of the user being used to complete the forms test
     * @var array
     */
    protected array $forms_login = [
        'username' => 'fantasy_clean',
        'password' => 'testlogindetails',
    ];
    /**
     * Login details of the user being used to complete the game management test
     * @var array
     */
    protected array $fantasy_login = [
        'username' => 'fantasy_user',
        'password' => 'testlogindetails',
    ];

    /**
     * Create and edit an entry
     * @return void
     */
    public function testEntryManagement(): void
    {
        $this->post('/login', $this->forms_login)->assertStatus(200);

        // Create a new entry.
        $this->registerActions(); // The actions listed on the homepage.
        $this->registerRender(); // Render the page.
        $this->registerSubmit(); // Submit the form.

        // Now update it.
        $this->updateActions(); // The actions listed on the homepage.
        $this->updateRender(); // Render the page.
        $this->updateSubmit(); // Submit the form.
    }

    /**
     * Registration sub-action: homepage actions
     * @return void
     */
    protected function registerActions(): void
    {
        $response = $this->get('/records/');
        $response->assertStatus(200);
        $response->assertSee('<h1>Welcome to Record Breakers!</h1>');
        $response->assertSee('<li class="link header_my-entry">
                        <a href="/records/my-entry" title="">My Entry</a>
        </li>');
        $response->assertSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/records/my-entry">click here</a> to
    confirm your details, or <em>Join</em> on any of the games below and get picking!
</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has been created &ndash; now join a game and start making your selections!</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has been updated.</fieldset>');
    }

    /**
     * Registration sub-action: rendering the initial form
     * @return void
     */
    protected function registerRender(): void
    {
        $response = $this->get($this->endpoint);
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; '
            . 'Create Entry</title>');
        $response->assertSee('<form method="post" action="/records/my-entry"');
        $response->assertSee('<h1>Create Your Entry</h1>');
        $response->assertSee('<ul class="inline_list field name clearfix">
        <li class="label " id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="" required minlength="5" maxlength="35" '
            . 'tabindex="1" data-tabindex="1">
        </li>
        <li class="status " id="name_status"></li>
        <li class="info">Must be between 5 and 35 characters</li>
        <li class="error details hidden" id="name_error"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list field email_updates clearfix">
        <li class="label " id="email_updates_label">
            <label for="email_type">Email Updates:</label>&nbsp;
        </li>
        <li class="value email_status">
            <input type="hidden" id="email_status__cb" name="email_status__cb" value="0">
            <input type="checkbox" id="email_status" name="email_status"  >
            <label for="email_status" data-id="email_status" tabindex="2" data-tabindex="2">Receive Email '
            . 'Updates?</label>
        </li>
        <li class="error details hidden" id="email_status_error"></li>
        <li class="value email_type">
            <input type="hidden" id="email_type" name="email_type" value="" data-is-dropdown="true" >
<dl class="dropdown " data-id="email_type" data-disabled="true" tabindex="3" data-tabindex="3">
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span><span class="icon icon_email_html">HTML</span></span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="html"><span class="icon icon_email_html">HTML</span></li>
                            <li  data-id="text"><span class="icon icon_email_text">Text Only</span></li>
                    </ul>');
        $response->assertSee('<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, creating your entry...
    </div>
    <div class="box error icon_error hidden">
      <strong>Unable to create entry</strong>: There were errors when attempting to create your entry. The fields '
            . 'marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before '
            . 'we can create your entry. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" tabindex="4" data-tabindex="4">Create Entry</button>
</div>');
    }

    /**
     * Registration sub-action: submitting the registration form
     * @return void
     */
    protected function registerSubmit(): void
    {
        // Pre-test query to determine the number of emails sent.
        $email_query = $this->getEntryEmailCountQuery();
        $emails_before = $email_query->count();

        // Submit the form.
        $post = $this->post($this->endpoint, [
            'form_ref' => '48b5cc',
            'name' => 'Test User Entry',
            'email_status' => 'on',
            'email_status__cb' => '1',
            'email_type' => 'html',
        ]);
        $post->assertStatus(303);
        $post->assertRedirect("https://fantasy.debear.test{$this->endpoint}");
        // After the Post/Redirect/Get, we should then redirect to the homepage.
        $redirect = $this->get($this->endpoint);
        $redirect->assertStatus(303);
        $redirect->assertRedirect('https://fantasy.debear.test/records');

        // Then check the displayed version.
        $get = $this->get('/records/');
        $get->assertStatus(200);
        $get->assertSee('<h1>Welcome to Record Breakers!</h1>');
        $get->assertSee('<fieldset class="user-action success icon_valid">Your entry to Record Breakers'
            . ' has been created &ndash; now join a game and start making your selections!</fieldset>');
        $get->assertDontSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/records/my-entry">click here</a> to
    confirm your details, or <em>Join</em> on any of the games below and get picking!
</fieldset>');
        $get->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has been updated.</fieldset>');

        // Number of emails sent - should have increased by one.
        $emails_after = $email_query->count();
        $this->assertEquals($emails_after, $emails_before + 1);
    }

    /**
     * Registration sub-action: homepage actions
     * @return void
     */
    protected function updateActions(): void
    {
        // Re-run the homepage query, as the flash message should not appear.
        $response = $this->get('/records/');
        $response->assertStatus(200);
        $response->assertSee('<h1>Welcome to Record Breakers!</h1>');
        $response->assertSee('<li class="link header_my-entry">
                        <a href="/records/my-entry" title="">My Entry</a>
        </li>');
        $response->assertDontSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/records/my-entry">click here</a> to
    confirm your details, or <em>Join</em> on any of the games below and get picking!
</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has been created &ndash; now join a game and start making your selections!</fieldset>');
        $response->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has been updated.</fieldset>');
    }

    /**
     * Registration sub-action: rendering the initial form
     * @return void
     */
    protected function updateRender(): void
    {
        $response = $this->get($this->endpoint);
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Fantasy Sports &raquo; Record Breakers &raquo; '
            . 'Update Entry</title>');
        $response->assertSee('<form method="post" action="/records/my-entry"');
        $response->assertSee('<h1>Update Your Entry</h1>');
        $response->assertSee('<ul class="inline_list field name clearfix">
        <li class="label " id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="Test User Entry" required minlength="5" '
            . 'maxlength="35" tabindex="1" data-tabindex="1">
        </li>
        <li class="status icon_valid" id="name_status"></li>
        <li class="info">Must be between 5 and 35 characters</li>
        <li class="error details hidden" id="name_error"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list field email_updates clearfix">
        <li class="label " id="email_updates_label">
            <label for="email_type">Email Updates:</label>&nbsp;
        </li>
        <li class="value email_status">
            <input type="hidden" id="email_status__cb" name="email_status__cb" value="1">
            <input type="checkbox" id="email_status" name="email_status"  checked="checked">
            <label for="email_status" data-id="email_status" tabindex="2" data-tabindex="2">Receive Email '
            . 'Updates?</label>
        </li>
        <li class="error details hidden" id="email_status_error"></li>
        <li class="value email_type">
            <input type="hidden" id="email_type" name="email_type" value="html" data-is-dropdown="true" >
<dl class="dropdown " data-id="email_type"  tabindex="3" data-tabindex="3">
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span><span class="icon icon_email_html">HTML</span></span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="html"><span class="icon icon_email_html">HTML</span></li>
                            <li  data-id="text"><span class="icon icon_email_text">Text Only</span></li>
                    </ul>');
        $response->assertSee('<div class="btn">
    <div class="box section icon_loading_section hidden">
        Please wait, updating your entry...
    </div>
    <div class="box error icon_error hidden">
      <strong>Unable to update entry</strong>: There were errors when attempting to update your entry. The fields '
            . 'marked in <strong class="error">red</strong> or with the exclamation symbol need to be amended before '
            . 'we can update your entry. Please correct these and try again.
    </div>
    <button class="btn_green" type="submit" tabindex="4" data-tabindex="4">Update Entry</button>
</div>');
    }

    /**
     * Registration sub-action: submitting the registration form
     * @return void
     */
    protected function updateSubmit(): void
    {
        // Pre-test query to determine the number of emails sent.
        $email_query = $this->getEntryEmailCountQuery();
        $emails_before = $email_query->count();

        // Submit the form.
        $post = $this->post($this->endpoint, [
            'form_ref' => '48b5cc',
            'name' => 'Record Hunters',
            'email_status' => 'on',
            'email_status__cb' => '1',
            'email_type' => 'html',
        ]);
        $post->assertStatus(303);
        $post->assertRedirect("https://fantasy.debear.test{$this->endpoint}");
        // After the Post/Redirect/Get, we should then redirect to the homepage.
        $redirect = $this->get($this->endpoint);
        $redirect->assertStatus(303);
        $redirect->assertRedirect('https://fantasy.debear.test/records');

        // Then check the displayed version.
        $get = $this->get('/records/');
        $get->assertStatus(200);
        $get->assertSee('<h1>Welcome to Record Breakers!</h1>');
        $get->assertSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has been updated.</fieldset>');
        $get->assertDontSee('<fieldset class="user-action success icon_valid">Your entry to Record Breakers'
            . ' has been created &ndash; now join a game and start making your selections!</fieldset>');
        $get->assertDontSee('<fieldset class="user-action info icon_info">
    One more step to get involved, just <a href="/records/my-entry">click here</a> to
    confirm your details, or <em>Join</em> on any of the games below and get picking!
</fieldset>');

        // Number of emails sent - should NOT have changed.
        $emails_after = $email_query->count();
        $this->assertEquals($emails_after, $emails_before);

        // Confirm the change when reloading the page.
        $confirm = $this->get($this->endpoint);
        $confirm->assertStatus(200);
        $confirm->assertSee('<ul class="inline_list field name clearfix">
        <li class="label " id="name_label">
            <label for="name">Entry Name:</label>&nbsp;
        </li>
        <li class="value">
            <input class="textbox" type="text" id="name" name="name" value="Record Hunters" required minlength="5" '
            . 'maxlength="35" tabindex="1" data-tabindex="1">
        </li>');
    }

    /**
     * Generate the base ORM query (still to be executed) to get the entry emails in the database
     * @return QueryBuilder Query object to load the information around the entry emails in the database
     */
    protected function getEntryEmailCountQuery(): QueryBuilder
    {
        return CommsEmail::query()
            ->where([
                'app' => 'fantasy_records',
                'email_reason' => 'entry_create',
                'user_id' => '63',
            ]);
    }

    /**
     * Join an available game
     * @return void
     */
    public function testGameJoin(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // When no user is logged in.
        $not_logged_in = $this->post("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $not_logged_in->assertStatus(401);
        $not_logged_in->assertHeader('Content-Type', 'application/json');
        $not_logged_in->assertJsonPath('success', false);

        // User does not have an entry.
        $this->post('/login', $this->forms_login)->assertStatus(200);
        $not_joined = $this->post("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $not_joined->assertStatus(201);
        $not_joined->assertHeader('Content-Type', 'application/json');
        $not_joined->assertJsonPath('success', true);
        // Now check the homepage for the flash message.
        $not_joined_home = $this->get('/records');
        $not_joined_home->assertStatus(200);
        $not_joined_home->assertSee('<fieldset class="user-action success icon_valid">Your entry to Record'
            . ' Breakers has also been created &ndash; <a href="/records/my-entry">click here</a> to review the'
            . ' settings.</fieldset>');

        // Log the user in for the remaining tests.
        $this->post('/logout')->assertStatus(200);
        $this->post('/login', $this->fantasy_login)->assertStatus(200);

        // Passed game not found.
        $unknown_game_ref = $this->post("{$this->endpoint}/asd-2019-1", [], ['Accept' => 'application/json']);
        $unknown_game_ref->assertStatus(404);
        $unknown_game_ref->assertHeader('Content-Type', 'application/json');
        $unknown_game_ref->assertJsonPath('success', false);

        // Game not open for entries yet / anymore.
        $game_not_open = $this->post("{$this->endpoint}/mlb-2020-1", [], ['Accept' => 'application/json']);
        $game_not_open->assertStatus(404);
        $game_not_open->assertHeader('Content-Type', 'application/json');
        $game_not_open->assertJsonPath('success', false);

        // A valid request.
        $valid = $this->post("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $valid->assertStatus(201);
        $valid->assertHeader('Content-Type', 'application/json');
        $valid->assertJsonPath('success', true);

        // User has already joined this game.
        $already_joined = $this->post("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $already_joined->assertStatus(409);
        $already_joined->assertHeader('Content-Type', 'application/json');
        $already_joined->assertJsonPath('success', false);
        // Ensure the homepage has been updated accordingly.
        $homepage = $this->get('/records');
        $homepage->assertStatus(200);
        $this->assertMatchesRegularExpression($this->gameJoinHomepageRegex(), $homepage->getContent());
        $homepage->assertSeeInOrder([
            '<h3 class="group">Current Games</h3>',
            '<h3 class="group">In Progress Games</h3>',
        ]);
    }

    /**
     * Form the RegEx used to validate the game on a homepage
     * @return string Matching Regular Expression for how nfl-2019-2 is represented on the homepage.
     */
    protected function gameJoinHomepageRegex(): string
    {
        return '/<fieldset class="grass_box game" data-ref="nfl-2019-2">
                    <h3 class="invert sports_nfl_icon">55 Passing TD Season<\/h3>
                    <p class="descrip">[^<]+<\/p>
                    <div class="game-detail">                                            '
            . '<div class="sel"><h3 class="invert period">Week 6<\/h3>
<ul class="inline_list clearfix">
    <li class="mugshot cdn-small"><\/li>
    <li class="row-1 name"><em>No Selection<\/em><\/li>
        <\/ul>
<style nonce="[^"]+">
    fieldset\[data-ref="nfl-2019-2"\] \.mugshot { background-image: url\([^)]+\);  }
<\/style>
<\/div>
                                                                '
            . '<div class="progress"><h3 class="progress invert">Player Progress<\/h3>
    <ul class="progress nfl-2019-2 inline_list clearfix">
                                <li class="bar target">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">Target<\/span>
                <span class="value">56<\/span>
            <\/li>
                                <li class="bar active">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">NFL Leader<\/span>
                <span class="value">5<\/span>
            <\/li>
                                <li class="bar current">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">Current Total<\/span>
                <span class="value">0<\/span>
            <\/li>
            <\/ul>

    <style nonce="[^"]+">
        ul\.progress\.nfl-2019-2 li\.bar\.target div\.progress { width: 100%; }
ul\.progress\.nfl-2019-2 li\.bar\.active div\.progress { width: 8\.9285714285714%; }
ul\.progress\.nfl-2019-2 li\.bar\.current div\.progress { width: 0%; }
    <\/style>
<\/div>
                                        <\/div>                    <div class="game-action-container">
    <ul class="inline_list game-action clearfix">
        <li class="label invert">Joined by mistake\?<\/li>
        <li class="action"><button class="btn btn_small btn_red game-leave">Hide<\/button><\/li>
    <\/ul>
<\/div>/ms';
    }

    /**
     * Leave a joined game
     * @return void
     */
    public function testGameLeave(): void
    {
        // Uses a preset time.
        $this->setTestDateTime('2019-10-15 12:34:56');

        // When no user is logged in.
        $not_logged_in = $this->delete("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $not_logged_in->assertStatus(401);
        $not_logged_in->assertHeader('Content-Type', 'application/json');
        $not_logged_in->assertJsonPath('success', false);

        // User does not have an entry.
        $this->post('/login', $this->forms_login)->assertStatus(200);
        $not_joined = $this->delete("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $not_joined->assertStatus(400);
        $not_joined->assertHeader('Content-Type', 'application/json');
        $not_joined->assertJsonPath('success', false);
        $this->post('/logout')->assertStatus(200);

        // Log the user in for the remaining tests.
        $this->post('/login', $this->fantasy_login)->assertStatus(200);

        // Passed game not found.
        $unknown_game_ref = $this->delete("{$this->endpoint}/asd-2019-1", [], ['Accept' => 'application/json']);
        $unknown_game_ref->assertStatus(404);
        $unknown_game_ref->assertHeader('Content-Type', 'application/json');
        $unknown_game_ref->assertJsonPath('success', false);

        // Game not open for entries yet / anymore.
        $game_not_open = $this->delete("{$this->endpoint}/mlb-2020-1", [], ['Accept' => 'application/json']);
        $game_not_open->assertStatus(404);
        $game_not_open->assertHeader('Content-Type', 'application/json');
        $game_not_open->assertJsonPath('success', false);

        // User has not joined the game yet.
        $not_joined = $this->delete("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $not_joined->assertStatus(404);
        $not_joined->assertHeader('Content-Type', 'application/json');
        $not_joined->assertJsonPath('success', false);

        // A valid request to join (used for some subsequent tests).
        $valid_join = $this->post("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $valid_join->assertStatus(201);
        $valid_join->assertHeader('Content-Type', 'application/json');
        $valid_join->assertJsonPath('success', true);
        // Now the request to leave.
        $valid_leave = $this->delete("{$this->endpoint}/nfl-2019-2", [], ['Accept' => 'application/json']);
        $valid_leave->assertStatus(200);
        $valid_leave->assertHeader('Content-Type', 'application/json');
        $valid_leave->assertJsonPath('success', true);
        // Ensure the homepage has been updated accordingly.
        $homepage = $this->get('/records');
        $homepage->assertStatus(200);
        $this->assertMatchesRegularExpression('/<fieldset class="grass_box game" data-ref="nfl-2019-2">
                    <h3 class="invert sports_nfl_icon">55 Passing TD Season<\/h3>
                    <p class="descrip">[^<]+<\/p>
                                                                                    '
            . '<div class="progress"><h3 class="progress invert">Player Progress<\/h3>
    <ul class="progress nfl-2019-2 inline_list clearfix">
                                <li class="bar target">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">Target<\/span>
                <span class="value">56<\/span>
            <\/li>
                                <li class="bar active">
                <div class="progress"><\/div>
                <div class="container"><\/div>
                <span class="name">NFL Leader<\/span>
                <span class="value">5<\/span>
            <\/li>
            <\/ul>

    <style nonce="[^"]+">
        ul\.progress\.nfl-2019-2 li\.bar\.target div\.progress { width: 100%; }
ul\.progress\.nfl-2019-2 li\.bar\.active div\.progress { width: 8\.9285714285714%; }
    <\/style>
<\/div>
                                                            <div class="game-action-container">
    <ul class="inline_list game-action clearfix">
        <li class="label invert">Up for the challenge\?<\/li>
        <li class="action"><button class="btn btn_small btn_green game-join">Join<\/button><\/li>
    <\/ul>
<\/div>


                                            <a class="view cta bottom" '
            . 'href="\/records\/nfl\/55-passing-td-season-2019">View Game Details<\/a>
                                    <\/fieldset>/ms', $homepage->getContent());

        // Game management locked.
        $locked = $this->delete("{$this->endpoint}/nhl-2019-1", [], ['Accept' => 'application/json']);
        $locked->assertStatus(423);
        $locked->assertHeader('Content-Type', 'application/json');
        $locked->assertJsonPath('success', false);
    }
}
