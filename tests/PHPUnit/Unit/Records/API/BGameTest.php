<?php

namespace Tests\PHPUnit\Unit\Records\API;

use Tests\PHPUnit\Fantasy\UnitTestCase;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use Carbon\Carbon;

class BGameTest extends UnitTestCase
{
    /**
     * Testing the period summary information
     * @return void
     */
    public function testScoreFormatting(): void
    {
        $base_data = [
            'sport' => 'mlb',
            'season' => 2020,
            'game_id' => 3,
            'date_complete' => (new Carbon('2099-12-31 23:59:59')), // We do not want a completed game.
            'active' => (object)[
                'name' => 'Active Leader',
                'icon' => 'test',
            ],
            'active_score' => null,
            'overall' => (object)[
                'entry_name' => 'Overall Leader',
                'user_id' => 10,
                'overall_res' => null,
                'current_res' => null,
            ],
            'current' => (object)[
                'entry_name' => 'Current Leader',
                'user_id' => 20,
                'overall_res' => null,
                'current_res' => null,
            ],
        ];

        // First test with scores under 1,000.
        $sub_thousand = new GameModel($base_data);
        $sub_thousand->active_score = '123.5';
        $sub_thousand->overall->overall_res = 2;
        $sub_thousand->overall->current_res = 1;
        $api_json = $sub_thousand->rowToAPI();
        $this->assertEquals($api_json['active']['score'], 123.5);
        $this->assertEquals($api_json['overall']['score'], 2);
        $this->assertArrayNotHasKey('current', $api_json);

        // Then test with scores 1,000 or more.
        $over_thousand = new GameModel($base_data);
        $over_thousand->active_score = '1,234.3';
        $over_thousand->active_limit = 10; // Returns a Current leaderboard.
        $over_thousand->overall->overall_res = '1,234.2';
        $over_thousand->overall->current_res = 1;
        $over_thousand->current->overall_res = 1;
        $over_thousand->current->current_res = '1,233';
        $api_json = $over_thousand->rowToAPI();
        $this->assertEquals($api_json['active']['score'], 1234.3);
        $this->assertEquals($api_json['overall']['score'], 1234.2);
        $this->assertEquals($api_json['current']['score'], 1233);
    }
}
