<?php

namespace Tests\PHPUnit\Unit\Records;

use Tests\PHPUnit\Fantasy\UnitTestCase;
use DeBear\ORM\Fantasy\Records\Game as GameModel;

class BGameTest extends UnitTestCase
{
    /**
     * Testing the game flag methods
     * @return void
     */
    public function testFlags(): void
    {
        $modelA = new GameModel([
            'sel_type' => 'ind',
            'score_type' => 'total',
            'config_flags' => 'SEL_REQUIRED',
            'active_limit' => null,
        ]);
        $this->assertTrue($modelA->isSelTypeIndividual());
        $this->assertFalse($modelA->isSelTypeTeam());
        $this->assertTrue($modelA->isScoreTypeTotal());
        $this->assertFalse($modelA->isScoreTypeStreak());
        $this->assertFalse($modelA->isScoreTypePeriods());
        $this->assertTrue($modelA->isSelectionRequired());
        $this->assertTrue($modelA->canReuseSelection());
        $this->assertTrue($modelA->canRestartGame());
        $this->assertFalse($modelA->hasActiveLimit());

        $modelB = new GameModel([
            'sel_type' => 'team',
            'score_type' => 'streak',
            'config_flags' => 'SEL_SINGLE_USE,GAME_ENDS_ON_NEGATIVE',
            'active_limit' => 0,
        ]);
        $this->assertFalse($modelB->isSelTypeIndividual());
        $this->assertTrue($modelB->isSelTypeTeam());
        $this->assertFalse($modelB->isScoreTypeTotal());
        $this->assertTrue($modelB->isScoreTypeStreak());
        $this->assertFalse($modelB->isScoreTypePeriods());
        $this->assertFalse($modelB->isSelectionRequired());
        $this->assertFalse($modelB->canReuseSelection());
        $this->assertFalse($modelB->canRestartGame());
        $this->assertFalse($modelB->hasActiveLimit());

        $modelC = new GameModel([
            'score_type' => 'periods',
            'active_limit' => 10,
        ]);
        $this->assertFalse($modelC->isScoreTypeTotal());
        $this->assertFalse($modelC->isScoreTypeStreak());
        $this->assertTrue($modelC->isScoreTypePeriods());
        $this->assertTrue($modelC->hasActiveLimit());
    }
}
