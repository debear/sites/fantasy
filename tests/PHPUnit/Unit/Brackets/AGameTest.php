<?php

namespace Tests\PHPUnit\Unit\Brackets;

use Tests\PHPUnit\Fantasy\UnitTestCase;
use DeBear\Helpers\Fantasy\Brackets\Game as GameModel;

class AGameTest extends UnitTestCase
{
    /**
     * Update internal objects between tests
     * @return void
     */
    protected function setUp(): void
    {
        // Configure our test as if running the MLB bracket.
        $_SERVER['REQUEST_URI'] = '/mlb-bracket/';
        parent::setUp();
    }

    /**
     * Testing the game status methods
     * @return void
     */
    public function testStatuses(): void
    {
        // Load our objects and expand our history.
        $this->setTestConfig(['debear.subsites.brackets-mlb.setup.season.min' => 2023]);
        $models = [
            'archive' => new GameModel('mlb', 2023),
            'current' => new GameModel('mlb', 2024),
        ];

        // Current game has yet to open.
        $this->setTestDateTime('2024-08-31 12:34:56');
        foreach ($models as $key => $model) {
            $this->assertEquals($key == 'current', $model->isHidden());
            $this->assertFalse($model->isPendingFinalisedSeeds());
            $this->assertFalse($model->isBeforePlayoffsStart());
            $this->assertFalse($model->isDuringPlayoffs());
            $this->assertFalse($model->isAfterPlayoffsCompleted());
            $this->assertEquals($key == 'archive', $model->isArchive());
            $this->assertFalse($model->canRegister());
            $this->assertFalse($model->canMakeSelections());
        }
        // Current game is visible but still the regular season.
        $this->setTestDateTime('2024-09-01 12:34:56');
        foreach ($models as $key => $model) {
            $this->assertFalse($model->isHidden());
            $this->assertEquals($key == 'current', $model->isPendingFinalisedSeeds());
            $this->assertFalse($model->isBeforePlayoffsStart());
            $this->assertFalse($model->isDuringPlayoffs());
            $this->assertFalse($model->isAfterPlayoffsCompleted());
            $this->assertEquals($key == 'archive', $model->isArchive());
            $this->assertEquals($key == 'current', $model->canRegister());
            $this->assertFalse($model->canMakeSelections());
        }
        // Current game is between the regular season and playoffs.
        $this->setTestDateTime('2024-09-30 23:59:59');
        foreach ($models as $key => $model) {
            $this->assertFalse($model->isHidden());
            $this->assertFalse($model->isPendingFinalisedSeeds());
            $this->assertEquals($key == 'current', $model->isBeforePlayoffsStart());
            $this->assertFalse($model->isDuringPlayoffs());
            $this->assertFalse($model->isAfterPlayoffsCompleted());
            $this->assertEquals($key == 'archive', $model->isArchive());
            $this->assertEquals($key == 'current', $model->canRegister());
            $this->assertEquals($key == 'current', $model->canMakeSelections());
        }
        // Current game is in the middle of its playoffs.
        $this->setTestDateTime('2024-10-21 12:34:56');
        foreach ($models as $key => $model) {
            $this->assertFalse($model->isHidden());
            $this->assertFalse($model->isPendingFinalisedSeeds());
            $this->assertFalse($model->isBeforePlayoffsStart());
            $this->assertEquals($key == 'current', $model->isDuringPlayoffs());
            $this->assertFalse($model->isAfterPlayoffsCompleted());
            $this->assertEquals($key == 'archive', $model->isArchive());
            $this->assertFalse($model->canRegister());
            $this->assertFalse($model->canMakeSelections());
        }
        // Current game had completed its playoffs.
        $this->setTestDateTime('2024-11-01 12:34:56');
        foreach ($models as $key => $model) {
            $this->assertFalse($model->isHidden());
            $this->assertFalse($model->isPendingFinalisedSeeds());
            $this->assertFalse($model->isBeforePlayoffsStart());
            $this->assertFalse($model->isDuringPlayoffs());
            $this->assertEquals($key == 'current', $model->isAfterPlayoffsCompleted());
            $this->assertEquals($key == 'archive', $model->isArchive());
            $this->assertFalse($model->canRegister());
            $this->assertFalse($model->canMakeSelections());
        }
        // Current game is archived.
        $this->setTestDateTime('2024-12-31 12:34:56');
        foreach ($models as $key => $model) {
            $this->assertFalse($model->isHidden());
            $this->assertFalse($model->isPendingFinalisedSeeds());
            $this->assertFalse($model->isBeforePlayoffsStart());
            $this->assertFalse($model->isDuringPlayoffs());
            $this->assertFalse($model->isAfterPlayoffsCompleted());
            $this->assertEquals(true, $model->isArchive());
            $this->assertFalse($model->canRegister());
            $this->assertFalse($model->canMakeSelections());
        }
    }
}
