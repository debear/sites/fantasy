<?php

namespace Tests\PHPUnit\Unit\Entities;

use Tests\PHPUnit\Fantasy\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Fantasy\Entities;

class LoadingTest extends UnitTestCase
{
    /**
     * A test of NFL entity loading.
     * @return void
     */
    public function testNFL(): void
    {
        // Define who we'll be loading and load them.
        $link_ids = [
            // Players.
            1439 => [1, 2], // Aaron Rodgers.
            221 => 2, // Matthew Stafford.
             // Teams.
            65515 => [1, 2], // Green Bay Packers.
            65533 => 1, // Tampa Bay Buccaneers.
        ];
        $models = Entities::load('nfl', 2019, $link_ids);

        // Now validate.
        $this->assertEquals(6, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(3, $models->where('type', 'team')->count());
        // Aaron Rodgers.
        $ind_ar = $models->where('link_id', 1439);
        $this->assertTrue($ind_ar->isset());
        $this->assertEquals(2, $ind_ar->count());
        // Week 1.
        $ind_a = $models->where('entity_ref', '1439:1');
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('ind', $ind_a->type);
        $this->assertEquals('Aaron Rodgers', $ind_a->name);
        $this->assertEquals('GB', $ind_a->icon);
        $this->assertEquals('@', $ind_a->info);
        $this->assertEquals('CHI', $ind_a->info_icon);
        $this->assertEquals('2019-09-06T01:15:00+01:00', $ind_a->lock_time->system->format('c'));
        // Week 2.
        $ind_b = $models->where('entity_ref', '1439:2');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('ind', $ind_b->type);
        $this->assertEquals('Aaron Rodgers', $ind_b->name);
        $this->assertEquals('GB', $ind_b->icon);
        $this->assertEquals('v', $ind_b->info);
        $this->assertEquals('MIN', $ind_b->info_icon);
        $this->assertEquals('2019-09-15T17:55:00+01:00', $ind_b->lock_time->system->format('c'));
        // Matthew Stafford.
        $ind_c = $models->where('link_id', 221);
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('ind', $ind_c->type);
        $this->assertEquals('Matthew Stafford', $ind_c->name);
        $this->assertEquals('DET', $ind_c->icon);
        $this->assertEquals('v', $ind_c->info);
        $this->assertEquals('LAC', $ind_c->info_icon);
        $this->assertEquals('2019-09-15T17:55:00+01:00', $ind_c->lock_time->system->format('c'));
        // Green Bay Packers.
        $team_gb = $models->where('link_id', 65515);
        $this->assertTrue($team_gb->isset());
        $this->assertEquals(2, $team_gb->count());
        // Week 1.
        $team_a = $models->where('entity_ref', '65515:1');
        $this->assertTrue($team_a->isset());
        $this->assertEquals(1, $team_a->count());
        $this->assertEquals('team', $team_a->type);
        $this->assertEquals('Green Bay Packers', $team_a->name);
        $this->assertEquals('GB', $team_a->icon);
        $this->assertEquals('@', $team_a->info);
        $this->assertEquals('CHI', $team_a->info_icon);
        $this->assertEquals('2019-09-06T01:15:00+01:00', $team_a->lock_time->system->format('c'));
        // Week 2.
        $team_b = $models->where('entity_ref', '65515:2');
        $this->assertTrue($team_b->isset());
        $this->assertEquals(1, $team_b->count());
        $this->assertEquals('team', $team_b->type);
        $this->assertEquals('Green Bay Packers', $team_b->name);
        $this->assertEquals('GB', $team_b->icon);
        $this->assertEquals('v', $team_b->info);
        $this->assertEquals('MIN', $team_b->info_icon);
        $this->assertEquals('2019-09-15T17:55:00+01:00', $team_b->lock_time->system->format('c'));
        // Tampa Bay Buccaneers.
        $team_c = $models->where('link_id', 65533);
        $this->assertTrue($team_c->isset());
        $this->assertEquals(1, $team_c->count());
        $this->assertEquals('team', $team_c->type);
        $this->assertEquals('Tampa Bay Buccaneers', $team_c->name);
        $this->assertEquals('TB', $team_c->icon);
        $this->assertEquals('v', $team_c->info);
        $this->assertEquals('SF', $team_c->info_icon);
        $this->assertEquals('2019-09-08T21:20:00+01:00', $team_c->lock_time->system->format('c'));
    }

    /**
     * A test of MLB entity loading.
     * @return void
     */
    public function testMLB(): void
    {
        // Define who we'll be loading and load them.
        $link_ids = [
            // Players.
            2536 => '2020-08-09', // Jose Ramirez.
            2638 => ['2020-08-09', '2020-08-10'], // Jacob deGrom.
            // Teams.
            65521 => '2020-08-09', // Milwaukee Brewers.
            65532 => ['2020-08-09', '2020-08-10'], // Tampa Bay Rays.
        ];
        $models = Entities::load('mlb', 2020, $link_ids);

        // Now validate.
        $this->assertEquals(6, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(3, $models->where('type', 'team')->count());
        // Jose Ramirez.
        $ind_a = $models->where('link_id', 2536);
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('ind', $ind_a->type);
        $this->assertEquals('Jose Ramirez', $ind_a->name);
        $this->assertEquals('CLE', $ind_a->icon);
        $this->assertEquals('@', $ind_a->info);
        $this->assertEquals('CWS', $ind_a->info_icon);
        $this->assertEquals('2020-08-10T00:03:00+01:00', $ind_a->lock_time->system->format('c'));
        // Jacob deGrom.
        $ind_jdg = $models->where('link_id', 2638);
        $this->assertTrue($ind_jdg->isset());
        $this->assertEquals(2, $ind_jdg->count());
        // 2020-08-09.
        $ind_b = $models->where('entity_ref', '2638:2020-08-09');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('ind', $ind_b->type);
        $this->assertEquals('Jacob deGrom', $ind_b->name);
        $this->assertEquals('NYM', $ind_b->icon);
        $this->assertEquals('v', $ind_b->info);
        $this->assertEquals('MIA', $ind_b->info_icon);
        $this->assertEquals('2020-08-09T18:05:00+01:00', $ind_b->lock_time->system->format('c'));
        // 2020-08-09.
        $ind_c = $models->where('entity_ref', '2638:2020-08-10');
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('ind', $ind_c->type);
        $this->assertEquals('Jacob deGrom', $ind_c->name);
        $this->assertEquals('NYM', $ind_c->icon);
        $this->assertEquals('v', $ind_c->info);
        $this->assertEquals('WSH', $ind_c->info_icon);
        $this->assertEquals('2020-08-11T00:05:00+01:00', $ind_c->lock_time->system->format('c'));
        // Milwaukee Brewers.
        $team_a = $models->where('link_id', 65521);
        $this->assertTrue($team_a->isset());
        $this->assertEquals(1, $team_a->count());
        $this->assertEquals('team', $team_a->type);
        $this->assertEquals('Milwaukee Brewers', $team_a->name);
        $this->assertEquals('MIL', $team_a->icon);
        $this->assertEquals('v', $team_a->info);
        $this->assertEquals('CIN', $team_a->info_icon);
        $this->assertEquals('2020-08-09T19:05:00+01:00', $team_a->lock_time->system->format('c'));
        // Tampa Bay Rays.
        $team_tb = $models->where('link_id', 65532);
        $this->assertTrue($team_tb->isset());
        $this->assertEquals(2, $team_tb->count());
        // 2020-08-09.
        $team_b = $models->where('entity_ref', '65532:2020-08-09');
        $this->assertTrue($team_b->isset());
        $this->assertEquals(1, $team_b->count());
        $this->assertEquals('team', $team_b->type);
        $this->assertEquals('Tampa Bay Rays', $team_b->name);
        $this->assertEquals('TB', $team_b->icon);
        $this->assertEquals('v', $team_b->info);
        $this->assertEquals('NYY', $team_b->info_icon);
        $this->assertEquals('2020-08-09T18:05:00+01:00', $team_tb->lock_time->system->format('c'));
        // 2020-08-10.
        $team_c = $models->where('entity_ref', '65532:2020-08-10');
        $this->assertTrue($team_c->isset());
        $this->assertEquals(1, $team_c->count());
        $this->assertEquals('team', $team_c->type);
        $this->assertEquals('Tampa Bay Rays', $team_c->name);
        $this->assertEquals('TB', $team_c->icon);
        $this->assertEquals('@', $team_c->info);
        $this->assertEquals('BOS', $team_c->info_icon);
        $this->assertEquals('2020-08-11T00:25:00+01:00', $team_c->lock_time->system->format('c'));
    }

    /**
     * A test of NHL entity loading.
     * @return void
     */
    public function testNHL(): void
    {
        // Define who we'll be loading and load them.
        $link_ids = [
            2220 => ['2019-12-09', '2020-01-16'], // Nikita Kucherov.
            2368 => '2019-12-10', // Andrei Vasilevskiy.
        ];
        $models = Entities::load('nhl', 2019, $link_ids);

        // Now validate.
        $this->assertEquals(3, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(0, $models->where('type', 'team')->count());
        // Nikita Kucherov.
        $ind_nk = $models->where('link_id', 2220);
        $this->assertTrue($ind_nk->isset());
        $this->assertEquals(2, $ind_nk->count());
        // 2019-12-09.
        $ind_a = $models->where('entity_ref', '2220:2019-12-09');
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('ind', $ind_a->type);
        $this->assertEquals('Nikita Kucherov', $ind_a->name);
        $this->assertEquals('TB', $ind_a->icon);
        $this->assertEquals('v', $ind_a->info);
        $this->assertEquals('NYI', $ind_a->info_icon);
        $this->assertEquals('2019-12-09T23:55:00+00:00', $ind_a->lock_time->system->format('c'));
        // 2020-01-16.
        $ind_b = $models->where('entity_ref', '2220:2020-01-16');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('ind', $ind_b->type);
        $this->assertEquals('Nikita Kucherov', $ind_b->name);
        $this->assertEquals('TB', $ind_b->icon);
        $this->assertEquals('@', $ind_b->info);
        $this->assertEquals('MIN', $ind_b->info_icon);
        $this->assertEquals('2020-01-17T00:55:00+00:00', $ind_b->lock_time->system->format('c'));
        // Andrei Vasilevskiy.
        $ind_c = $models->where('entity_ref', '2368:2019-12-10');
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('ind', $ind_c->type);
        $this->assertEquals('Andrei Vasilevskiy', $ind_c->name);
        $this->assertEquals('TB', $ind_c->icon);
        $this->assertEquals('@', $ind_c->info);
        $this->assertEquals('FLA', $ind_c->info_icon);
        $this->assertEquals('2019-12-10T23:55:00+00:00', $ind_c->lock_time->system->format('c'));
    }

    /**
     * A test of AHL entity loading.
     * @return void
     */
    public function testAHL(): void
    {
        // Define who we'll be loading and load them.
        $link_ids = [
            5591 => ['2019-11-08', '2019-11-09'], // Alex Barre-Boulet.
            5560 => '2019-11-09', // Kaapo Kahkonen.
        ];
        $models = Entities::load('ahl', 2019, $link_ids);

        // Now validate.
        $this->assertEquals(3, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(0, $models->where('type', 'team')->count());
        // Alex Barre-Boulet.
        $ind_abb = $models->where('link_id', 5591);
        $this->assertTrue($ind_abb->isset());
        $this->assertEquals(2, $ind_abb->count());
        // 2019-11-08.
        $ind_a = $models->where('entity_ref', '5591:2019-11-08');
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('ind', $ind_a->type);
        $this->assertEquals('Alex Barre-Boulet', $ind_a->name);
        $this->assertEquals('SYR', $ind_a->icon);
        $this->assertEquals('v', $ind_a->info);
        $this->assertEquals('BEL', $ind_a->info_icon);
        $this->assertEquals('2019-11-08T23:55:00+00:00', $ind_a->lock_time->system->format('c'));
        // 2019-11-09.
        $ind_b = $models->where('entity_ref', '5591:2019-11-09');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('ind', $ind_b->type);
        $this->assertEquals('Alex Barre-Boulet', $ind_b->name);
        $this->assertEquals('SYR', $ind_b->icon);
        $this->assertEquals('@', $ind_b->info);
        $this->assertEquals('WBS', $ind_b->info_icon);
        $this->assertEquals('2019-11-10T00:00:00+00:00', $ind_b->lock_time->system->format('c'));
        // Kaapo Kahkonen.
        $ind_c = $models->where('link_id', 5560);
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('ind', $ind_c->type);
        $this->assertEquals('Kaapo Kahkonen', $ind_c->name);
        $this->assertEquals('IAW', $ind_c->icon);
        $this->assertEquals('v', $ind_c->info);
        $this->assertEquals('ONT', $ind_c->info_icon);
        $this->assertEquals('2019-11-10T00:55:00+00:00', $ind_c->lock_time->system->format('c'));
    }

    /**
     * A test of F1 entity loading with qualifying locktimes.
     * @return void
     */
    public function testF1Quali(): void
    {
        $cfg_key = 'debear.sports.f1.period.lock_time';
        $this->setTestConfig([$cfg_key => FrameworkConfig::get("{$cfg_key}.pts")]);

        // Define who we'll be loading and load them.
        $link_ids = [
            68 => 1, // Esteban Ocon.
            71 => [2, 3], // Pierre Gasly.
        ];
        $models = Entities::load('f1', 2020, $link_ids);

        // Now validate.
        $this->assertEquals(3, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(0, $models->where('type', 'team')->count());
        // Esteban Ocon.
        $ind_a = $models->where('link_id', 68);
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('ind', $ind_a->type);
        $this->assertEquals('Esteban Ocon', $ind_a->name);
        $this->assertEquals('fr', $ind_a->icon);
        $this->assertEquals('Renault', $ind_a->info);
        $this->assertEquals('fr', $ind_a->info_icon);
        $this->assertEquals('2020-03-14T05:55:00+00:00', $ind_a->lock_time->system->format('c'));
        // Pierre Gasly.
        $ind_pg = $models->where('link_id', 71);
        $this->assertTrue($ind_pg->isset());
        $this->assertEquals(2, $ind_pg->count());
        // Round 2.
        $ind_b = $models->where('entity_ref', '71:2');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('ind', $ind_b->type);
        $this->assertEquals('Pierre Gasly', $ind_b->name);
        $this->assertEquals('fr', $ind_b->icon);
        $this->assertEquals('AlphaTauri', $ind_b->info);
        $this->assertEquals('it', $ind_b->info_icon);
        $this->assertEquals('2020-03-21T14:55:00+00:00', $ind_b->lock_time->system->format('c'));
        // Round 3.
        $ind_c = $models->where('entity_ref', '71:3');
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('ind', $ind_c->type);
        $this->assertEquals('Pierre Gasly', $ind_c->name);
        $this->assertEquals('fr', $ind_c->icon);
        $this->assertEquals('AlphaTauri', $ind_c->info);
        $this->assertEquals('it', $ind_c->info_icon);
        $this->assertEquals('2020-04-04T08:55:00+01:00', $ind_c->lock_time->system->format('c'));
    }

    /**
     * A test of F1 entity loading with race start locktimes.
     * @return void
     */
    public function testF1Race(): void
    {
        $cfg_key = 'debear.sports.f1.period.lock_time';
        $this->setTestConfig([$cfg_key => FrameworkConfig::get("{$cfg_key}.fl")]);

        // Define who we'll be loading and load them.
        $link_ids = [
            68 => 1, // Esteban Ocon.
            71 => [2, 3], // Pierre Gasly.
        ];
        $models = Entities::load('f1', 2020, $link_ids);

        // Now validate.
        $this->assertEquals(3, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(0, $models->where('type', 'team')->count());
        // Esteban Ocon.
        $ind_a = $models->where('link_id', 68);
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('Esteban Ocon', $ind_a->name);
        $this->assertEquals('2020-03-15T05:05:00+00:00', $ind_a->lock_time->system->format('c'));
        // Pierre Gasly.
        $ind_pg = $models->where('link_id', 71);
        $this->assertTrue($ind_pg->isset());
        $this->assertEquals(2, $ind_pg->count());
        // Round 2.
        $ind_b = $models->where('entity_ref', '71:2');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('Pierre Gasly', $ind_b->name);
        $this->assertEquals('2020-03-22T15:05:00+00:00', $ind_b->lock_time->system->format('c'));
        // Round 3.
        $ind_c = $models->where('entity_ref', '71:3');
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('Pierre Gasly', $ind_c->name);
        $this->assertEquals('2020-04-05T08:05:00+01:00', $ind_c->lock_time->system->format('c'));
    }

    /**
     * A test of SGP entity loading.
     * @return void
     */
    public function testSGP(): void
    {
        // Define who we'll be loading and load them.
        $link_ids = [
            24 => 1, // Niels-Kristian Iversen.
            15 => [2, 3], // Tai Woffinden.
        ];
        $models = Entities::load('sgp', 2020, $link_ids);

        // Now validate.
        $this->assertEquals(3, $models->count());
        $this->assertEquals(3, $models->where('type', 'ind')->count());
        $this->assertEquals(0, $models->where('type', 'team')->count());
        // Niels-Kristian Iversen.
        $ind_a = $models->where('link_id', 24);
        $this->assertTrue($ind_a->isset());
        $this->assertEquals(1, $ind_a->count());
        $this->assertEquals('ind', $ind_a->type);
        $this->assertEquals('Niels-Kristian Iversen', $ind_a->name);
        $this->assertEquals('dk', $ind_a->icon);
        $this->assertNull($ind_a->info);
        $this->assertNull($ind_a->info_icon);
        $this->assertEquals('2020-03-28T16:55:00+00:00', $ind_a->lock_time->system->format('c'));
        // Tai Woffinden.
        $ind_tw = $models->where('link_id', 15);
        $this->assertTrue($ind_tw->isset());
        $this->assertEquals(2, $ind_tw->count());
        // Round 2.
        $ind_b = $models->where('entity_ref', '15:2');
        $this->assertTrue($ind_b->isset());
        $this->assertEquals(1, $ind_b->count());
        $this->assertEquals('ind', $ind_b->type);
        $this->assertEquals('Tai Woffinden', $ind_b->name);
        $this->assertEquals('gb', $ind_b->icon);
        $this->assertNull($ind_b->info);
        $this->assertNull($ind_b->info_icon);
        $this->assertEquals('2020-05-30T17:55:00+01:00', $ind_b->lock_time->system->format('c'));
        // Round 3.
        $ind_c = $models->where('entity_ref', '15:3');
        $this->assertTrue($ind_c->isset());
        $this->assertEquals(1, $ind_c->count());
        $this->assertEquals('ind', $ind_c->type);
        $this->assertEquals('Tai Woffinden', $ind_c->name);
        $this->assertEquals('gb', $ind_c->icon);
        $this->assertNull($ind_c->info);
        $this->assertNull($ind_c->info_icon);
        $this->assertEquals('2020-06-03T17:55:00+01:00', $ind_c->lock_time->system->format('c'));
    }

    /**
     * A test of quirks and edge cases in entity loading.
     * @return void
     */
    public function testQuirks(): void
    {
        // Teams having changed name / location.
        $link_ids = [
            65528 => 1, // Oakland Raiders.
            65535 => 1, // Washington Redskins.
        ];
        $models = Entities::load('nfl', 2019, $link_ids);
        // Oakland Raiders, now primarily stored as Las Vegas.
        $team_oak = $models->where('link_id', 65528);
        $this->assertEquals('Oakland Raiders', $team_oak->name);
        // Washington Redskins, now primarily stored as Washington Football Team.
        $team_wsh = $models->where('link_id', 65535);
        $this->assertEquals('Washington Redskins', $team_wsh->name);
    }
}
