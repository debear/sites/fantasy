<?php

namespace Tests\PHPUnit\Unit\Entities;

use Tests\PHPUnit\Fantasy\UnitTestCase;
use DeBear\ORM\Fantasy\Common\Selection;

class StatusTest extends UnitTestCase
{
    /**
     * A test of NFL entity statuses.
     * @return void
     */
    public function testNFL(): void
    {
        // No status.
        $no_status = new Selection([
            'sport' => 'nfl',
        ]);
        $this->assertEquals('', $no_status->display_status);
        // Injury status.
        $injury = new Selection([
            'sport' => 'nfl',
            'status' => 'inj-d',
        ]);
        $this->assertEquals('<abbrev class="status status-inj" title="Doubtful">D</abbrev>', $injury->display_status);
        // Roster status.
        $roster = new Selection([
            'sport' => 'nfl',
            'status' => 'ir-r',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-roster" title="Injury Reserve - Recall">IR-R</abbrev>',
            $roster->display_status
        );
    }

    /**
     * A test of MLB entity statuses.
     * @return void
     */
    public function testMLB(): void
    {
        // No status.
        $no_status = new Selection([
            'sport' => 'mlb',
        ]);
        $this->assertEquals('', $no_status->display_status);
        // Starting status.
        $starter = new Selection([
            'sport' => 'mlb',
            'status' => 'pos-3',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-pos-start" title="Starting 3rd">3rd</abbrev>',
            $starter->display_status
        );
        $not_starting = new Selection([
            'sport' => 'mlb',
            'status' => 'pos-dns',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-pos-dns" title="Not in Starting Lineup">Bench</abbrev>',
            $not_starting->display_status
        );
        // Roster status.
        $roster = new Selection([
            'sport' => 'mlb',
            'status' => 'dl-15',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-roster" title="15 day IL">IL-15</abbrev>',
            $roster->display_status
        );
    }

    /**
     * A test of NHL entity statuses.
     * @return void
     */
    public function testNHL(): void
    {
        // No status.
        $no_status = new Selection([
            'sport' => 'nhl',
        ]);
        $this->assertEquals('', $no_status->display_status);
        // Injury status.
        $injury = new Selection([
            'sport' => 'nhl',
            'status' => 'inj-o',
        ]);
        $this->assertEquals('<abbrev class="status status-inj" title="Out">O</abbrev>', $injury->display_status);
        // Roster status.
        $roster = new Selection([
            'sport' => 'nhl',
            'status' => 'ir',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-roster" title="Injury Reserve">IR</abbrev>',
            $roster->display_status
        );
    }

    /**
     * A test of AHL entity statuses.
     * @return void
     */
    public function testAHL(): void
    {
        // No status (only option to be expected).
        $no_status = new Selection([
            'sport' => 'ahl',
        ]);
        $this->assertEquals('', $no_status->display_status);
    }

    /**
     * A test of F1 entity statuses.
     * @return void
     */
    public function testF1(): void
    {
        // No status.
        $no_status = new Selection([
            'sport' => 'f1',
        ]);
        $this->assertEquals('', $no_status->display_status);
        // Custom status.
        $custom = new Selection([
            'sport' => 'f1',
            'status' => 'inj',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-motors-inj" title="Injured">Inj</abbrev>',
            $custom->display_status
        );
    }

    /**
     * A test of SGP entity statuses.
     * @return void
     */
    public function testSGP(): void
    {
        // No status.
        $no_status = new Selection([
            'sport' => 'sgp',
        ]);
        $this->assertEquals('', $no_status->display_status);
        // Custom status.
        $custom = new Selection([
            'sport' => 'sgp',
            'status' => 'sub',
        ]);
        $this->assertEquals(
            '<abbrev class="status status-motors-sub" title="Substitute">Sub</abbrev>',
            $custom->display_status
        );
    }
}
