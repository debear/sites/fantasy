<?php

namespace Tests\PHPUnit\Unit\Commands;

use Tests\PHPUnit\Fantasy\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Skeleton\CommsEmail;
use DeBear\ORM\Fantasy\Admin\AutomatedUser;
use DeBear\ORM\Fantasy\Records\GamePeriodSummary;

class ArtisanTest extends UnitTestCase
{
    /**
     * A test of the automated user creation command.
     * @return void
     */
    public function testAutoUserCreate(): void
    {
        // Tweak the config for our test.
        $min = 1234;
        $max = 1299;
        $this->setTestConfig([
            'debear.setup.automated_players.user_ids.min' => $min,
            'debear.setup.automated_players.user_ids.max' => $max,
        ]);

        // A "before" picture.
        $before = AutomatedUser::query()->whereBetween('id', [$min, $max])->count();
        // Run the command, ensure we get an appropriate response.
        $this->artisanCustomised('debear:fantasy:autouser-create')
            ->expectsOutputToContain('Batch 1')
            ->expectsOutputToContain('Batch 2')
            ->expectsOutputToContain('Batch 3')
            ->doesntExpectOutputToContain('Batch 4')
            ->assertSuccessful();

        // Confirm the "after" picture.
        $after = AutomatedUser::query()->whereBetween('id', [$min, $max])->count();
        $this->assertEquals($max - $min + 1, $after - $before);
    }

    /**
     * A test of the open graph image creation command.
     * @return void
     */
    public function testOpenGraphCreate(): void
    {
        // Prepare our tests.
        $config_path = 'debear.fantasy.subsites.records.meta.og.create';
        $config = FrameworkConfig::get($config_path);
        // Remove anything that may already be in place.
        $img_path = resource_path('images') . "/{$config['img_path']}/nfl-2019-*";
        array_map('unlink', glob("$img_path/*"));
        array_map('unlink', glob("$img_path.png"));
        array_map('rmdir', glob($img_path));
        // Some faux period summaries that are in the processing logic.
        GamePeriodSummary::create([
            'sport' => 'nfl',
            'season' => 2019,
            'game_id' => 2,
            'period_id' => 1,
        ]);

        // Bespoke argument validation.
        $this->artisan('debear:fantasy:records-og-create --sport=xxx --date=2019-09-11')
            ->expectsOutputToContain('\'xxx\' is an unknown sport - aborting.');
        $this->artisan('debear:fantasy:records-og-create --sport=nfl --date=2019-09-31')
            ->expectsOutputToContain('\'2019-09-31\' is an invalid date - aborting.');

        // Command failed.
        $this->setTestConfig(["$config_path.cnv_path" => '/cmd/not/found']);
        $this->artisan('debear:fantasy:records-og-create --sport=nfl --date=2019-09-11')
            ->expectsOutputToContain('- An error occurred: /cmd/not/found failed; V:t, C:127')
            ->expectsOutputToContain('Overall Status: FAILED')
            ->expectsOutputToContain('- Success: 0')
            ->expectsOutputToContain('- Failed:  3');
        $this->setTestConfig(["$config_path.cnv_path" => $config['cnv_path']]);

        // File size check failed.
        $this->setTestConfig(["$config_path.min_bytes" => 4608]);
        $this->artisan('debear:fantasy:records-og-create --sport=nfl --date=2019-09-11')
            ->expectsOutputToContain('- An error occurred: Generated file size is below the expected minimum size')
            ->expectsOutputToContain('Overall Status: FAILED')
            ->expectsOutputToContain('- Success: 0')
            ->expectsOutputToContain('- Failed:  3');
        $this->setTestConfig(["$config_path.min_bytes" => $config['min_bytes']]);

        // Run the command now expecting success, ensuring we get an appropriate response.
        $this->artisan('debear:fantasy:records-og-create --sport=nfl --date=2019-09-11')
            ->expectsOutputToContain('Processing NFL Open Graph images for 2019-09-10')
            ->expectsOutputToContain('3 games to process:')
            ->expectsOutputToContain('- nfl-2019-1, \'2,105 Yard Rushing Season\' (Period 1)')
            ->expectsOutputToContain('- nfl-2019-2, \'55 Passing TD Season\' (Period 1)')
            ->expectsOutputToContain('- nfl-2019-3, \'The Perfect Season\' (Period 1)')
            ->expectsOutputToContain('Overall Status: SUCCESS')
            ->expectsOutputToContain('- Success: 3')
            ->expectsOutputToContain('- Failed:  0')
            ->assertSuccessful();

        // After the final period, ensure the archive OG image has been created.
        GamePeriodSummary::create([
            'sport' => 'nfl',
            'season' => 2019,
            'game_id' => 2,
            'period_id' => 17,
        ]);
        $this->artisan('debear:fantasy:records-og-create --sport=nfl --date=2020-01-01')
            ->expectsOutputToContain('- Copying image to archive location')
            ->expectsOutputToContain('Overall Status: SUCCESS');

        // Re-run on a date when there are no games to process (pruning the test data first).
        GamePeriodSummary::query()
            ->where([
                ['sport', '=', 'nfl'],
                ['season', '=', 2019],
                ['period_id', '=', 2],
            ])->get()
            ->deleteAll();

        $this->artisan('debear:fantasy:records-og-create --sport=nfl --date=2019-09-12')
            ->expectsOutputToContain('Processing NFL Open Graph images for 2019-09-11')
            ->expectsOutputToContain('0 games to process.')
            ->expectsOutputToContain('Overall Status: SUCCESS')
            ->expectsOutputToContain('- Success: 0')
            ->expectsOutputToContain('- Failed:  0')
            ->assertSuccessful();
    }

    /**
     * A test of the open graph preview image creation command.
     * - Tests for the image creation errors are handled above - this test covers the preview calling script.
     * @return void
     */
    public function testOpenGraphPreview(): void
    {
        // Argument validation.
        $this->artisan('debear:fantasy:records-og-preview --sport=xxx --season=2020')
            ->expectsOutputToContain('\'xxx\' is an unknown sport - aborting.');
        $this->artisan('debear:fantasy:records-og-preview --sport=mlb')
            ->expectsOutputToContain('Season argument is missing or invalid - aborting.');
        $this->artisan('debear:fantasy:records-og-preview --sport=mlb --season=text')
            ->expectsOutputToContain('Season argument is missing or invalid - aborting.');
        $this->artisan('debear:fantasy:records-og-preview --sport=mlb --season=2000')
            ->expectsOutputToContain('Season argument is missing or invalid - aborting.');
        $this->artisan('debear:fantasy:records-og-preview --sport=mlb --season=2019')
            ->expectsOutputToContain('No valid games found for sport \'mlb\' in season \'2019\' - aborting.');

        // Run the command now expecting success, ensuring we get an appropriate response.
        $this->artisan('debear:fantasy:records-og-preview --sport=mlb --season=2020')
            ->expectsOutputToContain('Processing MLB Open Graph preview images')
            ->expectsOutputToContain('3 games to process:')
            ->expectsOutputToContain('- mlb-2020-1, \'56 Game Hitting Streak\'')
            ->expectsOutputToContain('- mlb-2020-2, \'73 Home Run Season\'')
            ->expectsOutputToContain('- mlb-2020-3, \'26 Game Winning Streak\'')
            ->expectsOutputToContain('Overall Status: SUCCESS')
            ->expectsOutputToContain('- Success: 3')
            ->expectsOutputToContain('- Failed:  0')
            ->assertSuccessful();
    }

    /**
     * A test of the game end email sending script
     * @return void
     */
    public function testEmailGameEnd(): void
    {
        $this->setTestDateTime('2020-01-15 12:34:56');

        // First pass, should fail to identify any games to process.
        $this->artisan('debear:fantasy:records-email-gameend')
            ->expectsOutputToContain('Processing game end email updates for')
            ->expectsOutputToContain('0 sports to process')
            ->assertSuccessful();

        // Tweak the database to ensure our next run can match.
        $data = [];
        foreach ([1,2,3] as $game_id) {
            foreach (range(1, 17) as $period_id) {
                $data[] = [
                    'sport' => 'nfl',
                    'season' => 2019,
                    'game_id' => $game_id,
                    'period_id' => $period_id,
                    'when_synced' => null,
                ];
            }
        }
        GamePeriodSummary::createMany($data);

        // Count of emails before.
        $email_query = CommsEmail::query()->where([
            'app' => 'fantasy_records',
            'email_reason' => 'game_end',
        ]);
        $emails_before = $email_query->count();

        // Now run our command to test the processing.
        $this->artisan('debear:fantasy:records-email-gameend')
            ->expectsOutputToContain('1 sport to process')
            ->expectsOutputToContain('- nfl-2019')
            ->expectsOutputToContain('  - User 60 (Type: html; Game(s): nfl-2019-1, nfl-2019-3)')
            ->assertSuccessful();

        // Count of emails before, and test.
        $emails_after = $email_query->count();
        $this->assertEquals(5, $emails_after - $emails_before);

        // Get and validate one of those emails.
        $email = CommsEmail::query()->where([
            'app' => 'fantasy_records',
            'email_reason' => 'game_end',
            'user_id' => 60,
        ])->orderByDesc('email_queued')->get();
        $this->assertEquals('DeBear Fantasy Sports &lt;noreply@debear.test&gt;', $email->email_from);
        $this->assertEquals('Your DeBear Fantasy Record Breakers NFL Update', $email->email_subject);
        $this->assertStringContainsString('Hi Fantasy,', $email->email_body);
        $this->assertStringContainsString('With the NFL season having come to an end, here is a recap of how your'
            . ' entries got on!', $email->email_body);
        $this->assertStringContainsString('2,105 Yard Rushing Season
&ndash; Target:&nbsp;2106
&ndash; Leader:&nbsp;154
&ndash; Your Best:&nbsp;39 &ndash; 99th
&ndash; Stress Score:&nbsp;100%', $email->email_body);
        $this->assertStringContainsString('The Perfect Season
&ndash; Target:&nbsp;17
&ndash; Leader:&nbsp;1
&ndash; Your Best:&nbsp;0 &ndash; 1234th
&ndash; Stress Score:&nbsp;100%', $email->email_body);
        $this->assertStringContainsString('please keep an eye on @DeBearRecords [1] for our latest updates before the '
            . 'start of the next NFL season!', $email->email_body);
        // The styling for our width bars must appear in the HTML block, not the text-only block.
        $this->assertStringContainsString('&lt;style type=&quot;text/css&quot;&gt;
  .game_box.game_nfl-2019-3 .bar.target { width: 100%; }', $email->email_body);
        $this->assertStringNotContainsString('

.game_box.game_nfl-2019-3 .bar.target { width: 100%; }', $email->email_body);
    }
}
