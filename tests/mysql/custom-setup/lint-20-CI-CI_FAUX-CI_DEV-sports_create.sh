#!/bin/bash
# Create the sports schema in the test database
echo -e "\e[1mCreating sports schema in the test database:\e[0m"

dir_base=$(realpath $(dirname $0)/..)
$dir_base/schema sports
