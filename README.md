# DeBear Fantasy Sports

The main site in the DeBear stable, this is the sub-site that was why development originally started and the kind of site that wanted to be continually built upon.

## Status

[![Version: 3.0](https://img.shields.io/badge/Version-3.0-brightgreen.svg)](https://gitlab.com/debear/fantasy/blob/master/CHANGELOG.md)
[![Build: 1952](https://img.shields.io/badge/Build-1952-yellow.svg)](https://gitlab.com/debear/fantasy/blob/master/CHANGELOG.md)
[![Skeleton: 1439](https://img.shields.io/badge/Skeleton-1439-orange.svg)](https://gitlab.com/debear/skeleton)
[![Coding Style: PSR-12](https://img.shields.io/badge/Coding_Style-PSR--12-lightgrey.svg)](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md)
[![Pipeline Status](https://gitlab.com/debear/fantasy/badges/master/pipeline.svg)](https://gitlab.com/debear/fantasy/commits/master)
[![Coverage Report](https://gitlab.com/debear/sites/fantasy/badges/master/coverage.svg)](https://gitlab.com/debear/sites/fantasy/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Motivation

Started by a keen player, looking to build his own platform. Not necessarily looking to revolutionise at this stage, just start by understanding what was involved and then grow from there. Although initial versions (pre-VCS) were based on baseball or hockey prototypes, this implementation started with some "low hanging fruit" within Motorsport and then is growning towards the target of the Major League Draft and Manage games as the [Sports](https://gitlab.com/debear/sports) integrations are developed and stabilised.

## Game List

* F1 _(on hold since the end of the 2016 season)_
* MotoGP _(on hold since the end of the 2015 season)_
* SGP _(on hold since the end of the 2016 season)_
* NFL Salary Cap

### Future Plans

* Record Breakers
* Playoff Brackets
* Major League Draft and Manage

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/fantasy/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/fantasy/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
