<?php

namespace DeBear\Models\Fantasy\Records;

use DeBear\Implementations\Model;

class Entry extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_RECORDS_ENTRIES';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'user_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'joined' => 'datetime:Y-m-d',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
