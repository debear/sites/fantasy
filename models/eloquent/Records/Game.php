<?php

namespace DeBear\Models\Fantasy\Records;

use DeBear\Implementations\Model;

class Game extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_RECORDS_GAMES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'sport',
        'season',
        'game_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date_open' => 'datetime',
        'date_start' => 'datetime',
        'date_close' => 'datetime',
        'date_end' => 'datetime',
        'date_complete' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
