<?php

namespace DeBear\Models\Fantasy\Records;

use DeBear\Implementations\Model;

class GameCommsTwitter extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_RECORDS_SETUP_TWITTER';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'sport',
        'season',
        'game_id',
        'tweet_type',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
