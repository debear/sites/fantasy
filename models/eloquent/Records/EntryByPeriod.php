<?php

namespace DeBear\Models\Fantasy\Records;

use DeBear\Implementations\Model;

class EntryByPeriod extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_RECORDS_ENTRIES_BYPERIOD';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'sport',
        'season',
        'game_id',
        'user_id',
        'period_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'time_saved' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
