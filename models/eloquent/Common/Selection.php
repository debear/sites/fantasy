<?php

namespace DeBear\Models\Fantasy\Common;

use DeBear\Implementations\Model;

class Selection extends Model
{
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'link_id';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
