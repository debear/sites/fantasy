<?php

namespace DeBear\Models\Fantasy\Common;

use DeBear\Implementations\Model;

class Announcement extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_COMMON_ANNOUNCEMENTS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'game',
        'season',
        'announce_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'disp_start' => 'datetime',
        'disp_end' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
