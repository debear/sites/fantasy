<?php

namespace DeBear\Models\Fantasy\Common;

use DeBear\Implementations\Model;

class MotorsSelectionStatus extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_COMMON_MOTORS_SELECTIONS_STATUS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'sport',
        'season',
        'period_id',
        'link_id',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
