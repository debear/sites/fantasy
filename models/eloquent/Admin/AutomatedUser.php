<?php

namespace DeBear\Models\Fantasy\Admin;

use DeBear\Implementations\Model;

class AutomatedUser extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_AUTOMATED_USER';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
