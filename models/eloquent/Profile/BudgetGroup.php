<?php

namespace DeBear\Models\Fantasy\Profile;

use DeBear\Implementations\Model;

class BudgetGroup extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_PROFILE_BUDGET_GROUPS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'game',
        'season',
        'group_id',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
