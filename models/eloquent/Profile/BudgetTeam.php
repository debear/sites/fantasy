<?php

namespace DeBear\Models\Fantasy\Profile;

class BudgetTeam extends GenericTeam
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_PROFILE_BUDGET_TEAMS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'user_id',
        'game',
        'season',
        'team_id',
    ];
}
