<?php

namespace DeBear\Models\Fantasy\Brackets;

use DeBear\Implementations\Model;

class SetupDates extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_fantasy';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'FANTASY_BRACKETS_SETUP_DATES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'sport',
        'season',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'reg_end' => 'datetime',
        'po_start' => 'datetime',
        'po_end' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
