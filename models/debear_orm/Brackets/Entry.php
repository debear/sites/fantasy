<?php

namespace DeBear\ORM\Fantasy\Brackets;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\User as UserModel;

class Entry extends Instance
{
    /**
     * Load the entry for the current user
     * @param integer|null $user_id The (numeric) ID of the user's entry being loaded.
     * @return self ORM object containing the entry for the user
     */
    public static function load(?int $user_id = null): self
    {
        return static::query()
            ->where([
                ['sport', '=', FrameworkConfig::get('debear.setup.sport_ref')],
                ['season', '=', FrameworkConfig::get('debear.setup.season.viewing')],
                ['user_id', '=', $user_id ?? UserModel::object()->id ?? 0],
            ])->get();
    }
}
