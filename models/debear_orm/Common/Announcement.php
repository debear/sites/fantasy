<?php

namespace DeBear\ORM\Fantasy\Common;

use DeBear\ORM\Base\Instance;
use DeBear\Repositories\Time;

class Announcement extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['game', 'season', 'section'];

    /**
     * Get the ordered list of relevant announcements for a game on the current date
     * @param string            $game    The game for which announcements are being loaded.
     * @param integer           $season  The game's season for which announcements are being loaded.
     * @param array|string|null $section An optional sub-section of announcements.
     * @return self An ORM object containing the relevant announcements
     */
    public static function load(string $game, int $season, array|string|null $section = null): self
    {
        $section_method = (is_array($section) ? 'whereIn' : 'where');
        return Announcement::query()
            ->where('game', $game)
            ->where('season', sprintf('%04d', $season))
            ->$section_method('section', $section)
            ->whereRaw('? BETWEEN disp_start AND disp_end', [Time::object()->getNowFmt()])
            ->orderBy('section')
            ->orderBy('disp_end')
            ->get();
    }
}
