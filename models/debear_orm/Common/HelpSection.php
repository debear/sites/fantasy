<?php

namespace DeBear\ORM\Fantasy\Common;

use DeBear\ORM\Base\Instance;

class HelpSection extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['game', 'season', 'active'];

    /**
     * Load the help articles for the provided game
     * @param string  $game   The fantasy game for which we are loading articles.
     * @param integer $season Optional season for filtering articles.
     * @return self An ORM object of help sections (and its articles)
     */
    public static function loadArticles(string $game, int $season = 0): self
    {
        return static::query()
            ->where([
                ['game', '=', $game],
                ['season', '=', sprintf('%04d', $season)],
                ['active', '=', 1],
            ])->orderBy('order')
            ->get()
            ->loadSecondary(['articles']);
    }

    /**
     * Load the articles for the discovered sections
     * @return void
     */
    protected function loadSecondaryArticles(): void
    {
        // Load the articles (at this time, assumes query is for a single game).
        list($game) = $this->unique('game');
        list($season) = $this->unique('season');
        $this->secondary['articles'] = HelpArticle::query()
            ->where([
                ['game', '=', $game],
                ['season', '=', sprintf('%04d', $season)],
                ['active', '=', 1]
            ])->whereIn('section_id', $this->unique('section_id'))
            ->orderBy('section_id')
            ->orderBy('section_order')
            ->get()
            ->setMultipleSecondary()
            ->hideFieldsFromAPI(['section_id']);
        // Now tie back together.
        foreach ($this->data as $i => $section) {
            $this->data[$i]['articles'] = $this->secondary['articles']->where('section_id', $section['section_id']);
        }
    }
}
