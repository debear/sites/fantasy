<?php

namespace DeBear\ORM\Fantasy\Common;

use DeBear\ORM\Base\Instance;
use DeBear\ORM\Skeleton\WeatherForecast;
use DeBear\Repositories\Time;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\Helpers\Fantasy\Common\Traits\SportTests as SportTestsTrait;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Carbon\Carbon;
use stdClass;

class GamePeriod extends Instance
{
    use SportTestsTrait;

    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'bool' => ['is_current'],
    ];
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['sport', 'season', 'sport_ref', 'summarise'];

    /**
     * Manipulate an entry for its API representation
     * @param array $row The row data (passed by reference) to be manipulated.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        // Remove unnecessary fields.
        if (!isset($row['name'])) {
            unset($row['name']);
            unset($row['name_short']);
        }
        if (!isset($row['icon_type'])) {
            unset($row['icon']);
            unset($row['icon_type']);
        }
    }

    /**
     * Render the game date / period's short name
     * @return string The game date / period short name in display form
     */
    public function getNameShortDispAttribute(): string
    {
        return $this->nameDisplayWorker($this->name_short, 'jS M');
    }

    /**
     * Render the game date / period's full name
     * @return string The game date / period full name in display form
     */
    public function getNameDispAttribute(): string
    {
        return $this->nameDisplayWorker($this->name, 'jS F Y');
    }

    /**
     * Worker method for rendering game date / period names
     * @param string|null $name     The bespoke name for this game period.
     * @param string      $date_fmt The format with which dates should be rendered.
     * @return string The game date / period name in display form
     */
    public function nameDisplayWorker(?string $name, string $date_fmt): string
    {
        if (!isset($name)) {
            // This is a date, not a bespoke name, so render that.
            return $this->end_date->format($date_fmt);
        }
        // Render a bespoke name.
        if (isset($this->icon)) {
            $icon_type = ($this->icon_type == 'flag' ? 'flag16' : 'icon');
            $icon_pos = ($this->icon_type == 'flag' ? '_right' : '');
            $name = "<span class=\"{$this->icon_type}{$icon_pos} {$icon_type}{$icon_pos}_{$this->icon}\">$name</span>";
        }
        return $name;
    }

    /**
     * Check whether the last event time has passed, and so the whole period locked, for the current period
     * @return boolean That the period as a whole is now locked
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsLockedAttribute(): bool
    {
        return isset($this->lock_time->system)
            && (Time::object()->getNowFmt() >= $this->lock_time->system->toDateTimeString());
    }

    /**
     * Determine the time at which the period as a whole locks (the last event time within its bounds)
     * @return stdClass Our display/system object representing the lock time in appropriate timzones
     */
    public function getLockTimeAttribute(): stdClass
    {
        if (!isset($this->lock_time_info)) {
            $db_tz = FrameworkConfig::get("debear.sports.subsites.{$this->sport}.datetime.timezone_db");
            $config = FrameworkConfig::get("debear.sports.{$this->sport}.period");
            $col_time = str_replace('TBL.', '', $config['lock_time']);
            $query = Namespaces::callStatic($this->sport, $config['class'], 'query')
                ->selectRaw("MAX($col_time) AS event_time")
                ->where('season', '=', $this->season);
            if ($this->isMajorLeague()) {
                // Major League games need an additional game_type clause.
                $query->where('game_type', 'regular');
            }
            $max_event = $query->where($config['sport_col'], '=', $this->{$config['ref_col']})
                ->get(['info-query' => true]);
            $this->lock_time_info = (object)[
                'display' => Carbon::createFromFormat('Y-m-d H:i:s', $max_event->event_time, $db_tz)
                    ->subMinutes(FrameworkConfig::get('debear.setup.lock_offset'))
                    ->setTimezone(Time::object()->getUserTimezone()),
                'system' => Carbon::createFromFormat('Y-m-d H:i:s', $max_event->event_time, $db_tz)
                    ->subMinutes(FrameworkConfig::get('debear.setup.lock_offset'))
                    ->setTimezone(Time::object()->getTimezone()),
            ];
        }
        return $this->lock_time_info;
    }

    /**
     * Get the list of teams that are taking part in this game period
     * @return array An array of team and opponent info for Major League game periods
     */
    public function getTeams(): array
    {
        $teams = [];
        if ($this->isMajorLeague()) {
            $db_tz = FrameworkConfig::get("debear.sports.subsites.{$this->sport}.datetime.timezone_db");
            $now = Time::object()->getAltTimezoneNowFmt($db_tz);
            $class = FrameworkConfig::get("debear.sports.{$this->sport}.period.class");
            $lock_offset = FrameworkConfig::get('debear.setup.lock_offset');
            $sched = Namespaces::callStatic($this->sport, $class, 'query')
                ->select('home', 'visitor')
                ->selectRaw('MIN(CONCAT(game_date, " ", game_time)) AS event_time')
                ->where([
                    ['season', '=', $this->season],
                    ['game_type', '=', 'regular'],
                ])
                ->whereRaw('game_date BETWEEN ? AND ?', [
                    $this->start_date->toDateString(),
                    $this->end_date->toDateString()
                ])->groupBy('season', 'game_type', 'home', 'visitor')
                ->havingRaw("DATE_SUB(event_time, INTERVAL ? MINUTE) > ?", [$lock_offset, $now])
                ->get(['info-query' => true]);
            foreach ($sched as $game) {
                $teams[$game->home] = ['venue' => 'v', 'opp_id' => $game->visitor];
                $teams[$game->visitor] = ['venue' => '@', 'opp_id' => $game->home];
            }
            ksort($teams);
        }
        return $teams;
    }

    /**
     * Determine the current game period
     * @param string  $sport  The sport being viewed.
     * @param integer $season The season being viewed.
     * @return self An ORM object representing the current game period
     */
    public static function getCurrentPeriod(string $sport, int $season): self
    {
        $app_tz = FrameworkConfig::get("debear.sports.subsites.$sport.datetime.timezone_app");
        $cur_date = Time::object()->getAltTimezoneCurDate($app_tz);
        $period = static::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['start_date', '<=', $cur_date],
                ['end_date', '>=', $cur_date],
            ])->get();
        // If we're not in the middle of the season, get the first / last date as appropriate.
        if (!$period->isset()) {
            $num_future = static::query()
                ->where([
                    ['sport', '=', $sport],
                    ['season', '=', $season],
                    ['start_date', '>', $cur_date],
                ])->count();
            $order_method = ($num_future ? 'orderBy' : 'orderByDesc');
            $period = static::query()
                ->where([
                    ['sport', '=', $sport],
                    ['season', '=', $season],
                ])->$order_method('start_date')
                ->limit(1)
                ->get();
        }
        return $period;
    }

    /**
     * Determine when the next locktime takes place within this period
     * @return Carbon|null The date/time for the next locktime within the period
     */
    public function getNextLockTime(): ?Carbon
    {
        $dt_cfg = FrameworkConfig::get("debear.sports.{$this->sport}.period");
        $col_time = str_replace('TBL.', '', $dt_cfg['lock_time']);
        $db_tz = FrameworkConfig::get("debear.sports.subsites.{$this->sport}.datetime.timezone_db");
        $now = Time::object()->getAltTimezoneNowFmt($db_tz);
        $next = Namespaces::callStatic($this->sport, $dt_cfg['class'], 'query')
            ->selectRaw("DATE_SUB($col_time, INTERVAL ? MINUTE) AS next_locktime", [
                FrameworkConfig::get('debear.setup.lock_offset'),
            ])->whereRaw("DATE($col_time) BETWEEN ? AND ?", [
                $this->start_date->toDateString(),
                $this->end_date->toDateString()
            ])->orderBy('next_locktime')
            ->havingRaw("next_locktime >= ?", [$now])
            ->limit(1)
            ->get(['info-query' => true]);
        return !isset($next->next_locktime) ? null
            : (new Carbon($next->next_locktime, $db_tz))->setTimezone(Time::object()->getTimezone());
    }

    /**
     * Load the weather info for the appropriate periods
     * @return void
     */
    protected function loadSecondaryWeather(): void
    {
        // This currently works on the expectation we are processing a motorsport as the initial use-case.
        list($sport) = $this->unique('sport');
        $unit = FrameworkConfig::get("debear.sports.$sport.weather.unit");
        $col_temp = ($unit == 'c' ? 'temp' : '(temp * (9 / 5)) + 32');
        $weather = WeatherForecast::query()
            ->select(['instance_key', 'symbol', 'summary', 'precipitation', 'wind_speed', 'wind_dir'])
            ->selectRaw("ROUND($col_temp) AS temp")
            ->selectRaw('REGEXP_REPLACE(instance_key, "^20[0-9]{2}-[^-]+-(.+)-[^-]+$", "$1") AS period_icon')
            ->selectRaw('SUBSTRING_INDEX(instance_key, "-", -1) AS period_event')
            ->where('app', '=', FrameworkConfig::get("debear.sports.subsites.$sport.weather.app"))
            ->where(function ($where) {
                foreach ($this->getData() as $row) {
                    $where->orWhere('instance_key', 'like', "{$row['season']}-{$row['sport']}-{$row['icon']}-%");
                }
            })->orderBy('forecast_date')->get();
        // Tie back together.
        foreach ($this->data as $i => $period) {
            $by_period = [];
            foreach ($weather->where('period_icon', $period['icon']) as $event) {
                $by_period[$event->period_event] = $weather->where('instance_key', $event->instance_key);
            }
            $this->data[$i]['weather'] = $by_period ? (object)$by_period : null;
        }
    }
}
