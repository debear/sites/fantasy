<?php

namespace DeBear\ORM\Fantasy\Common;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Fantasy\Common\Traits\SportTests as SportTestsTrait;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Selection extends Instance
{
    use SportTestsTrait;

    /**
     * Convert the name into a URL slug
     * @return string A slug representation of the name
     */
    public function getSlugAttribute(): string
    {
        return Strings::codifyURL($this->name) . "-{$this->link_id}";
    }

    /**
     * Produce a display-friendly version of the entity's name
     * @return string The entity's name in a display format
     */
    public function getDisplayNameAttribute(): string
    {
        if (!isset($this->name)) {
            return '<em>No Selection</em>';
        }
        $css = FrameworkConfig::get("debear.sports.{$this->sport}.{$this->type}.icon");
        return "<span class=\"{$css}{$this->icon}\">{$this->name}</span>";
    }

    /**
     * Produce a display-friendly version of the entity's status
     * @return string The entity's status in a display format
     */
    public function getDisplayStatusAttribute(): string
    {
        if (!isset($this->status)) {
            return '';
        }
        $method = 'displayStatus' . FrameworkConfig::get("debear.sports.{$this->sport}.namespace");
        return $this->$method();
    }

    /**
     * The Motorsport processing of selection statuses
     * @return string The Motorsport selection's status in HTML
     */
    protected function displayStatusMotorsport(): string
    {
        $css = "status-motors-{$this->status}";
        $config = FrameworkConfig::get("debear.sports.{$this->sport}.status.{$this->status}");
        return '<abbrev class="status ' . $css . '" title="' . $config['long'] . '">' . $config['short'] . '</abbrev>';
    }

    /**
     * The Major League processing of selection statuses
     * @return string The Major League selection's status in HTML
     */
    protected function displayStatusMajorLeague(): string
    {
        $config = FrameworkConfig::get("debear.sports.subsites.{$this->sport}.setup.players");
        if (isset($this->event_status) && $this->event_status != 'SCH') {
            // Schedule status.
            $abbrev = $this->event_status;
            $title = FrameworkConfig::get("debear.setup.event_statuses.$abbrev");
            $css = 'status-sched';
        } elseif (substr($this->status, 0, 4) == 'inj-') {
            // Injury designation.
            $inj = substr($this->status, 4);
            $title = $config['injuries'][$inj]['long'];
            $abbrev = $config['injuries'][$inj]['short'];
            $css = 'status-inj';
        } elseif (substr($this->status, 0, 4) == 'pos-') {
            // Position.
            $pos = substr($this->status, 4);
            if ($pos != 'dns') {
                $ord = Format::ordinal(intval($pos));
                $title = "Starting $ord";
                $abbrev = $ord;
                $css = 'status-pos-start';
            } else {
                // Not a starter.
                $title = 'Not in Starting Lineup';
                $abbrev = 'Bench';
                $css = 'status-pos-dns';
            }
        } else {
            // Roster status.
            $title = $config['status'][$this->status]['long'];
            $abbrev = $config['status'][$this->status]['short'];
            $css = 'status-roster';
        }
        return '<abbrev class="status ' . $css . '" title="' . $title . '">' . $abbrev . '</abbrev>';
    }

    /**
     * Produce a display-friendly version of the entity's info field
     * @return string The entity's info field in a display format
     */
    public function getDisplayInfoAttribute(): string
    {
        if (!isset($this->info)) {
            // Silently ignore no info.
            return '';
        }
        $css = FrameworkConfig::get("debear.sports.{$this->sport}.{$this->type}.icon");
        return $this->isMotorsport()
            ? "<span class=\"{$css}{$this->info_icon}\">{$this->info}</span>"
            : ("{$this->info}<span class=\"{$css}{$this->info_icon}\">{$this->info_icon}</span>"
                . ($this->info_dh ? ' (DH)' : ''));
    }

    /**
     * Return an appropriate mugshot / silhouette for this entity
     * @param array $opt Processing options.
     * @return string The URL to the mugshot / silhouette
     */
    public function imageURL(array $opt = []): string
    {
        // Some processing defaults.
        $opt['site'] = 'sports';
        $opt['sport'] = $this->sport;
        $opt['season'] = $this->season;
        // Get the on-disk path to the image (or some kind of fallback).
        if (isset($this->link_id)) {
            // Regular player mugshot / team logo.
            $img = $this->model->buildImagePath($this->team_id ?? $this->link_id, $opt);
        } else {
            // Silhouette / Logo for a non-selection.
            $class = FrameworkConfig::get("debear.sports.{$opt['sport']}.{$this->type}.class");
            $fullclass = Namespaces::fullClassName($opt['sport'], $class);
            $img = (new $fullclass())->buildImagePath(null, $opt);
        }
        // Return through the obfuscator.
        $config_key = 'debear.fantasy.subsites.records.cdn.mugshots';
        $config = FrameworkConfig::get($config_key) ?? FrameworkConfig::get('debear.cdn.mugshots');
        $size = $config[$opt['size'] ?? 'large'];
        $args = ['rs' => 1, 'i' => $img, 'w' => $size['w'], 'h' => $size['h']];
        if ($this->isMotorsport()) {
            // Motorsport images need cropping.
            $args['crop'] = 1;
            $args['offset'] = 'ct'; // Center-Top.
        } elseif ($this->type == 'team') {
            // Major League teams need their ratio preserved.
            $args['preserve'] = 1;
        }
        return HTTP::buildCDNURLs(http_build_query($args));
    }

    /**
     * Return the URL for the selection's DeBear Sports profile page
     * @return string The URL to the DeBear Sports profile page
     */
    public function profileURL(): string
    {
        return 'https:' . HTTP::buildDomain('sports') . "/{$this->sport}"
            . '/' . FrameworkConfig::get("debear.sports.{$this->sport}.{$this->type}.sports_path")
            . '/' . ($this->type == 'ind' ? $this->slug : Strings::codifyURL($this->name));
    }

    /**
     * Additional CSS to apply to the entry's image
     * @return string String of appropriate CSS attributes (not classes!)
     */
    public function imageCSS(): string
    {
        return ($this->type == 'team' && !isset($this->link_id) ? 'filter: grayscale(1);' : '');
    }

    /**
     * State whether the selection's event is active or not (postponed, cancelled, etc)
     * @return boolean
     */
    public function eventActive(): bool
    {
        return ($this->event_status == 'SCH');
    }

    /**
     * Convert the current selection row in to a summarised (not full) API object
     * @return array Array of summarised API selection information
     */
    public function rowToSummaryAPI(): array
    {
        return [
            'name' => $this->name,
            'icon' => $this->icon,
            'info' => $this->info ?: null,
            'info_icon' => $this->info_icon,
            'image' => $this->imageURL(),
        ];
    }

    /**
     * Convert the current selection row in to a summarised (not full) Web object
     * @param string|null $img_size An optional image size over-ride.
     * @return array Array of summarised Web selection information
     */
    public function rowToSummaryWeb(?string $img_size = null): array
    {
        return [
            'name' => $this->display_name,
            'status' => $this->display_status,
            'info' => $this->display_info ?: null,
            'image' => $this->imageURL(isset($img_size) ? ['size' => $img_size] : []),
            'slug' => $this->slug,
            'profile' => $this->profileURL(),
        ];
    }

    /**
     * Convert the database weather information into a web payload
     * @return array|null The resulting web payload
     */
    public function weatherToWeb(): ?array
    {
        // Skip if no weather info available.
        $status = ($this->weather_status ?? 'unset');
        return $status == 'unset' ? null : ($status == 'dome' ? ['symbol' => 'dome'] : [
            'symbol' => $this->weather_symbol,
            'summary' => $this->weather_summary,
            'temp' => $this->weather_temp . '&deg;',
            'precipitation' => sprintf('%d%%', round($this->weather_precip * 100)),
            'wind' => round($this->weather_windsp) . "mph {$this->weather_winddir}",
        ]);
    }

    /**
     * Convert the database weather information into an API payload
     * @return array|null The resulting API payload
     */
    public function weatherToAPI(): ?array
    {
        // Skip if no weather info available.
        $status = ($this->weather_status ?? 'unset');
        return $status == 'unset' ? null : ($status == 'dome' ? ['symbol' => 'dome'] : [
            'symbol' => $this->weather_symbol,
            'summary' => $this->weather_summary,
            'temp' => intval($this->weather_temp),
            'precipitation' => round($this->weather_precip * 100),
            'wind' => [
                'speed' => round($this->weather_windsp),
                'direction' => $this->weather_winddir,
            ],
        ]);
    }
}
