<?php

namespace DeBear\ORM\Fantasy\Common;

use DeBear\ORM\Base\Instance;

class HelpArticle extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['game', 'season', 'active'];
}
