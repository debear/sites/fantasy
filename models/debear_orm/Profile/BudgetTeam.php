<?php

namespace DeBear\ORM\Fantasy\Profile;

use Illuminate\Support\Facades\DB;

class BudgetTeam extends GenericTeam
{
    /**
     * Load Budget game team(s) for specific users
     * @param array $user_ids The ID(s) of users whose profile is/are being loaded.
     * @return GenericTeam An ORM object of budget game teams for the supplied user(s)
     */
    public static function load(array $user_ids): GenericTeam
    {
        $tbl_budget = static::getTable();
        $tbl_game = Game::getTable();
        return GenericTeam::query($tbl_budget)
            ->join($tbl_game, function ($join) use ($tbl_game, $tbl_budget) {
                $join->on("$tbl_game.game", '=', "$tbl_budget.game")
                    ->where("$tbl_game.game_type", '=', 'budget')
                    ->on("$tbl_game.season", '=', "$tbl_budget.season");
            })->select([
                "$tbl_budget.user_id", "$tbl_budget.season", "$tbl_budget.game", "$tbl_game.in_progress",
                "$tbl_budget.team_id", "$tbl_budget.name", "$tbl_budget.profile_points",
            ])->selectRaw("CONCAT($tbl_budget.game, '-', $tbl_budget.season, '-', $tbl_budget.team_id) AS team_ref")
            ->selectRaw('"budget" AS game_type')
            ->selectRaw("CONCAT('budget-', $tbl_budget.game) AS game_type_ref")
            ->whereIn("$tbl_budget.user_id", $user_ids)
            ->orderByDesc("$tbl_budget.season")
            ->orderBy("$tbl_budget.game")
            ->orderBy("$tbl_budget.team_id")
            ->get()
            ->setMultipleSecondary()
            ->hideFieldsFromAPI(['user_id', 'team_ref', 'game_type_ref'])
            ->loadSecondary(['details']);
    }

    /**
     * Load the expanded info for the team(s)
     * @param array $team_refs The team(s) to be processed.
     * @return self ORM objects for the requested team(s)
     */
    public static function loadDetail(array $team_refs): self
    {
        $tbl_team = static::getTable();
        $tbl_scoring = BudgetScoring::getTable();
        return static::query()
            ->join($tbl_scoring, function ($join) use ($tbl_scoring, $tbl_team) {
                $join->on("$tbl_scoring.game", '=', "$tbl_team.game")
                    ->on("$tbl_scoring.season", '=', "$tbl_team.season")
                    ->on("$tbl_scoring.combination", '=', "$tbl_team.combination");
            })->select("$tbl_team.*")
            ->selectRaw("IF($tbl_scoring.num_teams > 1,
                CAST(ROUND(100 - ((100 * ($tbl_team.rank - 1)) / ($tbl_scoring.num_teams - 1))) AS UNSIGNED),
                NULL) AS centile")
            ->selectRaw("CONCAT($tbl_team.game, '-', $tbl_team.season, '-', $tbl_team.team_id) AS team_ref")
            ->whereIn(DB::raw("CONCAT($tbl_team.game, '-', $tbl_team.season, '-', $tbl_team.team_id)"), $team_refs)
            ->get()
            ->hideFieldsFromAPI(['user_id', 'game', 'season', 'team_id', 'name', 'profile_points', 'team_ref'])
            ->loadSecondary(['groups', 'medals']);
    }

    /**
     * Load the groups the team(s) were part of
     * @return void
     */
    protected function loadSecondaryGroups(): void
    {
        $tbl_group_team = BudgetGroupTeam::getTable();
        $tbl_group = BudgetGroup::getTable();
        $this->secondary['groups'] = BudgetGroupTeam::query()
            ->join($tbl_group, function ($join) use ($tbl_group, $tbl_group_team) {
                $join->on("$tbl_group_team.game", '=', "$tbl_group.game")
                    ->on("$tbl_group_team.season", '=', "$tbl_group.season")
                    ->on("$tbl_group_team.group_id", '=', "$tbl_group.group_id");
            })->select(["$tbl_group.*", "$tbl_group_team.rank"])
            ->selectRaw(
                "CONCAT($tbl_group_team.game, '-', $tbl_group_team.season, '-', $tbl_group_team.team_id) AS team_ref"
            )->whereIn(
                DB::raw("CONCAT($tbl_group_team.game, '-', $tbl_group_team.season, '-', $tbl_group_team.team_id)"),
                $this->unique('team_ref')
            )->orderBy("$tbl_group_team.team_id")
            ->orderByRaw("$tbl_group.type = 'fanleague'") // This puts groups first (as groups == 0).
            ->orderBy("$tbl_group_team.rank")
            ->orderByDesc("$tbl_group.num_teams")
            ->orderBy("$tbl_group.name")
            ->get()
            ->setMultipleSecondary()
            ->hideFieldsFromAPI(['game', 'season', 'team_ref']);
        foreach ($this->data as $i => $team) {
            $this->data[$i]['groups'] = $this->secondary['groups']->where('team_ref', $team['team_ref']);
        }
    }

    /**
     * Determine the medals for each team
     * @return void
     */
    protected function loadSecondaryMedals(): void
    {
        foreach ($this->data as $i => $team) {
            $medals = [];
            // Overall medals.
            if ($team['rank'] == 1) {
                $medals[] = 'overall-winner';
            }
            if ($team['rank'] < 6) {
                $medals[] = 'top-5-finisher';
            }
            if ($team['rank'] < 31) {
                $medals[] = 'top-30-finisher';
            }
            // Group-level medals.
            foreach ($team['groups'] as $group) {
                if ($group->rank == 1) {
                    $medals[] = 'group-winner';
                }
                if ($group->rank < 4) {
                    $medals[] = 'group-top-3';
                }
            }
            // Bind back to the team.
            $this->data[$i]['medals'] = array_count_values($medals);
        }
    }
}
