<?php

namespace DeBear\ORM\Fantasy\Profile;

use DeBear\ORM\Base\Instance;
use Illuminate\Database\Query\Builder;

class RecordGameAchievement extends Instance
{
    /**
     * Load the full list of level info for the achievement(s) found
     * @return void
     */
    protected function loadSecondaryLevels(): void
    {
        $query = RecordGameAchievementLevel::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id, "-", achieve_id) AS achieve_ref');
        foreach ($this->data as $achieve) {
            $query->orWhere(function (Builder $where) use ($achieve) {
                $where->where([
                    ['sport', '=', $achieve['sport']],
                    ['season', '=', $achieve['season']],
                    ['game_id', '=', $achieve['game_id']],
                    ['achieve_id', '=', $achieve['achieve_id']],
                ]);
            });
        }
        $this->secondary['levels'] = $query->orderBy('achieve_id')->orderBy('disp_order')->get()
            ->setMultipleSecondary();
        // Now tie back together.
        foreach ($this->data as $i => $a) {
            $this->data[$i]['levels'] = $this->secondary['levels']
                ->where('achieve_ref', "{$a['sport']}-{$a['season']}-{$a['game_id']}-{$a['achieve_id']}");
        }
    }
}
