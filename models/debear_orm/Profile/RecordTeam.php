<?php

namespace DeBear\ORM\Fantasy\Profile;

use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as ScoringTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class RecordTeam extends GenericTeam
{
    use ScoringTrait;

    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'bool' => ['achieved'],
        'int' => ['centile'],
    ];

    /**
     * Load Record Breaker game entry(ies) for specific users
     * @param array $user_ids The ID(s) of users whose profile is/are being loaded.
     * @return GenericTeam An ORM object of Record Breaker game entries for the supplied user(s)
     */
    public static function load(array $user_ids): GenericTeam
    {
        $tbl_record = static::getTable();
        $tbl_game = RecordGame::getTable();
        return GenericTeam::query($tbl_record)
            ->join($tbl_game, function ($join) use ($tbl_game, $tbl_record) {
                $join->on("$tbl_game.sport", '=', "$tbl_record.sport")
                    ->on("$tbl_game.season", '=', "$tbl_record.season")
                    ->on("$tbl_game.game_id", '=', "$tbl_record.game_id");
            })->selectRaw("$tbl_record.user_id")
            ->selectRaw("$tbl_record.season")
            ->selectRaw('"records" AS game')
            ->selectRaw("$tbl_game.in_progress")
            ->selectRaw("$tbl_record.user_id AS team_id")
            ->selectRaw("CONCAT('<span class=\"icon records-sport-', $tbl_game.sport, '\">', $tbl_game.game_name, "
                . "'</span>') AS name")
            ->selectRaw("$tbl_record.profile_points")
            ->selectRaw("CONCAT($tbl_record.sport, '-', $tbl_record.season, '-', $tbl_record.game_id, '-', "
                . "$tbl_record.user_id) AS team_ref")
            ->selectRaw('"record" AS game_type')
            ->selectRaw('"records" AS game_type_ref')
            ->whereIn("$tbl_record.user_id", $user_ids)
            ->orderByDesc("$tbl_record.season")
            ->orderBy("$tbl_record.sport")
            ->orderBy("$tbl_game.game_name")
            ->get()
            ->setMultipleSecondary()
            ->hideFieldsFromAPI(['user_id', 'team_ref', 'game_type_ref'])
            ->loadSecondary(['details']);
    }

    /**
     * Load the expanded info for the team(s)
     * @param array $team_refs The team(s) to be processed.
     * @return self ORM objects for the requested team(s)
     */
    public static function loadDetail(array $team_refs): self
    {
        $tbl_team = static::getTable();
        $tbl_game = RecordGame::getTable();
        return static::query()
            ->join($tbl_game, function ($join) use ($tbl_game, $tbl_team) {
                $join->on("$tbl_game.sport", '=', "$tbl_team.sport")
                    ->on("$tbl_game.season", '=', "$tbl_team.season")
                    ->on("$tbl_game.game_id", '=', "$tbl_team.game_id");
            })->select("$tbl_team.*")
            ->selectRaw("IF($tbl_game.num_teams <= 1, NULL,
                ROUND(100 - ((100 * (CAST($tbl_team.rank AS UNSIGNED) - 1))
                    / (CAST($tbl_game.num_teams AS UNSIGNED) - 1)))) AS centile")
            ->selectRaw("CONCAT($tbl_team.sport, '-', $tbl_team.season, '-', $tbl_team.game_id, '-', "
                . "$tbl_team.user_id) AS team_ref")
            ->whereIn(DB::raw("CONCAT($tbl_team.sport, '-', $tbl_team.season, '-', $tbl_team.game_id, '-', "
                . "$tbl_team.user_id)"), $team_refs)
            ->get()
            ->loadSecondary(['achievements'])
            ->hideFieldsFromAPI(['user_id', 'season', 'profile_points', 'team_ref']);
    }

    /**
     * Load the full list of achievement info for the game(s) found
     * @return void
     */
    protected function loadSecondaryAchievements(): void
    {
        $game_ref = array_map(function ($t) {
            return array_intersect_key($t, ['sport' => true, 'season' => true, 'game_id' => true]);
        }, $this->getData());
        $query = RecordGameAchievement::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($game_ref as $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    ['sport', '=', $game['sport']],
                    ['season', '=', $game['season']],
                    ['game_id', '=', $game['game_id']],
                ]);
            });
        }
        $achievements = $query->orderBy('disp_order')->get()
            ->loadSecondary(['levels'])
            ->hideFieldsFromAPI(['sport', 'season', 'game_id', 'disp_order', 'game_ref']);
        // Now tie back together.
        foreach ($this->data as $i => $game) {
            $game_ref = "{$game['sport']}-{$game['season']}-{$game['game_id']}";
            $this->data[$i]['game_achievements'] = $achievements->where('game_ref', $game_ref);
        }
    }

    /**
     * Perform any final post-processing on the API data before sending back
     * @param array $row The rendered data for a single row to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        // Format the score appropriately.
        $row['score'] = $this->formatScore($row['score']);
        // Convert the achievements from a score to a list.
        if (!isset($row['achievements'])) {
            unset($row['achievements']);
        } else {
            $achieve_as_str = '0' . str_pad(strrev(decbin($row['achievements'] ?? 0)), 16, '0');
            $achieve_map = array_map('boolval', str_split($achieve_as_str));
            $row['achievements'] = $row['game_achievements']->resultsToAPI();
            foreach ($row['achievements'] as &$a) {
                $levels = [];
                foreach ($a['levels'] as $l) {
                    $levels[$l['level']] = $achieve_map[$l['bit_flag']];
                }
                $a['levels'] = $levels;
            }
        }
        // In all instances, we no longer need the game_achievements info.
        unset($row['game_achievements']);
    }
}
