<?php

namespace DeBear\ORM\Fantasy\Profile;

use DeBear\ORM\Base\Instance;

class GenericTeam extends Instance
{
    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'int' => ['season'],
        'bool' => ['in_progress'],
        'float' => ['profile_points'],
    ];

    /**
     * Load the expanded list of team info for the team(s) found
     * @return void
     */
    protected function loadSecondaryDetails(): void
    {
        // Get the info by game type.
        $by_type = [];
        foreach ($this->unique('game_type') as $type) {
            $class = __NAMESPACE__ . '\\' . ucfirst($type) . 'Team';
            $this->secondary['detail'] = $by_type[$type] = $class::loadDetail($this->where('game_type', $type)
                ->unique('team_ref'));
        }
        // Then tie-back to each team.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['detail'] = $by_type[$team['game_type']]->where('team_ref', $team['team_ref']);
        }
    }
}
