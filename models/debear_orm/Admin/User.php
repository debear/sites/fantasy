<?php

namespace DeBear\ORM\Fantasy\Admin;

use DeBear\ORM\Base\Instance;
use DeBear\ORM\Skeleton\Timezone;
use DeBear\ORM\Skeleton\Country;
use DeBear\Helpers\Fantasy\Admin\Traits\Users as UsersTrait;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class User extends Instance
{
    use UsersTrait;

    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        '1dp' => ['rating'],
    ];

    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * Load users via their numeric ID, without concern at this level of whether they are automated or not.
     * @param integer|array $ids  User IDs to be loaded.
     * @param array         $cols An optional array of columns to return in the dataset.
     * @return static A single ORM object with data from both automated users and not
     */
    public static function loadByNumericID(int|array $ids, array $cols = ['*']): static
    {
        $ids_as_array = (is_array($ids) ? $ids : [$ids]);
        // Split the IDs into those of automated users and those not.
        $ids_auto = array_filter($ids_as_array, function ($user_id) {
            return static::isAutomatedUser($user_id);
        });
        $ids_user = array_diff($ids_as_array, $ids_auto);
        // Load our two user objects and merge.
        $data_auto = AutomatedUser::query()->select($cols)->whereIn('id', $ids_auto)->get();
        $data_user = static::query()->select($cols)->whereIn('id', $ids_user)->get();
        return $data_user->merge($data_auto);
    }

    /**
     * Load users via their string ID, without concern at this level of whether they are automated or not.
     * @param string|array $ids  User IDs to be loaded.
     * @param array        $cols An optional array of columns to return in the dataset.
     * @return static A single ORM object with data from both automated users and not
     */
    public static function loadByStringID(string|array $ids, array $cols = ['*']): static
    {
        $ids_as_array = (is_array($ids) ? $ids : [$ids]);
        // As we cannot check the numeric ID range, load the users first.
        $data_user = static::query()->select($cols)->whereIn('user_id', $ids_as_array)->get();
        // Then determine those who failed to be loaded and load as automated user and merge in the results.
        $ids_auto = array_diff($ids_as_array, $data_user->unique('user_id'));
        $data_auto = AutomatedUser::query()->select($cols)->whereIn('user_id', $ids_auto)->get();
        return $data_user->merge($data_auto);
    }

    /**
     * Determine if the User ID passed is that of an automated user or not
     * @param integer $user_id The User ID to be analysed.
     * @return boolean That the supplied user is an automated user
     */
    public static function isAutomatedUser(int $user_id): bool
    {
        return ($user_id >= FrameworkConfig::get('debear.setup.automated_players.user_ids.min'))
            && ($user_id <= FrameworkConfig::get('debear.setup.automated_players.user_ids.max'));
    }

    /**
     * Get the profile info for the user(s) loaded
     * @param string $user_id The (string) User ID being loaded.
     * @return ?self An ORM object of the requested user with relevant Fantasy info, or null if user not found
     */
    public static function loadProfile(string $user_id): ?self
    {
        // Try and load the user.
        $tbl_user = static::getTable();
        $tbl_tz = Timezone::getTable();
        $tbl_country = Country::getTable();
        $user = static::query()
            ->select(["$tbl_user.id", "$tbl_user.user_id", "$tbl_user.display_name"])
            ->selectRaw("$tbl_country.name AS country")
            ->selectRaw("$tbl_country.code2 AS country_flag")
            ->join($tbl_tz, "$tbl_tz.timezone", '=', "$tbl_user.timezone")
            ->join($tbl_country, "$tbl_country.code2", '=', "$tbl_tz.code2")
            ->where([
                ["$tbl_user.user_id", '=', $user_id],
                ["$tbl_user.status", '=', 'Active'],
            ])->get();
        if (!$user->isset()) {
            return null;
        }
        // Then the relevant info for fantasy.
        $user->loadSecondary(['teams', 'medals']);
        // But if the user has no fantasy teams, they are not applicable for a profile.
        return $user->teams->isset() ? $user : null;
    }
}
