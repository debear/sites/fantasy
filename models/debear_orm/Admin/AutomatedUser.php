<?php

namespace DeBear\ORM\Fantasy\Admin;

use DeBear\ORM\Base\Instance;
use Faker as FakerModule;
use Nubs\RandomNameGenerator\Vgng as NameGeneratorModule;
use TeamNameGenerator\FakerProvider;
use DeBear\ORM\Skeleton\Country;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Fantasy\Admin\Traits\Users as UsersTrait;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class AutomatedUser extends Instance
{
    use UsersTrait;

    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['id'];
    /**
     * A cache of words to be used as user ID components
     * @var array
     */
    protected static $words = [];
    /**
     * A cache of used (in this batch) random numbers
     * @var array
     */
    protected static $uniqueRand = [];
    /**
     * A cache of weighted locales
     * @var array
     */
    protected static $locales = [];
    /**
     * A cache of weighted personalities
     * @var array
     */
    protected static $personalities = [];

    /**
     * Get the profile info for the user(s) loaded
     * @param string $user_id The (string) User ID being loaded.
     * @return ?self An ORM object of the requested user with relevant Fantasy info, or null if user not found
     */
    public static function loadProfile(string $user_id): ?self
    {
        // Try and load the user.
        $user = static::query()
            ->select(['id', 'user_id', 'display_name', 'locale'])
            ->where('user_id', $user_id)
            ->get();
        if (!$user->isset()) {
            return null;
        }
        // Because it woul be a cross-database query, get and add the country info from the locale.
        $country = Country::query()
            ->select(['name', 'code2'])
            ->where('code2', strtolower(substr($user->locale, 3)))
            ->get();
        $user->country = $country->name;
        $user->country_flag = $country->code2;
        // Then the relevant info for fantasy.
        $user->loadSecondary(['teams', 'medals']);
        // But if the user has no fantasy teams, they are not applicable for a profile.
        return $user->teams->isset() ? $user : null;
    }

    /**
     * Create the data for an automated user using the faker library
     * @param integer $id The ID to be assigned to this automated user.
     * @return array The array data to be used in record creation
     */
    public static function createWithFakerData(int $id): array
    {
        $locale = static::generateLocale();
        $faker = FakerModule\Factory::create($locale);
        $faker->addProvider(new FakerProvider($faker));
        $forename = $faker->firstName();
        $surname = $faker->lastName();
        // User ID to be a combination of factors (as Faker randomisation does not link to names generated later).
        $user_id = static::generateUserID();
        // Display name could be a combination of components.
        $display_name = static::generateDisplayName($forename, $surname, $user_id);
        // Now return these, along with the rest of the fields, as the array.
        return Strings::encodeInput([
            'id' => $id,
            'user_id' => $user_id,
            'forename' => $forename,
            'surname' => $surname,
            'display_name' => $display_name,
            'team_name' => rand(0, 100) < 70 ? $faker->teamName : (new NameGeneratorModule())->getName(),
            'locale' => $locale,
            'personality' => static::generatePersonality(),
            'risk_taking' => static::generateRiskTaking(),
            'interest_ahl' => static::generateSportInterest($locale, 'ahl'),
            'interest_nhl' => static::generateSportInterest($locale, 'nhl'),
            'interest_nfl' => static::generateSportInterest($locale, 'nfl'),
            'interest_mlb' => static::generateSportInterest($locale, 'mlb'),
            'interest_f1' => static::generateSportInterest($locale, 'f1'),
            'interest_sgp' => static::generateSportInterest($locale, 'sgp'),
        ]);
    }

    /**
     * Generate a random locale for this automated user
     * @return string The selected locale
     */
    protected static function generateLocale(): string
    {
        // On first run, build our ratio-d locales.
        if (!static::$locales) {
            $config = FrameworkConfig::get('debear.setup.automated_players.locales');
            $raw = array_combine(array_keys($config), array_column($config, 'weight'));
            foreach ($raw as $locale => $size) {
                static::$locales = array_merge(static::$locales, array_fill(0, $size, $locale));
            }
            shuffle(static::$locales);
        }
        return static::$locales[array_rand(static::$locales)];
    }

    /**
     * Generate a random, but unique, number
     * @return integer The unique random number
     */
    protected static function generateUniqueRandom(): int
    {
        for ($i = 0; !isset($rand) || isset(static::$uniqueRand[$rand]) || $i < 5; $i++) {
            $rand = rand(1000, 99999);
        }
        static::$uniqueRand[$rand] = true;
        return $rand;
    }

    /**
     * Select a User ID for this automated user
     * @return string The generated User ID
     */
    protected static function generateUserID(): string
    {
        if (!static::$words) {
            static::$words = array_unique(array_filter(explode(' ', strtolower(preg_replace(
                ['/[^a-z]/i', '/[ ]{2,}/'],
                ' ',
                FakerModule\Factory::create('en_US')->realText(500000)
            ))), function ($w) {
                return strlen($w) > 5;
            }));
            shuffle(static::$words);
        }
        return static::$words[array_rand(static::$words)] . static::generateUniqueRandom();
    }

    /**
     * Select a display name for this automated user
     * @param string $forename The automated user's first name.
     * @param string $surname  The automated user's surname.
     * @param string $user_id  The automated user's User ID.
     * @return string The selected display name
     */
    protected static function generateDisplayName(string $forename, string $surname, string $user_id): string
    {
        // Our possible options.
        $a = $forename;
        $b = "{$forename} {$surname}";
        $c = $user_id;
        // Three times the possibility of combined name, over firstname or user_id only.
        $ret = [$a, $b, $b, $b, $c];
        return $ret[array_rand($ret)];
    }

    /**
     * Select a personality for this automated user
     * @return string The selected personality
     */
    protected static function generatePersonality(): string
    {
        // On first run, build our ratio-d personalities.
        if (!static::$personalities) {
            foreach (FrameworkConfig::get('debear.setup.automated_players.personalities') as $personality => $size) {
                static::$personalities = array_merge(static::$personalities, array_fill(0, $size, $personality));
            }
            shuffle(static::$personalities);
        }
        return static::$personalities[array_rand(static::$personalities)];
    }

    /**
     * Select this automated user's desire for taking risks
     * @return integer The select risk taking as a percentage
     */
    protected static function generateRiskTaking(): int
    {
        // Exponent the random float to tend automated users away from risk.
        $base = rand(15, 20);
        return $base + (pow(rand() / getrandmax(), 1.5) * (100 - $base));
    }

    /**
     * Select an interest level in a sport for this automated user
     * @param string $locale The automated user's locale.
     * @param string $sport  The sport having the interest level generate.
     * @return integer An interest level (percentage) for the automated user
     */
    protected static function generateSportInterest(string $locale, string $sport): int
    {
        list($min, $max) = FrameworkConfig::get("debear.setup.automated_players.locales.$locale.$sport");
        return rand($min, $max);
    }
}
