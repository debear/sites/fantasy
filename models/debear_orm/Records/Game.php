<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Fantasy\Records\Traits\Game\Tests as GameTestsTrait;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Fantasy\Common\Traits\SportTests as SportTestsTrait;
use DeBear\Helpers\Fantasy\Entities;
use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as ScoringTrait;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Fantasy\Common\Announcement;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Common\SelectionMap;
use DeBear\ORM\Fantasy\Common\Selection as EntitySelection;
use DeBear\Repositories\Time;
use DeBear\Models\Skeleton\User;
use DeBear\Models\Skeleton\LockFileStamps;
use DeBear\Models\SysMon\Checks;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class Game extends Instance
{
    use GameTestsTrait;
    use ScoringTrait;
    use SportTestsTrait;

    /**
     * SET columns and their values
     * @var array
     */
    protected $sets = [
        'config_flags' => [
            'sel_required',
            'sel_single_use',
            'game_ends_on_negative',
            'hitrate_leaderboard',
            'streak_busters',
            'no_period_summary',
        ],
    ];
    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'int' => ['season', 'target'],
    ];
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['game_ref', 'active_id', 'active_score'];


    /**
     * Convert the score_type setting into a display version
     * @return string A display representation of the score_type setting
     */
    public function getScoreTypeDispAttribute(): string
    {
        return ($this->isScoreTypePeriods() ? 'total' : $this->score_type);
    }

    /**
     * Convert the name into a URL slug
     * @return string A slug representation of the name
     */
    public function getSlugAttribute(): string
    {
        return Strings::codifyURL($this->name);
    }

    /**
     * Build the URL for this game, excluding the section prefix
     * @return string The URL for this game within the Record Breakers section
     */
    public function getSectionURLAttribute(): string
    {
        return "/{$this->sport}/{$this->slug}-{$this->season}";
    }

    /**
     * Build the URL for this game, including the section prefix
     * @return string The full URL for this game
     */
    public function getFullURLAttribute(): string
    {
        return '/' . FrameworkConfig::get('debear.subsites.records.url') . $this->section_url;
    }

    /**
     * Determine the URL of the record holder(s) mugshot
     * @param string $size Configuration name of the requested size.
     * @return string The URL for the mugshot of the record holder(s)
     */
    public function recordHolderURL(string $size): string
    {
        $ext = $this->hasMultipleRecordHolders() ? 'gif' : 'png';
        $url = "/fantasy/records/holders/{$this->sport}-{$this->season}-{$this->game_id}.$ext";
        $config_key = 'debear.fantasy.subsites.records.cdn.holders';
        $config = FrameworkConfig::get($config_key) ?? FrameworkConfig::get('debear.cdn.holders');
        return HTTP::buildCDNURLs($url, array_merge([
            'rs' => !$this->hasMultipleRecordHolders(),
        ], $config[$size]));
    }

    /**
     * Get the current list of games we want to highlight on the homepage
     * @return array The list of games we consider to be the latest
     */
    public static function getHomepageGames(): array
    {
        $status_mapping = [
            'upcoming' => 'pre-season',
            'open' => 'active',
            'inprogress' => 'active',
            'closed' => 'active',
            'active' => 'active',
            'complete' => 'off-season',
        ];
        $entry = Entry::load();
        $ret = [];
        foreach (static::loadHighlighted() as $game) {
            $ret[] = [
                'sport' => $game->sport,
                'status' => $status_mapping[$game->status],
                'model' => $game->getCurrent(),
                'entry' => $entry,
            ];
        }
        return $ret;
    }

    /**
     * Get the list of games we should include within the sitemap.
     * @return array The list of games we consider appropriate for the sitemap
     */
    public static function getSitemapGames(): array
    {
        $tbl = static::getTable();
        $all_games = static::baseLoadQuery()
            ->where("$tbl.date_open", '<=', Time::object()->getNowFmt())
            ->orderByRaw("$tbl.season DESC")
            ->orderByRaw("$tbl.date_open")
            ->orderByRaw("$tbl.game_id")
            ->get();
        return array_filter([
            'active' => $all_games->whereIn('status', ['active', 'inprogress', 'closed']),
            'upcoming' => $all_games->where('status', 'open'),
            'archive' => $all_games->where('status', 'complete'),
        ], function ($model) {
            return $model?->isset();
        });
    }

    /**
     * Determine when the "last" and "current" game periods are for display
     * @param Entry|null $entry A possible entry for the user, null if nobody is logged in.
     * @return array An array containing ORM objects representing the "last" and "current" periods
     */
    public function getPeriods(?Entry $entry = null): array
    {
        $cache_key = $this->index() . '-' . $this->game_ref;
        if (isset($this->cached_periods) && isset($this->cached_periods[$cache_key])) {
            return $this->cached_periods[$cache_key];
        }
        // Get the total number of periods in our game and to be attached to the records.
        $total_num = GamePeriod::query()
            ->where([
                ['sport', '=', $this->sport],
                ['season', '=', $this->season],
            ])->count();
        // At the end of a game (which may have ended early for a specific user), display the last period only.
        $has_ended = ($this->isComplete())
            || (isset($entry) && !$this->canRestartGame() && !$entry->getGameScore($this)->isActive());
        // Some games may end early, so if the user is no longer able to play we need to pin to their final entry.
        if ($has_ended) {
            if ($this->isComplete()) {
                // When was the last scheduled period?
                $last_period = GamePeriod::query()
                    ->where([
                        ['sport', '=', $this->sport],
                        ['season', '=', $this->season],
                    ])->orderByDesc('period_order')
                    ->limit(1)
                    ->get();
            } else {
                // When did the user fail to progress?
                $last_period = $entry->getGameScore($this)->getLastSelectionPeriod($this);
            }
            // This becomes the last period.
            $period_col = ($last_period->isset() ? 'period_id' : 'period_order');
            $period_val = ($last_period->isset() ? $last_period->period_id : 1);
            $period_last = GamePeriod::query()
                ->select('*')
                ->selectRaw('? AS periods_total', [$total_num])
                ->where([
                    ['sport', '=', $this->sport],
                    ['season', '=', $this->season],
                    [$period_col, '=', $period_val],
                ])->get();
            // And future is an empty object.
            $period_curr = new GamePeriod();
        } else {
            // Otherwise, current (by date) are required.
            $period_curr = GamePeriod::getCurrentPeriod($this->sport, $this->season);
            $period_curr->periods_total = $total_num;
            $period_last = GamePeriod::query()
                ->select('*')
                ->selectRaw('? AS periods_total', [$total_num])
                ->where([
                    ['sport', '=', $this->sport],
                    ['season', '=', $this->season],
                    ['period_order', '=', $period_curr->period_order - 1],
                ])->get();
        }
        // Whether selections are available for these period(s).
        $sel_counts = Selection::query()
            ->select('period_id')
            ->selectRaw('COUNT(*) AS num_sel')
            ->where([
                    ['sport', '=', $this->sport],
                    ['season', '=', $this->season],
                    ['game_id', '=', $this->game_id],
            ])->whereIn('period_id', array_filter([$period_curr?->period_id, $period_last?->period_id]))
            ->groupBy('sport')
            ->groupBy('season')
            ->groupBy('game_id')
            ->groupBy('period_id')
            ->get();
        if ($period_curr->isset()) {
            $period_curr->sel_available = ($sel_counts->where('period_id', '=', $period_curr->period_id)->num_sel > 0);
            if (FrameworkConfig::get("debear.sports.{$this->sport}.weather.type") == 'period') {
                $period_curr->loadSecondary(['weather']);
            }
        }
        if ($period_last->isset()) {
            $period_last->sel_available = ($sel_counts->where('period_id', '=', $period_last->period_id)->num_sel > 0);
        }
        // And return.
        $this->cached_periods = array_merge($this->cached_periods ?? [], [
            $cache_key => ['curr' => $period_curr, 'last' => $period_last],
        ]);
        return $this->cached_periods[$cache_key];
    }

    /**
     * Perform any necessary configuration updates for the selected game
     * @return void
     */
    public function configure(): void
    {
        // Selection lock time.
        $key = "debear.sports.{$this->sport}.period.lock_time";
        if (is_array(FrameworkConfig::get($key))) {
            $lock_times = FrameworkConfig::get($key);
            FrameworkConfig::set([
                $key => $lock_times[$this->game_code],
                "{$key}_raw" => $lock_times,
            ]);
        }
    }

    /**
     * Undo any per-game configuration updates
     * @return void
     */
    public function revertConfigure(): void
    {
        $key = "debear.sports.{$this->sport}.period.lock_time";
        if (FrameworkConfig::get("{$key}_raw") !== null) {
            FrameworkConfig::set([
                $key => FrameworkConfig::get("{$key}_raw"),
                "{$key}_raw" => null,
            ]);
        }
    }

    /**
     * Load the summary stats for a period
     * @param array $periods The IDs of the periods being loaded.
     * @return GamePeriodSummary An ORM object containing the summary stats for the supplied period in this game
     */
    public function loadPeriodSummary(array $periods): GamePeriodSummary
    {
        return GamePeriodSummary::query()
            ->where([
                ['sport', '=', $this->sport],
                ['season', '=', $this->season],
                ['game_id', '=', $this->game_id],
            ])->whereIn('period_id', $periods)
                ->get()
                ->loadSecondary(['entities']);
    }

    /**
     * Perform any final post-processing on the API data before sending back
     * @param array $row The rendered data for a single row to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        // Leader object tidying.
        if (isset($row['active'])) {
            $row['active'] = [
                'name' => $row['active']->name,
                'icon' => $row['active']->icon,
                'score' => $this->formatScoreAPI($row['active_score']),
            ];
        }
        foreach (['overall' => 'overall_res', 'current' => 'current_res'] as $key => $col) {
            if ($key != 'current' || $this->hasCurrentLeaderboard()) {
                $row[$key] = [
                    'name' => $row[$key]->entry_name,
                    'user_id' => $row[$key]->user_id,
                    'score' => $this->formatScoreAPI($row[$key]->$col),
                ];
            } else {
                // Remove the not relevant 'current' leader.
                unset($row['current']);
            }
        }
    }

    /**
     * Provide a common set of column values when loading game(s) from the database
     * @return QueryBuilder The base database query from which our subsequent request can extend
     */
    protected static function baseLoadQuery(): QueryBuilder
    {
        $tbl = static::getTable();
        $tbl_entry = Entry::getTable();
        $tbl_score = EntryScore::getTable();
        $now = Time::object()->getNowFmt();
        return static::query()
            ->select("$tbl.*")
            ->selectRaw("replace(FORMAT($tbl.target, 1), '.0', '') AS target")
            ->selectRaw("replace(FORMAT($tbl.active_score, 1), '.0', '') AS active_score")
            ->selectRaw("CONCAT($tbl.sport, '-', $tbl.season, '-', $tbl.game_id) AS game_ref")
            ->selectRaw("IF(? >= $tbl.date_end, 'complete',
                IF($tbl_score.user_id IS NOT NULL, 'active',
                    IF(? >= $tbl.date_close, 'closed',
                        IF(? >= $tbl.date_start, 'inprogress',
                            IF(? >= $tbl.date_open, 'open', 'upcoming'))))) AS status", array_fill(0, 4, $now))
            ->leftJoin($tbl_entry, function ($join) use ($tbl_entry) {
                $join->where("$tbl_entry.user_id", '=', User::object()->id ?? 0);
            })->leftJoin($tbl_score, function ($join) use ($tbl, $tbl_score, $tbl_entry) {
                $join->on("$tbl_score.sport", '=', "$tbl.sport")
                    ->on("$tbl_score.season", '=', "$tbl.season")
                    ->on("$tbl_score.game_id", '=', "$tbl.game_id")
                    ->on("$tbl_score.user_id", '=', "$tbl_entry.user_id");
            });
    }

    /**
     * Load the list of games to be presented
     * @param boolean $exclude_offseason Optional flag to exclude from the results games that are out of season.
     * @return self The ORM object of games to be displayed
     */
    public static function loadHighlighted(bool $exclude_offseason = false): self
    {
        // Number of days before a game opens that we preview it.
        $key = 'games.upcoming_days';
        $upcoming_days = FrameworkConfig::get("debear.$key")
            ?? FrameworkConfig::get("debear.fantasy.subsites.records.$key");
        // Get the latest games for each sporting season.
        $now = Time::object()->getNowFmt();
        $max_seasons = static::query()
            ->select('sport')
            ->selectRaw('MAX(season) AS season')
            ->whereRaw('DATE_SUB(date_open, INTERVAL ? DAY) <= ?', [$upcoming_days, $now])
            ->groupBy('sport')
            ->get();
        // And now the games within those seasons.
        $tbl = static::getTable();
        $query = static::baseLoadQuery()->where(function ($where) use ($max_seasons, $tbl) {
            foreach ($max_seasons as $sport) {
                $where->orWhere(function ($subwhere) use ($sport, $tbl) {
                    $subwhere->where("$tbl.sport", '=', $sport->sport)
                        ->where("$tbl.season", '=', $sport->season);
                });
            }
        });
        if ($exclude_offseason) {
            $query->where("$tbl.date_complete", '>', $now);
        }
        return $query->orderBy("$tbl.date_complete")
            ->get()
            ->loadSecondary(['leaders', 'news']);
    }

    /**
     * Load the currently active list of games
     * @return self A new (not subset) ORM object of games we consider currently active
     */
    public static function loadCurrent(): self
    {
        return static::loadHighlighted(true);
    }

    /**
     * Load the list of games we have had available on the platform
     * @return self An ORM object of all games that are, or have at some point, been active
     */
    public static function loadAllOpen(): self
    {
        $now = Time::object()->getNowFmt();
        $tbl = static::getTable();
        return static::baseLoadQuery()
            ->where("$tbl.date_open", '<=', $now)
            ->orderBy("$tbl.date_complete")
            ->get()
            ->loadSecondary(['leaders', 'news']);
    }

    /**
     * Load the list of current games in a specific status
     * @param string|array $status The status (where the game is between its relative date fields) to be loaded.
     * @return self The ORM object of games in the applicable status
     */
    public static function loadByStatus(string|array $status): self
    {
        return static::loadHighlighted()->whereIn('status', is_string($status) ? [$status] : $status);
    }

    /**
     * Parse the games and group by their availability status
     * @return array An array of loaded games, grouped by status
     */
    public function groupByStatus(): array
    {
        $ret = array_fill_keys(['active', 'open', 'inprogress', 'upcoming', 'closed', 'complete'], []);
        foreach ($this->unique('status') as $status) {
            $ret[$status] = $this->where('status', $status);
        }
        return array_filter($ret);
    }

    /**
     * Load a Game object for the given game reference
     * @param string  $sport   The sport of the game being loaded.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being loaded.
     * @return self The ORM object of the game being referenced
     */
    public static function loadByGameRef(string $sport, int $season, int $game_id): self
    {
        $tbl = static::getTable();
        // Load the base game.
        $game = static::baseLoadQuery()
            ->where([
                "$tbl.sport" => $sport,
                "$tbl.season" => $season,
                "$tbl.game_id" => $game_id,
            ])->get();
        // Perform any per-game customisations, before we proceed with loading all the secondaries.
        $game->where('game_id', $game_id)->configure();
        // Finally return with the default secondaries.
        return $game->loadSecondary(['stats', 'leaders', 'news']);
    }

    /**
     * Load a Game object for the given display slug
     * @param string  $sport  The sport of the game being loaded.
     * @param integer $season The season in which the game is taking place.
     * @param string  $slug   The slug of the name of the game being loaded.
     * @return self The ORM object of the game being referenced
     */
    public static function loadBySlug(string $sport, int $season, string $slug): self
    {
        // Determine an ID based on the slug.
        $now = Time::object()->getNowFmt();
        $games = static::query()->where([
            ['sport', '=', $sport],
            ['season', '=', $season],
            ['date_open', '<=', $now],
        ])->get();
        $game_id = -1;
        foreach ($games as $g) {
            $game_id = ($g->slug == $slug ? $games->game_id : $game_id);
        }
        // And now load it.
        return static::loadByGameRef($sport, $season, $game_id);
    }

    /**
     * Load info about the current game leaders
     * @return void
     */
    protected function loadSecondaryLeaders(): void
    {
        // Prep our expanded leader query.
        $tbl_score = EntryScore::getTable();
        $tbl_entry = Entry::getTable();
        // Get the data.
        $leaders = $entries = [];
        foreach ($this->unique('season') as $season) {
            $by_season = $this->where('season', $season);
            $leaders[$season] = $entries[$season] = [];
            foreach ($by_season->unique('sport') as $sport) {
                $by_sport = $by_season->where('sport', $sport);

                // Active leader, broken down by lock_time column.
                $leaders_by_lock_time = [];
                $cfg_key = "debear.sports.$sport.period.lock_time";
                $cfg_raw = FrameworkConfig::get($cfg_key);
                $cfg = (is_array($cfg_raw) ? $cfg_raw : array_fill_keys($by_sport->pluck('game_code'), $cfg_raw));
                foreach (array_values(array_unique($cfg)) as $lock_time) {
                    // Get the games for this lock time.
                    $game_codes = array_keys(array_filter($cfg, function ($val) use ($lock_time) {
                        return $val == $lock_time;
                    }));
                    $by_lock_time = $by_sport->whereIn('game_code', $game_codes);
                    FrameworkConfig::set([$cfg_key => $lock_time]);

                    // Process the leaders.
                    $period_key = FrameworkConfig::get("debear.sports.$sport.period.ref_col");
                    $period = GamePeriod::getCurrentPeriod($sport, $season)->{$period_key};
                    if ($period instanceof Carbon) {
                        $period = $period->toDateString();
                    }
                    $active_leaders = array_fill_keys(array_filter($by_lock_time->unique('active_id')), $period);
                    $leaders_by_lock_time[] = Entities::load($sport, $season, $active_leaders);
                    // Reset the temporary config override.
                    FrameworkConfig::set([$cfg_key => $cfg_raw]);
                }
                // Merge into a single object.
                $leaders[$season][$sport] = new EntitySelection();
                foreach ($leaders_by_lock_time as $data) {
                    $leaders[$season][$sport]->merge($data);
                }

                // Game leaders.
                $entries[$season][$sport] = EntryScore::query()
                    ->select("$tbl_score.*")
                    ->selectRaw("$tbl_entry.name AS entry_name")
                    ->selectRaw("replace(FORMAT($tbl_score.overall_res, 1), '.0', '') AS overall_res")
                    ->selectRaw("replace(FORMAT($tbl_score.current_res, 1), '.0', '') AS current_res")
                    ->join($tbl_entry, "$tbl_entry.user_id", '=', "$tbl_score.user_id")
                    ->where([
                        "$tbl_score.sport" => $sport,
                        "$tbl_score.season" => $season,
                    ])->whereIn("$tbl_score.game_id", $by_sport->unique('game_id'))
                    ->whereRaw("1 IN ($tbl_score.overall_pos, $tbl_score.current_pos)")
                    ->get();
            }
        }
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['active'] = $leaders[$g['season']][$g['sport']]->where('link_id', $g['active_id'] ?? -1);
            $game_entries = $entries[$g['season']][$g['sport']]->where('game_id', $g['game_id']);
            $this->data[$i]['overall'] = $game_entries->where('overall_pos', 1);
            $this->data[$i]['current'] = $game_entries->where('current_pos', 1);
        }
    }

    /**
     * Load info about the current game announcements
     * @return void
     */
    protected function loadSecondaryNews(): void
    {
        $this->secondary['news'] = $news = Announcement::load('records', 0000, $this->unique('game_ref'))
            ->setMultipleSecondary();
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['news'] = $news->where('section', "{$g['sport']}-{$g['season']}-{$g['game_id']}");
        }
    }

    /**
     * Load the latest meta information for the current games
     * @return void
     */
    protected function loadSecondaryMeta(): void
    {
        // Build the query to get the info.
        $query = GameMeta::query()
            ->select(['*'])
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($this->data as $i => $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    ['sport', '=', $game['sport']],
                    ['season', '=', $game['season']],
                    ['game_id', '=', $game['game_id']],
                    ['is_latest', '=', 1],
                ]);
            });
        }
        $meta = $query->get();
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['meta'] = $meta->where('game_ref', "{$g['sport']}-{$g['season']}-{$g['game_id']}");
        }
    }

    /**
     * Load the rules for the current games
     * @return void
     */
    protected function loadSecondaryRules(): void
    {
        // Build the query to get the info.
        $query = GameRule::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($this->data as $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    ['sport', '=', $game['sport']],
                    ['season', '=', $game['season']],
                    ['game_id', '=', $game['game_id']],
                ]);
            });
        }
        $rules = $query->get();
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['rules'] = $rules->where('game_ref', "{$g['sport']}-{$g['season']}-{$g['game_id']}");
        }
    }

    /**
     * Load the achievements for the current games
     * @return void
     */
    protected function loadSecondaryAchievements(): void
    {
        $query = GameAchievement::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($this->data as $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    ['sport', '=', $game['sport']],
                    ['season', '=', $game['season']],
                    ['game_id', '=', $game['game_id']],
                ]);
            });
        }
        $achievements = $query->orderBy('disp_order')->get()->loadSecondary(['levels']);
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['achievements'] = $achievements
                ->where('game_ref', "{$g['sport']}-{$g['season']}-{$g['game_id']}");
        }

        // For the games that feature 'opponent' achievements, we also need the opponent lists.
        $achieve_opp = $achievements->where('type', 'opponent');
        if (!$achieve_opp->count()) {
            // If no achievements found, no need to continue.
            return;
        }
        $sport_refs = array_values(array_unique(array_map(function ($a) {
            return array_intersect_key($a, ['sport' => true, 'season' => true]);
        }, $achieve_opp->getData()), SORT_REGULAR));
        $query = SelectionMap::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season) AS sport_ref');
        foreach ($sport_refs as $i => $sport) {
            $query->orWhere(function (Builder $where) use ($sport) {
                $where->where([
                    ['sport', '=', $sport['sport']],
                    ['season', '=', $sport['season']],
                ]);
            });
        }
        $opp = $query->get();
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            if ($g['achievements']->where('type', 'opponent')->count()) {
                $this->data[$i]['opponents'] = $opp->where('sport_ref', "{$g['sport']}-{$g['season']}");
            }
        }
    }

    /**
     * Load the periods for the current games
     * @return void
     */
    protected function loadSecondaryPeriods(): void
    {
        // Build the query to get the info.
        $cur_date = Time::object()->getCurDate();
        $query = GamePeriod::query()
            ->select('*')
            ->selectRaw('? BETWEEN start_date AND end_date AS is_current', [$cur_date])
            ->selectRaw('CONCAT(sport, "-", season) AS sport_ref');
        foreach ($this->data as $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    ['sport', '=', $game['sport']],
                    ['season', '=', $game['season']],
                ]);
            });
        }
        $rules = $query->orderBy('sport')->orderBy('season')->orderBy('period_order')->get();
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['periods'] = $rules->where('sport_ref', "{$g['sport']}-{$g['season']}");
        }
    }

    /**
     * Load the current game leaderboards (more than just the team in first)
     * @return void
     */
    protected function loadSecondaryLeaderboard(): void
    {
        $this->secondary['leaders'] = $leaders = [];
        $tbl_entry = Entry::getTable();
        $tbl_score = EntryScore::getTable();
        $hitrate_bin = "LPAD(BIN($tbl_score.hitrate_num), 32, '0')";
        $refs = $this->unique('game_ref');
        // Get our data.
        $types = array_filter([
            'overall' => 'overall_pos',
            'current' => $this->hasCurrentLeaderboard() ? 'current_pos' : false,
            'hitrate' => $this->hasHitRateLeaderboard() ? 'hitrate_pos' : false,
        ]);
        foreach ($types as $key => $col) {
            $this->secondary['leaders'][$key] = $leaders[$key] = EntryScore::query()
                ->select(["$tbl_score.*"])
                ->selectRaw("CONCAT($tbl_score.sport, '-', $tbl_score.season, '-', $tbl_score.game_id) AS game_ref")
                ->selectRaw("$tbl_entry.name AS entry_name")
                ->selectRaw("CONV(SUBSTRING($hitrate_bin, 1, 8), 2, 10) AS hitrate_num_pos")
                ->selectRaw("CONV(SUBSTRING($hitrate_bin, 9, 8), 2, 10) AS hitrate_num_neu")
                ->selectRaw("CONV(SUBSTRING($hitrate_bin, 17, 8), 2, 10) AS hitrate_num_neg")
                ->selectRaw("CONV(SUBSTRING($hitrate_bin, 25, 8), 2, 10) AS hitrate_num_na")
                ->selectRaw("replace(FORMAT($tbl_score.overall_res, 1), '.0', '') AS overall_res")
                ->selectRaw("replace(FORMAT($tbl_score.current_res, 1), '.0', '') AS current_res")
                ->join($tbl_entry, "$tbl_entry.user_id", '=', "$tbl_score.user_id")
                ->whereIn(DB::raw("CONCAT($tbl_score.sport, '-', $tbl_score.season, '-', $tbl_score.game_id)"), $refs)
                ->where("$tbl_score.$col", '<=', 10)
                ->orderBy("$tbl_score.sport")
                ->orderBy("$tbl_score.season")
                ->orderBy("$tbl_score.game_id")
                ->orderBy("$tbl_score.$col")
                ->orderByRaw("$tbl_score.user_id = ? DESC", [User::object()->id ?? 0])
                ->orderBy("$tbl_entry.name")
                ->get()
                ->setChangesSinceLastPeriod()
                ->hideFieldsFromAPI(['sport', 'season', 'game_id', 'hitrate_num']);
        }
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['leaderboards'] = [];
            foreach ($types as $key => $col) {
                $this->data[$i]['leaderboards'][$key] = $leaders[$key]->where('game_ref', $g['game_ref']);
            }
        }
    }

    /**
     * Load the current game leaderboards (more than just the team in first)
     * @return void
     */
    protected function loadSecondaryLeaderboardWithUser(): void
    {
        // Start with our principle loader.
        $this->loadSecondaryLeaderboard();
        // Start off with an empty object (to ensure at least _an_ object is returned).
        $this->secondary['leaders']['user'] = $user_scores = (new EntryScore());
        // And now parse to determine if we need to load the user's entry, and if so load it.
        if (User::object()->isLoggedIn()) {
            $user_id = User::object()->id;
            $user_in_overall = $this->secondary['leaders']['overall']->where('user_id', $user_id)->isset();
            $user_in_current = $this->hasCurrentLeaderboard()
                ? $this->secondary['leaders']['current']->where('user_id', $user_id)->isset() : false;
            if (!$user_in_overall || !$user_in_current) {
                $tbl_entry = Entry::getTable();
                $tbl_score = EntryScore::getTable();
                $hitrate_bin = "LPAD(BIN($tbl_score.hitrate_num), 32, '0')";
                $r = $this->unique('game_ref');
                $this->secondary['leaders']['user'] = $user_scores = EntryScore::query()
                    ->select(["$tbl_score.*"])
                    ->selectRaw("CONCAT($tbl_score.sport, '-', $tbl_score.season, '-', $tbl_score.game_id) AS game_ref")
                    ->selectRaw("$tbl_entry.name AS entry_name")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 1, 8), 2, 10) AS hitrate_num_pos")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 9, 8), 2, 10) AS hitrate_num_neu")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 17, 8), 2, 10) AS hitrate_num_neg")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 25, 8), 2, 10) AS hitrate_num_na")
                    ->selectRaw("replace(FORMAT($tbl_score.overall_res, 1), '.0', '') AS overall_res")
                    ->selectRaw("replace(FORMAT($tbl_score.current_res, 1), '.0', '') AS current_res")
                    ->join($tbl_entry, "$tbl_entry.user_id", '=', "$tbl_score.user_id")
                    ->where("$tbl_score.user_id", $user_id)
                    ->whereIn(DB::raw("CONCAT($tbl_score.sport, '-', $tbl_score.season, '-', $tbl_score.game_id)"), $r)
                    ->get()
                    ->setChangesSinceLastPeriod()
                    ->hideFieldsFromAPI(['sport', 'season', 'game_id', 'hitrate_num']);
            }
        }
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['leaderboards']['user'] = $user_scores->where('game_ref', $g['game_ref']);
        }
    }

    /**
     * Load the selections made by the entries in our leaderboard secondaries for the given period
     * @param integer $period_id The period for which entry selections are to be loaded.
     * @return void
     */
    public function loadLeaderboardSelections(int $period_id): void
    {
        foreach (array_keys($this->secondary['leaders']) as $type) {
            $this->secondary['leaders'][$type]->loadPeriodSelections($period_id);
        }
    }

    /**
     * Load the ORM object containing the current (advertised) integration status
     * @return void
     */
    protected function loadSecondaryIntegrationStatus(): void
    {
        $statuses = $updated = [];
        // Get the info.
        $sports = array_map(function ($sport) {
            return "sports_$sport";
        }, $this->unique('sport'));
        $checks = Checks::whereIn('instance', $sports)->get();
        $lockfiles = LockFileStamps::whereIn('app', $sports)->get();
        // Parse by sport.
        foreach ($this->unique('sport') as $sport) {
            $check = $checks->where('instance', "sports_$sport")->first();
            $lockfile = $lockfiles->where('app', "sports_$sport")->first();
            // Clean the data.
            $check_status = $check?->last_status ?? 'okay';
            $check_last = $check?->last_checked?->timestamp ?? -1;
            $lockfile_start = $lockfile?->last_started?->timestamp ?? -1;
            $lockfile_last = $updated[$sport] = $lockfile?->last_finished?->timestamp ?? -1;
            // Analyse the data to confirm the current status.
            $check_status = (!in_array($check_status, ['warn', 'critical']) ? 'okay' : $check_status);
            $statuses[$sport] = ($lockfile_last >= min($lockfile_start, $check_last) ? 'okay' : $check_status);
        }
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['integration_status'] = $statuses[$g['sport']];
            $this->data[$i]['integration_updated'] = $updated[$g['sport']];
        }
    }

    /**
     * Convert the data for one of the leaderboards into a web-ready payload
     * @param string $type The leaderboard type to render.
     * @return array The leaderboard data in the object converted to an array
     */
    public function leaderboardsToWeb(string $type): array
    {
        return [
            'list' => $this->leaderboards[$type]->leaderboardsToWeb($type),
            'user' => $this->leaderboards['user']->leaderboardToWeb($type),
        ];
    }

    /**
     * Load the stats for a game
     * @return void
     */
    protected function loadSecondaryStats(): void
    {
        // Build the query to get the info.
        $query = GameStat::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($this->data as $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    ['sport', '=', $game['sport']],
                    ['season', '=', $game['season']],
                    ['game_id', '=', $game['game_id']],
                ]);
            });
        }
        $rules = $query->orderBy('sport')->orderBy('season')->orderBy('disp_order')->get();
        // Now tie back together.
        foreach ($this->data as $i => $g) {
            $this->data[$i]['stats'] = $rules->where('game_ref', "{$g['sport']}-{$g['season']}-{$g['game_id']}");
        }
    }

    /**
     * Parse the possible progress values that could be rendered for a game
     * @param Entry|null      $entry  An optional ORM object of a user's entry to include in processing.
     * @param GamePeriod|null $period An optional ORM object of the current period being processed.
     * @return array An ordered array of appropriate stats to render
     */
    public function generateProgressBars(?Entry $entry = null, ?GamePeriod $period = null): array
    {
        // Some initial setup info.
        $sport_name = FrameworkConfig::get("debear.sports.{$this->sport}.name_short");
        $game_over = ($this->isScoreTypeStreak() && !$this->canRestartGame() && isset($period) && !$period->isset());
        // Determine which progress bars apply to our use-case, and in which order.
        $progress = [
            // Final target.
            [
                'type' => 'target',
                'name' => 'Target',
                'value' => $this->target ?? 0,
                'order' => 10,
                'required' => true,
            ],
            // Best score recorded by any team.
            [
                'type' => 'overall',
                'name' => 'Overall Leader',
                'value' => $this->overall->overall_res ?? 0,
                'order' => 30,
                // Do not include at the start of the season, but ensure always visible once it has begun.
                'required' => isset($this->overall->overall_pos),
            ],
            // Best current score recorded by any team.
            [
                'type' => 'leader',
                'name' => 'Current Leader',
                'value' => $this->current->current_res ?? 0,
                'order' => 40,
                'required' => false,
                'skip' => $this->isComplete(),
            ],
            // Best score recorded by the user's team.
            [
                'type' => 'best',
                'name' => 'Your Best',
                'value' => $entry?->score->overall_res ?? 0,
                'order' => 50,
                'required' => $game_over || $this->isComplete(),
                'skip' => !isset($entry) || !$entry->isset() || !isset($entry->score) || !$entry->score->isset(),
            ],
            // Current score recorded by the user's team. .
            [
                'type' => 'current',
                'name' => 'Current ' . ucfirst($this->score_type_disp),
                'value' => $entry?->score->current_res ?? 0,
                'order' => 20,
                'required' => isset($entry) && isset($entry?->score->overall_res),
                'skip' => !isset($entry) || !isset($entry?->score->overall_res) || $game_over || $this->isComplete(),
            ],
            // Sport's active leader.
            [
                'type' => 'active',
                'name' => "{$sport_name} Leader",
                'value' => $this->active_score ?? 0,
                'order' => 60,
                'required' => true,
            ],
        ];
        array_walk($progress, function (&$a) {
            $a['value_sort'] = floatval(str_replace(',', '', $a['value']));
        });
        usort($progress, function ($a, $b) {
            if ($a['value_sort'] != $b['value_sort']) {
                return 1 - ($a['value_sort'] <=> $b['value_sort']); // Descending sort.
            }
            return ($a['order'] <=> $b['order']);
        });
        $progress = array_filter($progress, function ($a, $k) use ($progress) {
            return (!$k || $a['required'] || $a['value'] != $progress[$k - 1]['value'])
                && (!isset($a['skip']) || !$a['skip']);
        }, ARRAY_FILTER_USE_BOTH);
        // Calculate the width as a percentage.
        $target = static::formatProgressBarValue($this->target);
        foreach ($progress as $i => $bar) {
            $value = static::formatProgressBarValue($bar['value']);
            $progress[$i]['width'] = max(0, min(100, (100 * ($value / $target)))); // Ensure in range of 0..100.
        }
        // Now return.
        return $progress;
    }

    /**
     * Convert the value of a progress bar field to a display version
     * @param string $value The raw value to be converted.
     * @return integer|float Prettified value having been converted
     */
    protected static function formatProgressBarValue(string $value): int|float
    {
        $method = (strpos($value, '.') === false) ? 'intval' : 'floatval';
        return $method(str_replace(',', '', $value ?? 0));
    }
}
