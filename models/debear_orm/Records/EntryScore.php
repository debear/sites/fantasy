<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Fantasy\Records\Traits\API\EntryAchievement as EntryAchievementTrait;
use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as ScoringTrait;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Admin\User as UserORM;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Strings;
use DeBear\Repositories\Time;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;

class EntryScore extends Instance
{
    use EntryAchievementTrait;
    use ScoringTrait;

    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['game_ref', 'processed', 'achievements'];
    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'int' => ['hitrate_num_pos', 'hitrate_num_neg', 'hitrate_num_neu', 'hitrate_num_na'],
    ];

    /**
     * Convert the name into a URL slug
     * @return string A slug representation of the name
     */
    public function getSlugAttribute(): string
    {
        return Strings::codifyURL($this->entry_name) . "-{$this->user_id}";
    }

    /**
     * Check whether the user is still able to play this game
     * @return boolean That the user can still participate
     */
    public function isActive(): bool
    {
        return !isset($this->overall_pos) || ($this->current_res > 0);
    }

    /**
     * Determine the display icon to use for an entry's change in standings position
     * @param string $type The type of standings being displayed.
     * @return string The CSS class to use
     */
    public function posChangeIcon(string $type): string
    {
        $k = "{$type}_chg";
        $base = 'icon_right icon_right_pos_';
        return $base . (!isset($this->$k) ? 'new' : (!$this->$k ? 'nc' : ($this->$k > 0 ? 'up' : 'down')));
    }

    /**
     * Determine the CSS class to use for the row in a table
     * @param string       $col The column of the position being parsed.
     * @param integer|null $i   The optional fallback line number this row is being rendered in.
     * @return string The CSS class to use
     */
    public function rowCSS(string $col, ?int $i = null): string
    {
        $pos = $this->$col;
        if ($pos < 4) {
            // Entry is in a podium position.
            return "podium-{$pos}";
        } elseif ($this->user_id == UserModel::object()->id) {
            // Entry belongs to the current user.
            return 'row-user';
        }
        // Fall back to a generic even/odd shading.
        return 'row-' . (($i ?? $pos) % 2);
    }

    /**
     * Determine the last period in which a team made a selection
     * @param Game $game The game being interrogated.
     * @return GamePeriod An ORM object for the period in question
     */
    public function getLastSelectionPeriod(Game $game): GamePeriod
    {
        // Get the period of the last selection.
        $tbl_es = EntryByPeriod::getTable();
        $tbl_gp = GamePeriod::getTable();
        $tbl_sel = Selection::getTable();
        $sel_required = intval($game->isSelectionRequired());
        return GamePeriod::query()
            ->select("$tbl_gp.*")
            ->join($tbl_es, function ($join) use ($tbl_es, $tbl_gp, $game) {
                $join->on("$tbl_es.sport", '=', "$tbl_gp.sport")
                    ->on("$tbl_es.season", '=', "$tbl_gp.season")
                    ->where("$tbl_es.game_id", '=', $game->game_id)
                    ->where("$tbl_es.user_id", '=', $this->user_id)
                    ->on("$tbl_es.period_id", '=', "$tbl_gp.period_id")
                    ->whereNotNull("$tbl_es.link_id");
            })->join($tbl_sel, function ($join) use ($tbl_es, $tbl_sel) {
                $join->on("$tbl_sel.sport", '=', "$tbl_es.sport")
                    ->on("$tbl_sel.season", '=', "$tbl_es.season")
                    ->on("$tbl_sel.game_id", '=', "$tbl_es.game_id")
                    ->on("$tbl_sel.link_id", '=', "$tbl_es.link_id")
                    ->on("$tbl_sel.period_id", '=', "$tbl_es.period_id");
            })->where([
                ["$tbl_gp.sport", '=', $game->sport],
                ["$tbl_gp.season", '=', $game->season],
            ])->whereRaw("(? = 0 OR IFNULL($tbl_sel.status, 'negative') = 'negative')", [$sel_required])
            ->orderByDesc("$tbl_gp.period_order")->limit(1)->get();
    }

    /**
     * Calculate the scoring attributes that (may) have changed since the previous period
     * @return self The current object to allow for chaining
     */
    public function setChangesSinceLastPeriod(): self
    {
        if (!$this->count()) {
            // Skip an empty list.
            return $this;
        }
        // Determine the previously calced period for each game, so we can determine the previous period for changes.
        $tbl_g = Game::getTable();
        $tbl_ps = GamePeriodSummary::getTable();
        $tbl_gp = GamePeriod::getTable();
        $query = Game::query()
            ->select("$tbl_g.*")
            ->selectRaw("CONCAT($tbl_g.sport, '-', $tbl_g.season, '-', $tbl_g.game_id) AS game_ref")
            ->selectRaw('PERIOD_CURR.period_id AS curr_period_id')
            ->selectRaw('SUBSTRING(MAX(CONCAT(LPAD(PERIOD_PREV.period_order, 3, "0"), ":", PERIOD_PREV.period_id)), 5)'
                . ' AS prev_period_id')
            ->leftJoin($tbl_ps, function ($join) use ($tbl_ps, $tbl_g) {
                $join->on("$tbl_ps.sport", '=', "$tbl_g.sport")
                    ->on("$tbl_ps.season", '=', "$tbl_g.season")
                    ->on("$tbl_ps.game_id", '=', "$tbl_g.game_id");
            })->leftJoin("$tbl_gp AS PERIOD_CURR", function ($join) use ($tbl_ps) {
                $join->on('PERIOD_CURR.sport', '=', "$tbl_ps.sport")
                    ->on('PERIOD_CURR.season', '=', "$tbl_ps.season")
                    ->on('PERIOD_CURR.period_id', '=', "$tbl_ps.period_id");
            })->leftJoin("$tbl_gp AS PERIOD_PREV", function ($join) {
                $join->on('PERIOD_PREV.sport', '=', "PERIOD_CURR.sport")
                    ->on('PERIOD_PREV.season', '=', "PERIOD_CURR.season")
                    ->on('PERIOD_PREV.period_order', '<', "PERIOD_CURR.period_order");
            })->groupBy("$tbl_g.sport", "$tbl_g.season", "$tbl_g.game_id");
        foreach ($this->unique('game_ref') as $game_ref) {
            $query->orWhere(function ($where) use ($game_ref, $tbl_g) {
                list($sport, $season, $game_id) = explode('-', $game_ref);
                $where->where("$tbl_g.sport", '=', $sport)
                    ->where("$tbl_g.season", '=', $season)
                    ->where("$tbl_g.game_id", '=', $game_id);
            });
        }
        $game_periods = $query->get();
        // Use this to load the entry details for the previous round.
        $query = EntryByPeriod::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id, "-", user_id) AS user_ref')
            ->where(DB::raw(1), '=', DB::raw(0)); // Allow for no matching game periods above.
        foreach ($this->data as $entry_score) {
            $query->orWhere(function ($where) use ($entry_score, $game_periods) {
                $period = $game_periods->where('game_ref', $entry_score['game_ref']);
                if (isset($period?->prev_period_id)) {
                    list($sport, $season, $game_id) = explode('-', $entry_score['game_ref']);
                    $where->where('sport', '=', $sport)
                        ->where('season', '=', $season)
                        ->where('game_id', '=', $game_id)
                        ->where('user_id', '=', $entry_score['user_id'])
                        ->where('period_id', '=', $period->prev_period_id);
                }
            });
        }
        $prev_scores = $query->get();
        // Process to identify appropriate changes.
        foreach ($this->data as $i => $entry_score) {
            $prev = $prev_scores->where('user_ref', "{$entry_score['game_ref']}-{$entry_score['user_id']}");
            $this->data[$i]['overall_chg'] = (isset($prev?->overall_pos)
                ? $prev->overall_pos - $entry_score['overall_pos'] : null);
            $this->data[$i]['current_chg'] = (isset($prev?->current_pos)
                ? $prev->current_pos - $entry_score['current_pos'] : null);
            $this->data[$i]['hitrate_chg'] = (isset($prev?->hitrate_pos)
                ? $prev->hitrate_pos - $entry_score['hitrate_pos'] : null);
            $this->data[$i]['achievements_chg'] = $entry_score['achievements'] - ($prev?->achievements ?? 0);
            $this->data[$i]['opponents_chg'] = $entry_score['opponents'] - ($prev?->opponents ?? 0);
        }
        $this->hideFieldsFromAPI(['overall_chg', 'current_chg', 'hitrate_chg', 'achievements_chg', 'opponents_chg']);
        // Return ourselves to allow for chaining.
        return $this;
    }

    /**
     * Load the selections made by the entries for the given period
     * @param integer $period_id The period for which entry selections are to be loaded.
     * @return void
     */
    public function loadPeriodSelections(int $period_id): void
    {
        if (!$this->count()) {
            // Skip an empty list.
            return;
        }
        list($sport) = $this->unique('sport');
        list($season) = $this->unique('season');
        list($game_id) = $this->unique('game_id');
        // Get their selections.
        $tbl_entrysel = EntryByPeriod::getTable();
        $tbl_sel = Selection::getTable();
        $sel_list = EntryByPeriod::query()
            ->select([
                "$tbl_entrysel.*",
                "$tbl_sel.rating",
                "$tbl_sel.opp_rating",
                "$tbl_sel.status",
                "$tbl_sel.summary",
                "$tbl_sel.note",
            ])->selectRaw("replace(FORMAT($tbl_sel.score, 1), '.0', '') AS score")
            ->selectRaw("replace(FORMAT($tbl_entrysel.result, 1), '.0', '') AS result")
            ->leftJoin($tbl_sel, function ($join) use ($tbl_sel, $tbl_entrysel) {
                $join->on("$tbl_sel.sport", '=', "$tbl_entrysel.sport")
                    ->on("$tbl_sel.season", '=', "$tbl_entrysel.season")
                    ->on("$tbl_sel.game_id", '=', "$tbl_entrysel.game_id")
                    ->on("$tbl_sel.link_id", '=', "$tbl_entrysel.link_id")
                    ->on("$tbl_sel.period_id", '=', "$tbl_entrysel.period_id");
            })->where([
                "$tbl_entrysel.sport" => $sport,
                "$tbl_entrysel.season" => $season,
                "$tbl_entrysel.game_id" => $game_id,
            ])->whereIn("$tbl_entrysel.user_id", $this->unique('user_id'))
            ->where("$tbl_entrysel.period_id", $period_id)
            ->whereNotNull("$tbl_entrysel.link_id")
            ->get()
            ->loadSecondary(['entity']);
        // Now tie back together.
        $this->period_id = $period_id;
        foreach ($this->data as $i => $entry) {
            $this->data[$i]['sel'] = $sel_list->where('user_id', $entry['user_id']);
        }
    }

    /**
     * Get each of the selections (by period) for the entries in our dataset
     * @return void
     */
    protected function loadSecondarySelectionsAll(): void
    {
        $this->loadSelectionWorker('selections');
    }

    /**
     * Get each of the recent selections (by period) for the entries in our dataset
     * @return void
     */
    protected function loadSecondarySelectionsRecent(): void
    {
        $this->loadSelectionWorker('recent', FrameworkConfig::get('debear.setup.selections.team_modal_history'));
    }

    /**
     * Worker method for selection loading (by period) for the entries in our dataset
     * @param string  $key      The array key / element we will store the selections against.
     * @param integer $lookback An optional number of periods to limit the lookback.
     * @return void
     */
    protected function loadSelectionWorker(string $key, ?int $lookback = null): void
    {
        $game_refs = $this->unique('game_ref');
        // Determine the nuances to each game.
        $query = Game::query()
            ->select('config_flags')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($game_refs as $game_ref) {
            list($sport, $season, $game_id) = explode('-', $game_ref);
            $query->orWhere(function (Builder $where) use ($sport, $season, $game_id) {
                $where->where([
                    ['sport', '=', $sport],
                    ['season', '=', $season],
                    ['game_id', '=', $game_id],
                ]);
            });
        }
        $game_flags = $sport_lookback = $sport_now = [];
        $games_orm = $query->get();
        foreach ($game_refs as $game_ref) {
            list($sport, $season, $game_id) = explode('-', $game_ref);
            $game = $games_orm->where('game_ref', $game_ref);
            $game_flags[$game_ref] = [
                'sel_required' => (bool)$game->isSelectionRequired(),
                'game_restart' => (bool)$game->canRestartGame(),
                'lookback' => (!$game->isSelectionRequired() && $game->canRestartGame()),
            ];
            // Bubble-up to game/sport-level flags.
            $sport_ref = preg_replace('/\-\d+$/', '', $game_ref);
            $sport_lookback[$sport_ref] = ($sport_lookback[$sport_ref] ?? isset($lookback))
                && $game_flags[$game_ref]['lookback'];
            // Determine the current period for the sport.
            $sport_now[$sport_ref] = GamePeriod::getCurrentPeriod($sport, intval($season));
        }
        // Now we need to load the period and selection info we require.
        list($periods, $period_mapping, $team_sels)
            = $this->prepareSelectionWorkerInfo($game_refs, $lookback, $sport_now, $sport_lookback);
        // Merge these datasets back together.
        foreach ($this->data as $i => $score) {
            $sport_ref = preg_replace('/\-\d+$/', '', $score['game_ref']);
            $period = $periods[$sport_ref];
            // If the game requires a lookback (which the sport may not), ensure it is applied.
            if ($game_flags[$score['game_ref']]['lookback']) {
                $period = $period->slice(0, $lookback);
            }
            // Identify the team's selections in each period.
            $sel_by_team = $team_sels[$sport_ref]->where('user_id', $score['user_id']);
            foreach ($period as $p) {
                $p->selection = $sel_by_team->where('period_id', $p->period_id);
            }
            // Where team's entries could end due to their selections, determine the relevant periods to return.
            if (!$game_flags[$score['game_ref']]['lookback']) {
                $include = [];
                foreach (array_reverse($period->unique('period_order')) as $period_order) {
                    $sel = $sel_by_team->where('period_id', $period_mapping[$sport_ref][$period_order]);
                    $include[] = $period_mapping[$sport_ref][$period_order];
                    if (
                        ($game_flags[$score['game_ref']]['sel_required'] && !$sel->isset())
                        || (!$game_flags[$score['game_ref']]['game_restart'] && $sel->scoring?->status == 'negative')
                    ) {
                        // Allow this selection, but no more will be allowed.
                        break;
                    }
                }
                // Now we have our confirmed list, get only the appropriate periods.
                $period = $period->whereIn('period_id', array_slice(array_reverse($include), 0, $lookback));
            }
            $this->data[$i][$key] = $period;
        }
    }

    /**
     * Expand the game info we have for the entries loaded
     * @return void
     */
    public function loadSecondaryGames(): void
    {
        $query = Game::query()
            ->select('*')
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref');
        foreach ($this->unique('game_ref') as $game_ref) {
            list($sport, $season, $game_id) = explode('-', $game_ref);
            $query->orWhere(function (Builder $where) use ($sport, $season, $game_id) {
                $where->where([
                    ['sport', '=', $sport],
                    ['season', '=', $season],
                    ['game_id', '=', $game_id],
                ]);
            });
        }
        $games = $query->get();
        foreach ($this->data as $i => $score) {
            $this->data[$i]['game'] = $games->where('game_ref', $score['game_ref']);
        }
    }

    /**
     * Prepare some arrays of information for selection processing
     * @param array        $game_refs      The list of games to be processed.
     * @param integer|null $lookback       An optional number of periods to limit the lookback.
     * @param array        $sport_now      Current period info by sport.
     * @param array        $sport_lookback By-sport lookback configuration.
     * @return array The prepared arrays for selection processing
     */
    protected function prepareSelectionWorkerInfo(
        array $game_refs,
        ?int $lookback,
        array $sport_now,
        array $sport_lookback
    ): array {
        $periods = $period_mapping = $team_sels = $cur_dates = [];
        foreach ($game_refs as $game_ref) {
            list($sport, $season, $game_id) = explode('-', $game_ref);
            // Get the periods we will be looking back over.
            $sport_ref = "$sport-$season";
            if (!isset($cur_dates[$sport])) {
                $app_tz = FrameworkConfig::get("debear.sports.subsites.$sport.datetime.timezone_app");
                $cur_dates[$sport] = Time::object()->getAltTimezoneCurDate($app_tz);
            }
            if (!isset($periods[$sport_ref])) {
                $tbl_per = GamePeriod::getTable();
                $tbl_sum = GamePeriodSummary::getTable();
                $now = $sport_now[$sport_ref];
                $min_order = ($sport_lookback[$sport_ref] ? max($now->period_order - $lookback, 1) : 1);
                $periods[$sport_ref] = GamePeriod::query()
                    ->select("$tbl_per.*")
                    ->leftJoin($tbl_sum, function ($join) use ($tbl_sum, $tbl_per, $game_id) {
                        $join->on("$tbl_sum.sport", '=', "$tbl_per.sport")
                            ->on("$tbl_sum.season", '=', "$tbl_per.season")
                            ->where("$tbl_sum.game_id", '=', $game_id)
                            ->on("$tbl_sum.period_id", '=', "$tbl_per.period_id");
                    })->where([
                        ["$tbl_per.sport", '=', $sport],
                        ["$tbl_per.season", '=', $season],
                        ["$tbl_per.season", '=', $season],
                        ["$tbl_per.start_date", '<=', $cur_dates[$sport]],
                    ])->whereIn("$tbl_per.period_order", range($min_order, $now->period_order))
                    ->where(function ($query) use ($tbl_per, $tbl_sum, $cur_dates, $sport) {
                        $query->where("$tbl_per.end_date", '<', $cur_dates[$sport])
                            ->orWhereNotNull("$tbl_sum.period_id");
                    })->orderByDesc("$tbl_per.period_order")
                    ->limit($lookback)
                    ->get();
                $period_mapping[$sport_ref] = $periods[$sport_ref]->pluck('period_order', 'period_id');
            }
            // Then the selections for all teams with these period_ids.
            $team_sels[$sport_ref] = EntryByPeriod::query()
                ->select('*')
                ->selectRaw('replace(FORMAT(result, 1), ".0", "") AS result')
                ->where([
                    ['sport', '=', $sport],
                    ['season', '=', $season],
                    ['game_id', '=', $game_id],
                ])->whereIn('user_id', $this->where('game_ref', $game_ref)->unique('user_id'))
                ->whereIn('period_id', $periods[$sport_ref]->unique('period_id'))
                ->whereNotNull('link_id')
                ->get()
                ->loadSecondary(['entity', 'scoring'])
                ->setChangesSinceLastPeriod();
        }
        return [$periods, $period_mapping, $team_sels];
    }

    /**
     * A wrapper to the standard API output to ensure the sub-type results do not include the entry's other progress
     * @param string $type The classification type to be considered primary.
     * @return array An array of leaders without the other classification's progress info
     */
    public function leadersToAPI(string $type): array
    {
        // Start with our standard processing, but use the appropriate scoring fields as pos/result.
        $ret = [];
        foreach ($this->resultsToAPI() as $row) {
            $ret[] = [
                'user_id' => $row['user_id'],
                'stress' => $row['stress'],
                'result' => $row[$type]['result'],
                'pos' => [
                    'num' => $row[$type]['pos']['num'],
                    'chg' => $row[$type]['pos']['chg'],
                ],
                'entry_name' => $row['entry_name'],
            ];
        }
        return $ret;
    }

    /**
     * Perform any final post-processing on the API data before sending back
     * @param array $row The rendered data for a single row to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        list($sport, $season, $game_id) = explode('-', $this->game_ref);
        $game = Game::loadByGameRef($sport, intval($season), intval($game_id));
        $pos = [];
        // Any achievements completed or opponents beaten in this period?
        if (isset($this->apiCustomData['entry_scores'])) {
            $entry_score = $this->apiCustomData['entry_scores']->where('user_id', $row['user_id']);
            $achievements = [];
            if (($entry_score->achievements_chg ?? 0) > 0) {
                foreach ($this->apiCustomData['game']->achievements as $achieve) {
                    $achievements[$achieve->achieve_id] = [];
                    foreach ($achieve->levels as $level) {
                        if ($entry_score->achievements_chg_map[$level->bit_flag]) {
                            $achievements[$level->achieve_id][$level->level] = true;
                        }
                    }
                }
            }
            $row['achieved'] = [
                'achievements' => array_filter($achievements),
                'opponent' => (($entry_score->opponents_chg ?? 0) > 0),
            ];
        }
        // Format the overall and current info.
        $ov_keys = [
            'overall' => true,
            'current' => $game->hasCurrentLeaderboard(),
        ];
        foreach ($ov_keys as $type => $required) {
            if ($required) {
                $pos[$type] = [
                    'result' => $this->formatScoreAPI($row["{$type}_res"]),
                    'pos' => [
                        'num' => $row["{$type}_pos"],
                        'chg' => $row["{$type}_chg"],
                    ],
                ];
            }
            foreach (['res', 'pos', 'chg'] as $col) {
                unset($row["{$type}_{$col}"]);
            }
        }
        // Then the hitrate info.
        if ($game->hasHitRateLeaderboard()) {
            $pos['hitrate'] = [
                'result' => ($row['hitrate_num_pos'] + $row['hitrate_num_neu'] + $row['hitrate_num_neg']) > 0
                    ? sprintf('%0.02f%%', (100 * ($row['hitrate_num_pos'] + $row['hitrate_num_neu']))
                        / ($row['hitrate_num_pos'] + $row['hitrate_num_neu'] + $row['hitrate_num_neg']))
                    : null,
                'pos' => [
                    'num' => $row['hitrate_pos'],
                    'chg' => $row['hitrate_chg'],
                ],
                'raw' => [
                    'positive' => $row['hitrate_num_pos'],
                    'neutral' => $row['hitrate_num_neu'],
                    'negative' => $row['hitrate_num_neg'],
                    'na' => $row['hitrate_num_na'],
                ],
            ];
        }
        foreach (['res', 'pos', 'chg', 'num_pos', 'num_neu', 'num_neg', 'num_na'] as $col) {
            unset($row["hitrate_{$col}"]);
        }
        // And tweak the order to prepend these.
        $row = array_merge($pos, $row);
    }

    /**
     * Convert a single row into a web-ready payload
     * @param array $opt An array of customisations.
     * @return array The data to converted to an array
     */
    public function rowToWeb(array $opt): array
    {
        $ret = [
            'entry_id' => $this->user_id,
            'entry_name' => $this->entry_name,
            'user' => [
                'id' => $this->user->user_id,
                'name' => $this->user->display_name,
            ],
            'result' => $this->overall_res,
            'overall' => [
                'pos' => $this->overall_pos,
                'change' => $this->overall_chg,
            ],
            'current' => [
                'result' => $this->current_res,
                'pos' => $this->current_pos,
                'change' => $this->current_chg,
            ],
            'stress' => $this->stress_pct,
            'recent' => [],
        ];
        if ($opt['no-current']) {
            unset($ret['current']);
        }
        // Some pre-processing calcs.
        list($sport, $season, $game_id) = explode('-', $this->game_ref);
        $is_user = (UserModel::object()->id == $this->user_id);
        $query = GamePeriodSummary::query()
            ->selectRaw('CONCAT(sport, "-", season, "-", game_id, "-", period_id) AS period_ref');
        foreach ((isset($this->recent) ? $this->recent : []) as $recent) {
            $query->orWhere(function (Builder $where) use ($sport, $season, $game_id, $recent) {
                $where->where([
                    ['sport', '=', $sport],
                    ['season', '=', $season],
                    ['game_id', '=', $game_id],
                    ['period_id', '=', $recent->period_id],
                ]);
            });
        }
        $periods_calced = $query->get();
        // Process our recent selections (within this object).
        $last_result = $this->current_res ?? $this->overall_res;
        $last_stress = $this->stress_pct;
        foreach ((isset($this->recent) ? $this->recent : []) as $recent) {
            $sel_raw = $recent->selection;
            // Determine if this selection can be displayed.
            $is_calced = $periods_calced->where('period_ref', "{$this->game_ref}-{$recent->period_id}")->isset();
            $sel_raw = (!$is_user && !$is_calced ? 'hidden' : (!$sel_raw->isset() ? 'no_sel' : $sel_raw));
            $sel = is_object($sel_raw) ? $sel_raw : null;
            // Add to the list.
            $ret['recent'][] = array_filter([
                'period_id' => $recent->period_id,
                'period' => [
                    'short' => $recent->name_short_disp,
                    'long' => $recent->name_disp,
                ],
                'sel' => $sel?->entity?->rowToSummaryWeb() ?? ['status' => $sel_raw],
                'result' => $sel?->result ?? $last_result,
                'stress' => $sel?->stress_pct ?? $last_stress,
                'rating' => static::convertRating($sel?->scoring?->rating),
                'opp_rating' => ($opt['opp-rating'] ? static::convertRating($sel?->scoring?->opp_rating) : '*REMOVE*'),
                'score' => $this->formatScore($sel?->scoring?->score),
                'status' => $sel?->scoring?->status,
                'summary' => $sel?->scoring?->summary,
            ], function ($field) {
                return isset($field) || ($field != '*REMOVE*');
            });
            $last_result = $sel->result ?? $last_result;
            $last_stress = $sel->stress_pct ?? $last_stress;
        }
        // Finally, return.
        return $ret;
    }

    /**
     * Convert the selections for row into an API payload
     * @param array $opt An array of customisations.
     * @return array The data to converted to an array
     */
    public function selectionsToAPI(array $opt = []): array
    {
        $ret = [];
        $last_result = $this->formatScoreAPI($this->current_res ?? $this->overall_res);
        $last_stress = $this->stress;
        foreach ($this->selections as $selection) {
            $sel = $selection->selection;
            // Any achievements completed or opponents beaten in this period?
            $achieved = null;
            if (isset($opt['game']) && isset($sel->achievements_chg)) {
                $achievements = [];
                foreach ($opt['game']->achievements as $achieve) {
                    $achievements[$achieve->achieve_id] = [];
                    foreach ($achieve->levels as $level) {
                        if ($sel->achievements_chg_map[$level->bit_flag]) {
                            $achievements[$level->achieve_id][$level->level] = true;
                        }
                    }
                }
                $achieved = [
                    'achievements' => array_filter($achievements),
                    'opponent' => (($sel->opponents_chg ?? 0) > 0),
                ];
            }
            // Form the API row.
            $ret[] = array_filter([
                'period_id' => $selection->period_id,
                'period' => [
                    'short' => $selection->name_short_disp,
                    'long' => $selection->name_disp,
                ],
                'sel' => !$sel?->isset() ? [] : [
                    'link_id' => $sel->link_id,
                    'link' => $sel->entity->rowToSummaryAPI(),
                ],
                'result' => $this->formatScoreAPI($sel?->result) ?? $last_result,
                'stress' => $sel?->stress ?? $last_stress,
                'rating' => $sel?->scoring?->rating,
                'opp_rating' => ($opt['game']->isMajorLeague() ? $sel?->scoring?->opp_rating : '*REMOVE*'),
                'score' => $this->formatScoreAPI($sel?->scoring?->score),
                'status' => $sel?->scoring?->status,
                'summary' => $sel?->scoring?->summary,
                'achieved' => $achieved ?? '*REMOVE*',
            ], function ($field) {
                return isset($field) && ($field != '*REMOVE*');
            });
            $last_result = (isset($sel?->result) ? $this->formatScoreAPI($sel->result) : $last_result);
            $last_stress = $sel?->stress ?? $last_stress;
        }
        return $ret;
    }

    /**
     * Convert all data into a web-ready leaderboard payload
     * @param string  $type     The leaderboard type to render.
     * @param boolean $curr_row Optional flag to indicate only the current row should be converted.
     * @return array The data in the object converted to an array
     */
    public function leaderboardsToWeb(string $type, bool $curr_row = false): array
    {
        $data = (!$curr_row ? $this : [$this->getCurrent()]);
        // Pre-req, the user details.
        $user_cols = ['id', 'user_id', 'display_name'];
        $users = UserORM::loadByNumericID(!$curr_row ? $data->unique('user_id') : [$this->user_id], $user_cols);
        // Pre-req, whether the period has been processed or not yet.
        $is_calced = GamePeriodSummary::query()
            ->where([
                ['sport', '=', $this->sport],
                ['season', '=', $this->season],
                ['game_id', '=', $this->game_id],
                ['period_id', '=', $this->period_id],
            ])->get()->isset();
        // Now process.
        $fields = [
            'overall' => ['pos' => 'overall_pos', 'result' => 'overall_res', 'change' => 'overall_chg'],
            'current' => ['pos' => 'current_pos', 'result' => 'current_res', 'change' => 'current_chg'],
            'hitrate' => ['pos' => 'hitrate_pos', 'result' => 'hitrate_res', 'change' => 'hitrate_chg'],
        ];
        $tied_pos = [];
        foreach ($data as $i => $row) {
            $tied_pos[$row->{$fields[$type]['pos']}] = ($tied_pos[$row->{$fields[$type]['pos']}] ?? 0) + 1;
        }
        $tied_pos = array_filter($tied_pos, function ($i) {
            return $i > 1;
        });
        $ret = $ties = $by_pos = [];
        foreach ($data as $i => $row) {
            // Determine if this selection can be displayed.
            $is_user = (UserModel::object()->id == $row->user_id);
            $sel_raw = (!$is_user && !$is_calced ? 'hidden' : (!$row->sel->isset() ? 'no_sel' : $row->sel));
            $sel = is_object($sel_raw) ? $sel_raw : null;
            // Add to the list.
            $user = $users->where('id', $row->user_id);
            $pos = $row->{$fields[$type]['pos']};
            $by_pos[$pos] = ($by_pos[$pos] ?? 0) + 1;
            $res = $row->{$fields[$type]['result']};
            $ret[] = [
                'css' => $row->rowCSS($fields[$type]['pos'], count(array_keys($by_pos))),
                'pos' => [
                    'num' => $pos,
                    'disp' => !isset($ties[$pos]) ? (isset($tied_pos[$pos]) ? 'T-' : '') . Format::ordinal($pos) : '=',
                    'chg' => $row->posChangeIcon($type),
                ],
                'entry_id' => $row->user_id,
                'entry_name' => $row->entry_name,
                'slug' => $row->slug,
                'user' => [
                    'id' => $user->user_id,
                    'name' => $user->display_name,
                ],
                'result' => $type == 'hitrate' ? "$res%" : $res,
                'stress' => $row->stress_pct,
                'sel' => $sel?->rowToWeb() ?? ['status' => $sel_raw],
            ];
            $ties[$pos] = true;
        }
        return (!$curr_row ? $ret : $ret[0]);
    }

    /**
     * Convert the data for a single row into a web-ready leaderboard payload
     * @param string $type The leaderboard type to render.
     * @return array|null The data in the object converted to an array, or null on an empty object
     */
    public function leaderboardToWeb(string $type): ?array
    {
        return ($this->isset() ? $this->leaderboardsToWeb($type, true) : null);
    }

    /**
     * Build the base Highcharts configuration for the Success Rate gauge
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the chart
     */
    public function hitrateChart(string $dom_id): array
    {
        // Breakdown as a pie chart.
        $chart = Highcharts::new($dom_id, ['styled-mode' => false, 'js-module' => 'solid-gauge']);
        // Customise - type.
        $chart['chart']['type'] = 'solidgauge';
        // Customise - plot options.
        $chart['plotOptions'] = [
            'solidgauge' => [
                'innerRadius' => '40%',
                'dataLabels' => ['y' => 5, 'borderWidth' => 0, 'useHTML' => true],
            ],
        ];
        // Customise - pane.
        $chart['pane'] = [
            'startAngle' => -90,
            'endAngle' => 90,
            'background' => [
                'backgroundColor' => 'rgba(0, 0, 0, 0.05)',
                'innerRadius' => '40%',
                'outerRadius' => '100%',
                'shape' => 'arc'
            ],
        ];

        // Customise - yAxis.
        $chart['yAxis'] = [
            'lineWidth' => 0,
            'minorTickInterval' => null,
            'min' => 0,
            'max' => 100,
            'tickAmount' => 2,
            'tickWidth' => 0,
            'labels' => ['y' => 16, 'format' => '{value}%'],
        ];
        // Customise - series (and calculate the stacking ourselves).
        $num_pos = $this->hitrate_num_pos;
        $num_neu = $this->hitrate_num_neu;
        $num_neg = $this->hitrate_num_neg;
        $num_na = $this->hitrate_num_na;
        $num_tot_valid = max($num_pos + $num_neu + $num_neg, 1); // For display, we only count valid selections.
        $num_tot = max($num_pos + $num_neu + $num_neg + $num_na, 1); // Avoids division by zero errors.
        $chart['series'] = [[
            'dataLabels' => ['enabled' => true, 'format' => sprintf('%0.2f%%', 100 * ($num_pos / $num_tot_valid))],
            'data' => array_values(array_filter([
                !$num_na
                    ? false
                    : ['colorIndex' => 3, 'y' => 100 * (($num_pos + $num_neu + $num_neg + $num_na) / $num_tot)],
                !$num_neg ? false : ['colorIndex' => 2, 'y' => 100 * (($num_pos + $num_neu + $num_neg) / $num_tot)],
                !$num_neu ? false : ['colorIndex' => 1, 'y' => 100 * (($num_pos + $num_neu) / $num_tot)],
                !$num_pos ? false : ['colorIndex' => 0, 'y' => 100 * ($num_pos / $num_tot)],
            ]))
        ]];
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { return successRateHover(this); }',
            'template' => join('<br />', array_filter([
                !$num_pos ? false : sprintf('<row-0>Successful: %0.02f%%</row-0>', 100 * ($num_pos / $num_tot)),
                !$num_neu ? false : sprintf('<row-1>Partial Success: %0.02f%%</row-1>', 100 * ($num_neu / $num_tot)),
                !$num_neg ? false : sprintf('<row-2>Unsuccessful: %0.02f%%</row-2>', 100 * ($num_neg / $num_tot)),
                !$num_na ? false : sprintf('<row-3>Did Not Appear: %0.02f%%</row-3>', 100 * ($num_na / $num_tot)),
            ])),
        ];
        return $chart;
    }

    /**
     * Load the achievements and render them for an API
     * @param Game $game The object representing the game to be loaded.
     * @return array The JSON representation of the entry's achievements
     */
    public function achievementsToAPI(Game $game): array
    {
        // Get the info.
        $game->loadSecondary(['achievements']);
        // Now parse together.
        $ret = [];
        foreach ($game->achievements as $achieve) {
            $a = [];
            foreach ($achieve->levels as $level) {
                $a[$level->level] = $this->achievements_map[$level->bit_flag];
            }
            $ret[$achieve->achieve_id] = $a;
        }
        return $ret;
    }

    /**
     * Load the opponents and render them for an API
     * @param Game $game The object representing the game to be loaded.
     * @return array The JSON representation of the entry's opponent status
     */
    public function opponentsToAPI(Game $game): array
    {
        // Get the info.
        $game->loadSecondary(['achievements']);
        // Now parse together.
        $ret = [];
        foreach ($game->opponents as $opp) {
            $ret[$opp->raw_id] = $this->opponents_map[$opp->bit_flag];
        }
        return $ret;
    }
}
