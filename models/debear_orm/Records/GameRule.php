<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;

class GameRule extends Instance
{
    /**
     * Convert the input string of rules into an array of individual lines
     * @return array An array of individual rule entries
     */
    public function getAsListAttribute(): array
    {
        return explode("\n", $this->rules);
    }
}
