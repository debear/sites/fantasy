<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\Models\Skeleton\User as UserModel;
use DeBear\ORM\Fantasy\Admin\User as UserORM;
use DeBear\ORM\Fantasy\Common\SelectionMap;
use DeBear\Helpers\Comms\Email;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Fantasy\Entities;
use DeBear\Repositories\Time;
use DeBear\Helpers\Fantasy\Records\Entry as EntryTrait;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Entry extends Instance
{
    use EntryTrait;

    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'bool' => ['email_status'],
    ];
    /**
     * Mapping of User ID > Name within API requests
     * @var array
     */
    protected $api_user_map = [];
    /**
     * Additional games scores not part of the standard load sequence
     * @var array
     */
    protected $games_addtl = [];

    /**
     * Convert the name into a URL slug
     * @return string A slug representation of the name
     */
    public function getSlugAttribute(): string
    {
        return Strings::codifyURL($this->name) . "-{$this->user_id}";
    }

    /**
     * Perform some preperatory steps before rendering API data
     * @return void
     */
    protected function preProcessAPIData(): void
    {
        $map = UserORM::loadByNumericID($this->unique('user_id'));
        $this->api_user_map = array_combine($map->unique('id'), $map->unique('user_id'));
    }

    /**
     * Manipulate an entry for its API representation
     * @param array $row The row data (passed by reference) to be manipulated.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        $row['user_id'] = $this->api_user_map[$row['user_id']]; // Numeric to name conversion.
        if (!$row['email_status']) {
            // If email is disabled, unset the type.
            $row['email_type'] = null;
        }
        if (isset($row['games_active'])) {
            foreach ($row['games_active'] as $k => $game) {
                $row['games_active'][$k] = $game->rowToAPI();
            }
        }
    }

    /**
     * Get the EntryScore object for this entry's participation in a specific game
     * @param Game $game Model representing the game being queried.
     * @return EntryScore ORM object of the entry's score for the requested game
     */
    public function getGameScore(Game $game): EntryScore
    {
        $score = null;
        if (!$game->isComplete() && isset($this->games_active[$game->game_ref])) {
            $score = $this->games_active[$game->game_ref];
        } elseif ($game->isComplete()) {
            // Game has completed so needs loading manually.
            if (!isset($this->games_addtl[$game->game_ref])) {
                $hitrate_bin = 'LPAD(BIN(hitrate_num), 32, "0")';
                $this->games_addtl[$game->game_ref] = EntryScore::query()
                    ->select([
                        'user_id',
                        'overall_pos',
                        'current_pos',
                        'stress',
                        'hitrate_pos',
                        'achievements',
                        'opponents',
                    ])->selectRaw("CONV(SUBSTRING($hitrate_bin, 1, 8), 2, 10) AS hitrate_num_pos")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 9, 8), 2, 10) AS hitrate_num_neu")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 17, 8), 2, 10) AS hitrate_num_neg")
                    ->selectRaw("CONV(SUBSTRING($hitrate_bin, 25, 8), 2, 10) AS hitrate_num_na")
                    ->selectRaw('replace(FORMAT(overall_res, 1), ".0", "") AS overall_res')
                    ->selectRaw('replace(FORMAT(current_res, 1), ".0", "") AS current_res')
                    ->selectRaw('CONCAT(sport, "-", season, "-", game_id) AS game_ref')
                    ->selectRaw('overall_pos IS NOT NULL AS processed')
                    ->where([
                        ['sport', '=', $game->sport],
                        ['season', '=', $game->season],
                        ['game_id', '=', $game->game_id],
                    ])->whereIn('user_id', $this->unique('user_id'))
                    ->get()
                    ->setChangesSinceLastPeriod()
                    ->hideFieldsFromAPI(['user_id', 'hitrate_num']);
            }
            $score = $this->games_addtl[$game->game_ref]->where('user_id', $this->user_id ?? 0);
        }
        return $score ?? (new EntryScore());
    }

    /**
     * Link a user's entry to a specific game
     * @param string  $sport   The sport of the game being joined.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being joined.
     * @return void
     */
    public function gameJoin(string $sport, int $season, int $game_id): void
    {
        EntryScore::create([
            'sport' => $sport,
            'season' => $season,
            'game_id' => $game_id,
            'user_id' => UserModel::object()->id,
            'overall_res' => 0.0,
            'current_res' => 0.0,
        ])->audit('records_games_join', 'Joined game', ['game_id']);
    }

    /**
     * Remove the link between a user's entry and a specific game
     * @param string  $sport   The sport of the game being removed.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being removed.
     * @return void
     */
    public function gameLeave(string $sport, int $season, int $game_id): void
    {
        EntryScore::query()->where([
            'sport' => $sport,
            'season' => $season,
            'game_id' => $game_id,
            'user_id' => UserModel::object()->id,
        ])->get()->audit('records_games_leave', 'Left game', ['game_id'])->deleteRow();
    }

    /**
     * Load the entry for the current user
     * @param integer|null $user_id The (numeric) ID of the user's entry being loaded.
     * @return self ORM object containing the entry for the user
     */
    public static function load(?int $user_id = null): self
    {
        return static::query()
            ->where('user_id', $user_id ?? UserModel::object()->id ?? 0)
            ->get()
            ->loadSecondary(['games']);
    }

    /**
     * Determine the (active) games this entry is currently taking part in
     * @return void
     */
    protected function loadSecondaryGames(): void
    {
        $now = Time::object()->getNowFmt();
        $tbl_score = EntryScore::getTable();
        $tbl_game = Game::getTable();
        $hitrate_bin = "LPAD(BIN($tbl_score.hitrate_num), 32, '0')";
        $this->secondary['games'] = EntryScore::query()
            ->select([
                "$tbl_score.user_id",
                "$tbl_score.overall_pos",
                "$tbl_score.current_pos",
                "$tbl_score.stress",
                "$tbl_score.hitrate_pos",
                "$tbl_score.achievements",
                "$tbl_score.opponents",
            ])->selectRaw("CONV(SUBSTRING($hitrate_bin, 1, 8), 2, 10) AS hitrate_num_pos")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 9, 8), 2, 10) AS hitrate_num_neu")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 17, 8), 2, 10) AS hitrate_num_neg")
            ->selectRaw("CONV(SUBSTRING($hitrate_bin, 25, 8), 2, 10) AS hitrate_num_na")
            ->selectRaw("replace(FORMAT($tbl_score.overall_res, 1), '.0', '') AS overall_res")
            ->selectRaw("replace(FORMAT($tbl_score.current_res, 1), '.0', '') AS current_res")
            ->selectRaw("CONCAT($tbl_score.sport, '-', $tbl_score.season, '-', $tbl_score.game_id) AS game_ref")
            ->selectRaw("$tbl_score.overall_pos IS NOT NULL AS processed")
            ->join($tbl_game, function ($join) use ($tbl_game, $tbl_score, $now) {
                return $join->on("$tbl_game.sport", '=', "$tbl_score.sport")
                    ->on("$tbl_game.season", '=', "$tbl_score.season")
                    ->on("$tbl_game.game_id", '=', "$tbl_score.game_id")
                    ->whereRaw("? BETWEEN $tbl_game.date_open AND "
                        . "DATE_SUB($tbl_game.date_complete, INTERVAL 1 SECOND)", [$now]);
            })->whereIn("$tbl_score.user_id", $this->unique('user_id'))
            // Prioritise in-season games over upcoming and then completed.
            ->orderByRaw("? BETWEEN $tbl_game.date_start AND $tbl_game.date_end DESC", [$now])
            ->orderByRaw("$tbl_game.date_end > ? DESC", [$now])
            ->orderBy("$tbl_game.date_end")
            ->orderBy("$tbl_game.sport")
            ->orderBy("$tbl_game.game_id")
            ->get()
            ->setChangesSinceLastPeriod()
            ->hideFieldsFromAPI(['user_id', 'opponents', 'hitrate_num']);
        $user_key = array_flip(array_column($this->data, 'user_id'));
        foreach ($this->secondary['games'] as $i => $user_game) {
            $user_index = $user_key[$user_game->user_id];
            if (!isset($this->data[$user_index]['games_active'])) {
                $this->data[$user_index]['games_active'] = [];
            }
            $this->data[$user_index]['games_active'][$user_game->game_ref] = $this->secondary['games']->getRow($i);
        }
    }

    /**
     * Perform all the single game loading of secondary data
     * @param Game $game Details of the game being viewed.
     * @return void
     */
    public function loadSingleGame(Game $game): void
    {
        $this->score = $this->getGameScore($game);
        if ($game->hasActiveLimit()) {
            $this->loadSelectionsDropoff($game); // Upcoming selection dropoff totals.
        }
        if ($game->canReuseSelection() && $game->hasHitRateLeaderboard()) {
            $this->loadSelectionsTopHitrate($game); // Top selections made by the entry.
        }
        if ($game->hasStreakBuster()) {
            $this->loadSelectionsStreakBuster($game); // Streak busting opponents..
        }
        if (!$game->canReuseSelection()) {
            $this->loadSelectionsAvailabilityStatus($game); // Selection pool and status.
        }
    }

    /**
     * Load the entry's selections in a specific game for a given list of periods
     * @param Game       $games   Details of the game(s) being viewed.
     * @param array|null $periods The array of period IDs, which if unset will be determine automatically.
     * @return void
     */
    public function loadSelections(Game $games, ?array $periods = null): void
    {
        $index = $this->index();
        $loading_multiple = ($games->count() > 1);
        foreach ($games as $game) {
            if ($loading_multiple) {
                $game->configure();
            }
            // If we haven't been supplied any periods, get the current and last periods to load.
            if ($loading_multiple || !isset($periods)) {
                $periods = $game->getPeriods($this);
                $periods = array_filter([$periods['curr']->period_id, $periods['last']->period_id]);
            }
            // Now the selection loading.
            $tbl_entrysel = EntryByPeriod::getTable();
            $tbl_sel = Selection::getTable();
            $sel_list = EntryByPeriod::query()
                ->select([
                    "$tbl_entrysel.*",
                    "$tbl_sel.rating",
                    "$tbl_sel.opp_rating",
                    "$tbl_sel.status",
                    "$tbl_sel.summary",
                    "$tbl_sel.stress",
                    "$tbl_sel.note",
                ])->selectRaw("replace(FORMAT($tbl_sel.score, 1), '.0', '') AS score")
                ->selectRaw("replace(FORMAT($tbl_entrysel.result, 1), '.0', '') AS result")
                ->leftJoin($tbl_sel, function ($join) use ($tbl_sel, $tbl_entrysel) {
                    $join->on("$tbl_sel.sport", '=', "$tbl_entrysel.sport")
                        ->on("$tbl_sel.season", '=', "$tbl_entrysel.season")
                        ->on("$tbl_sel.game_id", '=', "$tbl_entrysel.game_id")
                        ->on("$tbl_sel.link_id", '=', "$tbl_entrysel.link_id")
                        ->on("$tbl_sel.period_id", '=', "$tbl_entrysel.period_id");
                })->where([
                    "$tbl_entrysel.sport" => $game->sport,
                    "$tbl_entrysel.season" => $game->season,
                    "$tbl_entrysel.game_id" => $game->game_id,
                    "$tbl_entrysel.user_id" => $this->user_id,
                ])->whereIn("$tbl_entrysel.period_id", $periods)
                ->whereNotNull("$tbl_entrysel.link_id")
                ->orderBy("$tbl_entrysel.period_id")
                ->get()
                ->hideFieldsFromAPI([
                    'overall_res', 'overall_pos', 'current_res', 'current_pos', 'achievements', 'opponents',
                    'hitrate_num', 'hitrate_num_pos', 'hitrate_num_neu', 'hitrate_num_neg', 'hitrate_num_na',
                    'hitrate_pos',
                ])
                ->loadSecondary(['entity', 'stats'])
                ->setChangesSinceLastPeriod();
            $game_ref = $game->game_ref;
            if (!isset($this->data[$index]['games_sel'][$game_ref])) {
                $this->data[$index]['games_sel'][$game_ref] = [];
            }
            foreach ($periods as $p) {
                $sel = $sel_list->where('period_id', $p);
                $this->data[$index]['games_sel'][$game_ref][$p] = $sel->isset()
                    /* @phan-suppress-next-line PhanTypeMismatchArgumentSuperType from Iterator use */
                    ? $sel : EntryByPeriod::createBlank($game, $p);
            }
            // Revert the configure step.
            if ($loading_multiple) {
                $game->revertConfigure();
            }
        }
        $games->rewind(); // Revert the changes we made to the internal counters.
    }

    /**
     * Load the summary info for a users in a specific game
     * @param string  $game_ref    The game, via concated reference code, to be loaded.
     * @param boolean $recent_only An optional flag, default true, indicating if only 'recent' selections are loaded.
     * @return EntryScore An ORM object of entry-specific summary info
     */
    public function loadExpandedSelections(string $game_ref, bool $recent_only = true): EntryScore
    {
        // Transfer some info from main entry to game score and create as a standalone object.
        $data = $this->games_active[$game_ref]->toMaskedArray();
        $data['entry_name'] = $this->name;
        $data['user'] = UserORM::loadByNumericID($this->user_id);
        $score = new EntryScore($data);

        // Get the recent periods / selections and return.
        $score->loadSecondary(['selections' . ($recent_only ? 'Recent' : 'All')]);
        return $score;
    }

    /**
     * Get the pre-requested details of a selection in a given period
     * @param string  $game_ref  The game, via concated reference code, to be loaded.
     * @param integer $period_id The period being viewed.
     * @return EntryByPeriod|null An ORM object containing the entry's selection info in this period
     */
    public function getSelection(string $game_ref, int $period_id): ?EntryByPeriod
    {
        return $this->games_sel[$game_ref][$period_id] ?? null;
    }

    /**
     * Load the "dropoff" selections for this entry
     * @param Game $game Model representing the game being queried.
     * @return void
     */
    public function loadSelectionsDropoff(Game $game): void
    {
        $dropoff = EntryDropoff::query()
            ->select('user_id')
            ->selectRaw("replace(FORMAT(score4, 1), '.0', '') AS score4")
            ->selectRaw("replace(FORMAT(score3, 1), '.0', '') AS score3")
            ->selectRaw("replace(FORMAT(score2, 1), '.0', '') AS score2")
            ->selectRaw("replace(FORMAT(score1, 1), '.0', '') AS score1")
            ->selectRaw("replace(FORMAT(score0, 1), '.0', '') AS score0")
            ->where([
                ['sport', '=', $game->sport],
                ['season', '=', $game->season],
                ['game_id', '=', $game->game_id],
            ])->whereIn('user_id', $this->unique('user_id'))
            ->get();
        foreach ($this->data as $i => $entry) {
            $this->data[$i]['dropoff'] = $dropoff->where('user_id', $entry['user_id']);
        }
    }

    /**
     * Load the top selections made by this entry in a specific game
     * @param Game $game Model representing the game being queried.
     * @return void
     */
    public function loadSelectionsTopHitrate(Game $game): void
    {
        $periods = $game->getPeriods($this);
        $tbl_es = EntryByPeriod::getTable();
        $tbl_sel = Selection::getTable();
        foreach ($this->data as $i => $entry) {
            $this->data[$i]['top_sel'] = EntryByPeriod::query()
                ->select(["$tbl_es.sport", "$tbl_es.season", "$tbl_es.game_id", "$tbl_es.link_id"])
                ->selectRaw('? AS period_id', [$periods['last']->period_id ?? $periods['curr']->period_id])
                ->selectRaw('COUNT(*) AS times_sel')
                ->selectRaw("100 * AVG($tbl_sel.status IN ('positive', 'neutral')) AS success_sel")
                ->join($tbl_sel, function ($join) use ($tbl_sel, $tbl_es) {
                    $join->on("$tbl_sel.sport", '=', "$tbl_es.sport")
                        ->on("$tbl_sel.season", '=', "$tbl_es.season")
                        ->on("$tbl_sel.game_id", '=', "$tbl_es.game_id")
                        ->on("$tbl_sel.link_id", '=', "$tbl_es.link_id")
                        ->on("$tbl_sel.period_id", '=', "$tbl_es.period_id");
                })->where([
                    ["$tbl_es.sport", '=', $game->sport],
                    ["$tbl_es.season", '=', $game->season],
                    ["$tbl_es.game_id", '=', $game->game_id],
                    ["$tbl_es.user_id", '=', $entry['user_id']],
                ])->whereNotNull("$tbl_es.link_id")
                ->whereNotNull("$tbl_es.result")
                ->groupBy("$tbl_es.link_id")
                ->orderByDesc('times_sel')
                ->orderByDesc('success_sel')
                ->limit(FrameworkConfig::get('debear.setup.selections.entry_hitrate'))
                ->get()
                ->loadSecondary(['entity']);
        }
    }

    /**
     * Load the streak busting opponents for this entry in a specific game
     * @param Game $game Model representing the game being queried.
     * @return void
     */
    public function loadSelectionsStreakBuster(Game $game): void
    {
        $busters = EntryBusters::query()
            ->where([
                ['sport', '=', $game->sport],
                ['season', '=', $game->season],
                ['game_id', '=', $game->game_id],
            ])->whereIn('user_id', array_merge($this->unique('user_id'), [0])) // Fall back to the global list.
            ->get()
            ->loadSecondary(['entities']);
        // Now tie back together.
        foreach ($this->data as $i => $entry) {
            $detail = $busters->where('user_id', $entry['user_id']) ?: $busters->where('user_id', 0);
            $this->data[$i]['busters'] = $detail->busters;
        }
    }

    /**
     * Load the availability status of all selections in the pool
     * @param Game $game Model representing the game being queried.
     * @return void
     */
    public function loadSelectionsAvailabilityStatus(Game $game): void
    {
        // NOTE: This is deliberately team-centric at this time, as that is the expected use-case for this game mode.
        $sel_list = SelectionMap::query()
            ->where([
                ['sport', '=', $game->sport],
                ['season', '=', $game->season],
            ])->orderBy('link_id')->get();
        $sel_link_ids = array_fill_keys($sel_list->unique('link_id'), 0);
        $sel_all = Entities::load($game->sport, $game->season, $sel_link_ids);
        // Now determine all the team selections by our users.
        $sel_user = EntryByPeriod::query()
            ->where([
                ['sport', '=', $game->sport],
                ['season', '=', $game->season],
                ['game_id', '=', $game->game_id],
            ])->whereNotNull('link_id')
            ->whereNotNull('result')
            ->whereIn('user_id', $this->unique('user_id'))
            ->get()
            ->loadSecondary(['period']);

        // And tie together.
        foreach ($this->data as $i => $entry) {
            $this->data[$i]['selections'] = (object)[
                'all' => $sel_all,
                'made' => $sel_user->where('user_id', $entry['user_id']),
            ];
        }
    }

    /**
     * Add the entry's active games to the navigation
     * @return void
     */
    public function addActiveGamesWithinNavigation(): void
    {
        // Request the additional information we need from the root model.
        if (isset($this->secondary['games'])) {
            $this->secondary['games']->loadSecondary(['games']);
        }
        // Populate the mobile dropdown.
        $order = 0;
        foreach (($this?->games_active ?? []) as $game_ref => $games) {
            FrameworkConfig::set(["debear.links.game-$game_ref" => [
                'url' => $games->game->section_url,
                'label' => "<span class=\"sports_{$games->game->sport}_icon\">{$games->game->name}</span>",
                'order' => ++$order,
                'section' => 'my-entry',
                'url-section' => true,
                'navigation' => [
                    'enabled' => true,
                    'class' => 'mobile-only entry-game',
                ],
            ]]);
        }
    }

    /**
     * Add an additional (usually inactive) game the entry's list in the navigation
     * @param Game $game The ORM object of game to append to the navigation.
     * @return void
     */
    public function addOtherGamesWithinNavigation(Game $game): void
    {
        // Get the current order counter, so we can add to the bottom.
        $order = array_reduce(FrameworkConfig::get('debear.links'), function ($carry, $item) {
            $item_order = (($item['section'] ?? '') == 'my-entry' ? $item['order'] : null);
            return (isset($item_order) ? max($carry, $item_order) : $carry);
        }, 0);
        // Now append.
        FrameworkConfig::set([
            "debear.links.game-{$game->game_ref}" => [
                'url' => $game->section_url,
                'label' => "<span class=\"sports_{$game->sport}_icon\">{$game->name}</span>",
                'order' => $order + 1,
                'section' => 'my-entry',
                'url-section' => true,
                'navigation' => [
                    'enabled' => true,
                    'class' => 'mobile-only entry-game',
                ],
            ]
        ]);
    }

    /**
     * Create, and return, a new entry on behalf of the current user
     * @return self The newly created entry
     */
    public static function createForUser(): self
    {
        // Create the entry.
        $user_id = UserModel::object()->id;
        $entry = static::create([
            'user_id' => $user_id,
            'name' => UserModel::object()->forename . '&#39;s Entry',
            'joined' => Time::object()->getServerCurDate(),
            'email_status' => 1,
            'email_type' => 'html',
        ]);
        // Send an email informing the user.
        $args = [
            'type' => $entry->email_type,
            'reason' => 'entry_create',
            'send_time' => 'now',
            'data' => $entry->toMaskedArray(),
        ];
        static::createEmailAdditionalData($args);
        Email::send('Fantasy Records: Create Entry', $user_id, $args);
        // Return the model.
        return $entry;
    }
}
