<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Fantasy\Entities;
use DeBear\Helpers\Fantasy\Records\Traits\API\EntryAchievement as EntryAchievementTrait;
use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as ScoringTrait;
use DeBear\Helpers\Fantasy\Records\Traits\Selection as SelectionTrait;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\Repositories\Time;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;

class EntryByPeriod extends Instance
{
    use EntryAchievementTrait;
    use ScoringTrait;
    use SelectionTrait;

    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['sport', 'season', 'game_id', 'user_id'];
    /**
     * The list of completed periods
     * @var array
     */
    protected $completed_periods = [];

    /**
     * Load the current team's selection for a given game in a given period
     * @param string  $sport     The sport of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $game_id   The ID of the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return self An ORM object containing this entry's selection
     */
    public static function get(string $sport, int $season, int $game_id, int $period_id): self
    {
        return static::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['game_id', '=', $game_id],
                ['user_id', '=', User::object()->id],
                ['period_id', '=', $period_id],
            ])->whereNotNull('link_id')->get()->loadSecondary(['entity']);
    }

    /**
     * Load, if available, the historical selections by the user's team
     * @param string  $sport        The sport of the game being loaded.
     * @param integer $season       The season for the game being loaded.
     * @param integer $game_id      The ID of the game being loaded.
     * @param integer $period_order The order (not ID) of the period being loaded.
     * @return self An ORM object containing this entry's selections of the supplied selection
     */
    public static function getHistorical(string $sport, int $season, int $game_id, int $period_order): self
    {
        $tbl_es = static::getTable();
        $tbl_gp = GamePeriod::getTable();
        return static::query()
            ->select("$tbl_es.*")
            ->join($tbl_gp, function ($join) use ($tbl_gp, $tbl_es, $period_order) {
                $join->on("$tbl_gp.sport", '=', "$tbl_es.sport")
                    ->on("$tbl_gp.season", '=', "$tbl_es.season")
                    ->on("$tbl_gp.period_id", '=', "$tbl_es.period_id")
                    ->where("$tbl_gp.period_order", '<', $period_order);
            })->where([
                ["$tbl_es.sport", '=', $sport],
                ["$tbl_es.season", '=', $season],
                ["$tbl_es.game_id", '=', $game_id],
                ["$tbl_es.user_id", '=', User::object()->id],
            ])->whereNotNull('link_id')->get();
    }

    /**
     * Store (either create or update if existing) an entry's selection
     * @param string  $sport     The sport of the game being processed.
     * @param integer $season    The season for the game being processed.
     * @param integer $game_id   The ID of the game being processed.
     * @param integer $period_id The ID of the period being processed.
     * @param integer $link_id   The ID of the individual selection being processed.
     * @return self The ORM object resulting from this entry's selection creation / update
     */
    public static function store(string $sport, int $season, int $game_id, int $period_id, int $link_id): self
    {
        return static::create([
            'sport' => $sport,
            'season' => $season,
            'game_id' => $game_id,
            'user_id' => User::object()->id,
            'period_id' => $period_id,
            'link_id' => $link_id,
            'sel_saved' => Time::object()->getNowFmt(),
        ])->audit('records_sel_create', 'Selection added', ['period_id', 'link_id']);
    }

    /**
     * Calculate the scoring attributes that (may) have changed since the previous period
     * @return self The current object to allow for chaining
     */
    public function setChangesSinceLastPeriod(): self
    {
        if (!$this->count()) {
            // Skip an empty list.
            return $this;
        }
        // Get the raw changes.
        $tbl_ebp = self::getTable();
        $tbl_gp = GamePeriod::getTable();
        $query = self::query("$tbl_ebp AS ENTRYSEL_CURR")
            ->select([
                'ENTRYSEL_PREV.overall_pos',
                'ENTRYSEL_PREV.current_pos',
                'ENTRYSEL_PREV.hitrate_pos',
                'ENTRYSEL_PREV.achievements',
                'ENTRYSEL_PREV.opponents',
            ])->selectRaw('CONCAT(ENTRYSEL_CURR.sport, "-", ENTRYSEL_CURR.season, "-", ENTRYSEL_CURR.game_id, "-", '
                . 'ENTRYSEL_CURR.user_id, "-", ENTRYSEL_CURR.period_id) AS sel_ref')
            ->join("$tbl_gp AS PERIOD_CURR", function ($join) {
                $join->on('PERIOD_CURR.sport', '=', 'ENTRYSEL_CURR.sport')
                    ->on('PERIOD_CURR.season', '=', 'ENTRYSEL_CURR.season')
                    ->on('PERIOD_CURR.period_id', '=', 'ENTRYSEL_CURR.period_id');
            })->join("$tbl_gp AS PERIOD_PREV", function ($join) {
                $join->on('PERIOD_PREV.sport', '=', 'PERIOD_CURR.sport')
                    ->on('PERIOD_PREV.season', '=', 'PERIOD_CURR.season')
                    ->on('PERIOD_PREV.period_order', '=', DB::raw('PERIOD_CURR.period_order - 1'));
            })->leftJoin("$tbl_ebp AS ENTRYSEL_PREV", function ($join) {
                $join->on('ENTRYSEL_PREV.sport', '=', 'ENTRYSEL_CURR.sport')
                    ->on('ENTRYSEL_PREV.season', '=', 'ENTRYSEL_CURR.season')
                    ->on('ENTRYSEL_PREV.game_id', '=', 'ENTRYSEL_CURR.game_id')
                    ->on('ENTRYSEL_PREV.user_id', '=', 'ENTRYSEL_CURR.user_id')
                    ->on('ENTRYSEL_PREV.period_id', '=', 'PERIOD_PREV.period_id');
            });
        foreach ($this->data as $row) {
            $query->orWhere(function ($where) use ($row) {
                $where->where("ENTRYSEL_CURR.sport", '=', $row['sport'])
                    ->where("ENTRYSEL_CURR.season", '=', $row['season'])
                    ->where("ENTRYSEL_CURR.game_id", '=', $row['game_id'])
                    ->where("ENTRYSEL_CURR.user_id", '=', $row['user_id'])
                    ->where("ENTRYSEL_CURR.period_id", '=', $row['period_id']);
            });
        }
        $prev_scores = $query->get();
        // Process to identify appropriate changes.
        foreach ($this->data as $i => $entry_sel) {
            $prev = $prev_scores->where('sel_ref', "{$entry_sel['sport']}-{$entry_sel['season']}-"
                . "{$entry_sel['game_id']}-{$entry_sel['user_id']}-{$entry_sel['period_id']}");
            $this->data[$i]['overall_chg'] = (isset($prev?->overall_pos)
                ? $prev->overall_pos - $entry_sel['overall_pos'] : null);
            $this->data[$i]['current_chg'] = (isset($prev?->current_pos)
                ? $prev->current_pos - $entry_sel['current_pos'] : null);
            $this->data[$i]['hitrate_chg'] = (isset($prev?->hitrate_pos)
                ? $prev->hitrate_pos - $entry_sel['hitrate_pos'] : null);
            $this->data[$i]['achievements_chg'] = $entry_sel['achievements'] - ($prev?->achievements ?? 0);
            $this->data[$i]['opponents_chg'] = $entry_sel['opponents'] - ($prev?->opponents ?? 0);
        }
        $this->hideFieldsFromAPI(['overall_chg', 'current_chg', 'hitrate_chg', 'achievements_chg', 'opponents_chg']);
        // Return ourselves to allow for chaining.
        return $this;
    }

    /**
     * Perform some preperatory steps before rendering API data
     * @return void
     */
    protected function preProcessAPIData(): void
    {
        list($sport) = $this->unique('sport');
        list($season) = $this->unique('season');
        $db_tz = FrameworkConfig::get("debear.sports.subsites.$sport.datetime.timezone_db");
        $cur_date = Time::object()->getAltTimezoneCurDate($db_tz);
        $periods = GamePeriod::query()
            ->select('period_id')
            ->selectRaw('end_date < ? AS is_complete', [$cur_date])
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
            ])->whereIn('period_id', $this->unique('period_id'))
            ->get();
        $this->completed_periods = array_combine($periods->unique('period_id'), $periods->unique('is_complete'));
    }

    /**
     * Manipulate an entry for its API representation
     * @param array $row The row data (passed by reference) to be manipulated.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        foreach (['score', 'result'] as $field) {
            if (isset($row[$field])) {
                $row[$field] = $this->formatScoreAPI($row[$field]);
            }
        }
        // Any achievements completed or opponents beaten in this period?
        if (isset($this->apiCustomData['game']) && isset($row['achievements_chg'])) {
            $sel_ref = "{$row['sport']}-{$row['season']}-{$row['game_id']}-{$row['user_id']}-{$row['period_id']}";
            $entry_sel = $this->filter(function ($r) use ($sel_ref) {
                return "{$r['sport']}-{$r['season']}-{$r['game_id']}-{$r['user_id']}-{$r['period_id']}" == $sel_ref;
            });
            $achievements = [];
            if (($entry_sel?->achievements_chg ?? 0) > 0) {
                foreach ($this->apiCustomData['game']->achievements as $achieve) {
                    $achievements[$achieve->achieve_id] = [];
                    foreach ($achieve->levels as $level) {
                        if ($entry_sel->achievements_chg_map[$level->bit_flag]) {
                            $achievements[$level->achieve_id][$level->level] = true;
                        }
                    }
                }
            }
            $row['achieved'] = [
                'achievements' => array_filter($achievements),
                'opponent' => (($entry_sel?->opponents_chg ?? 0) > 0),
            ];
        }
        // Replace our object with a slimmed down version (we control).
        if (isset($row['link_id'])) {
            $row['lock_time'] = [
                'status' => !$row['entity']->eventActive() ? $row['entity']->event_status : null,
                'raw' => (string)$row['entity']->lock_time->system?->format('c'),
            ];
            $row['entity'] = $row['entity']->rowToSummaryAPI();
        } else {
            $row['link_id'] = null;
            unset($row['entity']);
            $row['lock_time'] = null; // Lock time will be converted to the period's lock time.
        }
        // Remove stats if we're viewing a completed period.
        if (isset($this->completed_periods[$row['period_id']]) && $this->completed_periods[$row['period_id']]) {
            unset($row['stats']);
        }
    }

    /**
     * Convert the current row (object) into a web-ready payload
     * @param array $opt An optional array of customisations.
     * @return array The current row rendered into a web-ready array
     */
    public function rowToWeb(?array $opt = []): array
    {
        // Adapt from our standard API call.
        $row = $this->toMaskedArray();
        return empty($row) ? [] : [
            'link_id' => $row['link_id'],
            'result' => $row['result'],
            'stress' => $row['stress'],
            'rating' => static::convertRating($row['rating']),
            'opp_rating' => static::convertRating($row['opp_rating']),
            'status' => $row['status'],
            'summary' => $row['summary'],
            'score' => $this->formatScore($row['score']),
            'entity' => $row['entity']->rowToSummaryWeb($opt['img_size'] ?? null),
        ];
    }

    /**
     * Load from the appropriate database table, the entities from an earlier query
     * @return void
     */
    public function loadSecondaryEntity(): void
    {
        // Determine the sport and season (which relies on the base query applying to only a single sport/season combo).
        list($sport) = $this->unique('sport');
        list($season) = $this->unique('season');
        // Get the appropriate period value for our entity loader and link to each entity.
        $periods_raw = GamePeriod::query()
            ->select('period_id')
            ->selectRaw(FrameworkConfig::get("debear.sports.$sport.period.ref_col") . ' AS period_col')
            ->where('sport', '=', $sport)
            ->where('season', '=', $season)
            ->whereIn('period_id', $this->unique('period_id'))
            ->get()
            ->getData();
        $period_map = array_column($periods_raw, 'period_col', 'period_id');
        $load_ids = [];
        foreach ($this->data as $entity) {
            if (!isset($load_ids[$entity['link_id']])) {
                $load_ids[$entity['link_id']] = [$period_map[$entity['period_id']]];
            } else {
                $load_ids[$entity['link_id']][] = $period_map[$entity['period_id']];
            }
        }
        // Load and link.
        $entities = Entities::load($sport, $season, $load_ids);
        foreach ($this->data as $i => $entity) {
            $entity_ref = $entity['link_id'] . ':' . $period_map[$entity['period_id']];
            $this->data[$i]['entity'] = $entities->where('entity_ref', $entity_ref);
        }
    }

    /**
     * Load from the appropriate database table, the selection scoring info from an earlier query
     * @return void
     */
    public function loadSecondaryScoring(): void
    {
        $query = Selection::query();
        foreach ($this->data as $entry_sel) {
            $query->orWhere(function (Builder $where) use ($entry_sel) {
                $where->where([
                    ['sport', '=', $entry_sel['sport']],
                    ['season', '=', $entry_sel['season']],
                    ['game_id', '=', $entry_sel['game_id']],
                    ['link_id', '=', $entry_sel['link_id']],
                    ['period_id', '=', $entry_sel['period_id']],
                ]);
            });
        }
        $sel = $query->get();
        // Now tie-back together.
        foreach ($this->data as $i => $entity_sel) {
            $this->data[$i]['scoring'] = $sel->where('sport', $entity_sel['sport'])
                ->where('season', $entity_sel['season'])
                ->where('game_id', $entity_sel['game_id'])
                ->where('link_id', $entity_sel['link_id'])
                ->where('period_id', $entity_sel['period_id']);
        }
    }

    /**
     * Load the period information for these selections
     * @return void
     */
    public function loadSecondaryPeriod(): void
    {
        list($sport) = $this->unique('sport');
        list($season) = $this->unique('season');
        $periods = GamePeriod::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
            ])->whereIn('period_id', $this->unique('period_id'))
            ->get();
        // Now tie-back together.
        foreach ($this->data as $i => $entity_sel) {
            $this->data[$i]['period'] = $periods->where('period_id', $entity_sel['period_id']);
        }
    }
}
