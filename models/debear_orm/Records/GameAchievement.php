<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use Illuminate\Database\Query\Builder;

class GameAchievement extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['sport', 'season', 'game_id', 'type', 'disp_order', 'game_ref'];

    /**
     * Load the levels for each of the loaded achievements
     * @return void
     */
    protected function loadSecondaryLevels(): void
    {
        $tbl_a = GameAchievement::getTable();
        $tbl_l = GameAchievementLevel::getTable();
        $achieve_concat = "$tbl_l.sport, '-', $tbl_l.season, '-', $tbl_l.game_id, '-', $tbl_l.achieve_id";
        $query = GameAchievementLevel::query()
            ->select(["$tbl_l.*", "$tbl_a.type"])
            ->selectRaw("CONCAT($achieve_concat) AS achieve_ref")
            ->join($tbl_a, function ($join) use ($tbl_a, $tbl_l) {
                $join->on("$tbl_a.sport", '=', "$tbl_l.sport")
                    ->on("$tbl_a.season", '=', "$tbl_l.season")
                    ->on("$tbl_a.game_id", '=', "$tbl_l.game_id")
                    ->on("$tbl_a.achieve_id", '=', "$tbl_l.achieve_id");
            });
        foreach ($this->data as $i => $achieve) {
            $query->orWhere(function (Builder $where) use ($tbl_l, $achieve) {
                $where->where([
                    ["$tbl_l.sport", '=', $achieve['sport']],
                    ["$tbl_l.season", '=', $achieve['season']],
                    ["$tbl_l.game_id", '=', $achieve['game_id']],
                    ["$tbl_l.achieve_id", '=', $achieve['achieve_id']],
                ]);
            });
        }
        $this->secondary['levels'] = $query->orderBy('achieve_id')->orderBy('disp_order')->get()
            ->hideFieldsFromAPI(['type'])
            ->setMultipleSecondary();
        // Now tie back together.
        foreach ($this->data as $i => $a) {
            $this->data[$i]['levels'] = $this->secondary['levels']
                ->where('achieve_ref', "{$a['sport']}-{$a['season']}-{$a['game_id']}-{$a['achieve_id']}");
        }
    }

    /**
     * Perform any final post-processing on the API data before sending back
     * @param array $row The rendered data for a single row to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        // Remove the value field for 'event' achievements.
        if ($row['type'] == 'event') {
            foreach ($row['levels'] as &$level) {
                unset($level['value']);
            }
        }
    }
}
