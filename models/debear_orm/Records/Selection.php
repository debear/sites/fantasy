<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\ORM\Base\Helpers\QueryBuilder;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Common\SelectionMap;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Fantasy\Entities;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as TraitScoring;
use DeBear\Helpers\Fantasy\Records\Traits\Selection as TraitSelection;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;

class Selection extends Instance
{
    use TraitScoring;
    use TraitSelection;

    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        '2dp' => ['sel_pct'],
    ];
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['sport', 'season', 'game_id', 'entity'];
    /**
     * The list of various overrides we could apply to our data
     * @var array
     */
    protected $overrides = [];
    /**
     * The additional information of the selection's recent period(s)
     * @var GamePeriod
     */
    protected $periods;
    /**
     * The additional information about the selection during their recent period(s)
     * @var static
     */
    protected $periodInfo;

    /**
     * Load information about an individual selection on a given game period
     * @param string  $sport     The sport of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $game_id   The ID of the game being loaded.
     * @param integer $link_id   The ID of the individual selection being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return self An ORM object containing the information of a single selection
     */
    public static function get(string $sport, int $season, int $game_id, int $link_id, int $period_id): self
    {
        return static::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['game_id', '=', $game_id],
                ['link_id', '=', $link_id],
                ['period_id', '=', $period_id],
            ])->get()
            ->loadSecondary(['entity']);
    }

    /**
     * Validate that selections are available for the given period
     * @param string  $sport     The sport of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $game_id   The ID of the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return boolean True if the pre-calculations have been applied to the supplied period
     */
    public static function validatePeriod(string $sport, int $season, int $game_id, int $period_id): bool
    {
        return static::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['game_id', '=', $game_id],
                ['period_id', '=', $period_id],
            ])->count() > 0;
    }

    /**
     * Load the selections for a given game period
     * @param string  $sport     The sport of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $game_id   The ID of the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @param array   $args      Query customisations.
     * @return self An ORM object containing the paged selection list
     */
    public static function loadByPeriod(string $sport, int $season, int $game_id, int $period_id, array $args): self
    {
        // Ensure our arguments have default values.
        $game = Game::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['game_id', '=', $game_id],
            ])->get();
        $args = array_merge([
            'page' => 1,
            'per_page' => FrameworkConfig::get('debear.setup.selections.edit_modal_rows'),
            // True: include locked selections (i.e.,, "show all"); False: exclude locked selections.
            'locked' => !$game->isMajorLeague() || $game->isSelTypeTeam(),
            // True: include beaten opponents (i.e., "show all"); False: exclude beaten opponents.
            'beaten_opp' => true,
            'team_id' => false,
            'sort' => 'rating',
        ], $args);
        // Start with the core part of our query.
        $tbl_s = static::getTable();
        $query = static::query()
            ->select("$tbl_s.*")
            ->where([
                ["$tbl_s.sport", '=', $sport],
                ["$tbl_s.season", '=', $season],
                ["$tbl_s.game_id", '=', $game_id],
                ["$tbl_s.period_id", '=', $period_id],
            ])->paginate($args['page'], $args['per_page']);

        // Major League filtering may require additional table joins.
        if ($game->canFilterSelections() && (!$args['locked'] || $args['team_id'] || !$args['beaten_opp'])) {
            static::loadByPeriodMajorLeagueFilters($query, $sport, $season, $game, $period_id, $args);
        }

        // What is our sort order?
        if (is_numeric($args['sort'])) {
            $tbl_stats = SelectionStat::getTable();
            $query->join($tbl_stats, function ($join) use ($tbl_stats, $tbl_s, $args) {
                $join->on("$tbl_stats.sport", '=', "$tbl_s.sport")
                    ->on("$tbl_stats.season", '=', "$tbl_s.season")
                    ->on("$tbl_stats.game_id", '=', "$tbl_s.game_id")
                    ->on("$tbl_stats.link_id", '=', "$tbl_s.link_id")
                    ->on("$tbl_stats.period_id", '=', "$tbl_s.period_id")
                    ->where("$tbl_stats.stat_id", '=', $args['sort']);
            })->orderBy("$tbl_stats.stat_order", 'ASC')
                ->orderBy("$tbl_s.rating", 'DESC')
                ->orderBy("$tbl_s.opp_rating", 'DESC');
        } else {
            // Currently, both ratings are descending sorts.
            $sorts = array_flip(array_merge([
                'rating' => 1,
                'opp_rating' => 2,
            ], [$args['sort'] => 0]));
            ksort($sorts);
            foreach ($sorts as $sort) {
                $query->orderBy("$tbl_s.$sort", 'DESC');
            }
            // Always sort by link_id to ensure deterministic results.
            $query->orderBy("$tbl_s.link_id");
        }

        // Now run our query and return the results.
        return $query->get()->loadSecondary(['entity', 'stats']);
    }

    /**
     * Customise the base selection loading query with Major League filtering
     * @param QueryBuilder $query     Original query to be updated.
     * @param string       $sport     The sport of the game being loaded.
     * @param integer      $season    The season for the game being loaded.
     * @param Game         $game      ORM object of the game being loaded.
     * @param integer      $period_id The ID of the period being loaded.
     * @param array        $args      Query customisations.
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength) Until this can be better broken down.
     */
    protected static function loadByPeriodMajorLeagueFilters(
        QueryBuilder $query,
        string $sport,
        int $season,
        Game $game,
        int $period_id,
        array $args
    ): void {
        $tbl_s = static::getTable();
        $period = GamePeriod::query()->where([
            ['sport', '=', $sport],
            ['season', '=', $season],
            ['period_id', '=', $period_id],
        ])->get();

        if ($game->isSelTypeIndividual()) {
            // Join to the Roster table for the selection info.
            $tbl_info = Namespaces::callStatic($sport, 'TeamRoster', 'getTable');
            $tbl_info_team_col = "$tbl_info.team_id";
            $db_info = Namespaces::callStatic($sport, 'TeamRoster', 'getDatabase');
            // Determine the join clause.
            $join_type = FrameworkConfig::get("debear.sports.{$period->sport}.period.sport_col");
            $the_date = $period->end_date->toDateString();
            if ($join_type == 'game_date') {
                $the_date = Namespaces::callStatic($sport, 'TeamRoster', 'query')
                    ->selectRaw('MAX(the_date) AS max_date')
                    ->where([
                        ['season', '=', $season],
                        ['the_date', '<=', $the_date],
                    ])->get()->max_date;
            }
            $joins = [
                'week' => [
                    ["$tbl_info.game_type", '=', 'regular'],
                    ["$tbl_info.week", '=', $period->period_id],
                ],
                'game_date' => [
                    ["$tbl_info.the_date", '=', $the_date],
                ],
            ];
            $join_info = $joins[$join_type];
            // Add the join.
            $query->join("$db_info.$tbl_info", function ($join) use ($tbl_info, $tbl_s, $join_info) {
                $join->on("$tbl_info.season", '=', "$tbl_s.season")
                    ->where($join_info)
                    ->on("$tbl_info.player_id", '=', "$tbl_s.link_id");
            });
        } else {
            // Join to the mapping table for additional info.
            $tbl_info = SelectionMap::getTable();
            $tbl_info_team_col = "$tbl_info.raw_id";
            $query->join($tbl_info, function ($join) use ($tbl_info, $tbl_s) {
                $join->on("$tbl_info.sport", '=', "$tbl_s.sport")
                    ->on("$tbl_info.season", '=', "$tbl_s.season")
                    ->on("$tbl_info.link_id", '=', "$tbl_s.link_id");
            });
        }

        // Some filters need to include a join to the schedule info.
        if (!$args['locked'] || !$args['beaten_opp']) {
            // Requires an additional join to the Schedule.
            $tbl_sched = Namespaces::callStatic($sport, 'Schedule', 'getTable');
            $db_tbl_sched = Namespaces::callStatic($sport, 'Schedule', 'getDatabase') . ".$tbl_sched";
            $query->join($db_tbl_sched, function ($join) use ($tbl_sched, $tbl_info, $period, $tbl_info_team_col) {
                $join->on("$tbl_sched.season", '=', "$tbl_info.season")
                    ->where("$tbl_sched.game_type", '=', 'regular')
                    ->whereBetween("$tbl_sched.game_date", [
                        $period->start_date->toDateString(),
                        $period->end_date->toDateString(),
                    ])->whereRaw("$tbl_info_team_col IN ($tbl_sched.home, $tbl_sched.visitor)");
            })->selectRaw("MIN(CONCAT($tbl_sched.game_date, ' ', $tbl_sched.game_time)) AS event_time")
                ->groupBy("$tbl_s.sport", "$tbl_s.season", "$tbl_s.game_id", "$tbl_s.link_id", "$tbl_s.period_id");
        }

        // Are we including locked selections?
        if (!$args['locked']) {
            // Apply our 'locked' filter.
            $db_tz = FrameworkConfig::get("debear.sports.subsites.$sport.datetime.timezone_db");
            $query->havingRaw("DATE_SUB(event_time, INTERVAL ? MINUTE) > ?", [
                FrameworkConfig::get('debear.setup.lock_offset'),
                Time::object()->getAltTimezoneNowFmt($db_tz)
            ]);
        }

        // Are we loading selections from a specific team?
        if ($args['team_id']) {
            $query->where($tbl_info_team_col, '=', $args['team_id']);
        }

        // Are we only loading selections with an as-yet-undefeated opponent?
        if (!$args['beaten_opp']) {
            // Determine the pool of opponents.
            $game->loadSecondary(['achievements']);
            $raw_opp = $game->opponents->getData();
            array_unshift($raw_opp, false); // Make our keys overlap (opps are zero-index, bit flags one-indexed).
            // Determine opponents already defeated by the user.
            $opponents_map = EntryScore::query()
                ->select('opponents')
                ->where([
                    ['sport', '=', $game->sport],
                    ['season', '=', $game->season],
                    ['game_id', '=', $game->game_id],
                    ['user_id', '=', User::object()->id],
                ])->get()->opponents_map;
            $beaten = array_column(array_intersect_key($raw_opp, array_filter($opponents_map)), 'raw_id');
            // Exclude these achieved opponents from the query.
            $tbl_sched = Namespaces::callStatic($sport, 'Schedule', 'getTable');
            $opp_where = DB::raw("IF($tbl_sched.home = $tbl_info_team_col, $tbl_sched.visitor, $tbl_sched.home)");
            $query->whereNotIn($opp_where, $beaten);
        }
    }

    /**
     * Load from the appropriate database table, the entities from an earlier query
     * @return void
     */
    public function loadSecondaryEntity(): void
    {
        // Determine the sport and season (which relies on the base query applying to only a single sport/season combo).
        list($sport) = $this->unique('sport');
        list($season) = $this->unique('season');
        // Get the appropriate period value for our entity loader and link to each entity.
        list($period_id) = $this->unique('period_id');
        $period = GamePeriod::query()
            ->selectRaw(FrameworkConfig::get("debear.sports.$sport.period.ref_col") . ' AS period_col')
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['period_id', '=', $period_id],
            ])->get();
        // Load and link.
        $entities = Entities::load($sport, $season, array_fill_keys($this->unique('link_id'), $period->period_col));
        foreach ($this->data as $i => $entity) {
            $this->data[$i]['entity'] = $entities->where('entity_ref', "{$entity['link_id']}:{$period->period_col}");
        }
    }

    /**
     * Get additional information about the selection's recent results
     * @return void
     */
    public function loadSecondaryRecentResults(): void
    {
        // Determine the current period.
        $period = GamePeriod::query()
            ->where('sport', $this->sport)
            ->where('season', $this->season)
            ->where('period_id', $this->period_id)
            ->get();
        // Load the info (at this time, assumes query is for a single selection).
        $tbl_sel = static::getTable();
        $tbl_period = GamePeriod::getTable();
        $this->secondary['recent'] = static::query()
            ->select("$tbl_sel.*")
            ->join($tbl_period, function ($join) use ($tbl_period, $tbl_sel) {
                $join->on("$tbl_period.sport", '=', "$tbl_sel.sport")
                    ->on("$tbl_period.season", '=', "$tbl_sel.season")
                    ->on("$tbl_period.period_id", '=', "$tbl_sel.period_id");
            })->where("$tbl_sel.sport", $this->sport)
            ->where("$tbl_sel.season", $this->season)
            ->where("$tbl_sel.game_id", $this->game_id)
            ->where("$tbl_sel.link_id", $this->link_id)
            ->where("$tbl_period.period_order", '<=', $period->period_order)
            ->orderByDesc("$tbl_period.period_order")
            ->limit(FrameworkConfig::get('debear.setup.selections.view_modal_history'))
            ->get()
            ->setMultipleSecondary()
            ->hideFieldsFromAPI(['link_id']);
        foreach ($this->data as $i => $sel) {
            $this->data[$i]['recent'] = $this->secondary['recent']->where('link_id', $sel['link_id']);
        }
    }

    /**
     * Store with the raw selection data, any selections the user cannot use
     * @param GamePeriod $period The appropriate game period being processed.
     * @return void
     */
    public function setUnavailable(GamePeriod $period): void
    {
        $historical = EntryByPeriod::getHistorical($this->sport, $this->season, $this->game_id, $period->period_order);
        $this->overrides['available'] = ['raw' => $historical->unique('link_id')];
    }

    /**
     * Store with the list of opponents determined to be a streak buster
     * @return void
     */
    public function setStreakBusters(): void
    {
        if ($this->count()) {
            list($sport) = $this->unique('sport');
            list($season) = $this->unique('season');
            list($game_id) = $this->unique('game_id');
            $busters = EntryBusters::load($sport, $season, $game_id);
        }
        $this->overrides['busters'] = array_fill_keys(array_keys($busters ?? []), true);
    }

    /**
     * Store the list of opponents in the pool the user has already beaten
     * @param Game       $game       The appropriate game being loaded.
     * @param EntryScore $entryScore The appropriate users score for the game being processed.
     * @return void
     */
    public function setAchievedOpponents(Game $game, EntryScore $entryScore): void
    {
        // Get the raw list of available opponents.
        $raw = $game->opponents->getData();
        array_unshift($raw, false); // Make our keys overlap (opps are zero-index, bit flags one-indexed).
        // Use the bit flags and array indices to determine the beaten opponents.
        $beaten = array_column(array_intersect_key($raw, array_filter($entryScore->opponents_map)), 'raw_id');
        $this->overrides['achieve_opp'] = array_fill_keys($beaten, true);
    }

    /**
     * Convert a single row into a web-ready payload
     * @param array $opt An optional array of customisations.
     * @return array The data to converted to an array
     */
    public function rowToWeb(?array $opt = []): array
    {
        return $this->processToWeb($this->toMaskedArray(), $opt);
    }

    /**
     * Convert the data into a web-ready payload
     * @param array $opt An optional array of customisations.
     * @return array The data in the object converted to an array
     */
    public function rowsToWeb(?array $opt = []): array
    {
        return array_merge(['sel' => $this->processToWeb($this->data, $opt)], $this->resultsPagination());
    }

    /**
     * Processor method for converting data to a web-ready payload
     * @param array $data The actual row data to be processed and converted.
     * @param array $opt  An optional array of customisations.
     * @return array The passed data to converted to an array
     */
    protected function processToWeb(array $data, ?array $opt = []): array
    {
        $single_row = $data && Arrays::isAssociative($data);
        // Determine the display format for the lock time.
        list($sport) = ($data ? array_unique(array_column($data, 'sport')) : ['unknown']);
        $fmt = FrameworkConfig::get("debear.sports.{$sport}.period.format");
        $s = FrameworkConfig::get('debear.setup.event_statuses');
        // Loop through and process each selection.
        $this->preProcessAPIData();
        // Some fields only apply in certain conditions.
        $flag_exclude = array_filter([
            // Streak Buster is only available in Major League streak games.
            'buster' => !isset($this->overrides['busters']),
            // Achieved Opponent is only available in games with an Opponents achievement.
            'achieve_opp' => !isset($this->overrides['achieve_opp']),
        ]);
        // Process the rows into an array entry.
        $ret = [];
        foreach (($single_row ? [$data] : $data) as $row) {
            $l = $row['entity']->lock_time;
            $ret[] = [
                'link_id' => $row['link_id'],
                'link' => $row['entity']->rowToSummaryWeb($opt['img_size'] ?? null),
                'lock_time' => [
                    'status' => !$row['entity']->eventActive() ? $row['entity']->event_status : null,
                    'raw' => isset($l->system) ? (string)$l->system->format('c') : null,
                    'disp' => isset($l->system) ? (string)$l->system->format($fmt) : $s[$row['entity']->event_status],
                ],
                'weather' => $row['entity']->weatherToWeb(),
                'flags' => array_diff_key([
                    'available' => $this->overrides['available']['proc'][$row['link_id']] ?? true,
                    'buster' => $this->overrides['busters'][$row['entity']?->info_icon ?? '*unset*'] ?? false,
                    'achieve_opp' => $this->overrides['achieve_opp'][$row['entity']?->info_icon ?? '*unset*'] ?? false,
                ], $flag_exclude),
                'rating' => static::convertRating($row['rating']),
                'opp_rating' => static::convertRating($row['opp_rating']),
                'stats' => $row['stats'],
                'recent' => isset($row['recent']) ? $row['recent']->recentToWeb() : null,
                'note' => $row['note'],
            ];
        }
        return $single_row ? $ret[0] : $ret;
    }

    /**
     * Convert the recent event data into a web-ready payload
     * @return array The data to converted to an array
     */
    public function recentToWeb(): array
    {
        $data = $this->getData();
        // Load info about the periods.
        list($sport) = array_unique(array_column($data, 'sport'));
        list($season) = array_unique(array_column($data, 'season'));
        $periods = GamePeriod::query()
            ->where('sport', $sport)
            ->where('season', $season)
            ->whereIn('period_id', array_unique(array_column($data, 'period_id')))
            ->get();
        // Then the info for the selection during each period.
        $period_col = FrameworkConfig::get("debear.sports.$sport.period.ref_col");
        list($link_id) = array_unique(array_column($data, 'link_id'));
        $period_info = Entities::load($sport, $season, [$link_id => $periods->unique($period_col)]);
        // Finally, parse each row.
        $ret = [];
        foreach ($data as $row) {
            $period = $periods->where('period_id', $row['period_id']);
            $info = $period_info->where('entity_ref', "{$row['link_id']}:{$period->$period_col}");
            $ret[] = [
                'period_id' => $row['period_id'],
                'period' => [
                    'short' => $period->name_short_disp,
                    'long' => $period->name_disp,
                ],
                'link' => [
                    'status' => isset($info->status) ? $info->display_status : null,
                    'info' => $info->display_info ?: null,
                ],
                'rating' => static::convertRating($row['rating']),
                'opp_rating' => static::convertRating($row['opp_rating']),
                'score' => isset($row['score']) ? $this->formatScore($row['score']) : null,
                'status' => $row['status'],
                'stress' => isset($row['stress']) ? intval(($row['stress'] / 255) * 100) : null,
                'summary' => $row['summary'],
                'usage' => isset($row['sel_pct']) ? floatval($row['sel_pct']) : null,
            ];
        }
        return $ret;
    }

    /**
     * Perform some preperatory steps before rendering API data
     * @return void
     */
    protected function preProcessAPIData(): void
    {
        if (isset($this->overrides['available']) && !isset($this->overrides['available']['proc'])) {
            $this->overrides['available']['proc'] = Arrays::merge(
                array_fill_keys($this->unique('link_id'), true),
                array_fill_keys($this->overrides['available']['raw'], false)
            );
        }
        // If this is part of a 1:M relationship, extract the additional period info.
        if (isset($this->secondaryMultiple) && $this->secondaryMultiple) {
            list($sport) = array_unique(array_column($this->getData(), 'sport'));
            list($season) = array_unique(array_column($this->getData(), 'season'));
            list($link_id) = array_unique(array_column($this->getData(), 'link_id'));
            $period_col = FrameworkConfig::get("debear.sports.$sport.period.ref_col");
            $this->periods = GamePeriod::query()
                ->where('sport', $sport)
                ->where('season', $season)
                ->whereIn('period_id', array_unique(array_column($this->getData(), 'period_id')))
                ->get()
                ->hideFieldsFromAPI(['period_id', 'period_order']);
            $this->periodInfo = Entities::load($sport, $season, [$link_id => $this->periods->unique($period_col)]);
        }
    }

    /**
     * Perform any final post-processing on the API data before sending back
     * @param array $row The rendered data for a single row to be post-processed.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        unset($row['event_time']);
        if (isset($row['score'])) {
            $row['score'] = $this->formatScoreAPI($row['score']);
        }
        // Modify the order whilst we fill-out the entity info.
        if (isset($row['entity'])) {
            // Some fields only apply in certain conditions.
            $flag_exclude = array_filter([
                // Streak Buster is only available in Major League streak games.
                'buster' => !isset($this->overrides['busters']),
                // Achieved Opponent is only available in games with an Opponents achievement.
                'achieve_opp' => !isset($this->overrides['achieve_opp']),
            ]);
            $l = $row['entity']->lock_time;
            $row = array_merge(array_intersect_key($row, ['link_id' => true]), [
                'link' => $row['entity']->rowToSummaryAPI(),
                'weather' => $row['entity']->weatherToAPI(),
                'lock_time' => [
                    'status' => !$row['entity']->eventActive() ? $row['entity']->event_status : null,
                    'raw' => isset($l->system) ? (string)$l->system->format('c') : null,
                ],
                'flags' => array_diff_key([
                    'available' => $this->overrides['available']['proc'][$row['link_id']] ?? true,
                    'buster' => $this->overrides['busters'][$row['entity']?->info_icon ?? '*unset*'] ?? false,
                    'achieve_opp' => $this->overrides['achieve_opp'][$row['entity']?->info_icon ?? '*unset*'] ?? false,
                ], $flag_exclude),
            ], $row);
        }
        // If applicable, expand on the period info.
        if (isset($this->periodInfo) && $this->periodInfo->isset()) {
            $period_col = FrameworkConfig::get("debear.sports.{$row['sport']}.period.ref_col");
            $period = $this->periods->where('period_id', $row['period_id']);
            $info = $this->periodInfo->where('entity_ref', "{$row['link_id']}:{$period->$period_col}");
            $row = array_merge(array_intersect_key($row, ['period_id' => true]), [
                'period' => $period->rowToAPI(),
                'info' => $info->info ?: null,
                'info_icon' => $info->info_icon,
            ], array_diff_key($row, ['period_id' => true]));
        }
    }
}
