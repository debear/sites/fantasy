<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;

class GameAchievementLevel extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['sport', 'season', 'game_id', 'achieve_id', 'bit_flag', 'achieve_ref', 'disp_order'];

    /**
     * Convert the raw level value into a display string
     * @return string A display representation of the numeric level
     */
    public function getValueDispAttribute(): string
    {
        if ($this->type == 'event' && $this->sport == 'mlb' && $this->value == 1) {
            // Bespoke Num:String conversion.
            return 'Yes';
        } elseif ($this->type == 'sel_usage') {
            // Append the % symbol to usage-based achievements.
            return "&le;{$this->value}%";
        }
        // Default format is to num format the value.
        return number_format($this->value);
    }
}
