<?php

namespace DeBear\ORM\Fantasy\Records;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Base\Instance;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\Helpers\Fantasy\Entities;
use DeBear\Helpers\Highcharts;
use Carbon\Carbon;

class GamePeriodSummary extends Instance
{
    /**
     * Attributes not to be included when representing a row in an API response
     * @var array
     */
    protected $hidden = ['sport', 'season', 'game_id', 'period_id', 'when_tweeted', 'when_synced'];
    /**
     * Attributes not to be formatted when representing a row in an API response
     * @var array
     */
    protected $apiFormats = [
        'float' => ['entry_pos_pct', 'sel_pos_pct', 'popular_pos_pct', 'popular_neg_pct'],
    ];

    /**
     * Build the base Highcharts configuration for the Success Rate gauge
     * @param string     $dom_id DOM element ID the chart will be rendered within.
     * @param float|null $val    The value for the gauge.
     * @return array Appropriate highcharts configurations for the chart
     */
    public function successChart(string $dom_id, ?float $val): array
    {
        // Breakdown as a pie chart.
        $chart = Highcharts::new($dom_id, ['styled-mode' => false, 'js-module' => 'solid-gauge']);
        // Customise - type.
        $chart['chart']['type'] = 'solidgauge';
        // Customise - plot options.
        $chart['plotOptions'] = [
            'solidgauge' => [
                'innerRadius' => '40%',
                'dataLabels' => ['y' => 5, 'borderWidth' => 0, 'useHTML' => true],
            ],
        ];
        // Customise - pane.
        $chart['pane'] = [
            'startAngle' => -90,
            'endAngle' => 90,
            'background' => [
                'backgroundColor' => 'rgba(0, 0, 0, 0.05)',
                'innerRadius' => '40%',
                'outerRadius' => '100%',
                'shape' => 'arc'
            ],
        ];

        // Customise - yAxis.
        $chart['yAxis'] = [
            'stops' => [[0.1, '#DF5353'], [0.5, '#DDDF0D'], [0.9, '#55BF3B']],
            'lineWidth' => 0,
            'minorTickInterval' => null,
            'min' => 0,
            'max' => 100,
            'tickAmount' => 2,
            'tickWidth' => 0,
            'labels' => ['y' => 16, 'format' => '{value}%'],
        ];
        // Customise - series.
        $chart['series'] = [['dataLabels' => ['enabled' => true, 'format' => '{y}%'], 'data' => [$val ?? 0]]];
        // Customise - tooltips.
        $chart['tooltip'] = ['enabled' => false];
        return $chart;
    }

    /**
     * Build the Highcharts configuration for the Entry Success Rate gauge
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the chart
     */
    public function entrySuccessChart(string $dom_id): array
    {
        return $this->successChart($dom_id, $this->entry_pos_pct);
    }

    /**
     * Build the Highcharts configuration for the Selection Success Rate gauge
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the chart
     */
    public function selSuccessChart(string $dom_id): array
    {
        return $this->successChart($dom_id, $this->sel_pos_pct);
    }

    /**
     * Load details of the entities in the summary row
     * @return void
     */
    protected function loadSecondaryEntities(): void
    {
        // Determine the entities we need to load, and which period they relate to.
        $period_key = FrameworkConfig::get("debear.sports.{$this->sport}.period.ref_col");
        $periods = GamePeriod::query()->where([
            ['sport', '=', $this->sport],
            ['season', '=', $this->season],
        ])->whereIn('period_id', $this->unique('period_id'))->get();
        $map = array_fill_keys(array_unique(array_merge(
            $this->unique('popular_pos_link_id'),
            $this->unique('popular_neg_link_id'),
        )), []);
        foreach ($this->getData() as $row) {
            $raw = $periods->where('period_id', $row['period_id'])->{$period_key};
            $period = $raw instanceof Carbon ? $raw->toDateString() : $raw;
            $map[$row['popular_pos_link_id']][] = $period;
            $map[$row['popular_neg_link_id']][] = $period;
        }
        $entities = Entities::load($this->sport, $this->season, $map);
        // Now tie back together.
        $period_map = [];
        foreach ($periods as $period) {
            $raw = $period->{$period_key};
            $period_map[$period->period_id] = ($raw instanceof Carbon ? $raw->toDateString() : $raw);
        }
        foreach ($this->data as $i => $s) {
            foreach (['popular_pos', 'popular_neg'] as $key) {
                $period_ref = $period_map[$s['period_id']];
                $entity_ref = (isset($s["{$key}_link_id"]) ? "{$s["{$key}_link_id"]}:$period_ref" : 0);
                $this->data[$i][$key] = $entities->where('entity_ref', $entity_ref);
            }
        }
    }

    /**
     * Manipulate a summary for its API representation
     * @param array $row The row data (passed by reference) to be manipulated.
     * @return void
     */
    protected function postProcessAPIData(array &$row): void
    {
        // Convert our popular links.
        foreach (['popular_pos', 'popular_neg'] as $key) {
            $row[$key] = array_merge(
                ['link_id' => $row[$key . '_link_id']],
                (isset($row[$key . '_link_id'])) ? $row[$key]->rowToSummaryAPI() : [],
            );
            unset($row[$key . '_link_id']);
        }
    }
}
