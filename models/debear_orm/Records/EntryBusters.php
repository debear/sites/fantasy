<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Fantasy\Namespaces;
use DeBear\Helpers\Fantasy\Records\Buster;
use DeBear\Models\Skeleton\User;
use Illuminate\Support\Facades\DB;

class EntryBusters extends Instance
{
    /**
     * Load, if available, the "Streak Buster" selections by the user's team
     * @param string  $sport   The sport of the game being loaded.
     * @param integer $season  The season for the game being loaded.
     * @param integer $game_id The ID of the game being loaded.
     * @return array An array containing this entry's streak buster selections
     */
    public static function load(string $sport, int $season, int $game_id): array
    {
        $busters = static::query()
            ->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['game_id', '=', $game_id],
                ['user_id', '=', User::object()->id ?? 0], // Fall back to the global list.
            ])->get()->toArray();
        $ret = [];
        for ($i = 1; $i <= 3; $i++) {
            if (isset($busters["team{$i}"])) {
                $ret[$busters["team{$i}"]] = $busters["team{$i}_score"];
            }
        }
        return $ret;
    }

    /**
     * Load the entity info for the appropriate busters in this object
     * @return void
     */
    protected function loadSecondaryEntities(): void
    {
        // Get the team_id's to load.
        $team_ids = array_filter(array_unique(array_merge(
            $this->unique('team1'),
            $this->unique('team2'),
            $this->unique('team3')
        )));
        // Then the raw info for this team from the appropriate league object.
        list($sport) = $this->unique('sport');
        list($season) = $this->unique('season');
        $tbl_t = Namespaces::callStatic($sport, 'Team', 'getTable');
        $tbl_tn = Namespaces::callStatic($sport, 'TeamNaming', 'getTable');
        $info = Namespaces::callStatic($sport, 'Team', 'query')
            ->selectRaw("$tbl_t.team_id")
            ->selectRaw("CONCAT(IFNULL($tbl_tn.alt_city, $tbl_t.city), ' ',
                IFNULL($tbl_tn.alt_franchise, $tbl_t.franchise)) AS name")
            ->leftJoin($tbl_tn, function ($join) use ($tbl_tn, $tbl_t, $season) {
                $join->whereRaw("IFNULL($tbl_tn.alt_team_id, $tbl_tn.team_id) = $tbl_t.team_id")
                    ->where("$tbl_tn.season_from", '<=', $season)
                    ->where(DB::raw("IFNULL($tbl_tn.season_to, 2099)"), '>=', $season);
            })->whereIn("$tbl_t.team_id", $team_ids)
            ->get();
        // Now tie back together.
        foreach ($this->data as $i => $buster) {
            $by_entry = [];
            for ($n = 1; $n <= 3 && isset($buster["team{$n}"]); $n++) {
                $by_entry[$buster["team{$n}"]] = new Buster(array_merge(
                    ['sport' => $sport],
                    $info->where('team_id', $buster["team{$n}"])->toArray(),
                    ['score' => $buster["team{$n}_score"]],
                ));
            }
            $this->data[$i]['busters'] = $by_entry;
        }
    }
}
