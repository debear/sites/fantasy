<?php

namespace DeBear\ORM\Fantasy\Records;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Fantasy\Records\Traits\API\Scoring as ScoringTrait;

class EntryDropoff extends Instance
{
    use ScoringTrait;
}
