<?php

namespace DeBear\Http\Controllers\Fantasy;

use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Records\Traits\Controller as TraitController;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Fantasy\Common\Announcement;
use DeBear\ORM\Fantasy\Records\Game;

class Records extends Controller
{
    use TraitController;

    /**
     * Record Breakers homepage
     * @return Response
     */
    public function index(): Response
    {
        // Determine the games that are available.
        $games = Game::loadHighlighted();
        $news = Announcement::load('records', 0000);

        // Determine the appropriate user action.
        $user_action = $entry = null;
        if (!User::object()->isLoggedIn()) {
            $user_action = 'signin';
            Resources::addJS('_common/actions/signin.js');
        } elseif (!$this->getUserEntry()->isset()) {
            $user_action = 'register';
        } else {
            $entry = $this->getUserEntry();
            /* @phan-suppress-next-line PhanTypeMismatchArgumentSuperType from Iterator use */
            $entry->loadSelections($games->where('status', 'active'));
        }
        // Update the navigation with entries.
        (User::object()->isLoggedIn() ? $this->getUserEntry() : null)?->addActiveGamesWithinNavigation();

        // Custom page config.
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');
        Resources::addCSS('_common/box_colours.css');
        Resources::addCSS('records/pages/home.css');
        Resources::addCSS('records/widgets/progress_bars.css');
        foreach ($games as $game) {
            if ($game->isMajorLeague()) {
                Resources::addCSS("_common/{$game->sport}/teams.css");
            }
        }
        Resources::addJS('records/actions/manage-game.js');

        return response(view('fantasy.records.home', compact(['games', 'news', 'user_action', 'entry'])));
    }
}
