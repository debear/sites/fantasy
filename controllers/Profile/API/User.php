<?php

namespace DeBear\Http\Controllers\Fantasy\Profile\API;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use DeBear\Helpers\API;
use DeBear\ORM\Fantasy\Admin\User as UserModel;
use DeBear\ORM\Fantasy\Admin\AutomatedUser as AutomatedUserModel;

class User extends Controller
{
    /**
     * Performance and summary info of a particular user
     * @param string $user_id The (string) ID of the user to be viewed.
     * @return JsonResponse An appropriate JSON Response, either the user info or HTTP status
     */
    public function show(string $user_id): JsonResponse
    {
        $user = UserModel::loadProfile($user_id) ?? AutomatedUserModel::loadProfile($user_id);
        if (!isset($user)) {
            return API::sendNotFound();
        }
        return response()->json($user->rowToAPI());
    }
}
