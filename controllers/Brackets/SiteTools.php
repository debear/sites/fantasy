<?php

namespace DeBear\Http\Controllers\Fantasy\Brackets;

use Illuminate\Http\Response;
use DeBear\Http\Controllers\Skeleton\SiteTools as SkeletonSiteTools;
use DeBear\Helpers\Fantasy\Brackets\Traits\Controller as ControllerTrait;

class SiteTools extends SkeletonSiteTools
{
    use ControllerTrait;

    /**
     * Modify the base configuration before generating the manifest
     * @return Response A response object featuring our manifest
     */
    public function manifest(): Response
    {
        // Due to the limitation in not being passed in the season, get the season from the URL.
        $endpoint = explode('/', $_SERVER['REQUEST_ROUTE']);
        // Validate nuances of the request, error if invalid, or perform preparatory steps if okay.
        $validation_error = $this->validateAndPrepare(intval($endpoint[2]));
        if (isset($validation_error)) {
            return $validation_error;
        }
        // Call the parent version to geneate the sitemap.
        return parent::manifest();
    }
}
