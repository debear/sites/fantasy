<?php

namespace DeBear\Http\Controllers\Fantasy\Brackets;

use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Brackets\Traits\Controller as ControllerTrait;
use DeBear\Http\Modules\Forms;

class Entry extends Controller
{
    use ControllerTrait;

    /**
     * Render the Registration / Entry details page
     * @param integer $season Game season being loaded.
     * @return Response|RedirectResponse
     */
    public function index(int $season): Response|RedirectResponse
    {
        // Validate nuances of the request, error if invalid, or perform preparatory steps if okay.
        $validation_error = $this->validateAndPrepare($season);
        if (isset($validation_error)) {
            return $validation_error;
        }
        // Update the navigation with entries.
        $entry = $this->getUserEntry();
        $action = !($entry?->isset() ?? false) ? 'Create' : 'Update';
        // Set appropriate page info.
        HTML::setNavCurrent('/my-entry');
        HTML::setPageTitle("$action Entry");
        HTML::setMetaDescription("$action your entry to {config|debear.names.section}");
        Resources::addCSS('_common/forms/entry.css');
        Resources::addJS('skel/widgets/forms.js');
        Resources::addJS('_common/forms/entry.js');

        // Passed the validation, now render the form.
        $obj = new Forms('fantasy/_common/entry');
        return $obj->run();
    }
}
