<?php

namespace DeBear\Http\Controllers\Fantasy;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Brackets\Traits\Controller as ControllerTrait;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Fantasy\Common\Announcement;

class Brackets extends Controller
{
    use ControllerTrait;

    /**
     * Bracket Challenge homepage
     * @param integer $season Game season being loaded.
     * @return Response
     */
    public function index(int $season): Response
    {
        // Validate nuances of the request, error if invalid, or perform preparatory steps if okay.
        $validation_error = $this->validateAndPrepare($season);
        if (isset($validation_error)) {
            return $validation_error;
        }
        $sport_ref = FrameworkConfig::get('debear.setup.sport_ref');
        $news = Announcement::load('brackets', $season);
        // Determine the appropriate user action, if viewing a current / active game.
        if ($this->game->canRegister()) {
            if (!User::object()->isLoggedIn()) {
                $user_action = 'signin';
                Resources::addJS('_common/actions/signin.js');
            } elseif (!$this->getUserEntry()->isset() && true) {
                $user_action = 'register';
            }
        }

        // Custom page config.
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');
        Resources::addCSS("_common/$sport_ref/colours.css");
        Resources::addCSS("_common/$sport_ref/small.css");
        Resources::addCSS("_common/$sport_ref/misc/narrow_small.css");
        Resources::addCSS("_common/$sport_ref/playoffs.css");
        Resources::addCSS('_common/box_colours.css');
        Resources::addCSS('brackets/pages/home.css');
        $template_ref = FrameworkConfig::get('debear.setup.template.ref');
        Resources::addCSS("brackets/templates/$template_ref.css");

        return response(view('fantasy.brackets.home', [
            'sport' => $sport_ref,
            'season' => $season,
            'game' => $this->game,
            'entry' => $this->getUserEntry(),
            'user_action' => $user_action ?? null,
            'news' => $news,
            'template' => FrameworkConfig::get('debear.setup.template'),
            'groupings' => $this->getGroupings(),
            'series' => $this->getKnownSeries(),
        ]));
    }
}
