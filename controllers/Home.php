<?php

namespace DeBear\Http\Controllers\Fantasy;

use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Homepage as HomepageHelper;

class Home extends Controller
{
    /**
     * General, game-agnostic, summary
     * @return Response
     */
    public function index(): Response
    {
        // Process the sports into appropriate groupings.
        $sports = HomepageHelper::getSportGroups();

        // Custom page config.
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');
        Resources::addCSS('_common/box_colours.css');
        Resources::addCSS('sports_icons.css');
        Resources::addCSS('_global/pages/home.css');
        Resources::addCSS('records/widgets/progress_bars.css');

        return response(view('fantasy._global.home', compact(['sports'])));
    }
}
