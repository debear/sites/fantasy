<?php

namespace DeBear\Http\Controllers\Fantasy;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;

class SiteTools extends Controller
{
    /**
     * Load a sitemap index of the games we have
     * @return Response The relevant sitemap index containing all the available games
     */
    public function sitemapXmlGames(): Response
    {
        $games = array_map(function ($ele) {
            return $ele['url'];
        }, array_filter(FrameworkConfig::get('debear.subsites'), function ($ele) {
            return $ele['sitemap_xml'] ?? $ele['enabled'] ?? false;
        }));

        return response(view('fantasy._global.sitemap_xml', compact('games')))
            ->header('Content-Type', 'text/xml');
    }
}
