<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Records\Traits\Controller as TraitController;
use DeBear\Http\Modules\Forms;

class Entry extends Controller
{
    use TraitController;

    /**
     * Render the Registration / Entry details page
     * @return Response|RedirectResponse
     */
    public function index(): Response|RedirectResponse
    {
        // Update the navigation with entries.
        $entry = $this->getUserEntry();
        $entry?->addActiveGamesWithinNavigation();
        $action = !($entry?->isset() ?? false) ? 'Create' : 'Update';
        // Set appropriate page info.
        HTML::setNavCurrent('/my-entry');
        HTML::setPageTitle("$action Entry");
        HTML::setMetaDescription("$action your entry to {config|debear.names.section}");
        Resources::addCSS('_common/forms/entry.css');
        Resources::addJS('skel/widgets/forms.js');
        Resources::addJS('_common/forms/entry.js');

        // Passed the validation, now render the form.
        $obj = new Forms('fantasy/_common/entry');
        return $obj->run();
    }
}
