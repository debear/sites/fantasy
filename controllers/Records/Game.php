<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\API;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Records\Traits\Controller as TraitController;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use DeBear\Repositories\Time;

class Game extends Controller
{
    use TraitController;

    /**
     * View / Dashboard for a single game
     * @param string  $sport  The sport of the game being loaded.
     * @param string  $slug   The name of the game being loaded.
     * @param integer $season The season for the game being loaded.
     * @return Response
     */
    public function index(string $sport, string $slug, int $season): Response
    {
        // Validate and load the game info.
        $game = GameModel::loadBySlug($sport, $season, $slug);
        if (!$game->isset()) {
            return HTTP::sendNotFound();
        }
        // Load the additional info.
        $game->loadSecondary(['meta', 'rules', 'leaderboard', 'integrationStatus']);
        // Load the user's entry (if there is one).
        $entry = $this->getUserEntry();
        $has_joined = isset($entry) && $entry->getGameScore($game)->isset();
        if ($has_joined) {
            $entry->loadSingleGame($game);
            $game->loadSecondary(['achievements']);
        }
        $entry?->addActiveGamesWithinNavigation();
        if ($game->isComplete() && $has_joined) {
            $entry?->addOtherGamesWithinNavigation($game);
        }
        // Determine the relevant dates.
        $periods = $game->getPeriods($entry);
        // Load the entry's selections for these periods.
        if ($has_joined) {
            $entry->loadSelections($game);
        }
        // Any period summary details available?
        $summaries = $game->loadPeriodSummary([$periods['last']->period_id, $periods['curr']->period_id]);
        $summary_key = in_array($periods['curr']->period_id, $summaries->unique('period_id')) ? 'curr' : 'last';
        $summary = $summaries->where('period_id', $periods[$summary_key]->period_id ?? 'no-match');
        // If the period results are published before it officially ends, we should still display them.
        $curr_editable = $periods['curr']->isset() &&
            (!$periods['curr']->is_locked || !$summary->isset() || $summary->period_id != $periods['curr']->period_id);
        // Determine an appropriate user action.
        $user_action = null;
        if ($game->date_close->toDateTimeString() >= Time::object()->getNowFmt()) {
            if (!User::object()->isLoggedIn()) {
                $user_action = 'signin';
                Resources::addJS('records/actions/signin.js');
            } elseif (!$has_joined || !$entry->score->processed) {
                $user_action = (!$has_joined ? 'join' : 'leave');
                Resources::addJS('records/actions/manage-game.js');
            }
        }

        // Custom page config.
        HTML::setNavCurrent($game->section_url);
        HTML::setPageTitle([FrameworkConfig::get("debear.sports.$sport.name_short"), $game->name]);
        HTML::setMetaDescription($game->description);
        HTML::setMetaThemeColour(FrameworkConfig::get("debear.sports.subsites.$sport.meta.theme_colour"));
        if ($game->meta->isset() || $game->isComplete()) {
            // Over-ride with the custom meta details for this game.
            $cdn_domain = HTTP::buildDomain('cdn');
            $cfg = FrameworkConfig::get('debear.meta.og.create');
            $path_fallback = HTTP::buildCDNURLs("{$cfg['img_path']}/{$game->game_ref}.{$cfg['format']}");
            HTML::setMetaImage($game->meta->isset() ? "$cdn_domain{$game->meta->path_og}" : $path_fallback);
            HTML::setMetaImageOpenGraphSize($cfg['width'], $cfg['height']);
            HTML::setMetaTwitterCard('summary_large_image');
        }
        Resources::addCSS('_common/box_colours.css');
        if ($game->isMajorLeague()) {
            Resources::addCSS("_common/$sport/teams.css");
        }
        Resources::addCSS('sports_icons.css');
        Resources::addCSS('records/pages/game.css');
        Resources::addCSS('records/widgets/progress_bars.css');
        if (!$game->isComplete()) {
            Resources::addCSS('records/pages/game/modal/team.css');
            Resources::addCSS('skel/widgets/modals.css');
            Resources::addCSS('records/pages/game/modal/view.css');
            if ($has_joined) {
                Resources::addCSS('records/pages/game/modal/edit.css');
            }
        }
        Resources::addJS('records/pages/game.js');
        if ($game->leaderboards['overall']->isset()) {
            Resources::addJS('records/pages/game/leaderboard.js');
        }
        if (!$game->isComplete()) {
            Resources::addJS('records/pages/game/selection.js');
            if ($game->leaderboards['overall']->isset()) {
                Resources::addJS('records/pages/game/modal/team.js');
            }
            Resources::addJS('records/pages/game/modal/view.js');
            if ($has_joined && $curr_editable) {
                Resources::addJS('records/pages/game/modal/edit.js');
            }
        }

        return response(view('fantasy.records.' . (!$game->isComplete() ? 'game' : 'game_archived'), compact([
            'game',
            'entry',
            'user_action',
            'periods',
            'summary',
            'curr_editable',
        ])));
    }

    /**
     * Load the leaderboard for a specific Record Breakers gamae
     * @param string  $sport  The sport of the game being loaded.
     * @param string  $slug   The name of the game being loaded.
     * @param integer $season The season for the game being loaded.
     * @return JsonResponse A JSON-based response in all instances
     */
    public function leaderboard(string $sport, string $slug, int $season): JsonResponse
    {
        // Validate and load the game info.
        $game = GameModel::loadBySlug($sport, $season, $slug);
        if (!$game->isset()) {
            return API::sendNotFound();
        }
        // Load the additional info.
        $periods = $game->getPeriods();
        $game->loadSecondary(['leaderboardWithUser']);
        $game->loadLeaderboardSelections($periods['last']->period_id);
        // Format the results and output.
        return response()->json(array_filter([
            'overall' => $game->leaderboardsToWeb('overall'),
            'current' => $game->hasCurrentLeaderboard() ? $game->leaderboardsToWeb('current') : false,
            'hitrate' => $game->hasHitRateLeaderboard() ? $game->leaderboardsToWeb('hitrate') : false,
        ]));
    }
}
