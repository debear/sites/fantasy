<?php

namespace DeBear\Http\Controllers\Fantasy\Records\API;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use DeBear\ORM\Fantasy\Common\HelpSection as HelpSectionModel;

class Help extends Controller
{
    /**
     * Show the help articles
     * @return JsonResponse An appropriate JSON Response of help sections and articles
     */
    public function index(): JsonResponse
    {
        $articles = HelpSectionModel::loadArticles('records');
        return response()->json($articles->resultsToAPI());
    }
}
