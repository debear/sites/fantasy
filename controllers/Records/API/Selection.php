<?php

namespace DeBear\Http\Controllers\Fantasy\Records\API;

use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\Fantasy\Records\Traits\API\ControllerValidation as TraitControllerValidation;
use DeBear\Helpers\Fantasy\Records\Traits\Controller\EntrySel as TraitControllerEntrySel;
use DeBear\Helpers\API;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use DeBear\ORM\Fantasy\Records\Selection as SelectionModel;

class Selection extends Controller
{
    use TraitControllerValidation;
    use TraitControllerEntrySel;

    /**
     * Info about the game being processed
     * @var GameModel
     */
    protected $game;
    /**
     * Info about the period being processed
     * @var GamePeriod
     */
    protected $period;

    /**
     * View information about a single link within a game
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $link_id   The ID of the link being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse
     */
    public function show(string $sport, int $season, int $game_id, int $link_id, ?int $period_id = null): JsonResponse
    {
        // Validate the game reference - including whether the game is open for entries.
        $this->game = $this->validateGameAndLoad($sport, $season, $game_id);
        if ($this->game instanceof JsonResponse) {
            // A validation error.
            return $this->game;
        }
        // If supplied, validate the period, defaulting to the current period.
        $curr_period = $this->period = GamePeriod::getCurrentPeriod($sport, $season);
        if (isset($period_id)) {
            $this->period = $this->validatePeriodAndLoad($sport, $season, $period_id);
            if ($this->period instanceof JsonResponse) {
                // A validation error.
                return $this->period;
            } elseif ($this->period->period_order > $curr_period->period_order) {
                return API::sendNotFound('Game period not found');
            }
        }
        // Validate (by loading) the link info.
        $link = SelectionModel::get($sport, $season, $this->game->game_id, $link_id, $this->period->period_id);
        if (!$link->isset()) {
            return API::sendNotFound('Selection not found or not available');
        }
        // Set any flags for the selections in question.
        $this->setSelectionFlags($link);
        // Get the additional info we need.
        $link->loadSecondary(['stats', 'recentResults'])
            ->hideFieldsFromAPI(['period_id', 'score', 'status', 'stress', 'summary', 'sel_pct', 'note']);
        return response()->json($link->rowToAPI());
    }
}
