<?php

namespace DeBear\Http\Controllers\Fantasy\Records\API;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use DeBear\Helpers\API;
use DeBear\Helpers\Fantasy\Records\Traits\API\ControllerValidation;
use DeBear\Http\Modules\Forms;
use DeBear\ORM\Fantasy\Admin\User as UserModel;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;
use DeBear\Repositories\InternalCache;

class Entry extends Controller
{
    use ControllerValidation;

    /**
     * Get the details of the entry of a given user.
     * @param string $user_id The user for whom we are searching for an entry.
     * @return JsonResponse An appropriate JSON Response, either the entry info or HTTP status
     */
    public function show(string $user_id): JsonResponse
    {
        $numeric_id = UserModel::loadByStringID($user_id)->id;
        if (!isset($numeric_id)) {
            // User does not exist, so no further checks required.
            return API::sendNotFound('User could not be found');
        }
        // Get the entry for the requested user.
        $entry = EntryModel::load($numeric_id);
        // If no entry found, return a 404.
        if (!$entry->isset()) {
            return API::sendNotFound('Entry could not be found');
        }
        // Otherwise, render the data for the API.
        return response()->json($entry->rowToAPI());
    }

    /**
     * Create a new entry for the given user
     * @param string $user_id The user for whom we are creating a new entry.
     * @return JsonResponse An appropriate JSON Response for the creation process
     */
    public function store(string $user_id): JsonResponse
    {
        $ret = $this->processEntry($user_id, true);
        if (!is_bool($ret)) {
            // There was an error in the processing we should pass on.
            return $ret;
        }
        // Return a successful response.
        return API::sendCreated('Entry created successfully');
    }

    /**
     * Update an existing entry for the given user
     * @param string $user_id The user for whom we are updating their entry.
     * @return JsonResponse An appropriate JSON Response for the update process
     */
    public function update(string $user_id): JsonResponse
    {
        $ret = $this->processEntry($user_id, false);
        if (!is_bool($ret)) {
            // There was an error in the processing we should pass on.
            return $ret;
        }
        // Return a successful response.
        return API::sendResponse('Entry updated successfully');
    }

    /**
     * Process the entry for the given user
     * @param string  $user_id     The user for whom we are processing their entry.
     * @param boolean $create_mode The flag to say we are creating, rather than updating, the entry.
     * @return JsonResponse|boolean An appropriate JSON Response for an error in the processing, or true on success
     */
    protected function processEntry(string $user_id, bool $create_mode): JsonResponse|bool
    {
        // Validate whether the user has an entry or not.
        $entry = $this->validateUserAndLoadEntry($user_id, false);
        if ($entry instanceof JsonResponse) {
            // A validation error.
            return $entry;
        } elseif ($create_mode && $entry->isset()) {
            return API::sendConflict('An entry already exists for this user');
        } elseif (!$create_mode) {
            if (!$entry->isset()) {
                // Updating a non-existant entry.
                return API::sendNotFound('Entry could not be found');
            }
            // Store in a way we can use as default data within the form.
            InternalCache::object()->set('fantasy.records.entry', $entry);
        }
        // Then get the JSON payload and ensure we have a non-empty array.
        $json = InternalCache::object()->get('php://input');
        if (!is_array($json) || !$json) {
            return API::sendBadRequest();
        }
        // Process the data.
        $obj = new Forms('fantasy/_common/entry');
        $ret = $obj->processFromAPI($json);
        if (is_array($ret)) {
            return API::sendValidationFailure($ret);
        }
        // All okay, so return an appropriate status.
        return true;
    }
}
