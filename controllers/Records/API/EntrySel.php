<?php

namespace DeBear\Http\Controllers\Fantasy\Records\API;

use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\API;
use DeBear\Helpers\Fantasy\Records\Traits\API\ControllerValidation as TraitControllerValidation;
use DeBear\Helpers\Fantasy\Records\Traits\Controller\EntrySel as TraitControllerEntrySel;
use DeBear\ORM\Fantasy\Common\GamePeriod as GamePeriodModel;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;
use DeBear\ORM\Fantasy\Records\EntryByPeriod as EntryByPeriodModel;
use DeBear\ORM\Fantasy\Records\EntryScore as EntryScoreModel;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use DeBear\ORM\Fantasy\Records\Selection as SelectionModel;
use DeBear\Repositories\InternalCache;

class EntrySel extends Controller
{
    use TraitControllerValidation;
    use TraitControllerEntrySel;

    /**
     * The entry being processed
     * @var EntryModel
     */
    protected $entry;
    /**
     * Info about the game being processed
     * @var GameModel
     */
    protected $game;
    /**
     * Info about the period being processed
     * @var GamePeriodModel
     */
    protected $period;
    /**
     * The entry's score in the appropriate game
     * @var EntryScoreModel
     */
    protected $score;
    /**
     * Info about the selection being processed
     * @var SelectionModel
     */
    protected $link;

    /**
     * View the list of selections available for a specific game in a given period
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse An appropriate JSON Response for the selection list
     */
    public function index(string $sport, int $season, int $game_id, int $period_id): JsonResponse
    {
        // Validate the request.
        $validate = $this->validateGameLookup($sport, $season, $game_id, $period_id);
        if (isset($validate)) {
            return $validate;
        }

        // Now load the selections for this period.
        $args = $this->validateIndexQueryArguments();
        if (!isset($args)) {
            return API::sendBadRequest();
        } elseif (!SelectionModel::validatePeriod($sport, $season, $this->game->game_id, $period_id)) {
            return API::setStatus(422, 'Requested period not yet available');
        }
        $sel = SelectionModel::loadByPeriod($sport, $season, $this->game->game_id, $period_id, $args);
        if ($sel->totalRows() && !$sel->count()) {
            // No rows - when some are available - indicate bad pagination.
            return API::sendBadRequest();
        }

        // Set any flags for the selections in question.
        $this->setSelectionFlags($sel);
        // Return our data response.
        $sel->hideFieldsFromAPI(['period_id', 'sel_pct']);
        return response()->json($sel->resultsToPaginatedAPI('sel'));
    }

    /**
     * View the entry info to a specific game in a given period
     * @param string  $user_id   The user for whom we are viewing.
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse An appropriate JSON Response for the request
     */
    public function show(string $user_id, string $sport, int $season, int $game_id, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateEntry($user_id);
        if (isset($validate)) {
            return $validate;
        }
        $validate = $this->validateGameLookup($sport, $season, $game_id, $period_id);
        if (isset($validate)) {
            return $validate;
        }
        // Load and process this entry's selection.
        $this->entry->loadSelections($this->game, [$this->period->period_id]);
        $sel_obj = $this->entry->getSelection($this->game->game_ref, $this->period->period_id);
        $sel = $sel_obj?->rowToAPI([
            'game' => $this->game,
        ]);
        if (isset($sel) && array_key_exists('lock_time', $sel) && !isset($sel['lock_time'])) {
            // Fall back to the period's deadline.
            $sel['lock_time'] = [
                'status' => null,
                'raw' => $this->period->lock_time->system->format('c'),
            ];
        }
        // Finally, return.
        return response()->json($sel);
    }

    /**
     * Store a new the selection to a specific game in a given period
     * @param string  $user_id   The user for whom we are creating a new selection.
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse An appropriate JSON Response for the selection creation
     */
    public function store(string $user_id, string $sport, int $season, int $game_id, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateEntry($user_id);
        if (isset($validate)) {
            return $validate;
        }
        $validate = $this->validateGameLookup($sport, $season, $game_id, $period_id);
        if (isset($validate)) {
            return $validate;
        }
        // Validate the request payload.
        $validate = $this->validatePayload();
        if (isset($validate)) {
            return $validate;
        }
        // Validate the user does not have an existing selection.
        $curr_sel = $this->validateCurrentSelection(false);
        if ($curr_sel instanceof JsonResponse) {
            return $curr_sel;
        }

        // Perform the requested action.
        EntryByPeriodModel::store($sport, $season, $this->game->game_id, $period_id, $this->link->link_id);
        return API::sendCreated('Selection saved');
    }

    /**
     * Update a selection to a specific game in a given period
     * @param string  $user_id   The user for whom we are updating a selection.
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse An appropriate JSON Response for the selection update
     */
    public function update(string $user_id, string $sport, int $season, int $game_id, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateEntry($user_id);
        if (isset($validate)) {
            return $validate;
        }
        $validate = $this->validateGameLookup($sport, $season, $game_id, $period_id);
        if (isset($validate)) {
            return $validate;
        }
        // Validate the request payload.
        $validate = $this->validatePayload();
        if (isset($validate)) {
            return $validate;
        }
        // Validate the user has an existing selection.
        $curr_sel = $this->validateCurrentSelection(true);
        if ($curr_sel instanceof JsonResponse) {
            return $curr_sel;
        }

        // Perform the requested action.
        $curr_sel->update(['link_id' => $this->link->link_id])
            ->audit('records_sel_update', 'Selection updated', ['period_id', 'link_id'])
            ->save();
        return API::sendResponse('Selection updated');
    }

    /**
     * Remove the selection to a specific game in a given period
     * @param string  $user_id   The user for whom we are removing a selection.
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse An appropriate JSON Response for the selection removal
     */
    public function destroy(string $user_id, string $sport, int $season, int $game_id, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateEntry($user_id);
        if (isset($validate)) {
            return $validate;
        }
        $validate = $this->validateGameLookup($sport, $season, $game_id, $period_id);
        if (isset($validate)) {
            return $validate;
        }
        // Validate the user has an existing selection.
        $curr_sel = $this->validateCurrentSelection(true);
        if ($curr_sel instanceof JsonResponse) {
            return $curr_sel;
        }

        // Perform the requested action.
        $curr_sel->audit('records_sel_remove', 'Selection removed', ['period_id', 'link_id'])->deleteRow();
        return API::sendResponse('Selection removed');
    }

    /**
     * Validate the user entry arguments are appropriate
     * @param string $user_id The user for whom we are removing a selection.
     * @return JsonResponse|null A JSON-based response in case of a validation error, null otherwise
     */
    protected function validateEntry(string $user_id): ?JsonResponse
    {
        // Validate whether the user has an entry or not.
        $this->entry = $this->validateUserAndLoadEntry($user_id);
        if ($this->entry instanceof JsonResponse) {
            // A validation error.
            return $this->entry;
        }
        // If we're here, this part of the validation is okay.
        return null;
    }

    /**
     * Validate the game reference arguments are appropriate
     * @param string  $sport     The sport of the game being referenced.
     * @param integer $season    The season in which the game is taking place.
     * @param integer $game_id   The ID of the game being referenced.
     * @param integer $period_id The ID of the period being referenced.
     * @return JsonResponse|null A JSON-based response in case of a validation error, null otherwise
     */
    protected function validateGameLookup(string $sport, int $season, int $game_id, int $period_id): ?JsonResponse
    {
        // Validate the game reference - including whether the game is open for entries.
        $this->game = $this->validateGameAndLoad($sport, $season, $game_id);
        if ($this->game instanceof JsonResponse) {
            // A validation error.
            return $this->game;
        }
        // Validate the game period.
        $this->period = $this->validatePeriodAndLoad($sport, $season, $period_id);
        if ($this->period instanceof JsonResponse) {
            // A validation error.
            return $this->period;
        }
        // Ensure the entry (if loaded) has joined the game and can continue playing.
        if (isset($this->entry)) {
            $this->score = $this->validateEntryGamePeriod($this->entry, $this->game, $this->period);
            if ($this->score instanceof JsonResponse) {
                // A validation error.
                return $this->score;
            }
        }
        // If we're here, this part of the validation is okay.
        return null;
    }

    /**
     * Validate the request payload is appropriate
     * @return JsonResponse|null A JSON-based response in case of a validation error, null otherwise
     */
    protected function validatePayload(): ?JsonResponse
    {
        // Validate the request payload.
        $input_arr = InternalCache::object()->get('php://input');
        $link_id = ($input_arr['link_id'] ?? null);
        if (!is_integer($link_id)) {
            return API::sendBadRequest();
        }
        $game = $this->game;
        $period = $this->period;
        $this->link = SelectionModel::get($game->sport, $game->season, $game->game_id, $link_id, $period->period_id);
        if (!$this->link->isset()) {
            return API::sendBadRequest();
        } elseif (!$this->link->entity->eventActive()) {
            return API::setStatus(410, 'Selection no longer available');
        } elseif ($this->link->is_locked) {
            return API::setStatus(423, 'Requested selection locked');
        } elseif (!$game->canReuseSelection()) {
            // Has the selection already been used when we are limiting their re-use?
            $period_order = $period->period_order;
            $historical = EntryByPeriodModel::getHistorical($game->sport, $game->season, $game->game_id, $period_order);
            if ($historical->where('link_id', strval($link_id))->count()) {
                return API::sendConflict('Selection cannot be re-used');
            }
        }
        // If we're here, this part of the validation is okay.
        return null;
    }

    /**
     * Validate the status of the user's current selection
     * @param boolean $has_existing Whether the user has an existing selection.
     * @return JsonResponse|EntryByPeriodModel A JSON-based response in case of a validation error, ORM object otherwise
     */
    protected function validateCurrentSelection(bool $has_existing): JsonResponse|EntryByPeriodModel
    {
        // Does the curent status match expectation?
        $game = $this->game;
        $curr_sel = EntryByPeriodModel::get($game->sport, $game->season, $game->game_id, $this->period->period_id);
        if ($has_existing != $curr_sel->isset()) {
            $msg = ($curr_sel->isset() ? 'already saved' : 'not found');
            return API::sendConflict("Existing selection $msg");
        }
        // If the user does have a selection, is it locked?
        if ($curr_sel->isset() && $curr_sel->is_locked) {
            return API::setStatus(423, 'Current selection locked');
        }
        // If we're here, the request is valid.
        return $curr_sel;
    }
}
