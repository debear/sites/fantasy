<?php

namespace DeBear\Http\Controllers\Fantasy\Records\API;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use DeBear\Helpers\API;
use DeBear\ORM\Fantasy\Records\Game as GameModel;

class Game extends Controller
{
    /**
     * Record Breakers game lists
     * @param string $status An optional status for the game list being requested.
     * @return JsonResponse An appropriate JSON Response, either the game list or HTTP status
     */
    public function index(?string $status = null): JsonResponse
    {
        $games = GameModel::loadCurrent();
        if (isset($status)) {
            // If we are after a status subset, filter.
            $games = $games->where('status', $status);
        }
        return response()->json($games->resultsToAPI());
    }

    /**
     * Load information about a specific Record Breakers gamae
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON response, either the game info or HTTP status
     */
    public function show(string $sport, int $season, int $game_id): JsonResponse
    {
        $game = GameModel::loadCurrent()
            ->where('game_ref', '=', "$sport-$season-$game_id")
            ->where('status', '!=', 'upcoming'); // These aren't for viewing yet.
        if (!$game->isset()) {
            return API::sendNotFound('Game could not be found');
        }
        return response()->json($game->rowToAPI());
    }

    /**
     * Load the rules for a specific Record Breakers gamae
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON response, either the game rules or HTTP status
     */
    public function rules(string $sport, int $season, int $game_id): JsonResponse
    {
        $game = GameModel::loadCurrent()->loadSecondary(['rules'])->where('game_ref', "$sport-$season-$game_id");
        if (!$game->isset()) {
            return API::sendNotFound('Game could not be found');
        }
        return response()->json($game->rules->as_list);
    }

    /**
     * Load the list of achievements for a specific Record Breakers gamae
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON response, either the game's achievements or HTTP status
     */
    public function achievements(string $sport, int $season, int $game_id): JsonResponse
    {
        $game = GameModel::loadCurrent()->loadSecondary(['achievements'])
            ->where('game_ref', "$sport-$season-$game_id");
        if (!$game->isset()) {
            return API::sendNotFound('Game could not be found');
        }
        return response()->json($game->achievements->resultsToAPI());
    }

    /**
     * Load the periods for a specific Record Breakers gamae
     * @param string      $sport   The sport of the game being referenced.
     * @param integer     $season  The season in which the game is taking place.
     * @param integer     $game_id The ID of the game being referenced.
     * @param string|null $type    An alias for a point-in-time period.
     * @return JsonResponse An appropriate JSON response, either the game periods or HTTP status
     */
    public function periods(string $sport, int $season, int $game_id, ?string $type = null): JsonResponse
    {
        $game = GameModel::loadCurrent()->loadSecondary(['periods'])->where('game_ref', "$sport-$season-$game_id");
        if (!$game->isset()) {
            return API::sendNotFound('Game could not be found');
        }
        // Get the period data out.
        $is_single = (isset($type) && $type == 'current');
        if (!$is_single) {
            $periods = $game->periods->resultsToAPI();
        } else {
            $periods = $game->periods->where('is_current', 1)->rowToAPI();
            if (!count($periods)) {
                // Empty array means there is no 'current' period (either before the start or after the finish).
                return API::sendNotFound('This game is not currently taking place');
            }
            // To simplify future processing we'll convert this to a longer list.
            $periods = [$periods];
        }
        // Then our game-specific summary info.
        $summaries = $game->loadPeriodSummary(array_column($periods, 'period_id'));
        foreach ($periods as $i => $period) {
            $summary = $summaries->where('period_id', '=', $period['period_id']);
            if ($summary->isset()) {
                $periods[$i]['summary'] = $summary->rowToAPI();
            }
        }
        // Return as a response.
        return response()->json($is_single ? $periods[0] : $periods);
    }

    /**
     * Load the leaderboard for a specific Record Breakers gamae
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @param string  $type    A specific leaderboard type to render, or all if not supplied.
     * @return JsonResponse An appropriate JSON response, either the game leaders or HTTP status
     */
    public function leaderboard(string $sport, int $season, int $game_id, ?string $type = null): JsonResponse
    {
        $game_ref = "$sport-$season-$game_id";
        $game_validation = GameModel::loadCurrent()->where('game_ref', $game_ref);
        if (!$game_validation->isset()) {
            return API::sendNotFound('Game could not be found');
        } elseif (isset($type) && !$game_validation->hasCurrentLeaderboard() && $type == 'current') {
            return API::sendBadRequest('Current leaderboard not available for this game');
        } elseif (isset($type) && !$game_validation->hasHitRateLeaderboard() && $type == 'hitrate') {
            return API::sendBadRequest('Success Rate leaderboard not available for this game');
        }
        $game = GameModel::loadByGameRef($sport, $season, $game_id)->loadSecondary(['leaderboard']);
        $has_current = $game->hasCurrentLeaderboard();
        $has_hitrate = $game->hasHitRateLeaderboard();
        $ret = array_filter([
            'overall' => $game->leaderboards['overall']->leadersToAPI('overall'),
            'current' => $has_current ? $game->leaderboards['current']->leadersToAPI('current') : false,
            'hitrate' => $has_hitrate ? $game->leaderboards['hitrate']->leadersToAPI('hitrate') : false,
        ]);
        return response()->json(!isset($type) ? $ret : $ret[$type]);
    }
}
