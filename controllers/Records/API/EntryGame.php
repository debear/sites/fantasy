<?php

namespace DeBear\Http\Controllers\Fantasy\Records\API;

use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\API;
use DeBear\Helpers\Fantasy\Records\Traits\API\ControllerValidation;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use DeBear\ORM\Fantasy\Admin\User as UserModel;
use DeBear\Repositories\Time;

class EntryGame extends Controller
{
    use ControllerValidation;

    /**
     * The entry joining / leaving the game
     * @var EntryModel
     */
    protected $entry;
    /**
     * Info about the game being processed
     * @var GameModel
     */
    protected $game;

    /**
     * View the entry info to a specific game
     * @param string  $user_id The user for whom we are viewing.
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON Response for the request
     */
    public function show(string $user_id, string $sport, int $season, int $game_id): JsonResponse
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($user_id, $sport, $season, $game_id, false);
        if (isset($validation)) {
            return $validation;
        }
        // Build up our base entry object, before selections are loaded.
        $ret = $this->entry->getGameScore($this->game)->rowToAPI([
            'game' => $this->game,
            'entry_scores' => $this->entry->getGameScore($this->game),
        ]);
        // Load the appropriate selections for this view then add to our object.
        $p = $this->game->getPeriods($this->entry);
        $this->entry->loadSelections($this->game);
        $ret['sel'] = [];
        foreach (['last' => $p['last'], 'curr' => $p['curr']] as $key => $period) {
            $sel_obj = $this->entry->getSelection($this->game->game_ref, $period?->period_id ?? 0);
            $sel = isset($sel_obj) ? $sel_obj->rowToAPI() : null;
            if (isset($sel) && array_key_exists('lock_time', $sel) && !isset($sel['lock_time'])) {
                // Fall back to the period's deadline.
                $sel['lock_time'] = [
                    'status' => null,
                    'raw' => $period->lock_time->system->format('c'),
                ];
            }
            $ret['sel'][$key] = $sel;
        }
        // Finally, return.
        return response()->json($ret);
    }

    /**
     * View the achievements made by an entry in a specific game
     * @param string  $user_id The user for whom we are viewing.
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON Response for the request
     */
    public function achievements(string $user_id, string $sport, int $season, int $game_id): JsonResponse
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($user_id, $sport, $season, $game_id);
        if (isset($validation)) {
            return $validation;
        }
        $entry_score = $this->entry->getGameScore($this->game);
        if (!$entry_score->isset()) {
            return API::sendNotFound('The entry has not joined this game');
        }
        return response()->json($entry_score->achievementsToAPI($this->game));
    }

    /**
     * View the achievements made by an entry in a specific game
     * @param string  $user_id The user for whom we are viewing.
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON Response for the request
     */
    public function opponents(string $user_id, string $sport, int $season, int $game_id): JsonResponse
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($user_id, $sport, $season, $game_id);
        if (isset($validation)) {
            return $validation;
        }
        // This game needs to have an opponents-based achievements to proceed.
        $this->game->loadSecondary(['achievements']);
        if (!$this->game->hasAchievement('opponent')) {
            return API::sendBadRequest('Request is not available for this game');
        }
        // .
        $entry_score = $this->entry->getGameScore($this->game);
        if (!$entry_score->isset()) {
            return API::sendNotFound('The entry has not joined this game');
        }
        return response()->json($entry_score->opponentsToAPI($this->game));
    }

    /**
     * View the selections made by an entry in a specific game
     * @param string  $user_id The user for whom we are viewing.
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON Response for the request
     */
    public function selections(string $user_id, string $sport, int $season, int $game_id): JsonResponse
    {
        // Validate whether the user has an entry or not.
        $numeric_id = UserModel::loadByStringID($user_id)->id;
        $entry = EntryModel::load($numeric_id ?? -1);
        if (!$entry->isset()) {
            return API::sendNotFound('User could not be found');
        }
        // Validate the game reference.
        $game = $this->validateGameAndLoad($sport, $season, $game_id);
        if ($game instanceof JsonResponse) {
            // A validation error.
            return $game;
        } elseif (!$entry->getGameScore($game)->isset()) {
            // Entry has not joined.
            return API::sendNotFound('The entry has not joined this game');
        }
        // Load and process this entry's entire selection history.
        $entry_game = $entry->loadExpandedSelections($game->game_ref, false);
        return response()->json($entry_game->selectionsToAPI([
            'game' => $game,
        ]));
    }

    /**
     * Join an entry to a specific game
     * @param string  $user_id The user for whom we are joining to a game.
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON Response for the joining process
     */
    public function store(string $user_id, string $sport, int $season, int $game_id): JsonResponse
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($user_id, $sport, $season, $game_id);
        if (isset($validation)) {
            return $validation;
        }
        // Validate the user hasn't already taken part in this game.
        if ($this->entry->getGameScore($this->game)->isset()) {
            return API::sendConflict('The entry has already joined this game');
        }
        // Create the link.
        $this->entry->gameJoin($sport, $season, $game_id);
        return API::sendCreated('Game joined successfully');
    }

    /**
     * Join an entry to a specific game
     * @param string  $user_id The user for whom we are leaving a game.
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return JsonResponse An appropriate JSON Response for the leaving process
     */
    public function destroy(string $user_id, string $sport, int $season, int $game_id): JsonResponse
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($user_id, $sport, $season, $game_id);
        if (isset($validation)) {
            return $validation;
        }
        // Validate the user is taking part in this game and has not processed their first selection.
        $score = $this->entry->getGameScore($this->game);
        if (!$score->isset()) {
            return API::sendNotFound('The entry has not already joined this game');
        } elseif ($score->processed) {
            // HTTP Status 423 == Locked.
            return API::setStatus(423, 'The entry has started playing this game');
        }
        // Remove the link.
        $this->entry->gameLeave($sport, $season, $game_id);
        return API::sendResponse('Game left successfully');
    }

    /**
     * Validate the incoming request for the user and game combination requested
     * @param string  $user_id       The user for whom we are acting for.
     * @param string  $sport         The sport of the game being referenced.
     * @param integer $season        The season in which the game is taking place.
     * @param integer $game_id       The ID of the game being referenced.
     * @param boolean $action_window Whether we should validate the current date against the game's open window.
     * @return JsonResponse|null A JsonResponse object in the case of a validation failure, null otherwise
     */
    protected function validateAction(
        string $user_id,
        string $sport,
        int $season,
        int $game_id,
        bool $action_window = true
    ): ?JsonResponse {
        // Validate whether the user has an entry or not.
        $this->entry = $this->validateUserAndLoadEntry($user_id);
        if ($this->entry instanceof JsonResponse) {
            // A validation error.
            return $this->entry;
        }
        // Validate the game reference - including whether the game is open for entries.
        $this->game = $this->validateGameAndLoad($sport, $season, $game_id);
        if ($this->game instanceof JsonResponse) {
            // A validation error.
            return $this->game;
        } elseif ($action_window && Time::object()->getNowFmt() >= $this->game->date_close->toDateTimeString()) {
            return API::sendBadRequest('The action is not available for this game');
        }
        // If we're here, the request is valid.
        return null;
    }
}
