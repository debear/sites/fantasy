<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\API;
use DeBear\ORM\Fantasy\Common\GamePeriod;
use DeBear\ORM\Fantasy\Records\Game;
use DeBear\ORM\Fantasy\Records\Selection as SelectionModel;

class Selection extends Controller
{
    /**
     * View information about a single link within a game
     * @param string  $sport     The sport of the game being loaded.
     * @param string  $slug      The name of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param string  $link_slug The name of the link being loaded.
     * @param integer $link_id   The ID of the link being loaded.
     * @param integer $period_id An optional period from which to reference.
     * @return JsonResponse
     */
    public function show(
        string $sport,
        string $slug,
        int $season,
        string $link_slug,
        int $link_id,
        ?int $period_id = null
    ): JsonResponse {
        // Validate and load the game info.
        $game = Game::loadBySlug($sport, $season, $slug);
        if (!$game->isset()) {
            return API::sendNotFound('Game not found');
        }
        // Determine the appropriate period from which to process.
        $curr_period = GamePeriod::getCurrentPeriod($sport, $season);
        if (isset($period_id)) {
            // Validate the supplied argument.
            $period = GamePeriod::query()->where([
                ['sport', '=', $sport],
                ['season', '=', $season],
                ['period_id', '=', $period_id],
            ])->get();
            if (!$period->isset() || $period->period_order > $curr_period->period_order) {
                return API::sendNotFound('Game period not found');
            }
        } else {
            // Determine and use the selection's most recent period.
            $tbl_period = GamePeriod::getTable();
            $tbl_sel = SelectionModel::getTable();
            $period = GamePeriod::query()
                ->select("$tbl_period.*")
                ->join($tbl_sel, function ($join) use ($tbl_sel, $tbl_period, $game, $link_id) {
                    $join->on("$tbl_sel.sport", '=', "$tbl_period.sport")
                        ->on("$tbl_sel.season", '=', "$tbl_period.season")
                        ->where("$tbl_sel.game_id", '=', $game->game_id)
                        ->where("$tbl_sel.link_id", '=', $link_id)
                        ->on("$tbl_sel.period_id", '=', "$tbl_period.period_id");
                })
                ->where([
                    ["$tbl_period.sport", '=', $sport],
                    ["$tbl_period.season", '=', $season],
                    ["$tbl_period.period_order", '<=', $curr_period->period_order],
                ])->orderByDesc("$tbl_period.period_order")
                ->limit(1)
                ->get();

            // If this did not return a row, the selection passed is not valid (or available yet).
            if (!$period->isset()) {
                return API::sendNotFound('Selection not found or not available');
            }
        }
        // Validate (by loading) the link info.
        $link = SelectionModel::get($sport, $season, $game->game_id, $link_id, $period->period_id);
        if (!$link->isset() || $link->entity->slug != "{$link_slug}-{$link_id}") {
            return API::sendNotFound('Selection not found or not available');
        }
        // Get the additional info we need.
        $link->loadSecondary(['stats', 'recentResults']);

        return response()->json($link->rowToWeb());
    }
}
