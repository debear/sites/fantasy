<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\API;
use DeBear\Helpers\Fantasy\Records\Traits\Controller as TraitController;
use DeBear\Helpers\Fantasy\Records\Traits\Controller\EntrySel as TraitControllerEntrySel;
use DeBear\ORM\Fantasy\Common\GamePeriod as GamePeriodModel;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;
use DeBear\ORM\Fantasy\Records\EntryByPeriod as EntryByPeriodModel;
use DeBear\ORM\Fantasy\Records\Selection as SelectionModel;
use DeBear\Repositories\InternalCache;
use DeBear\Repositories\Time;

class EntrySel extends Controller
{
    use TraitController;
    use TraitControllerEntrySel;

    /**
     * The user's entry
     * @var EntryModel
     */
    protected $entry;
    /**
     * Info about the game being processed
     * @var GameModel
     */
    protected $game;
    /**
     * Info about the period being processed
     * @var GamePeriodModel
     */
    protected $period;
    /**
     * Info about the selection being processed
     * @var SelectionModel
     */
    protected $link;

    /**
     * Load the available selections for a given game period
     * @param string  $sport     The sport of the game being loaded.
     * @param string  $slug      The name of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return JsonResponse A JSON-based response in all instances
     */
    public function index(string $sport, string $slug, int $season, int $period_id): JsonResponse
    {
        // Validate the request.
        $validate = $this->validateAction($sport, $slug, $season, $period_id, null, false);
        if (isset($validate)) {
            return $validate;
        }
        $game_id = $this->game->game_id;

        // Now load the selections for this period.
        $args = $this->validateIndexQueryArguments();
        if (!isset($args)) {
            return API::sendBadRequest();
        } elseif (!SelectionModel::validatePeriod($sport, $season, $game_id, $period_id)) {
            return API::setStatus(422, 'Requested period not yet available');
        }
        $sel = SelectionModel::loadByPeriod($sport, $season, $game_id, $period_id, $args);
        if ($sel->totalRows() && !$sel->count()) {
            // No rows - when some are available - indicate bad pagination.
            return API::sendBadRequest();
        }

        // Determine which selections, if any, are not available to the user.
        if (!$this->game->canReuseSelection()) {
            $sel->setUnavailable($this->period);
        }
        // Determine if any selections are considered "Streak Busters".
        if ($this->game->hasStreakBuster()) {
            $sel->setStreakBusters();
        }
        // Determine, if applicable, the opponents already beaten by the user.
        if ($this->game->hasAchievement('opponent')) {
            $sel->setAchievedOpponents($this->game, $this->entry->getGameScore($this->game));
        }

        // Determine the next lock time for the period, which is when this response could be cached until.
        $headers = [];
        if (FrameworkConfig::get("debear.setup.js_cache_ttl.sel")) {
            $next_locktime = $this->period->getNextLockTime();
            if (isset($next_locktime)) {
                // Subtract our offset to prevent caching during the lineup posting period.
                // Negating, because Carbon's diff applies backwards to the order we are providing the arguments.
                $time_diff = max(0 - $next_locktime
                    ->subMinutes(FrameworkConfig::get("debear.sports.{$sport}.period.lineup_refresh"))
                    ->diffInSeconds(Time::object()->getNowFmt(), false), 0);
                if ($time_diff) {
                    // Set our caching headers.
                    $headers = [
                        'Cache-Control' => "max-age=$time_diff, private",
                        'Expires' => $next_locktime->format('r'), // As subMinutes affects the original object.
                        'X-Next-Locktime' => $time_diff,
                    ];
                }
            }
        }

        // Return our data response.
        return response()->json($sel->rowsToWeb())
            ->withHeaders($headers);
    }

    /**
     * Store a new selection for a team in a given game and period
     * @param string  $sport     The sport of the game being loaded.
     * @param string  $slug      The name of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return JsonResponse A JSON-based response in all instances
     */
    public function store(string $sport, string $slug, int $season, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateAction($sport, $slug, $season, $period_id, false, true);
        if (isset($validate)) {
            return $validate;
        }
        // Perform the requested action.
        EntryByPeriodModel::store($sport, $season, $this->game->game_id, $period_id, $this->link->link_id);
        return API::sendCreated('Selection saved');
    }

    /**
     * Update an existing selection for a team in a given game and period
     * @param string  $sport     The sport of the game being loaded.
     * @param string  $slug      The name of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return JsonResponse A JSON-based response in all instances
     */
    public function update(string $sport, string $slug, int $season, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateAction($sport, $slug, $season, $period_id, true, true);
        if (isset($validate)) {
            return $validate;
        }
        // Perform the requested action.
        EntryByPeriodModel::get($sport, $season, $this->game->game_id, $period_id)
            ->update(['link_id' => $this->link->link_id])
            ->audit('records_sel_update', 'Selection updated', ['period_id', 'link_id'])
            ->save();
        return API::sendResponse('Selection updated');
    }

    /**
     * Remove a stored selection for a team in a given game and period
     * @param string  $sport     The sport of the game being loaded.
     * @param string  $slug      The name of the game being loaded.
     * @param integer $season    The season for the game being loaded.
     * @param integer $period_id The ID of the period being loaded.
     * @return JsonResponse A JSON-based response in all instances
     */
    public function destroy(string $sport, string $slug, int $season, int $period_id): JsonResponse
    {
        // Validate the core of the request (arguments are valid and user has an entry).
        $validate = $this->validateAction($sport, $slug, $season, $period_id, true, false);
        if (isset($validate)) {
            return $validate;
        }
        // Perform the requested action.
        EntryByPeriodModel::get($sport, $season, $this->game->game_id, $period_id)
            ->audit('records_sel_remove', 'Selection removed', ['period_id', 'link_id'])
            ->deleteRow();
        return API::sendResponse('Selection removed');
    }

    /**
     * Validate the request arguments are appropriate
     * @param string       $sport         The sport of the game being loaded.
     * @param string       $slug          The name of the game being loaded.
     * @param integer      $season        The season for the game being loaded.
     * @param integer      $period_id     The ID of the period being loaded.
     * @param boolean|null $has_sel       Whether we should check for an existing selection, and if so how.
     * @param boolean      $check_payload Whether we should validate a selection payload.
     * @return JsonResponse|null A JSON-based response in case of a validation error, null otherwise
     */
    protected function validateAction(
        string $sport,
        string $slug,
        int $season,
        int $period_id,
        ?bool $has_sel,
        bool $check_payload
    ): ?JsonResponse {
        // Validate and load the game info.
        $this->game = GameModel::loadBySlug($sport, $season, $slug);
        if (!$this->game->isset()) {
            return API::sendNotFound('Game not found');
        }
        $game_id = $this->game->game_id;
        $this->game->loadSecondary(['achievements']);

        // Then the period requested.
        $this->period = GamePeriodModel::query()->where([
            ['sport', '=', $sport],
            ['season', '=', $season],
            ['period_id', '=', $period_id],
        ])->get();
        if (!$this->period->isset()) {
            return API::sendNotFound('Game period not found');
        }

        // Does the user have an entry and playing in this game?
        $this->entry = $this->getUserEntry();
        $score = $this->entry->getGameScore($this->game);
        if (!$this->entry->isset() || !$score->isset()) {
            return API::sendNotFound('Entry not found');
        }
        // If the user's entry is no longer active, it cannot access periods after it was eliminated.
        if (!$this->game->canRestartGame() && !$score->isActive()) {
            $last_period = $score->getLastSelectionPeriod($this->game);
            if ($last_period->isset() && $last_period->period_order < $this->period->period_order) {
                return API::setStatus(422, 'Period is not available to this entry'); // 422 = Unprocessable Entity.
            }
        }

        // Validate the request payload.
        $link_id = null;
        if ($check_payload) {
            $input_arr = InternalCache::object()->get('php://input');
            $link_id = ($input_arr['link_id'] ?? null);
            if (!is_numeric($link_id)) {
                return API::sendBadRequest();
            }
            $this->link = SelectionModel::get($sport, $season, $game_id, $link_id, $period_id);
            if (!$this->link->isset()) {
                return API::sendBadRequest();
            } elseif (!$this->link->entity->eventActive()) {
                return API::setStatus(410, 'Selection no longer available');
            } elseif ($this->link->is_locked) {
                return API::setStatus(423, 'Requested selection locked');
            } elseif (!$this->game->canReuseSelection()) {
                // Has the selection already been used when we are limiting their re-use?
                $period_order = $this->period->period_order;
                $historical = EntryByPeriodModel::getHistorical($sport, $season, $game_id, $period_order);
                if ($historical->where('link_id', $link_id)->count()) {
                    return API::sendConflict('Selection cannot be re-used');
                }
            }
        }

        // Validate the status of the user's current selection.
        if (isset($has_sel)) {
            // Does the curent status match expectation?
            $curr_sel = EntryByPeriodModel::get($sport, $season, $game_id, $period_id);
            if ($has_sel != $curr_sel->isset()) {
                $msg = ($curr_sel->isset() ? 'already saved' : 'not found');
                return API::sendConflict("Existing selection $msg");
            }
            // If the user does have a selection, is it locked?
            if ($curr_sel->isset() && $curr_sel->is_locked) {
                return API::setStatus(423, 'Current selection locked');
            }
        }

        // If we're here, the request is valid.
        return null;
    }
}
