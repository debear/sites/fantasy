<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTTP;
use DeBear\ORM\Fantasy\Records\Game;

class SiteTools extends Controller
{
    /**
     * Load a sitemap of the pages within the game
     * @return Response The relevant sitemap containing all the pages available within the game
     */
    public function sitemapXml(): Response
    {
        // This sitemap does not need to include the main homepage, as it is included in the default sitemap.
        $games = Game::loadAllOpen();
        $base_url = 'https:' . HTTP::buildDomain() . '/' . FrameworkConfig::get('debear.subsites.records.url');
        return response(view('fantasy.records.sitemap_xml', compact(['games', 'base_url'])))
            ->header('Content-Type', 'text/xml');
    }
}
