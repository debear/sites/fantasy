<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Fantasy\Records\Traits\Controller as TraitController;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Fantasy\Common\HelpSection;

class Help extends Controller
{
    use TraitController;

    /**
     * Show the help articles
     * @return Response
     */
    public function index(): Response
    {
        // Load the articles to render.
        $articles = HelpSection::loadArticles('records');
        // Update the navigation with entries.
        (User::object()->isLoggedIn() ? $this->getUserEntry() : null)?->addActiveGamesWithinNavigation();
        // Set appropriate page info.
        HTML::setNavCurrent('/help');
        HTML::setPageTitle('Help and Support');
        HTML::setMetaDescription('A resource for help and support in how to play {config|debear.names.external}');
        Resources::addCSS('_global/pages/help.css');
        Resources::addJS('_global/pages/help.js');
        return response(view('fantasy._global.help', compact('articles')));
    }
}
