<?php

namespace DeBear\Http\Controllers\Fantasy\Records;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\API;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Fantasy\Records\Traits\Controller as TraitController;
use DeBear\ORM\Fantasy\Records\Entry as EntryModel;
use DeBear\ORM\Fantasy\Records\Game as GameModel;
use DeBear\Repositories\Time;

class EntryGame extends Controller
{
    use TraitController;

    /**
     * The user's entry
     * @var EntryModel
     */
    protected $entry;
    /**
     * Info about the game being processed
     * @var GameModel
     */
    protected $game;

    /**
     * Join an entry to a specific game
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return Response An API Response object befitting the request
     */
    public function store(string $sport, int $season, int $game_id): Response
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($sport, $season, $game_id, true);
        if (isset($validation)) {
            return $validation;
        }
        // Validate the user hasn't already taken part in this game.
        if ($this->entry->getGameScore($this->game)->isset()) {
            return HTTP::setStatus(409, ['success' => false]); // 409 == Conflict.
        }
        // Create the link.
        $this->entry->gameJoin($sport, $season, $game_id);
        return HTTP::sendCreated(['success' => true]);
    }

    /**
     * Remove an entry to a specific game, when an entry has no selections made yet
     * @param string  $sport   The sport of the game being referenced.
     * @param integer $season  The season in which the game is taking place.
     * @param integer $game_id The ID of the game being referenced.
     * @return Response An API Response object befitting the request
     */
    public function destroy(string $sport, int $season, int $game_id): Response
    {
        // Perform our principle validation of the entry/game request details.
        $validation = $this->validateAction($sport, $season, $game_id, false);
        if (isset($validation)) {
            return $validation;
        }
        // Validate the user is taking part in this game and has not processed their first selection.
        $score = $this->entry->getGameScore($this->game);
        if (!$score->isset()) {
            return HTTP::sendNotFound(['success' => false]);
        } elseif ($score->processed) {
            // HTTP Status 423 == Locked.
            return HTTP::setStatus(423, ['success' => false]);
        }
        // Remove the link.
        $this->entry->gameLeave($sport, $season, $game_id);
        return HTTP::setStatus(200, ['success' => true]);
    }

    /**
     * Validate the incoming request for the user and game combination requested
     * @param string  $sport        The sport of the game being referenced.
     * @param integer $season       The season in which the game is taking place.
     * @param integer $game_id      The ID of the game being referenced.
     * @param boolean $create_entry A boolean indicating whether we create a user's missing entry for them.
     * @return Response|null A Response object in the case of a validation failure, null otherwise
     */
    protected function validateAction(string $sport, int $season, int $game_id, bool $create_entry): ?Response
    {
        // Validate we have a user with an active entry.
        $this->entry = $this->getUserEntry();
        if (!$this->entry->isset()) {
            if ($create_entry) {
                // We may be creating an entry for the user, so do so.
                $this->entry = EntryModel::createForUser();
                session()->flash('form_complete', 'Your entry to ' . FrameworkConfig::get('debear.names.section')
                    . ' has also been created &ndash; <a href="/records/my-entry">click here</a> to review the'
                    . ' settings.');
            } else {
                // But if not expecting this, then this is an error in the request.
                return HTTP::sendBadRequest(['success' => false]);
            }
        }
        // Validate the game reference - including whether the game is open for entries.
        $this->game = $game = GameModel::loadByGameRef($sport, $season, $game_id);
        $now = Time::object()->getNowFmt();
        $tDTS = 'toDateTimeString'; // Shorthand method invocation.
        if (!$game->isset() || $now < $game->date_open->$tDTS() || $now >= $game->date_complete->$tDTS()) {
            return HTTP::sendNotFound(['success' => false]);
        } elseif ($now >= $game->date_close->$tDTS()) {
            return HTTP::sendBadRequest(['success' => false]);
        }
        // If we're here, the request is valid.
        return null;
    }

    /**
     * View information about a single entry within a game
     * @param string  $sport      The sport of the game being loaded.
     * @param string  $slug       The name of the game being loaded.
     * @param integer $season     The season for the game being loaded.
     * @param string  $entry_slug The name of the entry being loaded.
     * @param integer $user_id    The ID of the entry being loaded.
     * @return JsonResponse
     */
    public function summary(string $sport, string $slug, int $season, string $entry_slug, int $user_id): JsonResponse
    {
        // Validate and load the game info.
        $game = GameModel::loadBySlug($sport, $season, $slug);
        if (!$game->isset()) {
            return API::sendNotFound('Game not found');
        }
        // Validate and load the entry info.
        $entry = EntryModel::load($user_id);
        if (
            !$entry->isset()
            || ($entry->slug != "$entry_slug-$user_id")
            || !array_key_exists($game->game_ref, $entry->games_active)
        ) {
            return API::sendNotFound('Entry not found');
        }
        // Expand the info we need to retrieve on a per-entry summary, and return.
        $entry_game = $entry->loadExpandedSelections($game->game_ref);
        return response()->json($entry_game->rowToWeb([
            'no-current' => !$game->hasCurrentLeaderboard(),
            'opp-rating' => $game->isMajorLeague(),
        ]));
    }
}
