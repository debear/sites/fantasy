<?php

namespace DeBear\Http\Controllers\Fantasy;

use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\ORM\Fantasy\Admin\AutomatedUser;
use DeBear\ORM\Fantasy\Admin\User;

class Profile extends Controller
{
    /**
     * Performance and summary info of a particular user
     * @param string $user_id The (string) ID of the user to be viewed.
     * @return Response|RedirectResponse The relevant content response
     */
    public function show(string $user_id): Response|RedirectResponse
    {
        // Validate the user.
        $user = User::loadProfile($user_id) ?? AutomatedUser::loadProfile($user_id);
        if (!isset($user)) {
            return HTTP::sendNotFound();
        }

        // Custom page config.
        HTML::setNavCurrent('/profile');
        HTML::linkMetaDescription('profile');
        HTML::setPageTitle(['Profile', $user->display_name]);
        Resources::addCSS('nfl-cap/teams/tiny.css');
        Resources::addCSS('_global/pages/profile.css');
        Resources::addCSS('records/profile.css');
        Resources::addJS('_global/pages/profile.js');

        return response(view('fantasy.profile.view', compact(['user'])));
    }
}
